<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsbestmanufacturers}default>statsbestmanufacturers_4f29d8c727dcf2022ac241cb96c31083'] = 'خالی کردن ثبت شده ها';
$_MODULE['<{statsbestmanufacturers}default>statsbestmanufacturers_f5c493141bb4b2508c5938fd9353291a'] = 'نمايش %1$s از %2$s';
$_MODULE['<{statsbestmanufacturers}default>statsbestmanufacturers_49ee3087348e8d44e1feda1917443987'] = 'نام';
$_MODULE['<{statsbestmanufacturers}default>statsbestmanufacturers_2a0440eec72540c5b30d9199c01f348c'] = 'تعداد فروخته شده';
$_MODULE['<{statsbestmanufacturers}default>statsbestmanufacturers_ea067eb37801c5aab1a1c685eb97d601'] = 'مجموع پرداختی';
$_MODULE['<{statsbestmanufacturers}default>statsbestmanufacturers_72fd9b5482201824daae557360d91196'] = 'بهترین تولیدکنندگان';
$_MODULE['<{statsbestmanufacturers}default>statsbestmanufacturers_aa1b814546fe3e2e96ef3a185e4df8e9'] = 'لیستی از بهترین تولیدکنندگان';
$_MODULE['<{statsbestmanufacturers}default>statsbestmanufacturers_998e4c5c80f27dec552e99dfed34889a'] = 'استخراج CSV';
