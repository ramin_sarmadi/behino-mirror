<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockpaymentlogo}default>blockpaymentlogo_ea2d4e3948f9543e5939efd1663ddef5'] = 'بخش آرم درگاههای پرداخت';
$_MODULE['<{blockpaymentlogo}default>blockpaymentlogo_d272c8b17089521db4660c7852f8839c'] = 'افزودن یک بلوک برای نمایش لوگو درگاه های پرداخت.';
$_MODULE['<{blockpaymentlogo}default>blockpaymentlogo_fb0c64625321a2f2e5e8800a7ddbd759'] = 'آرم درگاه پرداخت';
$_MODULE['<{blockpaymentlogo}default>blockpaymentlogo_efc226b17e0532afff43be870bff0de7'] = 'تنظیمات به روز شد';
$_MODULE['<{blockpaymentlogo}default>blockpaymentlogo_5c5e5371da7ab2c28d1af066a1a1cc0d'] = 'هیچ صفحه مدیریت محتوایی وجود ندارد';
$_MODULE['<{blockpaymentlogo}default>blockpaymentlogo_f1206f9fadc5ce41694f69129aecac26'] = 'ویرایش';
$_MODULE['<{blockpaymentlogo}default>blockpaymentlogo_4b3c34d991275b9fdb0facfcea561be9'] = 'صفحه مدیریت محتوا برای لینک';
$_MODULE['<{blockpaymentlogo}default>blockpaymentlogo_01bb71b3e70e9e5057c9678a903d47ac'] = 'انتخاب یک صفحه';
$_MODULE['<{blockpaymentlogo}default>blockpaymentlogo_d4dccb8ca2dac4e53c01bd9954755332'] = 'ذخیره تنظیمات';
