<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{lofdeal}default>default_9e834f13e35e4edf64863ab414a6217a'] = 'تخفیف';
$_MODULE['<{lofdeal}default>default_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{lofdeal}default>default_545f6c2f382c04810103b3e5e6f7d841'] = 'بینهایت';
$_MODULE['<{lofdeal}default>default_755c2ee898997b914a261928d3f9bf08'] = 'تخفیف های امروز';
$_MODULE['<{lofdeal}default>default_b24344b6a2c4a94c9b0d0ebd4602aeca'] = 'نمایش بیشتر';
$_MODULE['<{lofdeal}default>list_e807d3ccf8d24c8c1a3d86db5da78da8'] = 'روز';
$_MODULE['<{lofdeal}default>list_6a7e73161603d87b26a8eac49dab0a9c'] = 'ساعت';
$_MODULE['<{lofdeal}default>list_78d811e98514cd165dda532286610fd2'] = 'دقیقه';
$_MODULE['<{lofdeal}default>list_2d5cc554c126edb041901fa7d79b558b'] = 'ثانیه';
$_MODULE['<{lofdeal}default>list_dd1f775e443ff3b9a89270713580a51b'] = 'قبلی';
$_MODULE['<{lofdeal}default>list_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'بعدی';
