<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{graphxmlswfcharts}default>graphxmlswfcharts_fd1d6df8df2837bc1690e3ca9d84f502'] = 'نمودارهای XML/SWF';
$_MODULE['<{graphxmlswfcharts}default>graphxmlswfcharts_723f92861aa774c9d255e0b8c42c29e8'] = 'نمودارهای XML/SWF، یک ابزار ساده و قدرتمند که از Adobe Flash برای ایجاد نمودارها و گراف‌هایی با داده های پویا، استفاده می کند.';
