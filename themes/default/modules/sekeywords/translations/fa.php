<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{sekeywords}default>sekeywords_65b0b42febc8ea16db4652eab6f420a4'] = 'کلمات کلیدی موتور جستجو';
$_MODULE['<{sekeywords}default>sekeywords_de13be6263895a5efe4d51e15ab1535e'] = 'نمایش کلمه های کلیدی که مراجعین را به وب سایت شما هدایت کرده است';
$_MODULE['<{sekeywords}default>sekeywords_16d5f8dc3bc4411c85848ae9cf6a947a'] = '%d كلمه كليدي با درخواست شما مطابقت دارند.';
$_MODULE['<{sekeywords}default>sekeywords_5029f8eef402bb8ddd6191dffb5e7c19'] = '%d كلمه كليدي با درخواست شما مطابقت دارند.';
$_MODULE['<{sekeywords}default>sekeywords_867343577fa1f33caa632a19543bd252'] = 'کلمه های کلیدی';
$_MODULE['<{sekeywords}default>sekeywords_e52e6aa1a43a0187e44f048f658db5f9'] = 'رویداد';
$_MODULE['<{sekeywords}default>sekeywords_998e4c5c80f27dec552e99dfed34889a'] = 'استخراج CSV';
$_MODULE['<{sekeywords}default>sekeywords_0849140171616600e8f2c35f0a225212'] = 'فیلتر با کلمه کلیدی';
$_MODULE['<{sekeywords}default>sekeywords_6e632566b6e16dbd2273e83d7c53182b'] = 'و حداقل رویداد';
$_MODULE['<{sekeywords}default>sekeywords_fa73b7cd0d2681f6d871c9ef4023ad39'] = 'اعمال';
$_MODULE['<{sekeywords}default>sekeywords_7b48f6cc4a1dde7fca9597e717c2465f'] = 'هیچ کلمه کلیدی نیست';
$_MODULE['<{sekeywords}default>sekeywords_6602bbeb2956c035fb4cb5e844a4861b'] = 'راهنما';
$_MODULE['<{sekeywords}default>sekeywords_9ed50bd6876a9273f2192c224b87657b'] = 'تشخیص کلیدواژه های موتورهای جستجوی خارجی';
$_MODULE['<{sekeywords}default>sekeywords_6534eadba477de8a632ff59ac20b572f'] = 'يكي از معمول ترين راههاي پيدا كردن يك سايت از طريق يك موتور جستجو.';
$_MODULE['<{sekeywords}default>sekeywords_0d7ce5d105706cedba41887d3f1c0ea1'] = 'با مشخص كردن بيشترين كلمات مورد استفاده توسط كاربران سايت، به شما اجازه مي دهد تصميم بگيريد كدام كالاها را بايد جلوتر قرار دهيد تا بازديدكننده ها و مشتريان بالفوه بيشتري جذب كنيد.';
$_MODULE['<{sekeywords}default>sekeywords_359f9e79e746fa9f684e5cda9e60ca2e'] = 'چگونه این مورد انجام می شود؟';
$_MODULE['<{sekeywords}default>sekeywords_722e091cccbd9a9ec8f4a35bf1f35893'] = 'زماني كه يك بازديد كننده به سايت شما سر ميزند،سرور جايگاه قبلي آنها را مشخص مي نمايد. اين ماژول URL را تجزيه مي كند و لغات كليدي را در آن پيدا مي كند.';
$_MODULE['<{sekeywords}default>sekeywords_d8b08c48a8d8e739399594adec89458a'] = 'در حال حاضر، اين موتورهاي جستجو را مديريت مي كند: %1$s و %2$s';
$_MODULE['<{sekeywords}default>sekeywords_50dca930b804e845a852512b44d51c52'] = 'به زودی امکان افزودن پویای موتور جستجو و کمک به این ماژول فراهم می شود.';
$_MODULE['<{sekeywords}default>sekeywords_e15832aa200f342e8f4ab580b43a72a8'] = '10 کلمه کلیدی اول';
$_MODULE['<{sekeywords}default>sekeywords_52ef9633d88a7480b3a938ff9eaa2a25'] = 'سایر موارد';
