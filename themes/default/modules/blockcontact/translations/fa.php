<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcontact}default>blockcontact_0f45a6908556b5b1d7c7d79e60fa3fa7'] = 'بخش  تماس با ما';
$_MODULE['<{blockcontact}default>blockcontact_318ed85b9852475f24127167815e85d9'] = 'این امکان را به شما می دهد تا اطلاعات اضافی در مورد سرویس مشتری اضافه نمایید';
$_MODULE['<{blockcontact}default>blockcontact_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'تنظیمات بروز شد';
$_MODULE['<{blockcontact}default>blockcontact_17870f54a180e69e60e84125f805db67'] = 'شماره تلفن';
$_MODULE['<{blockcontact}default>blockcontact_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'ایمیل';
$_MODULE['<{blockcontact}default>blockcontact_b17f3f4dcf653a5776792498a9b44d6a'] = 'به روز رسانی تنظیمات';
$_MODULE['<{blockcontact}default>blockcontact_02d4482d332e1aef3437cd61c9bcc624'] = 'تماس با ما';
$_MODULE['<{blockcontact}default>blockcontact_75858d311c84e7ba706b69bea5c71d36'] = 'خط‌هاي فوري ما از 7 تا 24 در دسترس مي باشند';
$_MODULE['<{blockcontact}default>blockcontact_673ae02fffb72f0fe68a66f096a01347'] = 'تلفن';
$_MODULE['<{blockcontact}default>blockcontact_736c5a7e834b7021bfa97180fc453115'] = 'تماس با خط‌هاي فوري ما';
