<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsdata}default>statsdata_a51950bf91ba55cd93a33ce3f8d448c2'] = 'کندوکاو داده های آماری';
$_MODULE['<{statsdata}default>statsdata_c77dfd683d0d76940e5e04cb24e8bce1'] = 'جهت استفاده از آمارها، این ماژول باید فعال باشد.';
$_MODULE['<{statsdata}default>statsdata_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'تنظیمات به روز شد';
$_MODULE['<{statsdata}default>statsdata_f4f70727dc34561dfde1a3c529b6205c'] = 'تنظیمات';
$_MODULE['<{statsdata}default>statsdata_1a5b75c4be3c0100c99764b21e844cc8'] = 'ذخیره صفحات بازدید شده برای هر مشتری';
$_MODULE['<{statsdata}default>statsdata_93cba07454f06a4a960172bbd6e2a435'] = 'بله';
$_MODULE['<{statsdata}default>statsdata_bafd7322c6e97d25b6299b5d6fe8920b'] = 'خیر';
$_MODULE['<{statsdata}default>statsdata_f3e159134846bd13c0c967f361229233'] = 'بازدید مشتری مقدار زیادی از سی پی یو و فضای پایگاه داده را استفاده می کند';
$_MODULE['<{statsdata}default>statsdata_b368e11d9d23b26f1fb4a1b78584560d'] = 'ذخیره بازدید عمومی';
$_MODULE['<{statsdata}default>statsdata_339acfd90b82e91ce9141ec75e4bff24'] = 'بازدید عمومی منابع کمتری از بازدید مشتری استفاده میکند اما با این حال منابعی را بکار می گیرد.';
$_MODULE['<{statsdata}default>statsdata_c0e4406117ba4c29c4d66e3069ebf3d3'] = 'تشخیص پلاگین ها';
$_MODULE['<{statsdata}default>statsdata_7a6cf506bde903b2b30c0a4cbcfa0291'] = 'تشخصی پلاگین ها 20 کیلوبایت فایل اضافی برای مشتری ها لود می کند.';
$_MODULE['<{statsdata}default>statsdata_06933067aafd48425d67bcb01bba5cb6'] = 'به روز رسانی';
