<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statscarrier}default>statscarrier_2e6774abc54cb13cef2c5bfd5a2cb463'] = 'پراکندگی حامل ها';
$_MODULE['<{statscarrier}default>statscarrier_c521b5a8b0e2966709e4338368e59054'] = 'نمایش پراکندگی حامل ها';
$_MODULE['<{statscarrier}default>statscarrier_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'همه';
$_MODULE['<{statscarrier}default>statscarrier_d7778d0c64b6ba21494c97f77a66885a'] = 'فیلتر';
$_MODULE['<{statscarrier}default>statscarrier_b4db3f09700b25a1acc487d3488d0216'] = 'این نمودار نشان دهنده توزیع حامل برای سفارشات شما است.  شما همچنین می توانید آن را به سفارشات در یک استان محدود کنید.';
$_MODULE['<{statscarrier}default>statscarrier_998e4c5c80f27dec552e99dfed34889a'] = 'استخراج CSV';
$_MODULE['<{statscarrier}default>statscarrier_ae916988f1944283efa2968808a71287'] = 'هیچ سفارش معتبری برای این دوره وجود ندارد.';
$_MODULE['<{statscarrier}default>statscarrier_d5b9d0daaf017332f1f8188ab2a3f802'] = 'درصد سفارشات برحسب حامل';
