<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statslive}default>statslive_fa55230e9791f2b71322869318a5f00f'] = 'مراجعین آنلاین';
$_MODULE['<{statslive}default>statslive_b8a5ea9b6e7f2d0b56fbb18e5b6b9246'] = 'نمایش فهرستی از مشتریان و بازدیدکنندگان آنلاین فعلی.';
$_MODULE['<{statslive}default>statslive_e6214c05e17c56f81e4a56f4c4fdce8c'] = 'شما  باید گزینه \"بازدید برای هر مشتری\" را در ماژول \"بررسی داده های آماری\" را  فعال کنید تا بتوانید صفحاتی را که هم اکنون مشتری های شما بازدید می  کنند، ببینید.';
$_MODULE['<{statslive}default>statslive_5c948349bdf1a7a77ba54497d019e4ca'] = 'مشتری های آنلاین';
$_MODULE['<{statslive}default>statslive_66c4c5112f455a19afde47829df363fa'] = 'مجموع:';
$_MODULE['<{statslive}default>statslive_b718adec73e04ce3ec720dd11a06a308'] = 'شناسه';
$_MODULE['<{statslive}default>statslive_49ee3087348e8d44e1feda1917443987'] = 'نام';
$_MODULE['<{statslive}default>statslive_f88589c3e803217e3f6fe9d8e740e6e8'] = 'صفحه جاری';
$_MODULE['<{statslive}default>statslive_4351cfebe4b61d8aa5efa1d020710005'] = 'نمایش';
$_MODULE['<{statslive}default>statslive_4ce4b5e9a6a3e91ac46dec882a36e0db'] = 'هیچ مشتری آنلاین نیست.';
$_MODULE['<{statslive}default>statslive_adb831a7fdd83dd1e2a309ce7591dff8'] = 'مهمان';
$_MODULE['<{statslive}default>statslive_a12a3079e14ced46e69ba52b8a90b21a'] = 'ip';
$_MODULE['<{statslive}default>statslive_38c50b731f70abc42c8baa3e7399b413'] = 'از زمان';
$_MODULE['<{statslive}default>statslive_13aa8652e950bb7c4b9b213e6d8d0dc5'] = 'صفحه جاری';
$_MODULE['<{statslive}default>statslive_b6f05e5ddde1ec63d992d61144452dfa'] = 'ارجاع کننده';
$_MODULE['<{statslive}default>statslive_ec0fc0100c4fc1ce4eea230c3dc10360'] = 'تعریف نشده';
$_MODULE['<{statslive}default>statslive_6adf97f83acf6453d4a6a4b1070f3754'] = 'هیچ';
$_MODULE['<{statslive}default>statslive_a55533db46597bee3cd16899c007257e'] = 'بازدیدکننده ای آنلاین نیست.';
