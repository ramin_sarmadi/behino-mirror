<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsproduct}default>statsproduct_78e454064a7d3a7755a011a3b79f31a7'] = 'جزئیات محصول';
$_MODULE['<{statsproduct}default>statsproduct_285201dab507840d082d58dc13602b00'] = 'دریافت آمار جزئی برای هر محصول';
$_MODULE['<{statsproduct}default>statsproduct_3ec365dd533ddb7ef3d1c111186ce872'] = 'جزئیات';
$_MODULE['<{statsproduct}default>statsproduct_981a6013008c8f4b75836b0f415b05b8'] = 'خرید کل:';
$_MODULE['<{statsproduct}default>statsproduct_af7cba0f47dac2ad181cc0471979a746'] = 'فروش ها (Tx-):';
$_MODULE['<{statsproduct}default>statsproduct_b262bbde88b3b8dca3b27d730aca69bf'] = 'مجموع نمایش ها:';
$_MODULE['<{statsproduct}default>statsproduct_bebbab2c1ac732ccef4181f51ed52967'] = 'نرخ تبدیل:';
$_MODULE['<{statsproduct}default>statsproduct_998e4c5c80f27dec552e99dfed34889a'] = 'استخراج CSV';
$_MODULE['<{statsproduct}default>statsproduct_c90b9dca2d3f5bb6e0d0bdceac8a631d'] = 'پراکندگی خصوصیت فروش';
$_MODULE['<{statsproduct}default>statsproduct_11ff9f68afb6b8b5b8eda218d7c83a65'] = 'فروش ها';
$_MODULE['<{statsproduct}default>statsproduct_44749712dbec183e983dcd78a7736c41'] = 'تاریخ';
$_MODULE['<{statsproduct}default>statsproduct_a240fa27925a635b08dc28c9e4f9216d'] = 'سفارش';
$_MODULE['<{statsproduct}default>statsproduct_ce26601dac0dea138b7295f02b7620a7'] = 'مشتری';
$_MODULE['<{statsproduct}default>statsproduct_f2bbdf9f72c085adc4d0404e370f0f4c'] = 'خصوصیت';
$_MODULE['<{statsproduct}default>statsproduct_694e8d1f2ee056f98ee488bdc4982d73'] = 'تعداد';
$_MODULE['<{statsproduct}default>statsproduct_3601146c4e948c32b6424d2c0a7f0118'] = 'قیمت';
$_MODULE['<{statsproduct}default>statsproduct_87017c9259838bb0918a2ab4f96016c0'] = 'حد وسط فروش';
$_MODULE['<{statsproduct}default>statsproduct_df644ae155e79abf54175bd15d75f363'] = 'نام محصول';
$_MODULE['<{statsproduct}default>statsproduct_2a0440eec72540c5b30d9199c01f348c'] = 'تعداد فروش';
$_MODULE['<{statsproduct}default>statsproduct_844c29394eea07066bb2efefc35784ec'] = 'قیمت متوسط';
$_MODULE['<{statsproduct}default>statsproduct_0173374ac20f5843d58b553d5b226ef6'] = 'انتخاب یک شاخه';
$_MODULE['<{statsproduct}default>statsproduct_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'همه';
$_MODULE['<{statsproduct}default>statsproduct_818f7defe18dbc97da82d054831df2ad'] = 'جهت دیدن آمار یک محصول روی آن کلیک کنید.';
$_MODULE['<{statsproduct}default>statsproduct_6ccf03bc05e4b749b74b5d577c7e7d3a'] = 'محصولات موجود';
$_MODULE['<{statsproduct}default>statsproduct_12d3c7a4296542c62474856ec452c045'] = 'مرجع';
$_MODULE['<{statsproduct}default>statsproduct_49ee3087348e8d44e1feda1917443987'] = 'نام';
$_MODULE['<{statsproduct}default>statsproduct_7bd5825a187064017975513b95d7f7de'] = 'موجودی';
$_MODULE['<{statsproduct}default>statsproduct_6602bbeb2956c035fb4cb5e844a4861b'] = 'راهنما';
$_MODULE['<{statsproduct}default>statsproduct_af6de9b8b390e324cd90cc17779783ba'] = 'تعداد خریدهای انجام گرفته در مقایسه با بازدیدهای صورت گرفته';
$_MODULE['<{statsproduct}default>statsproduct_6e13e61b8535a66feb27d285f5c42855'] = 'پس از انتخاب یک شاخه و انتخاب یک محصول، نمودارهای اطلاعاتی نمایان خواهند شد. سپس، شما می توانید به تجزیه و تحلیل آنها بپردازید.';
$_MODULE['<{statsproduct}default>statsproduct_ff7008d8daed8cf92213ca8955a42d92'] = 'اگر  متوجه شده اید که یک محصول موفق است و اغلب خریده می شود، اما تعداد  بازدید آن بسیار کم است، شما می بایست آن محصول را در فروشگاه خود در معرض  دید کاربران برجسته تر سازید.';
$_MODULE['<{statsproduct}default>statsproduct_e22269bb51f9f2394e148716babbafbb'] = 'محبوبیت';
$_MODULE['<{statsproduct}default>statsproduct_5e9613e58f3bdbc89b1fef07274c0877'] = 'بازدیدها (x100)';
$_MODULE['<{statsproduct}default>statsproduct_287234a1ff35a314b5b6bc4e5828e745'] = 'خصوصیت‌ها';
$_MODULE['<{statsproduct}default>statsproduct_27ce7f8b5623b2e2df568d64cf051607'] = 'موجودی';
