<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocknewsletter}default>blocknewsletter_9e31b08a00c1ed653bcaa517dee84714'] = 'بخش خبرنامه';
$_MODULE['<{blocknewsletter}default>blocknewsletter_ba457fab18d697d978befb95e827eb91'] = 'افزودن یک بلوک برای عضویت در خبرنامه.';
$_MODULE['<{blocknewsletter}default>blocknewsletter_396c88991101b5ca362932952293d291'] = 'آیا مطمئن هستید، می خواهید همه تماس هایتان را حذف کنید؟';
$_MODULE['<{blocknewsletter}default>blocknewsletter_524ec52bffb5d76b943ff1ae20c27f0a'] = 'کد فاکتور معتبر نمی باشد';
$_MODULE['<{blocknewsletter}default>blocknewsletter_ff0a3b7f3daef040faf89a88fdac01b7'] = 'به روز شد';
$_MODULE['<{blocknewsletter}default>blocknewsletter_f4f70727dc34561dfde1a3c529b6205c'] = 'تنظیمات';
$_MODULE['<{blocknewsletter}default>blocknewsletter_6d56757498a13d0c1e54261795469e99'] = 'نمایش تنظیمات در صفحه جدید؟';
$_MODULE['<{blocknewsletter}default>blocknewsletter_93cba07454f06a4a960172bbd6e2a435'] = 'بلی';
$_MODULE['<{blocknewsletter}default>blocknewsletter_bafd7322c6e97d25b6299b5d6fe8920b'] = 'خیر';
$_MODULE['<{blocknewsletter}default>blocknewsletter_54d2f1bab16b550e32395a7e6edb8de5'] = 'ارسال ایمیل تاییدیه پس از عضویت؟';
$_MODULE['<{blocknewsletter}default>blocknewsletter_b37f32d509cf5aabe806b16791588aa3'] = 'کد فاکتور خوش آمدگویی';
$_MODULE['<{blocknewsletter}default>blocknewsletter_506e58042922bff5bd753dc612e84f5b'] = 'کد فاکتور خوش آمدگویی';
$_MODULE['<{blocknewsletter}default>blocknewsletter_1d612b943b8f4792bff603384a46e5f1'] = 'برای غیر فعال کردن خالی رها کنید';
$_MODULE['<{blocknewsletter}default>blocknewsletter_06933067aafd48425d67bcb01bba5cb6'] = 'به روز رسانی';
$_MODULE['<{blocknewsletter}default>blocknewsletter_59e4904d4366fab35a8282f418e699de'] = 'آدرس ایمیل نامعتبر است';
$_MODULE['<{blocknewsletter}default>blocknewsletter_1c623b291d95f4f1b1d0c03d0dc0ffa1'] = 'آدرس ایمیل ثبت نشده است';
$_MODULE['<{blocknewsletter}default>blocknewsletter_3b1f17a6fd92e35bc744e986b8e7a61c'] = 'خطایی در زمان حذف اشتراک رخ داده است';
$_MODULE['<{blocknewsletter}default>blocknewsletter_f7dc297e2a139ab4f5a771825b46df43'] = 'حذف اشتراک با موفقیت انجام شد';
$_MODULE['<{blocknewsletter}default>blocknewsletter_f6618fce0acbfca15e1f2b0991ddbcd0'] = 'این آدرس ایمیل قبلاً ثبت شده است';
$_MODULE['<{blocknewsletter}default>blocknewsletter_e172cb581e84f43a3bd8ee4e3b512197'] = 'خطایی در زمان عضویت اشتراک رخ داده است';
$_MODULE['<{blocknewsletter}default>blocknewsletter_ebc069b1b9a2c48edfa39e344103be1e'] = 'ایمیل تایید برای شما ارسال شد، لطفآ ایمیل خود را بررسی نمایید.';
$_MODULE['<{blocknewsletter}default>blocknewsletter_77c576a354d5d6f5e2c1ba50167addf8'] = 'اشتراک با موفقیت ایجاد شد';
$_MODULE['<{blocknewsletter}default>blocknewsletter_99c8b8e5e51bf8f00f1d66820684be9a'] = 'این آدرس ایمیل قبلاً ثبت شده است و یا نامعتبر است';
$_MODULE['<{blocknewsletter}default>blocknewsletter_4e1c51e233f1ed368c58db9ef09010ba'] = 'تشکر برای عضویت در خبرنامه ما';
$_MODULE['<{blocknewsletter}default>blocknewsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'خبرنامه';
$_MODULE['<{blocknewsletter}default>blocknewsletter_198584454b0ce1101ff5b50323325aa8'] = 'ایمیل شما';
$_MODULE['<{blocknewsletter}default>blocknewsletter_b26917587d98330d93f87808fc9d7267'] = 'عضویت اشتراک';
$_MODULE['<{blocknewsletter}default>blocknewsletter_4182c8f19d40c7ca236a5f4f83faeb6b'] = 'حذف اشتراک';
