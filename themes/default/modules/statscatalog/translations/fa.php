<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statscatalog}default>statscatalog_cf3aa21c6a2147ddbd86f34091daeccd'] = 'آمار کاتالوگ';
$_MODULE['<{statscatalog}default>statscatalog_226ed1224c5b2db3d3e0716bb4f74da5'] = 'آمار عمومی درباره محصولات شما.';
$_MODULE['<{statscatalog}default>statscatalog_74cda5a02df704cc5c3e8fee7fc0f7bc'] = '( 1 خريد / %d بازديدكننده)';
$_MODULE['<{statscatalog}default>statscatalog_0173374ac20f5843d58b553d5b226ef6'] = 'انتخاب یک طبقه بندی';
$_MODULE['<{statscatalog}default>statscatalog_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'همه';
$_MODULE['<{statscatalog}default>statscatalog_a7b623414d4b6a3225b4e935babec6d2'] = 'محصولات موجود:';
$_MODULE['<{statscatalog}default>statscatalog_1099377f1598a0856e2457a5145d89c2'] = 'قیمت متوسط (قیمت پایه):';
$_MODULE['<{statscatalog}default>statscatalog_48a93dc02c74f3065af1ba47fca070d0'] = 'صفحه های محصولات بازدید شده:';
$_MODULE['<{statscatalog}default>statscatalog_156e5c5872c9af24a5c982da07a883c2'] = 'محصولات خریداری شده:';
$_MODULE['<{statscatalog}default>statscatalog_85f179d4142ca061d49605a7fffdc09d'] = 'متوسط تعداد بازدید صفحات:';
$_MODULE['<{statscatalog}default>statscatalog_05ff4bfc3baf0acd31a72f1ac754de04'] = 'متوسط تعداد خرید:';
$_MODULE['<{statscatalog}default>statscatalog_c09d09e371989d89847049c9574b6b8e'] = 'تصاویر موجود:';
$_MODULE['<{statscatalog}default>statscatalog_65275d1b04037d8c8e42425002110363'] = 'متوسط تعداد تصاویر:';
$_MODULE['<{statscatalog}default>statscatalog_51b8891d531ad91128ba58c8928322ab'] = 'محصولاتی که هیچ وقت بازدید نشده اند:';
$_MODULE['<{statscatalog}default>statscatalog_8725647ef741e5d48c1e6f652ce80b50'] = 'محصولات بدون خرید:';
$_MODULE['<{statscatalog}default>statscatalog_b86770bc713186bcf43dbb1164c5fd28'] = 'نرخ تبدیل*:';
$_MODULE['<{statscatalog}default>statscatalog_0468e0edbf9f5807c25c106248bd7401'] = 'متوسط نرخ تبدیل برای صفحه محصولات. چون خرید یک محصول بدون دیدن صفحه محصول هم ممکن است بنابرین این نرخ ممکن است بیشتر از یک باشد.';
$_MODULE['<{statscatalog}default>statscatalog_58a714d3e9bb2902a5b688c99bd4d8e6'] = 'محصولات بدون خرید';
$_MODULE['<{statscatalog}default>statscatalog_b718adec73e04ce3ec720dd11a06a308'] = 'شناسه';
$_MODULE['<{statscatalog}default>statscatalog_49ee3087348e8d44e1feda1917443987'] = 'نام';
$_MODULE['<{statscatalog}default>statscatalog_8e7c9a35104a5a68199678bd6bc5d187'] = 'ویرایش / دیدن';
