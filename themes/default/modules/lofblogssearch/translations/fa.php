<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{lofblogssearch}default>lofblogssearch_fd0e8333d1a83f9f98e2bb27f0007eb0'] = 'جستجو مطالب وبلاگ';
$_MODULE['<{lofblogssearch}default>lofblogssearch_6f110ff14a2ed76cd6f322c44fe695c2'] = 'نمایش فرم جستجو مطالب در بخش کاربری';
$_MODULE['<{lofblogssearch}default>lofblogssearch_a5b17f28c22d6c09b2b8b4a6a2a7a7b0'] = 'آیا شما واقعا می خواهید مطالب آرشیو وبلاگ را حذف کنید';
$_MODULE['<{lofblogssearch}default>lofblogssearch_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات بروز رسان شد';
$_MODULE['<{lofblogssearch}default>form_06933067aafd48425d67bcb01bba5cb6'] = 'به روز رسانی';
$_MODULE['<{lofblogssearch}default>params_lang_4ede758053afb49f54ea2ef0d5276910'] = 'تنظیمات عمومی';
$_MODULE['<{lofblogssearch}default>params_lang_d721757161f7f70c5b0949fdb6ec2c30'] = 'قالب';
$_MODULE['<{lofblogssearch}default>left_88a287b90728bd1dea45acfedb29bda3'] = 'جستجوی بلاگ';
$_MODULE['<{lofblogssearch}default>left_13348442cc6a27032d2b4aa28b75a5d3'] = 'جستجو';
