{if ($products_compare_in_cookie|count)>0}
    {include file="./product-list-compare.tpl" products=$products_compare_in_cookie}
{/if}
{capture name=path}{l s='Product Comparison'}{/capture}
<div class="wrapper-comparison">
    {*$products_compare_in_cookie|print_r*}

    <h1 class="title_category ">{l s='Product Comparison'}
        <input type="text" size="25" id="ProductItemName" placeholder="{l s='جستجو برای مقایسه محصولات...'}"
               autocomplete="off"/>

        <div class="compare-results" style="display: none">
            <img src="{$img_dir}loading.gif" id="retrive-compare">
            <ul class="compare-append">
            </ul>
        </div>
    </h1>
    {$html}
    {if $hasProduct}
        <div class="products_block">
            <table id="product_comparison">
                <td width="20%" class="td_empty">
                </td>
                {assign var='taxes_behavior' value=false}
                {if $use_taxes && (!$priceDisplay  || $priceDisplay == 2)}
                    {assign var='taxes_behavior' value=true}
                {/if}
                {foreach from=$products item=product name=for_products}
                    {assign var='replace_id' value=$product->id|cat:'|'}
                    <td width="{$width}%" class="ajax_block_product product_block comparison_infos">
                        <div class="product-container clearfix">
                            <div class="center_block">
                                <a href="{$product->getLink()}" title="{$product->name|escape:html:'UTF-8'}"
                                   class="product_image">
                                    <img src="{$link->getImageLink($product->link_rewrite, $product->id_image, 'compare-allinmart')}"
                                         alt="{$product->name|escape:html:'UTF-8'}"/>
                                </a>
                            </div>
                            <div class="right_block clearfix">
                                <h3 class="s_title_block"><a href="{$product->getLink()}"
                                                             title="{$product->name|truncate:32:'...'|escape:'htmlall':'UTF-8'}">{$product->name|truncate:27:'...'|escape:'htmlall':'UTF-8'}</a>
                                </h3>

                                <div class="product_desc"><a
                                            href="{$product->getLink()}">{$product->description_short|strip_tags|truncate:60:'...'}</a>
                                </div>
                                {* <a class="lnk_more" href="{$product->getLink()}" title="{l s='View'}">{l s='View'}</a>*}

                                <div class="comparison_product_infos">
                                    <div class="prices_container">
                                        {if isset($product->show_price) && $product->show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
                                            <p class="price_container"><span
                                                        class="price">{convertPrice price=$product->getPrice($taxes_behavior)}</span>
                                            </p>
                                            <div class="product_discount">
                                                {if $product->on_sale}
                                                    <span class="on_sale">{l s='On sale!'}</span>
                                                {elseif $product->specificPrice AND $product->specificPrice.reduction}
                                                    <span class="discount">{l s='Reduced price!'}</span>
                                                {/if}
                                            </div>
                                            {if !empty($product->unity) && $product->unit_price_ratio > 0.000000}
                                                {math equation="pprice / punit_price"  pprice=$product->getPrice($taxes_behavior)  punit_price=$product->unit_price_ratio assign=unit_price}
                                                <p class="comparison_unit_price">{convertPrice price=$unit_price} {l s='per %d' sprintf=$product->unity|escape:'htmlall':'UTF-8'}</p>
                                            {else}
                                                &nbsp;
                                            {/if}
                                        {/if}
                                    </div>
                                    <!-- availability -->
                                    <p class="comparison_availability_statut">
                                        {if !(($product->quantity <= 0 && !$product->available_later) OR ($product->quantity != 0 && !$product->available_now) OR !$product->available_for_order OR $PS_CATALOG_MODE)}
                                            <span id="availability_label">{l s='Availability:'}</span>
                                            <span id="availability_value"{if $product->quantity <= 0} class="warning-inline"{/if}>
									{if $product->quantity <= 0}
                                        {if $allow_oosp}
                                            {$product->available_later|escape:'htmlall':'UTF-8'}
                                        {else}
                                            {l s='This product is no longer in stock'}
                                        {/if}
                                    {else}
                                        {$product->available_now|escape:'htmlall':'UTF-8'}
                                    {/if}
								</span>
                                        {/if}
                                    </p>
                                    {*<form type="post" action="{$link->getPageLink('products-comparison', true)}">*}
                                    {*<input type="hidden" name="form" value="1">*}
                                    {*<input type="hidden" name="action" value="remove">*}
                                    {*<input type="hidden" name="id_product" value="{$product->id}">*}
                                    {*<input class="cmp_remove btn btn-danger" type="submit" value="{l s='Remove'}">*}
                                    {*</form>*}
                                    <a class="cmp_remove btn btn-danger"
                                       href="{$link->getPageLink('products-comparison', true)}?form=1&action=remove&id_product={$product->id}"
                                       rel="ajax_id_product_{$product->id}">{l s='Remove'}</a>
                                    {if (!$product->hasAttributes() OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $product->minimal_quantity == 1 AND $product->customizable != 2 AND !$PS_CATALOG_MODE}
                                        {if ($product->quantity > 0 OR $product->allow_oosp)}
                                            <a class="exclusive ajax_add_to_cart_button"
                                               rel="ajax_id_product_{$product->id}"
                                               href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$product->id}&amp;token={$static_token}&amp;add")}"
                                               title="{l s='Add to cart'}"><span
                                                        class="icon-addcart"></span>{l s='Add to cart'}</a>
                                        {else}
                                            <span class="exclusive">{l s='Add to cart'}</span>
                                        {/if}
                                    {else}
                                        <div style="height:23px;"></div>
                                    {/if}
                                </div>
                            </div>
                            <div class="back-top-color">
                                <div class="our-copmare">
                                    <span
                                            style="font-size: 16px;">{$product->TopFeature[0]['name']}</span></span>
                                    : {$product->TopFeature[0]['value']}</span></div>
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[1]['name']}</span></span>
                                    : {$product->TopFeature[1]['value']}</span></div>
                                <div class="our-copmare"><span
                                style="font-size: 16px;">{$product->TopFeature[2]['name']}</span></span>
                                : {$product->TopFeature[2]['value']}</span></div>
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[3]['name']}</span></span>
                                    : {$product->TopFeature[3]['value_num_1']}{$product->TopFeature[3]['value']}</span></div>
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[4]['name']}</span></span>
                                    : {$product->TopFeature[4]['value_num_1']}
                                    {$product->TopFeature[4]['value']}</span></div>
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[5]['name']}</span></span>
                                    : {$product->TopFeature[5]['value_num_1']}
                                    {$product->TopFeature[5]['value']}</span></div>
                            </div>
                        </div>
                    </td>
                {/foreach}
                </tr>
                <tr class="comparison_header">
                    <td>
                        {l s='Features'}
                    </td>
                    {section loop=$products|count step=1 start=0 name=td}
                        <td></td>
                    {/section}
                </tr>

                {if $ordered_features}
                    {foreach from=$ordered_features item=feature}
                        <tr>
                            {cycle values='comparison_feature_odd,comparison_feature_even' assign='classname'}
                            <td class="{$classname}">
                                <strong>{$feature.name|escape:'htmlall':'UTF-8'}</strong>
                            </td>

                            {foreach from=$products item=product name=for_products}
                                {assign var='product_id' value=$product->id}
                                {assign var='feature_id' value=$feature.id_feature}
                        {*        <div>
                                    {$product_features|print_r}
                                </div>
                                <hr>*}
                                {if isset($product_features[$product_id])}
                                    {assign var='tab' value=$product_features[$product_id]}
                                    <td width="{$width}%"
                                        class="{$classname} comparison_infos">{$tab[$feature_id]|escape:'htmlall':'UTF-8'}</td>
                                {else}
                                    <td width="{$width}%" class="{$classname} comparison_infos"></td>
                                {/if}
                            {/foreach}
                        </tr>
                    {/foreach}
                {else}
                    <tr>
                        <td></td>
                        <td colspan="{$products|@count + 1}">{l s='No features to compare'}</td>
                    </tr>
                {/if}

                {$HOOK_EXTRA_PRODUCT_COMPARISON}
            </table>
        </div>
    {else}
        <p class="warning">{l s='There are no products selected for comparison'}</p>
    {/if}
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var AjaxCompareProgress = '';
        $("#ProductItemName").keyup(function () {
            if (AjaxCompareProgress) {
                AjaxCompareProgress.abort();
            }
            var lengthWord = $(this).val().length;
            if (lengthWord > 2) {
                $('.compare-results').fadeIn();
                $(".compare-append").empty();
                $("#retrive-compare").show().css('display', 'block');
                AjaxCompareProgress = $.ajax({
                    url: '{$link->getPageLink('search', true)}',
                    data: {
                        ajaxSearch: '1',
                        id_lang: '1',
                        limit: 20,
                        q: $(this).val()
                    },
                    type: 'POST',
                    dataType: "json",
                    success: function (data) {
                        $("#retrive-compare").hide();
                        if (data.resultErrProduct != 'Not Find Product') {
                            for (var k in data.resultProduct) {
                                if (data.resultProduct[k]['idimg'] > 0) {
                                    var idimage = data.resultProduct[k]['idimg'];
                                    var ArrIdImage = idimage.split("");
                                    var CountArrIdImage = ArrIdImage.length;
                                    var Base_dir = '{$img_prod_dir}';
                                    var UrlImageId = Base_dir;
                                    for (var j = 0; j < CountArrIdImage; j++) {
                                        UrlImageId = UrlImageId + '/' + ArrIdImage[j];
                                    }
                                    UrlImageId = UrlImageId + '/' + idimage + '-medium_allinmart.jpg';
                                }
                                else {
                                    UrlImageId = '{$img_prod_dir}' + 'search-no.jpg';
                                }
                                var pName = data.resultProduct[k]['pname'];
                                pName = pName.substring(0, 40);
                                $('.compare-append').append(
                                        '<li class="Psearch-li">'
                                                + '<a href="index.php?controller=products-comparison&form=1&action=add&id_product='
                                                + data.resultProduct[k]['id_product']
                                                + '">'
                                                + '<img class="res-product-img" src="' + UrlImageId + '" >'
                                                + '<span style="margin-top: 30px;">'
                                                + pName
                                                + '</span>'
                                                + '</a>'
                                                + '</li>'
                                );
                            }
                        }
                        else {
                            $('.compare-append').html('محصولی وجود ندارد.');
                        }
                        $('.compare-append').mCustomScrollbar({
                            set_height: 300,
                            theme: 'dark'
                            /*scrollButtons:{
                             enable:true
                             }*/
                        });
                    }
                });
            }
        });
        /* $(document).on('click', '.add-cmp-from-search', function (event) {
         //   event.preventDefault();
         var idProductSearch = $(this).attr('data-idproduct');
         $.ajax({
         url: 'index.php?controller=products-comparison&ajax=1&action=add&id_product=' + idProductSearch,
         cache: false,
         success: function (data) {
         if(data == 0){
         alert('حداکثر 5 محصول قابل مقایسه هستند.');
         }
         else if (data == 102){
         alert('محصول اضافه شده با محصولات فعلی در مقایسه هم شاخه نیست.')
         }
         else{
         alert('محصول به مقایسه افزوده شد.صفحه مجددا بارگزاری خواهد شد.')
         location.reload();
         }
         },
         async: false
         });
         });*/
        $(document).click(function () {
            $('.compare-results').fadeOut();
        });
//        $('.compare-results').click(function(event){
//            event.stopPropagation();
//        });
    });
</script>
