$(document).ready(function() {

    $('.popover-link').click(function(event)
        {
            event.preventDefault();
        }
    );


    $(function() {
        var isVisible = false;

        var hideAllPopovers = function() {
            $('.popover-link').each(function() {
                $(this).popover('hide');
            });
        };

        $('*[data-content]').popover({
            title: 'نمایش سریع جزئیات',
            placement: 'top',
            html: true,
            trigger: 'manual'
        }).on('click', function(e) {
                // if any other popovers are visible, hide them
                if(isVisible) {
                    hideAllPopovers();
                }

                $(this).popover('show');

                // handle clicking on the popover itself
               /* $('.popover').off('click').on('click', function(e) {
                    e.stopPropagation(); // prevent event for bubbling up => will not get caught with document.onclick
                });*/

                isVisible = true;
                e.stopPropagation();
            });


        $(document).on('click', function(e) {
            hideAllPopovers();
            isVisible = false;
        });
    });

    /*
     $('a.iframe').each(function()
     {
     $(this).fancybox({
     titleShow     : false,
     width:    450,
     height:   550,
     autoDimensions: false,

     href: '{$base_dir}productajax.php?content_only=1&id_product='+$(this).attr('rel')
     });
     });
     */

    $('*[data-poload]').bind('click',function() {
        var e=$(this);
        e.unbind('click');
        $.get(e.data('poload'),function(d) {
            e.popover({content: d}).popover('show');
        });
    });

});