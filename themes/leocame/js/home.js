$(document).ready(function () {
    var revapi17;
    if ($('.fullwidthabanner').revolution == undefined)
        revslider_showDoubleJqueryError('.fullwidthabanner');
    else
        revapi17 = $('.fullwidthabanner').show().revolution(
            {
                dottedOverlay: "none",
                delay: 5000,
                startwidth: 1300,
                startheight: 500,
                hideThumbs: 0,
                thumbWidth: 200,
                thumbHeight: 200,
                thumbAmount: 3,
                navigationType: "none",
                navigationArrows: "solo",
                navigationStyle: "round-old",
                touchenabled: "on",
                onHoverStop: "on",
                navigationHAlign: "center",
                navigationVAlign: "bottom",
                navigationHOffset: 0,
                navigationVOffset: 90,
                soloArrowLeftHalign: "left",
                soloArrowLeftValign: "center",
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,
                soloArrowRightHalign: "right",
                soloArrowRightValign: "center",
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,
                shadow: 0,
                fullWidth: "on",
                fullScreen: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                forceFullWidth: "off",
                hideThumbsOnMobile: "on",
                hideBulletsOnMobile: "on",
                hideArrowsOnMobile: "on",
                hideThumbsUnderResolution: 0,
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ""
            });
    $('#thumb1').click(function () {
        revapi17.revshowslide(1);
    });
    $('#thumb2').click(function () {
        revapi17.revshowslide(2);
    });
    $('#thumb3').click(function () {
        revapi17.revshowslide(3);
    });
    $('#thumb4').click(function () {
        revapi17.revshowslide(4);
    });
    $('[class*="best-item"],[class*="feature-item"]').css('opacity', 0);
    $('.best-item1').waypoint(function () {
            $('.best-item1').animate({ opacity: 1, left: '0px' }, 1000);
            $('.feature-item1').animate({ opacity: 1, right: '0px' }, 1000);
        },
        {
            offset: '600'
        });
    $('.best-item2').waypoint(function () {
            $('.best-item2').animate({ opacity: 1, left: '0px' }, 1000);
            $('.feature-item2').animate({ opacity: 1, right: '0px' }, 1000);
        },
        {
            offset: '600'
        });
    $('.best-item3').waypoint(function () {
            $('.best-item3').animate({ opacity: 1, left: '0px' }, 1000);
            $('.feature-item3').animate({ opacity: 1, right: '0px' }, 1000);
        },
        {
            offset: '600'
        });
    $('.best-item4').waypoint(function () {
            $('.best-item4').animate({ opacity: 1, left: '0px' }, 1000);
            $('.feature-item4').animate({ opacity: 1, right: '0px' }, 1000);
        },
        {
            offset: '600'
        });
    if ( $(window).width() >= 1024 ) {
    setTimeout(function () {
        $('.new-products-li').trigger('click');
    }, 10);
    }

    $('.new-products-li').click(function () {
        var tabDiv = $(this).data('div');
        $('.best-seller-li').removeClass('best-seller-li-active');
        $('.most-visited-li').removeClass('most-visited-li-active');
        $('.popular-li').removeClass('popular-li-active');
        $(this).addClass('new-products-li-active');
        $('.new-loading').show();
        if ($('#new-products-div').html().length < 300) {
            $.ajax({
                url: baseDir + 'modules/specialproduct/ajax.php',
                data: {
                    gettpl: '1'
                },
                type: 'post',
                dataType: "json",
                success: function (data) {
                    $('#new-products-div').html(data.resulthtml);
                    carouselTabs(tabDiv);
                }
            });
        }
    });
    if ( $(window).width() < 1024 ) {
    $('#new-products-mobile').waypoint(function () {
            var tabDiv = $(this).attr('id');
            if ($('#new-products-mobile').html().length < 300)
            {
                $.ajax({
                    url: baseDir + 'modules/specialproduct/ajax.php',
                    data: {
                        gettpl: '1'
                    },
                    type: 'post',
                    dataType: "json",
                    success: function (data) {
                        $('#new-products-mobile').html(data.resulthtml);
                        carouselMobile(tabDiv);
                            $('.hide-device').css('display','none');
                            $('.hide-desktop').css('display','block');
                            $('.element-item').css('float', 'right');
                            $('#' + tabDiv).find('.tab-right-arrow, .tab-left-arrow').addClass('alwaysHide');
                    }
                });
            }
    },
        {
            offset: 1000

        });
} else {
        $('.hide-device').css('display','block');
        $('.hide-desktop').css('display','none');
        $('.element-item').css('float', 'left');
        $('.tab-right-arrow, .tab-left-arrow').removeClass('alwaysHide');
    }
    $('.best-seller-li').click(function () {
        var tabDiv = $(this).data('div');
        $('.new-products-li').removeClass('new-products-li-active');
        $('.most-visited-li').removeClass('most-visited-li-active');
        $('.popular-li').removeClass('popular-li-active');
        $(this).addClass('best-seller-li-active');
        $('.best-loading').show();
        if ($('#best-seller-div').html().length < 300) {
            $.ajax({
                url: baseDir + 'modules/bestsellerspecialproduct/ajax.php',
                data: {
                    gettpl: '1'
                },
                type: 'post',
                dataType: "json",
                success: function (data) {
                    $('#best-seller-div').html(data.resulthtml);
                    carouselTabs(tabDiv);
                }
            });
        }
    });

    $('.most-visited-li').click(function () {
        var tabDiv = $(this).data('div');
        $('.new-products-li').removeClass('new-products-li-active');
        $('.best-seller-li').removeClass('best-seller-li-active');
        $('.popular-li').removeClass('popular-li-active');
        $(this).addClass('most-visited-li-active');
        $('.most-loading').show();
        if ($('#most-visited-div').html().length < 300) {
            $.ajax({
                url: baseDir + 'modules/mostvisitedproduct/ajax.php',
                data: {
                    gettpl: '1'
                },
                type: 'post',
                dataType: "json",
                success: function (data) {
                    $('#most-visited-div').html(data.resulthtml);
                    carouselTabs(tabDiv);
                }
            });
        }
    });
    $('.popular-li').click(function () {
        var tabDiv = $(this).data('div');
        $('.new-products-li').removeClass('new-products-li-active');
        $('.best-seller-li').removeClass('best-seller-li-active');
        $('.most-visited-li').removeClass('most-visited-li-active');
        $(this).addClass('popular-li-active');
        $('.popular-loading').show();
        if ($('#popular-div').html().length < 300) {
            $.ajax({
                url: baseDir + 'modules/popularityspecialproduct/ajax.php',
                data: {
                    gettpl: '1'
                },
                type: 'post',
                dataType: "json",
                success: function (data) {
                    $('#popular-div').html(data.resulthtml);
                    carouselTabs(tabDiv);
                }
            });
        }
    });
    if ( $(window).width() < 1024 ) {
        $('#popular-mobile').waypoint(function () {
                var tabDiv = $(this).attr('id');
                if ($('#popular-mobile').html().length < 300) {
                    $.ajax({
                        url: baseDir + 'modules/popularityspecialproduct/ajax.php',
                        data: {
                            gettpl: '1'
                        },
                        type: 'post',
                        dataType: "json",
                        success: function (data) {
                            $('#popular-mobile').html(data.resulthtml);
                            carouselMobile(tabDiv);
                            $('.hide-device').css('display','none');
                            $('.hide-desktop').css('display','block');
                            $('.element-item').css('float', 'right');
                            $('#' + tabDiv).find('.tab-right-arrow, .tab-left-arrow').addClass('alwaysHide');
                        }
                    });
                }
            },
            {
                offset: 1000

            });
    }   else {
        $('.hide-device').css('display','block');
        $('.hide-desktop').css('display','none');
        $('.element-item').css('float', 'left');
        $('.tab-right-arrow, .tab-left-arrow').removeClass('alwaysHide');
    }
    function carouselTabs(tabDiv) {
        var right = $('#' + tabDiv).closest('.tab-pane').find('.tab-right-arrow');
        var left = $('#' + tabDiv).closest('.tab-pane').find('.tab-left-arrow');
        $('#' + tabDiv).find('.carousel-tabs').carouFredSel(
            {
                auto: false,
                next: right,
                prev: left,
                responsive: false,
                items: 6,
                //   width: 186,
                scroll: {
                    items: 1,
                    duration: 1000,
                    pauseOnHover: true
                }
            }
        );
    }

    function carouselMobile(tabDiv) {
        var right = $('#' + tabDiv).closest('.tab-pane').find('.tab-right-arrow');
        var left = $('#' + tabDiv).closest('.tab-pane').find('.tab-left-arrow');
        $('#' + tabDiv).find('.carousel-tabs').owlCarousel();
    }

    $('.home-tabs-li').each(function () {
        $(this).click(function () {
            $('.home-tabs-li').not(this).removeClass('flesh');
            $(this).addClass('flesh');
        });
    });
    $('#myTab li').click(function (e) {
        e.preventDefault();
        $(this).find('a').tab('show');
    });
    $('.carousel').carouFredSel({
        scroll: {
            items: 1,
            easing: "elastic",
            duration: 300,
            pauseOnHover: true
        }
    });
    /*var $container = $('#container');
     $container.isotope({
     itemSelector: '.element-item',
     layoutMode: 'fitRows'
     });
     $('#filters').on('click', 'a', function (event) {
     event.preventDefault();
     var filtr = $(this).attr('data-filter');
     $container.isotope({ filter: filtr });
     $(this).addClass('.active-isotope');
     });*/
    $(".isotope-hover").each(function () {
        $(this).hover(
            function () {
                $(this).find(".main-text").addClass("blue");
                $(this).find('span').css('color', '#ffffff');
            },
            function () {
                $(".main-text").removeClass("blue ");
                $(this).find('.new-model').css('color', '#36AAD8');
                $(this).find('.new-price').css('color', '#9A9A9A');
            });
    });
    var footertop = $('#footer').offset().top;
    var newsTop = $('#news-back').offset().top;
    $(window).scroll(function () {
        var giftop = $('.gif').offset().top;
        if (giftop > footertop) {
            $('.gif').hide();
        }
        if (giftop < newsTop) {
            $('.gif').show();
        }
    });


    $(window).resize(function () {
        if ($(window).width() < 1024) {
            $('.hide-device').hide();
            $('.hide-desktop').show();
            $('.element-item').css('float', 'right');
            $('.tab-right-arrow').addClass('alwaysHide');
        }
        else {
            $('.hide-device').show();
            $('.hide-desktop').hide();
            $('.element-item').css('float', 'left');
            $('.tab-right-arrow').removeClass('alwaysHide');
        }
    });
});