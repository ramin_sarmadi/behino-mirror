$(document).ready(function () {
    $("#productsview a.btn").click(function () {
        if ($(this).attr("rel") == "view-grid") {
            $("#product_list").addClass("view-grid").removeClass("view-list");
        } else {
            $("#product_list").addClass("view-list").removeClass("view-grid");
        }
        return false;
    });
    $('#grid_button_id').bind('click', function () {
        $("#product_list").removeClass("list_view");
        $("#grid_button_id").addClass("active");
        $("#list_button_id").removeClass("active");
    });
    $('#list_button_id').bind('click', function () {
        $("#product_list").addClass("list_view");
        $("#grid_button_id").removeClass("active");
        $("#list_button_id").addClass("active");
    });
    $('.leo-search-button').click(function () {
        $('#searchbox').slideToggle('slow');
    });
    $('#search_query_top').keyup(function () {
        checkInput($(this));
    });
    var menu = $('#main-nav-ul').clone();
//    var search = $('#search_block_top').clone();
//    var cart = $('#topminibasket').clone();
    $(window).scroll(function () {
        if ($(window).scrollTop() > 135) {
            $('#phantom').css('opacity', 1);
            $('#phantom').css('visibility', 'visible');
            $('.menu-box').append(menu);
//            $('.search-box').append(search);
            /*    $('.cart-menu').append(cart);*/
        }
        else {
            $('#phantom').css('opacity', 0);
            $('#phantom').css('visibility', 'hidden');
        }
    });
    $(document).on('mouseover', '.nav-li', function () {
        $(this).find('.sub-nav').css('opacity', 1);
        $(this).find('.sub-nav').css('visibility', 'visible');
    });
    $(document).on('mouseout', '.nav-li', function () {
        $(this).find('.sub-nav').css('opacity', 0);
        $(this).find('.sub-nav').css('visibility', 'hidden');
    });
    $('body').on('click', '.quick-icon', function () {
        var th = $(this);
        var posfromright = ($(window).width() - (th.offset().left + th.outerWidth()));
        var quickwidth = th.find('#quick-box').width();
        if (posfromright < quickwidth) {
            th.find('#quick-box').removeClass('quick-content');
            th.find('#quick-box').addClass('quick-content2');
        }
        else {
        }
        var divshow = th.find('#quick-box');
        $('html, body').animate({
            scrollTop: $(th).offset().top - 300
        });
        divshow.show();
        $('.quick-loading').css('display', 'block');
        var idProduct = th.attr('data-idProduct');
        $.ajax({
            url: baseDir + 'index.php?controller=ProductAjax?content_only=1&id_product=' + idProduct + '&action=quickview',
            dataType: "json",
            success: function (data) {
                divshow.empty();
                divshow.append(
                    '<img class="quick-img" src="'
                        + data.resultsProduct[0]['link_lmg']
                        + '">'
                );
                for (var k in data.resultsFeature) {
                    if (data.resultsFeature[k]['value_num_1'] != 0) {
                        var v_n_1 = data.resultsFeature[k]['value_num_1']
                    } else {
                        v_n_1 = '';
                    }
                    divshow.append(
                        '<p class="quick-p"><label class="feature-name-quick">'
                            + data.resultsFeature[k]['name']
                            + ':  </label>'
                            + '<label class="feature-value">'
                            + v_n_1
                            + data.resultsFeature[k]['value']
                            + '</label></p>'
                    );
                }
            }
        });
        $('.quick-content, .quick-content2').not(divshow).hide();
    });
    $('body').click(function () {
        $('.quick-content, .quick-content2').fadeOut();
    });
    $('body').on('click', '.quick-icon', function (event) {
        event.stopPropagation();
    });
});
//Detail-product
// Change the current product images regarding the combination selected
function refreshProductImages(id_product_attribute) {
    $('#thumbs_list_frame').scrollTo('li:eq(0)', 700, {axis: 'x'});
    $('#thumbs_list li').hide();
    id_product_attribute = parseInt(id_product_attribute);
    if (typeof(combinationImages) != 'undefined' && typeof(combinationImages[id_product_attribute]) != 'undefined') {
        for (var i = 0; i < combinationImages[id_product_attribute].length; i++)
            $('#thumbnail_' + parseInt(combinationImages[id_product_attribute][i])).show();
    }
    if (i > 0) {
        var thumb_height = $('#thumbs_list_frame >li').height() + parseInt($('#thumbs_list_frame >li').css('marginTop'));
        $('#thumbs_list_frame').height((parseInt((thumb_height) * i) + 3) + 'px'); //  Bug IE6, needs 3 pixels more ?
    }
    else {
        $('#thumbnail_' + idDefaultImage).show();
        displayImage($('#thumbnail_' + idDefaultImage + ' a'));
    }
    $('#thumbs_list').trigger('goto', 0);
    serialScrollFixLock('', '', '', '', 0);// SerialScroll Bug on goto 0 ?
}
function checkPersian(firstChar) {
    if (typeof this.characters == 'undefined')
        this.characters = ['ا', 'ب', 'پ', 'ت', 'س', 'ج', 'چ', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'ژ', 'س', 'ش', 'ص', 'ض', 'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ک', 'گ', 'ل', 'م', 'ن', 'و', 'ه', 'ی', 'ي'];
    return this.characters.indexOf(firstChar) != -1;
}
function checkInput(selector) {
    selector.css('text-align', checkPersian(selector.val().substr(0, 1)) ? 'right' : 'left');
    selector.css('font-family', checkPersian(selector.val().substr(0, 1)) ? 'WYekan' : 'open sans');
    $(".submit_search").css('float', checkPersian(selector.val().substr(0, 1)) ? 'left' : 'right');
//    if (checkPersian(selector.val().substr(0, 1))) {
//        $(".submit_search").addClass('flip-horizontal');
//    }
//    else {
//        $(".submit_search").removeClass('flip-horizontal');
//    }
}
/*$('#search_query_top').change( checkInput );*/
//$('#search_query_top').keydown( checkInput );
//$('#search_query_top').keyup( checkInput );




