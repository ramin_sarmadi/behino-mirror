$(document).ready(function () {
    $("#select-brand-tablet").prop('disabled', true);
//    $("#select-sim-tablet").prop('disabled', true).multiselect('disable');
    $("#device_1").prop('checked', true);
    $("#device_1").click(function () {
        $("#select-brand-mobile").prop('disabled', false).multiselect('enable');
        $("#select-brand-tablet").prop('disabled', true).multiselect('disable');
        $("#select-sim-num").prop('disabled', false).multiselect('enable');
        $("#select-sim-tablet").prop('disabled', true).multiselect('disable');
        $("#device_24").prop('disabled', false);
        $(".tag").each(function () {
            var mobtabtext = $(this).find('span').text().trim();
            if (mobtabtext.contains('تبلت')) {
                $(this).remove();
            }
        });
    });
    $("#device_24").click(function () {
        $("#select-brand-mobile").prop('disabled', true).multiselect('disable');
        $("#select-brand-tablet").prop('disabled', false).multiselect('enable');
        $("#select-sim-num").prop('disabled', true).multiselect('disable');
        $("#select-sim-tablet").prop('disabled', false).multiselect('enable');
        $("#device_1").prop('disabled', false);
        $(".tag").each(function () {
            var mobtabtext = $(this).find('span').text().trim();
            if (mobtabtext.contains('موبایل')) {
                $(this).remove();
            }
        });
    });
    $(".select-cls").each(function () {
        multipleselect(this, $(this).parent().children('.combo-name').html())
    });
    $(".select-single").each(function () {
        singleselect(this, $(this).parent().children('.combo-name').html())
    });
    $("#mobile-brand li").each(function () {
        $(this).change(function () {
            if ($(this).find("input").prop('checked')) {
                $("#device_1").prop('disabled', true);
            }
        });
    });
    $("#tablet-brand li").each(function () {
        $(this).change(function () {
            if ($(this).find("input").prop('checked')) {
                $("#device_24").prop('disabled', true);
            }
        });
    });
    function multipleselect(element, titlename) {
        $(element).multiselect({
            buttonClass: 'btn btn-default',
            numberDisplayed: 0,
            buttonWidth: '230px',
            maxHeight: 200,
            nonSelectedText: titlename,
            nSelectedText: 'مورد',
            buttonText: function (options, select) {
                if (options.length == 0) {
                    return this.nonSelectedText + ' <b class="caret"></b>';
                }
                return this.nonSelectedText + ': ' + options.length + ' ' + this.nSelectedText + ' <b class="caret"></b>';
            }
        });
    }

    function singleselect(element, titlename) {
        $(element).multiselect({
            buttonClass: 'btn btn-default',
            numberDisplayed: 0,
            buttonWidth: '230px',
            maxHeight: 200,
            nonSelectedText: titlename,
            nSelectedText: 'مورد',
            buttonText: function (options, select) {
                if (options.length == 0) {
                    return this.nonSelectedText + ' <b class="caret"></b>';
                }
                else {
                    var selected = '';
                    options.each(function () {
                        var label = ($(this).attr('label') !== undefined) ? $(this).attr('label') : $(this).html();
                        selected = label;
                    });
                }
                return this.nonSelectedText + ': ' + selected + ' <b class="caret"></b>';
            }
        });
    }

    $('.search-items li').each(function () {
        $(this).on('change', function (e) {
            if ($(this).find("input").prop('checked')) {
                var tagname = $(this).find("label").text();
                var valueinput = $(this).find("input").val();
                $(".tag-box-inner").append(
                    '<li class="tag" data-name="' +
                        valueinput
                        + '">'
                        + '<span>'
                        + tagname
                        + '</span>'
                        + '<a href="#" class="remove-tag">   *</a>'
                        + '</li>'
                );
            }
            else {
                var valueinput = $(this).find("input").val();
                var tagtoremove = $(".tag-box-inner").find('.tag[data-name=' + valueinput + ']');
                tagtoremove.remove();
            }
        });
    });
    $('.search-items-2 li').each(function () {
        $(this).on('change', function (e) {
            var id1 = $(this).find("label").text().trim();
            var id2 = 'دارد';
            var id3 = 'دارد با قابلیت تماس';
            if (id1 == id2 || id1 == id3) {
                var tagname = $(this).parents().eq(2).find("span").text();
                var valueinput = $(this).find("input").val();
                $(".tag-box-inner").append(
                    '<li class="tag" data-name="' +
                        valueinput
                        + '">'
                        + '<span>'
                        + tagname
                        + '</span>'
                        + '<a href="#" class="remove-tag-2">   *</a>'
                        + '</li>'
                );
            }
            var id3 = 'مهم نیست';
            var id4 = 'ندارد';
            if (id1 == id3 || id1 == id4) {
                var multispantext = $(this).parents().eq(2).find("span").text();
                $(".tag").find('span').each(function () {
                    var tagtext = $(this).text();
                    if (tagtext == multispantext) {
                        $(this).closest('li').remove();
                    }
                });
            }
        });
    });
    $(document).on("click", ".remove-tag", function (e) {
        e.preventDefault();
        var tagid = $(this).parent().attr('data-name');
        $('.select-cls').multiselect('deselect', tagid);
        var selector = "input[value='" + tagid + "']";
        var selectorel = $(selector);
        $(this).parent().remove();
    });
    $(document).on("click", ".remove-tag-2", function (e) {
        e.preventDefault();
        var tagspanname = $(this).parent().find('span').text().trim();
        $(".search-items-2").each(function () {
            var singlename = $(this).find('span').text().trim();
            if (singlename == tagspanname) {
                var arrayValues = $(this).find('option').first().val();
                var arrayValues2 = $(this).find('option:nth-child(2)').val();
                $('.select-single').multiselect('deselect', arrayValues2);
                $('.select-single').multiselect('select', arrayValues);
            }
        });
        var tagid = $(this).parent().attr('data-name');
        var selector = "input[value='" + tagid + "']";
        var selectorel = $(selector);
        $(this).parent().remove();
    });
//    $("#reset-form").click(function (event) {
//        event.preventDefault();
//        var form = $("#search-ajax-form").get(0);
//        form.reset();
//        $('option').each(function(element) {
//            $(this).multiselect('deselect', $(this).val());
//        });
//    });
    /*** Ajax ***/
    $("#more").hide();
    $('#search-ajax-form').on('submit', function (event) {
        event.preventDefault();
        $("#pagination").val(1);
        $('#results').show();
        $('#results').css('opacity', 0.5);
        $(".ajax-loading").show();
        $("#no-result").hide();
        $.ajax({
            url: baseDir + 'index.php?controller=searchajax&go=true',
            data: $('#search-ajax-form').serialize(),
            type: 'post',
            dataType: "json",
            success: function (data) {
                if (data.resultshtml != null) {
                    $(".ajax-loading").hide();
                    $("#results-count-div").show();
                    $('#results').html(data.resultshtml);
                    $('#results-count').html(data.countresult);
                    $('#results').css('opacity', 1);
                    if (12 * $("#pagination").val() < data.countresult) {
                        $("#more").show().css('display', 'block');
                    }
                    $('html, body').animate({
                        scrollTop: $("#results-count-div").offset().top - 100
                    }, 1000);
                }
                else {
                    $(".ajax-loading").hide();
                    $("#no-result").show();
                    $('#results').hide();
                    $("#results-count-div").hide();
                    $("#more").hide();
                }
            },
            error: function () {
                $(".ajax-loading").hide();
                $('#results').hide();
                $("server-error").show();
                $("#more").show().css('display', 'none');
            }

        });
    });
    $('#more').click(function () {
        var thisMore= $(this);
        var page = parseInt($('#pagination').val());
        page++;
        $('#pagination').val(page);
        $(".ajax-loading-more").show();
        $.ajax({
            url: baseDir + 'index.php?controller=searchajax&go=true',
            data: $('#search-ajax-form').serialize(),
            type: 'post',
            dataType: "json",
            success: function (data) {
                $(".ajax-loading-more").hide();
                if (data) {
                    $('#results').append(
                        '<div class="line"></div> '
                    );
                    $("#more").hide()
                    $('#results').append(data.resultshtml);
                    if (12 * $("#pagination").val() < data.countresult) {
                        $("#more").show().css('display', 'block');
                    }

                }
                else {
                    $("#no-result").show();
                }
            },
            error: function () {
                $("server-error").show();
            }
        });
    });
    $(".input-reso").each(function () {
        $(this).keypress(function () {
            $("#reso-check").prop('checked', true);
        });
    });
    $(".range-div").each(function () {
        var th = $(this);
        var slider = th.find('.value-slider');
        var minVal = th.find('.min-val');
        var maxVal = th.find('.max-val');
        var MinStart = Math.round(minVal.attr('data-min'));
        var MaxStart = Math.round(maxVal.attr('data-max'));
        slider.noUiSlider({
            range: [MinStart, MaxStart],
            start: [MinStart, MaxStart],
            connect: true,
            behaviour: 'tap-drag',
            handles: 2,
            step: 1
        });
        var rangeonevalue = slider.val();
        minVal.text(Math.round(rangeonevalue[0]));
        maxVal.text(Math.round(rangeonevalue[1]));
        var minValHidden = $(this).find('.min_val_hidden');
        var maxValHidden = $(this).find('.max_val_hidden');
        minValHidden.val(rangeonevalue[0]);
        maxValHidden.val(rangeonevalue[1]);
        th.find('.min_val_hidden').attr('disabled', 'disabled');
        th.find('.max_val_hidden').attr('disabled', 'disabled');
        slider.change(function () {
            var rangeonevalue = slider.val();
            var minVal = th.find('.min-val');
            var maxVal = th.find('.max-val');
            minVal.text(Math.round(rangeonevalue[0]));
            maxVal.text(Math.round(rangeonevalue[1]));
            var minValHidden = th.find('.min_val_hidden');
            var maxValHidden = th.find('.max_val_hidden');
            minValHidden.val(rangeonevalue[0]);
            maxValHidden.val(rangeonevalue[1]);
            if (rangeonevalue[0] != MinStart || rangeonevalue[1] != MaxStart) {
                th.find('.range-checkbox').attr('checked', true);
                th.find('.min_val_hidden').removeAttr("disabled");
                th.find('.max_val_hidden').removeAttr("disabled");
            }
            if (parseInt(rangeonevalue[0], 10) === parseInt(MinStart, 10) && parseInt(rangeonevalue[1], 10) === parseInt(MaxStart, 10)) {
                th.find('.range-checkbox').attr('checked', false);
                th.find('.min_val_hidden').attr('disabled', 'disabled');
                th.find('.max_val_hidden').attr('disabled', 'disabled');
            }
        });
    });
});