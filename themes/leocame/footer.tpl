{if !$content_only}
    {include file="$tpl_dir./layout/{$LEO_LAYOUT_DIRECTION}/footer.tpl"  }
    </div>
{*for layerd slider problem*}
    {if $page_name != 'index'}
        </div>
    {/if}

    </section>

    <!-- Footer -->
    <footer id="footer" class="omega clearfix">
        <section class="footer">
            <div class="container">
                <div class="row-fluid">

                    <section class="foorer-box span3">
                        {$HOOK_FOOTER}
                    </section>
                    <section class="foorer-box span3">
                        <h3 class="footer-h3">{l s='راهنمای مشتریان'}</h3>

                        <div class="footer-links-container">
                            <ul class="footer-ul" id="menu-shortcodes">
                                <li class="footer-item">
                                    <a href="{$base_dir}content/16-saman-insurance">{l s='خرید با بیمه'}</a>
                                </li>
                                <li class="footer-item">
                                    <a href="{$base_dir}content/11-order-guide">{l s='راهنمای خرید'}</a>
                                </li>
                                <li class="footer-item">
                                    <a href="{$base_dir}content/15-coupon-guide">{l s='راهنمای استفاده از کوپن(بن)'}</a>
                                </li>
                                <li class="footer-item">
                                    <a href="{$base_dir}content/13-advanced-search-guide">{l s='راهنمای جستجوی پیشرفته'}</a>
                                </li>
                                <li class="footer-item">
                                <a href="{$base_dir}content/14-product-groups-guide">{l s='راهنمای صفحه محصولات برند / شاخه محصول'}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearboth"></div>
                    </section>

                    <section class="foorer-box foorer-first-box span3">
                        <h3 class="footer-h3">درباره ما</h3>

                        <div class="footer-links-container">
                            <ul class="footer-ul" id="menu-shortcodes">
                                <li class="footer-item">
                                    <a href="{$base_dir}content/4-aboutBehino">درباره ما</a>
                                </li>
                                <li class="footer-item">
                                    <a href="{$base_dir}content/2-PrivacyPolicy">سیاست حفظ حریم شخصی</a>
                                </li>

                                <li class="footer-item">
                                    <a href="{$base_dir}content/3-TermsOfServices">{l s='شرایط و قوانین'}</a>
                                </li>
                                <li class="footer-item footer-last">
                                    <a href="{$base_dir}index.php?controller=contact">{l s='تماس با ما'}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearboth"></div>
                    </section>
                    <section class="foorer-box span3">
                        <h3 class="footer-h3">حساب کاربری</h3>

                        <div class="footer-links-container">
                            <ul class="footer-ul" id="menu-shortcodes">
                                <li class="footer-item">
                                    <a href="{$base_dir}index.php?controller=my-account"
                                       class="{$base_dir}">{l s='حساب کاربری شما'}</a>
                                </li>
                                <li class="footer-item">
                                    <a href="{$base_dir}index.php?controller=history"
                                       class="{$base_dir}">{l s='سفارشات شما'}</a>
                                </li>
                                <li class="footer-item">
                                    <a href="{$base_dir}index.php?controller=identity"
                                       class="{$base_dir}">{l s='اطلاعات شخصی شما'}</a>
                                </li>
                                <li class="footer-item">
                                    <a href="{$base_dir}index.php?fc=module&module=favoriteproducts&controller=account"
                                       class="{$base_dir}">{l s='محصولات مورد علاقه شما'}</a>
                                </li>
                               {* <li class="footer-item footer-last">
                                    <a href="{$base_dir}index.php?fc=module&module=favoriteproducts&controller=account"
                                       class="{$base_dir}">{l s='بن های شما'}</a>
                                </li>*}
                            </ul>
                        </div>
                        <div class="clearboth"></div>
                    </section>

                    {*{if $PS_ALLOW_MOBILE_DEVICE}
                        <p class="center clearBoth hidden-desktop"><a
                                    href="{$link->getPageLink('index', true)}?mobile_theme_ok">{l s='Browse the mobile site'}</a>
                        </p>
                    {/if}*}
                </div>
            </div>

            {*</div>*}
        </section>
        <section id="footer-bottom">
            {*<div class="container">*}
            <div class="row-fluid">
                <div class="span4">
                </div>
                <div class="span3">
                    <div class="copyright">
                        <span>{l s='کلیه حقوق سایت برای بهینو محفوظ است'}</span>
                    </div>
                </div>
                {if $HOOK_FOOTNAV}
                    <div class="span5">
                        <div class="footnav">{$HOOK_FOOTNAV}</div>
                    </div>
                {/if}
            </div>
            {*</div>*}
        </section>
    </footer>
    </div>
{/if}
{if $LEO_PANELTOOL}
    {include file="$tpl_dir./info/paneltool.tpl"}
{/if}
{*<script type="text/javascript">*}
{*var classBody = "{$LEO_PATTERN}";*}
{*$("body").addClass( classBody.replace(/\.\w+$/,"")  );*}
{*</script>*}
<div id="phantom">
    <div class="ph-wrap with-logo">
        <div class="ph-wrap-content">
            <div class="container">
                <div class="row-fluid">
                    <div class="ph-wrap-inner">
                        <div class="logo-box span1">
                            <a href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
                                <img src="{$img_dir}logo-behino.png" alt="{$shop_name|escape:'htmlall':'UTF-8'}"/>
                            </a>
                        </div>
                        <div class="search-box span5 offset1">
                            <div class="nav-item block-search-top">
                                <div id="search_block_top" class="search-class">
                                    <form id="searchbox" action="http://localhost/ibartar2/index.php?controller=search"
                                          method="get">
                                        <label for="search_query_top"><!-- image on background --></label>
                                        <input type="hidden" value="search" name="controller">
                                        <input type="hidden" value="position" name="orderby">
                                        <input type="hidden" value="desc" name="orderway">
                                        <input type="text" autocomplete="off" value="" name="search_query"
                                               id="search_query_top" class="search_query">
                                        <input type="submit" class="button" value="جستجو" name="submit_search">
                                    </form>
                                </div>
                                <div style="display: none;top: 34px;" class="search-results">
                                    <div class="search-products">
                                        <h3 class="search-h3">محصولات</h3>
                                        <ul class="products-append">
                                        </ul>
                                    </div>
                                    <div class="search-video">
                                        <h3 class="search-h3">ویدئو ها</h3>
                                        <ul class="video-append">
                                        </ul>
                                        <h3 class="search-h3">اخبار</h3>
                                        <ul class="article-append">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="menu-box span4 offset1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{literal}
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47616653-2', 'behino.com');
        ga('send', 'pageview');

    </script>
{/literal}
</body>
</html>
