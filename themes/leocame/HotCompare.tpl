<style>
    .our-copmare {
        margin: 5px 0;
        color: #ffffff;
    }
    .title-compare {
        font-size: 26px !important;
        line-height: 12px;
        margin: 20px 0;
        text-align: center;
    }
    .description-compare {
        font-size: 16px;
    }
    .back-color {
        background-color: #eaeaea !important;
    }
    .back-top-color {
        background-color: rgba(42, 131, 237, 0.75);
        padding: 5px;
        margin-bottom: 10px;
    }
    .new-product-title1 {
        color: #373A41 !important;
        font-size: 36px;
        font-weight: normal;
        letter-spacing: 1px;
        margin: 46px 0 22px;
        text-align: center;
        word-spacing: 5px;
    }
    .border-title-main {
        border-bottom: 1px solid #F0EFEF;
        color: #35AAD8;
        display: block;
        margin-bottom: 22px;
        margin-left: auto !important;
        margin-right: auto !important;
        padding-bottom: 10px;
        width: 40%;
    }
    .border-title-main span {
        background: none repeat scroll 0 0 #35AAD8;
        display: block;
        height: 1px;
        margin-bottom: -11px;
        margin-left: auto !important;
        margin-right: auto !important;
        margin-top: 28px;
        width: 140px;
    }
</style>
<h1 class="title_category"></h1>
<div class="wrapper-comparison ">
    <h1 class="new-product-title1 border-title-main title-compare"> {l s='Name'}: {$result_hotcompare[0]['name']}
        <span></span>
    </h1>
        <div class="description-compare">
            {l s='Description'}: {$result_hotcompare[0]['description']}


        </div>

    {if $hasProduct}
        <div class="products_block">
            <table id="product_comparison">
                <td width="20%" class="td_empty">
                </td>
                {assign var='taxes_behavior' value=false}
                {if $use_taxes && (!$priceDisplay  || $priceDisplay == 2)}
                    {assign var='taxes_behavior' value=true}
                {/if}
                {foreach from=$products item=product name=for_products}

                    {assign var='replace_id' value=$product->id|cat:'|'}
                    <td width="{$width}%" class="ajax_block_product product_block comparison_infos">
                        <div class="product-container clearfix">
                            {* <div>{$product->TopFeature|print_r}</div>*}
                            <div class="center_block">
                                <a href="{$product->getLink()}" title="{$product->name|escape:html:'UTF-8'}"
                                   class="product_image">
                                    <img src="{$link->getImageLink($product->link_rewrite, $product->id_image, 'compare-allinmart')}"
                                         alt="{$product->name|escape:html:'UTF-8'}"/>
                                </a>
                            </div>
                            <div class="right_block clearfix back-color">
                                <h3 class="s_title_block"><a href="{$product->getLink()}"
                                                             title="{$product->name|truncate:32:'...'|escape:'htmlall':'UTF-8'}">{$product->name|truncate:27:'...'|escape:'htmlall':'UTF-8'}</a>
                                </h3>

                                <div class="product_desc"><a
                                            href="{$product->getLink()}">{$product->description_short|strip_tags|truncate:60:'...'}</a>
                                </div>
                                {* <a class="lnk_more" href="{$product->getLink()}" title="{l s='View'}">{l s='View'}</a>*}

                                <div class="comparison_product_infos">
                                    <div class="prices_container">
                                        {if isset($product->show_price) && $product->show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
                                            <p class="price_container"><span
                                                        class="price">{convertPrice price=$product->getPrice($taxes_behavior)}</span>
                                            </p>
                                            <div class="product_discount">
                                                {if $product->on_sale}
                                                    <span class="on_sale">{l s='On sale!'}</span>
                                                {elseif $product->specificPrice AND $product->specificPrice.reduction}
                                                    <span class="discount">{l s='Reduced price!'}</span>
                                                {/if}
                                            </div>
                                            {if !empty($product->unity) && $product->unit_price_ratio > 0.000000}
                                                {math equation="pprice / punit_price"  pprice=$product->getPrice($taxes_behavior)  punit_price=$product->unit_price_ratio assign=unit_price}
                                                <p class="comparison_unit_price">{convertPrice price=$unit_price} {l s='per %d' sprintf=$product->unity|escape:'htmlall':'UTF-8'}</p>
                                            {else}
                                                &nbsp;
                                            {/if}
                                        {/if}
                                    </div>
                                    <!-- availability -->
                                    <p class="comparison_availability_statut">
                                        {if !(($product->quantity <= 0 && !$product->available_later) OR ($product->quantity != 0 && !$product->available_now) OR !$product->available_for_order OR $PS_CATALOG_MODE)}
                                            <span id="availability_label">{l s='Availability:'}</span>
                                            <span id="availability_value"{if $product->quantity <= 0} class="warning-inline"{/if}>
									{if $product->quantity <= 0}
                                        {if $allow_oosp}
                                            {$product->available_later|escape:'htmlall':'UTF-8'}
                                        {else}
                                            {l s='This product is no longer in stock'}
                                        {/if}
                                    {else}
                                        {$product->available_now|escape:'htmlall':'UTF-8'}
                                    {/if}
								</span>
                                        {/if}
                                    </p>

                                </div>
                            </div>
                            <div class="back-top-color">
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[0]['name']}</span></span>
                                    : {$product->TopFeature[0]['value']}</span></div>
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[1]['name']}</span></span>
                                    : {$product->TopFeature[1]['value']}</span></div>
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[2]['name']}</span></span>
                                    : {$product->TopFeature[2]['value']}</span></div>
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[3]['name']}</span></span>
                                    : {$product->TopFeature[3]['value_num_1']} / {$product->TopFeature[3]['value']}</span></div>
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[4]['name']}</span></span>
                                    : {$product->TopFeature[4]['value_num_1']} / {$product->TopFeature[4]['value']}</span></div>
                                <div class="our-copmare"><span
                                            style="font-size: 16px;">{$product->TopFeature[5]['name']}</span></span>
                                    : {$product->TopFeature[5]['value_num_1']} / {$product->TopFeature[5]['value']}</span></div>
                            </div>
                        </div>
                    </td>
                {/foreach}
                </tr>
                <tr class="comparison_header">
                    <td>
                        {l s='Features'}
                    </td>
                    {section loop=$products|count step=1 start=0 name=td}
                        <td></td>
                    {/section}
                </tr>

                {if $ordered_features}
                    {foreach from=$ordered_features item=feature}
                        <tr>
                            {cycle values='comparison_feature_odd,comparison_feature_even' assign='classname'}
                            <td class="{$classname}">
                                <strong>{$feature.name|escape:'htmlall':'UTF-8'}</strong>
                            </td>

                            {foreach from=$products item=product name=for_products}
                                {assign var='product_id' value=$product->id}
                                {assign var='feature_id' value=$feature.id_feature}
                                {if isset($product_features[$product_id])}
                                    {assign var='tab' value=$product_features[$product_id]}
                                    <td width="{$width}%"
                                        class="{$classname} comparison_infos">{$tab[$feature_id]|escape:'htmlall':'UTF-8'}</td>
                                {else}
                                    <td width="{$width}%" class="{$classname} comparison_infos"></td>
                                {/if}
                            {/foreach}
                        </tr>
                    {/foreach}
                {else}
                    <tr>
                        <td></td>
                        <td colspan="{$products|@count + 1}">{l s='No features to compare'}</td>
                    </tr>
                {/if}

                {$HOOK_EXTRA_PRODUCT_COMPARISON}
            </table>
        </div>
    {else}
        <p class="warning">{l s='There are no products selected for comparison'}</p>
    {/if}
</div>
