<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockwishlist}leocame>blockwishlist-ajax_4a84e5921e203aede886d04fc41a414b'] = 'این محصول را از لیست علاقمندیهای من حذف کن';
$_MODULE['<{blockwishlist}leocame>blockwishlist-ajax_f2a6c498fb90ee345d997f888fce3b18'] = 'حذف';
$_MODULE['<{blockwishlist}leocame>blockwishlist-ajax_4b7d496eedb665d0b5f589f2f874e7cb'] = 'جزییات محصول';
$_MODULE['<{blockwishlist}leocame>blockwishlist-ajax_d037160cfb1fa5520563302d3a32630a'] = 'قبل از افزودن محصولات، شما می بایست یک لیست علاقمندی ایجاد کنید';
$_MODULE['<{blockwishlist}leocame>blockwishlist-ajax_09dc02ecbb078868a3a86dded030076d'] = 'بدون محصول';
$_MODULE['<{blockwishlist}leocame>blockwishlist-extra_15b94c64c4d5a4f7172e5347a36b94fd'] = 'افزودن به علاقمندی ها';
$_MODULE['<{blockwishlist}leocame>blockwishlist_2715a65604e1af3d6933b61704865daf'] = 'بخش لیست علاقمندیها';
$_MODULE['<{blockwishlist}leocame>blockwishlist_7244141a5de030c4c882556f4fd70a8b'] = 'افزودن یک بلوک شامل لیست علاقمندی های مشتری.';
$_MODULE['<{blockwishlist}leocame>blockwishlist_9ae79c1fccd231ac7fbbf3235dbf6326'] = 'لیست علاقمندی شما';
$_MODULE['<{blockwishlist}leocame>blockwishlist_5ef2d617096ae7b47a83f3d4de9c84bd'] = 'فعال سازی ماژول : انتخاب معتبر نمی باشد.';
$_MODULE['<{blockwishlist}leocame>blockwishlist_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات به روز رسانی شد';
$_MODULE['<{blockwishlist}leocame>blockwishlist_56ee3495a32081ccb6d2376eab391bfa'] = 'فهرست کردن';
$_MODULE['<{blockwishlist}leocame>blockwishlist_e6d0e1c8fc6a4fcf47869df87e04cd88'] = 'مشتریان';
$_MODULE['<{blockwishlist}leocame>blockwishlist_e0fd9b310aba555f96e76738ff192ac3'] = 'لیست علاقمندیها';
$_MODULE['<{blockwishlist}leocame>blockwishlist_deb10517653c255364175796ace3553f'] = 'محصول';
$_MODULE['<{blockwishlist}leocame>blockwishlist_694e8d1f2ee056f98ee488bdc4982d73'] = 'تعداد';
$_MODULE['<{blockwishlist}leocame>blockwishlist_502996d9790340c5fd7b86a5b93b1c9f'] = 'اولویت';
$_MODULE['<{blockwishlist}leocame>blockwishlist_655d20c1ca69519ca647684edbb2db35'] = 'بالا';
$_MODULE['<{blockwishlist}leocame>blockwishlist_87f8a6ab85c9ced3702b4ea641ad4bb5'] = 'متوسط';
$_MODULE['<{blockwishlist}leocame>blockwishlist_28d0edd045e05cf5af64e35ae0c4c6ef'] = 'پایین';
$_MODULE['<{blockwishlist}leocame>blockwishlist_862af8838fba380d3b30e63546a394e5'] = 'بدون لیست علاقمندی';
$_MODULE['<{blockwishlist}leocame>blockwishlist_641254d77e7a473aa5910574f3f9453c'] = 'لیست علاقمندیها';
$_MODULE['<{blockwishlist}leocame>blockwishlist_88b589bbf6282a2e02f50ebe90aae6f1'] = 'جهت مدیریت لیست علاقمندی ها، ابتدا وارد شوید.';
$_MODULE['<{blockwishlist}leocame>blockwishlist_4a84e5921e203aede886d04fc41a414b'] = 'حذف این محصول از لیست علاقمندی ها';
$_MODULE['<{blockwishlist}leocame>blockwishlist_f2a6c498fb90ee345d997f888fce3b18'] = 'حذف';
$_MODULE['<{blockwishlist}leocame>blockwishlist_4b7d496eedb665d0b5f589f2f874e7cb'] = 'جزییات محصول';
$_MODULE['<{blockwishlist}leocame>blockwishlist_09dc02ecbb078868a3a86dded030076d'] = 'بدون محصول';
$_MODULE['<{blockwishlist}leocame>blockwishlist_7ec9cceb94985909c6994e95c31c1aa8'] = 'لیست علاقمندیهای شما';
$_MODULE['<{blockwishlist}leocame>buywishlistproduct_607e1d854783c8229998ac2b5b6923d3'] = 'دسترسی نامعتبر است';
$_MODULE['<{blockwishlist}leocame>buywishlistproduct_b0ffc4925401f6f4edb038f5ca954937'] = 'شما باید وارد شوید';
$_MODULE['<{blockwishlist}leocame>cart_607e1d854783c8229998ac2b5b6923d3'] = 'دسترسی نامعتبر است';
$_MODULE['<{blockwishlist}leocame>cart_a9839ad48cf107667f73bad1d651f2ca'] = 'قالبی وجود ندارد';
$_MODULE['<{blockwishlist}leocame>cart_16a23698e7cf5188ce1c07df74298076'] = 'شما باید وارد شوید تا بتوانید لیست علاقمندیتان را مدیریت کنید.';
$_MODULE['<{blockwishlist}leocame>managewishlist_a9839ad48cf107667f73bad1d651f2ca'] = 'قالبی وجود ندارد';
$_MODULE['<{blockwishlist}leocame>managewishlist_65a26fd603de62416ed333c7a8928713'] = 'لغو ارسال این لیست علاقمندی';
$_MODULE['<{blockwishlist}leocame>managewishlist_0ac1aeb2429db494dd42ad2dc219ca7e'] = 'مخفی کردن محصولات';
$_MODULE['<{blockwishlist}leocame>managewishlist_0de9d09a36e820f9da7e87ab3678dd12'] = 'نمایش محصولات';
$_MODULE['<{blockwishlist}leocame>managewishlist_0bb3e067c0514f5ff2d5a8e45fc0f4be'] = 'مخفی کردن اطلاعات محصولات خریداری شده';
$_MODULE['<{blockwishlist}leocame>managewishlist_6fe88f5681da397d46fefe19b3020a6a'] = 'نمایش اطلاعات محصولات خریداری شده';
$_MODULE['<{blockwishlist}leocame>managewishlist_30820a1bf6a285e45cda2beda3d7738d'] = 'ارسال این لیست علاقمندی';
$_MODULE['<{blockwishlist}leocame>managewishlist_f2a6c498fb90ee345d997f888fce3b18'] = 'حذف';
$_MODULE['<{blockwishlist}leocame>managewishlist_4b7d496eedb665d0b5f589f2f874e7cb'] = 'جزییات محصول';
$_MODULE['<{blockwishlist}leocame>managewishlist_694e8d1f2ee056f98ee488bdc4982d73'] = 'تعداد';
$_MODULE['<{blockwishlist}leocame>managewishlist_502996d9790340c5fd7b86a5b93b1c9f'] = 'اولویت';
$_MODULE['<{blockwishlist}leocame>managewishlist_655d20c1ca69519ca647684edbb2db35'] = 'بالا';
$_MODULE['<{blockwishlist}leocame>managewishlist_87f8a6ab85c9ced3702b4ea641ad4bb5'] = 'متوسط';
$_MODULE['<{blockwishlist}leocame>managewishlist_28d0edd045e05cf5af64e35ae0c4c6ef'] = 'پایین';
$_MODULE['<{blockwishlist}leocame>managewishlist_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{blockwishlist}leocame>managewishlist_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'ایمیل';
$_MODULE['<{blockwishlist}leocame>managewishlist_94966d90747b97d1f0f206c98a8b1ac3'] = 'ارسال';
$_MODULE['<{blockwishlist}leocame>managewishlist_19f823c6453c2b1ffd09cb715214813d'] = 'اطلاعات مورد نیاز';
$_MODULE['<{blockwishlist}leocame>managewishlist_deb10517653c255364175796ace3553f'] = 'محصول';
$_MODULE['<{blockwishlist}leocame>managewishlist_3384622e86410fd01fa318fedc6a98ce'] = 'پیشنهاد شده بوسیله';
$_MODULE['<{blockwishlist}leocame>managewishlist_44749712dbec183e983dcd78a7736c41'] = 'تاریخ';
$_MODULE['<{blockwishlist}leocame>managewishlist_09dc02ecbb078868a3a86dded030076d'] = 'هیچ محصولی وجود ندارد';
$_MODULE['<{blockwishlist}leocame>my-account_7ec9cceb94985909c6994e95c31c1aa8'] = 'لیست علاقمندیهای شما';
$_MODULE['<{blockwishlist}leocame>my-account_723edf7c24638ed18d2fa831e647a5cc'] = 'لیست علاقمندی';
$_MODULE['<{blockwishlist}leocame>mywishlist_607e1d854783c8229998ac2b5b6923d3'] = 'دسترسی نامعتبر است';
$_MODULE['<{blockwishlist}leocame>mywishlist_b30545c7b2d429352b9afdd85be810c7'] = 'شما باید یک اسم در نظر بگیرید.';
$_MODULE['<{blockwishlist}leocame>mywishlist_b74c118d823d908d653cfbf1c877ae55'] = 'این اسم در حال حاضر توسط لیست دیگری مورد استفاده قرار گرفته است.';
$_MODULE['<{blockwishlist}leocame>mywishlist_7098d49878bbd102b13038a748125e27'] = 'نمی توان لیست علاقمندی ها را حذف کرد';
$_MODULE['<{blockwishlist}leocame>mywishlist_a9839ad48cf107667f73bad1d651f2ca'] = 'قالبی وجود ندارد';
$_MODULE['<{blockwishlist}leocame>sendwishlist_8f4be21ec3cfbba15a349e9c5e888579'] = 'نشانه ی نامعتبر';
$_MODULE['<{blockwishlist}leocame>sendwishlist_90d8a44a1fba13198035d86caeeb2d4d'] = 'علاقه‌مندی نامعتبر';
$_MODULE['<{blockwishlist}leocame>sendwishlist_072df51ea0cb142b770d6209dab5a85b'] = 'خطای ارسال فهرست علاقه‌مندی';
$_MODULE['<{blockwishlist}leocame>view_7cff73659ca47fea5d29644da84f35e2'] = 'اجازه لیست علاقمندی های اشتباه';
$_MODULE['<{blockwishlist}leocame>view_a9839ad48cf107667f73bad1d651f2ca'] = 'قالبی وجود ندارد';
$_MODULE['<{blockwishlist}leocame>view_641254d77e7a473aa5910574f3f9453c'] = 'لیست علاقمندی';
$_MODULE['<{blockwishlist}leocame>view_9862d189193590487b2686b1c9043714'] = 'لیست علاقمندی های دیگر';
$_MODULE['<{blockwishlist}leocame>view_3d01e7d8174b6a9b088b44870714cf07'] = 'خوش آمدید به لیست علاقمندی';
$_MODULE['<{blockwishlist}leocame>view_4351cfebe4b61d8aa5efa1d020710005'] = 'نمایش';
$_MODULE['<{blockwishlist}leocame>view_4b7d496eedb665d0b5f589f2f874e7cb'] = 'جزییات محصول';
$_MODULE['<{blockwishlist}leocame>view_3d0d1f906e27800531e054a3b6787b7c'] = 'تعداد:';
$_MODULE['<{blockwishlist}leocame>view_05f32f85cccc8308d09a81e7bad3f80e'] = 'اولویت:';
$_MODULE['<{blockwishlist}leocame>view_655d20c1ca69519ca647684edbb2db35'] = 'بالا';
$_MODULE['<{blockwishlist}leocame>view_87f8a6ab85c9ced3702b4ea641ad4bb5'] = 'متوسط';
$_MODULE['<{blockwishlist}leocame>view_28d0edd045e05cf5af64e35ae0c4c6ef'] = 'پایین';
$_MODULE['<{blockwishlist}leocame>view_2d0f6b8300be19cf35e89e66f0677f95'] = 'اضافه به سبد خرید';
$_MODULE['<{blockwishlist}leocame>view_09dc02ecbb078868a3a86dded030076d'] = 'هیچ محصولی وجود ندارد';
$_MODULE['<{blockwishlist}leocame>mywishlist_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'حساب کاربری شما';
$_MODULE['<{blockwishlist}leocame>mywishlist_7ec9cceb94985909c6994e95c31c1aa8'] = 'لیست علاقمندیهای شما';
$_MODULE['<{blockwishlist}leocame>mywishlist_06c335f27f292a096a9bf39e3a58e97b'] = 'لیست علاقمندی جدید';
$_MODULE['<{blockwishlist}leocame>mywishlist_49ee3087348e8d44e1feda1917443987'] = 'نام';
$_MODULE['<{blockwishlist}leocame>mywishlist_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{blockwishlist}leocame>mywishlist_03ab340b3f99e03cff9e84314ead38c0'] = 'تعداد';
$_MODULE['<{blockwishlist}leocame>mywishlist_5e729042e30967c9d6f65c6eab73e2fe'] = 'مشاهده شده';
$_MODULE['<{blockwishlist}leocame>mywishlist_0eceeb45861f9585dd7a97a3e36f85c6'] = 'ایجاد شده';
$_MODULE['<{blockwishlist}leocame>mywishlist_45284ef16392f85ff424b2ef36ab5948'] = 'لینک مستقیم';
$_MODULE['<{blockwishlist}leocame>mywishlist_f2a6c498fb90ee345d997f888fce3b18'] = 'حذف';
$_MODULE['<{blockwishlist}leocame>mywishlist_4351cfebe4b61d8aa5efa1d020710005'] = 'نمایش';
$_MODULE['<{blockwishlist}leocame>mywishlist_d025259319054206be54336a00defe89'] = 'آیا می خواهید این لیست علاقمندی را حذف کنید؟';
$_MODULE['<{blockwishlist}leocame>mywishlist_0b3db27bc15f682e92ff250ebb167d4b'] = 'برگشت به حساب کاربری';
$_MODULE['<{blockwishlist}leocame>mywishlist_8cf04a9734132302f96da8e113e80ce5'] = 'خانه';
