<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{specialproduct}leocame>specialproduct_12c0a3268463d1a571fa292095c9b937'] = 'مدیریت محصولات مخصوص جدید';
$_MODULE['<{specialproduct}leocame>specialproduct_02f1d5e9af91ea626b78b1a00567bb67'] = 'اضافه نمودن محصول مخصوص جدید';
$_MODULE['<{specialproduct}leocame>specialproduct_49ee3087348e8d44e1feda1917443987'] = 'نام';
$_MODULE['<{specialproduct}leocame>specialproduct_b9aefa40a54680bb258f9f9569290fae'] = 'نام محصول';
$_MODULE['<{specialproduct}leocame>specialproduct_309ca8965c310374e5c5585d1904dd05'] = 'تعداد روز';
$_MODULE['<{specialproduct}leocame>specialproduct_3c7679577cd1d0be2b98faf898cfc56d'] = 'زمان اضافه کردن';
$_MODULE['<{specialproduct}leocame>specialproduct_a3b5ce10a6ec3a16f8c712b69d0e7f3a'] = 'تاریخ شروع';
$_MODULE['<{specialproduct}leocame>specialproduct_3b8dea39d7cf07402edd7270ae0a3ee2'] = 'تاریخ پایان';
$_MODULE['<{specialproduct}leocame>specialproduct_ef61fb324d729c341ea8ab9901e23566'] = 'اضافه کردن محصول جدید';
$_MODULE['<{specialproduct}leocame>specialproduct_7a63f844fc42cdfe47f4bf09e58092e8'] = 'نام محصول';
$_MODULE['<{specialproduct}leocame>specialproduct_5491b218818ac07d04e629e253d62bc9'] = 'نوع محصول جدید';
$_MODULE['<{specialproduct}leocame>specialproduct_9885108021c2c91e0e19090236d0aa52'] = 'تعداد روز';
$_MODULE['<{specialproduct}leocame>specialproduct_b5a7adde1af5c87d7fd797b6245c2a39'] = 'توضیح';
$_MODULE['<{specialproduct}leocame>specialproduct_323126045a230b218de6ebd6a53fb7f8'] = 'تصویر';
$_MODULE['<{specialproduct}leocame>specialproduct_1432596d084873bee8df589b0b01448c'] = 'تاریخ فعال شدن';
$_MODULE['<{specialproduct}leocame>specialproduct_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
