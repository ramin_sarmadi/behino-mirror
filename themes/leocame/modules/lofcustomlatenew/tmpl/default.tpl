<div class="customlatestnew {$class_prefix} span7 block" id="lof-custom-{$pos}">
	<div class="list-new">
		{if $show_title}
			<h3 class="title_block"><span>{$module_title}</span></h3>
		{/if}
		<div class="block_content">
			{$content}
		</div>
	</div>
</div>