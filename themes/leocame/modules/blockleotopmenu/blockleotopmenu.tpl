{if $MENU != ''}
	<!-- Menu -->
	<div class="navbar"><div class="navbar-inner">
		<button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
    		<span class="icon-bar"></span>
    		<span class="icon-bar"></span>
    		<span class="icon-bar"></span>
		</button>
		<div class="nav-collapse collapse">
			<ul class="nav ">
				<li class="first-item"><a href="{$link->getPageLink('index')}">&nbsp;</a></li>
				{$MENU}
			</ul>
		</div>
    </div></div>
	<!--/ Menu -->
{/if}