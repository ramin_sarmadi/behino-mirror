<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statssales}leocame>statssales_45c4b3e103155326596d6ccd2fea0f25'] = 'فروش ها و سفارشات';
$_MODULE['<{statssales}leocame>statssales_f57f893f9fbc11843fd4c9997ec41824'] = 'نمایش سیر تکاملی فروش ها و سفارش های وضعیت';
$_MODULE['<{statssales}leocame>statssales_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'همه';
$_MODULE['<{statssales}leocame>statssales_d7778d0c64b6ba21494c97f77a66885a'] = 'فیلتر';
$_MODULE['<{statssales}leocame>statssales_35214bc44485bc7161f9d1bdbacad5b8'] = 'این  نمودارها سیر تکاملی سفارشات و فروش شما را در یک بازه ی زمانی نمایش می  دهند. این ابزار به شما امکان مرور سریع انعطاف پذیری فروشگاهتان را می  دهد. همچنین شما می توانید بازه ی زمانی مشخصی را مشاهده کنید (به طور مثال  روزهای تعطیل). تنها سفارشات تایید شده در این دو نمودار نمایش داده می  شوند.';
$_MODULE['<{statssales}leocame>statssales_9ccb8353e945f1389a9585e7f21b5a0d'] = 'سفارشات ثبت شده:';
$_MODULE['<{statssales}leocame>statssales_156e5c5872c9af24a5c982da07a883c2'] = 'محصولات خریده شده:';
$_MODULE['<{statssales}leocame>statssales_998e4c5c80f27dec552e99dfed34889a'] = 'استخراج CSV';
$_MODULE['<{statssales}leocame>statssales_ec3e48bb9aa902ba2ad608547fdcbfdc'] = 'فروش ها:';
$_MODULE['<{statssales}leocame>statssales_14b2ae2d824b5dfd798a8caf35732136'] = 'شما می توانید آمار بخش توزیع شده ی سفارش را اینجا مشاهده کنید.';
$_MODULE['<{statssales}leocame>statssales_da80af4de99df74dd59e665adf1fac8f'] = 'سفارشی برای این دوره نیست.';
$_MODULE['<{statssales}leocame>statssales_6602bbeb2956c035fb4cb5e844a4861b'] = 'راهنما';
$_MODULE['<{statssales}leocame>statssales_75288849ad081e6ef15e00c37b7570f2'] = 'وضعیت های سفارش مختلف';
$_MODULE['<{statssales}leocame>statssales_ecfa88580e342ade21a8403765c0dde6'] = 'شما  در پنل مدیریت وضعیت های زیر را برای سفارشات خود خواهید یافت: منظر تایید  پرداخت، پرداخت تایید شده، در حال آماده سازی، در حال حمل، ارسال شده، لغو  شده، بازپرداخت شده، خطا در پرداخت، خارج از موجودی و منتظر پرداخت توسط  بانک.';
$_MODULE['<{statssales}leocame>statssales_d60bf28bef003143f1b95edbbc08cd68'] = 'وضعیت را از پنل مدیریت نمی توان پاک کرد اما می توانید به تعداد دلخواه وضعیت اضافه نمایید.';
$_MODULE['<{statssales}leocame>statssales_bb7ad89807bf69ddca986c142311f936'] = 'محصولات و سفارشات';
$_MODULE['<{statssales}leocame>statssales_12c500ed0b7879105fb46af0f246be87'] = 'سفارش';
$_MODULE['<{statssales}leocame>statssales_b52b44c9d23e141b067d7e83b44bb556'] = 'محصول';
$_MODULE['<{statssales}leocame>statssales_14f1c54626d722168ee62dff05ed811e'] = 'فروش در';
$_MODULE['<{statssales}leocame>statssales_b63d7e91b286748a447bf3573dbc9bfb'] = 'درصد سفارش ها بواسطه وضعیت';
