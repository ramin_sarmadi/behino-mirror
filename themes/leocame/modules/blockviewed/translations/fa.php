<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockviewed}leocame>blockviewed_fcb72d5a95d9a713f84425c807bbd667'] = 'بلوک محصولات بازدید شده';
$_MODULE['<{blockviewed}leocame>blockviewed_eaa362292272519b786c2046ab4b68d2'] = 'افزودن یک بلوک جهت نمایش محصولات بازدیدشده توسط مشتری.';
$_MODULE['<{blockviewed}leocame>blockviewed_2e57399079951d84b435700493b8a8c1'] = 'شما باید فیلد محصولات نمایشی را پر نمایید.';
$_MODULE['<{blockviewed}leocame>blockviewed_73293a024e644165e9bf48f270af63a0'] = 'تعداد نامعتبر';
$_MODULE['<{blockviewed}leocame>blockviewed_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات به روز رسانی شد';
$_MODULE['<{blockviewed}leocame>blockviewed_f4f70727dc34561dfde1a3c529b6205c'] = 'تنظیمات';
$_MODULE['<{blockviewed}leocame>blockviewed_e451e6943bb539d1bd804d2ede6474b5'] = 'محصولات نمایشی';
$_MODULE['<{blockviewed}leocame>blockviewed_d36bbb6066e3744039d38e580f17a2cc'] = 'تعیین تعداد محصولاتی که در این در این بخش نمایش داده می شوند';
$_MODULE['<{blockviewed}leocame>blockviewed_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{blockviewed}leocame>blockviewed_43560641f91e63dc83682bc598892fa1'] = 'محصولات بازدید شده';
$_MODULE['<{blockviewed}leocame>blockviewed_8f7f4c1ce7a4f933663d10543562b096'] = 'بیشتر درباره';
