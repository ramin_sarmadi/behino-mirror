<!-- Data Sheet -->
{if ($datasheet != 0)}
    <div class="row-fluid">
        <div class="span12">
            <div id="description">
                <div id="more_info_block" class="clear">
                    <div class="row-fluid box-row">
                        <div class="span12">
                            <div id="fani-title" class="product-box-title">
                                <img id="in-one-eye" src="{$img_dir}product-icons/technical.png"/>
                                <span class="prod-title-text">{l s="مشخصات فنی"}</span>
                            </div>
                        </div>
                    </div>

                    {*<div id="more_info_sheets" class="sheets align_justify row-fluid">*}
                    <div id="groupedfeatures feature-scroll" style="float:right; width:100%; margin-bottom:30px;">
                        {if $loc=='footer'}<h2>{l s='Data Sheet' mod='sodfeaturesgrouped'}</h2>{/if}
                        <table width="100%" style="border-collapse:collapse; border-spacing:0;">
                            {foreach from=$datasheet item=sheet}
                                <tr style="background:{$groupcellcolor}; border:1px solid #999999">
                                    <td align="right" valign="middle" colspan="2"
                                        style="border:1px solid #999999; padding:5px;">
<span style="color:{$groupfontcolor}; font-family:wyekan, tahoma, Helvetica, sans-serif; font-size:19px;">
{$sheet.groupname}
</span>
                                    </td>
                                </tr>
                                {foreach from=$sheet.features item=feat}
                                    <div dir="ltr">{*$sodprodfeat1|print_r*}</div>
                                    {foreach from=$sodprodfeat item=feature}


                                        {if ($feature.id_feature == $feat) and ($feature.id_group == $sheet.id_group) and  ($feature.featurevalue != '')}
                                            <tr border="1px" style="border:1px solid #999999">
                                                <td align="right" valign="middle" width="30%"
                                         style="border:1px solid #999999; padding:5px; background:{$featurecellcolor}; font-size:14px; ">
                                        <span style="color:{$featurefontcolor}; font-family:wyekan, tahoma, Helvetica, sans-serif; ">
                                        {$feature.feature_name}
                                        </span>
                                                </td>
                                                <td align="right" valign="middle" width="70%"
                                                    style="border:1px solid #999999; padding:5px; background:{$featurevaluecellcolor}; font-size:12px;">
                                                <span style="color:{$featurevaluefontcolor}; font-family:wyekan, tahoma, Helvetica, sans-serif;">
                                                {$feature.featurevalue}
                                                </span>
                                                </td>
                                            </tr>
                                        {/if}

                                    {/foreach}

                                {/foreach}

                            {/foreach}
                        </table>
                    </div>

                    {*</div>*}
                    {*<div style="clear:both"></div>*}
                    {*<div style="clear:both"></div>*}
                </div>
            </div>
        </div>
    </div>
{/if}
<!-- /Data Sheet Table -->


