<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_632535e0cdfd3380906f6b7d40ba78d3'] = 'بلوک اطلاعات تماس';
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_86458ae1631be34a6fcbf1a4584f5abe'] = 'برای نمایش برخی از اطلاعات تماس فروشگاه خود از این بلوک استفاده نمایید';
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'تنظیمات بروز شد.';
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_c281f92b77ba329f692077d23636f5c9'] = 'نام شرکت';
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_dd7bf230fde8d4836917806aff6a6b27'] = 'آدرس';
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_1f8261d17452a959e013666c5df45e07'] = 'شماره تلفن';
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'ایمیل';
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_b17f3f4dcf653a5776792498a9b44d6a'] = 'به روز رسانی تنظیمات';
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_02d4482d332e1aef3437cd61c9bcc624'] = 'تماس با ما';
$_MODULE['<{blockcontactinfos}leocame>blockcontactinfos_2e006b735fbd916d8ab26978ae6714d4'] = 'تلفن :';
