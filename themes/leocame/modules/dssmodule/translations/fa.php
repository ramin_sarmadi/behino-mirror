<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{dssmodule}leocame>dssmodule_3cbeccf055a0d61c6f622450bd77f2f9'] = 'بروز رسانی قیمت با موفقیت انجام شد.';
$_MODULE['<{dssmodule}leocame>dssmodule_d9b7d880bea1a788549628eb84c4e432'] = 'بروز رسانی قیمت با موفقیت انجام نشد!';
$_MODULE['<{dssmodule}leocame>dssmodule_712a465b5cc67168e8852965932ec64e'] = 'بروز رسانی موجودی با موفقیت انجام شد.';
$_MODULE['<{dssmodule}leocame>dssmodule_dd41264cde7a658f3d7f3823844df647'] = 'بروز رسانی موجودی با موفقیت انجام نشد!';
$_MODULE['<{dssmodule}leocame>dssmodule_2306a9bbf177eb7041e625e53b98580b'] = 'بروز رسانی موجودی با موفقیت انجام شد.';
$_MODULE['<{dssmodule}leocame>dssmodule_8670106b42a1b613b467c58d1631bac0'] = 'بروز رسانی موجودی با موفقیت انجام نشد!';
$_MODULE['<{dssmodule}leocame>dssmodule_2ad8caa590ef238b20e87375e43570b4'] = 'موجودی برای این ترکیب با موفقیت اضافه شد.';
$_MODULE['<{dssmodule}leocame>dssmodule_9660010252d047eefe747148510cfa98'] = 'موجودی برای این ترکیب با موفقیت اضافه نشد!';
$_MODULE['<{dssmodule}leocame>dssmodule_21fe209cd8b510679878dcdc6afda4db'] = 'خطای غیرقابل پیش بینی رخ داده است.';
$_MODULE['<{dssmodule}leocame>dssmodule_8049f2cd277669fd0daab69a607b7f0e'] = 'نام محصول';
$_MODULE['<{dssmodule}leocame>dssmodule_7479f46a308783919b16f8fe5b0b0d22'] = 'حداقل قیمت';
$_MODULE['<{dssmodule}leocame>dssmodule_e045ebc2e4acfae09727ef83c90aabbc'] = 'حداقل موجودی';
