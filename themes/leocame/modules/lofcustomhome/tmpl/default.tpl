<div class="lof-custom {$class_prefix} top-custom" id="lof-custom-{$pos}">
<div class="box-customtop">
	{if $show_title}
		<h2 class="b_title_block">{$module_title}</h2>
	{/if}
	<div class="lof-custom-content">
		{$content}
	</div>
	</div>
</div>