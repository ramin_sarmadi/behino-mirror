<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockstore}leocame>blockstore_68e9ecb0ab69b1121fe06177868b8ade'] = 'بلوک فروشگاه ها';
$_MODULE['<{blockstore}leocame>blockstore_2d7884c3777bd04028c4a55a820880a8'] = 'این بلوک مکان فروشگاه های شما را نمایش میدهد';
$_MODULE['<{blockstore}leocame>blockstore_126b21ce46c39d12c24058791a236777'] = 'تصویر معتبر نیست';
$_MODULE['<{blockstore}leocame>blockstore_df7859ac16e724c9b1fba0a364503d72'] = 'خطایی در هنگام بارگذاری فایل به وجود آمده است';
$_MODULE['<{blockstore}leocame>blockstore_efc226b17e0532afff43be870bff0de7'] = 'تنظیمات ذخیره شد';
$_MODULE['<{blockstore}leocame>blockstore_151e79863510ec66281329505bf9fbde'] = 'تنظیمات بلوک فروشگاه ها';
$_MODULE['<{blockstore}leocame>blockstore_2dd1d28275cdb8b78ebd17f6e25aac0d'] = 'عکس فروشگاه';
$_MODULE['<{blockstore}leocame>blockstore_8c38cf08a0d0a01bd44c682479432350'] = 'تغییر عکس';
$_MODULE['<{blockstore}leocame>blockstore_3eedfc0fbc9042acf0ecfe0f325428c4'] = 'تصاویر در سایز 174*115 نمایش داده می شوند';
$_MODULE['<{blockstore}leocame>blockstore_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{blockstore}leocame>blockstore_8c0caec5616160618b362bcd4427d97b'] = 'فروشگاه های ما';
$_MODULE['<{blockstore}leocame>blockstore_142fe29b7422147cdac10259a0333c11'] = 'بررسی فروشگاه های ما';
