<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{priceanothersite}leocame>priceanothersite_e71e2b469de12b30c768e64c3fc37aa8'] = 'رکورد حذف گردید';
$_MODULE['<{priceanothersite}leocame>priceanothersite_668445f09da337d276dc852bb6a0dbb3'] = 'نام سایت';
$_MODULE['<{priceanothersite}leocame>priceanothersite_b9aefa40a54680bb258f9f9569290fae'] = 'نام محصول';
$_MODULE['<{priceanothersite}leocame>priceanothersite_97e7c9a7d06eac006a28bf05467fcc8b'] = 'لینک';
$_MODULE['<{priceanothersite}leocame>priceanothersite_32fb657c40fcfa7adbae0bd35eb3a5fb'] = 'اضافه نمودن لینک برای محصول';
$_MODULE['<{priceanothersite}leocame>priceanothersite_8d7228e23a4cc1522370eba0276ed355'] = 'اضافه نمودن محصول';
$_MODULE['<{priceanothersite}leocame>priceanothersite_f69bd72e3531942a4605105f0a7a6dbc'] = 'اضافه نمودن سایت';
$_MODULE['<{priceanothersite}leocame>priceanothersite_4b2fbfa9d5ef07f4dbbe7c096ab2c54d'] = 'نمایش اطلاعات';
$_MODULE['<{priceanothersite}leocame>priceanothersite_469bba0a564235dfceede42db14f17b0'] = 'برگشت';
$_MODULE['<{priceanothersite}leocame>priceanothersite_189f63f277cd73395561651753563065'] = 'تگ 1';
$_MODULE['<{priceanothersite}leocame>priceanothersite_e599a1ed262c0cd839ac5a723477f6f5'] = 'تگ 2';
$_MODULE['<{priceanothersite}leocame>priceanothersite_84c2c371fc59141918072a3f84170fcc'] = 'تگ 3';
$_MODULE['<{priceanothersite}leocame>priceanothersite_ef61fb324d729c341ea8ab9901e23566'] = 'اضافه نمودن';
$_MODULE['<{priceanothersite}leocame>priceanothersite_9b1333baae5c888de083c7f81f9e8bcd'] = 'محصول مرتبط';
$_MODULE['<{priceanothersite}leocame>priceanothersite_13059170ab4b4b87c19a07d553c9282c'] = 'نام سایت';
$_MODULE['<{priceanothersite}leocame>priceanothersite_19b167084010c66bb8a4aa5ad74a5733'] = 'آدرس سایت';
$_MODULE['<{priceanothersite}leocame>priceanothersite_4e43e89a33e3016d984fa2b9bd104ce0'] = 'تگ 1';
$_MODULE['<{priceanothersite}leocame>priceanothersite_e7cfb6c8b6def7f1a137651f92bb6bc9'] = 'تگ 2';
$_MODULE['<{priceanothersite}leocame>priceanothersite_e1de96b6f94e0155ae3c71f1fc2f7421'] = 'تگ 3';
$_MODULE['<{priceanothersite}leocame>priceanothersite_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{priceanothersite}leocame>priceanothersite_14c70d662861c87251e8fe80de1d629f'] = 'ارتباط با محصول';
$_MODULE['<{priceanothersite}leocame>priceanothersite_dafeefef053bfa2474a35f8a77f93d4e'] = 'لینک محصول';
$_MODULE['<{priceanothersite}leocame>priceanothersite_a7d6475ec8993b7224d6facc8cb0ead6'] = 'سایت';
$_MODULE['<{priceanothersite}leocame>priceanothersite_3601146c4e948c32b6424d2c0a7f0118'] = 'قیمت';
$_MODULE['<{priceanothersite}leocame>priceanothersite_9e0fd4bd4b5707d6ea0ddecca8a8e0cd'] = 'دارای لینک نمی باشد';
