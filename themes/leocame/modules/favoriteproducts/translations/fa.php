<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{favoriteproducts}leocame>favoriteproducts_c249aeb21294d5e97598462b550e73eb'] = 'محصولات مورد علاقه';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts_018770f4456d82ec755cd9d0180e4cce'] = 'نمایش صفحه ای شامل محصولات مورد علاقه مشتری';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts-account_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'حساب کاربری شما';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts-account_e5090b68524b1bbfd7983bfa9500b7c9'] = 'محصولات مورد علاقه من';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts-account_44fd621c6504b8a5346946650682a842'] = 'هنوز هیچ محصول مورد علاقه ای وجود ندارد';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts-account_6f16227b3e634872e87b0501948310b6'] = 'برگشت به حساب کاربری';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts-extra_8128d66be9351da562f9b01f591e00cd'] = 'افزودن به علاقه مندی ها';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts-extra_b895a379bd5ea51680ead24ee13a1bb7'] = 'حذف از علاقه مندی ها';
$_MODULE['<{favoriteproducts}leocame>my-account_e5090b68524b1bbfd7983bfa9500b7c9'] = 'محصولات مورد علاقه من';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts-account_a34e0796b9c15d09300f67d972379722'] = 'محصولات مورد علاقه من';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts-account_e2233f8a7cec20324ed48bc32ec98a5d'] = 'هنوز محصول مورد علاقه ای وجود ندارد.';
$_MODULE['<{favoriteproducts}leocame>favoriteproducts-account_0b3db27bc15f682e92ff250ebb167d4b'] = 'بازگشت به حساب کاربری';
