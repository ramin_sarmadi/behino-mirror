<div class=" carousel slide" id="{$tabname}">
    {*$pricesimularproducts|print_r*}
    {if count($pricesimularproducts)>$itemsperpage}
        <a class="carousel-control left c-left same-next-pr" href="#{$tabname}" data-slide="prev"></a>
        <a class="carousel-control right c-right same-next-pr" href="#{$tabname}" data-slide="next"></a>
    {/if}
    <div class="wrapper-newproducts">
        <div class="carousel-inner">
            {$mpricesimularproducts=array_chunk($pricesimularproducts,$itemsperpage)}

            {foreach from=$mpricesimularproducts item=pricesimularproducts name=mypLoop}
                <div class="item {if $smarty.foreach.mypLoop.first}active{/if}">
                    {*$HAMID*}
                    {foreach from=$pricesimularproducts item=pricesimularproduct name=pricesimularproducts}
                        {if $pricesimularproduct@iteration%$columnspage==1}
                            <div class="row-fluid">
                        {/if}
                        <div class="span{$scolumn} product_block ajax_block_product {if $smarty.foreach.pricesimularproducts.first}first_item{elseif $smarty.foreach.pricesimularproducts.last}last_item{/if} {if $smarty.foreach.pricesimularproducts.index % 2}alternate_item{else}p-item{/if} clearfix">
                            <div class="list-products">
                                <div class="product-container clearfix">
                                    <div class="center_block">
                                        <a href="{$pricesimularproduct.link}"
                                           title="{$pricesimularproduct.name|escape:html:'UTF-8'}"
                                           class="product_image"><img
                                                    src="{$link->getImageLink($pricesimularproduct.link_rewrite, $pricesimularproduct.id_image, 'home_allinmart')}"
                                                    alt="{$pricesimularproduct.name|escape:html:'UTF-8'}"/>{if isset($pricesimularproduct.new) && $pricesimularproduct.new == 1}
                                            <span class="new">{l s='New' mod='blockleorelatedproducts'}</span>{/if}</a>
                                    </div>
                                    <div class="right_block">
                                        <h4 class="s_title_block"><a href="{$pricesimularproduct.link}"
                                                                     title="{$pricesimularproduct.name|truncate:50:'...'|escape:'htmlall':'UTF-8'}">{$pricesimularproduct.name|truncate:35:'...'|escape:'htmlall':'UTF-8'}</a>
                                        </h4>

                                        <div class="product_desc"><a href="{$pricesimularproduct.link}"
                                                                     title="{l s='More' mod='blockleorelatedproducts'}">{$pricesimularproduct.description_short|strip_tags|truncate:65:'...'}</a>
                                        </div>
                                        <a class="lnk_more" href="{$pricesimularproduct.link}"
                                           title="{l s='View' mod='blockleorelatedproducts'}">{l s='View' mod='blockleorelatedproducts'}</a>
                                        {if $pricesimularproduct.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
                                            <div class="price_container"><span
                                                    class="price">{if !$priceDisplay}{convertPrice price=$pricesimularproduct.price}{else}{convertPrice price=$pricesimularproduct.price_tax_exc}{/if}</span>
                                            </div>{else}
                                            <div style="height:21px;"></div>
                                        {/if}

                                        {if ($pricesimularproduct.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $pricesimularproduct.available_for_order AND !isset($restricted_country_mode) AND $pricesimularproduct.minimal_quantity == 1 AND $pricesimularproduct.customizable != 2 AND !$PS_CATALOG_MODE}
                                            {if ($pricesimularproduct.quantity > 0 OR $pricesimularproduct.allow_oosp)}
                                                <a class="exclusive ajax_add_to_cart_button"
                                                   rel="ajax_id_product_{$pricesimularproduct.id_product}"
                                                   href="{$link->getPageLink('cart')}?qty=1&amp;id_product={$pricesimularproduct.id_product}&amp;token={$static_token}&amp;add"
                                                   title="{l s='Add to cart' mod='blockleorelatedproducts'}"><span
                                                            class="icon-addcart"></span>{l s='Add to cart' mod='blockleorelatedproducts'}
                                                </a>
                                            {else}
                                                <span class="exclusive"><span
                                                            class="icon-addcart"></span>{l s='Add to cart' mod='blockleorelatedproducts'}</span>
                                            {/if}
                                        {else}
                                            <div style="height:23px;"></div>
                                        {/if}
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {if $pricesimularproduct@iteration%$columnspage==0||$smarty.foreach.pricesimularproducts.last}
                            </div>
                        {/if}

                    {/foreach}
                </div>
            {/foreach}
        </div>
    </div>
</div>
