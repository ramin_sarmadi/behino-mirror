<!-- MODULE Block specials -->

<!-- <div id="relatedproducts" class="block products_block exclusive nopadding blockleorelatedproducts">
    <h3 class="title_category">{$products|@count} {l s='other products in the same category:' mod='blockleorelatedproducts'}</h3>
    <div class="block_content">
        {if !empty($products)}
            {$tabname="leorelatedcarousel"}
            {include file="{$product_tpl}"}
        {/if}
    </div>
</div> -->



{if $simularproducts|@count > 0}
    <!-- iBartar -->
    <div id="relatedproducts" class="block products_block exclusive nopadding blockleorelatedproducts">
        <h3 class="title_category">{l s='simular products in the same category:' mod='blockleorelatedproducts'} {*$products|@count*}  </h3>
        <div class="block_content">
            {*$simularproducts|@count*}
            {if !empty($products)}
                {$tabname="leosimularcarousel"}
                {include file="{$simularproduct_tpl}"}
            {/if}
        </div>
    </div>
    <!-- iBartar -->

    <!-- /MODULE Block specials -->
    <script>
        $(document).ready(function() {
            $('.blockleorelatedproducts .carousel').each(function(){
                $(this).carousel({
                    pause: true,
                    interval: false
                });
            });

        });
    </script>
{/if}


{if $pricesimularproducts|@count > 0}
    <!-- iBartar -->
    <div id="relatedproducts" class="block products_block exclusive nopadding blockleorelatedproducts">
        <h3 class="title_category">{l s='Price simular products in the same category:' mod='blockleorelatedproducts'} {*$products|@count*}  </h3>
        <div class="block_content">
            {*$simularproducts|@count*}
            {if !empty($products)}
                {$tabname="leopricesimularcarousel"}
                {include file="{$pricesimularproduct_tpl}"}
            {/if}
        </div>
    </div>
    <!-- iBartar -->

    <!-- /MODULE Block specials -->
    <script>
        $(document).ready(function() {
            $('.blockleorelatedproducts .carousel').each(function(){
                $(this).carousel({
                    pause: true,
                    interval: false
                });
            });

        });
    </script>
{/if}


