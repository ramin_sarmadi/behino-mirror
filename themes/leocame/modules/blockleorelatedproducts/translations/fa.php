<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockleorelatedproducts}leocame>products_03c2e7e41ffc181a4e84080b4710e81e'] = 'جدید';
$_MODULE['<{blockleorelatedproducts}leocame>products_d3da97e2d9aee5c8fbe03156ad051c99'] = 'بیشتر';
$_MODULE['<{blockleorelatedproducts}leocame>products_4351cfebe4b61d8aa5efa1d020710005'] = 'نمایش';
$_MODULE['<{blockleorelatedproducts}leocame>products_2d0f6b8300be19cf35e89e66f0677f95'] = 'افزودن به سبد';
$_MODULE['<{blockleorelatedproducts}leocame>blockleorelatedproducts_16ff9964ab4a71241a4754081423c34d'] = 'محصولات مشابه';
$_MODULE['<{blockleorelatedproducts}leocame>blockleorelatedproducts_3908b38b86dba8e9c20241354dc358b6'] = 'محصولات با قیمت مشابه';
$_MODULE['<{blockleorelatedproducts}leocame>simularproducts_03c2e7e41ffc181a4e84080b4710e81e'] = 'جدید';
$_MODULE['<{blockleorelatedproducts}leocame>simularproducts_d3da97e2d9aee5c8fbe03156ad051c99'] = 'بیشتر';
$_MODULE['<{blockleorelatedproducts}leocame>simularproducts_4351cfebe4b61d8aa5efa1d020710005'] = 'نمایش';
$_MODULE['<{blockleorelatedproducts}leocame>simularproducts_2d0f6b8300be19cf35e89e66f0677f95'] = 'افزودن به سبد';
