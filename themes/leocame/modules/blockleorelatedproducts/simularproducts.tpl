<div class=" carousel slide" id="{$tabname}">
    {*$pricesimularproducts|print_r*}
    {if count($simularproducts)>$itemsperpage}
        <a class="carousel-control left" href="#{$tabname}" data-slide="prev"></a>
        <a class="carousel-control right" href="#{$tabname}" data-slide="next"></a>
    {/if}
    <div class="wrapper-newproducts">
        <div class="carousel-inner">
            {$msimularproducts=array_chunk($simularproducts,$itemsperpage)}

            {foreach from=$msimularproducts item=simularproducts name=mypLoop}
                <div class="item {if $smarty.foreach.mypLoop.first}active{/if}">
                    {*$HAMID*}
                    {foreach from=$simularproducts item=simularproduct name=simularproducts}
                        {if $simularproduct@iteration%$columnspage==1}
                            <div class="row-fluid">
                        {/if}
                        <div class="span{$scolumn} product_block ajax_block_product {if $smarty.foreach.simularproducts.first}first_item{elseif $smarty.foreach.simularproducts.last}last_item{/if} {if $smarty.foreach.simularproducts.index % 2}alternate_item{else}p-item{/if} clearfix">
                            <div class="list-products">
                                <div class="product-container clearfix">
                                    <div class="center_block">
                                        <a href="{$simularproduct.link}"
                                           title="{$simularproduct.name|escape:html:'UTF-8'}" class="product_image"><img
                                                    src="{$link->getImageLink($simularproduct.link_rewrite, $simularproduct.id_image, 'home_allinmart')}"
                                                    alt="{$simularproduct.name|escape:html:'UTF-8'}"/>{if isset($simularproduct.new) && $simularproduct.new == 1}
                                            <span class="new">{l s='New' mod='blockleorelatedproducts'}</span>{/if}</a>
                                    </div>
                                    <div class="right_block">
                                        <h4 class="s_title_block"><a href="{$simularproduct.link}"
                                                                     title="{$simularproduct.name|truncate:50:'...'|escape:'htmlall':'UTF-8'}">{$simularproduct.name|truncate:35:'...'|escape:'htmlall':'UTF-8'}</a>
                                        </h4>

                                        <div class="product_desc"><a href="{$simularproduct.link}"
                                                                     title="{l s='More' mod='blockleorelatedproducts'}">{$simularproduct.description_short|strip_tags|truncate:65:'...'}</a>
                                        </div>
                                        <a class="lnk_more" href="{$simularproduct.link}"
                                           title="{l s='View' mod='blockleorelatedproducts'}">{l s='View' mod='blockleorelatedproducts'}</a>
                                        {if $simularproduct.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE AND $simularproduct.price_tax_exc>0}
                                            <div class="price_container"><span
                                                    class="price">{if !$priceDisplay}{convertPrice price=$simularproduct.price}{else}{convertPrice price=$simularproduct.price_tax_exc}{/if}</span>
                                            </div>{else}
                                            <div style="height:21px;"></div>
                                        {/if}

                                        {if ($simularproduct.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $simularproduct.available_for_order AND !isset($restricted_country_mode) AND $simularproduct.minimal_quantity == 1 AND $simularproduct.customizable != 2 AND !$PS_CATALOG_MODE}
                                            {if ($simularproduct.quantity > 0 OR $simularproduct.allow_oosp)}
                                                <a class="exclusive ajax_add_to_cart_button"
                                                   rel="ajax_id_product_{$simularproduct.id_product}"
                                                   href="{$link->getPageLink('cart')}?qty=1&amp;id_product={$simularproduct.id_product}&amp;token={$static_token}&amp;add"
                                                   title="{l s='Add to cart' mod='blockleorelatedproducts'}"><span
                                                            class="icon-addcart"></span>{l s='Add to cart' mod='blockleorelatedproducts'}
                                                </a>
                                            {else}
                                                <span class="exclusive"><span
                                                            class="icon-addcart"></span>{l s='Add to cart' mod='blockleorelatedproducts'}</span>
                                            {/if}
                                        {else}
                                        {*<div style="height:23px;"></div>*}
                                        {/if}
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {if $simularproduct@iteration%$columnspage==0||$smarty.foreach.simularproducts.last}
                            </div>
                        {/if}

                    {/foreach}
                </div>
            {/foreach}
        </div>
    </div>
</div>
