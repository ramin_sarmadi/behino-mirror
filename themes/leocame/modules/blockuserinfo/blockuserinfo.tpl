<div id="header_user">
    <div id="header_user_info" {if $logged}class="logged"{/if}>
        {if $logged}
            <div class="nav-item">
                <span class="welcome">خوش آمدید،</span>
                <a href="{$link->getPageLink('my-account', true)}" class="account">
                    {$cookie->customer_firstname} {$cookie->customer_lastname}
                </a>
                <a href="{$link->getPageLink('index', true, NULL, "mylogout")}" class="logout">(خروج)</a>
            </div>
        {else}
            <img src="{$img_dir}login.png" class="login" style="width: 25px;height: 25px;">
            <a href="{$link->getPageLink('my-account', true)}"
               class="login">{l s='ورود/ثبت نام' }</a>
        {/if}
        <div class="nav-item" id="your_account">
            <div class="item-top"><a href="{$link->getPageLink('my-account', true)}"
                                     title="{l s='Your Account' mod='blockuserinfo'}">{l s='Your Account' mod='blockuserinfo'}</a>
            </div>
        </div>
    </div>
</div>