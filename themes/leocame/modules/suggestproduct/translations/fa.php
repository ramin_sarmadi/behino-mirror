<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{suggestproduct}leocame>suggestproduct_344a7df42bbefa0710e95ab428f2cc4c'] = 'مدیریت محصولات پیشنهادی';
$_MODULE['<{suggestproduct}leocame>suggestproduct_3b92956702b3384317c41af55087f5b2'] = 'اضافه نمودن تصویر و محصول پیشنهادی در سیستم';
$_MODULE['<{suggestproduct}leocame>suggestproduct_a1fa27779242b4902f7ae3bdd5c6d508'] = 'نوع محصول';
$_MODULE['<{suggestproduct}leocame>suggestproduct_49ee3087348e8d44e1feda1917443987'] = 'نام';
$_MODULE['<{suggestproduct}leocame>suggestproduct_b9aefa40a54680bb258f9f9569290fae'] = 'نام گروه محصول';
$_MODULE['<{suggestproduct}leocame>suggestproduct_309ca8965c310374e5c5585d1904dd05'] = 'تعداد روز';
$_MODULE['<{suggestproduct}leocame>suggestproduct_3c7679577cd1d0be2b98faf898cfc56d'] = 'تاریخ افزودن';
$_MODULE['<{suggestproduct}leocame>suggestproduct_a3b5ce10a6ec3a16f8c712b69d0e7f3a'] = 'تاریخ شروع';
$_MODULE['<{suggestproduct}leocame>suggestproduct_3b8dea39d7cf07402edd7270ae0a3ee2'] = 'تاریخ پایان';
$_MODULE['<{suggestproduct}leocame>suggestproduct_ef61fb324d729c341ea8ab9901e23566'] = 'افزودن';
$_MODULE['<{suggestproduct}leocame>suggestproduct_5f51aafee26398db560184a18f21a622'] = 'نام محصول پیشنهادی';
$_MODULE['<{suggestproduct}leocame>suggestproduct_527dd18d712c16542d6cc88a94e76e9f'] = 'اگر محصولی در ویرایش انتخاب نشده یعنی این محصول غیرفعال گشته است';
$_MODULE['<{suggestproduct}leocame>suggestproduct_9b8e6b9b26bae76b1128858b7634919c'] = 'عنوان گروه محصول';
$_MODULE['<{suggestproduct}leocame>suggestproduct_9885108021c2c91e0e19090236d0aa52'] = 'بازه زمانی (روز)';
$_MODULE['<{suggestproduct}leocame>suggestproduct_b5a7adde1af5c87d7fd797b6245c2a39'] = 'توضیحات';
$_MODULE['<{suggestproduct}leocame>suggestproduct_323126045a230b218de6ebd6a53fb7f8'] = 'تصویر';
$_MODULE['<{suggestproduct}leocame>suggestproduct_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
