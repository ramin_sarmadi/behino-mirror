
<!-- MODULE Home Featured Products -->
<div id="featured-products_block_center" class="block products_block clearfix span6">
    <h4>{l s='Featured products' mod='homefeatured'}</h4>
    {if isset($products) AND $products}
        <div class="block_content">
            {assign var='liHeight' value=250}
            {assign var='nbItemsPerLine' value=4}
            {assign var='nbLi' value=$products|@count}
            {math equation="nbLi/nbItemsPerLine" nbLi=$nbLi nbItemsPerLine=$nbItemsPerLine assign=nbLines}
            {math equation="nbLines*liHeight" nbLines=$nbLines|ceil liHeight=$liHeight assign=ulHeight}

            <ul class="product_images">
                        {foreach from=$products item=product name=myLoop}
                    <li class="feature-item{$smarty.foreach.myLoop.iteration}">
                        <a href="{$product.link}" title="{$product.legend|escape:'htmlall':'UTF-8'}" class="content_img clearfix">
                            <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_allinmart')}"  alt="{$product.legend|escape:'htmlall':'UTF-8'}" />
                        </a>
                        <h4 class="s_title_block">
                            <a href="{$product.link}" title="{$product.legend|escape:'htmlall':'UTF-8'}">
                                {$product.name}</a>
                        </h4>
                        {*<p class="product_desc"><a href="{$product.link}" title="{l s='More' mod='homefeatured'}">{$product.description_short|strip_tags|truncate:65:'...'}</a></p>*}
                        <p><span class="price">{convertPrice price=$product.price}</span></p>
                    </li>
                {/foreach}
            </ul>

        </div>
    {else}
        <p>{l s='No featured products' mod='homefeatured'}</p>
    {/if}
</div>
<!-- /MODULE Home Featured Products -->
