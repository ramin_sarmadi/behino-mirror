<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{homefeatured}leocame>homefeatured_dcf3f48ad88dabefc99b82013c4443de'] = 'محصولات پرطرفدار در صفحه خانگی';
$_MODULE['<{homefeatured}leocame>homefeatured_33d0c6fa8dcda0ea168f8ef69556acfe'] = 'محصولات پرطرفدار را در وسط صفحه اصلی شما نمایش می دهد.';
$_MODULE['<{homefeatured}leocame>homefeatured_8f31eb2413dea86c661532d4cf973d2f'] = 'تعداد محصولات نامعتبر';
$_MODULE['<{homefeatured}leocame>homefeatured_6af91e35dff67a43ace060d1d57d5d1a'] = 'تنظیمات به روز رسانی شد';
$_MODULE['<{homefeatured}leocame>homefeatured_f4f70727dc34561dfde1a3c529b6205c'] = 'تنظیمات';
$_MODULE['<{homefeatured}leocame>homefeatured_4cad5369b27bff0ba256a479a575eb6f'] = 'برای افزودن محصولات به صفحه خانگی، فقط آنها را به شاخه خانه اضافه کنید';
$_MODULE['<{homefeatured}leocame>homefeatured_87ae431d2f8285c91660bd00d330a257'] = 'تعداد محصولات نمایش داده شده';
$_MODULE['<{homefeatured}leocame>homefeatured_b263d5a770ca788195f4038d1a0ea6fc'] = 'تعداد محصولاتی که می خواهید در صفحه اول به نمایش در بیاید را بنویسید (پیش فرض : 8)';
$_MODULE['<{homefeatured}leocame>homefeatured_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{homefeatured}leocame>homefeatured_ca7d973c26c57b69e0857e7a0332d545'] = 'محصولات پرطرفدار';
$_MODULE['<{homefeatured}leocame>homefeatured_03c2e7e41ffc181a4e84080b4710e81e'] = 'جدید';
$_MODULE['<{homefeatured}leocame>homefeatured_d3da97e2d9aee5c8fbe03156ad051c99'] = 'بیشتر';
$_MODULE['<{homefeatured}leocame>homefeatured_4351cfebe4b61d8aa5efa1d020710005'] = 'مشاهده';
$_MODULE['<{homefeatured}leocame>homefeatured_2d0f6b8300be19cf35e89e66f0677f95'] = 'افزودن به سبد خرید';
$_MODULE['<{homefeatured}leocame>homefeatured_e0e572ae0d8489f8bf969e93d469e89c'] = 'محصول پرطرفداری وجود ندارد';
