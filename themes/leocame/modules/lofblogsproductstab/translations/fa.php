<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{lofblogsproductstab}leocame>lofblogsproductstab_7405db25c5dfcdf430bf1d4604968f89'] = 'مطالب مرتبط شده با محصولات ماژول وبلاگ';
$_MODULE['<{lofblogsproductstab}leocame>lofblogsproductstab_843b4e436e842ae7cd38a426a2b9813f'] = 'مطالب مرتبط وبلاگ را در یک سر برگ صفحه محصولات نمایش دهید';
$_MODULE['<{lofblogsproductstab}leocame>lofblogsproductstab_42ede76e2cad29bad5114c17a1e91a4d'] = 'آیا شما واقعا می خواهید محصولات مطالب وبلاگ را حذف کنید؟';
$_MODULE['<{lofblogsproductstab}leocame>lofblogsproductstab_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات بروزرسانی شد';
$_MODULE['<{lofblogsproductstab}leocame>form_06933067aafd48425d67bcb01bba5cb6'] = 'به روز رسانی';
$_MODULE['<{lofblogsproductstab}leocame>params_lang_4ede758053afb49f54ea2ef0d5276910'] = 'تنظیمات عمومی';
$_MODULE['<{lofblogsproductstab}leocame>params_lang_d721757161f7f70c5b0949fdb6ec2c30'] = 'قالب';
$_MODULE['<{lofblogsproductstab}leocame>params_lang_38d4de28b224822af6f27b2f47accc2a'] = 'آیتم شمرده شده';
$_MODULE['<{lofblogsproductstab}leocame>params_lang_af18d0cf4499137708004e3fa16b04f0'] = 'تعداد مقالات نمایش داده شده، 0 را تایپ کنید برای تعداد نامحدود';
$_MODULE['<{lofblogsproductstab}leocame>params_lang_e160d770ad6c13a9fe736308cc661d10'] = 'محدودیت متن معرفی';
$_MODULE['<{lofblogsproductstab}leocame>params_lang_af6fb6d5f01201e08a4fdeb0bea45acf'] = 'نوع لیست';
$_MODULE['<{lofblogsproductstab}leocame>tabproduct_32f5797d9542915bf86b8423abb0f16f'] = 'مقالات مرتبط';
