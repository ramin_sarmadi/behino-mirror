<div class="lof-custom {$class_prefix}" id="lof-custom-{$pos}">
		{if $show_title}
			<h2 class="b_title_block">{$module_title}</h2>
		{/if}
		<div class="lof-custom-content">
			{$content}
		</div>
</div>