<!-- Block Newsletter module-->
<div id="newsletter_block_left" class="block">
	<h4 class="title_block">{l s='عضویت در خبرنامه'  mod='blocknewsletter'}</h4>
    <h5>برای دریافت آخرین اخبار ما در خبرنامه عضو شوید:</h5>
	<div class="block_content">
	{if isset($msg) && $msg}
		<p class="{if $nw_error}warning_inline{else}success_inline{/if}">{$msg}</p>
	{/if}
	<div class="input-append">
		<form class="navbar-form pull-left" action="{$link->getPageLink('index')}" method="post">
			
				{* @todo use jquery (focusin, focusout) instead of onblur and onfocus *}
				<input type="text" name="email" size="18" 
					value="{if isset($value) && $value}{$value}{else}{l s='your e-mail' mod='blocknewsletter'}{/if}" 
					onfocus="javascript:if(this.value=='{l s='your e-mail' mod='blocknewsletter' js=1}')this.value='';" 
					onblur="javascript:if(this.value=='')this.value='{l s='your e-mail' mod='blocknewsletter' js=1}';" 
					class="inputNew" />
				<!--<select name="action">
					<option value="0"{if isset($action) && $action == 0} selected="selected"{/if}>{l s='Subscribe' mod='blocknewsletter'}</option>
					<option value="1"{if isset($action) && $action == 1} selected="selected"{/if}>{l s='Unsubscribe' mod='blocknewsletter'}</option>
				</select>-->
					<input type="submit" value="{l s='Go' mod='blocknewsletter'}" class="btn" name="submitNewsletter" />
					<input type="hidden" name="action" value="0" />
			
		</form>
		</div>
	</div>
</div>
<!-- /Block Newsletter module-->
