<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{productvideo}leocame>productvideo_d0c17170ec2caec97e6e74707ef44aa8'] = 'مدیریت ویدئو محصولات';
$_MODULE['<{productvideo}leocame>productvideo_7bdac8529eed60b8c50a85e400c7fc68'] = 'اضافه نمودن ویدئو محصولات';
$_MODULE['<{productvideo}leocame>productvideo_57603ebd26ab29282070b75501f7e2e4'] = 'نام ویدئو';
$_MODULE['<{productvideo}leocame>productvideo_5f5f0783b6157a131f5f5739d5f5322a'] = 'توضیح ویدئو';
$_MODULE['<{productvideo}leocame>right_b4317d54048f6c998edd997fb2228214'] = 'لیست ویدئو محصولات';
$_MODULE['<{productvideo}leocame>right_c06f70bd45cc9a5677cae1f5d5d1098a'] = 'تاریخ ویدئو';
$_MODULE['<{productvideo}leocame>right_03ecc5b4ed9ad25af251d3d380a9c0dc'] = 'ویدئو موجود نیست';
$_MODULE['<{productvideo}leocame>tab_cae200d60293fefbb3910796d9c99bfb'] = 'توضیح';
$_MODULE['<{productvideo}leocame>tab_2b946b08a5f3b98a946605e450789621'] = 'نوع ویدئو';
$_MODULE['<{productvideo}leocame>tab_c06f70bd45cc9a5677cae1f5d5d1098a'] = 'تاریخ ویدئو';
