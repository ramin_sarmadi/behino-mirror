<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockspecials}leocame>blockspecials_c19ed4ea98cbf319735f6d09bde6c757'] = 'بلوک ویژه ها';
$_MODULE['<{blockspecials}leocame>blockspecials_42bcf407ca1621390ed7cbea2b2c0c62'] = 'با افزودن این بلوک محصولات ویژه را به نمایش بگذارید';
$_MODULE['<{blockspecials}leocame>blockspecials_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات به روز رسانی شد';
$_MODULE['<{blockspecials}leocame>blockspecials_f4f70727dc34561dfde1a3c529b6205c'] = 'تنظیمات';
$_MODULE['<{blockspecials}leocame>blockspecials_41385d2dca40c2a2c7062ed7019a20be'] = 'همواره این بلوک نمایش داده شود';
$_MODULE['<{blockspecials}leocame>blockspecials_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'فعال';
$_MODULE['<{blockspecials}leocame>blockspecials_b9f5c797ebbf55adccdd8539a65a0241'] = 'غیر فعال';
$_MODULE['<{blockspecials}leocame>blockspecials_a8a670d89a6d2f3fa59942fc591011ef'] = 'حتی اگر محصول ویژه موجود نیست این بلوک را نمایش بده';
$_MODULE['<{blockspecials}leocame>blockspecials_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{blockspecials}leocame>blockspecials_d1aa22a3126f04664e0fe3f598994014'] = 'محصولات ویژه';
$_MODULE['<{blockspecials}leocame>blockspecials_b4f95c1ea534936cc60c6368c225f480'] = 'همه محصولات ویژه';
$_MODULE['<{blockspecials}leocame>blockspecials_077a28bcf4e93718e678ec57128669a3'] = 'در حال حاضر محصول ویژه ای موجود نیست';
