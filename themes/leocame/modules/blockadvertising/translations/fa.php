<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockadvertising}leocame>blockadvertising_fd4c71c948857cce596a69fbaea7426b'] = 'بخش تبلیغات';
$_MODULE['<{blockadvertising}leocame>blockadvertising_91cd1ee56ea5324ff51578684a393a81'] = 'افزودن یک بلوک جهت نمایش تبلیغات.';
$_MODULE['<{blockadvertising}leocame>blockadvertising_070e16b4f77b90e802f789b5be583cfa'] = 'خطا در انتقال فایل آپلودی';
$_MODULE['<{blockadvertising}leocame>blockadvertising_6e7be6d836003f069c00cd217660913b'] = 'تنظیمات بخش تبلیغات';
$_MODULE['<{blockadvertising}leocame>blockadvertising_83b5a65e518c21ed0a5f2b383dd9b617'] = 'حذف تصویر';
$_MODULE['<{blockadvertising}leocame>blockadvertising_9dd7104e68d1b5f994264b9387c9d271'] = 'بدون تصویر';
$_MODULE['<{blockadvertising}leocame>blockadvertising_8c38cf08a0d0a01bd44c682479432350'] = 'تغییر تصویر';
$_MODULE['<{blockadvertising}leocame>blockadvertising_56d9dfa26d7848a3fbcd2ae3091d38d9'] = 'تصویر در اندازه 155x163 نمایش داده خواهد شد';
$_MODULE['<{blockadvertising}leocame>blockadvertising_9ce38727cff004a058021a6c7351a74a'] = 'لینک تصویر';
$_MODULE['<{blockadvertising}leocame>blockadvertising_b78a3223503896721cca1303f776159b'] = 'عنوان';
$_MODULE['<{blockadvertising}leocame>blockadvertising_ad3d06d03d94223fa652babc913de686'] = 'اعتبار';
