<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_8c4d7af5f578693f9a6cf391e912ee33'] = 'خالی کردن ثبت شده ها';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_f5c493141bb4b2508c5938fd9353291a'] = 'نمايش %1$s از %2$s';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_12d3c7a4296542c62474856ec452c045'] = 'مرجع';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_49ee3087348e8d44e1feda1917443987'] = 'نام';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_2a0440eec72540c5b30d9199c01f348c'] = 'تعداد فروش';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_6771f2d557a34bd89ea7abc92a0a069c'] = 'قیمت فروخته شده';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_11ff9f68afb6b8b5b8eda218d7c83a65'] = 'فروش ها';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_96e887520933606d3928a4ae164fe5e5'] = 'تعداد فروش/روز';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_7664a37e0cc56aaf39aebf2edbd3f98e'] = 'صفحه مشاهده شده';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_6e71d214907cd43403f4bbde5731a9a3'] = 'مناسب بودن تعداد برای فروش';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_b9ef20f6c20406b631db52374a519b1f'] = 'بهترین محصولات';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_dec6192cbc59ba37d38a3fdc5c3ed7f7'] = 'لیستی از بهترین محصولات';
$_MODULE['<{statsbestproducts}leocame>statsbestproducts_998e4c5c80f27dec552e99dfed34889a'] = 'استخراج CSV';
