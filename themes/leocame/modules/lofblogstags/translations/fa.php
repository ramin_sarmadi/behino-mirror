<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{lofblogstags}leocame>lofblogstags_dfd0b43b7f27a5e9e556af9fca9d1b0d'] = 'برچسب های وبلاگ';
$_MODULE['<{lofblogstags}leocame>lofblogstags_a200191c4a539c2c82fe002a3b1bde56'] = 'نمایش یک بلاک برچسب مطالب';
$_MODULE['<{lofblogstags}leocame>lofblogstags_a5b17f28c22d6c09b2b8b4a6a2a7a7b0'] = 'آیا شما واقعا می خواهید مطالب آرشیو وبلاگ را حذف کنید؟';
$_MODULE['<{lofblogstags}leocame>lofblogstags_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات بروزرسانی شد';
$_MODULE['<{lofblogstags}leocame>form_06933067aafd48425d67bcb01bba5cb6'] = 'به روز رسانی';
$_MODULE['<{lofblogstags}leocame>params_lang_4ede758053afb49f54ea2ef0d5276910'] = 'تنظیمات عمومی';
$_MODULE['<{lofblogstags}leocame>params_lang_d721757161f7f70c5b0949fdb6ec2c30'] = 'قالب';
$_MODULE['<{lofblogstags}leocame>params_lang_b07920dbfe83e3e97d2c0fdc069354de'] = 'محدودیت آیتم ها';
$_MODULE['<{lofblogstags}leocame>left_6490885aa56826aaaeefdb8dbb390e2a'] = 'برچسب های بلاگ';
$_MODULE['<{lofblogstags}leocame>left_49fa2426b7903b3d4c89e2c1874d9346'] = 'بیشتر درباره';
$_MODULE['<{lofblogstags}leocame>left_4e6307cfde762f042d0de430e82ba854'] = 'هیچ تگی هنوز مشخص نشده';
