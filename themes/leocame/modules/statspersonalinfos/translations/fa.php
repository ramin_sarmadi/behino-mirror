<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_1b94af23fd8ea879910a6307460ebba4'] = 'اطلاعات مشتری عضو شده';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_ff9c34d2829b95a0936b22b409640143'] = 'نمایش مشخصاتی چون جنسیت و سن.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_62a9b5081b2dab89113e94a0b4cb4be0'] = 'توزیع جنسیت به شما امکان تشخیص درصد زنان و مردان مشتری شما را می دهد.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_998e4c5c80f27dec552e99dfed34889a'] = 'استخراج CSV';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_4c5625b65180ffa4f4ad215c2d5ef7d7'] = 'بازه های سنی به شما امکان تشخیص سن مشتری های شما را می دهد.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_3a5c52ba51fe8f566a222da649c5e1f1'] = 'توزیع کشورها به شما امکان تشخیص اینکه در کجای دنیا مشتری ها از شما خرید می کنند.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_e0fcbbd416a4872ec8f91fefa823debe'] = 'بازه های ارزی به شما امکان تشخیص اینکه مشتری های شما از چه ارزی استفاده می کنند را می دهد.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_7cede292ed06f274bb9e8702738c058e'] = 'توزیع زبان به شما امکان تشخیص زبان های عمومی که مشتریان شما در فروشگاه استفاده می کنند را می دهد.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_9fde3510abe63111086fd4288a19e1be'] = 'هنوز هیچ مشتری ثبت نشده است.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_6602bbeb2956c035fb4cb5e844a4861b'] = 'راهنما';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_ce2dd26ca39bdbc6444f4dad3fa5d5fe'] = 'پیامگیران خود را مشخص کنید';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_74036f0101d2f3f9b082b8de981c18dc'] = 'برای اینکه هر پیام تأثیر داشته باشد، شما باید بدانید که به چه کسی باید بفرستید.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_daa0ad6a8b07a64f18b26e0f5e1255bd'] = 'روی کردن به مخاطب درست برای بکار بردن ابزار صحیح و پیروزی بر آنها ضروری است.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_4f7fe9e063af69bcd7f73d2011722c00'] = 'بهترین کار محدود کردن اقدامات به یک گروه یا گروه هایی از مشتریان است.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_04cac0902d33f8d779111bddde436f03'] = 'اطلاعات  مربوط به مشتری ثبت نام شده به شما اجازه می دهد تا پروفایل مشتری را به  دقت تعریف کنید به طوری که شما می توانید محصولات ویژه خود را با معیارهای  مختلف وفق دهید.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_1b97d8091d4b383e8316b5bf85b2194c'] = 'شما باید از این اطلاعات استفاده کنید تا فروش خود را افزایش دهید بوسیله';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_aaf4306af7449d0b6b99e8df15f422a2'] = 'راه اندازی کمپین های تبلیغاتی خطاب به مشتری های معین که ممکن است به پیشنهادی خاص در زمانی خاص علاقه مند باشند.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_416e130b744a7becbe84de8e251135bb'] = 'تماس با یک گروه مشتری ها بوسیله ایمیل یا روزنامه.';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_8dc514f6da8c696c4a522efc145ad28a'] = 'تفسیم جنسیتی';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_63889cfb9d3cbe05d1bd2be5cc9953fd'] = 'مرد';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_b719ce180ec7bd9641fece2f920f4817'] = 'زن';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_88183b946cc5f0e8c96b2e66e1c74a7e'] = 'نامشخص';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_2d0f27acbb474dff9aba76851970618a'] = 'رده سنی';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_6e69fbf88d84db874f365542b0284a95'] = '0-18 ساله';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_b20e0ed6158978a3a23d092060b5dbab'] = '18-24 ساله';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_e5884ca49180d38295ee426c624d936c'] = '25-34 ساله';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_62ce9f61153b331eabe9efc2fc7eb5c2'] = '35-49 ساله';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_a7724f78dcdf2179c5ca651d15ed5b2c'] = '50-59 ساله';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_6a239f88f1aeb0561e7786b6120d1d5e'] = '60 ساله و بیشتر';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_73b0130037e21b76351aebfd29d0b9aa'] = 'تقسیم کشوری';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_28e81c8343702f6c813cc31a7f90616a'] = 'تقسیم واحد پولی';
$_MODULE['<{statspersonalinfos}leocame>statspersonalinfos_f1dd68fb6a00d3e4d7f751deacde995d'] = 'تقسیم زبانی';
