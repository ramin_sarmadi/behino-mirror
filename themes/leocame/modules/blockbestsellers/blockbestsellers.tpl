<div id="best-sellers_block_right" class="block gray products_block span6">
    <div class="product-sellers">
        <h3 class="title_block"><a
                    href="{$link->getPageLink('best-sales')}">{l s='Top sellers' mod='blockbestsellers'}</a></h3>

        <div class="block_content">
            {if $best_sellers|@count > 0}
                <ul class="product_images">
                    {foreach from=$best_sellers item=product name=myLoop}
                        <li class="best-item{$smarty.foreach.myLoop.iteration}">
                            <a href="{$product.link}" title="{$product.legend|escape:'htmlall':'UTF-8'}"
                               class="content_img clearfix">
                                <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_allinmart')}"
                                     alt="{$product.legend|escape:'htmlall':'UTF-8'}"/>
                            </a>
                            <h4 class="s_title_block">
                                <a href="{$product.link}"
                                   title="{$product.legend|escape:'htmlall':'UTF-8'}">
                                    {$product.name|strip_tags:'UTF-8'|escape:'htmlall':'UTF-8'}</a></h4>
                            {*<p class="product_desc"><a href="{$product.link}" title="{l s='More' mod='homefeatured'}">{$product.description_short|strip_tags|truncate:65:'...'}</a></p>*}
                            <p><span class="price">{$product.price}</span></p>
                        </li>
                    {/foreach}
                </ul>

            {/if}
        </div>
    </div>
</div>