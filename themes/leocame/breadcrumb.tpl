<!-- Breadcrumb -->
{if $page_name != index}
    {if isset($smarty.capture.path)}{assign var='path' value=$smarty.capture.path}{/if}
    <div class="container">
        <div id="breadcrumb">
            <ul class="breadcrumb">
                <li id="first-bread">
                    <a href="{$base_dir}" title="{l s='return to Home'}">{l s='Home'}</a>
                    {if isset($path) AND $path}
                        <span class="divider"
                              {if isset($category) && $category->id_category == 1}style="display:none;"{/if}>{$navigationPipe|escape:html:'UTF-8'}</span>
                    {/if}
                </li>
                {if isset($path) AND $path}
                    {if !$path|strpos:'span'}
                        <li>{$path}</li>
                    {else}
                        <li class="active">{$path}</li>
                    {/if}
                {/if}
            </ul>
        </div>

    </div>
{/if}
<!-- /Breadcrumb -->
