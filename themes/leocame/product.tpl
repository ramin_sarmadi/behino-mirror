<link rel="stylesheet" type="text/css" href="{$css_dir}owl.theme.css"/>
<link rel="stylesheet" type="text/css" href="{$css_dir}owl.carousel.css"/>
{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
    <link type="text/css" rel="stylesheet" href="{$css_dir}simple-slider.css">
    <script type="text/javascript" src="{$js_dir}jquery.carouFredSel-6.2.1-packed.js"></script>
    <script type="text/javascript" src="{$js_dir}simple-slider.min.js"></script>
    <script type="text/javascript">
        // <![CDATA[
        // PrestaShop internal settings
        var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
        var currencyRate = '{$currencyRate|floatval}';
        var currencyFormat = '{$currencyFormat|intval}';
        var currencyBlank = '{$currencyBlank|intval}';
        var taxRate = {$tax_rate|floatval};
        var jqZoomEnabled = {if $jqZoomEnabled}true{else}false{/if};
        //JS Hook
        var oosHookJsCodeFunctions = new Array();
        // Parameters
        var id_product = '{$product->id|intval}';
        var productHasAttributes = {if isset($groups)}true{else}false{/if};
        var quantitiesDisplayAllowed = {if $display_qties == 1}true{else}false{/if};
        var quantityAvailable = {if $display_qties == 1 && $product->quantity}{$product->quantity}{else}0{/if};
        var allowBuyWhenOutOfStock = {if $allow_oosp == 1}true{else}false{/if};
        var availableNowValue = '{$product->available_now|escape:'quotes':'UTF-8'}';
        var availableLaterValue = '{$product->available_later|escape:'quotes':'UTF-8'}';
        var productPriceTaxExcluded = {$product->getPriceWithoutReduct(true)|default:'null'} - {$product->ecotax};
        var productBasePriceTaxExcluded = {$product->base_price} - {$product->ecotax};
        var reduction_percent = {if $product->specificPrice AND $product->specificPrice.reduction AND $product->specificPrice.reduction_type == 'percentage'}{$product->specificPrice.reduction*100}{else}0{/if};
        var reduction_price = {if $product->specificPrice AND $product->specificPrice.reduction AND $product->specificPrice.reduction_type == 'amount'}{$product->specificPrice.reduction|floatval}{else}0{/if};
        var specific_price = {if $product->specificPrice AND $product->specificPrice.price}{$product->specificPrice.price}{else}0{/if};
        var product_specific_price = new Array();
        {foreach from=$product->specificPrice key='key_specific_price' item='specific_price_value'}
        product_specific_price['{$key_specific_price}'] = '{$specific_price_value}';
        {/foreach}
        var specific_currency = {if $product->specificPrice AND $product->specificPrice.id_currency}true{else}false{/if};
        var group_reduction = '{$group_reduction}';
        var default_eco_tax = {$product->ecotax};
        var ecotaxTax_rate = {$ecotaxTax_rate};
        var currentDate = '{$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}';
        var maxQuantityToAllowDisplayOfLastQuantityMessage = {$last_qties};
        var noTaxForThisProduct = {if $no_tax == 1}true{else}false{/if};
        var displayPrice = {$priceDisplay};
        var productReference = '{$product->reference|escape:'htmlall':'UTF-8'}';
        var productAvailableForOrder = {if (isset($restricted_country_mode) AND $restricted_country_mode) OR $PS_CATALOG_MODE}'0'
        {else}'{$product->available_for_order}'{/if};
        var productShowPrice = '{if !$PS_CATALOG_MODE}{$product->show_price}{else}0{/if}';
        var productUnitPriceRatio = '{$product->unit_price_ratio}';
        var idDefaultImage = {if isset($cover.id_image_only)}{$cover.id_image_only}{else}0{/if};
        var stock_management = {$stock_management|intval};
        {if !isset($priceDisplayPrecision)}
        {assign var='priceDisplayPrecision' value=2}
        {/if}
        {if !$priceDisplay || $priceDisplay == 2}
        {assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL, $priceDisplayPrecision)}
        {assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
        {elseif $priceDisplay == 1}
        {assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL, $priceDisplayPrecision)}
        {assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
        {/if}
        var productPriceWithoutReduction = '{$productPriceWithoutReduction}';
        var productPrice = '{$productPrice}';
        // Customizable field
        var img_ps_dir = '{$img_ps_dir}';
        var customizationFields = new Array();
        {assign var='imgIndex' value=0}
        {assign var='textFieldIndex' value=0}
        {foreach from=$customizationFields item='field' name='customizationFields'}
        {assign var="key" value="pictures_`$product->id`_`$field.id_customization_field`"}
        customizationFields[{$smarty.foreach.customizationFields.index|intval}] = new Array();
        customizationFields[{$smarty.foreach.customizationFields.index|intval}][0] = '{if $field.type|intval == 0}img{$imgIndex++}{else}textField{$textFieldIndex++}{/if}';
        customizationFields[{$smarty.foreach.customizationFields.index|intval}][1] = {if $field.type|intval == 0 && isset($pictures.$key) && $pictures.$key}2{else}{$field.required|intval}{/if};
        {/foreach}

        // Images
        var img_prod_dir = '{$img_prod_dir}';
        var combinationImages = new Array();
        {if isset($combinationImages)}
        {foreach from=$combinationImages item='combination' key='combinationId' name='f_combinationImages'}
        combinationImages[{$combinationId}] = new Array();
        {foreach from=$combination item='image' name='f_combinationImage'}
        combinationImages[{$combinationId}][{$smarty.foreach.f_combinationImage.index}] = {$image.id_image|intval};
        {/foreach}
        {/foreach}
        {/if}
        combinationImages[0] = new Array();
        {if isset($images)}
        {foreach from=$images item='image' name='f_defaultImages'}
        combinationImages[0][{$smarty.foreach.f_defaultImages.index}] = {$image.id_image};
        {/foreach}
        {/if}

        // Translations
        var doesntExist = '{l s='This combination does not exist for this product. Please select another combination.' js=1}';
        var doesntExistNoMore = '{l s='This product is no longer in stock' js=1}';
        var doesntExistNoMoreBut = '{l s='with those attributes but is available with others.' js=1}';
        var uploading_in_progress = '{l s='Uploading in progress, please be patient.' js=1}';
        var fieldRequired = '{l s='Please fill in all the required fields before saving your customization.' js=1}';
        {if isset($groups)}
        // Combinations
        {foreach from=$combinations key=idCombination item=combination}
        var specific_price_combination = new Array();
        var available_date = new Array();
        specific_price_combination['reduction_percent'] = {if $combination.specific_price AND $combination.specific_price.reduction AND $combination.specific_price.reduction_type == 'percentage'}{$combination.specific_price.reduction*100}{else}0{/if};
        specific_price_combination['reduction_price'] = {if $combination.specific_price AND $combination.specific_price.reduction AND $combination.specific_price.reduction_type == 'amount'}{$combination.specific_price.reduction}{else}0{/if};
        specific_price_combination['price'] = {if $combination.specific_price AND $combination.specific_price.price}{$combination.specific_price.price}{else}0{/if};
        specific_price_combination['reduction_type'] = '{if $combination.specific_price}{$combination.specific_price.reduction_type}{/if}';
        specific_price_combination['id_product_attribute'] = {if $combination.specific_price}{$combination.specific_price.id_product_attribute|intval}{else}0{/if};
        available_date['date'] = '{$combination.available_date}';
        available_date['date_formatted'] = '{dateFormat date=$combination.available_date full=false}';
        addCombination({$idCombination|intval}, new Array({$combination.list}), {$combination.quantity}, {$combination.price}, {$combination.ecotax}, {$combination.id_image}, '{$combination.reference|addslashes}', {$combination.unit_impact}, {$combination.minimal_quantity}, available_date, specific_price_combination);
        {/foreach}
        {/if}

        {if isset($attributesCombinations)}
        // Combinations attributes informations
        var attributesCombinations = new Array();
        {foreach from=$attributesCombinations key=id item=aC}
        tabInfos = new Array();
        tabInfos['id_attribute'] = '{$aC.id_attribute|intval}';
        tabInfos['attribute'] = '{$aC.attribute}';
        tabInfos['group'] = '{$aC.group}';
        tabInfos['id_attribute_group'] = '{$aC.id_attribute_group|intval}';
        attributesCombinations.push(tabInfos);
        {/foreach}
        {/if}
        var id_product_simular = {$product->id|intval};
        //]]>
    </script>
    {if isset($adminActionDisplay) && $adminActionDisplay}
        <div id="admin-action">
            <p>{l s='This product is not visible to your customers.'}
                <input type="hidden" id="admin-action-product-id" value="{$product->id}"/>
                <input type="submit" value="{l s='Publish'}" class="exclusive"
                       onclick="submitPublishProduct('{$base_dir}{$smarty.get.ad|escape:'htmlall':'UTF-8'}', 0, '{$smarty.get.adtoken|escape:'htmlall':'UTF-8'}')"/>
                <input type="submit" value="{l s='Back'}" class="exclusive"
                       onclick="submitPublishProduct('{$base_dir}{$smarty.get.ad|escape:'htmlall':'UTF-8'}', 1, '{$smarty.get.adtoken|escape:'htmlall':'UTF-8'}')"/>
            </p>

            <p id="admin-action-result"></p>
        </div>
    {/if}
    {if isset($confirmation) && $confirmation}
        <p class="confirmation">
            {$confirmation}
        </p>
    {/if}
    <div class="container">
    <div class="row-fluid">
    <div class="span4 visible-phone visible-tablet picture-tablet hidden-desktop" >
        {*{$images|print_r}*}
        <div class="row-fluid">
            {foreach from=$images item=image name=thumbnails}
                {if $image['cover'] == 1}
                    <div>
                        {assign var=imageIds value="`$product->id`-`$image.id_image`"}
                        <img src="{$link->getImageLink($product->link_rewrite, $imageIds, 'tabs_allinmart')}"
                             id="bigpic2"/>
                    </div>
                {/if}
            {/foreach}
        </div>

    </div>
    <div class="span3" style="margin-left: 0;">
        <div class="top-feature row-fluid">
            <span id="memory" class="top-icon"></span>
            <span class="feature-name">{$TopFeature[0]['name']}:</span>
            <span class="top-feature-value">{$TopFeature[0]['value']}</span>
        </div>
        <div class="top-feature row-fluid">
            <span id="sim" class="top-icon"></span>
            <span class="feature-name">{$TopFeature[1]['name']}:</span>
            <span class="top-feature-value">{$TopFeature[1]['value']|truncate:15}</span>
        </div>
        <div class="top-feature row-fluid">
            <span id="cpu" class="top-icon"></span>
            <span class="feature-name">{$TopFeature[3]['name']}:</span>
            <span class="top-feature-value">{$TopFeature[3]['value_num_1']} {$TopFeature[3]['value']}</span>
        </div>
        <div class="top-feature row-fluid">
            <span id="display" class="top-icon"></span>
            <span class="feature-name">{$TopFeature[4]['name']}:</span>
            <span class="top-feature-value">{$TopFeature[4]['value_num_1']} {$TopFeature[4]['value']}</span>
        </div>
        {if isset($TopFeature[5]['value_num_1'])}
        <div class="top-feature row-fluid">
            <span id="camera" class="top-icon"></span>
            <span class="feature-name">{$TopFeature[5]['name']}:</span>
            <span class="top-feature-value">{$TopFeature[5]['value_num_1']} {$TopFeature[5]['value']}</span>
        </div>
        {/if}
        <div class="row-fluid" id="product_comments_block_extra">
            <span class="rate-icon"></span>

            <div class="rate-txt-num">
                <span class="rate-text">{l s='امتیاز کاربران:'}</span>
                <span class="rate-num">{$averageTotal|string_format:"%.2f"}</span>
                <span class="from">{l s='(از'}</span>
                <span class="tota-count-rate">{$TotalCountRate}</span>
                <span class="ray">{l s='رای)'}</span>
            </div>
            <div class="comments_note">
                <div class="star_content clearfix">
                    {section name="i" start=0 loop=5 step=1}
                        {if $averageTotal le $smarty.section.i.index}
                            <div class="star"></div>
                        {else}
                            <div class="star star_on"
                                 title="{l s='برای ثبت رای خود کلیک کنید'}"></div>
                        {/if}
                    {/section}
                </div>
                <span class="you-can-rate">{l s='شما هم امتیاز بدهید'}</span>
            </div>
        </div>
        {if isset($HOOK_EXTRA_RIGHT) && $HOOK_EXTRA_RIGHT && $lofdeal_product.js.hour > 0}
            <div class="row-fluid">
                <div class="span12">
                    <div class="top-feature price-counter">
                        <span class="counter-icon"></span>
                        {$HOOK_EXTRA_RIGHT}
                    </div>
                </div>
            </div>
        {/if}
    </div>
    <div class="span5 cart-tablet">
    <div class="product-price-name row-fluid">
        <div class="span12">
            <p class="product-en-name">{$product->name}</p>

            <p class="product-fa-name">{$product->description_short|strip_tags}</p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="share-item">
            <form action="#" id="frmLike">
                <input id="comment_like" name="Valuelike" type="hidden" value="1"/>
                <input id="id_product_comment_send" name="id_product" type="hidden"
                       value='{$id_product_comment_form}'>
                <button class="like-btn {if $product->customer_like==0}like{else}liked{/if}" id="submitNewLike"
                        name="submitLike" type="submit">
                    <span class="like-count">{$product->count_like}</span>
                    <span class="like-status {if $product->customer_like==0}like-hand{else}like-tik{/if}"></span>
                    <span class="like-pipe"></span>
                    <span class="like-text">{l s='می پسندم'}</span>
                </button>
            </form>
        </div>
        {if $HOOK_PRODUCT_OOS}
            <div class="share-item">
                {$HOOK_PRODUCT_OOS}
            </div>
        {/if}
        <div class="share-item favorite-mobile">
            <ul id="usefull_link_block">
                {$HOOK_EXTRA_LEFT}
            </ul>
        </div>
    </div>
    <div class="row-fluid availabe-row">
        <div id="availability_statut">
            <span id="availability_label">{l s='Availability:'}</span>
            <span id="availability_value"{if $product->quantity <= 0} class="warning_inline"{/if}>
             {if $product->quantity > 0}
                 {l s='Available'}
             {else}
                 {l s='Unavailable'}
             {/if}
            </span>
        </div>
    </div>
    <div class="row-fluid attrib-row">
        <div class="span12">
            <div class="product_attributes">
                {if isset($groups)}
                    <!-- attributes -->
                    <div id="attributes">
                        {*     <div id="availability_statut2">
                                 <span id="availability_label">{l s='نوع فروش:'}</span>
                                         <span id="availability_value"{if $product->quantity <= 0} class="warning_inline"{/if}>
                                             {$product->condition}

                                              </span>
                             </div>*}

                        {foreach from=$groups key=id_attribute_group item=group}
                            {if $group.attributes|@count}
                                <fieldset class="attribute_fieldset">
                                    <label class="attribute_label"
                                           for="group_{$id_attribute_group|intval}">{$group.name|escape:'htmlall':'UTF-8'}
                                        :&nbsp;</label>

                                    {assign var="groupName" value="group_$id_attribute_group"}
                                    <div class="attribute_list attribute-pull-right">
                                        {if ($group.group_type == 'select')}
                                            <select name="{$groupName}" id="group_{$id_attribute_group|intval}"
                                                    class="attribute_select"
                                                    onchange="findCombination();getProductAttribute();">
                                                {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                    <option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if}
                                                            title="{$group_attribute|escape:'htmlall':'UTF-8'}">{$group_attribute|escape:'htmlall':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        {elseif ($group.group_type == 'color')}
                                            <ul id="color_to_pick_list" class="clearfix">
                                                {assign var="default_colorpicker" value=""}
                                                {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                    <li{if $group.default == $id_attribute} class="selected"{/if}>
                                                        <a id="color_{$id_attribute|intval}"
                                                           class="color_pick{if ($group.default == $id_attribute)} selected{/if}"
                                                           style="background: {$colors.$id_attribute.value};"
                                                           title="{$colors.$id_attribute.name}"
                                                           onclick="colorPickerClick(this);getProductAttribute();">
                                                            {if file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
                                                                <img src="{$img_col_dir}{$id_attribute}.jpg"
                                                                     alt="{$colors.$id_attribute.name}" width="20"
                                                                     height="20"/>
                                                                <br/>
                                                            {/if}
                                                        </a>
                                                    </li>
                                                {*<label>{$colors.$id_attribute.name}</label>*}

                                                    {if ($group.default == $id_attribute)}
                                                        {$default_colorpicker = $id_attribute}
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                            <input type="hidden" class="color_pick_hidden" name="{$groupName}"
                                                   value="{$default_colorpicker}"/>
                                        {elseif ($group.group_type == 'radio')}
                                            <ul>
                                                {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                    <li>
                                                        <input type="radio" class="attribute_radio" name="{$groupName}"
                                                               value="{$id_attribute}" {if ($group.default == $id_attribute)} checked="checked"{/if}
                                                               onclick="findCombination();getProductAttribute();"/>
                                                        <span>{$group_attribute|escape:'htmlall':'UTF-8'}</span>
                                                    </li>
                                                {/foreach}
                                            </ul>
                                        {/if}
                                    </div>
                                </fieldset>
                            {/if}
                        {/foreach}
                    </div>
                {/if}
                <!-- availability -->
                <p id="availability_date"{if ($product->quantity > 0) OR !$product->available_for_order OR $PS_CATALOG_MODE OR !isset($product->available_date) OR $product->available_date < $smarty.now|date_format:'%Y-%m-%d'} style="display: none;"{/if}>
                    <span id="availability_date_label">{l s='Availability date:'}</span>
                    <span id="availability_date_value">{dateFormat date=$product->available_date full=false}</span>
                </p>
                <!-- number of item in stock
				{if ($display_qties == 1 && !$PS_CATALOG_MODE && $product->available_for_order)}
				<p id="pQuantityAvailable" class="bg-stock" {if $product->quantity <= 0} style="display: none;"{/if}>
					<span id="quantityAvailable">{$product->quantity|intval}</span>
					<span {if $product->quantity > 1} style="display: none;"{/if} id="quantityAvailableTxt">{l s='item in stock'}</span>
					<span {if $product->quantity == 1} style="display: none;"{/if} id="quantityAvailableTxtMultiple">{l s='items in stock'}</span>
				</p>
				{/if}

				<p id="product_reference" {if isset($groups) OR !$product->reference}style="display: none;"{/if}>
					<label for="product_reference">{l s='Reference:'} </label>
					<span class="editable">{$product->reference|escape:'htmlall':'UTF-8'}</span>
				</p>
-->                                    <!-- quantity wanted -->
                <!-- minimal quantity wanted -->
                <p id="minimal_quantity_wanted_p"{if $product->minimal_quantity <= 1 OR !$product->available_for_order OR $PS_CATALOG_MODE} style="display: none;"{/if}>
                    {l s='This product is not sold individually. You must select at least'} <b
                            id="minimal_quantity_label">{$product->minimal_quantity}</b> {l s='quantity for this product.'}
                </p>
                {if $product->minimal_quantity > 1}
                    <script type="text/javascript">
                        checkMinimalQuantity();
                    </script>
                {/if}
                <!-- Out of stock hook iBartar -->
                {*<p class="warning_inline"*}
                {*id="last_quantities"{if ($product->quantity > $last_qties OR $product->quantity <= 0) OR $allow_oosp OR !$product->available_for_order OR $PS_CATALOG_MODE} style="display: none"{/if} >*}
                {*{l s='Warning: Last items in stock!'}</p>*}
            </div>
        </div>
    </div>
    <hr class="product-hr">
    <div class="row-fluid">
        <div class="span6">
            {if $product->specificPrice}
                <div class="leo-product-suffix" style="margin-bottom: 15px;">
                    <p id="quantity_wanted_p"{if (!$allow_oosp && $product->quantity <= 0) OR $virtual OR !$product->available_for_order OR $PS_CATALOG_MODE} style="display: none;"{/if}>
                        <label>{l s='Quantity:'}</label>
                        <input type="text" name="qty" id="quantity_wanted" class="text"
                               value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}"
                               size="2" maxlength="3"
                               {if $product->minimal_quantity > 1}onkeyup="checkMinimalQuantity({$product->minimal_quantity});"{/if} />
                    </p>

                    <div class="content_prices clearfix">
                        <!-- prices -->
                        <span class="takhfif">{l s='تخفیف:'}</span>
                        <span id="darsad">
                            {if $product->specificPrice AND $product->specificPrice.reduction_type == 'percentage'}
                                {$product->specificPrice.reduction * $productPriceWithoutReduction}
                            {/if}
                            {if $product->specificPrice AND $product->specificPrice.reduction_type == 'amount'}
                                {$product->specificPrice.reduction|abs}
                            {/if}
                            {l s='تومان'}
                        </span>
                        {*<span id="">*}
                        {*{if $product->specificPrice AND $product->specificPrice.reduction_type == 'amount' AND $product->specificPrice.reduction|intval !=0}*}
                        {*-{convertPrice price=$productPriceWithoutReduction-$productPrice|floatval}*}
                        {*{/if}*}
                        {*</span>*}

                        {if $product->online_only}
                            <p class="online_only">{l s='Online only'}</p>
                        {/if}
                        {if $product->specificPrice AND $product->specificPrice.reduction && $product->specificPrice.reduction > 0}
                            {if $priceDisplay >= 0 && $priceDisplay <= 2}
                                {if $productPriceWithoutReduction > $productPrice}

                                    {if $tax_enabled && $display_tax_label == 1}
                                        {if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}
                                    {/if}
                                {/if}
                            {/if}


                        {/if}

                        {if $packItems|@count && $productPrice < $product->getNoPackPrice()}
                            <p class="pack_price">{l s='instead of'} <span
                                        style="text-decoration: line-through;">{convertPrice price=$product->getNoPackPrice()}</span>
                            </p>
                            <br class="clear"/>
                        {/if}
                        {if $product->ecotax != 0}
                            <p class="price-ecotax">{l s='include'} <span
                                        id="ecotax_price_display">{if $priceDisplay == 2}{$ecotax_tax_exc|convertAndFormatPrice}{else}{$ecotax_tax_inc|convertAndFormatPrice}{/if}</span> {l s='for green tax'}
                                {if $product->specificPrice AND $product->specificPrice.reduction}
                                    <br/>
                                    {l s='(not impacted by the discount)'}
                                {/if}
                            </p>
                        {/if}
                        {if !empty($product->unity) && $product->unit_price_ratio > 0.000000}
                            {math equation="pprice / punit_price"  pprice=$productPrice  punit_price=$product->unit_price_ratio assign=unit_price}
                            <p class="unit-price"><span
                                        id="unit_price_display">{convertPrice price=$unit_price}</span> {l s='per'} {$product->unity|escape:'htmlall':'UTF-8'}
                            </p>
                        {/if}
                        {*close if for show price*}
                    </div>
                </div>
            {/if}
        </div>
        <div class="span6">
            <span id="old_price_display">{convertPrice price=$productPriceWithoutReduction}</span>

            <div class="price">
                <p class="our_price_display">
                    {if $priceDisplay >= 0 && $priceDisplay <= 2 && $productPrice > 0}
                    {*<span class="price">{l s='Price'}:</span>*}
                        <span id="our_price_display">{convertPrice price=$productPrice}</span>
                    {* {if $tax_enabled  && ((isset($display_tax_label) && $display_tax_label == 1) OR !isset($display_tax_label))}
        {if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}
                      {/if}*}


                    {/if}
                </p>

                {if $product->on_sale}
                    <img src="{$img_dir}onsale_{$lang_iso}.gif" alt="{l s='On sale'}" class="on_sale_img"/>
                    <span class="on_sale">{l s='On sale!'}</span>
                {elseif $product->specificPrice AND $product->specificPrice.reduction AND $productPriceWithoutReduction > $productPrice}
                    <span class="discount">{l s='Reduced price!'}</span>
                {/if}
                {if $priceDisplay == 2}
                    <br/>
                    <span id="pretaxe_price"><span
                                id="pretaxe_price_display">{convertPrice price=$product->getPrice(false, $smarty.const.NULL)}</span>&nbsp;{l s='tax excl.'}</span>
                {/if}
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="buttons_bottom_block" id="add_to_cart">
            <a class="cart-icon">
                <img class="cart-img" src="{$img_dir}add-cart-icon.png">
            </a>
            <input type="submit" name="Submit" value="{l s='Add to cart'}" id="add-to-card"
                   class="exclusive"/>
        </div>
        <span class="or">{l s="یا"}</span>
        <a href="{$base_dir}index.php?controller=products-comparison&form=1&action=add&id_product={$id_product}"
           id="compareTo-form"
           target="_blank">
            {l s='مقایسه با ...'}
        </a>
    </div>
    <form id="buy_block" {if $PS_CATALOG_MODE AND !isset($groups) AND $product->quantity > 0}class="hidden"{/if}
          action="{$link->getPageLink('cart')}" method="post">
        <!-- hidden datas -->
        <p class="hidden">
            <input type="hidden" name="token" value="{$static_token}"/>
            <input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id"/>
            <input type="hidden" name="add" value="1"/>
            <input type="hidden" name="id_product_attribute" id="idCombination" value=""/>
        </p>


        {if (isset($quantity_discounts) && count($quantity_discounts) > 0)}
            <!-- quantity discount -->
            <ul class="idTabs clearfix">
                <li><a href="#discount" style="cursor: pointer" class="selected">{l s='Quantity discount'}</a></li>
            </ul>
            <div id="quantityDiscount">
                <table class="std">
                    <thead>
                    <tr>
                        <th>{l s='product'}</th>
                        <th>{l s='from (qty)'}</th>
                        <th>{l s='discount'}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
                        <tr id="quantityDiscount_{$quantity_discount.id_product_attribute}">
                            <td>
                                {if (isset($quantity_discount.attributes) && ($quantity_discount.attributes))}
                                    {$product->getProductName($quantity_discount.id_product, $quantity_discount.id_product_attribute)}
                                {else}
                                    {$product->getProductName($quantity_discount.id_product)}
                                {/if}
                            </td>
                            <td>{$quantity_discount.quantity|intval}</td>
                            <td>
                                {if $quantity_discount.price >= 0 OR $quantity_discount.reduction_type == 'amount'}
                                    -{convertPrice price=$quantity_discount.real_value|floatval}
                                {else}
                                    -{$quantity_discount.real_value|floatval}%
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        {/if}
    </form>
    </div>
    <div class="span4 hidden-phone hidden-tablet visible-desktop">
        {*{$images|print_r}*}
        <div class="row-fluid">
            {foreach from=$images item=image name=thumbnails}
                {if $image['cover'] == 1}
                    <div>
                        {assign var=imageIds value="`$product->id`-`$image.id_image`"}
                        <img src="{$link->getImageLink($product->link_rewrite, $imageIds, 'tabs_allinmart')}"
                             id="bigpic"/>
                    </div>
                {/if}
            {/foreach}
        </div>
        <div class="row-fluid hidden-tablet hidden-phone">
            <a class="thumb-left"></a>
            {if count($images) > 0}
            {*<div id="myCarousel" class="carousel slide">*}
            {*<div class="carousel-inner">*}
                <div class="thumb-carousel">
                    {foreach from=$images item=image name=thumbnails}
                        <div class="thumbnail-item">
                            {assign var=imageIds value="`$product->id`-`$image.id_image`"}
                            <a rel="group" class="thumb-pic"
                               href="{$link->getImageLink($product->link_rewrite, $imageIds, 'large_allinmart')}">
                                <img src="{$link->getImageLink($product->link_rewrite, $imageIds, 'medium_allinmart')}"/>
                            </a>
                        </div>
                    {/foreach}
                </div>
            {*</div>*}
            {*</div>*}


            {/if}
            <a class="thumb-right"></a>
        </div>
    </div>
    </div>
    </div>
    {$HOOK_PRODUCT_ACTIONS}
    <div class="container">
        {if (isset($product) && ($product->description) && ($product->description|strlen >100))}
            <div class="row-fluid">
                <div class="span12">
                    <div id="description">
                        <div id="more_info_block" class="clear">
                            <div class="row-fluid box-row">
                                <div class="span12">
                                    <div class="product-box-title">
                                        <img id="in-one-eye" src="{$img_dir}product-icons/eye.png"/>
                                        <span class="prod-title-text">{l s="در یک نگاه"}</span>
                                    </div>
                                </div>
                            </div>
                            <div id="more_info_sheets" class="sheets align_justify row-fluid">
                                <div id="idTab1" class="rte">{$product->description}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
        <div class="row-fluid">
            <div class="span12">
                <div class="row-fluid box-row">
                    <div class="span12">
                        <ul class="nav nav-tabs similar-tabs">
                            <li id="similar-tab1" class="sim-tab-item active"><a href="#similiarGo" data-toggle="tab">{l s='محصولات مشابه'}</a>
                            </li>
                            <li id="similar-tab2" class="sim-tab-item"><a href="#same-priceGo" data-toggle="tab">{l s='محصولات هم قیمت'}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="similiarGo">
                                <div id="similiar">
                                <img class="loading-similar" src="{$img_dir}loading.gif">
                                </div>
                            </div>
                            <div class="tab-pane" id="same-priceGo">
                                <div id="same-price">
                                <img class="loading-similar" src="{$img_dir}loading.gif">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {if isset($HOOK_PRODUCT_TAB_CONTENT) && $HOOK_PRODUCT_TAB_CONTENT}
            {$HOOK_PRODUCT_TAB_CONTENT}
        {/if}
        <div class="row-fluid">
            {if isset($product) && $product->description_AdminReview && ($product->description_AdminReview|strlen >100)}
                <div id="feature">
                    <div id="more_info_block" class="clear">
                        <div class="row-fluid box-row">
                            <div class="span12">
                                <div class="product-box-title">
                                    <img id="review-icon" src="{$img_dir}product-icons/naghd.png"/>
                                    <span class="prod-title-text">{l s="نقد و بررسی"}</span>
                                </div>
                            </div>
                        </div>
                        <div id="more_info_sheets" class="sheets align_justify">
                            <div id="idTab11" class="rte">{$product->description_AdminReview}</div>
                        </div>
                    </div>
                </div>
            {/if}
        </div>
        <div id="more_info_block">
            <div class="span6">
                {if isset($HOOK_PRODUCT_FOOTER) && $HOOK_PRODUCT_FOOTER}{$HOOK_PRODUCT_FOOTER}{/if}
            </div>
            <div class="span6">
                <div class="row-fluid box-row">
                    <div class="product-box-title">
                        <img id="review-icon" src="{$img_dir}product-icons/emtiaz.png"/>
                        <span class="prod-title-text">{l s="ثبت امتیاز"}</span>
                    </div>
                    <div id="more_info_sheets" class="sheets align_justify">
                        <div id="idTab12" class="rte">
                            <form action="#" id="RateGrade">
                                <div class="new_comment_form_content">
                                    <div id="new_comment_form_error" class="error" style="display: none;">
                                        <ul></ul>
                                    </div>
                                    {if $criterions|@count > 0}
                                        <ul id="criterions_list">
                                            {foreach from=$criterions item='criterion'}
                                                <label>
                                                    {$criterion.name|escape:'html':'UTF-8'}
                                                </label>
                                                <input
                                                        id="output-data"
                                                        readonly="readonly"
                                                        data-slider-highlight="true"
                                                        data-slider="true"
                                                        data-slider-values="0,1,2,3,4,5"
                                                        data-slider-snap="true"
                                                        data-slider-step="1"
                                                        data-slider-range="0,5"
                                                        data-slider-highlight="true"
                                                        class="ratevalue"
                                                        type="text"
                                                        name="criterion[{$criterion.id_product_comment_criterion|round}]"
                                                        value="3"
                                                        />
                                                <div class="clearfix"></div>
                                            {/foreach}
                                        </ul>
                                    {/if}
                                    <input id="comment_title" name="title" type="hidden" value=""/>
                                    <input id="content" name="content" value="" type="hidden"/>
                                    {if $allow_guests == true && $logged == 0}
                                        <input id="commentCustomerName" name="customer_name" type="hidden" value=""/>
                                    {/if}
                                    <input id="id_product_comment_send" name="id_product" type="hidden"
                                           value='{$id_product_comment_form}'/>
                                    <button class="btn btn-large" id="submitNewGrade" name="submitGrade"
                                            type="submit">{l s='SaveRate'}</button>
                                    <span id="user-rate-success"></span>
                                </div>
                            </form>
                            <!-- Grade iBartar -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {if isset($attachments) && $attachments}
        <div id="more_info_block" class="clear">
            <div id="more_info_sheets" class="sheets align_justify">
                <ul id="idTab9" class="bullet">
                    {foreach from=$attachments item=attachment}
                        <li>
                            <a href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")}">{$attachment.name|escape:'htmlall':'UTF-8'}</a><br/>{$attachment.description|escape:'htmlall':'UTF-8'}
                        </li>
                    {/foreach}
                </ul>
            </div>
        </div>
    {/if}
    {if isset($accessories) AND $accessories}
        <div id="more_info_block" class="clear">
            <div id="more_info_sheets" class="sheets align_justify">
                <!-- accessories -->
                <ul id="idTab4" class="bullet">
                    <div class="block products_block accessories_block clearfix">
                        <div class="block_content">
                            <ul>
                                {foreach from=$accessories item=accessory name=accessories_list}
                                    {if ($accessory.allow_oosp || $accessory.quantity_all_versions > 0 || $accessory.quantity > 0) AND $accessory.available_for_order AND !isset($restricted_country_mode)}
                                        {assign var='accessoryLink' value=$link->getProductLink($accessory.id_product, $accessory.link_rewrite, $accessory.category)}
                                        <li class="ajax_block_product{if $smarty.foreach.accessories_list.first} first_item{elseif $smarty.foreach.accessories_list.last} last_item{else} item{/if} product_accessories_description">
                                            <p class="s_title_block">
                                                <a href="{$accessoryLink|escape:'htmlall':'UTF-8'}">{$accessory.name|escape:'htmlall':'UTF-8'}</a>
                                                {if $accessory.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE} -
                                                    <span class="price">{if $priceDisplay != 1}{displayWtPrice p=$accessory.price}{else}{displayWtPrice p=$accessory.price_tax_exc}{/if}</span>{/if}
                                            </p>

                                            <div class="product_desc">
                                                <a href="{$accessoryLink|escape:'htmlall':'UTF-8'}"
                                                   title="{$accessory.legend|escape:'htmlall':'UTF-8'}"
                                                   class="product_image"><img
                                                            src="{$link->getImageLink($accessory.link_rewrite, $accessory.id_image, 'medium_allinmart')}"
                                                            alt="{$accessory.legend|escape:'htmlall':'UTF-8'}"
                                                            width="{$mediumSize.width}"
                                                            height="{$mediumSize.height}"/></a>

                                                <div class="block_description">
                                                    <a href="{$accessoryLink|escape:'htmlall':'UTF-8'}"
                                                       title="{l s='More'}"
                                                       class="product_description">{$accessory.description_short|strip_tags|truncate:400:'...'}</a>
                                                </div>
                                                <div class="clear_product_desc">&nbsp;</div>
                                            </div>
                                            <p class="clearfix" style="margin-top:5px">
                                                <a class="button" href="{$accessoryLink|escape:'htmlall':'UTF-8'}"
                                                   title="{l s='View'}">{l s='View'}</a>
                                                {if !$PS_CATALOG_MODE && ($accessory.allow_oosp || $accessory.quantity > 0)}
                                                    <a class="exclusive button ajax_add_to_cart_button"
                                                       href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$accessory.id_product|intval}&amp;token={$static_token}&amp;add")}"
                                                       rel="ajax_id_product_{$accessory.id_product|intval}"
                                                       title="{l s='Add to cart'}">{l s='Add to cart'}</a>
                                                {/if}
                                            </p>
                                        </li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                </ul>
            </div>
        </div>
    {/if}
    {if isset($product) && $product->customizable}
        <div id="idTab10" class="bullet customization_block">
            <form method="post" action="{$customizationFormTarget}" enctype="multipart/form-data"
                  id="customizationForm" class="clearfix">
                <p class="infoCustomizable">
                    {l s='After saving your customized product, remember to add it to your cart.'}
                    {if $product->uploadable_files}<br/>{l s='Allowed file formats are: GIF, JPG, PNG'}{/if}
                </p>
                {if $product->uploadable_files|intval}
                    <div class="customizableProductsFile">
                        <h3>{l s='Pictures'}</h3>
                        <ul id="uploadable_files" class="clearfix">
                            {counter start=0 assign='customizationField'}
                            {foreach from=$customizationFields item='field' name='customizationFields'}
                                {if $field.type == 0}
                                    <li class="customizationUploadLine{if $field.required} required{/if}">{assign var='key' value='pictures_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
                                        {if isset($pictures.$key)}
                                            <div class="customizationUploadBrowse">
                                                <img src="{$pic_dir}{$pictures.$key}_small" alt=""/>
                                                <a href="{$link->getProductDeletePictureLink($product, $field.id_customization_field)}"
                                                   title="{l s='Delete'}">
                                                    <img src="{$img_dir}icon/delete.gif" alt="{l s='Delete'}"
                                                         class="customization_delete_icon" width="11" height="13"/>
                                                </a>
                                            </div>
                                        {/if}
                                        <div class="customizationUploadBrowse">
                                            <label class="customizationUploadBrowseDescription">{if !empty($field.name)}{$field.name}{else}{l s='Please select an image file from your hard drive'}{/if}{if $field.required}
                                                    <sup>*</sup>
                                                {/if}</label>
                                            <input type="file" name="file{$field.id_customization_field}"
                                                   id="img{$customizationField}"
                                                   class="customization_block_input {if isset($pictures.$key)}filled{/if}"/>
                                        </div>
                                    </li>
                                    {counter}
                                {/if}
                            {/foreach}
                        </ul>
                    </div>
                {/if}
                {if $product->text_fields|intval}
                    <div class="customizableProductsText">
                        <h3>{l s='Text'}</h3>
                        <ul id="text_fields">
                            {counter start=0 assign='customizationField'}
                            {foreach from=$customizationFields item='field' name='customizationFields'}
                                {if $field.type == 1}
                                    <li class="customizationUploadLine{if $field.required} required{/if}">
                                        <label for="textField{$customizationField}">{assign var='key' value='textFields_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field} {if !empty($field.name)}{$field.name}{/if}{if $field.required}
                                                <sup>*</sup>
                                            {/if}</label>
                                        <textarea type="text" name="textField{$field.id_customization_field}"
                                                  id="textField{$customizationField}" rows="1" cols="40"
                                                  class="customization_block_input">{if isset($textFields.$key)}{$textFields.$key|stripslashes}{/if}</textarea>
                                    </li>
                                    {counter}
                                {/if}
                            {/foreach}
                        </ul>
                    </div>
                {/if}
                <p id="customizedDatas">
                    <input type="hidden" name="quantityBackup" id="quantityBackup" value=""/>
                    <input type="hidden" name="submitCustomizedDatas" value="1"/>
                    <input type="button" class="button" value="{l s='Save'}"
                           onclick="javascript:saveCustomization()"/>
                        <span id="ajax-loader" style="display:none">
                            <img src="{$img_ps_dir}loader.gif"
                                 alt="loader"/></span>
                </p>
            </form>
            <p class="clear required"><sup>*</sup> {l s='required fields'}</p>
        </div>
    {/if}
    {if isset($packItems) && $packItems|@count > 0}
        <div id="blockpack">
            <h2>{l s='Pack content'}</h2>
            {include file="$tpl_dir./product-list.tpl" products=$packItems}
        </div>
    {/if}
{/if}
<script type="text/javascript" src="{$js_dir}owl.carousel.min.js"></script>