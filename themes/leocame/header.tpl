﻿<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$lang_iso}">
<head>
    <title>{$meta_title|escape:'htmlall':'UTF-8'}</title>
    <!--[if IE]>
    <script type="text/javascript" src="{$js_dir}html5shiv.js"></script>
    <![endif]-->
    {if isset($meta_description) AND $meta_description}
        <meta name="description" content="{$meta_description|escape:html:'UTF-8'}"/>
    {/if}
    {if isset($meta_keywords) AND $meta_keywords}
        <meta name="keywords" content="{$meta_keywords|escape:html:'UTF-8'}"/>
    {/if}
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/>
    <meta http-equiv="content-language" content="{$meta_language}"/>
    <meta name="generator" content="PrestaShop"/>
    <meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow"/>
    <!-- Mobile Specific Metas ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="icon" type="image/vnd.microsoft.icon" href="{$img_dir}favicon.ico"/>
    <link rel="shortcut icon" type="image/x-icon" href="{$img_dir}favicon.ico"/>
    <script type="text/javascript">
        var baseDir = '{$content_dir}';
        var baseUri = '{$base_uri}';
        var static_token = '{$static_token}';
        var token = '{$token}';
        var priceDisplayPrecision = {$priceDisplayPrecision*$currency->decimals};
        var priceDisplayMethod = {$priceDisplay};
        var roundMode = {$roundMode};
    </script>
    <link rel="stylesheet" type="text/css" href="{$BOOTSTRAP_CSS_URI}"/>
    {if isset($css_files)}
        {foreach from=$css_files key=css_uri item=media}
            <link href="{$css_uri}" rel="stylesheet" type="text/css"/>
        {/foreach}
    {/if}
    <!--[if IE 8]>
    <!--<link href="{$css_dir}ie8.css" rel="stylesheet" type="text/css"/>-->
    <![endif]-->
    {if $LEO_SKIN_DEFAULT &&  $LEO_SKIN_DEFAULT !="default"}
        <link rel="stylesheet" type="text/css"
              href="{$content_dir}themes/{$LEO_THEMENAME}/skins/{$LEO_SKIN_DEFAULT}/css/skin.css"/>
    {/if}

    {$LEO_CUSTOMWIDTH}
    <link rel="stylesheet" type="text/css" href="{$content_dir}themes/{$LEO_THEMENAME}/css/theme-responsive.css"/>
    <link rel="stylesheet" type="text/css" href="{$BOOTSTRAP_RESPONSIVECSS_URI}"/>
    {if isset($js_files)}
        {foreach from=$js_files item=js_uri}
            <script type="text/javascript" src="{$js_uri}"></script>
        {/foreach}
    {/if}
    {*{if !$LEO_CUSTOMFONT}
        <link href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto+Condensed' rel='stylesheet' type='text/css'>
    {/if}*}

    {if $hide_left_column||!in_array($page_name,array('index','cms'))}{$HOOK_LEFT_COLUMN=null}{/if}
    {if $hide_right_column||in_array($page_name,array('checkout','order','address'))}{$HOOK_RIGHT_COLUMN=null}{/if}


    {$HOOK_HEADER}
</head>
<body {if isset($page_name)} id="{$page_name|escape:'htmlall':'UTF-8'}"{/if} class="{$LEO_BGPATTERN} fs{$FONT_SIZE}">
{if !$content_only}
{if isset($restricted_country_mode) && $restricted_country_mode}
    <div id="restricted-country">
        <p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
    </div>
{/if}
{*{if $page_name == 'index'}*}
    {*<div class="container">*}
        {*<div id="notify-bar">*}
        {*<h4>{l s='تمامی کالاها و خدمات این فروشگاه، حسب مورد دارای مجوزهای لازم از مراجع مربوطه می باشند و فعالیت های این سایت تابع قوانین و مقررات جمهوری اسلامی ایران است.'}</h4>*}


{*<h4>{l s='توجه: در حال حاضر نسخه آزمایشی وب سایت آلین مارت در حال بهره برداری بوده و ممکن است بخشی از اطلاعات به روز نباشد و ایراداتی نیز در عملکرد وب سایت مشاهده شود.*}
{*جهت اصلاح سریعتر ایرادات موجود، لطفا نظرات خود را با زدن کلید "گزارش بازخورد" برای ما ارسال نمایید. از وقتی که صرف می کنید متشکریم.'}</h4>*}
            {*<span id="close-notify"></span>*}
        {*</div>*}
    {*</div>*}

    {*<script type="text/javascript">*}
        {*$(document).ready(function () {*}
            {*$('#close-notify').click(function () {*}
                {*$('#notify-bar').slideUp();*}
            {*});*}
        {*});*}
    {*</script>*}
{*{/if}*}
<div id="page" class="clearfix">
<!-- Header -->
<header id="header">
<section class="topbar">
    <div class="container">
        <div class="row-fluid">
            <div class="span2 top-header-span"><span class="telephone">(داخلی 110)23050</span></div>
            <div id="top-header-links" class="span6 top-header-span">
                <div class="header_icons">
                    <a href="#" id="header_text" class="header_text">راهنمای خرید</a>
                </div>
                <img class="splite" src="{$img_dir}splite.png" />
                <div class="header_icons">
                    <a href="{$base_dir}index.php?view=category&id=5&fc=module&module=lofblogs&controller=articles"
                       class="header_text">اخبار</a>
                </div>
                <img class="splite" src="{$img_dir}splite.png" />
                <div class="header_icons">
                    <a href="{$link->getPageLink('contact', true)}" class="header_text">{l s='گزارش بازخورد'}</a>
                </div>
                <img class="splite" src="{$img_dir}splite.png" />
                <div class="header_icons">
                    <a href="{$link->getPageLink('contact', true)}" class="header_text">تماس با ما</a>
                </div>
            </div>
            <div class="span4 top-header-span">{$HOOK_HEADERRIGHT}</div>
        </div>
    </div>
</section>
<section class="midd-nav">
    <div class="container">
        <div class="row-fluid">


            <div class="span2">
                {$HOOK_PROMOTETOP}
            </div>
            <div class="span5 offset2" style="padding-top: 7px">
                {$HOOK_TOPNAVIGATION}
            </div>
            <div class="span1 offset2 logo-min-resolution">
                <a id="header_logo" href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
                    <img class="img-responsive logo" src="{$img_dir}logo-behino.png" alt="{$shop_name|escape:'htmlall':'UTF-8'}"
                            />
                </a>
            </div>
        </div>
    </div>
</section>
<section class="header">
<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="container">
                        <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="responsive-btn btn btn-navbar ">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <div class="nav-collapse collapse navbar-responsive-collapse">
                            <ul class="nav" id="main-nav-ul">
                                <li class="nav-li nav-li-active">
                                    <a class="main-nav-a" href="{$base_dir}">
                                        <span class="icon-nav" id="nav-home"></span>
                                        <label class="nav-label">صفحه اول</label>
                                    </a>
                                </li>
                                <li class="nav-li">
                                    <a class="main-nav-a" href="{$base_dir}index.php?id_category=6&controller=category">
                                        <span class="icon-nav" id="nav-mobile"></span>
                                        <label class="nav-label">موبایل</label>
                                    </a>
                                    <ul class="sub-nav">
                                        <li class="menu-item">
                                            <a href="index.php?id_category=7&controller=category">
                                                <label class="brand-name">SONY</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=8&controller=category">
                                                <label class="brand-name">SAMSUNG</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=9&controller=category">
                                                <label class="brand-name">APPLE</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=10&controller=category">
                                                <label class="brand-name">NOKIA</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=11&controller=category">
                                                <label class="brand-name">HTC</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=12&controller=category">
                                                <label class="brand-name">LG</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=13&controller=category">
                                                <label class="brand-name">Asus</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=14&controller=category">
                                                <label class="brand-name">Motorola</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=15&controller=category">
                                                <label class="brand-name">Huawei</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=16&controller=category">
                                                <label class="brand-name">BlackBerry</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=17&controller=category">
                                                <label class="brand-name">Sony Ericsson</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=18&controller=category">
                                                <label class="brand-name">GLX</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=19&controller=category">
                                                <label class="brand-name">Prestigio</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=20&controller=category">
                                                <label class="brand-name">Alcatel</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=21&controller=category">
                                                <label class="brand-name">Hyundai</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=22&controller=category">
                                                <label class="brand-name">Dell</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=23&controller=category">
                                                <label class="brand-name">Dimo</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=24&controller=category">
                                                <label class="brand-name">Acer</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=25&controller=category">
                                                <label class="brand-name">Marshal</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=26&controller=category">
                                                <label class="brand-name">i-mate</label>
                                            </a>
                                        </li>
                                        {*<li class="menu-item">*}
                                        {*<a href="index.php?id_category=27&controller=category">*}
                                        {*<label class="brand-name">Miniyator</label>*}
                                        {*</a>*}
                                        {*</li>*}
                                        {*<li class="menu-item">*}
                                        {*<a href="index.php?id_category=28&controller=category">*}
                                        {*<label class="brand-name">Mercury</label>*}
                                        {*</a>*}
                                        {*</li>*}
                                    </ul>
                                </li>
                                <li class="nav-li">
                                    <a class="main-nav-a" href="{$base_dir}index.php?id_category=29&controller=category">
                                        <span class="icon-nav" id="nav-tablet"></span>
                                        <label class="nav-label">تبلت</label>
                                    </a>
                                    <ul class="sub-nav">
                                        <li class="menu-item">
                                            <a href="index.php?id_category=30&controller=category">
                                                <label class="brand-name">APPLE</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=32&controller=category">
                                                <label class="brand-name">Asus</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=35&controller=category">
                                                <label class="brand-name">SONY</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=31&controller=category">
                                                <label class="brand-name">SAMSUNG</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=34&controller=category">
                                                <label class="brand-name">Huawei</label>
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a href="index.php?id_category=33&controller=category">
                                                <label class="brand-name">LG</label>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                               {* <li class="nav-li">
                                    <a class="main-nav-a" href="{$base_dir}">
                                        <span class="icon-nav" id="nav-accesoris"></span>
                                        <label class="nav-label">لوازم جانبی</label>
                                    </a>
                                </li>*}
                            </ul>
                            <ul class="nav pull-left nav-left">
                                <li class="nav-li">
                                    <a class="main-nav-a" href="{$base_dir}index.php?controller=searchajax">
                                        <label class="nav-label">جستجوی پیشرفته</label>
                                    </a>
                                </li>
                                <li class="nav-li">
                                    <a class="main-nav-a" href="{$base_dir}index.php?controller=prices-drop">
                                        <label class="nav-label">فروش ویژه</label>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</section>
</header>
    <section class="leo-breadscrumb">
        <div class="container">
            {include file="$tpl_dir./breadcrumb.tpl"}
        </div>
    </section>
{if $HOOK_SLIDESHOW &&  in_array($page_name,array('index'))}
    <section id="slideshow">
        <div class="main_silde">
            <div class="container">
                <div class="row-fluid">
                    {$HOOK_SLIDESHOW}
                    <!-- /wrapper -->
                </div>
            </div>
        </div>
    </section>
{/if}


<section id="columns" class="clearfix">
    {*for layerd slider problem*}
    {if $page_name != 'index' && $page_name != 'product'}
    <div class="container">
        {/if}
        <div class="row-fluid">
            {include file="$tpl_dir./layout/{$LEO_LAYOUT_DIRECTION}/header.tpl" hide_left_column=$hide_left_column hide_right_column=$hide_right_column }
            {/if}
