{if $HOOK_CONTENTBOTTOM && in_array($page_name,array('index')) }
    {$HOOK_CONTENTBOTTOM}
    <div class="full-width-separator  parallax" data-parallax-background-ratio="0.7" data-parallax-enabled="1"
         style=" border-top: 1px solid #9e9e9e;">
        <div class="content pattern">
            <div class="carousel-container">
                <div class="clients-wrapper">
                    <div class="caroufredsel_wrapper"
                         style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 1100px; height: 100px; margin: 0px; overflow: hidden;">
                        <ul class="clients carousel">
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=10&controller=category">
                                    <img alt=""
                                         src="{$img_dir}logo-brand/nokia.png"></a>
                            </li>
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=9&controller=category">
                                    <img alt=""
                                                                                                        src="{$img_dir}logo-brand/apple.png"></a>
                            </li>
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=16&controller=category">
                                    <img alt=""
                                                                                                        src="{$img_dir}logo-brand/blackbery.png"></a>
                            </li>
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=18&controller=category">
                                    <img alt=""
                                                                                                        src="{$img_dir}logo-brand/glx.png"></a>
                            </li>
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=11&controller=category">
                                    <img alt=""
                                                                                                        src="{$img_dir}logo-brand/htc.png"></a>
                            </li>
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=15&controller=category">
                                    <img alt=""
                                                                                                        src="{$img_dir}logo-brand/huawei.png"></a>
                            </li>
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=12&controller=category">
                                    <img alt=""
                                                                                                        src="{$img_dir}logo-brand/lg.png"></a>
                            </li>
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=19&controller=category">
                                    <img alt=""
                                                                                                        src="{$img_dir}logo-brand/prestigio.png"></a>
                            </li>
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=8&controller=category">
                                    <img alt=""
                                                                                                        src="{$img_dir}logo-brand/samsung.png"></a>
                            </li>
                            <li class="client" style="width: 215px;"><a href="{$base_dir}index.php?id_category=7&controller=category">
                                    <img alt=""
                                                                                                        src="{$img_dir}logo-brand/sony.png"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}
</section>
{if $page_name == 'category' }
    <section id="right_column" class="column span3 omega">
        {$HOOK_RIGHT_COLUMN}
    </section>
{/if}