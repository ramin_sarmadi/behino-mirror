{include file="$tpl_dir./errors.tpl"}

{if isset($category)}
    {if $category->id AND $category->active}
        <div class="title_category resumecat category-product-count">
            {include file="$tpl_dir./category-count.tpl"}
        </div>
        {if $scenes || $category->description || $category->id_image}
            <div class="content_scene_cat">
                {if $scenes}
                    <!-- Scenes -->
                    {include file="$tpl_dir./scenes.tpl" scenes=$scenes}
                {else}
                    <!-- Category image -->
                    {if $category->id_image}
                        <div class="align_center">
                            <img src="{$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_allinmart')}"
                                 alt="{$category->name|escape:'htmlall':'UTF-8'}"
                                 title="{$category->name|escape:'htmlall':'UTF-8'}" id="categoryImage"
                                 width="{$categorySize.width}" height="{$categorySize.height}"/>
                        </div>
                    {/if}
                {/if}

                {if $category->description}
                    <div class="cat_desc">
                        {if strlen($category->description) > 120}
                            <p id="category_description_short">{$category->description|truncate:120}</p>
                            <p id="category_description_full" style="display:none">{$category->description}</p>
                            <a href="#"
                               onclick="$('#category_description_short').hide(); $('#category_description_full').show(); $(this).hide(); return false;"
                               class="lnk_more">{l s='More'}</a>
                        {else}
                            <p>{$category->description}</p>
                        {/if}
                    </div>
                {/if}
            </div>
        {/if}




        {if isset($subcategories)}
            <!-- Subcategories -->
            <!-- <div id="subcategories">
			<h3 class="title_category">{l s='Subcategories'}</h3>
			<div class="wrapper">
			<div class="inline_list">
			
			{foreach from=$subcategories item=subcategory name=subcategories}
				{if $subcategory@iteration%3==1}
				<div class="row-fluid">
				{/if}
		
				<div class="clearfix span4 subcategories">
					<div class="subcategories-container clearfix">
						<a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'htmlall':'UTF-8'}" title="{$subcategory.name|escape:'htmlall':'UTF-8'}" class="img">
							{if $subcategory.id_image}
								<img src="{$link->getCatImageLink($subcategory.link_rewrite, $subcategory.id_image, 'home_allinmart')}" alt="" />
							{else}
								<img src="{$img_cat_dir}default-home_allinmart.jpg" />
							{/if}
						</a>
						<div class="desc_subcategorise">
							<h3 class="s_title_block"><a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'htmlall':'UTF-8'}" class="cat_name">{$subcategory.name|escape:'htmlall':'UTF-8'}</a></h3>
							{if $subcategory.description}
								<p class="cat_desc">{$subcategory.description}</p>
							{/if}
						</div>
					</div>
				</div>
				
				{if $subcategory@iteration%3==0||$smarty.foreach.subcategories.last}
				</div>
				{/if}
			{/foreach}
			</div>
			</div>
			<br class="clear"/>
		</div> -->
        {/if}
        <div class="wrapper clearfix">
            {if $products}
                <div class="content_sortPagiBar">
                    <div class="row-fluid sortPagiBar">
                        <div class="span2">
                            <div class="inner">
                                {include file="./product-compare.tpl"}
                            </div>
                        </div>
                        <div class="span2">
                            <div class="inner">
                                <div class="btn-group" id="productsview">
                                    <a class="btn" href="#" rel="view-grid"><i class="icon-th"></i></a>
                                    <a class="btn" href="#" rel="view-list"><i class="icon-th-list"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="span8">
                            <div class="inner">
                                {include file="./product-sort.tpl"}
                                {include file="./nbr-product-page.tpl"}
                            </div>
                        </div>
                    </div>
                </div>
                {include file="./product-list.tpl" products=$products}
                {include file="$tpl_dir./pagination.tpl"}
            {/if}
            {elseif $category->id}
            <p class="warning">{l s='This category is currently unavailable.'}</p>
        </div>
    {/if}

{/if}
