{if isset($products)}
    <div class="wrapper-comparison">
        <h1 class="title_category">{l s='Archive Product view for compare'}
            {if ($products|count)>0 }
                <div class="controls">
                    <a id="next1" class="carousel-right"></a>
                    <a id="prev1" class="jcarousel-left"></a>
                </div>
                <a style="" class="delete_all_cookie compare-btn"
                   href="{$link->getPageLink('products-comparison?form=1&action=delallcookie', true)}"
                   rel="ajax_id_product_{$product.id_product}">
                    {l s='delete all cookies product'}
                </a>
            {/if}
        </h1>
        <!-- Products list -->
        <div id="cmp-offer-list" class="products_block main-box">
            <div class="carousel">
                {foreach from=$products item=product name=products}
                    {if $product.isValid==1}
                        {if $product@iteration%(100)==1}
                        {/if}
                        <div class="product-height p-item product_block ajax_block_product {if $smarty.foreach.products.first}first_item
                {elseif $smarty.foreach.products.last}last_item{/if}clearfix" >
                            <div class="list-products">
                                <div class="product-container clearfix">
                                    <div class="center_block">
                                        <a href="{$product.link|escape:'htmlall':'UTF-8'}"
                                           class="product_img_link no-img-height"
                                           title="{$product.name|escape:'htmlall':'UTF-8'}">
                                            <img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'compare-allinmart')}"
                                                 alt="{$product.legend|escape:'htmlall':'UTF-8'}"/>
                                            {if isset($product.new) && $product.new == 1}<span
                                                    class="new">{l s='New'}</span>{/if}
                                        </a>
                                    </div>
                                    <div class="right_block" style="height: 100px;">
                                        <h3 class="s_title_block"><a href="{$product.link|escape:'htmlall':'UTF-8'}"
                                                                     title="{$product.name|escape:'htmlall':'UTF-8'}">{$product.name|escape:'htmlall':'UTF-8'|truncate:35:'...'}</a>
                                        </h3>

                                        {if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
                                            <span class="on_sale">{l s='On sale!'}</span>
                                        {elseif isset($product.reduction) && $product.reduction && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
                                            <span class="discount">{l s='Reduced price!'}</span>{/if}
                                        {if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
                                            <div class="content_price">
                                                {if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
                                                    <span class="price"
                                                          style="display: inline;">{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}</span>
                                                    <br/>
                                                {/if}
                                            </div>
                                            {if isset($product.online_only) && $product.online_only}<span
                                                    class="online_only">{l s='Online only!'}</span>{/if}
                                        {/if}

                                        <div class="suggest-links">

                                        {if $product.isValid==1}
                                            <a class="prd_add"
                                               href="{$link->getPageLink('products-comparison', true)}?form=1&action=add&id_product={$product.id_product}"
                                               rel="ajax_id_product_{$product.id_product}">{l s='Add to compare'}
                                            </a>
                                        {else}
                                            <div title="{l s='Don`t equal parent with other product comparison'}">{l s='Don`t compare'}</div>
                                        {/if}
                                        |
                                        <a class="delete_cookie"
                                           href="{$link->getPageLink('products-comparison', true)}?form=1&action=delcookie&id_product={$product.id_product}"
                                           rel="ajax_id_product_{$product.id_product}">{l s='delete cookie product'}</a>

                                        </div>

                                        <div class="left_block">
                                            {if isset($comparator_max_item) && $comparator_max_item}
                                                <p class="compare">
                                                    <input type="checkbox" class="comparator"
                                                           id="comparator_item_{$product.id_product}"
                                                           value="comparator_item_{$product.id_product}"
                                                           {if isset($compareProducts) && in_array($product.id_product, $compareProducts)}checked="checked"{/if} />
                                                    <label for="comparator_item_{$product.id_product}">{l s='Select to compare'}</label>
                                                </p>
                                            {/if}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {if $product@iteration%(6)==0||$smarty.foreach.products.last}
                        {/if}
                    {/if}
                {/foreach}
            </div>
        </div>
        <!-- /Products list -->
    </div>
{/if}
