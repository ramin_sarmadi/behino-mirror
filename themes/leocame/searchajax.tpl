<link href="{$css_dir}bootstrap-multiselect.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="{$js_dir}bootstrap-multiselect.js"></script>
<link href="{$css_dir}jquery.nouislider.min.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="{$js_dir}jquery.nouislider.min.js"></script>
<script type="text/javascript" src="{$js_dir}searchajax.js"></script>
<link href="{$css_dir}searchajax.css" type="text/css" rel="stylesheet"/>
<h1 class="title_category">جستجوی پیشرفته</h1>
<div class="wrapper clearfix">
<div class="row-fluid">
<section class="span12">
<form class="std" id="search-ajax-form" action="" method="post" enctype="multipart/form-data">
<div class="row-fluid">

    <div class="span6">
        {*<div class="four-value">
            <span>{$AllFeaturesWithGroup_1_10[0].name}:</span>
            <input id="reso-check" type="checkbox"
                   value="{$AllFeaturesWithGroup_1_10[4].id_feature}_{$AllFeaturesWithGroup_1_10[0].id_feature_value}"
                   name="feature_value[]" autocomplete="off">
            {l s='حداقل'}:<input class="input-reso" maxlength="6" type="text" value="320"
                                 name="min_feature_value_{$AllFeaturesWithGroup_1_10[0].id_feature_value}">
            {l s='X'}<input class="input-reso" maxlength="6" type="text" value="320"
                            name="max_feature_value_{$AllFeaturesWithGroup_1_10[0].id_feature_value}">
            {l s='حدکثر'}:<input class="input-reso" maxlength="6" type="text"
                                 value="1920"
                                 name="min_feature_value_2_{$AllFeaturesWithGroup_1_10[0].id_feature_value}">
            {l s='X'}<input class="input-reso" maxlength="6" type="text"
                            value="1920"
                            name="max_feature_value_2_{$AllFeaturesWithGroup_1_10[0].id_feature_value}">
        </div>*}

        <div class="range-div">
            <span class="price-span">{$AllFeaturesWithGroup_3_27[0].name}:</span>

            <div class="value-slider" id="camera-slider"></div>
            <div class="range-values">
                <input class="range-checkbox" id="camera-checkbox" style="display: none" type="checkbox" name="feature_value[]"
                       value="{$AllFeaturesWithGroup_3_27[0].id_feature}_{$AllFeaturesWithGroup_3_27[0].id_feature_value}" autocomplete="off">
                <span class="max-val" data-max="{$MaxCameraDB}"></span>
                <label id="price-label" class="price-label">-</label>
                <span class="min-val" data-min="{$MinCameraDB}"></span>
                <span class="range-unit">{l s='مگاپیکسل'}</span>
                <input class="min_val_hidden" id="min-camera" type="hidden" value=""
                       name="min_feature_value_{$AllFeaturesWithGroup_3_27[0].id_feature_value}"/>
                <input class="max_val_hidden" id="max-camera" type="hidden" value=""
                       name="max_feature_value_{$AllFeaturesWithGroup_3_27[0].id_feature_value}"/>
            </div>
        </div>
        <div class="range-div">
            <span class="price-span"> {$AllFeaturesWithGroup_1_22[0].name}: </span>
            <div class="value-slider" id="display-slider"></div>
            <div class="range-values">
                <input class="range-checkbox" id="display-checkbox" style="display: none" type="checkbox" name="feature_value[]"
                       value="{$AllFeaturesWithGroup_1_22[0].id_feature}_{$AllFeaturesWithGroup_1_22[0].id_feature_value}" autocomplete="off">
                <span class="max-val" data-max="{$MaxScreenDB}"></span>
                <label id="price-label" class="price-label">-</label>
                <span class="min-val" data-min="{$MinScreenDB}"></span>
                <span class="range-unit">{l s='اینچ'}</span>
                <input class="min_val_hidden" id="min-display" type="hidden" value=""
                       name="min_feature_value_{$AllFeaturesWithGroup_1_22[0].id_feature_value}"/>
                <input class="max_val_hidden" id="max-display" type="hidden" value=""
                       name="max_feature_value_{$AllFeaturesWithGroup_1_22[0].id_feature_value}"/>
            </div>
        </div>
        <div class="range-div">
            <span class="price-span"> {$AllFeaturesWithGroup_1_21[0].name}: </span>
            <div class="value-slider" id="trakom-slider"></div>
            <div class="range-values">
                <input class="range-checkbox" id="trakom-checkbox" style="display: none" type="checkbox" name="feature_value[]"
                       value="{$AllFeaturesWithGroup_1_21[0].id_feature}_{$AllFeaturesWithGroup_1_21[0].id_feature_value}" autocomplete="off">
                <span class="max-val" data-max="{$MaxPpiDB}"></span>
                <label id="price-label" class="price-label">-</label>
                <span class="min-val" data-min="{$MinPpiDB}"></span>
                <span class="range-unit">{l s='پیکسل در اینچ'}</span>
                <input class="min_val_hidden" id="min-display" type="hidden" value=""
                       name="min_feature_value_{$AllFeaturesWithGroup_1_21[0].id_feature_value}"/>
                <input class="max_val_hidden" id="max-display" type="hidden" value=""
                       name="max_feature_value_{$AllFeaturesWithGroup_1_21[0].id_feature_value}"/>
            </div>
        </div>

    </div>


    <div class="span6">
        {*<div class="product-name">*}
            {*<span>نام محصول:</span> <input type="text" name="NAME_PRODUCT" value="">*}
        {*</div>*}
        <div class="range-div">
            <span class="price-span" id="price-span">محدوده قیمت:</span>
            <div class="value-slider" id="price-slider"></div>
            <div class="range-values">

                <span class="max-val" data-max="{$MaxPriceDB}"></span>
                <label id="price-label" class="price-label">-</label>
                <span class="min-val" data-min="{$MinPriceDB}"></span>
                <span class="range-unit">تومان</span>
                <input type="hidden" name="min_cost" class="min_val_hidden" value="0">
                <input type="hidden" name="max_cost" class="max_val_hidden" value="999999999">
            </div>
        </div>
        <div class="range-div">
            <span class="price-span" id="weight-span">{$AllFeaturesWithGroup_5_4[0].name}:</span>

            <div class="value-slider" id="weight-slider"></div>
            <div class="range-values">
                <input class="range-checkbox" id="weight-checkbox" style="display: none" type="checkbox" name="feature_value[]"
                       value="{$AllFeaturesWithGroup_5_4[0].id_feature}_{$AllFeaturesWithGroup_5_4[0].id_feature_value}" autocomplete="off">
                <span class="max-val" data-max="{$MaxWeightDB}"></span>
                <label id="price-label" class="price-label">-</label>
                <span class="min-val" data-min="{$MinWeightDB}"></span>
                <span class="range-unit">گرم</span>
                <input id="min-weight" class="min_val_hidden" type="hidden" value=""
                       name="min_feature_value_{$AllFeaturesWithGroup_5_4[0].id_feature_value}" class="min_val_hidden"/>
                <input id="max-weight" class="max_val_hidden" type="hidden" value=""
                       name="max_feature_value_{$AllFeaturesWithGroup_5_4[0].id_feature_value}" class="max_val_hidden"/>
            </div>
        </div>
        {*<div class="range-div">*}
            {*<span class="price-span">{$AllFeaturesWithGroup_6_18[0].name}:</span>*}
            {*<div class="value-slider" id="cpu-slider"></div>*}
            {*<div class="range-values">*}
                {*<input class="range-checkbox" id="cpu-checkbox" style="display: none" type="checkbox" name="feature_value[]"*}
                       {*value="{$AllFeaturesWithGroup_6_18[0].id_feature}_{$AllFeaturesWithGroup_6_18[0].id_feature_value}" autocomplete="off">*}
                {*<span class="max-val" data-max="{$MaxSpeedDB}"></span>*}
                {*<label id="price-label" class="price-label">-</label>*}
                {*<span class="min-val" data-min="{$MinSpeedDB}"></span>*}
                {*<span class="range-unit">گیگاهرتز</span>*}
                {*<input class="min_val_hidden" id="min-cpu" type="hidden" value=""*}
                       {*name="min_feature_value_{$AllFeaturesWithGroup_6_18[0].id_feature_value}"/>*}
                {*<input class="max_val_hidden" id="max-cpu" type="hidden" value=""*}
                       {*name="max_feature_value_{$AllFeaturesWithGroup_6_18[0].id_feature_value}"/>*}
            {*</div>*}
        {*</div>*}

    </div>




</div>

<div id="select-device-text" class="row-fluid">
    <label id="device-label">نوع دستگاه را انتخاب کنید:</label>
    {foreach from=$AllCategories key='id_category' item='AllCat' name="foo"}
        {if ($AllCat.is_root_category==1)}
            <input class="device-input" id="device_{$smarty.foreach.foo.iteration}" type="radio" value="{$AllCat.id_category}" name="id_category[]" autocomplete="off" > <label class="device-radio-label">{$AllCat.name}</label>
        {/if}
    {/foreach}
</div>


<div class="row-fluid">
   {* <div id="device" class="search-items">
        <span class="combo-name">نوع وسیله</span>
        <select name="id_category[]" class="select-cls" id="select-device" autocomplete="off" multiple>
            {foreach from=$AllCategories key='id_category' item='AllCat'}
                {if ($AllCat.is_root_category==1)}
                <option value="{$AllCat.id_category}">{$AllCat.name}</option>
                {/if}
            {/foreach}
        </select>
    </div>*}
    <div id="mobile-brand" class="search-items">
        <span class="combo-name">برند موبایل</span>
        <select name="id_category[]" class="select-cls" id="select-brand-mobile" autocomplete="off" multiple>
            {foreach from=$AllCategories key='id_category' item='AllCat'}
                {if ($AllCat.id_parent==6)}
                <option value="{$AllCat.id_category}">{$AllCat.Total_Name}</option>
                {/if}
            {/foreach}
        </select>
    </div>
	  <div id="tablet-brand" class="search-items">
        <span class="combo-name">برند تبلت</span>
        <select name="id_category[]" class="select-cls" id="select-brand-tablet" autocomplete="off" multiple>
            {foreach from=$AllCategories key='id_category' item='AllCat'}
                {if ($AllCat.id_parent==29)}
                <option value="{$AllCat.id_category}">{$AllCat.Total_Name}</option>
                {/if}
            {/foreach}
        </select>
    </div>


    <div class="search-items">
        <span class="combo-name">{$AllFeaturesWithGroup_10_45[0].name}</span>
        <select data-id="1" name="feature_value[]" class="select-cls" id="select-os" autocomplete="off" multiple>
            {foreach from=$AllFeaturesWithGroup_10_45 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items">
        <span class="combo-name">{$AllFeaturesWithGroup_1_8[0].name}</span>
        <select data-id="2" name="feature_value[]" class="select-cls" id="select-display" autocomplete="off" multiple>
            {foreach from=$AllFeaturesWithGroup_1_8 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items">
        <span class="combo-name">{$AllFeaturesWithGroup_1_9[0].name}</span>
        <select data-id="3" name="feature_value[]" class="select-cls" id="select-display-tech" autocomplete="off"
                multiple>
            {foreach from=$AllFeaturesWithGroup_1_9 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items">
        <span class="combo-name">{$AllFeaturesWithGroup_5_14[0].name}</span>
        <select data-id="4" name="feature_value[]" class="select-cls" id="select-sim-num" autocomplete="off" multiple>
            {foreach from=$AllFeaturesWithGroup_5_14 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>



   {* <div class="search-items">
        <span class="combo-name">{$AllFeaturesWithGroup_3_31[0].name}</span>
        <select data-id="5" name="feature_value[]" class="select-cls" id="select-video" autocomplete="off" multiple>
            {foreach from=$AllFeaturesWithGroup_3_31 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>*}
    <div class="search-items">
        <span class="combo-name">{$AllFeaturesWithGroup_7_13[0].name}</span>
        <select data-id="6" name="feature_value[]" class="select-cls" id="select-video" autocomplete="off" multiple>
            {foreach from=$AllFeaturesWithGroup_7_13 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items">
        <span class="combo-name">{$AllFeaturesWithGroup_7_24[0].name}</span>
        <select data-id="7" name="feature_value[]" class="select-cls" id="select-video" autocomplete="off" multiple>
            {foreach from=$AllFeaturesWithGroup_7_24 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
   <div class="search-items">
       <span class="combo-name">{$AllFeaturesWithGroup_5_88[0].name}</span>
       <select data-id="8" name="feature_value[]" class="select-cls" id="select-video" autocomplete="off" multiple>
           {foreach from=$AllFeaturesWithGroup_5_88 key='id_feature' item='AllFeatureWithGroup'}
               <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items">
       <span class="combo-name">{$AllFeaturesWithGroup_11_67[0].name}</span>
       <select data-id="8" name="feature_value[]" class="select-cls" id="select-video" autocomplete="off" multiple>
           {foreach from=$AllFeaturesWithGroup_11_67 key='id_feature' item='AllFeatureWithGroup'}
               <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items">
       <span class="combo-name">{$AllFeaturesWithGroup_6_118[0].name}</span>
       <select data-id="8" name="feature_value[]" class="select-cls" id="select-video" autocomplete="off" multiple>
           {foreach from=$AllFeaturesWithGroup_6_118 key='id_feature' item='AllFeatureWithGroup'}
               <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>

   </div>

<div class="row-fluid">
    <div id="tag-box">
        <span class="tag-title">موارد انتخاب شده</span>
        <div class="tag-box-inner span10"></div>
    </div>
</div>


<div class="row-fluid">


    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_8_79[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-gps" autocomplete="off">
            <option value="-1">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_8_79 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_1_7[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-touch" autocomplete="off">
            <option value="-2">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_1_7 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_3_29[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-vidcall" autocomplete="off">
            <option value="-3">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_3_29 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_3_34[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-flash" autocomplete="off">
            <option value="-4">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_3_34 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_1_6[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-dis-color" autocomplete="off">
            <option value="-5">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_1_6 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_7_25[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
            <option value="-6">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_7_25 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_8_11[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
            <option value="-7">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_8_11 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_8_76[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
            <option value="-8">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_8_76 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_8_77[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
            <option value="-9">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_8_77 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_8_82[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
            <option value="-10">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_8_82 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_8_83[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
            <option value="-11">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_8_83 key='id_feature' item='AllFeatureWithGroup'}
                <option value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_9_39[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
            <option value="-12">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_9_39 key='id_feature' item='AllFeatureWithGroup' name="foo"}
                <option data-number='{$smarty.foreach.foo.iteration}'
                        value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_9_39[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_9_39 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_3_30[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_3_30 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_3_33[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_3_33 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_12_62[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_12_62 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_8_12[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_8_12 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_8_81[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_8_81 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_8_85[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_8_85 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_14_89[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_14_89 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_14_90[0].name|truncate:30}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_14_90 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_14_92[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_14_92 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_14_93[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_14_93 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_14_94[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_14_94 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_14_95[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_14_95 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_14_96[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_14_96 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_14_97[0].name|truncate:30}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_14_97 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_14_98[0].name|truncate:30}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_14_98 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_11_100[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_11_100 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
   <div class="search-items-2">
       <span class="combo-name">{$AllFeaturesWithGroup_1_102[0].name}</span>
       <select name="feature_value[]" class="select-single" id="select-memory" autocomplete="off">
           <option value="-12">مهم نیست</option>
           {foreach from=$AllFeaturesWithGroup_1_102 key='id_feature' item='AllFeatureWithGroup' name="foo"}
               <option data-number='{$smarty.foreach.foo.iteration}'
                       value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
           {/foreach}
       </select>
   </div>
    <div class="search-items-2">
        <span class="combo-name">{$AllFeaturesWithGroup_5_119[0].name}</span>
        <select name="feature_value[]" class="select-single" id="select-sim-tablet" autocomplete="off">
            <option value="-12">مهم نیست</option>
            {foreach from=$AllFeaturesWithGroup_5_119 key='id_feature' item='AllFeatureWithGroup' name="foo"}
                <option data-number='{$smarty.foreach.foo.iteration}'
                        value="{$AllFeatureWithGroup.id_feature}_{$AllFeatureWithGroup.id_feature_value}">{$AllFeatureWithGroup.value}</option>
            {/foreach}
        </select>
    </div>
</div>
</div>
<input id="pagination" type="hidden" name="page" value="1">
<input id="search-go" class="btn" type="submit" value="جستجو">
{*<button class="btn" id="reset-form">پاک کردن فرم ها</button>*}
</form>
</section>
</div>
</div>
<div class="container">
    <div class="wrapper clearfix">
        <div id="results-count-div">
            <span id="result-count-text" >تعداد نتایج:</span><span id="results-count"></span>
        </div>
        <div class="ajax-loading"></div>
        <div id="no-result"><span>محصولی وجود ندارد</span></div>
        <div id="server-error"><span>در حال حاضر سیستم پاسخگو نیست لطفا بعدا مجددا تلاش کنید</span></div>
        <div id="results"></div>

        <button class="btn btn-primary" id="more">مشاهده نتایج بیشتر</button>
        <div class="ajax-loading-more" style=""></div>
    </div>
</div>
