


<h1 class="title_category"> {l s='Product Video Player'} </h1>

<div class="wrapper clearfix">
{if $ResultProductVideo}
                <div class="span12">

                    <div id="tag-box">

                        <div class="tag-box-inner">
                            {l s='Name Product Video'}: {$ResultProductVideo[0].name}
                        </div>

                        <div class="tag-box-inner" style="text-align: center">
                            {*$ResultProductVideo|print_r*}

                            <video width="{$ResultProductVideo[0].width}" height="{$ResultProductVideo[0].height}" controls poster="{$ProductVideoImageLocation}{$ResultProductVideo[0].imagefile}">
                                <source src="{$ProductVideoLocation}{$ResultProductVideo[0].file}" type="{$ResultProductVideo[0].video_format}">
                            </video>

                        </div>


                    </div>

                </div>
{else}
    <div class="span12">
        {l s='Not find video for this product!'}
    </div>
{/if}

    <div id="product_videos_block_tab">
        {l s='related video'}: {$ResultProductVideo[0].name}
        {if $ResultrelatedProductVideo}
            {if (($id_customer>0))}

                {section name=row loop=$ResultrelatedProductVideo}

                    <div class="video clearfix">

                        <div class="video_author_infos">
                            <span style="font-size: 14px; color: #003399">{$smarty.section.row.index+1}.</span>
                            <strong>
                                <a href="{$ResultrelatedProductVideo[row].ProductVideo_Link}" target="_blank">{*l s='Video Name'*}{$ResultrelatedProductVideo[row].name|escape:'html':'UTF-8'}</a>
                            </strong><br/>
                        </div>
                        <div class="video_author_infos">
                            <strong>{l s='Video Description'}:
                                <a href="{$ResultrelatedProductVideo[row].ProductVideo_Link}" target="_blank">
                                {$ResultrelatedProductVideo[row].description|escape:'html':'UTF-8'}</strong><br/>
                            </a>
                        </div>
                        <div class="video_author_infos">
                            <strong>{l s='Video Type'}:{$ResultrelatedProductVideo[row].type_name|escape:'html':'UTF-8'}</strong><br/>
                        </div>

                        <em>{l s='Video Date'}:{dateFormat date=$ResultrelatedProductVideo[row].date_add|escape:'html':'UTF-8' full=0}</em>

                    </div>

                    <hr/>


                {/section}


            {/if}

        {else}
            <p class="align_center">
                {l s='Noting Video' mod='productvideos'}!
            </p>
        {/if}
    </div>

</div>
<div id="product_videos_block_tab">
    {l s='All video'}:
    {section name=row loop=$ResultAllProductVideo}

        <div class="video clearfix">

            <div class="video_author_infos">
                <span style="font-size: 14px; color: #003399">{$smarty.section.row.index+1}.</span>
                <strong>
                    <a href="{$ResultAllProductVideo[row].ProductVideo_Link}" target="_blank">{*l s='Video Name'*}{$ResultAllProductVideo[row].name|escape:'html':'UTF-8'}</a>
                </strong><br/>
            </div>
            <div class="video_author_infos">
                <strong>{l s='Video Description'}:
                    <a href="{$ResultAllProductVideo[row].ProductVideo_Link}" target="_blank">
                    {$ResultAllProductVideo[row].description|escape:'html':'UTF-8'}</strong><br/>
                </a>
            </div>
            <div class="video_author_infos">
                <strong>{l s='Video Type'}:{$ResultAllProductVideo[row].type_name|escape:'html':'UTF-8'}</strong><br/>
            </div>

            <em>{l s='Video Date'}:{dateFormat date=$ResultAllProductVideo[row].date_add|escape:'html':'UTF-8' full=0}</em>

        </div>

        <hr/>


    {/section}
</div>
