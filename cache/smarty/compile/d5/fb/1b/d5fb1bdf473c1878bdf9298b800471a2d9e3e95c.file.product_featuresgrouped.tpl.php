<?php /* Smarty version Smarty-3.1.13, created on 2014-04-12 12:20:17
         compiled from "C:\wamp\www\ibartar2\themes\leocame\modules\sodfeaturesgrouped\product_featuresgrouped.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18437531585280394f1-91406034%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd5fb1bdf473c1878bdf9298b800471a2d9e3e95c' => 
    array (
      0 => 'C:\\wamp\\www\\ibartar2\\themes\\leocame\\modules\\sodfeaturesgrouped\\product_featuresgrouped.tpl',
      1 => 1394965858,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18437531585280394f1-91406034',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_531585280fce15_16272901',
  'variables' => 
  array (
    'datasheet' => 0,
    'img_dir' => 0,
    'loc' => 0,
    'groupcellcolor' => 0,
    'groupfontcolor' => 0,
    'sheet' => 0,
    'sodprodfeat' => 0,
    'feature' => 0,
    'feat' => 0,
    'featurecellcolor' => 0,
    'featurefontcolor' => 0,
    'featurevaluecellcolor' => 0,
    'featurevaluefontcolor' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_531585280fce15_16272901')) {function content_531585280fce15_16272901($_smarty_tpl) {?><!-- Data Sheet -->
<?php if (($_smarty_tpl->tpl_vars['datasheet']->value!=0)){?>
    <div class="row-fluid">
        <div class="span12">
            <div id="description">
                <div id="more_info_block" class="clear">
                    <div class="row-fluid box-row">
                        <div class="span12">
                            <div id="fani-title" class="product-box-title">
                                <img id="in-one-eye" src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
product-icons/technical.png"/>
                                <span class="prod-title-text"><?php echo smartyTranslate(array('s'=>"مشخصات فنی"),$_smarty_tpl);?>
</span>
                            </div>
                        </div>
                    </div>

                    
                    <div id="groupedfeatures feature-scroll" style="float:right; width:100%; margin-bottom:30px;">
                        <?php if ($_smarty_tpl->tpl_vars['loc']->value=='footer'){?><h2><?php echo smartyTranslate(array('s'=>'Data Sheet','mod'=>'sodfeaturesgrouped'),$_smarty_tpl);?>
</h2><?php }?>
                        <table width="100%" style="border-collapse:collapse; border-spacing:0;">
                            <?php  $_smarty_tpl->tpl_vars['sheet'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sheet']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['datasheet']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sheet']->key => $_smarty_tpl->tpl_vars['sheet']->value){
$_smarty_tpl->tpl_vars['sheet']->_loop = true;
?>
                                <tr style="background:<?php echo $_smarty_tpl->tpl_vars['groupcellcolor']->value;?>
; border:1px solid #999999">
                                    <td align="right" valign="middle" colspan="2"
                                        style="border:1px solid #999999; padding:5px;">
<span style="color:<?php echo $_smarty_tpl->tpl_vars['groupfontcolor']->value;?>
; font-family:wyekan, tahoma, Helvetica, sans-serif; font-size:19px;">
<?php echo $_smarty_tpl->tpl_vars['sheet']->value['groupname'];?>

</span>
                                    </td>
                                </tr>
                                <?php  $_smarty_tpl->tpl_vars['feat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['feat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sheet']->value['features']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['feat']->key => $_smarty_tpl->tpl_vars['feat']->value){
$_smarty_tpl->tpl_vars['feat']->_loop = true;
?>
                                    <div dir="ltr"></div>
                                    <?php  $_smarty_tpl->tpl_vars['feature'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['feature']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sodprodfeat']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['feature']->key => $_smarty_tpl->tpl_vars['feature']->value){
$_smarty_tpl->tpl_vars['feature']->_loop = true;
?>


                                        <?php if (($_smarty_tpl->tpl_vars['feature']->value['id_feature']==$_smarty_tpl->tpl_vars['feat']->value)&&($_smarty_tpl->tpl_vars['feature']->value['id_group']==$_smarty_tpl->tpl_vars['sheet']->value['id_group'])&&($_smarty_tpl->tpl_vars['feature']->value['featurevalue']!='')){?>
                                            <tr border="1px" style="border:1px solid #999999">
                                                <td align="right" valign="middle" width="30%"
                                         style="border:1px solid #999999; padding:5px; background:<?php echo $_smarty_tpl->tpl_vars['featurecellcolor']->value;?>
; font-size:14px; ">
                                        <span style="color:<?php echo $_smarty_tpl->tpl_vars['featurefontcolor']->value;?>
; font-family:wyekan, tahoma, Helvetica, sans-serif; ">
                                        <?php echo $_smarty_tpl->tpl_vars['feature']->value['feature_name'];?>

                                        </span>
                                                </td>
                                                <td align="right" valign="middle" width="70%"
                                                    style="border:1px solid #999999; padding:5px; background:<?php echo $_smarty_tpl->tpl_vars['featurevaluecellcolor']->value;?>
; font-size:12px;">
                                                <span style="color:<?php echo $_smarty_tpl->tpl_vars['featurevaluefontcolor']->value;?>
; font-family:wyekan, tahoma, Helvetica, sans-serif;">
                                                <?php echo $_smarty_tpl->tpl_vars['feature']->value['featurevalue'];?>

                                                </span>
                                                </td>
                                            </tr>
                                        <?php }?>

                                    <?php } ?>

                                <?php } ?>

                            <?php } ?>
                        </table>
                    </div>

                    
                    
                    
                </div>
            </div>
        </div>
    </div>
<?php }?>
<!-- /Data Sheet Table -->


<?php }} ?>