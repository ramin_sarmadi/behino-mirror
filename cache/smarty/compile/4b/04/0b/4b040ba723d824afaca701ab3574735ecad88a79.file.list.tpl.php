<?php /* Smarty version Smarty-3.1.13, created on 2014-02-12 18:33:27
         compiled from "C:\wamp\www\ibartar2\modules\lofblogs\themes\default\category\list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1358052fb8d3fbeee96-00230896%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b040ba723d824afaca701ab3574735ecad88a79' => 
    array (
      0 => 'C:\\wamp\\www\\ibartar2\\modules\\lofblogs\\themes\\default\\category\\list.tpl',
      1 => 1385292591,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1358052fb8d3fbeee96-00230896',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LBObject' => 0,
    'article' => 0,
    'thumbUri' => 0,
    'config' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52fb8d3fcb0415_99070934',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52fb8d3fcb0415_99070934')) {function content_52fb8d3fcb0415_99070934($_smarty_tpl) {?><?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['LBObject']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value){
$_smarty_tpl->tpl_vars['article']->_loop = true;
?>
    <div class="lofcontent_category_item">   
        <?php if ($_smarty_tpl->tpl_vars['article']->value['image']){?>
        <div class="lof_item_thumb">
            <img src="<?php echo $_smarty_tpl->tpl_vars['thumbUri']->value;?>
<?php echo $_smarty_tpl->tpl_vars['article']->value['image'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['article']->value['title'];?>
" />
        </div>
        <?php }?>
        <div class="lof_item_desc">
            <p class="lof_link_title" >
                <a href="<?php echo $_smarty_tpl->tpl_vars['article']->value['link'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['article']->value['title'];?>
" ><?php echo $_smarty_tpl->tpl_vars['article']->value['title'];?>
</a>
            </p>
        <?php if ($_smarty_tpl->tpl_vars['config']->value['showInfoList']){?><p class="article_infor"><?php echo smartyTranslate(array('s'=>'written by ','mod'=>'lofblogs'),$_smarty_tpl);?>
<span class="lofcontent_authorname"><?php echo $_smarty_tpl->tpl_vars['article']->value['authorname'];?>
</span><?php echo smartyTranslate(array('s'=>' at ','mod'=>'lofblogs'),$_smarty_tpl);?>
<?php echo $_smarty_tpl->tpl_vars['article']->value['displayDate'];?>
</p><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['config']->value['showRatingList']){?><?php echo $_smarty_tpl->tpl_vars['article']->value['ratingPage'];?>
<?php }?>
    <p class="lof_description"><?php echo $_smarty_tpl->tpl_vars['article']->value['introtext'];?>
</p>
</div>
</div>
<?php } ?>
<?php }} ?>