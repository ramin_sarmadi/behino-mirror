<?php /* Smarty version Smarty-3.1.13, created on 2014-02-12 18:41:43
         compiled from "C:\wamp\www\ibartar2\modules\lofblogs\themes\default\content\content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3158152fb8f2fe603f7-02718696%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1497618dfb1679648095a781930366238e1d1efe' => 
    array (
      0 => 'C:\\wamp\\www\\ibartar2\\modules\\lofblogs\\themes\\default\\content\\content.tpl',
      1 => 1385292591,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3158152fb8f2fe603f7-02718696',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'LBObject' => 0,
    'config' => 0,
    'ratingClass' => 0,
    'imgLoadingVote' => 0,
    'LOFSYSTEM_GALLERY' => 0,
    'imageUri' => 0,
    'rootUri' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52fb8f3001da20_40558909',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52fb8f3001da20_40558909')) {function content_52fb8f3001da20_40558909($_smarty_tpl) {?>
<div id="lofcontent_content" class="lofcontent_block">
    <h2><?php echo $_smarty_tpl->tpl_vars['LBObject']->value->title;?>
</h2>
    <div class="article_header">
    <?php if ($_smarty_tpl->tpl_vars['config']->value['showInfo']){?><p class="article_infor"><?php echo smartyTranslate(array('s'=>'written by ','mod'=>'lofblogs'),$_smarty_tpl);?>
<span class="lofcontent_authorname"><?php echo $_smarty_tpl->tpl_vars['LBObject']->value->authorname;?>
</span><?php echo smartyTranslate(array('s'=>' at ','mod'=>'lofblogs'),$_smarty_tpl);?>
<?php echo $_smarty_tpl->tpl_vars['LBObject']->value->date_upd;?>
</p><?php }?>
    <?php if ($_smarty_tpl->tpl_vars['config']->value['showRating']){?>
    <div class="lofcontent_rating_container" >
        <div id="article_rating_label"><?php echo smartyTranslate(array('s'=>'Article Rating','mod'=>'lofblogs'),$_smarty_tpl);?>
 </div>
        <div id="article_rating" <?php echo $_smarty_tpl->tpl_vars['ratingClass']->value;?>
 >
            <a href="javascript:void(0)"  class="article_rate_buttons" id="article_rate_btn1" title="1" >1</a>
            <a href="javascript:void(0)"  class="article_rate_buttons" id="article_rate_btn2" title="2" >2</a>
            <a href="javascript:void(0)"  class="article_rate_buttons" id="article_rate_btn3" title="3" >3</a>
            <a href="javascript:void(0)"  class="article_rate_buttons" id="article_rate_btn4" title="4" >4</a>
            <a href="javascript:void(0)"  class="article_rate_buttons" id="article_rate_btn5" title="5" >5</a>
        </div>
        <div><span id="article_rating_total"><?php echo $_smarty_tpl->tpl_vars['LBObject']->value->rating_num;?>
</span><span><?php echo smartyTranslate(array('s'=>'vote','mod'=>'lofblogs'),$_smarty_tpl);?>
</span></div>
        <div id="rating_status"><img id="loading_img" src="<?php echo $_smarty_tpl->tpl_vars['imgLoadingVote']->value;?>
" /><span id="aticle_rating_note"><?php echo smartyTranslate(array('s'=>'Thank you for vote this article','mod'=>'lofblogs'),$_smarty_tpl);?>
</span></div>
    </div>    
    <?php }?>
</div>
<?php if ($_smarty_tpl->tpl_vars['LOFSYSTEM_GALLERY']->value){?> <?php echo $_smarty_tpl->tpl_vars['LOFSYSTEM_GALLERY']->value;?>
<?php }?> 
<div class="lofcontent_content_main">
    <div class="lofcontent_main_content" >
        <?php if ($_smarty_tpl->tpl_vars['LBObject']->value->image){?>
        <div class="primay_img img_align_<?php echo $_smarty_tpl->tpl_vars['config']->value['imgAlign'];?>
">
            <img <?php if ($_smarty_tpl->tpl_vars['config']->value['content_img_width']){?>width="<?php echo $_smarty_tpl->tpl_vars['config']->value['content_img_width'];?>
"<?php }?> src="<?php echo $_smarty_tpl->tpl_vars['imageUri']->value;?>
<?php echo $_smarty_tpl->tpl_vars['LBObject']->value->image;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['LBObject']->value->title;?>
" />
        </div>
        <?php }?>
        <?php echo $_smarty_tpl->tpl_vars['LBObject']->value->content;?>

    </div>
</div>
<input type="hidden" name="root_uri" id="root_uri" value="<?php echo $_smarty_tpl->tpl_vars['rootUri']->value;?>
" />
<input type="hidden" id="article_id" name="article_id" value="<?php echo $_smarty_tpl->tpl_vars['LBObject']->value->id;?>
" />
<input type="hidden" id="update_star" name="update_star" value="" />
</div><?php }} ?>