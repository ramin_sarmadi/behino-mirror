<?php /* Smarty version Smarty-3.1.13, created on 2014-04-13 16:16:10
         compiled from "C:\wamp\www\ibartar2\themes\leocame\footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20896534a7902d8b436-50731451%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '620e7e339a9ae372e18b5414dde5d66b84ba15e7' => 
    array (
      0 => 'C:\\wamp\\www\\ibartar2\\themes\\leocame\\footer.tpl',
      1 => 1397285534,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20896534a7902d8b436-50731451',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'content_only' => 0,
    'LEO_LAYOUT_DIRECTION' => 0,
    'page_name' => 0,
    'HOOK_FOOTER' => 0,
    'base_dir' => 0,
    'HOOK_FOOTNAV' => 0,
    'LEO_PANELTOOL' => 0,
    'shop_name' => 0,
    'img_dir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_534a7902e77903_47874195',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534a7902e77903_47874195')) {function content_534a7902e77903_47874195($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include 'C:\\wamp\\www\\ibartar2\\tools\\smarty\\plugins\\modifier.escape.php';
?><?php if (!$_smarty_tpl->tpl_vars['content_only']->value){?>
    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./layout/".((string)$_smarty_tpl->tpl_vars['LEO_LAYOUT_DIRECTION']->value)."/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </div>

    <?php if ($_smarty_tpl->tpl_vars['page_name']->value!='index'){?>
        </div>
    <?php }?>

    </section>

    <!-- Footer -->
    <footer id="footer" class="omega clearfix">
        <section class="footer">
            <div class="container">
                <div class="row-fluid">

                    <section class="foorer-box span3">
                        <?php echo $_smarty_tpl->tpl_vars['HOOK_FOOTER']->value;?>

                    </section>
                    <section class="foorer-box span3">
                        <h3 class="footer-h3"><?php echo smartyTranslate(array('s'=>'راهنمای مشتریان'),$_smarty_tpl);?>
</h3>

                        <div class="footer-links-container">
                            <ul class="footer-ul" id="menu-shortcodes">
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
content/16-saman-insurance"><?php echo smartyTranslate(array('s'=>'خرید با بیمه'),$_smarty_tpl);?>
</a>
                                </li>
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
content/11-order-guide"><?php echo smartyTranslate(array('s'=>'راهنمای خرید'),$_smarty_tpl);?>
</a>
                                </li>
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
content/15-coupon-guide"><?php echo smartyTranslate(array('s'=>'راهنمای استفاده از کوپن(بن)'),$_smarty_tpl);?>
</a>
                                </li>
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
content/13-advanced-search-guide"><?php echo smartyTranslate(array('s'=>'راهنمای جستجوی پیشرفته'),$_smarty_tpl);?>
</a>
                                </li>
                                <li class="footer-item">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
content/14-product-groups-guide"><?php echo smartyTranslate(array('s'=>'راهنمای صفحه محصولات برند / شاخه محصول'),$_smarty_tpl);?>
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearboth"></div>
                    </section>

                    <section class="foorer-box foorer-first-box span3">
                        <h3 class="footer-h3">درباره ما</h3>

                        <div class="footer-links-container">
                            <ul class="footer-ul" id="menu-shortcodes">
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
content/4-aboutBehino">درباره ما</a>
                                </li>
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
content/2-PrivacyPolicy">سیاست حفظ حریم شخصی</a>
                                </li>

                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
content/3-TermsOfServices"><?php echo smartyTranslate(array('s'=>'شرایط و قوانین'),$_smarty_tpl);?>
</a>
                                </li>
                                <li class="footer-item footer-last">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
index.php?controller=contact"><?php echo smartyTranslate(array('s'=>'تماس با ما'),$_smarty_tpl);?>
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearboth"></div>
                    </section>
                    <section class="foorer-box span3">
                        <h3 class="footer-h3">حساب کاربری</h3>

                        <div class="footer-links-container">
                            <ul class="footer-ul" id="menu-shortcodes">
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
index.php?controller=my-account"
                                       class="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><?php echo smartyTranslate(array('s'=>'حساب کاربری شما'),$_smarty_tpl);?>
</a>
                                </li>
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
index.php?controller=history"
                                       class="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><?php echo smartyTranslate(array('s'=>'سفارشات شما'),$_smarty_tpl);?>
</a>
                                </li>
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
index.php?controller=identity"
                                       class="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><?php echo smartyTranslate(array('s'=>'اطلاعات شخصی شما'),$_smarty_tpl);?>
</a>
                                </li>
                                <li class="footer-item">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
index.php?fc=module&module=favoriteproducts&controller=account"
                                       class="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
"><?php echo smartyTranslate(array('s'=>'محصولات مورد علاقه شما'),$_smarty_tpl);?>
</a>
                                </li>
                               
                            </ul>
                        </div>
                        <div class="clearboth"></div>
                    </section>

                    
                </div>
            </div>

            
        </section>
        <section id="footer-bottom">
            
            <div class="row-fluid">
                <div class="span4">
                </div>
                <div class="span3">
                    <div class="copyright">
                        <span><?php echo smartyTranslate(array('s'=>'کلیه حقوق سایت برای بهینو محفوظ است'),$_smarty_tpl);?>
</span>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['HOOK_FOOTNAV']->value){?>
                    <div class="span5">
                        <div class="footnav"><?php echo $_smarty_tpl->tpl_vars['HOOK_FOOTNAV']->value;?>
</div>
                    </div>
                <?php }?>
            </div>
            
        </section>
    </footer>
    </div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['LEO_PANELTOOL']->value){?>
    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./info/paneltool.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }?>




<div id="phantom">
    <div class="ph-wrap with-logo">
        <div class="ph-wrap-content">
            <div class="container">
                <div class="row-fluid">
                    <div class="ph-wrap-inner">
                        <div class="logo-box span1">
                            <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['shop_name']->value, 'htmlall', 'UTF-8');?>
">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
logo-behino.png" alt="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['shop_name']->value, 'htmlall', 'UTF-8');?>
"/>
                            </a>
                        </div>
                        <div class="search-box span5 offset1">
                            <div class="nav-item block-search-top">
                                <div id="search_block_top" class="search-class">
                                    <form id="searchbox" action="http://localhost/ibartar2/index.php?controller=search"
                                          method="get">
                                        <label for="search_query_top"><!-- image on background --></label>
                                        <input type="hidden" value="search" name="controller">
                                        <input type="hidden" value="position" name="orderby">
                                        <input type="hidden" value="desc" name="orderway">
                                        <input type="text" autocomplete="off" value="" name="search_query"
                                               id="search_query_top" class="search_query">
                                        <input type="submit" class="button" value="جستجو" name="submit_search">
                                    </form>
                                </div>
                                <div style="display: none;top: 34px;" class="search-results">
                                    <div class="search-products">
                                        <h3 class="search-h3">محصولات</h3>
                                        <ul class="products-append">
                                        </ul>
                                    </div>
                                    <div class="search-video">
                                        <h3 class="search-h3">ویدئو ها</h3>
                                        <ul class="video-append">
                                        </ul>
                                        <h3 class="search-h3">اخبار</h3>
                                        <ul class="article-append">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="menu-box span4 offset1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-47616653-2', 'behino.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
<?php }} ?>