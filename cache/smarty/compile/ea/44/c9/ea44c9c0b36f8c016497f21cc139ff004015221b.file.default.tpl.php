<?php /* Smarty version Smarty-3.1.13, created on 2014-04-17 15:36:26
         compiled from "C:\wamp\www\ibartar2\modules\lofdeal\tmpl\default\default.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16161534a790248e885-08588880%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ea44c9c0b36f8c016497f21cc139ff004015221b' => 
    array (
      0 => 'C:\\wamp\\www\\ibartar2\\modules\\lofdeal\\tmpl\\default\\default.tpl',
      1 => 1397732591,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16161534a790248e885-08588880',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_534a790256a8d5_66351339',
  'variables' => 
  array (
    'lofdeal_products' => 0,
    'item' => 0,
    'lofdeal_show_des' => 0,
    'lofdeal_img_size' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534a790256a8d5_66351339')) {function content_534a790256a8d5_66351339($_smarty_tpl) {?><div class="span4 deal-span">
    <div class="row-fluid deal-row">
        <span class="sug-title-text"><?php echo smartyTranslate(array('s'=>'فروش ویژه به مناسبت روز زن'),$_smarty_tpl);?>
</span>

        <div class="sug-car-contr">
            <a id="deal-left"></a>
            <a id="deal-right"></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12 carousel-deal">
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['lofdeal_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['foo']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['foo']['index']++;
?>
                <div class="deal-item">
                    <div class="row-fluid">
                        <div class="deal-info">
                            <h4 class="time"><a title="<?php echo $_smarty_tpl->tpl_vars['item']->value['description'];?>
"
                                                href="<?php echo $_smarty_tpl->tpl_vars['item']->value['link'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></h4>
                            <?php if ($_smarty_tpl->tpl_vars['lofdeal_show_des']->value){?>
                                <div class="des"><?php echo $_smarty_tpl->tpl_vars['item']->value['description'];?>
</div>
                            <?php }?>
                        </div>
                        <div class="deal-img">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['item']->value['link_rewrite'],$_smarty_tpl->tpl_vars['item']->value['id_image'],$_smarty_tpl->tpl_vars['lofdeal_img_size']->value);?>
"
                                 alt="<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
"/>
                        </div>
                    </div>
                    <div class="row-fluid deal-price">
                        
                        
                        <div class="deal-new-price">
                                 <span class="lof-sale">
                                <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['item']->value['price']),$_smarty_tpl);?>

                                </span>
                        </div>
                        <div class="deal-old-price">
                                    <span class="lof-price">
                                <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['item']->value['price_without_reduction']),$_smarty_tpl);?>

                            </span>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-top: 40px !important;">
                        <div class="span12" style="text-align: center">
                            <span class="deal-time-text"><?php echo smartyTranslate(array('s'=>'زمان باقی مانده:'),$_smarty_tpl);?>
</span>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="deal-clock lof-clock-<?php echo $_smarty_tpl->tpl_vars['item']->value['id_product'];?>
-<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['foo']['index'];?>
">
                                <?php if ($_smarty_tpl->tpl_vars['item']->value['js']=='unlimited'){?>
                                    <div class="lof-labelexpired">
                                        <?php echo smartyTranslate(array('s'=>'Unlimited','mod'=>'lofdeal'),$_smarty_tpl);?>

                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="lof-li-wrapper">
                        <?php if ($_smarty_tpl->tpl_vars['item']->value['js']!='unlimited'){?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var date = '<?php echo $_smarty_tpl->tpl_vars['item']->value['js']['month'];?>
' + '/' + '<?php echo $_smarty_tpl->tpl_vars['item']->value['js']['day'];?>
' + '/' + '<?php echo $_smarty_tpl->tpl_vars['item']->value['js']['year'];?>
' + ' ' + '<?php echo $_smarty_tpl->tpl_vars['item']->value['js']['hour'];?>
' + ':' + '<?php echo $_smarty_tpl->tpl_vars['item']->value['js']['minute'];?>
' + ':' + '<?php echo $_smarty_tpl->tpl_vars['item']->value['js']['seconds'];?>
';
                                    var NY1 = Math.round((new Date(date)).getTime() / 1000);
                                    $('.lof-clock-<?php echo $_smarty_tpl->tpl_vars['item']->value['id_product'];?>
-<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['foo']['index'];?>
').flipcountdown({
                                        
                                        size: "sm",
                                        tick: function () {
                                            var nol = function (h) {
                                                return h > 9 ? h : '0' + h;
                                            }
                                            var range = NY1 - Math.round((new Date()).getTime() / 1000),
                                                    secday = 86400, sechour = 3600,
                                                    days = parseInt(range / secday),
                                                    hours = parseInt((range % secday) / sechour),
                                                    min = parseInt(((range % secday) % sechour) / 60),
                                                    sec = ((range % secday) % sechour) % 60;
                                            return nol(days) + ' ' + nol(hours) + ' ' + nol(min) + ' ' + nol(sec);
                                        }
                                    });
                                    $('.carousel-deal').carouFredSel({
                                        items: 1,
                                        direction: "right",
                                        width: 381,
                                        scroll: {
                                            items: 1,
                                            duration: 1000,
                                            pauseOnHover: true
                                        },
                                        auto: false,
                                        responsive: true,
                                        prev: $('#deal-left'),
                                        next: $('#deal-right')
                                    });
                                });
                                
                            </script>
                        <?php }?>
                    </div>
                    
                </div>
            <?php } ?>
        </div>


        
        
        
        
        
        

        
        
        
        
        
        
        
        
        
    </div>
</div>

<?php }} ?>