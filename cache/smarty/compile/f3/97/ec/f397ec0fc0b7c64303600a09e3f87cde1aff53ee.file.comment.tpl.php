<?php /* Smarty version Smarty-3.1.13, created on 2014-02-12 18:41:44
         compiled from "C:\wamp\www\ibartar2\modules\lofblogs\themes\default\content\comment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:861152fb8f3006c056-03295936%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f397ec0fc0b7c64303600a09e3f87cde1aff53ee' => 
    array (
      0 => 'C:\\wamp\\www\\ibartar2\\modules\\lofblogs\\themes\\default\\content\\comment.tpl',
      1 => 1385292591,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '861152fb8f3006c056-03295936',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'editor_config' => 0,
    'comments_list' => 0,
    'customer' => 0,
    'config' => 0,
    'captchar_uri' => 0,
    'LBObject' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52fb8f30115ab1_88086674',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52fb8f30115ab1_88086674')) {function content_52fb8f30115ab1_88086674($_smarty_tpl) {?><?php echo $_smarty_tpl->tpl_vars['editor_config']->value;?>

<div id="lofcontent_comments" class="lofcontent_block">
    <div id="display_errors"></div>    
    <h3 class="article_subheader"><span><?php echo smartyTranslate(array('s'=>'Comments','mod'=>'lofblogs'),$_smarty_tpl);?>
</span></h3>
    <div id="view_comment_list" >        
    <?php if ($_smarty_tpl->tpl_vars['comments_list']->value){?><?php echo $_smarty_tpl->tpl_vars['comments_list']->value;?>
<?php }?>    
    </div>
<div class="item_comment_form">
    <div class="loading_overlay"></div>
    <h3 class="article_subheader" ><span><?php echo smartyTranslate(array('s'=>'Leave a comment','mod'=>'lofblogs'),$_smarty_tpl);?>
</span></h3>
    <form action="" method="post" name="submit_comment" >
        <div class="lofcontent_line">
            <label  for="name"><?php echo smartyTranslate(array('s'=>'Your name','mod'=>'lofblogs'),$_smarty_tpl);?>
</label> 
            <div class="lofcontent_right_col">
                <div class="form_error_tip"><?php echo smartyTranslate(array('s'=>'Enter your name','mod'=>'lofblogs'),$_smarty_tpl);?>
</div>
                <input id="cm_name" class="validate_required" name="name" type="text" value="<?php echo $_smarty_tpl->tpl_vars['customer']->value['fullname'];?>
" />
            </div>

        </div>
        <div class="lofcontent_line">
            <label for="email"><?php echo smartyTranslate(array('s'=>'Your Email','mod'=>'lofblogs'),$_smarty_tpl);?>
</label>
            <div class="lofcontent_right_col">
                <input id="cm_email" name="email" type="text" value="<?php echo $_smarty_tpl->tpl_vars['customer']->value['email'];?>
" />
            </div>
        </div>
        <div class="lofcontent_line">
            <label for="website"><?php echo smartyTranslate(array('s'=>'Your website','mod'=>'lofblogs'),$_smarty_tpl);?>
</label>
            <div class="lofcontent_right_col">
                <input id="cm_website" name="website" type="text" value="" />     
            </div>
        </div>        
        <div class="lofcontent_line">
            <label for="content"><?php echo smartyTranslate(array('s'=>'Comment','mod'=>'lofblogs'),$_smarty_tpl);?>
</label>
            <div class="lofcontent_right_col" id="comment_area_col">
                <div class="form_error_tip"><?php echo smartyTranslate(array('s'=>'Enter your comment','mod'=>'lofblogs'),$_smarty_tpl);?>
</div>
                <textarea id="cm_content" class="rte" name="content" cols="40" rows="10" ></textarea>                
            </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['config']->value['showCaptcha']){?>
        <div class="lofcontent_line">
            <label for="website">&nbsp;</label>
            <div class="lofcontent_right_col" id="captcha_container">
                <img src="<?php echo $_smarty_tpl->tpl_vars['captchar_uri']->value;?>
get_captcha.php" alt="" id="captcha" />       
                <img src="<?php echo $_smarty_tpl->tpl_vars['captchar_uri']->value;?>
refresh.png" alt="" id="refresh_captcha" onClick="change_captcha();" />   
            </div>
        </div> 
        <div class="lofcontent_line">
            <label for="website"><?php echo smartyTranslate(array('s'=>'Confirm code','mod'=>'lofblogs'),$_smarty_tpl);?>
</label>
            <div class="lofcontent_right_col">
                <div class="form_error_tip"><?php echo smartyTranslate(array('s'=>'Enter captcha code above','mod'=>'lofblogs'),$_smarty_tpl);?>
</div>
                <input id="cm_captcha_validate" class="validate_required" name="captcha_validate" type="text" value="" />   
            </div>
        </div>  
        <input type="hidden" name="captcha_uri" id="captcha_uri" value="<?php echo $_smarty_tpl->tpl_vars['captchar_uri']->value;?>
get_captcha.php" />
        <?php }?>
        <div class="lofcontent_line" id="button_container" >
            <a href="javascript:void(0)" onClick="updateComments('<?php echo $_smarty_tpl->tpl_vars['LBObject']->value->id;?>
')" ><?php echo smartyTranslate(array('s'=>'Send','mod'=>'lofblogs'),$_smarty_tpl);?>
</a>
        </div>                        
       
        
    </form>
</div>
</div>
<?php }} ?>