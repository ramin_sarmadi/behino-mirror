<?php /* Smarty version Smarty-3.1.13, created on 2014-03-03 12:52:53
         compiled from "C:\wamp\www\ibartar2\modules\popularityspecialproduct\popularityspecialproduct.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3180652fb8651290dc8-47496013%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d4988ceb4647201c7e86d9c6e598ba5363fef11' => 
    array (
      0 => 'C:\\wamp\\www\\ibartar2\\modules\\popularityspecialproduct\\popularityspecialproduct.tpl',
      1 => 1393157453,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3180652fb8651290dc8-47496013',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52fb865139be45_03570534',
  'variables' => 
  array (
    'IconSpecialProductImage' => 0,
    'SpecialProductImage' => 0,
    'LinkSave' => 0,
    'ResultSpecialProductArr' => 0,
    'ProductArr' => 0,
    'Product' => 0,
    'SpecialType' => 0,
    'SpecialTypes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52fb865139be45_03570534')) {function content_52fb865139be45_03570534($_smarty_tpl) {?><fieldset>
    <legend><img src="<?php echo $_smarty_tpl->tpl_vars['IconSpecialProductImage']->value;?>
"/> <?php echo $_smarty_tpl->tpl_vars['SpecialProductImage']->value;?>
 </legend>
    <form action="<?php echo $_smarty_tpl->tpl_vars['LinkSave']->value;?>
" method="post" enctype="multipart/form-data">
        <input name="id_product_special" value="<?php echo $_smarty_tpl->tpl_vars['ResultSpecialProductArr']->value[0]['id_product_special'];?>
" type="hidden">

        <div style="padding: 5px">
            <span class="span_bold_12"><?php echo smartyTranslate(array('s'=>'Special Product Name','mod'=>'popularityspecialproduct'),$_smarty_tpl);?>
:</span>
            <input name="special_product_name" value="<?php echo $_smarty_tpl->tpl_vars['ResultSpecialProductArr']->value[0]['name'];?>
" maxlength="2000"
                   style="width: 280px">
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12"><?php echo smartyTranslate(array('s'=>'Product Name','mod'=>'popularityspecialproduct'),$_smarty_tpl);?>
:</span>
            <select name="id_product" dir="ltr">
                <?php  $_smarty_tpl->tpl_vars['Product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['Product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ProductArr']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['Product']->key => $_smarty_tpl->tpl_vars['Product']->value){
$_smarty_tpl->tpl_vars['Product']->_loop = true;
?>
                    <option style="<?php echo $_smarty_tpl->tpl_vars['Product']->value['BgColor'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['Product']->value['id_product'];?>
"
                            <?php if ($_smarty_tpl->tpl_vars['Product']->value['id_product']==$_smarty_tpl->tpl_vars['ResultSpecialProductArr']->value[0]['id_product']){?> selected="selected" <?php }?>
                            ><?php echo $_smarty_tpl->tpl_vars['Product']->value['id_product'];?>
. <?php echo $_smarty_tpl->tpl_vars['Product']->value['name'];?>
  //--> Count like: <?php echo $_smarty_tpl->tpl_vars['Product']->value['COUNT_PRD'];?>
 </option>
                <?php } ?>
            </select>
            <?php if ($_smarty_tpl->tpl_vars['ResultSpecialProductArr']->value[0]['id_product']>0){?><span style="font-size: 10px; font-style: italic"><?php echo smartyTranslate(array('s'=>'If the option is not selected, it is possible to disable the product','mod'=>'popularityspecialproduct'),$_smarty_tpl);?>
</span>  <?php }?>
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12"><?php echo smartyTranslate(array('s'=>'Special Product Type','mod'=>'popularityspecialproduct'),$_smarty_tpl);?>
:</span>
            <select name="id_product_special_type" dir="ltr">
                <option value="-1">...</option>
                <?php  $_smarty_tpl->tpl_vars['SpecialTypes'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['SpecialTypes']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['SpecialType']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['SpecialTypes']->key => $_smarty_tpl->tpl_vars['SpecialTypes']->value){
$_smarty_tpl->tpl_vars['SpecialTypes']->_loop = true;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['SpecialTypes']->value['id_product_special_type'];?>
"
                            <?php if ($_smarty_tpl->tpl_vars['SpecialTypes']->value['id_product_special_type']==$_smarty_tpl->tpl_vars['ResultSpecialProductArr']->value[0]['id_product_special_type']){?> selected="selected" <?php }?> >
                        <?php echo $_smarty_tpl->tpl_vars['SpecialTypes']->value['id_product_special_type'];?>
. <?php echo $_smarty_tpl->tpl_vars['SpecialTypes']->value['name'];?>

                    </option>
                <?php } ?>
            </select>
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12"><?php echo smartyTranslate(array('s'=>'Duration (Day) ','mod'=>'popularityspecialproduct'),$_smarty_tpl);?>
:</span>
            <input name="duration_days" value="<?php echo $_smarty_tpl->tpl_vars['ResultSpecialProductArr']->value[0]['duration_days'];?>
" maxlength="3"
                   style="width: 50px">
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12"><?php echo smartyTranslate(array('s'=>'Description','mod'=>'popularityspecialproduct'),$_smarty_tpl);?>
:</span>
            <textarea cols="50" rows="3" name="description"><?php echo $_smarty_tpl->tpl_vars['ResultSpecialProductArr']->value[0]['description'];?>
</textarea>
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12"><?php echo smartyTranslate(array('s'=>'Image File','mod'=>'popularityspecialproduct'),$_smarty_tpl);?>
:</span>
            <input type="file" name="ImageFile">
        </div>
        <div style="padding: 5px">date_start
            <span class="span_bold_12"><?php echo smartyTranslate(array('s'=>'Available from','mod'=>'popularityspecialproduct'),$_smarty_tpl);?>
:</span>
            <input class="datepicker" type="text" name="date_start"
                   value="<?php echo $_smarty_tpl->tpl_vars['ResultSpecialProductArr']->value[0]['date_start_h'];?>
" style="text-align: center" id="sp_from"/>
        </div>
        <div style="padding-left: 150px; padding-top: 10px">
            <input type="submit" value="<?php echo smartyTranslate(array('s'=>'Save','mod'=>'popularityspecialproduct'),$_smarty_tpl);?>
" class="button" name="AddProductLink">
        </div>
    </form>
</fieldset>
<script type="text/javascript">
    $(document).ready(function () {
        $('#leave_bprice').click(function () {
            if (this.checked)
                $('#sp_price').attr('disabled', 'disabled');
            else
                $('#sp_price').removeAttr('disabled');
        });
        $('.datepicker').datepicker({
            prevText: '',
            nextText: '',
            dateFormat: 'yy-mm-dd',
            // Define a custom regional settings in order to use PrestaShop translation tools
            currentText: '<?php echo smartyTranslate(array('s'=>'Now'),$_smarty_tpl);?>
',
            closeText: '<?php echo smartyTranslate(array('s'=>'Done'),$_smarty_tpl);?>
'
        });
    });
</script><?php }} ?>