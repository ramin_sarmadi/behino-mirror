<?php /* Smarty version Smarty-3.1.13, created on 2014-04-12 09:23:01
         compiled from "C:\wamp\www\ibartar2\modules\blocksearch\blocksearch-top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2703852f35b5c8cd9c3-61237104%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e810710f9434ec0e78e84308cca12a2f754058b6' => 
    array (
      0 => 'C:\\wamp\\www\\ibartar2\\modules\\blocksearch\\blocksearch-top.tpl',
      1 => 1396099424,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2703852f35b5c8cd9c3-61237104',
  'function' => 
  array (
  ),
  'cache_lifetime' => 31536000,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_52f35b5c9640a3_80782849',
  'variables' => 
  array (
    'hook_mobile' => 0,
    'link' => 0,
    'ENT_QUOTES' => 0,
    'img_dir' => 0,
    'img_prod_dir' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52f35b5c9640a3_80782849')) {function content_52f35b5c9640a3_80782849($_smarty_tpl) {?><?php if (isset($_smarty_tpl->tpl_vars['hook_mobile']->value)){?>
    <div class="input_search" data-role="fieldcontain">
        <form method="get" action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('search');?>
" id="searchbox">
            <input type="hidden" name="controller" value="search"/>
            <input type="hidden" name="orderby" value="position"/>
            <input type="hidden" name="orderway" value="desc"/>
            <input class="search_query" type="search" id="search_query_top" name="search_query"
                   placeholder="<?php echo smartyTranslate(array('s'=>'Search','mod'=>'blocksearch'),$_smarty_tpl);?>
"
                   value="<?php if (isset($_GET['search_query'])){?><?php echo stripslashes(htmlentities($_GET['search_query'],$_smarty_tpl->tpl_vars['ENT_QUOTES']->value,'utf-8'));?>
<?php }?>"/>
        </form>
    </div>
<?php }else{ ?>
    <!-- Block search module TOP -->
    <div class="nav-item block-search-top">
        <div id="search_block_top" class="search-class">
            <form method="get" action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('search');?>
" id="searchbox">
                <label for="search_query_top"><!-- image on background --></label>
                <input type="hidden" name="controller" value="search"/>
                <input type="hidden" name="orderby" value="position"/>
                <input type="hidden" name="orderway" value="desc"/>
                <input class="search_query" type="text" id="search_query_top" name="search_query"
                       value="<?php if (isset($_GET['search_query'])){?><?php echo stripslashes(htmlentities($_GET['search_query'],$_smarty_tpl->tpl_vars['ENT_QUOTES']->value,'utf-8'));?>
<?php }?>"
                       autocomplete="off"
                       placeholder="<?php echo smartyTranslate(array('s'=>'جستجوی سریع','mod'=>'blocksearch'),$_smarty_tpl);?>
"
                       />
                <input type="submit" name="submit_search" value="<?php echo smartyTranslate(array('s'=>'Search','mod'=>'blocksearch'),$_smarty_tpl);?>
" class="button"/>
            </form>
        </div>
        <div class="search-results" style="display: none">
            <div class="search-products">
                <h3 class="search-h3" >محصولات</h3>
                    <ul class="products-append">
                    </ul>
            </div>
            <div class="search-video">
                <h3 class="search-h3" >ویدئو ها</h3>

                <ul class="video-append">
                </ul>
                <h3 class="search-h3" >اخبار</h3>
                <ul class="article-append">
                </ul>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var AjaxProgrees = '';
            $(".block-search-top").each(function(){
                var th = $(this);
                th.find('#search_query_top').keyup(function () {
                    if (AjaxProgrees){
                        AjaxProgrees.abort();
                    }
                    var lengthOfSearch = th.find("#search_query_top").val().length;
                    if (lengthOfSearch > 1) {
                        $(".products-append, .video-append, .article-append").empty();
                        th.find('.search-results').fadeIn();
                        th.find('.products-append').html('<img id="retrive" src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
loading.gif">');
                        AjaxProgrees = $.ajax({
                            url: '<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('search',true);?>
',
                            data: {
                                ajaxSearch: '1',
                                id_lang: '1',
                                limit: 20,
                                q: th.find("#search_query_top").val()
                            },
                            type: 'POST',
                            dataType: "json",
                            success: function (data) {
                                $('#retrive').hide();
                                $(".products-append, .video-append, .article-append").empty();
                                if(data.resultErrProduct != 'Not Find Product' ){
                                    for (var k in data.resultProduct) {

                                        if (data.resultProduct[k]['idimg'] > 0) {
                                            var idimage = data.resultProduct[k]['idimg'];
                                            var ArrIdImage = idimage.split("");
                                            var CountArrIdImage = ArrIdImage.length;
                                            var Base_dir = '<?php echo $_smarty_tpl->tpl_vars['img_prod_dir']->value;?>
';
                                            var UrlImageId = Base_dir;
                                            for (var j = 0; j < CountArrIdImage; j++) {
                                                UrlImageId = UrlImageId + '/' + ArrIdImage[j];
                                            }
                                            UrlImageId = UrlImageId + '/' + idimage + '-medium_allinmart.jpg';
                                        }
                                        else{
                                            UrlImageId = '<?php echo $_smarty_tpl->tpl_vars['img_prod_dir']->value;?>
' + 'search-no.jpg';
                                        }


                                        var pName = data.resultProduct[k]['pname'];
                                        pName = pName.substring(0, 40);

                                        var priceContent = '';
                                        if((data.resultProduct[k]['min_price'] != '')&&(data.resultProduct[k]['min_price'] != null)&&(data.resultProduct[k]['min_price'] != '0'))
                                        {
                                            priceContent = '<span class="price-unit">تومان</span><span class="qseasrch-price">';
                                            priceContent = priceContent+data.resultProduct[k]['min_price'];
                                            priceContent = priceContent+'</span><span class="start-from"><?php echo smartyTranslate(array('s'=>'شروع از:'),$_smarty_tpl);?>
</span>';

                                        }
//
                                        else if((data.resultProduct[k]['quantity'] != '')&&(data.resultProduct[k]['quantity'] <= '0'))
                                        {

                                            priceContent = '<span class="unavailable-text"><?php echo smartyTranslate(array('s'=>'ناموجود'),$_smarty_tpl);?>
</span><img class="unavailable-img" src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
unavailable.png">';
                                        }


                                        th.find('.products-append').append(
                                                '<li class="Psearch-li">'
                                                        + '<a href="'
                                                        + data.resultProduct[k]['product_link']
                                                        + ' ">'
                                                        + '<img class="res-product-img" src="' + UrlImageId + '" >'
                                                        + '<span class="qsearch-price-name"> '
                                                        + '<span class="qsearch-name">'
                                                        + pName
                                                        + '</span>'
                                                        + priceContent
                                                        + '</a>'
                                                        + '</li>'
                                        );
                                    }
                                }
                                else{
                                    th.find('.products-append ').html('محصولی وجود ندارد.');
                                }
                                if( data.resultErrVideo != 'Not Find Video' ){
                                    for (var k in data.resultVideo) {
                                        th.find('.video-append').append(
                                                '<li class="Psearch-li">'
                                                        + '<a href="' + baseDir + 'index.php'
                                                        + data.resultVideo[k]['video_link']
                                                        + ' "><span>'
                                                        + data.resultVideo[k]['pvname']
                                                        + '</span>'
                                                        + '<img class="res-product-img" src="' + baseDir + 'upload/video/thumbnails/' + data.resultVideo[k]['pvimage'] + '" >'
                                                        + '</a>'
                                                        + '</li>'
                                        );
                                    }
                                }
                                else{
                                    th.find('.video-append ').html('<span class="search-no">ویدئویی وجود ندارد.</span>');
                                }

                                if(data.resultErrArticle != 'Not Find Article'){
                                    for (var k in data.resultArticle) {
                                        th.find('.article-append').append(
                                                '<li class="searchVideo-li">'
                                                        + '<a href="' + baseDir + 'index.php'
                                                        + data.resultArticle[k]['article_link']
                                                        + ' "><p class="search-news">'
                                                        + data.resultArticle[k]['title']
                                                        + '</p>'
                                                        + '</a>'
                                                        + '</li>'
                                        );
                                    }
                                }
                                else{
                                    th.find('.article-append').html('اخبار مرتبطی وجود ندارد.');
                                }

                                th.find('.products-append').mCustomScrollbar({
                                    set_height: 300,
                                    theme: 'dark'
                                    /*scrollButtons:{
                                     enable:true
                                     }*/
                                });
                            }
                        });
                    }
                    else {
                        $('.search-results').fadeOut();
                    }
                });
            })


            $(document).click(function(){
                 $('.search-results').fadeOut();
            });
            $('.search-results').click(function(event){
                event.stopPropagation();
            });
        });
    </script>
  <?php }?>
<!-- /Block search module TOP -->
<?php }} ?>