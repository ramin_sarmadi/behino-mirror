<?php /* Smarty version Smarty-3.1.13, created on 2014-03-16 13:07:34
         compiled from "C:\wamp\www\ibartar2\modules\pspcustomform\views\templates\front\form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7859532570ded6f865-52472460%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6680f9f68a12ac920ff01281eb8f02a8a5744f02' => 
    array (
      0 => 'C:\\wamp\\www\\ibartar2\\modules\\pspcustomform\\views\\templates\\front\\form.tpl',
      1 => 1394710288,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7859532570ded6f865-52472460',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'step' => 0,
    'firstname' => 0,
    'lastname' => 0,
    'id_gender' => 0,
    'email' => 0,
    'phone_mobile' => 0,
    'tours' => 0,
    'tour' => 0,
    'bestday' => 0,
    'price' => 0,
    'paid' => 0,
    'id_member' => 0,
    'link' => 0,
    'confirmed' => 0,
    'yourmail' => 0,
    'member' => 0,
    'customer' => 0,
    'validated' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_532570df3e2e21_95651657',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_532570df3e2e21_95651657')) {function content_532570df3e2e21_95651657($_smarty_tpl) {?>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'ثبت نام دوره آموزشی پرستاشاپ','mod'=>'pspcustomform'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>




<h2><?php echo smartyTranslate(array('s'=>'فرم ثبت نام'),$_smarty_tpl);?>
</h2>



<div class="block-center" id="block-history"></div>



<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>




<?php if ($_smarty_tpl->tpl_vars['step']->value=='validate'){?>

	<h4>مرحله چهارم - تأیید اطلاعات و پرداخت</h4>

	<br/>

	<p class="conf confirmation">نکته مهم: چنانچه پرداخت را انجام ندهید اطلاعات شما محفوظ خواهد ماند اما  طبیعی است با توجه به محدودیت در تعداد شرکت کنندگان، اولویت با افرادی است که زودتر از سایرین هزینه را پرداخته اند.</p>

	<p>در صورتی که اطلاعات زیر مورد تأیید شماست روی دکمه پرداخت کلیک کنید</p>

	<p>نام: <?php echo $_smarty_tpl->tpl_vars['firstname']->value;?>
</p>

	<p>نام خانوادگی: <?php echo $_smarty_tpl->tpl_vars['lastname']->value;?>
</p>

	<p>جنسیت: <?php if ($_smarty_tpl->tpl_vars['id_gender']->value==1){?>آقا<?php }else{ ?>خانوم<?php }?> </p>

	<p>ایمیل: <?php echo $_smarty_tpl->tpl_vars['email']->value;?>
</p>

	<p>شماره موبایل: <?php echo $_smarty_tpl->tpl_vars['phone_mobile']->value;?>
</p>

	<p>جلسات ثبت نام کرده:

	<?php  $_smarty_tpl->tpl_vars["tour"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["tour"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tours']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["tour"]->key => $_smarty_tpl->tpl_vars["tour"]->value){
$_smarty_tpl->tpl_vars["tour"]->_loop = true;
?>

	<?php if ($_smarty_tpl->tpl_vars['tour']->value[0]=='A'){?>مبتدی<?php }else{ ?>پیشرفته<?php }?> - 

	<?php if ($_smarty_tpl->tpl_vars['tour']->value[1]=='A'){?>هفته اول<?php }else{ ?>هفته دوم<?php }?> - 

	<?php if ($_smarty_tpl->tpl_vars['tour']->value[2]=='A'){?>روز اول(پنجشنبه)<?php }else{ ?>روز دوم(جمعه)<?php }?> // 

	<?php } ?>

	</p>

	<p>در صورت لغو یکی از تاریخ ها امکان شرکت در <?php if ($_smarty_tpl->tpl_vars['bestday']->value==1){?>فقط 21 و 22 شهریور<?php }elseif($_smarty_tpl->tpl_vars['bestday']->value==2){?>فقط 28 و 29 شهریور<?php }else{ ?> هر دو هفته اعلام شده<?php }?> را دارم.</p>

	<p>جمع هزینه ثبت نام: <?php echo $_smarty_tpl->tpl_vars['price']->value;?>
</p>

	<?php if ($_smarty_tpl->tpl_vars['paid']->value==1){?>

		<p>شما هزینه ثبت نام را پرداخت کرده اید</p>

	<?php }else{ ?>

		<form action="https://www.zarinpal.com/webservice/Simplepay" method="post">

			<input type="hidden" value="zp.30025" name="data[Transaction][account_id]">

			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['price']->value;?>
"  name="data[Transaction][amount]">

			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['id_member']->value;?>
 -  <?php  $_smarty_tpl->tpl_vars['tour'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tour']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tours']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tour']->key => $_smarty_tpl->tpl_vars['tour']->value){
$_smarty_tpl->tpl_vars['tour']->_loop = true;
?><?php echo $_smarty_tpl->tpl_vars['tour']->value;?>
 - <?php } ?><?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" name="data[Transaction][desc]">

			<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('pspcustomform','default',array('step'=>'payment'));?>
" name="data[Transaction][redirect_url]">

			<input name="submit" type="submit" class="button" style="font-family: Tahoma;font-size:9pt"  value="پـرداخـت ">



		</form>
	<a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('pspcustomform','default',array('step'=>'register','action'=>'edit'));?>
" class="button">اصلاح مشخصات</a>

	<?php }?>

	<br />




<?php }elseif(isset($_smarty_tpl->tpl_vars['confirmed']->value)&&$_smarty_tpl->tpl_vars['step']->value=='register'){?>

	<?php if (isset($_POST['confirm_code'])){?><p class="confirmation">ایمیل شما تأیید شد</p><?php }?>

	<p class="required"><sup>*</sup> <?php echo smartyTranslate(array('s'=>'فیلدهای ضروری'),$_smarty_tpl);?>
</p>



	<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('pspcustomform','default',array('step'=>'register'));?>
" method="post" class="std" id="account-creation_form">

	<h3><?php echo smartyTranslate(array('s'=>'مرحله سوم - ثبت اطلاعات','mod'=>'pspcustomform'),$_smarty_tpl);?>
</h3>

		<fieldset>

			<legend>اطلاعات شخصی</legend>

			<p class="required text">

				<label for="email"><?php echo smartyTranslate(array('s'=>'ایمیل شما:','mod'=>'pspcustomform'),$_smarty_tpl);?>
</label>

				<input type="text" id="email" name="showemail" value="<?php if (isset($_smarty_tpl->tpl_vars['yourmail']->value)){?><?php echo $_smarty_tpl->tpl_vars['yourmail']->value;?>
<?php }elseif(isset($_POST['email'])){?><?php echo $_POST['email'];?>
<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)){?><?php echo $_smarty_tpl->tpl_vars['member']->value['email'];?>
<?php }?>" disabled="disable"/>

				<input type="hidden"  name="email" value="<?php if (isset($_smarty_tpl->tpl_vars['yourmail']->value)){?><?php echo $_smarty_tpl->tpl_vars['yourmail']->value;?>
<?php }else{ ?><?php if (isset($_POST['email'])){?><?php echo $_POST['email'];?>
<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)){?><?php echo $_smarty_tpl->tpl_vars['member']->value['email'];?>
<?php }?><?php }?>" />



			</p>

			

			<p class="required text">

				<label for="firstname"><?php echo smartyTranslate(array('s'=>'نام','mod'=>'pspcustomform'),$_smarty_tpl);?>
</label>

				<input type="text" name="firstname" id="firstname" value="<?php if (isset($_POST['firstname'])){?><?php echo $_POST['firstname'];?>
<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)){?><?php echo $_smarty_tpl->tpl_vars['member']->value['firstname'];?>
<?php }else{ ?><?php if (isset($_smarty_tpl->tpl_vars['customer']->value->firstname)){?><?php echo $_smarty_tpl->tpl_vars['customer']->value->firstname;?>
<?php }?><?php }?>" />

			</p>

			<p class="required text">

				<label for="lastname"><?php echo smartyTranslate(array('s'=>'نام خانوادگی','mod'=>'pspcustomform'),$_smarty_tpl);?>
</label>

				<input type="text" id="lastname" name="lastname" value="<?php if (isset($_POST['lastname'])){?><?php echo $_POST['lastname'];?>
<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)){?><?php echo $_smarty_tpl->tpl_vars['member']->value['lastname'];?>
<?php }else{ ?><?php if (isset($_smarty_tpl->tpl_vars['customer']->value->lastname)){?><?php echo $_smarty_tpl->tpl_vars['customer']->value->lastname;?>
<?php }?><?php }?>" />

			</p>

			<p class="select">

				<label for="gender"><?php echo smartyTranslate(array('s'=>'جنسیت','mod'=>'pspcustomform'),$_smarty_tpl);?>
 </label>

				<select id="gender" name="gender">

					<option value='0'>- انتخاب -</option>

					<option value='1' <?php if (isset($_POST['gender'])&&$_POST['gender']==1){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['id_gender']==1){?>selected<?php }?>>آقا</option>

					<option value='2' <?php if (isset($_POST['gender'])&&$_POST['gender']==2){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['id_gender']==2){?>selected<?php }?>>خانم</option>

				<select>

			</p>

			<p class="required text">

				<label for="phone_mobile"><?php echo smartyTranslate(array('s'=>'شماره موبایل (مهم)','mod'=>'pspcustomform'),$_smarty_tpl);?>
 <sup>*</sup></label>

				<input type="text" id="phone_mobile" name="phone_mobile" value="<?php if (isset($_POST['phone_mobile'])){?><?php echo $_POST['phone_mobile'];?>
<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)){?><?php echo $_smarty_tpl->tpl_vars['member']->value['phone_mobile'];?>
<?php }?>" />

			</p>

			<p class=" text">

				<label for="address"><?php echo smartyTranslate(array('s'=>'محل سکونت (شهر / منطقه)','mod'=>'pspcustomform'),$_smarty_tpl);?>
</label>

				<input type="text" id="address1" name="address1" value="<?php if (isset($_POST['address1'])){?><?php echo $_POST['address1'];?>
<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)){?><?php echo $_smarty_tpl->tpl_vars['member']->value['location'];?>
<?php }?>" />

			</p>

			<p class="text">

				<label for="website"><?php echo smartyTranslate(array('s'=>'آدرس سایت شما','mod'=>'pspcustomform'),$_smarty_tpl);?>
 </label>

				<input type="text" id="website" name="website" value="<?php if (isset($_POST['website'])){?><?php echo $_POST['website'];?>
<?php }else{ ?><?php if (isset($_smarty_tpl->tpl_vars['member']->value)){?><?php echo $_smarty_tpl->tpl_vars['member']->value['website'];?>
<?php }?><?php }?>" />

			</p>

			<p class="select">

				<label for="education"><?php echo smartyTranslate(array('s'=>'میزان تحصیلات','mod'=>'pspcustomform'),$_smarty_tpl);?>
 </label>

				<select id="education" name="education">

					<option value='0'>- انتخاب -</option>

					<option value='1' <?php if (isset($_POST['education'])&&$_POST['education']==1){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['education']==1){?>selected<?php }?>>قبل از دیپلم</option>

					<option value='2' <?php if (isset($_POST['education'])&&$_POST['education']==2){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['education']==2){?>selected<?php }?>>دیپلم</option>

					<option value='3' <?php if (isset($_POST['education'])&&$_POST['education']==3){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['education']==3){?>selected<?php }?>>دانشجو</option>

					<option value='4' <?php if (isset($_POST['education'])&&$_POST['education']==4){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['education']==4){?>selected<?php }?>>فوق دیپلم</option>

					<option value='5' <?php if (isset($_POST['education'])&&$_POST['education']==5){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['education']==5){?>selected<?php }?>>لیسانس</option>

					<option value='6' <?php if (isset($_POST['education'])&&$_POST['education']==6){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['education']==6){?>selected<?php }?>>بالاتر از لیسانس</option>

				<select>

			</p>

			<p class="select">

				<label for="prestashop"><?php echo smartyTranslate(array('s'=>'میزان آشنایی با پرستاشاپ','mod'=>'pspcustomform'),$_smarty_tpl);?>
 </label>

				<select id="prestashop" name="prestashop">

					<option value='0' <?php if (isset($_POST['prestashop'])&&$_POST['prestashop']==1){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['familarity']==1){?>selected<?php }?>>- انتخاب -</option>

					<option value='1' <?php if (isset($_POST['prestashop'])&&$_POST['prestashop']==1){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['familarity']==1){?>selected<?php }?>>یک سال یا کمتر</option>

					<option value='2' <?php if (isset($_POST['prestashop'])&&$_POST['prestashop']==2){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['familarity']==2){?>selected<?php }?>>یک تا دو سال</option>

					<option value='3' <?php if (isset($_POST['prestashop'])&&$_POST['prestashop']==3){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['familarity']==3){?>selected<?php }?>>دو تا سه سال</option>

					<option value='4' <?php if (isset($_POST['prestashop'])&&$_POST['prestashop']==4){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['familarity']==4){?>selected<?php }?>>سه تا چهار سال</option>

					<option value='5' <?php if (isset($_POST['prestashop'])&&$_POST['prestashop']==5){?>selected<?php }elseif(isset($_smarty_tpl->tpl_vars['member']->value)&&$_smarty_tpl->tpl_vars['member']->value['familarity']==5){?>selected<?php }?>>چهار سال یا بیشتر</option>

				<select>

			</p>



		</fieldset>

			

		<fieldset>

			<legend>انتخاب کلاس ها</legend>

			<p class="info">نکات مهم</p>

			<p class="info">دو روز 28, 29 شهریور برای ثبت نام در نظر گرفته شده اما ممکن است فقط در یکی از این دو روز کلاس برگزار شود. بنابراین هرچند می توانید در هر دو روز ثبت نام کنید اما با در نظر گرفتن این احتمال بهترین تاریخ را انتخاب کنید.</p>

			<p class="info">در صورتی که ثبت نام در یکی از این دو روز به حد نصاب نرسد آن تاریخ لغو و ثبت نام کنندگان طبق اولویت زمان ثبت نام، امکان حضور در تاریخ ذکر شده دیگر را دارند. همچنین اگر هر دو روز به حدنصاب برسد امکان برگزاری کلاس در هر دو روز خواهد بود</p>

			<p class="info">در صورت لغو یا تغییر تاریخ انتخابی شما با شماره موبایل شما تماس خواهیم گرفت. یک بار دیگر صحت آن را بررسی کنید.<br /></p>

			<p class="info">مطالب آموزشی در دوره های هم نام در روزهای متفاوت یکسان هستند.</p>

			<p class="checkbox">

				<input type="checkbox" name="tourdaytime[]" value='ABA'> مبتدی - پنجشنبه 28 شهریور 92 - نوبت صبح<br />

				<input type="checkbox" name="tourdaytime[]" value='ABB'> مبتدی - جمعه 29 شهریور 92 - نوبت عصر<br />

				<input type="checkbox" name="tourdaytime[]" value='BBA'> پیشرفته - پنجشنبه 28 شهریور 92 - نوبت عصر<br />

				<input type="checkbox" name="tourdaytime[]" value='BBB'> پیشرفته - جمعه 29 شهریور 92 - نوبت صبح<br />

			</p>

			<p class="select">

				<label for="bestday"><?php echo smartyTranslate(array('s'=>'امکان شرکت در کدام تاریخ را دارید؟','mod'=>'pspcustomform'),$_smarty_tpl);?>
 </label>

				<select id="bestday" name="bestday">

					<option value='0'>- انتخاب -</option>

					<option value='1'>28 شهریور</option>

					<option value='2'>29 شهریور</option>

					<option value='3'>هردو تاریخ</option>

				<select>

			</p>

		</fieldset>

		<input type="submit" name="submitValidate" id="submitValidate" value="<?php echo smartyTranslate(array('s'=>'مرحله بعد','mod'=>'pspcustomform'),$_smarty_tpl);?>
" class="button" />

	</form>

<?php }elseif(isset($_smarty_tpl->tpl_vars['validated']->value)&&($_smarty_tpl->tpl_vars['step']->value=='confirm'||$_smarty_tpl->tpl_vars['step']->value=='register')){?>

	<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('pspcustomform','default',array('step'=>'register'));?>
" method="post" class="std" id="account-creation_form">

		<fieldset>

			<h3><?php echo smartyTranslate(array('s'=>'مرحله دوم - تأیید ایمیل','mod'=>'pspcustomform'),$_smarty_tpl);?>
</h3>

			

			<p class="required text">

				<label for="confirm_code"><?php echo smartyTranslate(array('s'=>'کد فعال سازی','mod'=>'pspcustomform'),$_smarty_tpl);?>
 <sup>*</sup></label>

				<input type="text" id="confirm_code" name="confirm_code" value="<?php if (isset($_POST['confirm_code'])){?><?php echo $_POST['confirm_code'];?>
<?php }?>" />

			</p>

		</fieldset>

		<input type="submit" name="submitConfirm" id="submitConfirm" value="<?php echo smartyTranslate(array('s'=>'مرحله بعد','mod'=>'pspcustomform'),$_smarty_tpl);?>
" class="button" />

	</form>

<?php }else{ ?>

	<form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getModuleLink('pspcustomform','default',array('step'=>'confirm'));?>
" method="post" class="std" id="account-creation_form">

		<fieldset>

			<h3><?php echo smartyTranslate(array('s'=>'مرحله اول - ثبت ایمیل','mod'=>'pspcustomform'),$_smarty_tpl);?>
</h3>

			

			<p class="required text">

				<label for="email"><?php echo smartyTranslate(array('s'=>'ایمیل خود را وارد کنید','mod'=>'pspcustomform'),$_smarty_tpl);?>
 <sup>*</sup></label>

				<input type="text" id="email" name="email" value="<?php if (isset($_POST['email'])){?><?php echo $_POST['email'];?>
<?php }?>" />

			</p>

		</fieldset>

		<input type="submit" name="submitEmail" id="submitEmail" value="<?php echo smartyTranslate(array('s'=>'مرحله بعد','mod'=>'pspcustomform'),$_smarty_tpl);?>
" class="button" />

	</form>

<?php }?>

<?php }} ?>