<?php

global $_LANGMAIL;
$_LANGMAIL = array();
$_LANGMAIL['Welcome!'] = 'خوش آمدید';
$_LANGMAIL['Process the payment of your order'] = 'فرآیند پرداخت سفارش شما';
$_LANGMAIL['Message from contact form\').\' [no_sync'] = '';
$_LANGMAIL['New credit slip regarding your order'] = 'رسید اعتبار جدیدی مربوط به سفارش شما';
$_LANGMAIL['Virtual product to download'] = 'محصولات مجازی برای دانلود در دسترس می باشد';
$_LANGMAIL['Fwd: Customer message'] = 'پیام مشتری';
$_LANGMAIL['Your guest account has been transformed to customer account'] = 'حساب کاربری مهمان شما با موفقیت به حساب کاربری مشتری تغییر کرد';
$_LANGMAIL['Package in transit'] = 'بسته در حال حمل است';
$_LANGMAIL['Log: You have a new alert from your shop'] = 'شما یک آگاهی جدید از فروشگاه خود دارید';
$_LANGMAIL['Your order has been changed'] = 'سفارش شما تغییر کرده است';
$_LANGMAIL['Order confirmation'] = 'تائید سفارش';
$_LANGMAIL['Message from a customer'] = 'پیام از یک مشتری';
$_LANGMAIL['New message regarding your order'] = 'پیام جدیدی مربوط به سفارش';
$_LANGMAIL['Your order return state has changed'] = 'وضعیت سفارش برگشتی شما تغییر کرده است';
$_LANGMAIL['Your new password'] = 'گذرواژه جدید شما';
$_LANGMAIL['Password query confirmation'] = 'فراموشی گذرواژه';
$_LANGMAIL['An answer to your message is available #ct%1$s #tc%2$s'] = 'یک پاسخ برای پیام شما در دسترس است';
$_LANGMAIL['New voucher regarding your order %s'] = 'بن جدید مربوط به سفارش %s';
$_LANGMAIL['Newsletter confirmation'] = 'تائید اشتراک در خبرنامه';
$_LANGMAIL['Email verification'] = 'تایید ایمیل';
$_LANGMAIL['Newsletter voucher'] = 'بن تخفیف خبرنامه';
$_LANGMAIL['Your wishlist\\\'s link'] = 'لینک فهرست علاقه مندی شما';
$_LANGMAIL['Message from %1$s %2$s'] = '';
$_LANGMAIL['Your cart and your discount'] = 'پیشنهاد تخفیف برای محصولات موجود در سبد';
$_LANGMAIL['Thanks for your order'] = 'با سپاس از سفارش شما';
$_LANGMAIL['You are one of our best customers'] = 'با سپاس از شما به عنوان بهترین مشتری ما';
$_LANGMAIL['We miss you'] = 'دلمان برای شما تنگ شده است';
$_LANGMAIL['Product available'] = 'محصول در دسترس قرار گرفت';
$_LANGMAIL['Stock coverage'] = 'پوشش موجودی';
$_LANGMAIL['Product out of stock'] = 'یک با چندی از محصولات در انبار موجود نیست';
$_LANGMAIL['Congratulations!'] = 'Congratulations!';
$_LANGMAIL['%1$s sent you a link to %2$s'] = '%1$s برای شما یک لینک فرستاد به %2$s';

?>