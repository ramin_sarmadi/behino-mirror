با سلام خدمت آقاي/خانم {firstname} {lastname},

شما پيغام جديدي از {shop_name} در رابطه با سفارش با شناسه ي {order_name} دريافت کرده ايد.

پيغام :

{message}


{shop_url}