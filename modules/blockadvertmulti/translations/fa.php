<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_ab7236667be1d9a234ed6e828744d005'] = 'بنر با موفقیت بروزرسانی گردید';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_ac50d1454f9c49fb99b7a5d99f6fe1d9'] = 'بنر با موفقیت ذخیره گردید';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_bfd60c732b030922775c0eb19e6bd907'] = 'بنر با موفقیت حذف گردید';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_df61abbe1a92fe918ea7dd47ce841387'] = 'بنر با موفقیت حذف نگردید';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_43855b46a2cae8034a0fd6461cb1a023'] = 'اضافه نمودن بنر جدید';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_ff09729bee8a82c374f6b61e14a4af76'] = 'توضیح بنر';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_605f5f0afe6bb379486a06b11cbdc528'] = 'محل نمایش بنر';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_78f92f357805eca3a79d9a71bac90f8f'] = 'شاخه مربوط به بنر';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_b694d0fabc83acd459c3b4e6a1465737'] = 'پیوند بنر';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_4b60f340cb2ae2a3c012b465f5f76d5c'] = 'اولویت';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_65066bb7705fbaeb69256c0d88aac664'] = 'محل نمایش در صفحه';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_137d2d6f01b4762e90938fbe1c053a74'] = 'تصویر';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_4d3d769b812b6faa6b76e1a8abaece2d'] = 'فعال';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_bff13b03e3e759a6dffcbfbf9abefb47'] = 'ذخیره بنر';
$_MODULE['<{blockadvertmulti}prestashop>blockadvertmulti_add_ac98a15c4e63b94fefaa821a6ae09b42'] = 'حذف بنر';
