<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_5b3e59dfc4ea628e69c3ede6a5379238'] = 'آرشیو مقالات بلاگ';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_1ebd27eabeecf9511ba426ae0b38c74c'] = 'نمایش آرشیو بلاگ شما';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_a5b17f28c22d6c09b2b8b4a6a2a7a7b0'] = 'آیا از حذف ماژول اطمینان دارید؟';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات به روز شد';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_86f5978d9b80124f509bdb71786e929e'] = 'ژانویه';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_659e59f062c75f81259d22786d6c44aa'] = 'فبریه';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_fa3e5edac607a88d8fd7ecb9d6d67424'] = 'مارس';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_3fcf026bbfffb63fb24b8de9d0446949'] = 'آوریل';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_195fbb57ffe7449796d23466085ce6d8'] = 'می';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_688937ccaf2a2b0c45a1c9bbba09698d'] = 'ژوئن';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_1b539f6f34e8503c97f6d3421346b63c'] = 'جولای';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_41ba70891fb6f39327d8ccb9b1dafb84'] = 'آگوست';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_cc5d90569e1c8313c2b1c2aab1401174'] = 'سپتامبر';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_eca60ae8611369fe28a02e2ab8c5d12e'] = 'اوکتبر';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_7e823b37564da492ca1629b4732289a8'] = 'نوامبر';
$_MODULE['<{lofblogsarchive}prestashop>lofblogsarchive_82331503174acbae012b2004f6431fa5'] = 'دسامبر';
$_MODULE['<{lofblogsarchive}prestashop>form_06933067aafd48425d67bcb01bba5cb6'] = 'به روز رسانی';
$_MODULE['<{lofblogsarchive}prestashop>params_lang_4ede758053afb49f54ea2ef0d5276910'] = 'تنظیمات عمومی';
$_MODULE['<{lofblogsarchive}prestashop>params_lang_d721757161f7f70c5b0949fdb6ec2c30'] = 'قالب';
$_MODULE['<{lofblogsarchive}prestashop>params_lang_971fd8cc345d8bd9f92e9f7d88fdf20c'] = 'پویا';
$_MODULE['<{lofblogsarchive}prestashop>params_lang_e16f248b047fb7d0c97dcc19b17296a3'] = 'فعال سازی حالت پویا (انیمه) برای زیر سطح ها';
$_MODULE['<{lofblogsarchive}prestashop>left_803410c655657addfd57eadb05e3b11d'] = 'آرشیو بلاگ';
