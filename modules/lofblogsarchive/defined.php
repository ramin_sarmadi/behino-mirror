<?php
/**
 * @name defined.php
 * @author landOfCoder
 * @todo defined some path and link
 */

// *************** SOME PATHS ******************************
define('LOFBLOGSARCHIVE_ROOT', _PS_MODULE_DIR_ . 'lofblogsarchive/');
define('LOFBLOGSARCHIVE_THEME', LOFBLOGSARCHIVE_ROOT.'themes/');

// ***************** SOME LINKS ***************************************

define('LOFBLOGSARCHIVE_BASE_URI', __PS_BASE_URI__ . 'modules/lofblogsarchive/');
define('LOFBLOGSARCHIVE_THEME_URI', LOFBLOGSARCHIVE_BASE_URI.'themes/');

?>
