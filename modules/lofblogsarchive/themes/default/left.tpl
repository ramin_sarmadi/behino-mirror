
<div id="archive_blogs_left" class="block">
    <h4>{l s='Archive blogs' mod='lofblogsarchive'}</h4>
    <div class="block_content">
        <ul class="tree {if $isDhtml}dhtml{/if}">
            {foreach from=$archive_items item=year key=key_year name=name_year}
                <li{if $smarty.foreach.name_year.last} class="last"{/if}>
                    <a href="{$lofContentHelper->getSearchLink(['year' => $key_year])}" title="{$key_year}"{if isset($current_date[$key_year]) && !is_array($current_date[$key_year])} class="selected"{/if}>{$key_year}</a>
                    <ul>
					{foreach from=$year item=month key=key_month name=name_month}
                        <li{if $smarty.foreach.name_month.last} class="last"{/if}>
                            <a href="{$lofContentHelper->getSearchLink(['year' => $key_year, 'month' => $key_month])}" title="{$key_month}"{if isset($current_date[$key_year]) && isset($current_date[$key_year][$key_month]) && !is_array($current_date[$key_year][$key_month])} class="selected"{/if}>{$archive_month[$key_month]}</a>
                            <ul>
                            {foreach from=$month item=item key=key name=name}
                                <li{if $smarty.foreach.name.last} class="last"{/if}>
                                    <a href="{$item.link}" title="{$item.title}"{if isset($current_date[$key_year]) && isset($current_date[$key_year][$key_month]) && isset($current_date[$key_year][$key_month][$item.id_lofblogs_publication])} class="selected"{/if}>{$item.title}</a>
                                </li>
                            {/foreach}
                            </ul>
                        </li>
                    {/foreach}
                    </ul>
                </li>
            {/foreach}
        </ul>
    </div>
</div>
<script type="text/javascript">
// <![CDATA[
    // we hide the tree only if JavaScript is activated
    $('div#archive_blogs_left ul.dhtml').hide();
// ]]>
</script>