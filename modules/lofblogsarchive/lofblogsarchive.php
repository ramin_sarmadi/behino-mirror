<?php

/*
 * 2011 LandOfCoder
 *
 *  @author LandOfCoder 
 *  @copyright  2011 LandOfCoder
 *  @version  Release: $Revision: 1.0 $
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
    exit;
require_once(_PS_MODULE_DIR_ . "lofblogsarchive/defined.php");
if(!defined('LOFCONTENT_IMAGES_THUMBS_URI')) {
    require_once _PS_MODULE_DIR_.'lofblogs/defined.php';
}
if (!class_exists('LOFXParams')) {
    require LOFBLOGSARCHIVE_ROOT . 'config/params.php';
}

if (!class_exists('lofContentHelper')) {
    require _PS_MODULE_DIR_ . 'lofblogs/libs/lof_content_helper.php';
}


class lofblogsarchive extends Module {
    /* @var boolean error */

    protected $error = false;
    private $_postErrors = array();

    public function __construct() {
        $this->name = 'lofblogsarchive';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'LandOfCoder';
        $this->need_instance = 0;

        parent::__construct();
        $this->params = new LOFXParams($this);
        $this->helper = new lofContentHelper();
        $this->template = $this->params->get('template', 'default');
        $this->displayName = $this->l('Lof Blogs Archive Articles');
        $this->description = $this->l('Display your Lof Blogs archive articles');
        $this->confirmUninstall = $this->l('do you realy want to uninstall Lof Blogs archive articles?');
    }

    public function install() {
        if ( parent::install() == false OR !$this->registerHook('displayLeftColumn') )
            return false;
        return true;
    }

    public function uninstall() {
        if (!parent::uninstall())
            return false;
        return true;
    }

    function hookdisplayRightColumn($params) {
        return $this->processHook($params, 'right');
    }

    function hookdisplayLeftColumn($params) {
        return $this->processHook($params, 'left');
    }

    /**
     * Render processing form && process saving data.
     */
    public function getContent() {
        $html = "";
        if (Tools::isSubmit('submit')) {
            if (is_array($this->_postErrors) && !count($this->_postErrors)) {
                $this->params->update();
                $html .= '<div class="conf confirm">' . $this->l('Settings updated') . '</div>';
            }
        }
        if ($this->params->hasError())
            die($this->params->getErrorMsg());
        
        return $html . $this->params->displayForm();
    }


    function processHook($params, $hook = 'tabproductcontent') {
        
        $this->hook = 'left';

        $this->helper->moduleMedia($this->name, $this->template);
        $filter = array();
        //switch layout :
        $order = 'id_lofblogs_publication DESC';
        $items = $this->getItems($order);
        $result = array();
        if ($items) {
            foreach ($items as $key => $value) {
                $time = strtotime($value['date_add']);
                $month = date("n",$time);
                $year = date("Y",$time);
                $result[$year][$month][] = $value;
            }
        }
        $current_date = array();
        if(Tools::getValue('controller') == 'articles'){
            if(Tools::getValue('view') == 'content'){
                if (!class_exists('LofPsblogsPublication')) {
                    require _PS_MODULE_DIR_ . 'lofblogs/classes/LofPsblogsPublication.php';
                }
                $obj = new LofPsblogsPublication((int)Tools::getValue('id'));
                if(Validate::isLoadedObject($obj)){
                    $time = strtotime($obj->date_add);
                    $year = date("Y",$time);
                    $month = date("n",$time);
                    $current_date[$year][$month][$obj->id] = 1;
                }
            }elseif (Tools::getValue('view') == 'search') {
                $year = (int)Tools::getValue('year');
                $month = (int)Tools::getValue('month');
                if($month)
                    $current_date[$year][$month] = 1;
                elseif($year)
                    $current_date[$year] = 1;
            }
        }
        $lofContentHelper = new lofContentHelper();
        $this->smarty->assign(array(
            'archive_items' => $result,
            'lofContentHelper' => $lofContentHelper,
            'archive_month' => $this->translateMonth(),
            'isDhtml' => (int)$this->params->get('dynamic'),
            'current_date' => $current_date,
        ));
        //select a layout by position :
        $layout = $this->getLayoutPath($this->hook);
        return $this->display(__FILE__, $layout);
    }

    public function translateMonth(){
        $return = array();
        $return['1'] = $this->l('January');
        $return['2'] = $this->l('February');
        $return['3'] = $this->l('March');
        $return['4'] = $this->l('April');
        $return['5'] = $this->l('May');
        $return['6'] = $this->l('June');
        $return['7'] = $this->l('July');
        $return['8'] = $this->l('August');
        $return['9'] = $this->l('September');
        $return['10'] = $this->l('October');
        $return['11'] = $this->l('November');
        $return['12'] = $this->l('December');
        return $return;
    }

    function getItems($order) {
        global $cookie;
        $lang = $cookie->id_lang ? $cookie->id_lang : $this->params->defaultLang;
        $groups = lofContentHelper::getCustomerGroups();

        $query = 'SELECT a.id_lofblogs_publication, a.date_add, al.title, al.link_rewrite';
        $query .= ' FROM ' . _DB_PREFIX_ . 'lofblogs_publication a ';
        $query .= ' LEFT JOIN ' . _DB_PREFIX_ . 'lofblogs_publication_lang al ON (a.id_lofblogs_publication = al.id_lofblogs_publication)';
        $query .= ' WHERE al.id_lang = ' . pSQL($lang) 
                . ' AND a.status = '.$this->helper->sqlQuote('published');
        $access = array();
        foreach($groups as $id_group){
            $access[] = $id_group . ' IN (a.access)';
        }
        if(count($access))
            $query .= ' AND ('. implode(' OR ', $access) .') ';
        //set order :
        $query .= ' ORDER BY ' . $order;

        $items = Db::getInstance()->ExecuteS($query);
        foreach ($items as $k => $item) {
            $items[$k]['link'] = lofContentHelper::getArticleLink($item['id_lofblogs_publication'], $item['link_rewrite']);
        }
        return $items;
    }

    public function getLayoutPath($layout = 'default') {
        $theme = $this->params->get('template', 'default');
        $layoutPath = LOFBLOGSARCHIVE_THEME . $theme . '/' . $layout . '.tpl';

        if (!file_exists($layoutPath)) {
            return 'themes/' . $theme . '/default.tpl';
        } else {
            return 'themes/' . $theme . '/' . $layout . '.tpl';
        }
    }

    function getParam($name, $default = '') {
        $paramName = $this->hook . ucfirst($name);
        return $this->params->get($paramName, $default);
    }

}
