<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsregistrations}prestashop>statsregistrations_8b15fc6468c919d299f9a601b61b95fc'] = 'حساب های مشتری';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_5eaafc713c2425b16202065927ea0220'] = 'نمایش پیشرفت ثبت نام مشتری.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_247b3bdef50a59d5a83f23c4f1c8fa47'] = 'بازدید کنندگانی که در مرحله ثبت نام متوقف شده اند:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_479c1246d97709e234574e1d2921994d'] = 'بازدید کنندگانی که درست بعد از ثبت نام، سفارشی را انجام داده اند:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_a751e9cc4ed4c7585ecc0d97781cb48a'] = 'جمع حساب های مشتری:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_998e4c5c80f27dec552e99dfed34889a'] = 'صادر کردن CSV';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_6602bbeb2956c035fb4cb5e844a4861b'] = 'راهنما';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_fba0e64541196123bbf8e3737bf9287b'] = 'تعداد حساب های مشتری که ساخته شده است';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_76dcf557776c2b40d47b72ebcd9ac6b7'] = 'تعداد  کامل حساب های ایجاد شده اطلاعات مهمی را در خود ندارد. اما به هر حال،  آنالیز این تعداد هر از چند وقت، مفید است. این نشان خواهد داد که چیز ها  در مسیر درست هستند یا خیر.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_57a6f41a27c9baa5b402d30e97d4c1e8'] = 'سیر جریان ثبت نام چگونه عمل می کند؟';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_2d8c349a97131b417bf7da166afb17bd'] = 'اگر فروشگاه شما بدون تغییر چیزی اجرا شود، تعداد مشتری های ثبت نام شده ثابت می ماند و یا اندکی کاهش می یابد.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_ac379194b8c2ea6c2e26be3672fdf7f7'] = 'یک  افزایش یا کاهش عمده، نشان می دهد که احتمالاً تغییری در فروشگاه شما  ایجاد شده است. بنابراین شما باید آن تغییر را شناسایی کنید، اگر آن تغییر  باعث کاهش تعداد ثبت نام ها شده باشد باید آن را اصلاح کنید و اگر باعث  افزایش ثبت نام شده است این تغییر را ادامه دهید.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_ded9c8756dc14fd26e3150c4718cd9d0'] = 'اینجا خلاصه ای از آن چیزی که ممکن است در ایجاد حساب های مشتری تاثیر بگذارد، وجود دارد:';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_3493de9f70480000ac99a1d5fc021dcb'] = 'یک  مبارزه ی تبلیغاتی می تواند تعداد بازدید کننده های بیشتری را جذب کند.  افزایشی در حساب های مشتری ایجاد خواهد شد که به \"کیفیت\" آن ها بستگی خواهد  داشت. تبلیغات خوب هدف گیری شده ممکن است بیشتز از تبلیغات وسیع تاثیر  گذار باشد.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_c53d1447202b4d629cf3b96dc4b1056d'] = 'فروش  فوق العاده یا مسابقات باعث توجه و کنجکاوی بیشتر می شود، نه فقط باعث  سرزنده نگه داشتن فروشگاه شما بلکه همچنین باعث افزایش ترافیک آن نیز می  شود. بدین ترتیب، شما می توانید انگیزه ی خریداران را تحریک کنید که در فکر  فرو روند.';
$_MODULE['<{statsregistrations}prestashop>statsregistrations_8cb5605d77d1d2f9eab6191c0e027747'] = 'نوع  طراحی وب سایت و کاربرپسند بودن بیش از همیشه مهم هستند: یک قالب گرافیکی  بد انتخاب شده و یا پیچیده می تواند باعث کاهش بازدید کننده ها شود. شما می  توانید توازن صحیح بین یک طراحی خلاق و اجازه ی حرکت آسان در اصراف وب  سایت به بازدید کننده ها ایجاد کنید.املای صحیح و وضوح مناسب نیز اعتماد به  نفس بیشتر مشتری در فروشگاه شما را القاء می کند.';
