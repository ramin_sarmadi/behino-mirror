<?php

/*
 * 2011 LandOfCoder
 *
 *  @author LandOfCoder 
 *  @copyright  2011 LandOfCoder
 *  @version  Release: $Revision: 1.0 $
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
    exit;
require_once(_PS_MODULE_DIR_ . "lofblogstags/defined.php");
if(!defined('LOFCONTENT_IMAGES_THUMBS_URI')) {
    require_once _PS_MODULE_DIR_.'lofblogs/defined.php';
}
if (!class_exists('LOFXParams')) {
    require LOFBLOGSTAGS_ROOT . 'config/params.php';
}

if (!class_exists('lofContentHelper')) {
    require _PS_MODULE_DIR_ . 'lofblogs/libs/lof_content_helper.php';
}


class lofblogstags extends Module {
    /* @var boolean error */

    protected $error = false;
    private $_postErrors = array();

    public function __construct() {
        $this->name = 'lofblogstags';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'LandOfCoder';
        $this->need_instance = 0;

        parent::__construct();
        $this->params = new LOFXParams($this);
        $this->helper = new lofContentHelper();
        $this->template = $this->params->get('template', 'default');
        $this->displayName = $this->l('Lof Blogs Tags');
        $this->description = $this->l('Display Block your Lof Blogs tags articles');
        $this->confirmUninstall = $this->l('do you realy want to uninstall Lof Blogs archive articles?');
    }

    public function install() {
        if ( parent::install() == false OR !$this->registerHook('displayRightColumn') )
            return false;
        return true;
    }

    public function uninstall() {
        if (!parent::uninstall())
            return false;
        return true;
    }

    function hookdisplayRightColumn($params) {
        return $this->processHook($params, 'right');
    }

    function hookdisplayLeftColumn($params) {
        return $this->processHook($params, 'left');
    }

    /**
     * Render processing form && process saving data.
     */
    public function getContent() {
        $html = "";
        if (Tools::isSubmit('submit')) {
            if (is_array($this->_postErrors) && !count($this->_postErrors)) {
                $this->params->update();
                $html .= '<div class="conf confirm">' . $this->l('Settings updated') . '</div>';
            }
        }
        if ($this->params->hasError())
            die($this->params->getErrorMsg());
        
        return $html . $this->params->displayForm();
    }


    function processHook($params, $hook = 'tabproductcontent') {
        $this->hook = 'left';

        $this->helper->moduleMedia($this->name, $this->template);
        $filter = array();
        //switch layout :
        $id_lang = (int)$this->context->language->id;
        $nb = $this->params->get('limit');
        $tags = $this->getMainTags($id_lang, $nb);
        
        $max = -1;
        $min = -1;
        foreach ($tags as $tag)
        {
            if ($tag['times'] > $max)
                $max = $tag['times'];
            if ($tag['times'] < $min || $min == -1)
                $min = $tag['times'];
        }
        
        if ($min == $max)
            $coef = $max;
        else
        {
            $coef = (BLOCKTAGS_MAX_LEVEL - 1) / ($max - $min);
        }
        
        if (!sizeof($tags))
            return false;
        foreach ($tags AS &$tag)
            $tag['class'] = 'tag_level'.(int)(($tag['times'] - $min) * $coef + 1);
        

        $lofContentHelper = new lofContentHelper();
        $this->smarty->assign(array(
            'article_related_items' => $tags,
            'lofContentHelper' => $lofContentHelper
        ));
        //select a layout by position :
        $layout = $this->getLayoutPath($this->hook);
        return $this->display(__FILE__, $layout);
    }

    public function getMainTags($id_lang, $nb = 10)
    {
        $groups = lofContentHelper::getCustomerGroups();

        $sql = '
        SELECT t.name, COUNT(pt.id_lofblogs_tag) AS times
        FROM `'._DB_PREFIX_.'lofblogs_tag_article` pt
        LEFT JOIN `'._DB_PREFIX_.'lofblogs_tag` t ON (t.id_lofblogs_tag = pt.id_lofblogs_tag)
        INNER JOIN `'._DB_PREFIX_.'lofblogs_publication` p ON (p.id_lofblogs_publication = pt.id_lofblogs_publication)

        WHERE t.`id_lang` = '.(int)$id_lang.'
        AND p.`status` = \'published\'';
        $access = array();
        foreach($groups as $id_group){
            $access[] = $id_group . ' IN (p.access)';
        }
        if(count($access))
            $sql .= ' AND ('. implode(' OR ', $access) .') ';
        $sql .= '
        GROUP BY t.id_lofblogs_tag
        ORDER BY times DESC
        LIMIT 0, '.(int)$nb;

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }


    public function getLayoutPath($layout = 'default') {
        $theme = $this->params->get('template', 'default');
        $layoutPath = LOFBLOGSTAGS_THEME . $theme . '/' . $layout . '.tpl';

        if (!file_exists($layoutPath)) {
            return 'themes/' . $theme . '/default.tpl';
        } else {
            return 'themes/' . $theme . '/' . $layout . '.tpl';
        }
    }

    function getParam($name, $default = '') {
        $paramName = $this->hook . ucfirst($name);
        return $this->params->get($paramName, $default);
    }

}
