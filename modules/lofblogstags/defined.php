<?php
/**
 * @name defined.php
 * @author landOfCoder
 * @todo defined some path and link
 */

// *************** SOME PATHS ******************************
define('LOFBLOGSTAGS_ROOT', _PS_MODULE_DIR_ . 'lofblogstags/');
define('LOFBLOGSTAGS_THEME', LOFBLOGSTAGS_ROOT.'themes/');

// ***************** SOME LINKS ***************************************

define('LOFBLOGSTAGS_BASE_URI', __PS_BASE_URI__ . 'modules/lofblogstags/');
define('LOFBLOGSTAGS_THEME_URI', LOFBLOGSTAGS_BASE_URI.'themes/');

?>
