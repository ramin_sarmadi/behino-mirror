<!-- Block tags module -->
<div id="blogstags_block_left" class="block tags_block">
	<h4 class="title_block">{l s='Blogs Tags' mod='lofblogstags'}</h4>
	<p class="block_content">
{if $article_related_items}
	{foreach from=$article_related_items item=tag name=myLoop}
		<a href="{$lofContentHelper->getTagLink( $tag.name )}" title="{l s='More about' mod='lofblogstags'} {$tag.name|escape:html:'UTF-8'}" class="{$tag.class} {if $smarty.foreach.myLoop.last}last_item{elseif $smarty.foreach.myLoop.first}first_item{else}item{/if}">{$tag.name|escape:html:'UTF-8'}</a>
	{/foreach}
{else}
	{l s='No tags have been specified yet.' mod='lofblogstags'}
{/if}
	</p>
</div>
<!-- /Block tags module -->
