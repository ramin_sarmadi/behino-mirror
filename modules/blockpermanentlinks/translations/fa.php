<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks-footer_5813ce0ec7196c492c97596718f71969'] = 'نقشه سایت';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks-footer_bbaff12800505b22a853e8b7f4eb6a22'] = 'ارتباط با ما';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks-footer_714814d37531916c293a8a4007e8418c'] = 'نشانه گذاری این صفحه';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks-header_2f8a6bf31f3bd67bd2d9720c58b19c9a'] = 'نشانه گذاری';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks-header_e1da49db34b0bdfdddaba2ad6552f848'] = 'نقشه سایت';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks-header_fad9383ed4698856ed467fd49ecf4820'] = 'نشانه گذاری';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks_39355c36cfd8f1d048a1f84803963534'] = 'بخش لینکهای دایمی';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks_52ecdc5932442c5f2a22ade561140870'] = 'افزودن بخشی جهت نمایش لینکهای دایمی مانند نقشه سایت، ارتباط با ما و...';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks_5813ce0ec7196c492c97596718f71969'] = 'نقشه سایت';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks_bbaff12800505b22a853e8b7f4eb6a22'] = 'ارتباط با ما';
$_MODULE['<{blockpermanentlinks}prestashop>blockpermanentlinks_2d29fbe96aaeb9737b355192f4683690'] = 'نشانه گذاری این صفحه';
