<div id="idTabArticlesRelated">
    <ul class="lofblogsarticles_related">
        {foreach from=$article_related_items item=item}
            <li class="lofblogsproduct_item">
                <h4><a class="itemTitle" href="{$item.link}" title="{$item.title}" >{$item.title}</a></h4>
                {if $item.image}
                    <a href="{$item.link}" title="{$item.title}" >
                        <img class="item_thumb"  src="{$thumbUri}{$item.image}" alt="{$item.title}" />    
                    </a>                        
                {/if}
                <div class="item_descrition">
                    {$item.introtext}
                </div>
                <div class="clearfix"></div>
            </li>      
        {/foreach}    
    </ul>  
</div>