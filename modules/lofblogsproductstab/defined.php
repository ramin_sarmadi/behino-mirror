<?php
/**
 * @name defined.php
 * @author landOfCoder
 * @todo defined some path and link
 */

// *************** SOME PATHS ******************************
define('LOFBLOGSPRODUCTSTAB_ROOT', _PS_MODULE_DIR_ . 'lofblogsproductstab/');
define('LOFBLOGSPRODUCTSTAB_THEME', LOFBLOGSPRODUCTSTAB_ROOT.'themes/');

// ***************** SOME LINKS ***************************************

define('LOFBLOGSPRODUCTSTAB_BASE_URI', __PS_BASE_URI__ . 'modules/lofblogsproductstab/');
define('LOFBLOGSPRODUCTSTAB_THEME_URI', LOFBLOGSPRODUCTSTAB_BASE_URI.'themes/');

?>
