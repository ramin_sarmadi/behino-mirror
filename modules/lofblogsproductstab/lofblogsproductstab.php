<?php

/*
 * 2011 LandOfCoder
 *
 *  @author LandOfCoder 
 *  @copyright  2011 LandOfCoder
 *  @version  Release: $Revision: 1.0 $
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
    exit;
require_once(_PS_MODULE_DIR_ . "lofblogsproductstab/defined.php");
if(!defined('LOFCONTENT_IMAGES_THUMBS_URI')) {
    require_once _PS_MODULE_DIR_.'lofblogs/defined.php';
}
if (!class_exists('LOFXParams')) {
    require LOFBLOGSPRODUCTSTAB_ROOT . 'config/params.php';
}

if (!class_exists('lofContentHelper')) {
    require _PS_MODULE_DIR_ . 'lofblogs/libs/lof_content_helper.php';
}


class lofblogsproductstab extends Module {
    /* @var boolean error */

    protected $error = false;
    private $_postErrors = array();

    public function __construct() {
        $this->name = 'lofblogsproductstab';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'LandOfCoder';
        $this->need_instance = 0;

        parent::__construct();
        $this->params = new LOFXParams($this);
        $this->helper = new lofContentHelper();
        $this->template = $this->params->get('template', 'default');
        $this->displayName = $this->l('Lof Blogs Product Articles Related');
        $this->description = $this->l('Display your Lof Blogs articles relate in a tab of product');
        $this->confirmUninstall = $this->l('do you realy want to uninstall Lof Blogs Articles Product?');
    }

    public function install() {
        if (parent::install() == false
                OR !$this->registerHook('displayProductTab')
                OR !$this->registerHook('displayProductTabContent')
        )
            return false;
        return true;
    }

    public function uninstall() {
        if (!parent::uninstall())
            return false;
        return true;
    }

    function hookdisplayProductTab($params) {
        $params = $this->params->getValues();
        $layout = $this->getLayoutPath('tabproduct');
        return $this->display(__FILE__, $layout);
    }

    function hookdisplayProductTabContent($params) {
        return $this->processHook($params, 'tabproductcontent');
    }

    /**
     * Render processing form && process saving data.
     */
    public function getContent() {
        $html = "";
        if (Tools::isSubmit('submit')) {
            if (is_array($this->_postErrors) && !count($this->_postErrors)) {

                $this->params->update();
                $html .= '<div class="conf confirm">' . $this->l('Settings updated') . '</div>';
            }
        }
        if ($this->params->hasError())
            die($this->params->getErrorMsg());

        return $html . $this->params->displayForm();
    }


    function processHook($params, $hook = 'tabproductcontent') {

        $this->hook = 'tabproductcontent';

        $this->helper->moduleMedia($this->name, $this->template);
        $filter = array();
        //switch layout :
        $type = $this->getParam('type', 'lastest');
        switch ($type) {
            case 'lastest' :
                $order = 'id_lofblogs_publication DESC';
                $items = $this->getItems($order);
                break;
            case 'featured' :
                $filter = array('featured' => 1);
                $order = 'id_lofblogs_publication DESC';
                $items = $this->getItems($order, $filter);
                break;
            case 'popular':
            default :
                $order = 'hits DESC';
                $items = $this->getItems($order);
                break;
        }

        if($items)
            foreach ($items as &$item) {            
                $text = trim(strip_tags($item['short_desc'])) ? trim(strip_tags($item['short_desc'])) : trim(strip_tags($item['content']));
                $item['introtext'] = lofContentHelper::limitString($text, $this->getParam('intro', 900));
            }
        

        $this->smarty->assign(array(
            'article_related_items' => $items,
            'thumbUri' => LOFCONTENT_IMAGES_THUMBS_URI,
            'imageUri' => LOFCONTENT_IMAGES_URI
        ));
        //select a layout by position :
        $layout = $this->getLayoutPath($this->hook);
        return $this->display(__FILE__, $layout);
    }

    function getItems($order, $filter = array()) {
        global $cookie;
        $lang = $cookie->id_lang ? $cookie->id_lang : $this->params->defaultLang;
        $limit = $this->getParam('count', 0);
        $groups = lofContentHelper::getCustomerGroups();
        
        $query = 'SELECT a.*, al.title, al.link_rewrite, al.short_desc, al.content, cl.name as categorytitle, cl.link_rewrite as categoryalias ';
        $query .= ' FROM ' . _DB_PREFIX_ . 'lofblogs_publication a ';
        $query .= ' LEFT JOIN ' . _DB_PREFIX_ . 'lofblogs_publication_lang al ON (a.id_lofblogs_publication = al.id_lofblogs_publication)';
        $query .= ' LEFT JOIN ' . _DB_PREFIX_ . 'lofblogs_category c ON (a.id_lofblogs_category = c.id_lofblogs_category)';
        $query .= ' INNER JOIN ' . _DB_PREFIX_ . 'lofblogs_category_lang cl ON (a.id_lofblogs_category = cl.id_lofblogs_category)';
        $query .= ' INNER JOIN ' . _DB_PREFIX_ . 'lofblogs_publication_product pp ON (a.id_lofblogs_publication = pp.id_lofblogs_publication)';
        $query .= ' WHERE al.id_lang = ' . pSQL($lang) 
                . ' AND pp.id_product = '.(int)(Tools::getValue('id_product'))
                . ' AND cl.id_lang = ' . pSQL($lang)
                . ' AND a.status = '.$this->helper->sqlQuote('published')
                . ' AND c.active = 1';
        
        $access = array();
        foreach($groups as $id_group){
            $access[] = $id_group . ' IN (a.access)';
        }
        if(count($access))
            $query .= ' AND ('. implode(' OR ', $access) .') ';

        $catids = $this->getParam('cats', '');
        if ($catids) {
            //filter bt category id :
            $query .= ' AND a.id_lofblogs_category IN (' . $catids . ')';
        }

        if (isset($filter['featured']) && $filter['featured']) {
            $query .= ' AND a.featured = 1';
        }

        //set order :
        $query .= ' ORDER BY ' . $order;

        //set limit :
        if ($limit) {
            $query .= ' LIMIT 0,' . $limit;
        }

        $items = Db::getInstance()->ExecuteS($query);
        foreach ($items as $k => $item) {
            $items[$k]['link'] = lofContentHelper::getArticleLink($item['id_lofblogs_publication'], $item['link_rewrite']);
        }
        return $items;
    }

    public function getLayoutPath($layout = 'default') {
        $theme = $this->params->get('template', 'default');
        $layoutPath = LOFBLOGSPRODUCTSTAB_THEME . $theme . '/' . $layout . '.tpl';

        if (!file_exists($layoutPath)) {
            return 'themes/' . $theme . '/default.tpl';
        } else {
            return 'themes/' . $theme . '/' . $layout . '.tpl';
        }
    }

    function getParam($name, $default = '') {
        $paramName = $this->hook . ucfirst($name);
        return $this->params->get($paramName, $default);
    }

}
