<?php
include('../../config/config.inc.php');
include('../../init.php');
include('suggestproduct.php');

$object = new SuggestProduct();
$res = $object->getResultContent(Context::getContext()->language->id, 0, 4);
Context::getContext()->smarty->assign(array(
    'SuggestProductArr' => $object->getResultContent(Context::getContext()->language->id, 0, 4)
));
if (Tools::getValue('gettpl')) {
    //echo __DIR__ . '\resultpage.tpl';
    $resulthtml = Context::getContext()->smarty->fetch(__DIR__ . '\resultpage.tpl');
    //$resulthtml;
    die(Tools::jsonEncode(array(
        'resulthtml' => $resulthtml
    )));
}
