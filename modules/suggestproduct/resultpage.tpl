<div class="span8">
    <div class="row-fluid banner-row">
        <a href="{$base_dir}content/11-order-guide" />
            <img class="first-banners" src="{$img_dir}banners/banner1.jpg"/>
        </a>
        <a href="{$base_dir}content/16-saman-insurance" />
            <img class="first-banners" src="{$img_dir}banners/banner2.jpg"/>
        </a>
        <a href="{$base_dir}index.php?id_product=468&controller=product" />
            <img class="first-banners" src="{$img_dir}banners/s5zan.jpg"/>
        </a>
    </div>
    <div class="row-fluid suggest-row">
        <div class="span12">
            <div class="row-fluid sug-title">
                <span class="sug-title-text">{l s='پیشنهاد ما '}</span>

                <div class="sug-car-contr">
                    <a id="sug-left"></a>
                    <a id="sug-right"></a>
                </div>
            </div>
            <div class="row-fluid">
                <div class="carousel-sugests">
                    {foreach from=$SuggestProductArr item=SuggestProduct}
                    {*{if $SuggestProduct@iteration%2 == 1}*}
                    {*<div class="double">*}
                    {*{/if}*}
                        <div data-category="{$SuggestProduct['parent_category_default']}"
                             class="suggests {if $SuggestProduct@iteration%2 == 0}sug-last{/if}">
                            <div class=" sug-img">
                                <img src="{$SuggestProduct['address_file']}">
                            </div>
                            <div class=" sug-txts">
                                <span class="sug-model">{$SuggestProduct['name']}</span>
                                </br></br>
                                <span class="sug-price">{convertPrice price=$SuggestProduct['price']}</span>
                                </br></br>
                                <a class="view-more" href="{$SuggestProduct['link']}" target="_blank">{l s='نمایش بیشتر'}</a>
                            </div>
                        </div>
                    {*{if $SuggestProduct@iteration%2 == 0}*}
                    {*</div>*}
                    {*{/if}*}
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var left = $('#sug-left');
        var right = $('#sug-right');
        $('.carousel-sugests').carouFredSel({
            items: 2,
            direction: "right",
            scroll: {
                items: 2,
                duration: 1000,
                pauseOnHover: true
            },
            auto: false,
            responsive: true,
            width: 395,
            height: 'auto',
            prev: left,
            next: right
        });
    });
</script>