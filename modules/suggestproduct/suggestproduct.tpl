<fieldset><legend><img src="{$IconSuggestProductImage}" /> {$SuggestProductImage} </legend>

    <form action="{$LinkSave}" method="post" enctype="multipart/form-data">
        <input name="id_product_special" value="{$ResultSuggestProductArr[0]['id_product_special']}" type="hidden">
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Suggest Product Name' mod='suggestproduct'}:</span>
            <input name="special_product_name" value="{$ResultSuggestProductArr[0]['name']}" maxlength="2000" style="width: 280px" >
        </div>

        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Product Name' mod='suggestproduct'}:</span>
            <select name="id_product" dir="ltr">
                <option value="-1">...</option>
                {foreach from=$ProductArr item=Product}
                    <option style="{$Product['BgColor']}" value="{$Product['id_product']}"
                            {if $Product['id_product']==$ResultSuggestProductArr[0]['id_product']} selected="selected" {/if}
                            >{$Product['id_product']}. {$Product['name']} //-->Presentation Date: {$Product['date_start']} </option>
                {/foreach}
            </select>
            {if $ResultSuggestProductArr[0]['id_product']>0}<span style="font-size: 10px; font-style: italic">{l s='If the option is not selected, it is possible to disable the product' mod='suggestproduct'}</span>  {/if}
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Suggest Product Type' mod='suggestproduct'}:</span>
            <select name="id_product_special_type" dir="ltr">
                <option value="-1">...</option>
                {foreach from=$SuggestType item=SuggestTypes}
                    <option value="{$SuggestTypes['id_product_special_type']}"
                            {if $SuggestTypes['id_product_special_type']==$ResultSuggestProductArr[0]['id_product_special_type']} selected="selected" {/if} >
                        {$SuggestTypes['id_product_special_type']}. {$SuggestTypes['name']}
                    </option>
                {/foreach}
            </select>
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Duration (Day) ' mod='suggestproduct'}:</span>
            <input name="duration_days" value="{$ResultSuggestProductArr[0]['duration_days']}" maxlength="3" style="width: 50px" >
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Description' mod='suggestproduct'}:</span>
            <textarea cols="50" rows="3" name="description">{$ResultSuggestProductArr[0]['description']}</textarea>
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Image File' mod='suggestproduct'}:</span>
            <input type="file" name="ImageFile">
        </div>

        <div style="padding: 5px">date_start
            <span class="span_bold_12">{l s='Available from' mod='suggestproduct'}:</span>
            <input class="datepicker" type="text" name="date_start" value="{$ResultSuggestProductArr[0]['date_start_h']}" style="text-align: center" id="sp_from" />
        </div>

        <div style="padding-left: 150px; padding-top: 10px">
            <input type="submit" value="{l s='Save' mod='suggestproduct'}" class="button" name="AddProductLink">
        </div>


    </form>
</fieldset>
<script type="text/javascript">
    $(document).ready(function(){

        $('#leave_bprice').click(function() {
            if (this.checked)
                $('#sp_price').attr('disabled', 'disabled');
            else
                $('#sp_price').removeAttr('disabled');
        });

        $('.datepicker').datepicker({
            prevText: '',
            nextText: '',
            dateFormat: 'yy-mm-dd',

            // Define a custom regional settings in order to use PrestaShop translation tools
            currentText: '{l s='Now'}',
            closeText: '{l s='Done'}'
        });
    });
</script>