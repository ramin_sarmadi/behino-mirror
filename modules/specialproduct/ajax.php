<?php
include('../../config/config.inc.php');
include('../../init.php');
include('specialproduct.php');
//Context::getContext()->controller->addCSS(_THEME_CSS_DIR_ . 'jquery.flipcountdown.css', 'all');
//Context::getContext()->controller->addJS(_THEME_JS_DIR_ . 'jquery.flipcountdown.js', 'all');
//Context::getContext()->controller->addCSS(_THEME_CSS_DIR_ . 'isotope.css', 'all');
//Context::getContext()->controller->addJS(_THEME_JS_DIR_ . 'isotope.pkgd.min.js', 'all');
$object = new SpecialProduct();
$res = $object->getResultContent(Context::getContext()->language->id, 0, 1);
Context::getContext()->smarty->assign(array(
    'SpecialProductArr' => $object->getResultContent(Context::getContext()->language->id, 0, 1)
));
if (Tools::getValue('gettpl')) {
    //echo __DIR__ . '\resultpage.tpl';
    $resulthtml = Context::getContext()->smarty->fetch(__DIR__ . '\resultpage.tpl');
    //$resulthtml;
    die(Tools::jsonEncode(array(
        'resulthtml' => $resulthtml
    )));
}
