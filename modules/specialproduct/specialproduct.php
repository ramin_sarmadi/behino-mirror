<?php

class SpecialProduct extends Module
{

    public function __construct()
    {
        $this->name = 'specialproduct';
        $this->tab = 'advertising_marketing';
        $this->version = 1.0;
        $this->author = 'Ahmad Khorshidi';
        $this->need_instance = 0;

        parent::__construct();

        SpecialProduct::initProcess();

        $this->displayName = $this->l('Manage special products');
        $this->description = $this->l('Add image and special product');
    }

    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('header')
            || !$this->registerHook('home')
            //|| $this->_createTables() == false
            )
            return false;

        return true;

    }

    public function uninstall()
    {
        $db = Db::getInstance();
        $query = 'DROP TABLE `'._DB_PREFIX_.'product_special`';
        $query1 = 'DROP TABLE `'._DB_PREFIX_.'product_special_type`';

        //$result = $db->Execute($query);
        //$result1 = $db->Execute($query1);

        if ((!parent::uninstall()) /*OR (!$result) OR (!$result1)*/ )
            return false;
        return true;
    }

    public function _createTables()
    {
        $db = Db::getInstance();

        $query = '
       CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_special` (
          `id_product_special` bigint(12) NOT NULL AUTO_INCREMENT,
          `id_product_special_type` int(6) unsigned NOT NULL,
          `id_product` bigint(10) unsigned NOT NULL,
          `name` varchar(255) NOT NULL,
          `file` mediumtext,
          `description` text,
          `duration_days` int(3),
          `date_add` datetime NOT NULL,
          `date_start` date NOT NULL,
       PRIMARY KEY (`id_product_special`,`id_product_special_type`,`id_product`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			';

        $query1 = '
       CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_special_type` (
          `id_product_special_type` int(6) NOT NULL AUTO_INCREMENT,
          `id_lang` int(6) NOT NULL,
          `name` varchar(255) NOT NULL,
          `upload_address` varchar(255) NULL,
       PRIMARY KEY (`id_product_special_type`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			';


        $result = $db->Execute($query);
        $result1 = $db->Execute($query1);

        if ((!$result)&&(!$result1))
            return false;
        return true;
    }

    public function initProcess()
    {
        $nameAction='';
        $action='';
        $ResultSpecialProductArr='';
        if (Tools::isSubmit('update'.$this->name))
        {
            $action = 'updateData';
            $id_product_special=Tools::getValue('id_product_special');
            $ResultSpecialProductArr = $this->getListContent($this->context->language->id,$id_product_special,0);
            $nameAction='Update';

        }elseif(Tools::isSubmit('add'.$this->name))
        {
            $ResultSpecialProductArr=array();
            $nameAction='Add';
            $action = 'insert';
        }


        //$this->context->controller->addJqueryUI('ui.datepicker');
        $this->context->smarty->assign(array(
        'SpecialProductImage' => $nameAction.' Special Product'
        ,'IconSpecialProductImage' => '../modules/'.$this->name.'/logo-special.png'
        ,'LinkSave' =>  AdminController::$currentIndex.'&configure='.$this->name.'&'.$action.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')
        ,'SpecialType' => $this->getListSpecialType($this->context->language->id,1)
        ,'ProductArr' => $this->getListProduct($this->context->language->id)
        ,'ResultSpecialProductArr' => $ResultSpecialProductArr
        ));



    }


    public function getContent()
    {

        $html = '';
        //$content = $this->displayForm();

        //return $content;
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');


        if (Tools::isSubmit('insert'.$this->name))
        {
            $id_product = (int)(Tools::getValue('id_product'));
            $id_product_special_type = (int)(Tools::getValue('id_product_special_type'));
            $special_product_name = strval(Tools::getValue('special_product_name'));
            $description = strval(Tools::getValue('description'));

            //--------------------

            $date_start=Pdate::convert_date_H_to_M(Tools::getValue('date_start'));
            //$date_start=Pdate::convert_date_M_to_H('2014-01-08');

            //--------------------

            $date = date("Y-m-d H:i:s");
            $duration_days = (int)(Tools::getValue('duration_days'));
            $file = $_FILES['ImageFile']['name'];
            $image_format = $_FILES['ImageFile']['type'];

            if(($image_format!='image/jpeg') && ($image_format!='image/pjpeg') && ($image_format!='image/png') &&($image_format!='image/x-png')){

                $html .= $this->displayError($this->l('Invalid image format! use (jpeg OR png) for show.'));
            }

            if ( (!$special_product_name) || ($id_product<0) || ($duration_days<0)
                || ($id_product_special_type<0) ||($date_start=='0000-00-00')  )
            {
                $html .= $this->displayError($this->l('Invalid Data value! Please try again.'));


            }else {

                $result_Type=$this->getListSpecialType($this->context->language->id,$id_product_special_type);
                if($result_Type[0]['upload_address']==''){

                    $html .= $this->displayError($this->l('Invalid Address value for upload file! Please contact to administrator.'));

                }else{


                    $uploadImage_target=_PS_UPLOAD_DIR_.''.$result_Type[0]['upload_address'];

                    if ($_FILES["ImageFile"]["error"] > 0)
                    {
                        $html .= "Error: " . $_FILES["file"]["error"] . "<br />";
                    }
                    else
                    {


                        $TF_Image=(explode(".",$_FILES["ImageFile"]["name"]));
                        $TFCount_Image=count($TF_Image);
                        $FILENAME_Image=((date('Ymd')*rand(12345,99999)).(date('Ymd')*rand(1234,9999))).'.'.$TF_Image[$TFCount_Image-1];
                        $TARGET_UPLOAD_Image=$uploadImage_target;
                        $target_N_Image = $TARGET_UPLOAD_Image.$FILENAME_Image;

                        if(( move_uploaded_file(($_FILES["ImageFile"]['tmp_name']), $target_N_Image) ))
                        {

                            $html .= $this->displayConfirmation($this->l('Add Thumbnail File '.$special_product_name));


                        }else{
                            $FILENAME_Image='';

                            //return false; //فایل ارسال نشد
                        }


                    }

                    $dataArr = array(
                        'id_product'=>$id_product
                    ,'id_product_special_type'=>$id_product_special_type
                    ,'name'=>$special_product_name
                    ,'description'=>$description
                    ,'file'=>$FILENAME_Image
                    ,'duration_days'=>$duration_days
                    ,'date_add'=>$date
                    ,'date_start'=>$date_start
                    );

                    $result_add=$this->add('product_special',$this->context->language->id,$dataArr,'id_product_special','name="'.$special_product_name.'" AND id_product_special_type='.$id_product_special_type,'id_product_special');



                }

                if($result_add=='OK'){
                    $html .= $this->displayConfirmation($this->l('Add Special Product record for '.$special_product_name.' successfully'));
                }else{
                    $html .= $this->displayConfirmation($this->l('<font color="#000000">Add Special Product record for '.$special_product_name.' unsuccessfully</font>'));
                }

            }





        }
        elseif(Tools::isSubmit('updateData'.$this->name)){


            $id_product = (int)(Tools::getValue('id_product'));
            $id_product_special = (int)(Tools::getValue('id_product_special'));
            $id_product_special_type = (int)(Tools::getValue('id_product_special_type'));
            $special_product_name = strval(Tools::getValue('special_product_name'));
            $description = strval(Tools::getValue('description'));

            //--------------------

            $date_start=Pdate::convert_date_H_to_M(Tools::getValue('date_start'));
            //$date_start=Pdate::convert_date_M_to_H('2014-01-08');

            //--------------------

            $date = date("Y-m-d H:i:s");
            $duration_days = (int)(Tools::getValue('duration_days'));
            $file = $_FILES['ImageFile']['name'];
            $image_format = $_FILES['ImageFile']['type'];



            if((!$file))
            {
                $html .= $this->displayError($this->l('No image for upload!'));

            }else{

                if(($image_format!='image/jpeg') && ($image_format!='image/pjpeg') && ($image_format!='image/png') &&($image_format!='image/x-png')){

                    $html .= $this->displayError($this->l('Invalid image format! use (jpeg OR png) for show.'));
                }
            }

            if ( (!$special_product_name) || ($id_product<0) || ($duration_days<0) || ($id_product_special_type<0)
                ||($date_start=='0000-00-00') || (!$id_product_special)  )
            {
                $html .= $this->displayError($this->l('Invalid Data value! Please try again.'));

            }else{
                $result_del = $this->getListContent($this->context->language->id,$id_product_special);
                $result_address=$result_del[0]['upload_address'];
                $result_file=$result_del[0]['file'];
                $file_del=_PS_UPLOAD_DIR_.$result_address.$result_file;

                $result_Type=$this->getListSpecialType($this->context->language->id,$id_product_special_type);
                if($result_Type[0]['upload_address']==''){

                    $html .= $this->displayError($this->l('Invalid Address value for upload file! Please contact to administrator.'));

                }else{


                    $uploadImage_target=_PS_UPLOAD_DIR_.''.$result_Type[0]['upload_address'];

                    if ($_FILES["ImageFile"]["error"] > 0)
                    {
                        $html .= "Error: " . $_FILES["file"]["error"] . "<br />";
                    }
                    else
                    {



                        if (!unlink($file_del))
                        {
                            $html .= $this->displayError($this->l('Can not delete image file from site!!!'));
                        }
                        else
                        {
                            $html .= $this->displayError($this->l('Deleted image file.'));

                        }



                        $TF_Image=(explode(".",$_FILES["ImageFile"]["name"]));
                        $TFCount_Image=count($TF_Image);
                        $FILENAME_Image=((date('Ymd')*rand(12345,99999)).(date('Ymd')*rand(1234,9999))).'.'.$TF_Image[$TFCount_Image-1];
                        $TARGET_UPLOAD_Image=$uploadImage_target;
                        $target_N_Image = $TARGET_UPLOAD_Image.$FILENAME_Image;

                        if(( move_uploaded_file(($_FILES["ImageFile"]['tmp_name']), $target_N_Image) ))
                        {
                            $html .= $this->displayConfirmation($this->l('Add Thumbnail File '.$special_product_name));

                        }else{
                            //return false; //فایل ارسال نشد
                        }


                    }

                }//upload_address!=''

                if(!$file){
                    $FILENAME_Image=$result_file;
                }else{
                    $FILENAME_Image=$FILENAME_Image;
                }
                $dataArr = array(
                 'id_product'=>$id_product
                ,'id_product_special_type'=>$id_product_special_type
                ,'name'=>$special_product_name
                ,'description'=>$description
                ,'file'=>$FILENAME_Image
                ,'duration_days'=>$duration_days
                ,'date_add'=>$date
                ,'date_start'=>$date_start
                );

                $result_add=$this->update('product_special',$this->context->language->id,$dataArr,'id_product_special='.$id_product_special);


                if($result_add=='OK'){
                    $html .= $this->displayConfirmation($this->l('Update Special Product record for '.$special_product_name.' successfully'));
                }else{
                    $html .= $this->displayConfirmation($this->l('<font color="#000000">Update Special Product record for '.$special_product_name.' <b>unsuccessfully</b></font>'));
                }

            }


            //Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&action=updateData&result='.$result.'&token='.Tools::getAdminTokenLite('AdminModules'));

        }

        if (Tools::isSubmit('update'.$this->name))
        {
            $this->context->controller->addJqueryUI('ui.datepicker');
            return $this->display(__FILE__, 'specialproduct.tpl');

        }elseif(Tools::isSubmit('add'.$this->name)){
            $this->context->controller->addJqueryUI('ui.datepicker');
            return $this->display(__FILE__, 'specialproduct.tpl');
        }
        elseif (Tools::isSubmit('delete'.$this->name))
        {
            $id_product_special = (int)(Tools::getValue('id_product_special'));
            $result_type = $this->getListContent($this->context->language->id,$id_product_special);
            $result_address=$result_type[0]['upload_address'];
            $result_file=$result_type[0]['file'];
            $result_name=$result_type[0]['name'];
            $file=_PS_UPLOAD_DIR_.$result_address.$result_file;
            //break;

            if (!unlink($file))
            {
                $result='101';
                $result=$this->deleted('product_special','id_product_special='.$id_product_special);

            }
            else
            {
                $result=$this->deleted('product_special','id_product_special='.$id_product_special);
                //$result='OK';
            }


            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&action=deleted&result='.$result.'&token='.Tools::getAdminTokenLite('AdminModules'));

        }
        elseif (Tools::getValue('action')=='deleted')
        {

            $result = Tools::getValue('result');

            if($result=='OK')
            {
                $html .= '<div class="conf confirm">'.$this->l('Deleted data from database and file from site.').'</div>';
            }elseif($result=='100'){

                $html .= '<div class="conf confirm">'.$this->l('Miss Data.').'</div>';

            }elseif($result=='101'){

                $html .= '<div class="conf confirm">'.$this->l('Miss File.').'</div>';

            }else{
                $html .= '<div class="conf confirm">'.$this->l('You have error in add data.').'</div>';
            }

            $helper = $this->initList();
            return $html.$helper->generateList($this->getListContent($this->context->language->id,0,1), $this->fields_list);

        }
        else
        {
            $helper = $this->initList();
            return $html.$helper->generateList($this->getListContent($this->context->language->id,0,1), $this->fields_list);
        }

        if (isset($_POST['submitModule']))
        {
            /* //Configuration::updateValue('wiki_nbs', ((isset($_POST['nbs']) && $_POST['nbs'] != '') ? (int)$_POST['nbs'] : ''));
             if ($this->removeFromDB() && $this->addToDB())
             {
                 //$this->_clearCache('wiki.tpl');
                 //$output = '<div class="conf confirm">'.$this->l('The  configuration has been updated.').'</div>';
             }
             else
             {
                 //$output = '<div class="conf error"><img src="../img/admin/disabled.gif"/>'.$this->l('An error occurred while attempting to
 save.').'</div>';
             }*/
        }



    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_product_special' => array(
                'title' => $this->l('Id'),
                'width' => 30,
                'type' => 'text',
            ),
            'type' => array(
                'title' => $this->l('Type'),
                'width' => 60,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'width' => 140,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'product_name' => array(
                'title' => $this->l('Product Name'),
                'width' => 140,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'duration_days' => array(
                'title' => $this->l('Duration Days'),
                'width' => 50,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'date_add_h' => array(
                'title' => $this->l('Date Add'),
                'width' => 80,
                'type' => 'text'
            ),
            'date_start_h' => array(
                'title' => $this->l('Date Start'),
                'width' => 80,
                'type' => 'text'
            ),
            'date_end_h' => array(
                'title' => $this->l('Date End'),
                'width' => 80,
                'type' => 'text'
            )
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'id_product_special';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new')
        );

        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    protected function getListContent($id_lang,$id_product_special = 0,$id_product_special_type = 0)
    {
        $sub_query='';
        if($id_product_special>0)
        {
            $sub_query = ' AND ps.id_product_special='.$id_product_special;
        }elseif($id_product_special_type>0){
            $sub_query = ' AND ps.id_product_special_type='.$id_product_special_type;
        }

        $SQL = '
			SELECT pl.id_product, ps.id_product_special
			, ps.name, ps.duration_days, ps.description
			, ps.date_add, ps.date_start, pl.name product_name, pl.description_short
			,(SELECT DATE_ADD(ps.date_start,INTERVAL ps.duration_days DAY)) AS date_end
            ,(SELECT IF(date_end>=(SELECT CURDATE()),"1","0") ) AS activity
			, pst.name type, pst.id_product_special_type,pst.upload_address
			, ps.file, p.price as price_product, p.id_category_default as category, pl.link_rewrite, p.ean13
			,(
			    SELECT MIN(pa.price)
                FROM `'._DB_PREFIX_.'product_attribute` pa
                INNER JOIN '._DB_PREFIX_.'stock_available sa ON sa.id_product_attribute=pa.id_product_attribute
                WHERE sa.quantity > 0
                AND pa.id_product=pl.id_product
             ) as price
            ,(SELECT name FROM `'._DB_PREFIX_.'category_lang` WHERE id_category=c.id_category) as category_default
			,(SELECT name FROM `'._DB_PREFIX_.'category_lang` WHERE id_category=c.id_parent) as parent_category_default
			FROM `'._DB_PREFIX_.'product_special` ps
			INNER JOIN `'._DB_PREFIX_.'product_special_type` pst ON ps.id_product_special_type = pst.id_product_special_type
			INNER JOIN `'._DB_PREFIX_.'product_lang` pl ON ps.id_product = pl.id_product
			INNER JOIN `'._DB_PREFIX_.'product` p ON pl.id_product = p.id_product
			INNER JOIN `'._DB_PREFIX_.'category` c ON c.id_category = p.id_category_default
			WHERE pst.id_lang='.(int)$id_lang.' '.$sub_query.' ORDER BY pst.id_product_special_type';

        $results =  Db::getInstance()->executeS($SQL);

        foreach ($results as &$result){

            if ((int)$result['id_product']!=0)
            {
                $result['link'] = $this->context->link->getProductLink((int)$result['id_product'], $result['link_rewrite'], $result['category'], $result['ean13']);
            }

            if ($result['date_start']!='0000-00-00')
            {

                $result['date_start_h'] = Pdate::convert_date_M_to_H($result['date_start']);

            }

            if ($result['date_end']!='0000-00-00')
            {

                $result['date_end_h'] = Pdate::convert_date_M_to_H($result['date_end']);

            }

            if ($result['date_add']!='0000-00-00 00:00:00')
            {

                $result['date_add_h'] = Pdate::convert_date_M_to_H($result['date_add']);

            }

            //$uploadImage_target=_PS_UPLOAD_DIR_.''.$result_Type[0]['upload_address'];

            if ($result['file']!='')
            {
                $result_Type=$this->getListSpecialType($this->context->language->id,$result['id_product_special_type']);
                $uploadImage_target='upload/'.$result_Type[0]['upload_address'];

                $result['address_file'] = $uploadImage_target.($result['file']);

            }

        }

        return $results;
    }

    public function getResultContent($id_lang,$id_product_special = 0,$id_product_special_type = 0)
    {
        $sub_query='';
        if($id_product_special>0)
        {
            $sub_query = ' AND ps.id_product_special='.$id_product_special;
        }elseif($id_product_special_type>0){
            $sub_query = ' AND ps.id_product_special_type='.$id_product_special_type;
        }

        $SQL = '
        SELECT * FROM
        (
			SELECT pl.id_product, ps.id_product_special, ps.description,
            (SELECT MAX(image_shop.`id_image`)  FROM '._DB_PREFIX_.'image i
            '.Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
            WHERE i.id_product = p.id_product
            ) id_image
			, ps.name, ps.duration_days, pl.description_short
			, ps.date_add, ps.date_start, pl.name product_name
			,(SELECT DATE_ADD(ps.date_start,INTERVAL ps.duration_days DAY)) AS date_end
            ,(SELECT IF(date_end>=(SELECT CURDATE()),"1","0") ) AS activity
			, pst.name type, pst.id_product_special_type,pst.upload_address
			, ps.file, p.price as price_product, p.id_category_default as category, pl.link_rewrite, p.ean13
			,(
			    SELECT MIN(pa.price)
                FROM `'._DB_PREFIX_.'product_attribute` pa
                INNER JOIN '._DB_PREFIX_.'stock_available sa ON sa.id_product_attribute=pa.id_product_attribute
                WHERE sa.quantity > 0
                AND pa.id_product=pl.id_product
             ) as price
			,(SELECT name FROM `'._DB_PREFIX_.'category_lang` WHERE id_category=c.id_category) as category_default
			,(SELECT name FROM `'._DB_PREFIX_.'category_lang` WHERE id_category=c.id_parent) as parent_category_default
			FROM `'._DB_PREFIX_.'product_special` ps
			INNER JOIN `'._DB_PREFIX_.'product_special_type` pst ON ps.id_product_special_type = pst.id_product_special_type
			INNER JOIN `'._DB_PREFIX_.'product_lang` pl ON ps.id_product = pl.id_product
			INNER JOIN `'._DB_PREFIX_.'product` p ON (pl.id_product = p.id_product AND p.active=1)
			INNER JOIN `'._DB_PREFIX_.'category` c ON c.id_category = p.id_category_default
			WHERE pst.id_lang='.(int)$id_lang.' '.$sub_query.' ORDER BY pst.id_product_special_type
			) as tmp WHERE activity=1
			';

        $results =  Db::getInstance()->executeS($SQL);

        foreach ($results as &$result){

            if ((int)$result['id_product']!=0)
            {
                $result['link'] = $this->context->link->getProductLink((int)$result['id_product'], $result['link_rewrite'], $result['category'], $result['ean13']);
            }

            if ($result['date_start']!='0000-00-00')
            {

                $result['date_start_h'] = Pdate::convert_date_M_to_H($result['date_start']);

            }

            if ($result['date_end']!='0000-00-00')
            {

                $result['date_end_h'] = Pdate::convert_date_M_to_H($result['date_end']);

            }

            if ($result['date_add']!='0000-00-00 00:00:00')
            {

                $result['date_add_h'] = Pdate::convert_date_M_to_H($result['date_add']);

            }

            //$uploadImage_target=_PS_UPLOAD_DIR_.''.$result_Type[0]['upload_address'];

            if ($result['file']!='')
            {
                $result_Type=$this->getListSpecialType($this->context->language->id,$result['id_product_special_type']);
                $uploadImage_target='upload/'.$result_Type[0]['upload_address'];

                $result['address_file'] = $uploadImage_target.($result['file']);

            }else{


                if(isset($result['id_image'])){
                    $result['address_file'] = $this->context->link->getImageLink($result['link_rewrite'], $result['id_image'], 'tabs_allinmart');
                }
                else
                {
                    $result['address_file'] = 'NoPic';
                }


            }

        }

        return $results;
    }

    protected function getListSpecialType($id_lang,$id_product_special_type =0)
    {
        $sub_query='';
        if($id_product_special_type>0)
        {
            $sub_query = 'AND pst.id_product_special_type='.$id_product_special_type;
        }

        $SQL = '
			SELECT *
			FROM `'._DB_PREFIX_.'product_special_type` pst
			WHERE pst.`id_lang` = '.(int)$id_lang.' '.$sub_query;

        return  Db::getInstance()->executeS($SQL);
    }

    protected function getListProduct($id_lang,$id_product=0)
    {
        $sub_query='';
        if($id_product>0)
        {
            $sub_query = 'AND pl.id_product='.$id_product;
        }

        $SQL = '
			SELECT pl.id_product, name, date_start
			FROM `'._DB_PREFIX_.'product_lang` pl
     		INNER JOIN `'._DB_PREFIX_.'product` p ON (pl.id_product = p.id_product AND p.active=1)
			WHERE pl.`id_lang` = '.(int)$id_lang.' '.$sub_query.' ORDER BY pl.date_start DESC';


        $results = Db::getInstance()->executeS($SQL);

        $count=0;
        foreach($results as &$result)
        {

            if (fmod($count,2) == 0){

                $result['BgColor'] = 'background-color: #EDEDED; ' ;

            }else{

                $result['BgColor'] = 'background-color: #CBCBCB; ' ;

            }

            $count++;
        }

        return $results;
    }

    protected function deleted($table,$WhereDeleted)
    {
        if(($table)&&($WhereDeleted))
        {
        $SQL = '
        DELETE FROM `'._DB_PREFIX_.''.$table.'` WHERE '.$WhereDeleted.'
        ';

            Db::getInstance()->executeS($SQL);

            return 'OK'; //success deleted
        }

        return 100; //miss data

    }

    protected function add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam,$ifselectCheckParam)
    {

        /*
                $SQL = '
                INSERT INTO `'._DB_PREFIX_.'wiki` (parameter_wiki,id_lang,description_wiki,date_add,status)
                VALUES("'.$parameter_wiki.'",'.(int)$id_lang.',"'.$description_wiki.'","'.$date_add.'",'.(int)$status.')
                ';*/
        $SQL = '
			SELECT '.$selectCheckParam.'
			FROM '._DB_PREFIX_.''.$table.'
			WHERE '.$whereCheckParam.' ';

        $result = Db::getInstance()->executeS($SQL);

        if(count($result)==0)
        {
            /*
            $data = array(
                'parameter_wiki' => $parameter_wiki,
                'id_lang' => $id_lang,
                'description_wiki' => $description_wiki,
                'date_add' => $date_add,
                'status' => $status
            );
            */


            if(($table)&&($dataArr))
            {
                //print_r($dataArr);
                //Db::getInstance()->insert($table, $dataArr);
                if((Db::getInstance()->insert($table, $dataArr)))
                {
                    return 'OK';
                }else{
                    return false;
                }

            }else{
                return '100'; //Miss data
            }

        }else{

            return '200'; //Duplicate record

        }

    }

    protected function update($table,$id_lang,$dataArr,$where)
    {

            if(($table)&&($dataArr)&&($where))
            {
                if((Db::getInstance()->update($table, $dataArr, $where)))
                {
                    return 'OK';
                }else{
                    return false;
                }

            }else{
                return '100'; //Miss data
            }

    }


    public function hookHeader($params)
    {
       // $this->context->controller->addCSS($this->_path.'default.css', 'all');

    }

    function hookHome($params)
    {
        //for  vander
        $this->context->controller->addCSS(_THEME_CSS_DIR_.'jquery.flipcountdown.css', 'all');
        $this->context->controller->addJS(_THEME_JS_DIR_.'jquery.flipcountdown.js', 'all');

        $this->context->controller->addCSS(_THEME_CSS_DIR_.'isotope.css', 'all');
        $this->context->controller->addJS(_THEME_JS_DIR_.'isotope.pkgd.min.js', 'all');

//        $SpecialProductArr = $this->getListContent($this->context->language->id, 0,1);
//        $id_products='';
//        foreach($SpecialProductArr as $SpecialProduct)
//        {
//            $id_products .= ','.$SpecialProduct['id_product'];
//        }
//        $id_products = substr($id_products,1);
//        $Q_Product = 'p.id_product IN ('.$id_products.')';
//        $order_by='ORDER BY p.id_product';
//        $products = Search::getAllProductsRow((int)$this->context->cookie->id_lang, $order_by, $Q_Product, '', '', '', '',true);
//
//        $re=$this->getListContent($this->context->language->id, 0 , 1);
//        $this->context->smarty->assign(array(
//         'SpecialProduct' => 'New Product '
//        ,'IconSpecialProduct' => '../modules/'.$this->name.'/logo-special.png'
//        ,'SpecialProductArr' => $this->getResultContent($this->context->language->id, 0,1)
//        ,'products' => $products
//        ));
   //     return $this->display(__FILE__, 'resultpage.tpl');
    }


}
