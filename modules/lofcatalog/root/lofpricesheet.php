<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) May 2011 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 */

require(dirname(__FILE__).'/config/config.inc.php');
ControllerFactory::getController('lofCatalogController')->run();