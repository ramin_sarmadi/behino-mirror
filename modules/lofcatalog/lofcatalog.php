<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}    
/**
 * lofcatalog Class
 */	
class lofcatalog extends Module
{
	/**
	 * @var LofParams $_params;
	 *
	 * @access private;
	 */
	private $_params = '';	
	
	/**
	 * @var array $_postErrors;
	 *
	 * @access private;
	 */
	private $_postErrors = array();		
	
	/**
	 * @var string $__tmpl is stored path of the layout-theme;
	 *
	 * @access private 
	 */	
	
   /**
    * Constructor 
    */
	function __construct()
	{
		$this->name = 'lofcatalog';
		parent::__construct();			
		$this->tab = 'LandOfCoder';				
		$this->version = '2.1';
		$this->displayName = $this->l('Lof Catalog Module');
		$this->description = $this->l('Lof Catalog Module');
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' ) && !class_exists("LofParams", false) ){
			if( !defined("LOF_LOAD_LIB_PARAMS") ){				
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' );
				define("LOF_LOAD_LIB_PARAMS",true);
			}
		}		
		$this->_params = new LofParams( $this->name );		   
	}
  
   /**
    * process installing 
    */
	function install(){		
		if (!parent::install())
			return false;
		if(!$this->registerHook('leftColumn'))
			return false;
        if(!$this->registerHook('header'))
			return false;	
		return true;
	}
	
    public function uninstall()
	{          		
		if (!parent::uninstall())
			return false;        
		return true;
	}    
    
	/*
	 * register hook right comlumn to display slide in right column
	 */
	function hookrightColumn($params)
	{		
		return $this->processHook( $params,"rightColumn");
	}
	
	/*
	 * register hook left comlumn to display slide in left column
	 */
	function hookleftColumn($params)
	{		
		return $this->processHook( $params,"leftColumn");
	}
	
	function hooktop($params)
	{		
		return $this->processHook( $params,"top");
	}
	
	function hookfooter($params)
	{		
		return $this->processHook( $params,"footer");
	}
	
	function hookcontenttop($params)
	{ 		
		return $this->processHook( $params,"contenttop");
	}
	
	
	function hookHeader($params)
	{ 
		$params = $this->_params;
		$theme = $this->_params->get('module_theme','default');
		if(_PS_VERSION_ < "1.5"){
			Tools::addCSS(_MODULE_DIR_.$this->name.'/tmpl/'.$this->_params->get("theme","default").'/assets/style.css', 'all');		     		        
		}else{
			$this->context->controller->addCSS(_MODULE_DIR_.$this->name.'/tmpl/'.$this->_params->get("theme","default").'/assets/style.css');		     		        
		}
	}
  	
	function hooklofTop($params){
		return $this->processHook( $params,"lofTop");
	}
		
	function hookHome($params)
	{
		return $this->processHook( $params,"home");
	}
    
    function hooklofcatalog1($params){
		return $this->processHook( $params,"lofcatalog1");
	}
    
    function hooklofcatalog2($params){
		return $this->processHook( $params,"lofcatalog2");
	}
    
    function hooklofcatalog3($params){
		return $this->processHook( $params,"lofcatalog3");
	}
    
    function hooklofcatalog4($params){
		return $this->processHook( $params,"lofcatalog4");
	}
	
	/**
    * Proccess module by hook
    * $pparams: param of module
    * $pos: position call
    */
	function processHook($pparams, $pos="left"){
		global $smarty, $cookie;                 
		//load param
		$params      = $this->_params;		
		$catSelected = $params->get("category","");
        $themes      = $params->get("module_theme","default");
        
        $id_customer = (int)$cookie->id_customer;
		$id_group = $id_customer ? Customer::getDefaultGroupId($id_customer) : _PS_DEFAULT_CUSTOMER_GROUP_;
        $id_lang = (int)($cookie->id_lang);
        $condition = '';
        if($catSelected){
            $condition = 'AND c.`id_category` IN('.$catSelected.')';
        }
		$maxdepth = 4;
		$id_customer = (int)$pparams['cookie']->id_customer;
		$groups = $id_customer ? implode(', ', Customer::getGroupsStatic($id_customer)) : Configuration::get('PS_UNIDENTIFIED_GROUP');
		if(_PS_VERSION_ >= "1.5"){
			if (!$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
					SELECT c.id_parent, c.id_category, cl.name, cl.description, cl.link_rewrite
					FROM `'._DB_PREFIX_.'category` c
					LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category` AND cl.`id_lang` = '.$id_lang.Shop::addSqlRestrictionOnLang('cl').')
					LEFT JOIN `'._DB_PREFIX_.'category_group` cg ON (cg.`id_category` = c.`id_category`)
					LEFT JOIN `'._DB_PREFIX_.'category_shop` cs ON (cs.`id_category` = c.`id_category`)
					WHERE (c.`active` = 1 '.$condition.')
					'.((int)($maxdepth) != 0 ? ' AND `level_depth` <= '.(int)($maxdepth) : '').'
					AND cg.`id_group` IN ('.pSQL($groups).')
					AND cs.`id_shop` = '.(int)Context::getContext()->shop->id.'
					GROUP BY id_category
					ORDER BY `level_depth` ASC'))
				return ;
		}else{
			if (!$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
					SELECT c.id_parent, c.id_category, cl.name, cl.description, cl.link_rewrite
					FROM `'._DB_PREFIX_.'category` c
					LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category` AND `id_lang` = '.$id_lang.')
					LEFT JOIN `'._DB_PREFIX_.'category_group` cg ON (cg.`id_category` = c.`id_category`)
					WHERE ((c.`active` = 1 '.$condition.') OR c.`id_category` = 1) AND cg.`id_group` = '.$id_group.'
					ORDER BY `level_depth` ASC, c.`position` ASC')
				)
					return;
		}
        $resultParents = array();
		$resultIds = array();       
		foreach ($result as &$row)
		{
			$resultParents[$row['id_parent']][] = &$row;
			$resultIds[$row['id_category']] = &$row;
            
		}
		if(_PS_VERSION_ >= "1.5"){
       		$blockCategTree = $this->getTreeV15($resultParents, $resultIds, 4);
		}else{
			$blockCategTree = $this->getTreeV14($resultParents, $resultIds, 4);
		}
		unset($resultParents);
		unset($resultIds);
		$smarty->assign( array(	
                              'moduleClass'		=> $params->get("moduleClass","block"),
                              'modName'         => $this->name,
                              'pos'             => $pos,
							  'blockid' 		=> $this->id,
                              'modtitle'		=> $params->get("moduleTitle","Lof Catalog"),
                              'lofcatalog_link'		=> ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://".$_SERVER['HTTP_HOST']._MODULE_DIR_.'lofcatalog/',
						));
        $smarty->assign('blockCategTree', $blockCategTree);                
        if (file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/tmpl/'.$themes.'/menu.tpl'))
				$smarty->assign('branche_tpl_path', _PS_THEME_DIR_.'modules/'.$this->name.'/tmpl/'.$themes.'/category-tree-branch.tpl');
			else
				$smarty->assign('branche_tpl_path', _PS_MODULE_DIR_.$this->name.'/tmpl/'.$themes.'/category-tree-branch.tpl');
                        
        return $this->display(__FILE__, 'tmpl/'.$params->get("module_theme","default").'/menu.tpl');	    					
	}
    
    public function getTreeV15($resultParents, $resultIds, $maxDepth, $id_category = null, $currentDepth = 0)
	{
		global $cookie;
		$id_lang = $cookie->id_lang;
		if (is_null($id_category))
			$id_category = $this->context->shop->getCategory();
        
		$children = array();
		if (isset($resultParents[$id_category]) AND sizeof($resultParents[$id_category]) AND ($maxDepth == 0 OR $currentDepth < $maxDepth))
			foreach ($resultParents[$id_category] as $subcat)
				$children[] = $this->getTreeV15($resultParents, $resultIds, $maxDepth, $subcat['id_category'], $currentDepth + 1);
		if (!isset($resultIds[$id_category]))
			return false;
		$linkCat = '';	
		if(file_exists(_PS_MODULE_DIR_."lofcatalog/libs/tcpdf/files/".$id_lang."/".$id_category.".pdf")){
			$linkCat = _MODULE_DIR_."lofcatalog/libs/tcpdf/files/".$id_lang."/".$id_category.".pdf";
		}
		
		$return = array('id' => $id_category, 'link' => $linkCat,
					 'name' => $resultIds[$id_category]['name'], 'desc'=> $resultIds[$id_category]['description'], 
					 'children' => $children);
		return $return;
	}
    
	public function getTreeV14($resultParents, $resultIds, $maxDepth, $id_category = 1, $currentDepth = 0)
	{
		global $cookie;
		$id_lang = $cookie->id_lang;
		$children = array();
		if (isset($resultParents[$id_category]) && count($resultParents[$id_category]) && ($maxDepth == 0 || $currentDepth < $maxDepth))
			foreach ($resultParents[$id_category] as $subcat)
				$children[] = $this->getTreeV14($resultParents, $resultIds, $maxDepth, $subcat['id_category'], $currentDepth + 1);
		if (!isset($resultIds[$id_category]))
			return false;
		$linkCat = '';	
		if(file_exists(_PS_MODULE_DIR_."lofcatalog/libs/tcpdf/files/".$id_lang."/".$id_category.".pdf")){
			$linkCat = _MODULE_DIR_."lofcatalog/libs/tcpdf/files/".$id_lang."/".$id_category.".pdf";
		}
		$return = array('id' => $id_category, 'link' => $linkCat,
					 'name' => $resultIds[$id_category]['name'], 'desc'=> $resultIds[$id_category]['description'],
					 'children' => $children);
		return $return;
	}
	
   /**
    * Get list of sub folder's name 
    */
	public function getFolderList( $path ) {
		$items = array();
		$handle = opendir($path);
		if (! $handle) {
			return $items;
		}
		while (false !== ($file = readdir($handle))) {
			if (is_dir($path . $file))
				$items[$file] = $file;
		}
		unset($items['.'], $items['..'], $items['.svn']);
		
		return $items;
	}
	
   /**
    * Render processing form && process saving data.
    */	
	public function getContent()
	{
	    global $smarty;   
		$html = "";        
		if (Tools::isSubmit('submit'))
		{
		      
			$this->_postValidation();

			if (!sizeof($this->_postErrors))
			{													
		        $definedConfigs = array(
                  'module_theme'  	  => '',
                  'moduleTitle'		  => '',			
                  'moduleClass'       => '',
                  'defSort'			  => '',	                    
                  'img_size'          => '',
                  'shortdesleng'      => '',
                  'longdesleng'       => '',
                  'loadfirst'         => '',                                 
                  //pdf config
                  'pdf_ma_top'        => '','pdf_ma_rig'        => '','pdf_ma_bot'        => '','pdf_ma_lef'        => '',
                  'page_format'       => '','page_orien'        => '','font_type'         => '','font_color'        => '',
                  'cell_hei'          => '','pdfName'           => '','pdfDes'            => '','pdfCache'          => '',
                  'pdfImgSca'         => '','he_display'        => '','he_dis_line'       => '',
                  'he_align'          => '','he_foType'         => '','he_foSize'         => '','he_foStyle'        => '',
                  'he_foColor'        => '','he_lineColor'      => '','he_bgColor'        => '','he_cell'           => '',
                  'he_mar'            => '','fo_display'        => '','fo_dis_line'       => '','fo_align'          => '',
                  'fo_dis_pa'         => '','fo_foType'         => '','fo_foColor'        => '','fo_lineColor'      => '',
                  'fo_bgColor'        => '','fo_cell'           => '','fo_mar'            => '','font_size'         => '',
                  'disViewPro'        => '','disPrint'          => '','disPdf'            => '','disExcel'          => '',
				  'pdfimg_size' 	  => '','pdftextCo' 	    => '','catNameCo' 	      => '','proNameCo' 	    => '',
                  'proPriceCo' 	    => '','proQuanCo' 	    => ''
		        );                                
                if (isset($_FILES["lofbackgroup"]['name']) && $_FILES["lofbackgroup"]['name'] != NULL ){
                    $uploadResult = $this->_lofUpload("lofbackgroup");                
                    if($uploadResult){                   
                        $_POST["bgfile"] = $uploadResult;
                        $definedConfigs["bgfile"]  = '';    
                    }else{                  
                        $html .= "<div>".$this->l("Can't upload backgroup image")."</div>";
                    }
                }    

                foreach( $definedConfigs as $config => $key ){
		            if(strlen($this->name.'_'.$config)>=32){
		              echo $this->name.'_'.$config;
		            }else{
		              Configuration::updateValue($this->name.'_'.$config, Tools::getValue($config), true);  
		            } 		      		
		    	}
                                                                         
                if(Tools::getValue('category')){
    		        if(in_array("",Tools::getValue('category'))){
    		          $catList = "";
    		        }else{
    		          $catList = implode(",",Tools::getValue('category'));  
    		        }
                    Configuration::updateValue($this->name.'_category', $catList, true);
                }
                if(Tools::getValue('listcheck')){
    		        if(in_array("",Tools::getValue('listcheck'))){
    		          $catList = "";
    		        }else{
    		          $catList = implode(",",Tools::getValue('listcheck'));  
    		        }
                    Configuration::updateValue($this->name.'_listcheck', $catList, true);
                }
                if(Tools::getValue('listdis')){
    		        if(in_array("",Tools::getValue('listdis'))){
    		          $catList = "";
    		        }else{
    		          $catList = implode(",",Tools::getValue('listdis'));  
    		        }
                    Configuration::updateValue($this->name.'_listdis', $catList, true);
                }
                                                				
		        $html .= '<div class="conf confirm">'.$this->l('Settings updated').'</div>';
			}
			else
			{
				foreach ($this->_postErrors AS $err)
				{
					$html .= '<div class="alert error">'.$err.'</div>';
				}
			}
			// reset current values.
			$this->_params = new LofParams( $this->name );	
		}else{
            $catId      = Tools::getValue('catid');
            $langId     = Tools::getValue('langid');
            if($catId && $langId){
                $result    = $this->findPro($catId,$langId);                
                $params      = $this->_params;                 
                     
                require_once( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/lofpdf.php' );
                
                $params->set("lang_id",$langId);
                $params->set("cat_id",$catId);                 
                $pdf = new lofPDF($params);
                $pdf->setContent($result);
                $pdf->render(false);
                die();  
            }
		}
			
		return $html.$this->_getFormConfig();
	}
	    
    
    public function findPro($catId, $langId)
	{
		global $cookie;
        
		$link = new Link();
		$db = Db::getInstance(_PS_USE_SQL_SLAVE_);			
        $params      = $this->_params;
        $whereCat    = ' AND cp.`id_category` = '.$catId.' ';
        
        $img_size = $params->get("img_size","large");
        $defaultSourt = $params->get("defSort",'position');
		
        $arrSort = explode('_',$defaultSourt);
		$orderBy = '';
		if(count($arrSort) > 1)
			$orderBy = ' p.'.$arrSort[0].' '.$arrSort[1];
		elseif($defaultSourt == 'position')
			$orderBy = ' cpro.'.$arrSort[0];     
             
        $query = '
        		SELECT cp.`id_product`, cp.`id_category`, cl.`name` category_name, cl.`link_rewrite`, cl.description
        		FROM `'._DB_PREFIX_.'category_group` cg
        		INNER JOIN `'._DB_PREFIX_.'category_product` cp ON cp.`id_category` = cg.`id_category`
        		INNER JOIN `'._DB_PREFIX_.'category` c ON cp.`id_category` = c.`id_category`
                INNER JOIN `'._DB_PREFIX_.'category_lang` cl ON cl.`id_category` = cp.`id_category` 
        		INNER JOIN `'._DB_PREFIX_.'product` p ON cp.`id_product` = p.`id_product`
        		WHERE c.`active` = 1 AND p.`active` = 1'.$whereCat.' AND id_lang ='.(int)$langId;
                
        $eligibleProducts = $db->ExecuteS($query);        
        $catInfo = array();
        if($eligibleProducts){
            $catInfo["id_category"]    = $eligibleProducts[0]["id_category"];
            $catInfo["category_name"]  = $eligibleProducts[0]["category_name"];
            $catInfo["link_rewrite"]   = $eligibleProducts[0]["link_rewrite"];
            $catInfo["description"]    = $eligibleProducts[0]["description"];
            $catInfo["link"]           = $link->getCategoryLink($catInfo["id_category"],$catInfo["link_rewrite"],$langId);    
        }else{
            $query = 'SELECT c.*,cl.`name` category_name,cl.description,cl.link_rewrite FROM `'._DB_PREFIX_.'category` AS c
                      INNER JOIN `'._DB_PREFIX_.'category_lang` AS cl ON c.`id_category` = cl.`id_category`
                      WHERE c.`active` = 1 AND c.`id_category`='.$catId.'  AND id_lang ='.(int)$langId;                      
            $catInfo = $db->ExecuteS($query);
            if($catInfo){
                $catInfo[0]["link"] = $link->getCategoryLink($catInfo[0]["id_category"],$catInfo[0]["link_rewrite"],$langId);    
            }
            return array("catInfo"=>$catInfo[0],"product"=>array());              
        }
                                    
        //get product ID
		$productPool = '';
        $productCat  = '';
		foreach ($eligibleProducts AS $product){
		    if(!isset($productCat[$product['id_product']])){
		      if (!empty($product['id_product']))
				$productPool .= (int)$product['id_product'].',';  
		    }            
            $productCat[$product['id_product']][] = array("category_name"=>$product['category_name'],"id_category"=>$product['id_category'],"link_rewrite"=>$product['link_rewrite']);
		}		
		if (empty($productPool)) return array();              
		$productPool = ((strpos($productPool, ',') === false) ? (' = '.(int)$productPool.' ') : (' IN ('.rtrim($productPool, ',').') '));
        
		if(_PS_VERSION_ >= "1.5") {
			$result = $this->getProductsV15($orderBy, $productPool, $langId);
		}else{
			$result = $this->getProductsV14($orderBy, $productPool, $langId);
		}
        
        foreach($result as &$row){            
            $images = $this->getImages($row["id_product"],(int)($langId));
                     
			$row["imgs"] = array();            
			foreach ($images AS $k => $image)
			{
			    if(!$image["cover"]){
                    $row["imgs"][] = $link->getImageLink($row["link_rewrite"], $row["id_product"].'-'.$image["id_image"], $img_size );     			       
			    } 			    				
			}
            //price
            /*$product["lofPrice"] = '';
            if(isset($product["on_sale"]) && $product["on_sale"] && isset($product["show_price"]) && $product["show_price"] && !$PS_CATALOG_MODE){
                $product["lofPrice"] = '<span class="on_sale">'.$this->l("On sale!").'</span>';
            }else if(isset($product["reduction"]) && $product["reduction"] && isset($product["show_price"]) && $product["show_price"] && !$PS_CATALOG_MODE){
                $product["lofPrice"] .= '<span class="discount">'.$this->l("Reduced price!").'</span>';
            }
            if(isset($product["online_only"]) && $product["online_only"]){
                $product["lofPrice"] .= '<span class="online_only">'.$this->l("Online only!").'</span>';
            }
            if(!$PS_CATALOG_MODE AND ((isset($product["show_price"]) && $product["show_price"]) || (isset($product["available_for_order"]) && $product["available_for_order"]))){
                 $product["lofPrice"] .= '<div>';
                 if(isset($product["show_price"]) && $product["show_price"] && !isset($restricted_country_mode)){
                    $product["lofPrice"] .= '<span class="price" style="display: inline;">';
                    if(!$priceDisplay)}{convertPrice|htmlentities:2:'UTF-8' price=$product.price}{else}{convertPrice|htmlentities:2:'UTF-8' price=$product.price_tax_exc}{/if}
                    $product["lofPrice"] .= '</span><br />'; 
                 }
                 $product["lofPrice"] .= '</div>';
                
					{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice|htmlentities:2:'UTF-8' price=$product.price}{else}{convertPrice|htmlentities:2:'UTF-8' price=$product.price_tax_exc}{/if}</span><br />{/if}
					{if isset($product.available_for_order) && $product.available_for_order && !isset($restricted_country_mode)}<span class="availability">{if ($product.allow_oosp || $product.quantity > 0)}{l s='Available'}{else}{l s='Out of stock'}{/if}</span>{/if}
				</div>
            } */           												
            $row["price"]    = Tools::displayPrice($row['price']);
                   
            $row["mainImge"] = $link->getImageLink($row["link_rewrite"], $row["id_image"], $img_size );
            
            //quantity
            if($row["quantity"]>1){
                $row["quantity"] = $row["quantity"].' '.$this->l("items in stock");    
            }else{
                $row["quantity"] = $row["quantity"].' '.$this->l("item in stock");
            }
            
            $row["textLang"] = array("des"=>$this->l("Description:"),"feature"=>$this->l("Feature:"),"manu"=>$this->l("Manufacturer:"));
            /*if(strlen($row['description_short'])>$params->get( 'shortdesleng',100)){                                                                                
                $row['description_short'] = trim(self::substr(trim(strip_tags($row['description_short'])),0,$params->get( 'shortdesleng',100))," .")."...";                                 
            }
            if(strlen($row['description'])>$params->get( 'longdesleng',200)){                                                                                  
                $row['description'] = trim(self::substr(trim(strip_tags($row['description'])),0,$params->get( 'longdesleng',100))," .")."...";
            }*/
        }
        
        return array("catInfo"=>$catInfo,"product"=>$result);                                 		
	} 
    public function getProductsV14($orderBy, $productPool, $langId){

		$id_country = (int)Country::getDefaultCountryId();
		$queryResults = '
    		SELECT SQL_CALC_FOUND_ROWS p.*, pl.`description_short`, pl.`description`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`name`, pa.`id_product_attribute`,
    			tax.`rate`, i.`id_image`, il.`legend`, m.`id_manufacturer` , m.`name` manufacturer_name , DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 new
    		FROM '._DB_PREFIX_.'product p
    		INNER JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$langId.')
    		LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
    		                                           AND tr.`id_country` = '.$id_country.'
    	                                           	   AND tr.`id_state` = 0)
    	    LEFT JOIN `'._DB_PREFIX_.'tax` tax ON (tax.`id_tax` = tr.`id_tax`)
    		LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
    		LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
    		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
    		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$langId.')
            LEFT JOIN `'._DB_PREFIX_.'category_product` cpro ON (p.`id_product`=cpro.`id_product` AND p.`id_category_default`=cpro.`id_category`)
    		WHERE p.`active` = 1 AND p.`id_product` '.$productPool.' ORDER BY '.$orderBy;
			
        $result = Db::ExecuteS($queryResults);
        return Product::getProductsProperties($langId, $result);  
	}
	
    public function getProductsV15($orderBy, $productPool, $langId){
		$id_lang = $langId;
		
		$context = Context::getContext();
		$front = true;
		if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
			$front = false;
		$sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, product_attribute_shop.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`available_now`,
				pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`,
				il.`legend`, m.`name` AS manufacturer_name, tl.`name` AS tax_name, t.`rate`, cl.`name` AS category_default,
				DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),
				INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
					DAY)) > 0 AS new,
				(product_shop.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1)) AS orderprice
			FROM `'._DB_PREFIX_.'category_product` cp
			LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product`
			'.Shop::addSqlAssociation('product', 'p').'
			LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product`)
			'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
			'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (product_shop.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
			LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group` AND tr.`id_country` = '.(int)$context->country->id.'
				AND tr.`id_state` = 0
				AND tr.`zipcode_from` = 0)
			LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
			LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)$id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
			
			LEFT JOIN `'._DB_PREFIX_.'category_product` cpro ON (p.`id_product`=cpro.`id_product` AND p.`id_category_default`=cpro.`id_category`)
			
			WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
			AND ((product_attribute_shop.id_product_attribute IS NOT NULL OR pa.id_product_attribute IS NULL) 
				OR (product_attribute_shop.id_product_attribute IS NULL AND pa.default_on=1))
				AND product_shop.`active` = 1  AND p.`id_product` '.$productPool
				.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').
			' ORDER BY '.$orderBy;
			
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);		
		return Product::getProductsProperties($id_lang, $result);
	}
	
    private function _lofUpload($name){        
        if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != NULL ){
           	if(_PS_VERSION_ >= "1.5"){
				$error = checkImage($_FILES[$name], 100000);	
			}else{
				$error = checkImageUploadError($_FILES[$name]);
			}
            if (!$error){             
                $ext = substr($_FILES[$name]['name'], strrpos($_FILES[$name]['name'], '.') + 1);
                $attachFileTypes = array("jpg","JPG","bmp","BMP","gif","GIF","png","PNG");
                if(in_array($ext, $attachFileTypes)){
                    $uploadFolder = _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/tcpdf/images';                              
                    if(!is_dir($uploadFolder)){
                        mkdir($uploadFolder, 0777);
                    }                    
          			if(@move_uploaded_file($_FILES[$name]['tmp_name'], $uploadFolder."/".$_FILES[$name]["name"])){
          			   return $_FILES[$name]["name"];   
       			    }else{
       			       return false;
       			    }
          		}else{
          		    return false;     
          		}    
            }else{                 
                return false;
            }				
        }
        return false;
    }
    
    static function substr($str, $start, $length = false, $encoding = 'utf-8')
	{
		if (is_array($str))
			return false;
		if (function_exists('mb_substr'))
			return mb_substr($str, (int)($start), ($length === false ? self::strlen($str) : (int)($length)), $encoding);
		return substr($str, $start, ($length === false ? self::strlen($str) : (int)($length)));
	}
    
    public function	getImages($id,$id_lang)
	{
		return Db::getInstance()->ExecuteS('
		SELECT i.`cover`, i.`id_image`, il.`legend`
		FROM `'._DB_PREFIX_.'image` i
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
		WHERE i.`id_product` = '.(int)($id).'
		ORDER BY `position`');
	}
    
	/**
	 * Render Configuration From for user making settings.
	 *
	 * @return context
	 */
	private function _getFormConfig(){		
		$html = '';
        $mes = '';        
        
	    $formats = ImageType::getImagesTypes( 'products' );
	    $themes=$this->getFolderList( dirname(__FILE__)."/tmpl/" );
        
        //font:
        $listFont = $this->_params->getFileList(dirname(__FILE__)."/libs/pdf_fonts/",'/(\.php)$/i');        
        
	    ob_start();
	    include_once dirname(__FILE__).'/config/lofcatalog.php'; 
	    $html .= ob_get_contents();
	    ob_end_clean(); 
		return $html;
	}
    
	/**
     * Process vadiation before saving data 
     */
	private function _postValidation()
	{
		/*if (!Validate::isCleanHtml(Tools::getValue('module_height')))
			$this->_postErrors[] = $this->l('The module height you entered was not allowed, sorry');
		if (!Validate::isCleanHtml(Tools::getValue('module_width')))
			$this->_postErrors[] = $this->l('The module width you entered was not allowed, sorry');
		if (!Validate::isCleanHtml(Tools::getValue('des_max_chars')) || !is_numeric(Tools::getValue('des_max_chars')))
			$this->_postErrors[] = $this->l('The description max chars you entered was not allowed, sorry');	
		if (!Validate::isCleanHtml(Tools::getValue('main_height')) || !is_numeric(Tools::getValue('main_height')))
			$this->_postErrors[] = $this->l('The Main Image Height you entered was not allowed, sorry');
		if (!Validate::isCleanHtml(Tools::getValue('main_width')) || !is_numeric(Tools::getValue('main_width')))
			$this->_postErrors[] = $this->l('The Main Image Width you entered was not allowed, sorry');        
        if (!Validate::isCleanHtml(Tools::getValue('limit_items')) || !is_numeric(Tools::getValue('limit_items')))
			$this->_postErrors[] = $this->l('The limit items you entered was not allowed, sorry');
        if (!Validate::isCleanHtml(Tools::getValue('currentItem')) || !is_numeric(Tools::getValue('currentItem')))
			$this->_postErrors[] = $this->l('The Current Item item you entered was not allowed, sorry');*/                   							
	}
	
   /**
    * Get value of parameter following to its name.
    * 
	* @return string is value of parameter.
	*/
	public function getParamValue($name, $default=''){
		return $this->_params->get( $name, $default );	
	}	  	  		
} 