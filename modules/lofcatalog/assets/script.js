$('document').ready( function() {
    $(".lofuse").click(function() {
        if($(this).attr("checked")){
            $("#"+$(this).attr("id")+"_wrapper").show();        
        }else{
            $("#"+$(this).attr("id")+"_wrapper").hide(); 
        }
    });
    $(".selectall").click(function() {
        if($(this).attr("checked")){
           $("."+$(this).attr("id")).attr("checked",true);          
        }else{
           $("."+$(this).attr("id")).attr("checked",false);
        }
    });
    $(".lofgroupchk").click(function() {
        listClass = $(this).attr("class").split(/\s+/);
        chkClass  = listClass[0];
        if($(this).attr("checked")){                                               
            $checked = true;                
            $('.' + chkClass).each(function(index) {
                if(!$(this).attr("checked")){
                    $checked = false; 
                }                   
            });
            $("#" + chkClass).attr("checked",$checked);        
        }else{
            $("#" + chkClass).attr("checked",false);          
        }
    });        
});
//print product
function lofprint(){       
   if($("#lof-print").attr("class").indexOf("button-unactice") !=-1){
     return;
   }
   
   if($("lof-print"))
   $("#loftype").val("print");    
   $("#frmpricesheet").submit(); 
}
//print product
function lofpdf(){
   if($("#lof-pdf").attr("class").indexOf("button-unactice") !=-1){
     return;
   } 
   $("#loftype").val("pdf");
   $("#ajaxSearch").val("1");      
   $("#frmpricesheet").submit(); 
}

function lofExcel(){
    if($("#lof-excel").attr("class").indexOf("button-unactice") !=-1){
     return;
    }
    $("#loftype").val("excel");    
    $("#frmpricesheet").submit();
}

//view product:
function lofViewPro(url){ 
    $("#loftype").val("viewproduct");
    $("#ajaxSearch").val("1");
    data = $("#frmpricesheet").serialize();    
    lof_ajax_load(url, data);
    return false;
}

function lofPaging(url){
    $("#loftype").val("viewproduct");
    $("#ajaxSearch").val("1");
    data = $("#frmpricesheet").serialize();    
    lof_ajax_load(url, data);    
}
//process ajax
function lof_ajax_load(url, data) {
	lof_displayLoading();
    $.post(url, data, function(res) {	
		lof_parseData(res);
	}, 'json');
}
//loading span
function lof_displayLoading() {			
    $('#lof_loading').show();
}
//parce data
function lof_parseData(response) {		
	if($('#lof_loading')) {					
		$('#lof_loading').hide();
	}	
	var result = true;
	var myResponse = null;
    
	if(response.data){			
		myResponse = response.data;			
	}else{			
		myResponse = response;		
	}    
	$.each(myResponse, function(i, item) {	    
		var divId = item.id;	
		var type = item.type;
		var content = item.content;
        	   
		if ($(divId) != "undefined") {				
			if (type == 'html') {
				$(divId).html(content);				
			} else if (type == 'class') {
				$(divId).attr('class', '');
				$(divId).addClass(content);
			}
            else if (type == 'removeclass') {			     
                 $(divId).removeClass(content);
			}            
			else if (type == 'css') {
				var arr = content.split(',');
				$(divId).css(arr[0], arr[1]);
			}
			else if(type=='reload'){
				content = content.replace('&amp;', '&');
				location.href = content;
			}
			else if(type=='append'){
				$(divId).append(content);
			}else{	
				$(divId).attr(type, content);
			}				
		}		
	});	
}