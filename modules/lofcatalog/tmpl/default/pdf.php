<?php
    //set font for category title
	$realPath = '';
    $pdf->SetFont('freesansb', '', 20);   
    $html = '<h1><a style="color: #'.$params->get("catNameCo","ffffff").';text-transform: uppercase;font-weight: bold;text-decoration: none;" href="'.$result["catInfo"]["link"].'">'.$result["catInfo"]["category_name"].'</a></h1>';
    // output the HTML content
    $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->SetFont('freesans', '', 10);
    $html = '<span style="color:#'.$params->get("pdftextCo","000000").';">'.$result["catInfo"]["description"].'</span><br/>';
    $pdf->writeHTML($html, true, false, true, false, '');
    $listDis = $params->get("listdis");
   
$html = '<ul style="color:#'.$params->get("pdftextCo","000000").';list-style-type:img|png|4|4|../modules/lofcatalog/libs/tcpdf/images/bullet_red.png">';    
    foreach($result["product"] as $product){
        //print_r($product);die;
        $html .= '<li style="margin-top:10px;">';
            //$listDis
            if(strpos($listDis,"proName") !== false)
            $html .= '<a style="background-color:#6495ed;color:#'.$params->get("proNameCo","b22222").';font-weight: bold;text-decoration: none;font-size: large;" href="'.$product["link"].'">'.$product["name"].'</a>';
            
            if(strpos($listDis,"proPrice") !== false)
            $html .= '<span style="background-color:#6495ed;color:#'.$params->get("proPriceCo","ff0000").';font-weight: bold;font-size: large;"> | '.$product["price"].'</span>';
                                    
            if(strpos($listDis,"proQuan") !== false){                
                $html .= '<span style="background-color:#6495ed;color:#'.$params->get("proQuanCo","800000").';font-size: large;font-weight: bold"> | '.$product["quantity"].'</span><br/>';                   
            }
            if(strpos($listDis,"proManu") !== false && $product["manufacturer_name"]){
                $html .= '<h4>'.$product["textLang"]["manu"].'</h4>';
                $html .= '<span style="color:#a52a2a">'.$product["manufacturer_name"].'</span>';
            }                
                
            if(strpos($listDis,"proFea") !== false && $product["features"]){
                $html .= '<h4>'.$product["textLang"]["feature"].'</h4>';                
                $html .= '<ol>';
                foreach($product["features"] as $feature){
                    $html .= '<li>'.$feature["name"]." : ".$feature["value"].'</li>';                                              
                }                
                $html .= '</ol>';
            }
                                                                                   
            if(strpos($listDis,"proImg") !== false){
                $html .= '<div style="text-align:center">';
				
				//if($realPath=$this->returnRealPath($product["mainImge"])){
                	$html .= '<img src="'.$product["mainImge"].'" title="'.$product["name"].'"/>';
				//}
                $html .= '</div><br/>';
                if($product["imgs"]){
                    $html .= '<table style="background-color:white;"><tr>';                                
                        foreach($product["imgs"] as $img){
							//if($realPath=$this->returnRealPath($img)){
	                            $html .= '<td><img src="'.$img.'" title="'.$product["name"].'"/></td>';
							//}
                        }                
                    $html .= '</tr></table>';
                }
            }
            
            
            if(strpos($listDis,"proSortDes") !== false){
                $html .= '<h4>'.$product["textLang"]["des"].'</h4>';
                $html .= '<span style="text-align:justify;font-size: small">'.$product["description_short"].'</span><br/>';
            }
            
            
            if(strpos($listDis,"proLongDes") !== false){
                $html .= '<h4>'.$product["textLang"]["des"].'</h4>';
                $html .= '<span style="text-align:justify;font-size:10px;font-size: small">'.$product["description"].'</span><br/>';
            } 
            
        $html .= '</li>';                                                            
    }
$html .= '</ul>';       
    //die($html);
    $html = <<<EOF
    {$html}
EOF;
    $pdf->writeHTML($html, true, false, true, false, '');
    //product page