<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
    
$languages = Language::getLanguages(true);
?>
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.css";?>" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/jquery-ui.css";?>" type="text/css" media="screen" charset="utf-8" />
<script type="text/javascript" src="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.js";?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
    /*var $tabs = $('#lof-pdf-tab').tabs({
        add: function(event, ui) {
            $tabs.tabs('select', '#' + ui.panel.id);
            $('.Multiple').jPicker();
        }
    });*/
    lofCulink = "<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>";
    lofCatList = Array();
    lofLang  = new Array(<?php foreach($languages as $lang){echo $lang["id_lang"].",";};?>999);            
  });
  
  function beginProcessAjax(landCount,id){
    catid = lofCatList[id];              
    if(id>(lofCatList.length-1)) return;
        
    var langid = lofLang[landCount];
    data = 'catid='+catid+'&langid='+langid;  
    $("#lofcat-"+catid+" .lof-dowpdf"+langid).hide();
    $("#lofcat-"+catid+" .lof-loading"+langid).css("display","inline-block");
        
    $.ajax({
      url: lofCulink,             
      data: data,
      success: function(respond){
        $("#lofcat-"+catid+" .lof-dowpdf"+langid).show();
        $("#lofcat-"+catid+" .lof-loading"+langid).hide();
        landNext = lofLang[landCount+1];
        if(landNext=="999"){
            beginProcessAjax(0,id+1);
        }else{
            beginProcessAjax(landCount+1,id);
        }             
      }              
    }); 
  }
  
  function lofGeneratePdf(){
    if($(".lof-checkpdf:checked").length <= 0){
        alert("<?php echo $this->l("Please select category to genreate PDF");?>");
        return false; 
    }    
    catList = Array();
    i = 0;
    $(".lof-checkpdf").each(function(index) {        
        if($(this).is(':checked')){           
            id = $(this).val();                      
            lofCatList[i] = id;
            i++;                                                     
        }
    });
     
    beginProcessAjax(0,0);    
    return false;
  }
</script>
<?php  if(version_compare(_PS_VERSION_, '1.4.2.5', '<')){?>
<?php
global $cookie;
$iso = Language::getIsoById((int)($cookie->id_lang));
echo ' <script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
				<script type="text/javascript">
					tinyMCE.init({
						mode : "textareas",
						theme : "advanced",
						plugins : "safari,pagebreak,style,layer,table,advimage,advlink,inlinepopups,media,searchreplace,contextmenu,paste,directionality,fullscreen",
						// Theme options
						theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
						theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
						theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
						theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						theme_advanced_resizing : false,
						content_css : "'.__PS_BASE_URI__.'themes/'._THEME_NAME_.'/css/global.css",
						document_base_url : "'.__PS_BASE_URI__.'",
						width: "600",
						height: "auto",
						font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
						elements : "nourlconvert,ajaxfilemanager",
						file_browser_callback : "ajaxfilemanager",
						entity_encoding: "raw",
						convert_urls : false,
						language : "'.(file_exists(_PS_ROOT_DIR_.'/js/tinymce/jscripts/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en').'"
						
					});
					function ajaxfilemanager(field_name, url, type, win) {
						var ajaxfilemanagerurl = "'.dirname($_SERVER["PHP_SELF"]).'/ajaxfilemanager/ajaxfilemanager.php";
						switch (type) {
							case "image":
								break;
							case "media":
								break;
							case "flash": 
								break;
							case "file":
								break;
							default:
								return false;
					}
		            tinyMCE.activeEditor.windowManager.open({
		                url: "'.dirname($_SERVER["PHP_SELF"]).'/ajaxfilemanager/ajaxfilemanager.php",
		                width: 782,
		                height: 440,
		                inline : "yes",
		                close_previous : "no"
		            },{
		                window : win,
		                input : field_name
		            });
            
		}
	</script>';
?>
<?php }else{
        // TinyMCE
		global $cookie;
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		echo '
			<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
}?>
<h3><?php echo $this->l('Lof Catalog');?></h3>
<?php
//register Yes - No Lang
$yesNoLang = array("1"=>$this->l('Yes'),"0"=>$this->l('No'));
$showHideLang = array("1"=>$this->l('Show'),"0"=>$this->l('Hide'));
$positHeader = array("L"=>$this->l('Left'),"R"=>$this->l('Right'),"C"=>$this->l('Center'),"J"=>$this->l('Justify'));
$pdfDes = array("I"=>$this->l('Inline Browser'),"D"=>$this->l('Force File Download'),"S"=>$this->l('Inline Browser (Popup)'));
$fonStyHeader = array(""=>$this->l('Regular'),"B"=>$this->l('Bold'),"I"=>$this->l('Italic'),"BI"=>$this->l('Bold Italic'),"U"=>$this->l('Underline'),"D"=>$this->l('Line Through'));
?>
<form action="<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>" enctype="multipart/form-data" method="post" id="lofform">
 <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />             
  
  <fieldset>
    <legend><img src="../img/admin/contact.gif" /><?php echo $this->l('General Setting'); ?></legend>
        <div class="lof_config_wrrapper clearfix"><ul>
            <?php
                echo $this->_params->selectTag("module_theme",$themes,$this->getParamValue("module_theme",'default'),$this->l('Theme - Layout'),'class="inputbox"', 'class="row" title="'.$this->l('Select a theme').'"');                
				echo $this->_params->inputTag("moduleClass",$this->getParamValue("moduleClass","block"),$this->l('Module Class'),'','class="row"','');
                echo $this->_params->getCategory("category[]",$this->getParamValue("category",""),$this->l('Select category'),'size="10" multiple="multiple" style="width: 90%;" class="inputbox"','class="row"','','',$this->l('All Categories'));
                $valDisP  = array (
                                    "proName"=>array("langName"=>$this->l("Name"),"langCheck"=>$this->l("Default checked or not")),
                                    "proPrice"=>array("langName"=>$this->l("Price"),"langCheck"=>$this->l("Default checked or not")),
                                    "proImg"=>array("langName"=>$this->l("Image"),"langCheck"=>$this->l("Default checked or not")),
                                    "proManu"=>array("langName"=>$this->l("Manufacture"),"langCheck"=>$this->l("Default checked or not")),
                                    "proQuan"=>array("langName"=>$this->l("Quantity"),"langCheck"=>$this->l("Default checked or not")),
                                    "proFea"=>array("langName"=>$this->l("Feature"),"langCheck"=>$this->l("Default checked or not")),
                                    "proSortDes"=>array("langName"=>$this->l("Short Description"),"langCheck"=>$this->l("Default checked or not")),
                                    "proLongDes"=>array("langName"=>$this->l("Long Description"),"langCheck"=>$this->l("Default checked or not")),                                  
                                    );							
                echo $this->_params->displayList($valDisP,$this->getParamValue("listdis","proName,proPrice,proImg,proQuan,proFea,proSortDes"),$this->getParamValue("listcheck","proName,proPrice,proDes"),$showHideLang,$this->l('Select Display Field'),'','class="row"','class="row"');
                //echo $this->_params->inputTag("shortdesleng",$this->getParamValue("shortdesleng","100"),$this->l('Short Description Max Chars'),'class="text_area"','class="row"','',$this->l("Set Emty to not cut string"));
                //echo $this->_params->inputTag("longdesleng",$this->getParamValue("longdesleng","200"),$this->l('Long Description Max Chars'),'class="text_area"','class="row"','',$this->l("Set Emty to not cut string"));               
                                                                                                                                
                $sort_list = array("position"=>$this->l("Position"),"price_asc"=>$this->l("Price: lowest first"),"price_desc"=>$this->l("Price: highest first"),"name_asc"=>$this->l("Product Name: A to Z"),"name_desc"=>$this->l("Product Name: Z to A"),"quantity_desc"=>$this->l("In-stock first"));
                echo $this->_params->selectTag("defSort",$sort_list,$this->getParamValue("defSort","manu"),$this->l('Default sort by'),"",'class="row"','');                                                                
                
                foreach ($formats as $k=> $format):
                $mainImgSize[$format['name']] = $format['name'].'('.$format['width']."x".$format['height'].')';
                endforeach;
                echo $this->_params->selectTag("img_size",$mainImgSize,$this->getParamValue("img_size","large"),$this->l('Image Size'),'class="inputbox"','class="row"','',$this->l("You can create a new size via Menu <b>Preferences/Image</b>."));                
        ?>                                                 
        </ul></div>    
  </fieldset>
  <fieldset>
        <legend><img src="../img/admin/contact.gif" /><?php echo $this->l('Generate PDF from Category'); ?></legend>
        <div class="lof_config_wrrapper clearfix">
            <?php 
                $list = $this->_params->pdfCateg();                          				
            ?>
            <div style="margin: 10px;">
                <input type="submit" <?php if($this->getParamValue("module_theme",'')=='') echo 'disabled="disabled"';?> onclick="return lofGeneratePdf();" class="button lofPdf" value="<?php echo $this->l("Generate");?>" name="generate"/>
				<b style="color:red;<?php if($this->getParamValue("module_theme",'')!='') echo 'display:none;';?>">(<?php echo $this->l("Please update module config first!");?>)</b>
				<br />
                <label style="font-weight: normal;font-size: 10px;"><?php echo $this->l("Please select one or more category then click on Generate button");?></label>
            </div>
            <table cellspacing="0" cellpadding="0" class="table">
    			<tbody><tr>
    				<th><input type="checkbox" onclick="checkDelBoxes(this.form, 'categoryBox[]', this.checked)" class="noborder" name="checkme"></th>
    				<th><?php echo $this->l("ID");?></th>
    				<th style="width: 100%"><?php echo $this->l("Name");?></th>
    			</tr>
                <?php 
                    foreach($list as $cat){
                        if($cat["id_category"] == 1):?>
                            <tr class="">
                                <td>
                    				
                    			</td>
                                <td>
                    				<?php echo $cat["id_category"];?>
                    			</td>
                                <td><img alt="" src="../img/admin/lv1.gif"/> &nbsp;
		                        <label class="t" for="categoryBox_1"><?php echo $cat["name"]; ?></label></td>
                            </tr>   
                <?php   else:?>
                            <tr class="alt_row" id="lofcat-<?php echo $cat["id_category"];?>">
                    			<td>
                    				<input type="checkbox" value="<?php echo $cat["id_category"];?>" id="categoryBox_<?php echo $cat["id_category"];?>" class="categoryBox lof-checkpdf id_category_default" name="categoryBox[]">
                    			</td>
                    			<td>
                    				<?php echo $cat["id_category"];?>
                    			</td>
                    			<td><?php
                                    if($cat["level_depth"] > 1){
                                        echo '<img alt="" src="../img/admin/lvl_1.gif">';
                                    }
                                    for($i = 2; $i< $cat["level_depth"]; $i++){
                                        echo '<img alt="" src="../img/admin/lvl_0.gif">';
                                    }
                                ?><img alt="" src="../img/admin/lv2_b.gif"> &nbsp;
                    			<label class="t" for="categoryBox_<?php echo $cat["id_category"];?>"><?php echo $cat["name"];?></label>                               
                                
                                    <?php foreach($languages as $lan):?>
                                        <?php 
                                            $style = '';                                            
                                            if(!is_file(_PS_ROOT_DIR_."/modules/".$this->name."/libs/tcpdf/files/".$lan["id_lang"]."/".$cat["id_category"].".pdf")){
                                                $style = 'style="display:none;"';
                                            }
                                        ?>
                                        <div class="lof-dowpdf lof-dowpdf<?php echo $lan["id_lang"];?>" <?php echo $style;?>>
                                            | <a target="_blank" href="../modules/lofcatalog/libs/tcpdf/files/<?php echo $lan["id_lang"];?>/<?php echo $cat["id_category"];?>.pdf" title="<?php echo $this->l("Click to download") ?>"><?php echo $lan["name"];?></a>
                                        </div> &nbsp;
                                        <div class="lof-loading lof-loading<?php echo $lan["id_lang"];?>">
                                            | <img alt="" src="../modules/lofcatalog/assets/admin/images/loading.gif"/>
                                        </div>    
                                    <?php endforeach;?>                                                                                                    
                                </td>
                    		</tr>    
                <?php   endif;                
                    }
                ?>				
            </tbody></table>
        
        </div>    
  </fieldset>     
  
  
  <fieldset class="disPdf-1">
    <legend><img src="../img/admin/contact.gif" /><?php echo $this->l('PDF Setting'); ?></legend>
        <div class="lof_config_wrrapper clearfix">
            <ul>
            <li class="row">
                <ul class="row">
                    <li class="lof-config-left">
                        <label><?php echo $this->l("Upload your backgroup of pdf file")?></label>
                    </li>
                    <li class="lof-config-right">
                        <input type="file" name="lofbackgroup" id="lofbackgroup"/>
                         <div style="width:100px;margin: 5px;">
                            <?php 
                                $imgFolder = _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/tcpdf/images';
                                $imgFolder = str_replace(_PS_ROOT_DIR_,"",$imgFolder);                                                    
                                $imageLink = __PS_BASE_URI__.$imgFolder."/".$this->getParamValue("bgfile","bg.JPG");
                                $imageLink = str_replace("//","/",$imageLink);
                            ?>
                            <img src="<?php echo $imageLink;?>" alt="" style="width:100%"/>
                        </div>
                    </li>
                </ul>
            </li>
            <?php
            echo $this->_params->inputTag("pdftextCo",$this->getParamValue("pdftextCo","000000"),$this->l('PDF Text Color'),'size="5" class="Multiple"','class="row"','');
            echo $this->_params->inputTag("catNameCo",$this->getParamValue("catNameCo","ffffff"),$this->l('Category Name Color'),'size="5" class="Multiple"','class="row"','');
            echo $this->_params->inputTag("proNameCo",$this->getParamValue("proNameCo","b22222"),$this->l('Product Color'),'size="5" class="Multiple"','class="row"','');
            echo $this->_params->inputTag("proPriceCo",$this->getParamValue("proPriceCo","ff0000"),$this->l('Price Color'),'size="5" class="Multiple"','class="row"','');    
            echo $this->_params->inputTag("proQuanCo",$this->getParamValue("proQuanCo","800000"),$this->l('Quantity Color'),'size="5" class="Multiple"','class="row"','');
			echo $this->_params->inputTag("pdf_ma_top",$this->getParamValue("pdf_ma_top","27"),$this->l('Margin Top (mm)'),'size="3"','class="row"','');
			echo $this->_params->inputTag("pdf_ma_rig",$this->getParamValue("pdf_ma_rig","15"),$this->l('Margin Right (mm)'),'size="3"','class="row"','');                
			echo $this->_params->inputTag("pdf_ma_bot",$this->getParamValue("pdf_ma_bot","25"),$this->l('Margin Bottom (mm)'),'size="3"','class="row"','');
			echo $this->_params->inputTag("pdf_ma_lef",$this->getParamValue("pdf_ma_lef","15"),$this->l('Margin Left (mm)'),'size="3"','class="row"','');
			$pageFormat = array(
								"4A0"=>$this->l("4A0"),"2A0"=>$this->l("2A0"),"A0"=>$this->l("A0"),"A1"=>$this->l("A1"),"A2"=>$this->l("A2"),"A4"=>$this->l("A4"),
								"A5"=>$this->l("A5"),"A6"=>$this->l("A6"),"A7"=>$this->l("A7"),"A8"=>$this->l("A8"),"A9"=>$this->l("A9"),"A10"=>$this->l("A10"),
								"B0"=>$this->l("B0"),"B1"=>$this->l("B1"),"B2"=>$this->l("B2"),"B3"=>$this->l("B3"),"B4"=>$this->l("B4"),"B5"=>$this->l("B5"),
								"B6"=>$this->l("B6"),"B7"=>$this->l("B7"),"B8"=>$this->l("B8"),"B9"=>$this->l("B9"),"B10"=>$this->l("B10"),
								"C0"=>$this->l("C0"),"C1"=>$this->l("C1"),"C2"=>$this->l("C2"),"C3"=>$this->l("C3"),"C4"=>$this->l("C4"),"C5"=>$this->l("C5"),
								"C6"=>$this->l("C6"),"C7"=>$this->l("C7"),"C8"=>$this->l("C8"),"C9"=>$this->l("C9"),"C10"=>$this->l("C10"),
								"RA0"=>$this->l("RA0"),"RA1"=>$this->l("RA1"),"RA2"=>$this->l("RA2"),"RA3"=>$this->l("RA3"),"RA4"=>$this->l("RA4"),
								"SRA0"=>$this->l("SRA0"),"SRA1"=>$this->l("SRA1"),"SRA2"=>$this->l("SRA2"),"SRA3"=>$this->l("SRA3"),"SRA4"=>$this->l("SRA4"),
								"LETTER"=>$this->l("LETTER"),"LEGAL"=>$this->l("LEGAL"),"EXECUTIVE"=>$this->l("EXECUTIVE"),"FOLIO"=>$this->l("FOLIO")
								);
			echo $this->_params->selectTag("page_format",$pageFormat,$this->getParamValue("page_format","A4"),$this->l('Page Format'),"inputbox",'class="row"','');
			$page_orien = array("P"=>$this->l("Portrait"),"L"=>$this->l("Landscape"));
			echo $this->_params->selectTag("page_orien",$page_orien,$this->getParamValue("page_orien","p"),$this->l('Page Orientation'),"inputbox",'class="row"','');                           
			echo $this->_params->inputTag("cell_hei",$this->getParamValue("cell_hei","1.2"),$this->l('Site Cell Height Ratio'),'size="3"','class="row"','');
                			
			echo $this->_params->selectTag("pdfDes",$pdfDes,$this->getParamValue("pdfDes","1"),$this->l('PDF Destination'),"inputbox",'class="row"','');
			echo $this->_params->selectTag("pdfCache",$yesNoLang,$this->getParamValue("pdfCache","0"),$this->l('Cache'),"inputbox",'class="row"','');                                            				
			echo $this->_params->inputTag("pdfImgSca",$this->getParamValue("pdfImgSca","4"),$this->l('Image Scale'),'size="3"','class="row"','');                
            //echo $this->_params->inputTag("fo_foColor",$this->getParamValue("fo_foColor","000000"),$this->l('Footer Font Color'),'size="5" class="Multiple"','class="row"','');
            //echo $this->_params->inputTag("fo_lineColor",$this->getParamValue("fo_lineColor","000000"),$this->l('Footer Line Color'),'size="5" class="Multiple"','class="row"','');
            ?>
            </ul>
        </div>
                  
  </fieldset>    
<br />
  <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
  	<fieldset><legend><img src="../img/admin/comment.gif" alt="" title="" /><?php echo $this->l('Information');?></legend>    	
    	<ul>
    	     <li>+ <a target="_blank" href="http://landofcoder.com/others/lof-catalog.html"><?php echo $this->l('Detail Information');?></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/supports/forum.html?id=76"><?php echo $this->l('Forum support');?></a></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/submit-request.html"><?php echo $this->l('Customization/Technical Support Via Email');?>.</a></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/prestashop-guide-and-docs/prestashop-14x-lof-catalog-module.html"><?php echo $this->l('UserGuide ');?></a></li>
        </ul>
        <br />
        @copyright: <a href="http://landofcoder.com">LandOfCoder.com</a>
    </fieldset>
</form>