<?php
require_once(dirname(__FILE__).'/tcpdf/tcpdf.php');
class lofTcpdf extends TCPDF
{
    protected $pluginP = null;
	protected $bgImg   = null;
    
	private function getPluginParameters() {		
		return $this->pluginP;
	}
    
    public function setPluginParameters($pluginP) {
        $this->pluginP = $pluginP;
    }
    
    private function getBGImg() {		
		return $this->bgImg;
	}
    
    public function setBGImg($bgImg) {
        $this->bgImg = $bgImg;
    }
    
    public function Header() {
		// get the current page break margin
		$bMargin = $this->getBreakMargin();
		// get current auto-page-break mode
		$auto_page_break = $this->AutoPageBreak;
		// disable auto-page-break
		$this->SetAutoPageBreak(false, 0);
		// set bacground image
		$img_file = K_PATH_IMAGES.$this->getBGImg();
		if(file_exists($img_file)){
			$this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
		}
		// restore auto-page-break status
		$this->SetAutoPageBreak($auto_page_break, $bMargin);
		// set the starting point for the page content
		$this->setPageMark();
	}
    
    public function Footer() {
			$footerfont = $this->getFooterFont();	
			$pluginP	= $this->getPluginParameters();
			// Params
			$params								= array();
			$params['footer_display_line']		= $pluginP->get('fo_dis_line', 1);
			$params['footer_font_color']		= ($pluginP->get('fo_foColor', 'ffffff'))?"#".$pluginP->get('fo_foColor', 'ffffff'):"";                       
			$params['footer_line_color']		= ($pluginP->get('fo_lineColor', 'ffffff'))?"#".$pluginP->get('fo_lineColor', 'ffffff'):"";
			$params['footer_bg_color']			= ($pluginP->get('fo_bgColor', ''))?"#".$pluginP->get('fo_bgColor', ''):"";           
			$params['footer_display']			= $pluginP->get('fo_display', 1);
			$params['footer_data']				= $pluginP->get('footer', '');
			$params['footer_display_pagination']= $pluginP->get('fo_dis_pa', 1);
			$params['footer_data_align']		= $pluginP->get('fo_align', 'R');
			$params['footer_margin']			= $pluginP->get('fo_mar', 15);
			$params['footer_cell_height']		= $pluginP->get('fo_cell', 1.2);

			//Extra values
			if ((int)$params['footer_cell_height'] > 3) {
				$params['footer_cell_height'] = 3;
			}
			
			if ((int)$params['footer_margin'] > 50) {
				$params['footer_margin'] = 50;
			}
		
			$currentCHRF = $this->getCellHeightRatio();
			$this->setCellHeightRatio($params['footer_cell_height']);
		
			$isHTML = false;
			if ($params['footer_data'] != '') {
				$isHTML = true;
			}
	
			if ($params['footer_display'] == 1) {
				$cur_y = $this->GetY();
				$ormargins = $this->getOriginalMargins();
				$this->SetTextColor(0, 0, 0);			
				//set style for cell border
				$border = 0;
				if ((int)$params['footer_display_line'] == 1) {
					$line_width = 0.85 / $this->getScaleFactor();
					$this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
					$border = 'T';
				}
				
				//print document barcode
				$barcode = $this->getBarcode();
				if (!empty($barcode)) {
					$this->Ln($line_width);
					$barcode_width = round(($this->getPageWidth() - $ormargins['left'] - $ormargins['right'])/3);
					$this->write1DBarcode($barcode, 'C128B', $this->GetX(), $cur_y + $line_width, $barcode_width, (($this->getFooterMargin() / 3) - $line_width), 0.3, '', '');	
				}
				if (empty($this->pagegroups)) {
					$pagenumtxt = $this->l['w_page'].' '.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
				} else {
					$pagenumtxt = $this->l['w_page'].' '.$this->getPageNumGroupAlias().' / '.$this->getPageGroupAlias();
				}		
				$this->SetY($cur_y);
				
				/*
				 * Specific Plugin code for Footer
				 * Header is set here in system plugin (Phoca PDF Content Plugin) because we need title and header data
				 * Footer is set in helper of system plugin (Phoca PDF Component) because we need TCPDF data (pagination)
				 */
				// Pagination
				if ($params['footer_display_pagination'] == 0) {
					$pagenumtxt = '';
				}
				// Footer Data
				if ($params['footer_data'] != '') {
					$footertxt = str_replace('{phocapdfpagination}', $pagenumtxt, $params['footer_data']);
				
				} else {
					$footertxt = $pagenumtxt;
				}
				
				$cell_height = round(($this->getCellHeightRatio() * $footerfont[2]) / $this->getScaleFactor(), 2);
				
				$this->SetFont($footerfont[0], $footerfont[1], $footerfont[2]);
				if ($this->getRTL()) {
					$this->SetX($ormargins['right']);
				} else {
					$this->SetX($ormargins['left']);
				}
			
				$fill = 0;                
				if ($params['footer_bg_color'] != '') {
					$fillColor = $this->convertHTMLColorToDec($params['footer_bg_color']);
					$this->SetFillColorArray(array($fillColor['R'],$fillColor['G'],$fillColor['B']));
					$fill = 1;
					
				}
			
				$this->MultiCell(0, $cell_height, $footertxt, $border, $params['footer_data_align'], $fill, 1, '', '', true, 0, $isHTML);
				
			}
			
			$posY	= $this->getPageHeight() -10;			
			
			// Set it back
			$this->setCellHeightRatio($currentCHRF);
			
	}
            	    	
}

class lofPDF
{
	var $_engine	= null;
    var $_params	= null;	
    var $_module_name  = "lofcatalog";
	var $_currentDate = "";
    var $_fileName    = "";
    
	/**
	 * Class constructore
	 *
	 * @access protected
	 * @param	array	$options Associative array of options
	 */
	function __construct($options = array())
	{		   
	   global $cookie;
        $this->_params = $options;
        if (!defined('K_TCPDF_EXTERNAL_CONFIG')) {            
    		define('K_TCPDF_EXTERNAL_CONFIG', true);        		
    		// Installation path
    		define("K_PATH_MAIN", _PS_ROOT_DIR_."/modules/".$this->_module_name."/libs/tcpdf");    
    		// URL path
    		define("K_PATH_URL", _PS_ROOT_DIR_);    
    		// Fonts path
    		define("K_PATH_FONTS", _PS_ROOT_DIR_."/modules/".$this->_module_name."/libs/pdf_fonts/");    
    		// Cache directory path
    		define("K_PATH_CACHE", K_PATH_MAIN."/cache/");    
    		// Cache URL path
    		define("K_PATH_URL_CACHE", K_PATH_URL."/cache/");    
    		// Images path
    		define("K_PATH_IMAGES", K_PATH_MAIN."/images/");    
    		// Blank image path
    		define("K_BLANK_IMAGE", K_PATH_IMAGES."/_blank.png");
            
            //header
                       
            define ('PDF_PAGE_FORMAT', $this->_params->get("page_format","A4"));
            define ('PDF_PAGE_ORIENTATION', $this->_params->get("page_orien","P"));
            define ('PDF_CREATOR', 'TCPDF');
            define ('PDF_AUTHOR', 'TCPDF');
            
            define ('PDF_UNIT', 'mm');
            
            define ('PDF_MARGIN_TOP', $this->_params->get("pdf_ma_top","27"));            
            define ('PDF_MARGIN_BOTTOM', $this->_params->get("pdf_ma_bot","25"));
            define ('PDF_MARGIN_RIGHT', $this->_params->get("pdf_ma_rig","15"));
            define ('PDF_MARGIN_LEFT', $this->_params->get("pdf_ma_lef","15"));
            define ('PDF_FONT_NAME_MAIN', $this->_params->get("font_type","helvetica"));
            define ('PDF_FONT_SIZE_MAIN', $this->_params->get("font_size","10"));
            define ('PDF_FONT_NAME_DATA', 'helvetica');
            define ('PDF_FONT_SIZE_DATA', 8);
            define ('PDF_FONT_MONOSPACED', 'courier');
            define ('PDF_IMAGE_SCALE_RATIO', $this->_params->get("pdfImgSca","4"));
            define('K_CELL_HEIGHT_RATIO', $this->_params->get("cell_hei","1.2"));
            define('K_TITLE_MAGNIFICATION', 1.3);
        	/**
        	 * reduction factor for small font
        	 */
        	define('K_SMALL_RATIO', 2/3);
        
        	/**
        	 * set to true to enable the special procedure used to avoid the overlappind of symbols on Thai language
        	 */
        	define('K_THAI_TOPCHARS', true);
        
        	/**
        	 * if true allows to call TCPDF methods using HTML syntax
        	 * IMPORTANT: For security reason, disable this feature if you are printing user HTML content.
        	 */
        	define('K_TCPDF_CALLS_IN_HTML', true);    		
        }              		                                
        $id_lang = intval($cookie->id_lang);  
        $language = Language::getLanguage($id_lang);
        $name = strtolower(substr($language["name"],0,3));        
        if(is_file(K_PATH_MAIN."/config/lang/".$name.".php")){
           require_once(K_PATH_MAIN."/config/lang//".$name.".php"); 
        }else{
           require_once(K_PATH_MAIN."/config/lang/eng.php"); 
        }
        
       
		// Default settings are a portrait layout with an A4 configuration using millimeters as units
		$this->_engine = new lofTcpdf(PDF_PAGE_ORIENTATION, PDF_UNIT , PDF_PAGE_FORMAT, true, 'UTF-8', $this->_params->get("pdfCache",false));              
        $this->_engine->setCellHeightRatio(K_CELL_HEIGHT_RATIO);        
        $this->_engine->setPluginParameters($this->_params);
        $this->_engine->setCellHeightRatio(K_CELL_HEIGHT_RATIO);
                                
		//set auto page breaks
		$this->_engine->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $this->_engine->SetMargins(PDF_MARGIN_LEFT,PDF_MARGIN_TOP,PDF_MARGIN_RIGHT,PDF_MARGIN_BOTTOM);		
		$this->_engine->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $this->_engine->setLanguageArray($l);               
	}
    
    function setContent($content){        
        $this->_content = $content;
    }
    
    function getContent(){
       return $this->_content;
    }
	
    function setFileName(){
        $this->_content = $content;
    }
    
    function getFileName(){
        $this->_content = $content;
    }
	
	function returnRealPath($path){
		if(version_compare(_PS_VERSION_, '1.4.3', '<')) return $path;				
		$tpath      = str_replace( _PS_BASE_URL_, "", $path );
		if(__PS_BASE_URI__ != '\\' && __PS_BASE_URI__ != '/'){
			$tpath      = str_replace( __PS_BASE_URI__, "", $tpath );
		}
		$sourceFile = _PS_ROOT_DIR_.'/'.$tpath;
		if(is_file($sourceFile)){
			return $sourceFile;
		}
		return false;
	}
	/**
	 * Render the document.
	 *
	 * @access public
	 * @param boolean 	$cache		If true, cache the output
	 * @param array		$params		Associative array of attributes
	 * @return 	The rendered data
	 */
	function render( $cache = false)
	{
	    global $link;
		$pdf = &$this->_engine;
        $params = $this->_params;
        $shop_name = htmlentities(Configuration::get('PS_SHOP_NAME'), NULL, 'utf-8');        
		// Set PDF Metadata
		$pdf->SetCreator($shop_name);
		$pdf->SetTitle($this->_params->get("pdfTitle","Quotes"));
        $pdf->SetAuthor($shop_name);
		$pdf->SetSubject($shop_name);
		$pdf->SetKeywords('prestashop','catalog','price','sheet');
                
		// Set PDF Header data
        $pdf->setBGImg($params->get("bgfile","bg.JPG"));        				         
        $pdf->setHeaderMargin(1);                             
        $pdf->setFooterFont(array($this->_params->get("fo_foType","helvetica"), '', $this->_params->get("fo_foSize","10")));
        $pdf->setFooterMargin($this->_params->get("fo_mar","10"));      
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);						        
        //$pdf->SetFont(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN, '', true);
        
 
		// add a page
        $pdf->AddPage();                                     
        $result = $this->getContent();
                      
        //include html theme              
        $themeDir = _PS_ROOT_DIR_.'/modules/lofcatalog/tmpl/';
        if(file_exists($themeDir.'/'.$params->get("module_theme","default").'/pdf.php')){
            include_once($themeDir.'/'.$params->get("module_theme","default").'/pdf.php'); 
        }else{
            include_once($themeDir.'/default/pdf.php');     
        }                    
        
        $pdf->lastPage();        
        //Close and output PDF document 
        if(!is_dir(K_PATH_MAIN."/"."files")){
            mkdir(K_PATH_MAIN."/"."files", 0777);            
        }        
        $lang_id = $this->_params->get("lang_id");
        if(!is_dir(K_PATH_MAIN."/"."files"."/".$lang_id)){
            mkdir(K_PATH_MAIN."/"."files"."/".$lang_id, 0777);            
        }        
        $cat_id  = $this->_params->get("cat_id");                
        $pdf->Output(K_PATH_MAIN."/files/".$lang_id."/".$cat_id.'.pdf',"F");        
	}
}
?>