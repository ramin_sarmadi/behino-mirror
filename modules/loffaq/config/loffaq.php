<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
?>
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.css";?>" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/jquery-ui.css";?>" type="text/css" media="screen" charset="utf-8" />


<script type="text/javascript" src="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.js";?>"></script>
<script type="text/javascript">
$(document).ready(function() {   	            
    //When page loads...
    $(".tab_content").hide(); //Hide all content
    $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content
    
    //On Click Event
    $("ul.tabs li").click(function() {
    
    	$("ul.tabs li").removeClass("active"); //Remove any "active" class
    	$(this).addClass("active"); //Add "active" class to selected tab
    	$(".tab_content").hide(); //Hide all tab content
    
    	var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content        
    	$(activeTab).fadeIn(); //Fade in the active ID content
    	return false;
    });    

  });
</script>
<?php  
    $yesNoLang = array("0"=>$this->l('No'),"1"=>$this->l('Yes'));  
    $themes = $this->getFolderList(_PS_MODULE_DIR_.$this->name."/tmpl/" );    
?>
<h3><?php echo $this->l('Lof FAQ Configuration');?></h3>
<form action="<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>" enctype="multipart/form-data" method="post" id="lofform">
<input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
	<fieldset>
		<legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Global Setting'); ?></legend>
		<div class="lof_config_wrrapper clearfix">
		  <ul>
			<li>
			<?php
				echo $this->_params->selectTag("module_theme",$themes,$this->getParamValue("module_theme","default"),$this->l('Theme - Layout'),'class="inputbox select-group"','class="row"','' );
				echo $this->_params->radioBooleanTag("enable_pdetail", $yesNoLang,$this->getParamValue("enable_pdetail",1),$this->l('Enable Faq in Product.'),'class="select-option"','class="row"','',$this->l(''));
				
				echo $this->_params->radioBooleanTag("enable_bCat", $yesNoLang,$this->getParamValue("enable_bCat",1),$this->l('Enable FAQ\'s Category Block.'),'class="select-option"','class="row"','',$this->l(''));
				echo $this->_params->radioBooleanTag("enable_bRen", $yesNoLang,$this->getParamValue("enable_bRen",1),$this->l('Enable FAQ Rencent Block.'),'class="select-option"','class="row"','',$this->l(''));
				
				echo $this->_params->radioBooleanTag("premoderation", $yesNoLang,$this->getParamValue("premoderation",1),$this->l('Approve by admin.'),'class="select-option"','class="row"','',$this->l('If Yes, the new questions will not displayed unless they are approved by admin'));
				echo $this->_params->radioBooleanTag("allow_guest", $yesNoLang,$this->getParamValue("allow_guest",1),$this->l('Guest can post questions.'),'class="select-option"','class="row"','',$this->l('Allow Non-users can post questions'));
                echo $this->_params->radioBooleanTag("captcha", $yesNoLang,$this->getParamValue("captcha",1),$this->l('Enable captcha'),'class="select-option"','class="row"','','');
				echo $this->_params->radioBooleanTag("is_private", $yesNoLang,$this->getParamValue("is_private",1),$this->l('Enable private FAQ.'),'class="select-option"','class="row"','',$this->l('Enable private FAQ checking box when submit a question'));
				echo $this->_params->radioBooleanTag("notification_email", $yesNoLang,$this->getParamValue("notification_email",0),$this->l('Send notification e-mail to admin.'),'class="select-option"','class="row"','',$this->l('(Send email to admin when a customer submit a question)'));
				echo $this->_params->inputTag("email_admin",$this->getParamValue("email_admin", Configuration::get("PS_SHOP_EMAIL")),$this->l('Admin\'s e-mail'),'class="text_area"','class="row"','');
                
			?> 
			</li>    	                   	        
		  </ul>
		</div>
   </fieldset>  
<br /> 
<fieldset>
		<legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Display Setting'); ?></legend>
		<div class="lof_config_wrrapper clearfix">
		  <ul>
			<li>
			<?php
				
                echo $this->_params->inputTag("limit_category",$this->getParamValue("limit_category", "5"),$this->l('Limit FAQ\'s Category.'),'class="text_area"','class="row"','');
                echo $this->_params->inputTag("limit_question",$this->getParamValue("limit_question", "5"),$this->l('Limit questions in one page.'),'class="text_area"','class="row"','');					
				
				echo $this->_params->radioBooleanTag("enable_gravatar", $yesNoLang,$this->getParamValue("enable_gravatar",0),$this->l('Enable Gravatar.'),'class="select-option"','class="row"','','');
				echo $this->_params->radioBooleanTag("show_username", $yesNoLang,$this->getParamValue("show_username",0),$this->l('Show UserName.'),'class="select-option"','class="row"','','');
				echo $this->_params->radioBooleanTag("show_createdate", $yesNoLang,$this->getParamValue("show_createdate",0),$this->l('Show Created Date.'),'class="select-option"','class="row"','','');
				echo $this->_params->radioBooleanTag("show_helpfulness", $yesNoLang,$this->getParamValue("show_helpfulness",0),$this->l('Show Helpfulness.'),'class="select-option"','class="row"','',$this->l('Show rating up/down '));
				echo $this->_params->radioBooleanTag("enable_rating", $yesNoLang,$this->getParamValue("enable_rating",0),$this->l('Enable Rating.'),'class="select-option"','class="row"','','');
				echo $this->_params->radioBooleanTag("show_catimage", $yesNoLang,$this->getParamValue("show_catimage",1),$this->l('Show category image.'),'class="select-option"','class="row"','','');
				echo $this->_params->inputTag("width",$this->getParamValue("width", "80"),$this->l('Category image width'),'class="text_area"','class="row"','');
				echo $this->_params->inputTag("height",$this->getParamValue("height", "80"),$this->l('Category image height'),'class="text_area"','class="row"','');
			?>
			</li>
		</ul>
</fieldset>
<br/> 
<fieldset>
		<legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Recent FAQ Setting'); ?></legend>
		<div class="lof_config_wrrapper clearfix">
		  <ul>
			<li>
			<?php
				
				echo $this->_params->inputTag("recent_title",$this->getParamValue("recent_title", "Rencent FAQ"),$this->l('Rencent FAQ title'),'class="text_area"','class="row"','');
                echo $this->_params->inputTag("recent_limit",$this->getParamValue("recent_limit", "5"),$this->l('Limit Recent FAQ'),'class="text_area"','class="row"','');
			?>
			</li>
		</ul>
</fieldset>
<br/>
   <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
<br />  
  	<fieldset><legend class="lof-legend"><img src="../img/admin/comment.gif" alt="" title="" /><?php echo $this->l('Information');?></legend>    	
    	<ul>
    	     
             <li>+ <a target="_blank" href="http://landofcoder.com/supports/forum.html?id=100"><?php echo $this->l('Forum support');?></a></li>
             <li>+ <a target="_blank" href="http://www.landofcoder.com/submit-request.html"><?php echo $this->l('Customization/Technical Support Via Email');?>.</a></li>
             <li>+ Facebook: <a target="_blank" href="http://www.facebook.com/LeoTheme"><?php echo $this->l('Like Us');?></a></li>
			 <li>+ Twitter: <a target="_blank" href="https://twitter.com/#!/leotheme"><?php echo $this->l('Follow Us');?></a></li>
        </ul>
        <br />
        @Copyright: <a href="http://wwww.landofcoder.com">LandOfCoder.com</a>
    </fieldset>
</form>