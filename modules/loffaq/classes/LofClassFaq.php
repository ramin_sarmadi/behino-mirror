<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads,
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com>
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */


if (!defined('_CAN_LOAD_FILES_'))
	exit;
if(!class_exists('LofClassFaq')) {
	/**
	 * Class LofFaq
	 */
	class LofClassFaq extends ObjectModel{
		public $id;
		public $id_loffaq_category;
		public $id_customer;
        public $customer_name;
        public $email;
        public $like;
        public $unlike;
		public $type;
		public $is_private;
		public $active;
		public $date_add;
		public $id_product;
		public $question;
		public $answer;
		public static $definition = array(
			'table' => 'loffaq_faq',
			'primary' => 'id_loffaq_faq',
			'multilang' => true,
			'fields' => array(
				'id_loffaq_category' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
				'id_customer' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
				'customer_name' => 			array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 200),
				'email' => 				array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 200),
				'like' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
				'unlike' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
				'type' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'size' => 4),
				'is_private' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'size' => 4),
				'active' => 			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
				'date_add' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
				'id_product' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),

				'question' => 				array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'required' => true),
				'answer' => 				array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'required' => false),
			),
		);
		
			
		
		public function __construct($id = NULL, $id_lang = NULL){
			parent::__construct($id, $id_lang);
		}
		
		public function add($autodate = true, $null_values = false)
		{
			//echo "<pre>".print_r($_POST,1);die;
			$context = Context::getContext();
			$id_shop = $context->shop->id;
			$res = parent::add($autodate, $null_values);
			$res &= Db::getInstance()->execute('
				INSERT INTO `'._DB_PREFIX_.'loffaq_faq_shop` (`id_shop`, `id_loffaq_faq`)
				VALUES('.(int)$id_shop.', '.(int)$this->id.')'
			);
			return $res;
		}
		
		public function delete()
		{
			$res = true;
			$res &= Db::getInstance()->execute('
				DELETE FROM `'._DB_PREFIX_.'loffaq_faq_shop`
				WHERE `id_loffaq_faq` = '.(int)$this->id
			);
			$res &= parent::delete();
			return $res;
		}
		
        public static function getFaqsByCategory($id_faq_category = NULL, $text = NULL, $id_product = 0){
			
			$context = Context::getContext();
			$id_shop = $context->shop->id;
            global $cookie;
            if($id_product == 0) $sql_idproduct = '';
            else $sql_idproduct = ' AND f.id_product = '.intval($id_product);
            if(!$id_faq_category) $id_faq_category = 0;
            if(!$text) $text = '';
            $search = ' AND hsss.question LIKE \'%'.$text.'%\'';
            $sql = '
                SELECT f.*,hsss.* 
            	   FROM '._DB_PREFIX_.'loffaq_faq AS f
				   INNER JOIN `'._DB_PREFIX_.'loffaq_faq_shop` hss ON (f.id_loffaq_faq = hss.id_loffaq_faq AND hss.`id_shop` = '.(int)$id_shop.')
				   INNER JOIN '._DB_PREFIX_.'loffaq_faq_lang AS hsss ON f.id_loffaq_faq = hsss.id_loffaq_faq 
				   LEFT JOIN '._DB_PREFIX_.'loffaq_category AS c ON f.id_loffaq_category = c.id_loffaq_category 
            	   WHERE f.active = 1   AND  c.active = 1 AND f.id_loffaq_category = '.intval($id_faq_category).''.$sql_idproduct.''.$search.' AND hsss.`id_lang` = '.(int)$context->cart->id_lang.'';
            $result = Db::getInstance()->ExecuteS($sql);
            foreach($result as $k=>$v){
                $result[$k]['grav_url'] = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $result[$k]['email'] ) ) ) . "&s=20"; 
                if($result[$k]['is_private'] == '1'){
                    if($result[$k]['id_customer'] == $cookie->id_customer){
                        
                    }else{
                        unset($result[$k]);
                    }
                }
            }
            return $result;
        }
        
        public static function getListFAQ($id_lang = NULL,$txtSearch = NULL, $id_category = 0, $id_product = 0){
            $faqcategories = LofFaqCategory::getFaqCategory($id_lang, 1, $id_category);
            foreach($faqcategories as $k=>$v){
                $faqcategories[$k]['faq'] = self::getFaqsByCategory($faqcategories[$k]['id_loffaq_category'], $txtSearch, $id_product);
            }
            return $faqcategories;
        }
        
        public static function countFAQ($faqcategories){
            $count = 0;
            foreach($faqcategories as $k=>$v){
                $count += sizeof($faqcategories[$k]['faq']);
            }
            return $count;
        }
        
        public static function recentFaq(){
			$context = Context::getContext();
			$id_shop = $context->shop->id;
            $sql = '
                SELECT hs.*, hsl.question, hsl.answer 
            	   FROM '._DB_PREFIX_.'loffaq_faq hs
            	   LEFT JOIN '._DB_PREFIX_.'loffaq_faq_lang hsl ON (hsl.id_loffaq_faq = hs.id_loffaq_faq AND hsl.`id_lang` ='.(int)($context->language->id).'	)
				   LEFT JOIN '._DB_PREFIX_.'loffaq_faq_shop hss ON (hs.id_loffaq_faq 	 = hss.id_loffaq_faq 	)
            	   WHERE hs.active = 1 AND hs.is_private = 0 AND hss.`id_shop` = '.(int)$id_shop.' 
            	   ORDER BY date_add DESC
            ';
            return Db::getInstance()->ExecuteS($sql);
        }
        
        public static function updateLikeCount($id_faq = NULL){
            if(!empty($id_faq)){
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'loffaq_faq SET `like` = `like` + 1 WHERE id_loffaq_faq = '.intval($id_faq));    
            }
            return false;
        }
    
        public static function updateUnLikeCount($id_faq = NULL){
            if(!empty($id_faq)){
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'loffaq_faq SET `unlike` = `unlike` + 1 WHERE id_loffaq_faq = '.intval($id_faq));    
            }
            return false;
        }
        
        public static function updateRatingCount($id_faq = NULL){
            if(!empty($id_faq)){
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'loffaq_faq SET `rating_count` = `rating_count` + 1 WHERE id_loffaq_faq = '.intval($id_faq));
            }
            return false;
        }
        
        public static function updateRatingSum($id_faq = NULL,$value = 0){
            if(!empty($id_faq)){
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'loffaq_faq SET `rating_sum` = `rating_sum` + '.intval($value).' WHERE id_loffaq_faq = '.intval($id_faq));
            }
            return false;
        }
	}
}
?>