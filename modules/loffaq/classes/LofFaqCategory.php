<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads,
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com>
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */


if (!defined('_CAN_LOAD_FILES_'))
	exit;
if(!class_exists('LofFaqCategory')) {
	/**
	 * Class LofFaqCategory
	 */
	class LofFaqCategory extends ObjectModel {
		public $id;
		public $image;
		public $name;
		public $active;
		public $position;
		//public $ordering = 0;
		public $description;
		
		public static $definition = array(
			'table' => 'loffaq_category',
			'primary' => 'id_loffaq_category',
			'multilang' => true,
			'fields' => array(
				'image' => 				array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
				'active' => 			array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
				'position' => 			array('type' => self::TYPE_INT),
				//'ordering' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),

				// Lang fields
				'name' => 				array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 200),
				'description' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 250)
			),
		);
	
		public function __construct($id = NULL, $id_lang = NULL){
			$this->image_dir = _PS_IMG_DIR_.'faq/';
			parent::__construct($id, $id_lang);
		}
		public function add($autodate = true, $null_values = false)
		{
			$context = Context::getContext();
			$id_shop = $context->shop->id;
			$res = parent::add($autodate, $null_values);
				$position = $this->getLastPosition($id_shop);
				if (!$position)
					$position = 1;
				$this->addPosition($position, $id_shop);
			return $res;

		}

		public function delete() {
			$res = true;
			$res &= Db::getInstance()->execute('
					DELETE FROM `'._DB_PREFIX_.'loffaq_category_shop`
					WHERE `id_loffaq_category` = '.(int)$this->id
			);
			$faqs = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT `id_loffaq_faq` 
					FROM `'._DB_PREFIX_.'loffaq_faq`
					WHERE `id_loffaq_category` = '.(int)$this->id);
			foreach($faqs as $faq){
				$objFaqs = new LofClassFaq($faq['id_loffaq_faq']);
				$res &= $objFaqs->delete();
			}
			$res &= parent::delete();
			return $res;
		}

		public function getName($id_lang = NULL) {
			if (!$id_lang){
				global $cookie;
				if (isset($this->name[$cookie->id_lang]))
					$id_lang = $cookie->id_lang;
				else
					$id_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
			}
			return isset($this->name[$id_lang]) ? $this->name[$id_lang] : '';
		}

		public static function getFaqCategory($id_lang = NULL,$active = NULL, $id_category = 0){
            if($id_category == 0){
		        $cat_sql = '';     
		    }else{
		        $cat_sql = ' AND cl.id_loffaq_category = '.$id_category;    
		    }
		      
			if($active == 1){
				$active_sql = ' c.active = '.intval($active).' AND ';
			}elseif($active == 0){
				$active_sql = ' c.active = \'0\' AND ';
			}else{
				$active_sql = '  ';
			}

			if(!$id_lang)$id_lang = Configuration::get('PS_LANG_DEFAULT');
			$context = Context::getContext();
			$id_shop = $context->shop->id;
        	$sql ='
            	SELECT c.*,cl.*,hss.*
            	FROM '._DB_PREFIX_.'loffaq_category c
					INNER JOIN `'._DB_PREFIX_.'loffaq_category_shop` hss ON (c.id_loffaq_category = hss.id_loffaq_category AND hss.`id_shop` = '.(int)$id_shop.' )
            		LEFT JOIN '._DB_PREFIX_.'loffaq_category_lang cl ON c.id_loffaq_category = cl.id_loffaq_category
            	WHERE '. $active_sql .' cl.`id_lang` = '.intval($id_lang).$cat_sql.'
				ORDER BY hss.`position` ASC
				';//.$limit_sql 
				//echo "<pre>".print_r(Db::getInstance()->ExecuteS($sql),1);die;
        	return Db::getInstance()->ExecuteS($sql);
		}
        
		public function addPosition($position, $id_shop = null)
		{
			$return = true;
			if (is_null($id_shop))
			{
				if (Shop::getContext() != Shop::CONTEXT_SHOP)
					foreach (Shop::getContextListShopID() as $id_shop)
						$return &= Db::getInstance()->execute('
							INSERT INTO `'._DB_PREFIX_.'loffaq_category_shop` (`id_loffaq_category`, `id_shop`, `position`) VALUES
							('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
							ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
				else
				{
					$id = Context::getContext()->shop->id;
					$id_shop = $id ? $id : Configuration::get('PS_SHOP_DEFAULT');
					$return &= Db::getInstance()->execute('
						INSERT INTO `'._DB_PREFIX_.'loffaq_category_shop` (`id_loffaq_category`, `id_shop`, `position`) VALUES
						('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
						ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
				}
			}
			else
				$return &= Db::getInstance()->execute('
				INSERT INTO `'._DB_PREFIX_.'loffaq_category_shop` (`id_loffaq_category`, `id_shop`, `position`) VALUES
				('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
				ON DUPLICATE KEY UPDATE `position` = '.(int)$position);

			return $return;
		}

		public function getLastPosition($id_shop)
		{
			return (int)(Db::getInstance()->getValue('
			SELECT MAX(`position`)
			FROM `'._DB_PREFIX_.'loffaq_category_shop`
			WHERE `id_shop` = '.(int)$id_shop) + 1);
		}	
		
        public static function getFaqCategoryName($id_lang = NULL, $id_faqcategory = 0){
			$context = Context::getContext();
			$id_shop = $context->shop->id;
            if(!$id_lang)$id_lang = Configuration::get('PS_LANG_DEFAULT');
            $sql = 'SELECT name FROM '._DB_PREFIX_.'loffaq_category_lang c
					INNER JOIN `'._DB_PREFIX_.'loffaq_category_shop` hss ON (c.id_loffaq_category = hss.id_loffaq_category AND hss.`id_shop` = '.(int)$id_shop.')
					WHERE id_loffaq_category = '.$id_faqcategory.' AND id_lang = '. intval($id_lang);
            return Db::getInstance()->getValue($sql);
        }
		
		public static function getFAQCategories($id_lang = NULL){
			$context = Context::getContext();
			$id_shop = $context->shop->id;
			if(!$id_lang)$id_lang = Configuration::get('PS_LANG_DEFAULT');
			$sql = 'SELECT * FROM '._DB_PREFIX_.'loffaq_category_lang c
					INNER JOIN `'._DB_PREFIX_.'loffaq_category_shop` hss ON (c.id_loffaq_category = hss.id_loffaq_category AND hss.`id_shop` = '.(int)$id_shop.')
					WHERE id_lang = '. intval($id_lang);
			return Db::getInstance()->ExecuteS($sql);
		}
		public function updatePosition($way, $position)
			{
				$context = Context::getContext();
				$id_shop = $context->shop->id;
				if (!$res = Db::getInstance()->ExecuteS('
					SELECT cp.`id_loffaq_category`, cp.`position`
					FROM `'._DB_PREFIX_.'loffaq_category_shop` cp
					WHERE cp.`id_shop` = '.(int)$id_shop.'
					ORDER BY cp.`position` ASC'
				))
					return false;
				foreach ($res AS $category)
					if ((int)($category['id_loffaq_category']) == (int)($this->id))
						$movedCategory = $category;

				if (!isset($movedCategory) || !isset($position))
					return false;
				// < and > statements rather than BETWEEN operator
				// since BETWEEN is treated differently according to databases
				$result = (Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'loffaq_category_shop`
					SET `position`= `position` '.($way ? '- 1' : '+ 1').'
					WHERE `position`
					'.($way
						? '> '.(int)($movedCategory['position']).' AND `position` <= '.(int)($position)
						: '< '.(int)($movedCategory['position']).' AND `position` >= '.(int)($position)).'
					')
				AND Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'loffaq_category_shop`
					SET `position` = '.(int)($position).'
					WHERE `id_shop` = '.(int)$id_shop.'
					AND `id_loffaq_category`='.(int)($movedCategory['id_loffaq_category'])));
				return $result;
		}
	}
}