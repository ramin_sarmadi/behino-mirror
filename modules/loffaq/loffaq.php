<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}    
/**
 * lofsliding Class
 */	
 
@session_start();
class loffaq extends Module{
	/** 
	* @variable 
	*/
	private $_params = '';	
	public $_postErrors = array();	
    public $_postSuccess = '';
    const INSTALL_SQL_FILE = 'install.sql';
    const UNINSTALL_SQL_FILE = 'uninstall.sql';	

   /**
    * Constructor 
    */
	function __construct(){
		$this->name = 'loffaq';
		parent::__construct();			
		$this->tab = 'LandOfCoder';				
		$this->version = '1.1';
		$this->displayName = $this->l('Lof FAQ Module');
		$this->description = $this->l('Lof FAQ Module');
		$this->module_key = "";
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' ) && !class_exists("LofParams", false) ){
		  require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' );
		}
        
        if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofClassFaq.php' ) ){
			if( !defined("PTX_LOAD_CLASSES_FAQ") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofClassFaq.php' );
				define("PTX_LOAD_CLASSES_FAQ",true);
			}
		}
        if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofFaqCategory.php' ) ){
			if( !defined("PTX_LOAD_CLASSES_FAQ_CATEGORY") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofFaqCategory.php' );
				define("PTX_LOAD_CLASSES_FAQ_CATEGORY",true);
			}
		}
        		
		$this->_params = new LofParams( $this->name );		   
	}
   /**
    * process installing 
    */
	function install(){
		if(!parent::install()
            OR !$this->registerHook('header')
            OR !$this->registerHook('rightColumn')
            OR !$this->registerHook('productTab') 
            OR !$this->registerHook('productTabContent')
            ) 
            return false;
            
        if(!$this->installTable()) return false;
		if(!$this->installAdminRights()) return false;
		
        $this->initFAQCategory();
		return true;
	}
    /**
    * process uninstalling 
    */
    function uninstall(){
		if(!parent::uninstall())return false;
		if(!$this->uninstallModuleTab('AdminLofManageFAQ')) return false;
        if(!$this->uninstallTable()) return false;
		return true;
	}
	/**
	* Install Module Tab
	*/
	private function installAdminRights() {
		$languages = Language::getLanguages(false);
		foreach ($languages as $language) {
			$arrName[$language['id_lang']] = 'Manage FAQ';
		}
		$idTab = Tab::getIdFromClassName('AdminParentModules');
		$this->installModuleTab('AdminLofManageFAQ', $arrName, $idTab);
		return true;
	}	
	private function installModuleTab($tabClass, $tabName, $idTabParent){
		@copy(_PS_MODULE_DIR_.$this->name.'/logo.png', _PS_IMG_DIR_.'t/'.$tabClass.'.png');	
		$tab = new Tab();
		$tab->name = $tabName;
		$tab->class_name = $tabClass;
		$tab->module = $this->name;
		$tab->id_parent = $idTabParent;
		if(!$tab->save())
			return false;
		return true;
	}
	private function uninstallModuleTab($tabClass){
		$idTab = Tab::getIdFromClassName($tabClass);
		if($idTab != 0){
			$tab = new Tab($idTab);
			$tab->delete();
			return true;
		}
		return false;
	}
    /**
    * Uninstall Table
    */
    public function uninstallTable(){
        if (!file_exists(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE))
			return (false);
		else if (!$sql = file_get_contents(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE))
			return (false);
		$sql = str_replace('ps_', _DB_PREFIX_, $sql);
		$sql = preg_split("/;\s*[\r\n]+/",$sql);
        foreach ($sql as $query)
			if (!Db::getInstance()->Execute(trim($query)))
				return (false);
        return true;  
    }
    /**
    * Install Table
    */
    public function installTable(){
        if (!file_exists(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE))
			return (false);
		else if (!$sql = file_get_contents(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE))
			return (false);
		$sql = str_replace('ps_', _DB_PREFIX_, $sql);
		$sql = preg_split("/;\s*[\r\n]+/",$sql);
		foreach ($sql as $query){
			if(!empty($query)){
				if (!Db::getInstance()->Execute(trim($query)))
					return (false);  
			}
		}
        return true;  
    }
    /**
    *** Init FAQCategory 
    **/
    private function initFAQCategory()
	{
        global $cookie;
        $faqcategories = LofFaqCategory::getFaqCategory($this->context->language->id);
        if(isset($faqcategories) AND !empty($faqcategories))return;

        $names = array('Product','Theme','Extension','Payment, Shipping, Refund, Upgrade');
        foreach($names as $name){
            $faqcategories = new LofFaqCategory();
	    	$languages = Language::getLanguages(true);
            foreach($languages as $lang)
            {
                $faqcategories->{'name'}[$lang['id_lang']] = $name;
            }
            $faqcategories->active = 1;
            
            $faqcategories->save(); 
	    }
	}
    
    /**
    * Hook Position
    */
    function hookHeader($params)
	{
		$this->context->controller->addCSS( ($this->_path).'assets/style.css', 'all');
		$this->context->controller->addCSS( ($this->_path).'tmpl/'. $this->getParamValue('module_theme','default').'/assets/style.css', 'all');
		if( !defined("_PTX_FAQ_") ){
			$this->context->controller->addJS( ($this->_path).'assets/script.js', 'all');
			define('_PTX_FAQ_', 1);         
		}
	}
    
  	function hookrightColumn($params)
	{	
	    global $smarty,$cookie; 
        $limit_items_recent = $this->getParamValue("recent_limit", 5);
        $recentfaq = LofClassFaq::recentFaq();
        
        $limit_items_faqcat = $this->getParamValue("limit_category", 5);
        $faqcategory = LofFaqCategory::getFaqCategory($this->context->language->id, 1);
        foreach($faqcategory as $k=>$v){
            $faqcategory[$k]['img_link'] = $this->getFaqImageLink($faqcategory[$k]['id_loffaq_category'], 'small');
        }
        
        $recent_title = $this->getParamValue("recent_title",'Recent FAQ');
		
		$enable_bCat = $this->getParamValue("enable_bCat",1);
		$enable_bRen = $this->getParamValue("enable_bRen",1);
       
        $this->context->smarty->assign(array(
			'enable_bCat' => $enable_bCat,
			'enable_bRen' => $enable_bRen,
			
            'recentfaq' => $recentfaq,
            'limit_items_recent' => $limit_items_recent,
            
            'faqcategory' => $faqcategory,
            'limit_items_faqcat' => $limit_items_faqcat,
            'recent_title' => $recent_title
        ));
		return $this->processHook( $params,"hookcolumn" );
	}
    function hookleftColumn($params)
	{
		global $smarty,$cookie; 
        $limit_items_recent = $this->getParamValue("recent_limit", 5);
        $recentfaq = LofClassFaq::recentFaq();
        
        $limit_items_faqcat = $this->getParamValue("limit_category", 5);
        $faqcategory = LofFaqCategory::getFaqCategory($this->context->language->id, 1);
        foreach($faqcategory as $k=>$v){
            $faqcategory[$k]['img_link'] = $this->getFaqImageLink($faqcategory[$k]['id_loffaq_category'], 'small');
        }
        
        $recent_title = $this->getParamValue("recent_title",'Recent FAQ');
		
		$enable_bCat = $this->getParamValue("enable_bCat",1);
		$enable_bRen = $this->getParamValue("enable_bRen",1);
        
        $this->context->smarty->assign(array(
			'enable_bCat' => $enable_bCat,
			'enable_bRen' => $enable_bRen,
			
            'recentfaq' => $recentfaq,
            'limit_items_recent' => $limit_items_recent,
            
            'faqcategory' => $faqcategory,
            'limit_items_faqcat' => $limit_items_faqcat,
            'recent_title' => $recent_title
        ));
		return $this->processHook( $params,"hookcolumn");
	}
    
    public function hookProductTab($params){
	    if (!$this->active) return;
		global $smarty,$cookie;
        $theme = Configuration::get($this->name.'_module_theme');
        if(!$theme) $theme = "default";
        $module_title = $this->l('FAQ');
		$enable_pdetail = $this->getParamValue('enable_pdetail', 1);

  		$this->context->smarty->assign(array(
			'module_title' => $module_title,
			'enable_pdetail' => $enable_pdetail
        ));
		return ($this->display(__FILE__, 'tmpl/'.$theme.'/hooktab.tpl'));
	}
    function initJS(){
		$loffaq = new loffaq();
		$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$site_url = Tools::htmlentitiesutf8($protocol.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		
        $rand = '';
        $module_name = $this->name;
        $id_cat = Tools::getValue("cat", 0);
        $id_product = Tools::getValue("id_product", 0);
		$iscaptcha = $this->getParamValue("captcha",1);
        $is_private = $this->getParamValue("is_private",0);
        $premoderation = $this->getParamValue("premoderation",1);
        $allow_guest = $this->getParamValue("allow_guest",1);
        
        $isguest = (!$this->context->customer->id) ? '1': '0'; 
                
        ob_start();
			require(dirname(__FILE__).'/initjs.php');
			$initjs = ob_get_contents();
		ob_end_clean();
		return $initjs;
	}
    //function hookproductfooter($params)
    public function hookProductTabContent($params)
    {   
        global $smarty,$cookie;
        $loffaq = new loffaq(); 
		
		$theme = Configuration::get($this->name.'_module_theme');
		if(isset($theme)){
			$theme = "default";
		}else{
			$theme = Configuration::get($this->name.'_module_theme');
		}
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$site_url = Tools::htmlentitiesutf8($protocol.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		
        $rand = '';
        $module_name = $this->name;
        $id_cat = Tools::getValue("cat", 0);
        $id_product = Tools::getValue("id_product", 0);
        //$is_product_faq = $this->getParamValue("product_faq");
        
        $faqcategories = LofFaqCategory::getFaqCategory($this->context->language->id,1);
        
        $page = 'product.php?id_product='.$id_product;
        $formsubmit = dirname(__FILE__).'/tmpl/'.$theme.'/formsubmit.tpl';

        $faqs = LofClassFaq::getListFAQ($this->context->language->id, NULL, $id_cat, $id_product);
        $html_faqs = $loffaq->generateFaqList($faqs);
        
        $iscaptcha = $this->getParamValue("captcha",1);
        $is_private = $this->getParamValue("is_private",0);
        $premoderation = $this->getParamValue("premoderation",1);
        $allow_guest = $this->getParamValue("allow_guest",1);
        
        $isguest = (!$this->context->customer->id) ? '1': '0';
                
        ob_start();
			require(dirname(__FILE__).'/initjs.php');
			$initjs = ob_get_contents();
		ob_end_clean();
 
        //Submit button
        if(Tools::isSubmit("loffaq_send")){
			
            $lofcaptcha = Tools::getValue("loffaq_captcha");
    
            if($iscaptcha == '1'){
                if(trim($lofcaptcha) == '' || empty($lofcaptcha) || ($_SESSION['loffaq_captcha'] != strtoupper($lofcaptcha))){
                    $this->_postErrors[] = $this->l('Captcha verification does not match.');
                }
            } 
            if(empty($this->_postErrors)){
                $objfaqs = new LofClassFaq();
                $question = array();
                $languages = Language::getLanguages(false);
                foreach ($languages as $lang)
                    $question[$lang['id_lang']] = Tools::getValue('loffaq_question');
                
                $objfaqs->question = $question;
                
                $objfaqs->is_private = Tools::getValue("is_private");   
                $objfaqs->id_customer = (!empty($this->context->customer->id)) ? $this->context->customer->id : '0';
                if(!$this->context->customer->isLogged()){
                    $objfaqs->customer_name = Tools::getValue("loffaq_name");;
                    $objfaqs->email = Tools::getValue("loffaq_email");
                }else{
					$objfaqs->customer_name = $this->context->customer->customer_firstname.' '.$this->context->customer->customer_lastname;
					$objfaqs->email = $this->context->customer->email;
				}
                $objfaqs->id_loffaq_category = Tools::getValue("loffaq_category");      
                $objfaqs->id_product = $id_product;                                           
                $objfaqs->type = 0;
                $objfaqs->active = ($premoderation == '1') ? 0 : 1;
                $objfaqs->add();
                $objfaqs->save();
                //send mail
                $configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_MAIL_SERVER', 'PS_MAIL_USER', 'PS_MAIL_PASSWD', 'PS_SHOP_NAME'));
                $notification_email = $this->getParamValue("notification_email");
                
                $to = $this->getParamValue("email_admin", Configuration::get("PS_SHOP_EMAIL"));
                if($notification_email){
                    if($this->context->customer->isLogged()){
                        $customer_name = ($this->context->customer->customer_firstname.' '.$this->context->customer->customer_lastname);
                    }else{
                        $customer_name = Tools::getValue('loffaq_name');
                    }
                    $question = $objfaqs->question;
                    $date_add = $objfaqs->date_add;
                    $faq_category = LofFaqCategory::getFaqCategoryName($this->context->customer->id_lang,Tools::getValue("loffaq_category"));
                    $templateVars = array(
						'{customer_name}' => $customer_name,
                        '{question}' => $question,
						'{date_add}' => $date_add,
                        '{faq_category}' => $faq_category
				    );
                    if(!Mail::Send((int)$this->context->language->id, 'faq', Mail::l('FAQ posted send to you '),
    					$templateVars, $to, NULL, ($configuration['PS_SHOP_EMAIL'] ? $configuration['PS_SHOP_EMAIL'] : NULL),
    					($configuration['PS_SHOP_NAME'] ? $configuration['PS_SHOP_NAME'] : NULL), NULL, NULL,
    					dirname(__FILE__).'/mails/', true)){
    					$this->_postErrors[] = $this->l('Send e-mail error occurred during the process.');
    				}
                }    
                
                $mes = ($premoderation == '1') ? $this->l('Please wait for approvement.') : $this->l('Please F5 refesh your page.'); 
                $this->_postSuccess =  $this->l('Your question has been posted successfully. '.$mes);
                //Tools::redirect('product.php?id_product='.$id_product);
            }         
        }
        
		$enable_pdetail = $this->getParamValue('enable_pdetail', 1);
        //$id_product =  Tools::getValue('id_product', 0);
        //{include file="{$formsubmit}"}
        $this->context->smarty->assign(array(
            'id_product' =>$id_product,
			'enable_pdetail' => $enable_pdetail,
            'isguest' => $isguest,
            'allow_guest' => $allow_guest,
            //'is_product_faq' => $is_product_faq,
            'module_name' => $this->name,
            'id_cat' => $id_cat,
            'rand' => rand(10,100),
            'site_url' => $site_url,
            'module_name' => $this->name,
            'iscaptcha' => $iscaptcha,
            'is_private' => $is_private,
            
            'html_faqs' => $html_faqs,
            'page' => $page,
            'formsubmit' => $formsubmit,
            
            'faqcategories' => $faqcategories,
            'postSuccess' => $this->_postSuccess,
            'postErrors' => $this->_postErrors
        ));
		return $this->processHook( $params,"hookproductfooter",$initjs);
	}
    function hooktop($params)
    {
        global $smarty,$cookie; 
		return $this->processHook( $params,"hooktop");
    }
	function hookHome($params)
	{
		return $this->processHook( $params,"home");
	}
    
    

	/**
    * Proccess module by hook
    * $pparams: param of module
    * $pos: position call
    */
	function processHook( $params, $pos, $extrahtml = ""){                
		$params = $this->_params;
		$theme = Configuration::get($this->name.'_module_theme');
        if(!$theme) $theme = "default";
		$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$site_url = Tools::htmlentitiesutf8($protocol.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
        
        
		return ($this->display(__FILE__, 'tmpl/'.$theme.'/'.$pos.'.tpl')).$extrahtml;	
	}
    public function getLayoutPath( $theme ){
        $layout = 'tmpl/'.$theme.'/default.tpl';
        if( !file_exists(__FILE__."/".$layout) ){
            return $layout;
        }
        return 'tmpl/default.tpl';
    }  	   	   	
   /**
    * Get list of sub folder's name 
    */
	public function getFolderList( $path ) {
		$items = array();
		$handle = opendir($path);
		if (! $handle) {
			return $items;
		}
		while (false !== ($file = readdir($handle))) {
			if (is_dir($path . $file))
				$items[$file] = $file;
		}
		unset($items['.'], $items['..'], $items['.svn']);
		return $items;
	}
   /**
    * Render processing form && process saving data.
    */	
	public function getContent()
	{
 		$html = "";
		if (Tools::isSubmit('submit')){
			$this->_postValidation();
			if (!sizeof($this->_postErrors)){	
                $definedConfigs = array(
                    'module_theme' => '',
					'enable_pdetail' => '',
					'enable_bCat' => '',
					'enable_bRen' => '',
                    'premoderation' => '',
                    'allow_guest' => '',
                    'captcha' => '',
                    'is_private' => '',
                    'notification_email' => '',
                    'email_admin' => '',
                    'product_faq' => '',
                    
                    'limit_question' => '',
                    'limit_category' => '',
                    'enable_gravatar' => '',
                    'show_username' => '',
                    'show_createdate' => '',
                    'show_helpfulness' => '',
                    'enable_rating' => '',
                    'show_catimage' => '',
                    'width' => '',
                    'height' => '',
                    
                    'recent_title' => '',
                    
                );
                
                foreach( $definedConfigs as $config => $key ){
					$config_value = Tools::getValue($config);
					if(is_array($config_value)){
						$config_value = serialize($config_value);
					}
		      		Configuration::updateValue($this->name.'_'.$config, $config_value, true);
		    	}
                
                
		        $html .= '<div class="conf confirm">'.$this->l('Settings updated').'</div>';
			}
			else{
				foreach ($this->_postErrors AS $err){
					$html .= '<div class="alert error">'.$err.'</div>';
				}
			}
			// reset current values.
			$this->_params = new LofParams( $this->name );	
		}
		return $html.$this->_getFormConfig();
	}
	/**
	 * Render Configuration From for user making settings.
	 *
	 * @return context
	 */
	private function _getFormConfig(){		
		$html = '';
	    $themes=$this->getFolderList( dirname(__FILE__)."/tmpl/" );
	    ob_start();
	    include_once dirname(__FILE__).'/config/loffaq.php'; 
	    $html .= ob_get_contents();
	    ob_end_clean(); 
		return $html;
	}
	/**
     * Process vadiation before saving data 
     */
	private function _postValidation()
	{
		//if (!Validate::isCleanHtml(Tools::getValue('module_height')))
		//	$this->_postErrors[] = $this->l('The module height you entered was not allowed, sorry');
	}
   /**
    * Get value of parameter following to its name.
    * 
	* @return string is value of parameter.
	*/
	public function getParamValue($name, $default=''){
		return $this->_params->get( $name, $default );	
	} 
    
    /**
    * Generate List FAQ
    **/
    
    public function generateFaqList($faqs){

        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$site_url = Tools::htmlentitiesutf8($protocol.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
        $html = "";
		$theme = Configuration::get($this->name.'_module_theme');
		if(!$theme) $theme = "default";
        $countFaq = LofClassFaq::countFAQ($faqs);
        $enable_gravatar = $this->getParamValue('enable_gravatar',1);
        $show_createdate = $this->getParamValue('show_createdate',1); 
        $show_username = $this->getParamValue('show_username',1);
        $show_helpfulness = $this->getParamValue('show_helpfulness',1);
        $enable_rating = $this->getParamValue('enable_rating',1);
        $limit_question = $this->getParamValue('limit_question',5);
        $allow_guest = $this->getParamValue("allow_guest",1);
        
        $initjs = $this->initJS();
        $l = $this->getLangs();
        if(!empty($faqs)){
			ob_start();
			require_once( _PS_MODULE_DIR_. 'loffaq/tmpl/'.$theme.'/listfaq.php');
			$html = ob_get_contents();
			ob_end_clean();
		}
		return $html;
    }
    
    public function getFaqImageLink($id_faq, $type = null)
	{
	   if(is_file(_PS_ROOT_DIR_.'/img/faq/'.$id_faq.($type ? '-'.$type : '').'.jpg'))
	       return(__PS_BASE_URI__.'img/faq/'.$id_faq.($type ? '-'.$type : '').'.jpg'); 
       else{
           return(__PS_BASE_URI__.'img/404.gif'); 
       }
	}
	public function getLangs(){
		return array(
			'Found' => $this->l('Found'),
			'record(s) FAQ' => $this->l('record(s) FAQ'),
			'No record(s) found' => $this->l('No record(s) found'),
			'record(s) FAQ' => $this->l('record(s) FAQ'),
			'out of' => $this->l(' out of '),
			'people find this question interesting' => $this->l(' people find this question interesting'),
			'Like' => $this->l('Like'),
			'UnLike' => $this->l('UnLike')
		);
	}
}



