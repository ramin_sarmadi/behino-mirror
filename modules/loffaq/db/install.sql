/*
Source Server         : landofcoder
Source Server Version : 50136
Source Host           : localhost:3306
Source Database       : db_prestashop

Target Server Type    : MYSQL
Target Server Version : 50136
File Encoding         : 65001

Date: 2012-05-18 14:57:24
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `ps_loffaq_category`
-- ----------------------------
DROP TABLE IF EXISTS `ps_loffaq_category`;
CREATE TABLE IF NOT EXISTS `ps_loffaq_category` (
  `id_loffaq_category` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(250) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_loffaq_category`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

-- ----------------------------
-- Records of ps_loffaq_category
-- ----------------------------
DROP TABLE IF EXISTS `ps_loffaq_category_shop`;
CREATE TABLE IF NOT EXISTS `ps_loffaq_category_shop` (
  `id_loffaq_category` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_loffaq_category`,`id_shop`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
-- ----------------------------
-- Table structure for `ps_loffaq_category_lang`
-- ----------------------------
DROP TABLE IF EXISTS `ps_loffaq_category_lang`;
CREATE TABLE IF NOT EXISTS `ps_loffaq_category_lang` (
  `id_loffaq_category` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_loffaq_category`,`id_lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for `ps_loffaq_faq`
-- ----------------------------
DROP TABLE IF EXISTS `ps_loffaq_faq`;
CREATE TABLE IF NOT EXISTS `ps_loffaq_faq` (
  `id_loffaq_faq` int(11) NOT NULL AUTO_INCREMENT,
  `id_loffaq_category` int(11) NOT NULL,
  `id_customer` int(11) DEFAULT '0' COMMENT '0:guest',
  `customer_name` varchar(200) DEFAULT '',
  `email` varchar(200) DEFAULT '',
  `like` int(11) DEFAULT '0',
  `unlike` int(11) DEFAULT '0',
  `type` tinyint(4) DEFAULT '0' COMMENT '0:customer create FAQ, 1:admin create FAQ',
  `is_private` tinyint(4) DEFAULT '0' COMMENT '1:private',
  `active` tinyint(4) DEFAULT '0',
  `date_add` datetime DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `rating_count` int(11) DEFAULT '0',
  `rating_sum` int(11) DEFAULT '0',
  PRIMARY KEY (`id_loffaq_faq`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

DROP TABLE IF EXISTS `ps_loffaq_faq_lang`;
CREATE TABLE IF NOT EXISTS `ps_loffaq_faq_lang` (
  `id_loffaq_faq` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL DEFAULT '0',
  `answer` text,
  `question` text,
  PRIMARY KEY (`id_loffaq_faq`,`id_lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
-- ----------------------------
-- Records of ps_loffaq_category_lang
-- ----------------------------
DROP TABLE IF EXISTS `ps_loffaq_faq_shop`;
CREATE TABLE IF NOT EXISTS `ps_loffaq_faq_shop` (
  `id_loffaq_faq` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_loffaq_faq`,`id_shop`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;