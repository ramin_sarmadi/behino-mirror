<script type="text/javascript">

    function faq_search(id_product){   
		//remove message
		if($('#search_loading')){
			$('#search_loading').show();
			$('.loffaq-btn').first().hide();
		}
		if($('#lof-messages').length > 0){
			$('#lof-messages').remove();
		}
        var value = $(".input-text").val();
        $.post(baseDir + 'modules/loffaq/ajax.php', {
    		action:'search',
            id_product: id_product,
            value:value,
            id_cat: <?php echo $id_cat;?>
	    },
        function (data) {
			if($('#search_loading')){
				$('#search_loading').hide();
				$('.loffaq-btn').first().show();
			}
    		if (data.status == 'success') {
        		$(".lof-faq").html('');	
				$(".lof-faq").prepend(data.params.content);
    		} else {
    			alert(data.message);
    		}
	    }, 'json');
    }
    
    $(document).ready(function() {
        $('#loffaq_refresh').click(function(){
			$('#loffaq_captcha').attr('src','<?php echo $site_url.'modules/'.$module_name; ?>/captcha.php?rand=' + Math.random());
		});        
        $('#form-toggle').click(function() {
            //$('#submit-box').slideToggle('slow');
            $('html, body').animate({scrollTop:$('#lof-faqsubmit-box').offset().top}, 'slow');
            return false;
        });
        $('#lof-faqcat-box').click(function() {
            $('#lof-faqcat-contentbox').slideToggle('slow');
                return;
        });
        $('#lof-faqsearch-box').click(function() {
            $('#lof-faqsearch-contentbox').slideToggle('slow');
                return;
        });
        $('#lof-faqsubmit-box').click(function() {
            $('#lof-faqsubmit-contentbox').slideToggle('slow');
                return;
        });
        
        
        
        // valid form
        var form = $("#loffaq-submit");
        var name = $("#loffaq_name");
        var nameInfo = $("#loffaq_name_info");
        var email = $("#loffaq_email");
        var emailInfo = $("#loffaq_email_info");
        
    	var cat = $("#loffaq_category");
    	var catInfo = $("#loffaq_category_info");
        var question = $("#loffaq_question");
        var questionInfo =$("#loffaq_question_info")
        var captcha = $("#loffaq_captchainput");
        var captchaInfo = $("#loffaq_captchainput_info");
    	
        
        question.blur(validateQues);
    	question.keyup(validateQues);
        cat.blur(validateCat);
    	cat.keyup(validateCat);
        
    	<?php if($iscaptcha == '1') {?> 
        captcha.blur(validateCap);
        captcha.keyup(validateCap);
        <?php } ?>
        <?php if($isguest == '1') {?>
        name.blur(validateName);
        name.keyup(validateName);
        email.blur(validateEmail);
        email.keyup(validateEmail);
        <?php } ?>
        
        
    	form.submit(function(){
    		if(validateCat() & validateQues() <?php if($iscaptcha == '1') {?>& validateCap()<?php } ?> <?php if($isguest == '1') {?> & validateEmail() & validateName()<?php } ?> )
    			return true
    		else
    			return false;
    	});
        function validateEmail(){
    		var a = $("#loffaq_email").val();
    		var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
    		if(filter.test(a)){
    			email.removeClass("lof-error");
    			emailInfo.text("");
    			emailInfo.removeClass("lof-error");
    			return true;
    		}
    		else{
    			email.addClass("lof-error");
    			emailInfo.text("Please type a valid e-mail!");
    			emailInfo.addClass("lof-error");
    			return false;
    		}
    	}
    	function validateName(){
    		if(name.val().length < 4){
    			name.addClass("lof-error");
    			nameInfo.text("<?php echo $loffaq->l('We want names with more than 3 letters!');?>");
    			nameInfo.addClass("lof-error");
    			return false;
    		}
    		else{
    			name.removeClass("lof-error");
    			nameInfo.text("");
    			nameInfo.removeClass("lof-error");
    			return true;
    		}
    	}
        function validateCat(){
            var scat = $("#loffaq_category option:selected").val();
            if(scat < 1){
                cat.addClass("lof-error");
                catInfo.text("<?php echo $loffaq->l('Please select a category!');?>");
                catInfo.addClass("lof-error");
            }
            else{
    			cat.removeClass("lof-error");
                catInfo.text("");
    			catInfo.removeClass("lof-error");
    			return true;
    		}
        }
        function validateQues(){
    		if(question.val().length < 4){
    			question.addClass("lof-error");
    			questionInfo.text("<?php echo $loffaq->l('We want question with more than 3 letters!');?>");
    			questionInfo.addClass("lof-error");
    			return false;
    		}
    		else{
    			question.removeClass("lof-error");
                questionInfo.text("");
    			questionInfo.removeClass("lof-error");
    			return true;
    		}
	    }
        function validateCap(){
            if(captcha.val().length < 6){
    			captcha.addClass("lof-error");
    			captchaInfo.text("<?php echo $loffaq->l('Please enter captcha!');?>");
    			captchaInfo.addClass("lof-error");
    			return false;
    		}
    		else{
    			captcha.removeClass("lof-error");
                captchaInfo.text("");
    			captchaInfo.removeClass("lof-error");
    			return true;
    		}
        }
    });
</script>