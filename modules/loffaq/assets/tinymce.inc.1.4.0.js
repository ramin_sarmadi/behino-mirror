

tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	plugins : "safari,pagebreak,advlink",
	// Theme options
	theme_advanced_buttons1 : "newdocument,|,bold,italic,link,unlink,anchor",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_buttons4 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : false,
	content_css : pathCSS+"global.css",
	document_base_url : ad,
	width: "415",
	height: "auto",
	font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
	elements : "nourlconvert",
	file_browser_callback : "",
	entity_encoding: "raw",
	convert_urls : false,
	language : iso
	
});
