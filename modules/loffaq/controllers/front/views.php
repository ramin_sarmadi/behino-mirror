<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 17015 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
class LoffaqViewsModuleFrontController extends ModuleFrontController
{
	public $errors;
	public $expandForm = false;
	private $_postSuccess;
	private $_postErrors;

	public function __construct()
	{
		parent::__construct();
		
		$this->context = Context::getContext();
		
		include_once(dirname(__FILE__).'/../../classes/LofClassFaq.php');
		include_once(dirname(__FILE__).'/../../classes/LofFaqCategory.php');

		// Declare smarty function to render pagination link
		smartyRegisterFunction($this->context->smarty, 'function', 'summarypaginationlink', array('LoyaltyDefaultModuleFrontController', 'getSummaryPaginationLink'));
	}

	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess() {
		if (Tools::isSubmit('loffaq_send')){
			$this->submitSend();
			}
	}
	/**
	 * Transform loyalty point to a voucher
	 */
	public function submitSend()
	{
		$lofcaptcha = Tools::getValue("loffaq_captcha");
		$iscaptcha = $this->module->getParamValue("captcha",1);
        if($iscaptcha == '1')
            if(trim($lofcaptcha) == '' || empty($lofcaptcha) || ($_SESSION['loffaq_captcha'] != strtoupper($lofcaptcha)))
                $this->_postErrors[] = $this->module->l('Captcha verification does not match.');
            
        if(empty($this->_postErrors)){ 
            $objfaqs = new LofClassFaq();
            $premoderation = $this->module->getParamValue("premoderation",1);
            $question = array();
            $languages = Language::getLanguages(false);
            foreach ($languages as $lang)
                $question[$lang['id_lang']] = Tools::getValue('loffaq_question');
            
            $objfaqs->question = $question;

            $objfaqs->is_private = Tools::getValue("is_private");
            $objfaqs->id_product = Tools::getValue('id_product',0);
            $objfaqs->id_customer = (!empty($this->context->customer->id)) ? $this->context->customer->id : '0';
            if(!$this->context->customer->id){
                $objfaqs->customer_name = Tools::getValue("loffaq_name");
                $objfaqs->email = Tools::getValue("loffaq_email");
            }else{
                $objfaqs->customer_name = $this->context->customer->customer_firstname.' '.$this->context->customer->customer_lastname;
                $objfaqs->email = $this->context->customer->email;
            }
            $objfaqs->id_loffaq_category = Tools::getValue("loffaq_category");
            $objfaqs->type = 0;
            $objfaqs->active = ($premoderation == '1') ? 0 : 1;
            $objfaqs->add();
            $objfaqs->save();
            //send mail
            $configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_MAIL_SERVER', 'PS_MAIL_USER', 'PS_MAIL_PASSWD', 'PS_SHOP_NAME'));
            $notification_email = $this->module->getParamValue("notification_email",0);
            
            $to = $this->module->getParamValue("email_admin", Configuration::get("PS_SHOP_EMAIL"));
            if($notification_email){
                if($this->context->customer->isLogged()){
                    $customer_name = ($this->context->customer->customer_firstname.' '.$this->context->customer->customer_lastname);
                }else{
                    $customer_name = Tools::getValue('loffaq_name');
                }
                $question = $objfaqs->question;
                $date_add = $objfaqs->date_add;
                $faq_category = LofFaqCategory::getFaqCategoryName($this->context->language->id,Tools::getValue("loffaq_category"));
                $templateVars = array(
					'{customer_name}' => $customer_name,
                    '{question}' => $question,
					'{date_add}' => $date_add,
                    '{faq_category}' => $faq_category
			    );
                if(!Mail::Send((int)$this->context->language->id, 'faq', Mail::l('FAQ posted send to you '),
					$templateVars, $to, NULL, ($configuration['PS_SHOP_EMAIL'] ? $configuration['PS_SHOP_EMAIL'] : NULL),
					($configuration['PS_SHOP_NAME'] ? $configuration['PS_SHOP_NAME'] : NULL), NULL, NULL,
					dirname(dirname(dirname(__FILE__))).'/mails/', true)){

					$this->_postErrors[] = $this->module->l('Send e-mail error occurred during the process.');
				}
            }
			
            //Tools::redirect('modules/loffaq/view.php');
            $mes = ($premoderation == '1') ? $this->module->l('Please wait for approvement.') : $this->module->l('Please F5 refesh your page.');
            $this->_postSuccess =  $this->module->l('Your question has been posted successfully. '.$mes);
        }
	}
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent() {
		parent::initContent();
		$this->viewList();
		
	}
	/**
	 * Assign summary template
	 */
	public function viewList() {
		global $cookie;

		$site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		//box category center
		$limit_items_faqcat = $this->module->getParamValue("limit_category", 5);
		$faqcategory = LofFaqCategory::getFaqCategory($this->context->language->id, 1);
		foreach($faqcategory as $k=>$v){
			$faqcategory[$k]['img_link'] = $this->module->getFaqImageLink($faqcategory[$k]['id_loffaq_category'], 'medium');
		}
		$id_cat = Tools::getValue("cat", 0);
		$module_title = $this->module->l("default");
		$theme = $this->module->getParamValue("module_theme","default");
		$iscaptcha = $this->module->getParamValue("captcha",1);
		$is_private = $this->module->getParamValue("is_private",1);
		$cat_width = $this->module->getParamValue("width",80);
		$cat_height = $this->module->getParamValue("height",80);
		$show_catimage = $this->module->getParamValue("show_catimage",1);
		$premoderation = $this->module->getParamValue("premoderation",1);
		$allow_guest = $this->module->getParamValue("allow_guest",1);
		
		$isguest = (!$this->context->customer->id) ? '1': '0';  
		
		
		
		$faqs = LofClassFaq::getListFAQ($this->context->language->id, NULL, $id_cat);
		//echo '<pre>'; print_r($faqs); die;
		$html_faqs = $this->module->generateFaqList($faqs);
		$faqcategories = LofFaqCategory::getFaqCategory($this->context->language->id,1);
		$formsubmit = dirname(__FILE__).'/../../tmpl/'.$theme.'/formsubmit.tpl';
		
		ob_start();
			include_once(dirname(__FILE__).'/initjs.php');
			$initjs = ob_get_contents();
		ob_end_clean();
		$this->context->smarty->assign(array(
            'id_proudct' => '0',
			'site_url' => $site_url,
			'isguest' => $isguest,
			'allow_guest' => $allow_guest,
		
			'faqcategory' => $faqcategory,
			'limit_items_faqcat' => $limit_items_faqcat,
		
			'loffaq_title' => $module_title,
			'rand' => rand(10,100),
			'module_name' => $this->module->name,
			'formsubmit' => $formsubmit,
			'html_faqs' => $html_faqs,
			'faqcategories' => $faqcategories,
			
			'iscaptcha' => $iscaptcha,
			'is_private' => $is_private,
			'postSuccess' => $this->_postSuccess,
			'postErrors' => $this->_postErrors,
			'id_cat' => $id_cat,
			'cat_width' => $cat_width,
			'cat_height' => $cat_height,
			'show_catimage' => $show_catimage,
			'initjs' => $initjs,
		));
		
		$this->setTemplate('default.tpl');
	}
	
	/**
	 * Render pagination link for summary
	 *
	 * @param (array) $params Array with to parameters p (for page number) and n (for nb of items per page)
	 * @return string link
	 */
	public static function getSummaryPaginationLink($params, &$smarty) {
		if (!isset($params['p']))
			$p = 1;
		else
			$p = $params['p'];

		if (!isset($params['n']))
			$n = 10;
		else
			$n = $params['n'];

		return Context::getContext()->link->getModuleLink(
			'loyalty',
			'default',
			array(
				'process' => 'summary',
				'p' => $p,
				'n' => $n,
			)
		);
	}
	
	public function getTemplatePath(){
		return _PS_MODULE_DIR_.$this->module->name.'/tmpl/'.$this->module->getParamValue('module_theme','default').'/';
	}
}
