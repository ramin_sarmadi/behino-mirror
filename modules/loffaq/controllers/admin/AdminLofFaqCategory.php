<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
include_once(dirname(__FILE__).'/../../classes/LofFaqCategory.php');
class AdminLofFaqCategoryController extends AdminController{
	public $_defaultFormLanguage;
	public $lofid_parent;
    private $imagesTypes = array(
                Array( 'name' => 'faq','width' => 500,'height' => 150),
                Array( 'name' => 'large','width' => 300,'height' => 300),
                Array( 'name' => 'medium','width' => 80,'height' => 80),
                Array( 'name' => 'small','width' => 45,'height' => 45)
           );
    
    public function __construct(){
		global $cookie;
		$this->lofid_parent = 'id_lofdcategory_parent';
        $this->table = 'loffaq_category';
	 	$this->className = 'LofFaqCategory';
		$this->identifiersDnd = array('id_loffaq_category' => 'id_loffaq_category_to_move');
		$this->position_identifier = 'id_loffaq_category_to_move';
	 	$this->identifier = 'id_loffaq_category';
	 	$this->lang = true;
		$this->addRowAction('view');
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		 Shop::addTableAssociation('lofreview_criterion', array('type' => 'shop'));
        $this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
        $this->fieldImageSettings = array('name' => 'logo', 'dir' => 'faq');
		
        parent::__construct(); 
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		
        $this->_select = 'hss.`position` as position';
        $this->_join = 'INNER JOIN `'._DB_PREFIX_.'loffaq_category_shop` hss ON (a.id_loffaq_category = hss.id_loffaq_category AND hss.`id_shop` = '.(int)$id_shop.')';
		
        $this->fields_list = array(
				'id_loffaq_category' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 30),
                'logo' => array('title' => $this->l('Logo'), 'align' => 'center', 'width' => 100, 'image' => 'faq', 'orderby' => false, 'search' => false),
				'name' => array('title' => $this->l('Name'),'width' => 200),
				'description' => array('title' => $this->l('Description'),'width' => 300),
				//postion
				'position' => array(
				'title' => $this->l('Position'),
				'width' => 40,
				'filter_key' => 'sa!position',
				'align' => 'center',
				'position' => 'position'
				),
                //'ordering' => array('title' => $this->l('Ordering'), 'width' => 40, 'align' => 'center'),
                'active' => array('title' => $this->l('Displayed'),'width' => 40, 'active' => 'status', 'align' => 'center', 'type' => 'bool', 'orderby' => false),
        );
        
		$this->getlanguages();
		$this->_defaultFormLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
    }
	public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
	{
		$alias = 'hss';
		parent::getList($id_lang, $alias.'.position', $order_way, $start, $limit);
		
	}
	public function initBreadcrumbs() {
		$this->breadcrumbs[] = $this->l('Category');
	}
	
	public function renderList() {
		$this->initToolbar();
		return parent::renderList();
	}
	
    public function renderForm($token = NULL)
	{
		global $currentIndex, $cookie;
		if (!($obj = $this->loadObject(true)))
			return;
		if (Validate::isLoadedObject($this->object))
			$this->display = 'edit';
		else
			$this->display = 'add';
		$this->initToolbar();
		
		$this->fields_form = array(
			'tinymce' => false,
			'legend' => array(
				'title' => $this->l('Category'),
				'image' => '../img/admin/quick.gif'
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Title:'),
					'name' => 'name',
					'lang' => true,
					'required' => true,
					'class' => '',
					'hint' => $this->l('Invalid characters:').' <>;=#{}',
                    'desc' => $this->l('criterion value, e.g., Technical Support'),
					'size' => 50
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Active:'),
					'name' => 'active',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						    )
				    	),
				),
				array(
					'type' => 'textarea',
					'label' => $this->l('Description:'),
					'name' => 'description',
					'lang' => true,
					'required' => true,
					'class' => '',
					'rows' => '10',
					'cols' => '100',
				),
				 array(
					'type' => 'file',
					'label' => $this->l('Image:'),
					'name' => 'image',
					'display_image' => true,
					'desc' => $this->l('Upload faq category logo from your computer')
				)
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);
		//$image = _PS_IMG_DIR_.'faq/'.$id_loffaq_category.'.jpg';
		$image = ImageManager::thumbnail(_PS_IMG_DIR_.'faq/'.$obj->id.'.jpg', $this->table.'_'.(int)$obj->id.'.'.$this->imageType, 350, $this->imageType, true);

		$this->fields_value = array(
			'image' => $image ? $image : false,
			'size' => $image ? filesize(_PS_IMG_DIR_.'faq/'.$obj->id.'.jpg') / 1000 : false
		);

		$this->tpl_form_vars = array(
			'active' => $this->object->active,
			'PS_ALLOW_ACCENTED_CHARS_URL', (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL')
		);
        return parent::renderForm();
	}

    public function cacheImage($image, $cacheImage, $size, $imageType = 'jpg', $disableCache = false)
	{
		return ImageManager::thumbnail($image, $cacheImage, $size, $imageType, $disableCache);
	}

    public function displayImage($id, $image, $size, $id_image = NULL, $token = NULL, $disableCache = false)
	{
		$html = '';
		if (!isset($token) OR empty($token))
			$token = $this->token;
		if ($id AND file_exists($image))
			$html .= '
			<div id="image" >
				'.$this->cacheImage($image, $this->table.'_'.(int)($id).'.'.$this->imageType, $size, $this->imageType, $disableCache).'
				<p align="center">'.$this->l('File size').' '.(filesize($image) / 1000).'kb</p>
			</div>';
		return $html;
	}
    
    public function postProcess($token = null)
	{
        global $cookie; 
		$currentIndex = self::$currentIndex;
		
		if (Tools::isSubmit('submitAdd'.$this->table)){
			if ($id_loffaq_category = (int)(Tools::getValue('id_loffaq_category'))){
			   if ($this->tabAccess['edit'] === '1'){
					$this->validateRules();
					$id = (int)(Tools::getValue($this->identifier));
					if (isset($id) AND $id){
							if (!sizeof($this->errors)){ 
								$object = new $this->className($id);
								$this->copyFromPost($object, $this->table);
								if ( $object->update() ){ 
									$parent_id = (int)(Tools::getValue('id_parent', 1));
									if(!is_dir(_PS_IMG_DIR_.'faq/')) 
										mkdir(_PS_IMG_DIR_.'faq/', 0777);
									$this->uploadImage($object->id, 'image', 'faq/');
									
									$this->postImage($object->id);  
									// Save and stay on same form
									if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
										Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=3&update'.$this->table.'&token='.$token);
									// Save and back to parent
									if (Tools::isSubmit('submitAdd'.$this->table.'AndBackToParent'))
										Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$parent_id.'&conf=3&token='.$token);
									// Default behavior (save and back)
									Tools::redirectAdmin($currentIndex.($parent_id ? '&'.$this->identifier.'='.$object->id : '').'&conf=3&token='.$token);
								}
								
							}
							
						}
					}else
					$this->errors[] = Tools::displayError('You do not have permission to edit here.');
				}
				else
				{
					if ($this->tabAccess['add'] === '1'){
						$object = new $this->className();
						$this->copyFromPost($object, $this->table);
						if ($object->add())
						{
							$parent_id = (int)(Tools::getValue('id_parent', 1));
							if(!is_dir(_PS_IMG_DIR_.'faq/')) 
								mkdir(_PS_IMG_DIR_.'faq/', 0777);
							//$this->uploadImage($object->id, 'image', 'faq/');
							if (($id_loffaq_category = (int)($object->id)) AND isset($_FILES) AND sizeof($_FILES) AND $_FILES['image']['name'] != NULL )
								{
									 $filename = _PS_IMG_DIR_.'faq/'. $id_loffaq_category . ".jpg";
									 if(!move_uploaded_file($_FILES['image']['name'], $filename)) {      
									 }   
									foreach ($this->imagesTypes AS $k => $imageType)
										ImageManager::resize(_PS_IMG_DIR_.'faq/'.$id_loffaq_category.'.jpg', _PS_IMG_DIR_.'faq/'.$id_loffaq_category.'-'.stripslashes($imageType['name']).'.jpg', (int)($imageType['width']), (int)($imageType['height']));
								}
							// Save and stay on same form
							if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=3&update'.$this->table.'&token='.$token);
							// Save and back to parent
							if (Tools::isSubmit('submitAdd'.$this->table.'AndBackToParent'))
								Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$parent_id.'&conf=3&token='.$token);
							// Default behavior (save and back)
							Tools::redirectAdmin($currentIndex.($parent_id ? '&'.$this->identifier.'='.$object->id : '').'&conf=3&token='.$token);
						}
					}
					else
						$this->errors[] = Tools::displayError('You do not have permission to add here.');
				}
		}
        /* Delete object */
		elseif (isset($_GET['delete'.$this->table]))
		{ 
			if ($this->tabAccess['delete'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()) AND isset($this->fieldImageSettings)) {
                    $this->deleteImage($object->id);
					$id = $object->id;
					if ($object->delete()){
						// process code to delete
						Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token'));
					}
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
        /* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.'Box']))
				{
					$object = new $this->className();
                    foreach(Tools::getValue($this->table.'Box') as $id) {
                        $this->deleteImage($id);
		            }
					if (isset($object->noZeroObject) AND
						// Check if all object will be deleted
						(sizeof(call_user_func(array($this->className, $object->noZeroObject))) <= 1 OR sizeof($_POST[$this->table.'Box']) == sizeof(call_user_func(array($this->className, $object->noZeroObject)))))
						$this->errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						$result = true;
						if ($this->deleted)
						{
						  
							foreach(Tools::getValue($this->table.'Box') as $id)
							{
								$toDelete = new $this->className($id);
								$toDelete->deleted = 1;
								$result = $result AND $toDelete->update();
							}
						}
						else
							$result = $object->deleteSelection(Tools::getValue($this->table.'Box'));

						if ($result)
							Tools::redirectAdmin($currentIndex.'&conf=2&token='.$token);
						$this->errors[] = Tools::displayError('An error occurred while deleting selection.');
					}
				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}elseif( Tools::isSubmit('action') == 'updatePositions' ){
			$id_category_to_move = (int)(Tools::getValue('id'));
			$way = (int)(Tools::getValue('way'));
			$positions = Tools::getValue('loffaq_category');
			if (is_array($positions))
				foreach ($positions as $key => $value)
				{
					$pos = explode('_', $value);
					if ((isset($pos[1])) && ($pos[2] == $id_category_to_move))
					{
						$position = $key + 1;
						break;
					}
				}

			$category = new LofFaqCategory($id_category_to_move);
			if (Validate::isLoadedObject($category))
			{
				if (isset($position) && $category->updatePosition($way, $position))
				{
					die(true);
				}
				else
					die('{"hasError" : true, errors : "Can not update categories position"}');
			}
			else
				die('{"hasError" : true, "errors" : "This category can not be loaded"}');
		}elseif (Tools::isSubmit('statusloffaq_category') && Tools::isSubmit('id_loffaq_category')) {
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->toggleStatus())
						Tools::redirectAdmin($currentIndex.'&conf=5&id_loffaq_category='.(int)$object->id.'&token='.Tools::getValue('token'));
					else
						$this->_errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}elseif (Tools::isSubmit('deleteImage') && Tools::isSubmit('id_loffaq_category')) {
			if ($this->tabAccess['edit'] === '1') {
				if (Validate::isLoadedObject($object = $this->loadObject(true)))
				{
					if ($this->deleteImage($object->id))
						Tools::redirectAdmin($currentIndex.'&conf=5&updateloffaq_category&id_loffaq_category='.(int)$object->id.'&token='.Tools::getValue('token'));
					else
						$this->_errors[] = Tools::displayError('An error occurred while deleting image status.');
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}elseif (Tools::isSubmit('submitBulkdeleteloffaq_category')){
			if ($this->tabAccess['delete'] === '1'){
				if (isset($_POST[$this->table.'Box'])){
					$obj = $this->loadObject();
					$result = true;
					$result = $obj->deleteSelection(Tools::getValue($this->table.'Box'));
					if ($result){
						Tools::redirectAdmin($currentIndex.'&conf=2&token='.Tools::getValue('token'));
					}
					$this->errors[] = Tools::displayError('An error occurred while deleting selection.');
				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		
        parent::postProcess();
	}
    
    protected function postImage($id)
	{       	   
		$ret = parent::postImage($id);
		if (($id_loffaq_category = (int)(Tools::getValue('id_loffaq_category'))) AND isset($_FILES) AND sizeof($_FILES) AND $_FILES['image']['name'] != NULL AND file_exists(_PS_IMG_DIR_.'faq/'.$id_loffaq_category.'.jpg'))
		{
			
			foreach ($this->imagesTypes AS $k => $imageType)
				ImageManager::resize(_PS_IMG_DIR_.'faq/'.$id_loffaq_category.'.jpg', _PS_IMG_DIR_.'faq/'.$id_loffaq_category.'-'.stripslashes($imageType['name']).'.jpg', (int)($imageType['width']), (int)($imageType['height']));
		}
		return $ret;
	}
	
    public function deleteImage($id){
        $dir = null;
		/* Deleting object images and thumbnails (cache) */
		if (key_exists('dir', $this->fieldImageSettings))
		{
			$dir = $this->fieldImageSettings['dir'].'/';
			if (file_exists(_PS_IMG_DIR_.$dir.$id.'.'.$this->imageType) AND !unlink(_PS_IMG_DIR_.$dir.$id.'.'.$this->imageType))
				return false;
		}
		if (file_exists(_PS_TMP_IMG_DIR_.$this->table.'_'.$id.'.'.$this->imageType) AND !unlink(_PS_TMP_IMG_DIR_.$this->table.'_'.$id.'.'.$this->imageType))
			return false;
		if (file_exists(_PS_TMP_IMG_DIR_.$this->table.'_mini_'.$id.'.'.$this->imageType) AND !unlink(_PS_TMP_IMG_DIR_.$this->table.'_mini_'.$id.'.'.$this->imageType))
			return false;
		$types = $this->imagesTypes;
		foreach ($types AS $imageType)
			if (file_exists(_PS_IMG_DIR_.$dir.$id.'-'.stripslashes($imageType['name']).'.'.$this->imageType) AND !unlink(_PS_IMG_DIR_.$dir.$id.'-'.stripslashes($imageType['name']).'.'.$this->imageType))
				return false;
		return true;
    }
}
