<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads,
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com>
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
include_once(dirname(__FILE__).'/../../classes/LofClassFaq.php');
class AdminLofFaqController extends AdminController{
  
    private $_faqcategory;
    
    public function __construct(){
		global $cookie;
        $this->table = 'loffaq_faq';
	 	$this->className = 'LofClassFaq';
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->lang = true;
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		
        parent::__construct();  
		
		
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		
		$this->_select = 'pl.name AS \'name_product\', CASE a.`is_private` WHEN 1 THEN \''.pSQL($this->l('Private')).'\' ELSE \''.pSQL($this->l('Public')).'\' END AS \'is_private\'';
		$this->_select .= ',CASE a.`type` WHEN 0 THEN \''.pSQL($this->l('Customer create')).'\' ELSE \''.pSQL($this->l('Admin Create')).'\' END AS \'ltype\'';
		$this->_join = 'INNER JOIN `'._DB_PREFIX_.'loffaq_faq_shop` hss ON (a.id_loffaq_faq = hss.id_loffaq_faq AND hss.`id_shop` = '.(int)$id_shop.')
		';
		$this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (a.id_product = pl.id_product AND pl.`id_lang` = '.(int)$this->context->language->id.')
		';
        $this->_stateArray=array(0=>$this->l('Customer create'),
                          1=>$this->l('Admin Create'));
		$this->_stateArray2=array(0=>$this->l('Public'),
                          1=>$this->l('Private'));
		
        $this->fields_list = array(
				'id_loffaq_faq' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 10),
				'question' => array('title' => $this->l('Question'),'align' => 'center','width' => 200),
				//'answer' => array('title' => $this->l('Answer'),'align' => 'center','width' => 100),
				'name_product' => array('title' => $this->l('Product Name'),'align' => 'center','width' => 100),
      
				'email' => array('title' => $this->l('Email'),'align' => 'center','width' => 50),
				'customer_name' => array('title' => $this->l('User Name'),'align' => 'center','width' => 50),
				
				'ltype' => array('title' => $this->l('Type'),'filter_key' => 'a!type','align' => 'center','width' => 50,'type' => 'select','list' => $this->_stateArray,'filter_type' => 'int', 'orderby' => false),
				'is_private' => array('title' => $this->l('Is Private'),'filter_key' => 'a!is_private','align' => 'center','width' => 20,'type' => 'select','list' => $this->_stateArray2,'filter_type' => 'int', 'orderby' => false),
                'active' => array('title' => $this->l('Displayed'), 'active' => 'status', 'align' => 'center','width' => 20, 'type' => 'bool', 'orderby' => false),
                'date_add' => array('title' => $this->l('Date'), 'width' => 35, 'align' => 'right', 'type' => 'datetime', 'filter_key' => 'a!date_add'),
        );
		$this->getlanguages();
		$this->_defaultFormLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
        $this->_faqcategory = AdminLofManageFAQController::getCurrentLofFaqCategory();
        $this->_filter = 'AND a.`id_loffaq_category` = '.(int)($this->_faqcategory->id);
        
    }
	
    public function postProcess($token = null)
	{
        global $cookie, $currentIndex;
        
		//$this->tabAccess = Profile::getProfileAccess($this->context->employee->id_profile, $this->id);
		
        if (Tools::isSubmit('submitAdd'.$this->table) || Tools::isSubmit('submitAdd'.$this->table.'AndStay') ){
            if ((Tools::getValue('id_loffaq_faq') && $this->tabAccess['edit'] === '1') || $this->tabAccess['add'] === '1'){
                $this->submitAddfaq($token);
            }
  			else
  				$this->errors[] = Tools::displayError('You do not have permission to add here.');
        }

        /* Delete object */
		elseif (isset($_GET['delete'.$this->table])){
			if ($this->tabAccess['delete'] === '1'){
				if (Validate::isLoadedObject($object = $this->loadObject())){
					$id = $object->id;
					if ($object->delete()){
						// process code to delete
						Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token'));
					}
					$this->errors[] = Tools::displayError('An error occurred during deletion.');
				}else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}

        /* Delete object */
		elseif (isset($_GET['postProcess'])){
			if ($this->tabAccess['delete'] === '1'){
				if (Validate::isLoadedObject($object = $this->loadObject())){
					$id = $object->id;
					if ($object->delete()){
						// process code to delete
						Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token'));
					}
					$this->errors[] = Tools::displayError('An error occurred during deletion.');
				}else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
        /* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.'Box']))
				{
					$object = new $this->className();

					if (isset($object->noZeroObject) AND
						// Check if all object will be deleted
						(sizeof(call_user_func(array($this->className, $object->noZeroObject))) <= 1 OR sizeof($_POST[$this->table.'Box']) == sizeof(call_user_func(array($this->className, $object->noZeroObject)))))
						$this->errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						$result = true;
						if ($this->deleted)
						{
							foreach(Tools::getValue($this->table.'Box') as $id)
							{
								$toDelete = new $this->className($id);
								$toDelete->deleted = 1;
								$result = $result AND $toDelete->update();
							}
						}
						else
							$result = $object->deleteSelection(Tools::getValue($this->table.'Box'));

						if ($result)
						{
							$id_loffaq_category = Tools::getValue('id_loffaq_category');
							$category_url = empty($id_loffaq_category) ? '' : '&id_loffaq_category='.$id_loffaq_category;

							Tools::redirectAdmin($currentIndex.'&conf=2&token='.$token.$category_url);
						}
						$this->errors[] = Tools::displayError('An error occurred while deleting selection.');
					}
				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}elseif(Tools::isSubmit('statusloffaq_faq') && Tools::getValue('id_loffaq_faq')){
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->toggleStatus())
						Tools::redirectAdmin($currentIndex.'&conf=5'.((int)$object->id_loffaq_category ? '&id_loffaq_category='.(int)$object->id_loffaq_category : '').'&token='.Tools::getValue('token'));
					else
						$this->_errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
		}elseif(Tools::isSubmit('submitBulkdeleteloffaq_faq')){
			if ($this->tabAccess['delete'] === '1'){
				if (isset($_POST[$this->table.'Box'])){
					$obj = new LofClassFaq();
					$result = true;
					$result = $obj->deleteSelection(Tools::getValue($this->table.'Box'));
					if ($result){
						Tools::redirectAdmin($currentIndex.'&conf=2&token='.Tools::getValue('token'));
					}
					$this->errors[] = Tools::displayError('An error occurred while deleting selection.');
					
				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		parent::postProcess();
	}
	
    public function initBreadcrumbs() {
		$this->breadcrumbs[] = $this->l('FAQ');
	}
	
	public function renderList() {
		$this->initToolbar();
		return parent::renderList();
	}
    
    public function renderForm($firstCall = true)
	{
	    
		global $currentIndex, $cookie;
		if (!($obj = $this->loadObject(true)))
			return;
		if (Validate::isLoadedObject($this->object))
			$this->display = 'edit';
		else
			$this->display = 'add';
		$this->initToolbar();
		
		    if ($id_category_back = (int)(Tools::getValue('id_loffaq_category')))
			$currentIndex .= '&id_loffaq_category='.$id_category_back;
    	if (!($obj = $this->loadObject(true))){
            return;
   		}
		if ($obj->id)
			$currentIndex .= '&id_loffaq_faq='.$obj->id;
		
        $id_loffaq_category = Tools::getValue("id_loffaq_category");
		if($obj->id_loffaq_category)
			$id_loffaq_category = $obj->id_loffaq_category;
		$active = $this->getFieldValue($obj, 'active');
        $isprivate = $this->getFieldValue($obj, 'is_private');
        $type = $this->getFieldValue($obj, 'type');
        // TinyMCE 
		$context = Context::getContext();
		$context->controller->addCSS(array(
			_PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css'
		));
		$context->controller->addJqueryUI(array(
			'ui.datepicker'
		));
		$context->controller->addJS(array(
				_PS_JS_DIR_.'tiny_mce/tiny_mce.js',
				_PS_JS_DIR_.'tinymce.inc.js',
				_PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.js'
			));
		$iso = Language::getIsoById((int)($this->context->language->id));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		$selectedCat = LofFaqCategory::getFAQCategories();
		$type = array(
                        array("value"=>1,
                            "name"=>$this->l('Admin Create')),
                        array("value"=>0,
                            "name"=>$this->l('Customer Create'))
                    );
		$this->fields_form = array(
			'tinymce' => false,
			'legend' => array(
				'title' => $this->l('FAQ'),
				'image' => '../img/admin/quick.gif'
			),
			'input' => array(
				 array(
					'type' => 'select',
					'label' => $this->l('FAQ\'s Category'),
					'name' => 'id_loffaq_category',
					'options' => array(
						'query' => $selectedCat,
                        'id' => 'id_loffaq_category',
                        'name' => 'name'
					),
				),
				array(
					'type' => 'textarea',
					'label' => $this->l('Question:'),
					'name' => 'question',
					'autoload_rte' => true,
					'lang' => true,
					'required' => true,
					'id'   => 'question',
					'rows' => '5',
					'cols' => '48',
				),
				array(
					'type' => 'textarea',
					'label' => $this->l('Answer:'),
					'name' => 'answer',
					'autoload_rte' => true,
					'lang' => true,
					'required' => true,
					'id'   => 'answer',
					'rows' => '5',
					'cols' => '48',
				),
				 array(
					'type' => 'radio',
					'label' => $this->l('Active:'),
					'name' => 'active',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						    )
				    	),
				),
				
				 array(
					'type' => 'select',
					'label' => $this->l('Type'),
					'name' => 'type',
					'options' => array(
						'query' => $type,
                        'id' => 'value',
                        'name' => 'name'
					),
				),
				
				 array(
					'type' => 'radio',
					'label' => $this->l('Is Private:'),
					'name' => 'is_private',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'is_private_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id' => 'is_private_off',
							'value' => 0,
							'label' => $this->l('No')
						    )
				    	),
				),
				
				
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);
		
		$this->tpl_form_vars = array(
			'active' => $this->object->active,
			'PS_ALLOW_ACCENTED_CHARS_URL', (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL')
		);
        return parent::renderForm();	
	}


    public function submitAddfaq($token = null){
        global $cookie, $currentIndex, $link;
        $this->validateRules();
        if (!sizeof($this->errors) || Tools::isSubmit('statusloffaq_faq'))
		{
		    $id = (int)(Tools::getValue('id_'.$this->table));
			$tagError = true;
            /* Update an existing faq */
            if (isset($id) AND !empty($id))
			{
			    $object = new $this->className($id);
				if (Validate::isLoadedObject($object))
				{
				    $this->copyFromPost($object, $this->table);
				    if ($object->update())
					{
						$parent_id = (int)(Tools::getValue('id_parent', 1));
      					if (Tools::isSubmit('submitAdd'.$this->table.'AndStay')) // ko vao dc day^
  	    				{
						
                            Tools::redirectAdmin($currentIndex.'&id_loffaq_faq='.$object->id.'&id_loffaq_category='.(!empty($_REQUEST['id_loffaq_category'])?$_REQUEST['id_loffaq_category']:'1').'&addloffaq_faq&conf=4&tabs='.(int)(Tools::getValue('tabs')).'&token='.($token ? $token : $this->token));
  	    				}
						else{
							Tools::redirectAdmin($currentIndex.($parent_id ? '&'.$this->identifier.'='.$object->id : '').'&conf=3&token='.$token);
						}
							// Save and back to parent
					}
					else
						$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> ('.Db::getInstance()->getMsgError().')';
				}
                else
					$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> ('.Tools::displayError('Cannot load object').')';

            }

			else
			{
			    $object = new $this->className();
                $this->copyFromPost($object, $this->table);
				if ($object->add())
				{
					Tools::redirectAdmin($currentIndex.'&id_loffaq_category='.(!empty($_REQUEST['id_loffaq_category'])?$_REQUEST['id_loffaq_category']:'1').'&conf=3&token='.$token);
				}else{
				    $this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.'</b>';
				}
			}

        }
		
    }
}
?>