<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
include_once(dirname(__FILE__).'/AdminLofFaq.php');
include_once(dirname(__FILE__).'/AdminLofFaqCategory.php');
class AdminLofManageFAQController extends AdminController
{
	/** @var object adminCMSCategories() instance */
	protected $adminFaqCategories;

	/** @var object adminCMS() instance */
	protected $adminFaq;

	/** @var object Category() instance for navigation*/
	protected static $_faqcategory = null;
	
	public function __construct()
	{
		/* Get current category */
		$id_loffaq_category = (int)Tools::getValue('id_loffaq_category', Tools::getValue('id_loffaq_category_parent', 1));
		self::$_faqcategory = new LofFaqCategory($id_loffaq_category);
		if (!Validate::isLoadedObject(self::$_faqcategory))
			die('Category cannot be loaded');
		
		//$this->table = array("loffaq_faq", "loffaq_category");
		$this->table = 'loffaq_faq';
		$this->className = 'LofManageClassFaq';
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		
		$this->adminFaqCategories = new AdminLofFaqCategoryController();
		$this->adminFaq = new AdminLofFaqController();

		parent::__construct();
	}
	/**
	 * Return current category
	 *
	 * @return object
	 */
	public static function getCurrentLofFaqCategory()
	{
		return self::$_faqcategory;
	}

	public function viewAccess($disable = false)
	{
		$result = parent::viewAccess($disable);
		$this->adminFaqCategories->tabAccess = $this->tabAccess;
		$this->adminFaq->tabAccess = $this->tabAccess;
		return $result;
	}
	
	public function initContent()
	{
		$this->adminFaqCategories->token = $this->token;
		$this->adminFaq->token = $this->token;
		$this->viewAccess();
		if ($this->display == 'edit_loffaq_category')
			$this->content .= $this->adminFaqCategories->renderForm();
		else if ($this->display == 'edit_loffaq_faq')
			$this->content .= $this->adminFaq->renderForm();
		else
		{
			$id_loffaq_category = (int)Tools::getValue('id_loffaq_category');
			if (!$id_loffaq_category)
				$id_loffaq_category = 1;

			// CMS categories breadcrumb
			$faq_tabs = array("loffaq_faq", "loffaq_category");
			// Cleaning links
			$cat_bar_index = self::$currentIndex;
			foreach ($faq_tabs as $tab)
				if (Tools::getValue($tab.'Orderby') && Tools::getValue($tab.'Orderway'))
					$cat_bar_index = preg_replace('/&'.$tab.'Orderby=([a-z _]*)&'.$tab.'Orderway=([a-z]*)/i', '', self::$currentIndex);

			$this->content .= $this->adminFaqCategories->renderList();
			$this->adminFaq->id_loffaq_category = $id_loffaq_category;
			$this->content .= $this->adminFaq->renderList();
			$this->context->smarty->assign(array(
				'faq_breadcrumb' => getPath($cat_bar_index, $id_loffaq_category, '', '', 'faq'),
			));
		}

		$this->context->smarty->assign(array(
			'content' => $this->content
		));
	}

	public function postProcess()
	{
		$this->viewAccess();
		if (Tools::isSubmit('submitDelloffaq_faq')
			|| Tools::isSubmit('previewSubmitAddcmsAndPreview')
			|| Tools::isSubmit('submitAddloffaq_faq')
			|| Tools::isSubmit('submitBulkdeleteloffaq_faq')
			|| Tools::isSubmit('deleteloffaq_faq')
			|| Tools::isSubmit('viewloffaq_faq')
			|| (Tools::isSubmit('statusloffaq_faq') && Tools::isSubmit('id_loffaq_faq'))
			|| (Tools::isSubmit('way') && Tools::isSubmit('id_loffaq_faq')) && (Tools::isSubmit('position'))){
			$this->adminFaq->postProcess($this->token);
			}
		elseif (Tools::isSubmit('submitDelloffaq_category')
			|| Tools::isSubmit('submitAddloffaq_categoryAndBackToParent')
			|| Tools::isSubmit('submitBulkdeleteloffaq_category')
			|| Tools::isSubmit('submitAddloffaq_category')
			|| Tools::isSubmit('deleteloffaq_category')
			|| (Tools::isSubmit('statusloffaq_category') && Tools::isSubmit('id_loffaq_category'))
			|| (Tools::isSubmit('deleteImage') && Tools::isSubmit('id_loffaq_category'))
			|| (Tools::getValue('action') == 'updatePositions')
			|| (Tools::isSubmit('position') && Tools::isSubmit('id_loffaq_category_to_move'))){
				$this->adminFaqCategories->postProcess($this->token);
		}
		if (((Tools::isSubmit('submitAddloffaq_category') || Tools::isSubmit('submitAddloffaq_categoryAndStay')) && count($this->adminFaqCategories->errors))
			|| Tools::isSubmit('updateloffaq_category')
			|| Tools::isSubmit('addloffaq_category'))
			$this->display = 'edit_loffaq_category';
		else if (((Tools::isSubmit('submitAddloffaq_faq') || Tools::isSubmit('submitAddloffaq_faqAndStay')) && count($this->adminFaq->errors))
			|| Tools::isSubmit('updateloffaq_faq')
			|| Tools::isSubmit('addloffaq_faq')){
			$this->display = 'edit_loffaq_faq';
			}
		elseif (isset($_GET['position']))
			{
				if ($this->tabAccess['edit'] !== '1')
					$this->_errors[] = Tools::displayError('You do not have permission to edit here.');
				elseif (!Validate::isLoadedObject($object = new LofFaqCategory((int)(Tools::getValue($this->identifier, Tools::getValue('id_loffaq_category_to_move', 1))))))
					$this->_errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
				if (!$object->updatePosition((int)(Tools::getValue('way')), (int)(Tools::getValue('position'))))
					$this->_errors[] = Tools::displayError('Failed to update the position.');
				else{
					Tools::redirectAdmin(self::$currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_loffaq_category = (int)Tools::getValue($this->identifier, Tools::getValue('id_category_parent', 1))) ? ('&'.$this->identifier.'='.$id_loffaq_category) : '').'&token='.Tools::getValue('token'));
				}
			}	
		else
		{
			$this->display = 'list';
			$this->id_loffaq_category = (int)Tools::getValue('id_loffaq_category');
		}

		if (isset($this->adminFaq->errors))
			$this->errors = array_merge($this->errors, $this->adminFaq->errors);

		if (isset($this->adminFaqCategories->errors))
			$this->errors = array_merge($this->errors, $this->adminFaqCategories->errors);

		parent::postProcess();
	}

	public function setMedia()
	{
		parent::setMedia();
		$this->addJqueryUi('ui.widget');
		$this->addJqueryPlugin('tagify');
	}
	
}