<div id="lof-messages">
    <ul class="messages">
        {if !empty($postSuccess)}
        <li class="success-msg" style="display:block;">
            <ul>
                <li>{$postSuccess}</li>
            </ul>
        </li>
        {/if}
        {if !empty($postErrors)}
        <li class="error-msg" style="display:block;">
            <ul>
                {foreach from=$postErrors item=loffaq_error}
                <li>{$loffaq_error}</li>
                {/foreach}
            </ul>
        </li>
        {/if}
    </ul>
</div>
<div class="clr"></div>

<div class="lof-faq-box"> 
    <h3 id="lof-faqsubmit-box">{l s='Add Question' mod='loffaq'}</h3>
    <div class="lof-faq-submit content" id="lof-faqsubmit-contentbox">
        <h4 class="title"></h4>
        <form id="loffaq-submit" method="post" action="{$link->getModuleLink('loffaq','views',['process' => 'view'])}">
            {if $isguest eq '1'}
            <div class="field">
                <h4>{l s='Name' mod='loffaq'}</h4>
                <input type="text" id="loffaq_name" name="loffaq_name"/> 
                <br />
                <span id="loffaq_name_info"></span> 
            </div>
            <div class="field">
                <h4>{l s='Email' mod='loffaq'}</h4>
                <input type="text" id="loffaq_email" name="loffaq_email"/>
                <br />
                <span id="loffaq_email_info"></span> 
            </div>
            {/if}
            <div class="field">
                <h4>{l s='Select a Category' mod='loffaq'}</h4>
                <select id="loffaq_category" name="loffaq_category">
                    <option value="0">---{l s='Select one' mod='loffaq'}---</option>
                    {foreach from=$faqcategories item=loffaqcat}
                        <option value="{$loffaqcat.id_loffaq_category}" {if $loffaqcat.id_loffaq_category == $id_cat}selected="selected"{else}{/if}>{$loffaqcat.name}</option>
                    {/foreach}
                </select>
                <br />
                <span id="loffaq_category_info"></span>
            </div>
            <div class="field">
                <h4>{l s='Question' mod='loffaq'}</h4>
                <textarea id="loffaq_question" cols="65" rows="10" name="loffaq_question"></textarea>
                <br />
                <span id="loffaq_question_info"></span>
            </div>
            {if $is_private eq '1'}
            <div class="field">
                <h4>{l s='Private FAQ' mod='loffaq'}</h4>
                <input type="radio" name="is_private" value="1" checked="checked" /> {l s='Yes' mod='loffaq'}&nbsp;
                <input type="radio" name="is_private" value="0" checked="checked"  /> {l s='No' mod='loffaq'}
            </div>
            {/if}
            {if $iscaptcha eq '1'}
            <div class="code">
                <h4>{l s='Security Code' mod='loffaq'}</h4>
                <table>
                    <tr>
                        <td>
                            <input type="text" name="loffaq_captcha" value="" id="loffaq_captchainput"/>
                        </td>
                        <td>
                            <img src="{$site_url}modules/{$module_name}/captcha.php?rand={$rand}" id="loffaq_captcha"/>
                            <div id="loffaq_refresh" href="javascript:void(0)" title="{l s='refresh' mod='loffaq'}">&nbsp;</div>
                        </td>
                    </tr>
                </table>
                <span id="loffaq_captchainput_info"></span>
            </div>
            {/if}
            <div class="box_btn">
				<input type="hidden" value="{if isset($id_product)}{$id_product}{else}0{/if}" name="id_product"/>
                <input class="loffaq-btn" type="submit" title="" name="loffaq_send" value="{l s='Send' mod='loffaq'}"/>
                &nbsp;&nbsp;&nbsp; 
				
                <input class="loffaq-btn" type="reset" title="" name="loffaq_reset" value="{l s='Reset' mod='loffaq'}"/>
            </div>
        </form>
    </div>
</div>