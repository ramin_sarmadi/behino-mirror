<script type="text/javascript">
    function makeHelpfull(id_cat,id_faq, yes_no){
        $.post(baseDir + 'modules/loffaq/ajax.php', {
    		action:'helpfull',
    		id_faq: id_faq,
            yes_no: yes_no
        },
        function (data) {
    		if (data.status == 'success') {
                setCookie("helpful-" + id_cat + "-" + id_faq, "helpful-" + id_cat + "-" + id_faq,1);
                $("#helpful-" + id_cat + "-" + id_faq).html('');    
    		} else {
    			alert(data.message);
    		}
        }, 'json');
    }
    //get Cookie
    function getCookie(c_name){
    	var i,x,y,ARRcookies=document.cookie.split(";");
    	for (i=0;i<ARRcookies.length;i++)
    	{
    		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
    		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
    		x=x.replace(/^\s+|\s+$/g,"");
    		if (x==c_name)
    		{
    			return unescape(y);
    		}
    	}
    }
    //set Cookie
    function setCookie(c_name,value,exdays)
    {
    	var exdate=new Date();
    	exdate.setDate(exdate.getDate() + exdays);
    	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    	document.cookie=c_name + "=" + c_value;
    }

    $(document).ready(function() {
        $('.faq-question').click(function() {
            var id = $(this).attr("id");
            $('#faq-answer-' + id).slideToggle('slow');
            return;
        });
        $('#lof-faqlist-box').click(function() {
            $('#lof-faqlist-contentbox').slideToggle('slow');
            return;
        });
        
        $(".helpful").each(function (index) {
    	     var id_like = $(this).attr("id");
             if(getCookie(id_like) != null || getCookie(id_like) !== undefined  ){
                $("#"+id_like).html(''); 
             }
        });
        //rating
        $('.lof-stars').jRating({
			phpPath : '<?php echo $site_url;?>modules/loffaq/ajax.php'
        });
    });
</script>
<?php
	echo isset($initjs)?$initjs:"";
?>
<div class="lof-faq-box"> 
    <h3 id="lof-faqlist-box" style="color: red;"></a><?php echo $l['Found'].' '.$countFaq.' '.$l['record(s) FAQ']?></h3>
    <div class="content" id="lof-faqlist-contentbox">
		<?php if($countFaq == '0'){ ?><div class="faq-count"><?php echo $l['No record(s) found']; ?></div> <?php } ?>

		<?php foreach($faqs as $loffaqcat){ ?>
        <?php if(!empty($loffaqcat['faq'])){?>
		<script type="text/javascript">
			$(document).ready(function () {
				$('#black-<?php echo $loffaqcat["id_loffaq_category"];?>').smartpaginator({ 
					totalrecords: <?php echo sizeof($loffaqcat['faq']); ?>, 
					recordsperpage: <?php echo $limit_question; ?>, 
					datacontainer: 'divs-<?php echo $loffaqcat["id_loffaq_category"];?>', 
					dataelement: 'span', 
					length: 4,
					initval: 0, 
					next: 'Next', 
					prev: 'Prev', 
					first: 'First', 
					last: 'Last', 
					theme: 'black' 
				});
			});
		</script>
		<h4 class="faq-name"><?php echo $loffaqcat['name'];?></h4>
		<div id="divs-<?php echo $loffaqcat["id_loffaq_category"];?>" class="faq-item">
			<?php foreach($loffaqcat['faq'] as $loffaq){ ?>
			<span class="lof-item-box"> 
				<?php if($enable_gravatar || $show_username || $show_createdate || $show_helpfulness || $enable_rating){?>
                <div class="lof-info">
                    <?php if($enable_gravatar){ ?><img src="<?php echo $loffaq['grav_url'];?>" width="30px" height="30px"/><?php }?>
                    <?php if($show_username){ ?><div class="name"><?php echo $loffaq['customer_name'];?></div><?php }?>
                    <?php if($show_createdate){ ?><div class="date"><?php echo '('.date('m/d/Y', strtotime($loffaq['date_add'])).')';?></div><?php }?>
                    <br />
                    <!-- show_helpfulness -->
                    <?php if($show_helpfulness){ ?>
                    <?php $total_like_count = (int)$loffaq["unlike"] + (int)$loffaq["like"];?>
                    <div class="help-text"><?php echo '(' . $loffaq["like"]. $l["out of"] . $total_like_count . $l["people find this question interesting"].')'; ?></div>
                    
                    <div id="helpful-<?php echo $loffaq['id_loffaq_category'].'-'.$loffaq['id_loffaq_faq'];?>" class="helpful">
                        <a href="javascript:void(0)" class="helpful-link-yes" onclick="makeHelpfull(<?php echo $loffaq["id_loffaq_category"]?>,<?php echo $loffaq["id_loffaq_faq"]?>,1);" title="<?php echo $l['Like']?>"></a>
                        <a href="javascript:void(0)" class="helpful-link-no" onclick="makeHelpfull(<?php echo $loffaq["id_loffaq_category"]?>,<?php echo $loffaq["id_loffaq_faq"]?>,0);" title="<?php echo $l['UnLike']?>"></a>
                    </div>
                    <?php }?>
                    <!-- rating -->
                    <?php if($enable_rating){?>
                    <?php
                        if($loffaq["rating_sum"] == 0 || $loffaq["rating_count"] ==0 )
                            $rating_avg = 0;
                        else
                            $rating_avg = (int)$loffaq["rating_sum"] / (int)$loffaq["rating_count"];    
                    ?>
                    <div class="lof-rating-box">
                    	<div class="lof-stars" id="<?php echo $rating_avg.'_'.$loffaq['id_loffaq_faq'];?>"></div>
                    </div>
                    <?php }?>
                </div>
                <?php }?>
				<div class="clr"></div>
                <h5 class="faq_question">
					<div class="img-question"></div>
                    <div class="faq-question" id="<?php echo $loffaq['id_loffaq_faq'];?>" >
						<?php echo $loffaq['question'];?> 
					</div>
                </h5>
				<div id="faq-answer-<?php echo $loffaq['id_loffaq_faq'];?>" class="faq_answer" style="display:none;"><?php echo $loffaq['answer'];?></div>
			</span>
			<?php } ?>
		</div>
		<div id="black-<?php echo $loffaqcat["id_loffaq_category"];?>" style="margin: auto;"></div>
		<div class="lof-spacer"></div>
		<?php   } ?> 
        <?php } ?>
	</div>
</div>