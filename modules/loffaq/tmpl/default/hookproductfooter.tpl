{if $enable_pdetail}
<div id="idTabLofFaq" class="loffaq_tabContent">
    
       <div class="lof-faq-box">
            <h3 id="lof-faqsearch-box"></h3>
            <div id="lof-faqsearch-contentbox" class="content">
                <div class="search">
                    <label class="" for="loffaq_search">{l s='Search FAQ' mod="loffaq"}:</label>
                    <input class="input-text" type="text" name="loffaq_search" maxlength="50" size="50"/>&nbsp;&nbsp; 
                    <a href="javascript:void(0);" class="loffaq-btn" onclick="faq_search({$id_product});">{l s="Search FAQ" mod="loffaq"}</a>
					<span class="search_loading" id="search_loading">{l s='Waiting' mod='loffaq'}</span>
                </div>
                <div class="clr"><a href="javascript:void(0);" id="form-toggle">{l s='Submit a Question' mod="loffaq"}</a></div>
            </div>
        </div>
        
        <div class="lof-faq">
            {$html_faqs}
        </div>
        <div class="clr"></div>
        
        {if $isguest}
            {if $allow_guest}
            {include file="{$formsubmit}"}
            {/if}
        {else}
        {include file="{$formsubmit}"}
        {/if}
</div>
{/if}
