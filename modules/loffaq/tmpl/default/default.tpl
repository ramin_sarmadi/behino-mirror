<h1>{$loffaq_title}</h1>

<div class="lof-faq-box"> 
    <h3 id="lof-faqcat-box">{l s='FAQ\'s Category Center' mod='loffaq'}</h3>    
    <div id="lof-faqcat-contentbox" class="cat-item" style="height: auto; opacity: 1;">
        <div class="content">
            {if empty($faqcategory)}
                {l s='No faq\'s category items.' mod='loffaq'}
            {else}
            <ul>
                {foreach from=$faqcategory item=loffaq_cat name=faqcategory}
                {if $smarty.foreach.faqcategory.index < $limit_items_faqcat}
                <li>
                    {if $show_catimage eq '1'}
                    <div class="img">
                        <a title="" href="{$link->getModuleLink('loffaq','views',['process' => 'view','cat'=>{$loffaq_cat.id_loffaq_category}])}">
                            <img src="{$loffaq_cat.img_link}" alt="" title="{$loffaq_cat.name}" width="{$cat_width}px" height="{$cat_height}px"/>
                        </a>
                    </div>
                    {/if}
                    <div class="text">
                        <a title="" href="{$link->getModuleLink('loffaq','views',['process' => 'view','cat'=>{$loffaq_cat.id_loffaq_category}])}">
                            {$loffaq_cat.name}
                        </a>
                    </div>
                </li>
                {/if}
                {/foreach}
                  
            </ul>
            {/if}
            <div class="clr"></div>
        </div>
    </div>
</div>


<div class="lof-faq-box">
    <h3 id="lof-faqsearch-box">{l s='Search FAQ' mod='loffaq'}</h3>
    <div id="lof-faqsearch-contentbox" class="content">
        <div class="search">
            <label class="" for="loffaq_search">{l s='Search FAQ' mod='loffaq'}:</label>
            <input class="input-text" type="text" name="loffaq_search" maxlength="50" size="50"/>&nbsp;&nbsp; 
            <a href="javascript:void(0);" class="loffaq-btn" onclick="faq_search(0);">{l s='Search FAQ' mod='loffaq'}</a>
			<span class="search_loading" id="search_loading">{l s='Waiting' mod='loffaq'}</span>
        </div>
        <div class="clr"><a href="javascript:void(0);" id="form-toggle">{l s='Submit a Question' mod='loffaq'}</a></div>
    </div>
</div>

<div class="lof-faq">
    {$html_faqs}
</div>
<div class="clr"></div>

{if $isguest}
    {if $allow_guest}
    <div id="submit-box" >
        {include file="{$formsubmit}"}
    </div>
    {/if}
{else}
<div id="submit-box" >
    {include file="{$formsubmit}"}
</div>
{/if}