{if $enable_bCat}
<div class="block">
    <h4>
        <a href="{$link->getModuleLink('loffaq','views',['process' => 'view'])}">{l s='FAQ\'s Category' mod='loffaq'}</a>
    </h4>
    <div class="block_content">
        <ul class="bullet">
            {if empty($faqcategory)}
            <li>{l s='No faq\'s category items.' mod='loffaq'}</li>
            {/if}
            {foreach from=$faqcategory item=loffaq_cat name=faqcategory}
            {if $smarty.foreach.faqcategory.index < $limit_items_faqcat}
            <li>
                <!--<img src="{$loffaq_cat.img_link}" width="30px" height="30px"/>-->
                <a title="" href="{$link->getModuleLink('loffaq','views',['process' => 'view','cat'=>{$loffaq_cat.id_loffaq_category}])}">
                    {$loffaq_cat.name}
                </a>
            </li>
            {/if}
            {/foreach}        
        </ul>
    </div>
</div>
{/if}
{if $enable_bRen}
<div class="block">
    <h4>
        <a href="{$link->getModuleLink('loffaq','views',['process' => 'view'])}">{$recent_title}</a>
    </h4>
    <div class="block_content">
        <ul class="bullet">
            {if empty($recentfaq)}
            <li>{l s='No faq items.' mod='loffaq'}</li>
            {/if}
            {foreach from=$recentfaq item=loffaq_recent name=recentfaq}
            {if $smarty.foreach.recentfaq.index < $limit_items_recent}
            <li>
                <a title="" href="javascript:void(0);">{$loffaq_recent.question}</a>
            </li>
            {/if}
            {/foreach}
        </ul>
    </div>
</div>
{/if}
