<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers-home_3cb29f0ccc5fd220a97df89dafe46290'] = 'پرفروش‌ترین‌ها';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers-home_d3da97e2d9aee5c8fbe03156ad051c99'] = 'بیشتر';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers-home_4351cfebe4b61d8aa5efa1d020710005'] = 'نمایش';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers-home_eae99cd6a931f3553123420b16383812'] = 'همه محصولات پرفروش';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers-home_adc570b472f54d65d3b90b8cee8368a9'] = 'درحال حاضر محصول پرفروشی وجود ندارد';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_ee520a08f4b3f527b11c4316aa399e84'] = 'بخش پرفروشترینها';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_4f6db60ed6f1c8555648014427822faf'] = 'افزودن یک بلوک جهت نمایش پرفروشترین محصولات.';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات به روز رسانی شد';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_f4f70727dc34561dfde1a3c529b6205c'] = 'تنظیمات';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_41385d2dca40c2a2c7062ed7019a20be'] = 'نمایش دائمی این بلوک';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'فعال';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_b9f5c797ebbf55adccdd8539a65a0241'] = 'غیر فعال';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_53d61d1ac0507b1bd8cd99db8d64fb19'] = 'نمایش این بلوک حتی اگر محصولی در آن نباشد.';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_3cb29f0ccc5fd220a97df89dafe46290'] = 'پرفروشترینها';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_eae99cd6a931f3553123420b16383812'] = 'تمام محصولات پرفروش';
$_MODULE['<{blockbestsellers}prestashop>blockbestsellers_f7be84d6809317a6eb0ff3823a936800'] = 'هم اکنون هیچ محصول پرفروشی وجود دارد';
