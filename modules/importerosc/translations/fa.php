<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{importerosc}prestashop>importerosc_e273168c3697c35b8c737d14b1a5fb26'] = 'وارد سازي osCommerce';
$_MODULE['<{importerosc}prestashop>importerosc_f4a344f88bab7b04a73077bafc8a0187'] = 'اين ماژول اجازه ورود از osCommerce به Prestashop را ب شما مي دهد';
$_MODULE['<{importerosc}prestashop>importerosc_a410107bacc21de421c07f0162f40878'] = 'زبان پيش فرض osCommerce:';
$_MODULE['<{importerosc}prestashop>importerosc_6325b420c7e3747295ebc4265ca5ebf5'] = 'واحد پول پيش فرض osCommerce:';
$_MODULE['<{importerosc}prestashop>importerosc_3128e575f57c8bff6950eb946748318a'] = 'URL فروشگاه:';
$_MODULE['<{importerosc}prestashop>importerosc_f106e79e646e4262fae765f77db9ca91'] = 'URL ريشه را براي سايت oscommerce خود تعيين نماييد';
$_MODULE['<{importerosc}prestashop>importerosc_a89a64592edf58aee0fc749735902cea'] = 'لطفآ زبان پيش فرضي را برگزينيد';
$_MODULE['<{importerosc}prestashop>importerosc_375e6e17d4bd18a5163d3a7d13b80b4b'] = 'لطفآ واحد پول پيش فرضي را برگزينيد';
$_MODULE['<{importerosc}prestashop>importerosc_294f0969b5f80f1bae24a7183eb338d5'] = 'لطفآ URL فروشگاه را برگزينيد';
$_MODULE['<{importerosc}prestashop>importerosc_86545d77ce5790e924190d9b5a7ac1b6'] = 'گروه پيش فرض osCommerce';
$_MODULE['<{importerosc}prestashop>importerosc_9aa1b03934893d7134a660af4204f2a9'] = 'سرور';
$_MODULE['<{importerosc}prestashop>importerosc_770b2f7556eecbe5000cfcbddc9f9885'] = '(نمونه : mysql.mydomain.com)';
$_MODULE['<{importerosc}prestashop>importerosc_8f9bfe9d1345237cb3b2b205864da075'] = 'نام كاربري';
$_MODULE['<{importerosc}prestashop>importerosc_dc647eb65e6711e155375218212b3964'] = 'گذرواژه';
$_MODULE['<{importerosc}prestashop>importerosc_14ae0ea02f571a833786d13d9ca6a897'] = '(گذرواژه مي تواند خالي باشد)';
$_MODULE['<{importerosc}prestashop>importerosc_e307db07b3975fef922a80d07455ee5e'] = 'پايگاه داده';
$_MODULE['<{importerosc}prestashop>importerosc_dac130bdd2c5492a8108a4145bd9f04a'] = 'پيشوند پايگاه داده';
$_MODULE['<{importerosc}prestashop>importerosc_6bdc02625540b5264cffe801c37a82dd'] = '(پيشوند انتخابي است.';
$_MODULE['<{importerosc}prestashop>importerosc_4685343b5e2e0f0fbee63dddafde693f'] = 'شما در حال تلاش براي وارد سازي شاخه‌ها هستيد و ما شناسايي كرديم كه پايگاه داده oscommerce شما داراي فيلد \"مرحله\" در شاخه جدول نمي باشد. شما براي وارد سازي شاخه‌ها مي بايد اين فيلد را داشته باشيد.';
$_MODULE['<{importerosc}prestashop>importerosc_16f35420186575c2a1d9c0b59edf6ad3'] = 'براي اضافه كردن  و برآورد كردن فيلد \"مرحله\" كليك نماييد';
$_MODULE['<{importerosc}prestashop>importerosc_fced104d747e0855ceff3020653104ab'] = 'فيلدهاي مرحله ساخته شده است، مي توانيد ادامه دهيد';
$_MODULE['<{importerosc}prestashop>importerosc_b405d0bebeedbdc1773a44ac36b8ffc4'] = 'وصيه جدي براي گرفتن پشتيبان از پايگاه داده خود قبل از هر اقدامي شده است. آيا قبلا تهيه نموديد؟';
$_MODULE['<{importerosc}prestashop>importerosc_9f95fc55011203d91d50a0ed512f805f'] = 'ALTER TABLE مقدور نيست';
