<script type="text/javascript">
	{literal}
		$.ajax({
			type: 'post',
			url: {/literal}'{$barcode_link_ajax}',
			data: 'task=getImage&id_product={$id_product}&id_shop={$id_shop}&secure_key={$barcode_secure_key}'{literal},
			success: function(data){
				$('input[name=upc]').parent().append(data);
			}
		});
	{/literal}
</script>