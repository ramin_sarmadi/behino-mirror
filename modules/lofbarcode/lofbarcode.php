<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if(!defined('_PS_VERSION_'))
	exit;	
	
include_once(_PS_MODULE_DIR_.'lofbarcode/libs/Params.php');
include_once(_PS_MODULE_DIR_.'lofbarcode/libs/barcode.class.php');

class lofbarcode extends Module
{
	private $_html = '';
	private $_configs = '';
	protected $params = null;
	
	public function __construct() {
		$this->name = 'lofbarcode';
		$this->tab  = 'LandOfCoder';
		$this->version = '1.0';
		$this->author  = 'LandOfCoder';
		$this->module_key  = '93a5e5fc1f457d8d600c8c50113af07a';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		
		parent::__construct();
		$this->displayName = $this->l('Lof Barcode Module');
		$this->description = $this->l('Lof barcode allow generate barcode and display in front-end');
		$this->_prepareForm();
		
		$this->params = new LofNParams( $this, 'LOFBARCODE', $this->_configs );
	}
	
	public function _prepareForm(){
		$this->_configs = array(
			'theme' => 'default',
			'show_barcode' => '1',
			'type' => 'vcard',
			'barcodeheight' => '70',
			'scale' => '2',
			'bgcolor' => '#FFFFFF',
			'barcolor' => '#000000',
			'show_productname' => '1',
			'show_reference' => '1',
			'show_fprice' => '1',
			'show_bprice' => '1',
			'show_weight' => '1',
			'show_width' => '1',
			'show_height' => '1',
			'show_depth' => '1',

			'imgtype' => 'png',
			'dataencode' => ''
		);
	}
	
	public function getParams() {
		return $this->params;
	}
	
	/**
	*	@see Module::install()
	*/
	public function install() {
		/* Adds Module */
		if (parent::install() && $this->registerHook('displayFooterProduct') && $this->registerHook('actionProductSave') && $this->registerHook('displayBackOfficeFooter'))
		{
			/* Sets up configuration */
			$this->getParams()->batchUpdate( $this->_configs );
			/* Creates tables */
			return true;
		}
		return false;
	}
	/**
	 * @see Module::uninstall()
	**/
	public function uninstall() {
		/* Deletes Module */
		if(parent::uninstall())
		{
			/* Unsets configuration */
			$res = $this->getParams()->delete();
			
			return $res;
		}
		return false;
	}
	/**
	* function getContent()
	*/
	public function getContent() {
		$this->_html .= '<h3>'.$this->displayName.' '.$this->l('Configuration').'</h3>';
		/* Validate & process */
		if(Tools::isSubmit('submitBarcode') || Tools::isSubmit('submitGenerate')) {
			if($this->_postValidation()) {
				$this->_postProcess();
			}
			$this->_displayForm();
		} else {
			$this->_displayForm();
		}
		return $this->_html;
	}
	
	private function _displayForm()
	{
		$this->context->controller->addCSS($this->_path.'assets/colorpicker.css');
		$this->context->controller->addJS($this->_path.'assets/js/colorpicker.js');
		$this->context->controller->addJS($this->_path.'assets/js/eye.js');
		$this->context->controller->addJS($this->_path.'assets/js/layout.js');
		$this->context->controller->addJS($this->_path.'assets/js/utils.js');
		$params = $this->params;
		/* Gets Slides */
		require_once( dirname(__FILE__).'/form.php' );
	}
	
	private function _postValidation()
	{
		$errors = array();
		if (Tools::isSubmit('submitBarcode')){
			
		}
		/* Returns if validation is ok */
		return true;
	}
	
	private function _postProcess()
	{
		$errors = array();

		/* Processes Slider */
		if (Tools::isSubmit('submitBarcode')){
			$res = $this->getParams()->batchUpdate( $this->_configs );
			$this->getParams()->refreshConfig(); 
		
			if (!$res)
				$errors .= $this->displayError($this->l('Configuration could not be updated'));
			$this->_html .= $this->displayConfirmation($this->l('Configuration updated'));
		}elseif(Tools::isSubmit('submitGenerate')){
			$this->_html .= $this->generateBarcodeCatId();
		}
		
		if (count($errors))
			$this->_html .= $this->displayError(implode('<br />', $errors));
	}
	
	/**
	* Function generate by select category product
	*/
	private function generateBarcodeCatId(){
		$categories = Tools::getValue('categoryBox');
		$return = true;
		if($categories)
			foreach($categories as $catId){
				$objCat = new Category($catId);
				$catProId = $objCat->getProducts($this->context->language->id, 0,10000, null, null, false, false, false, 1, false);
				foreach($catProId as $objPro){
					$obj = new Product($objPro['id_product'], true, (int)($this->context->language->id), (int)($this->context->shop->id));
					$return &= $this->generateImage($obj);
				}
			}
		$str = '<div class="alert error">'.$this->l('Generate barcode error').'</div>';
		if($return){
			$str = '<div class="conf confirm">'.$this->l('Generate barcode success').'</div>';
		}
		return $str;
	}
	
	private function generateImage($obj){
		
		$encoding = $this->getParams()->get('type');
		$height = $this->getParams()->get('barcodeheight');
		$scale =$this->getParams()->get('scale');
		$bgcolor = $this->getParams()->get('bgcolor');
		$barcolor = $this->getParams()->get('barcolor');
		$imgtype = $this->getParams()->get('imgtype');

		$encodearr = array();
		if(  $this->getParams()->get('show_productname') )
			$encodearr[] = $this->l('Product name: ').$obj->name;
		if( $this->getParams()->get('show_reference') )
			$encodearr[] = $this->l('Product reference: ').$obj->reference;
		$price = $obj->getPrice(true, null, 2, null, false, true);
		$rePrice = $obj->getPrice(true, null, 2, null, false, false);
		if( $this->getParams()->get('show_bprice') && $rePrice != $price)
			$encodearr[] = $this->l('Price before reduction: ').Tools::displayPrice($rePrice);
		if( $this->getParams()->get('show_fprice') )	
			$encodearr[] = $this->l('Final Price: ').Tools::displayPrice($price);
		if( $this->getParams()->get('show_width') )
			$encodearr[] = $this->l('Width: ').$obj->width.$this->l(' in');
		if( $this->getParams()->get('show_height') )
			$encodearr[] = $this->l('Height: ').$obj->height.$this->l(' in');
		if( $this->getParams()->get('show_depth') )
			$encodearr[] = $this->l('Depth: ').$obj->depth.$this->l(' in');
		if( $this->getParams()->get('show_weight') )
			$encodearr[] = $this->l('Weight: ').$obj->weight.$this->l(' lb');

		$encodearr[] = '';
		$encodearr[] = '';
		$encodearr[] = '';
		$encodearr[] = '';
		$encodearr[] = '';
		$encodearr[] = '';
		$encodearr[] = '';
		$encodearr[] = '';
		$encodearr[] = '';
		$encodearr[] = '';
		$encodearr[] = '';
		
		$bar	= new BARCODE();
		$folder = _PS_IMG_DIR_.$this->name."/";
		if(!is_dir($folder))
			mkdir($folder,0755);
		$code = '';
		switch($encoding){
			case 'UPC-A':
				if(!$obj->upc)
					return true;
				$code = $obj->upc;
			break;
			case 'UPC-E':
				if(!$obj->upc)
					return true;
				$code = $obj->upc;
			break;
			case 'UPC-E':
				if(!$obj->upc)
					return true;
				$code = $obj->upc;
			break;
			case 'EAN-8':
				if(!$obj->ean13)
					return true;
				$code = $obj->ean13;
			break;
			case 'EAN-13':
				if(!$obj->ean13)
					return true;
				$code = $obj->ean13;
			break;
			case 'link':
				$link = new Link();
				$code = array();
				$code[] = $link->getProductLink($obj);
			break;
		}

		$filename = $obj->id.'-'.$this->context->shop->id;
		if($encoding=="link")
			$bar->QRCode_save($encoding, $code, $filename, $folder, $height, $scale);
		elseif($encoding=="vcard")
			$bar->QRCode_save($encoding, $encodearr, $filename, $folder, $height, $scale);
		else
			$bar->BarCode_save($encoding, $code, $filename, $folder, $imgtype, $height, $scale, $bgcolor, $barcolor);			
		
		if($bar->_error){
			return false;
		}
		return true;
	}
	
	function hookdisplayBackOfficeFooter($params) {
		if(strtolower(Tools::getValue('controller','barcode')) != 'adminproducts' || !Tools::getValue('id_product'))
			return;
		$theme = $this->getParams()->get('theme','default');
		$tpl = 'themes/default/backend.tpl';
		if( file_exists(dirname(__FILE__)."/themes/".$theme.'/backend.tpl') ){
			$tpl = 'themes/'.$theme.'/backend.tpl';
		}
		$this->smarty->assign(array(
			'barcode_link_ajax' => _MODULE_DIR_.$this->name."/ajax.php",
			'barcode_secure_key' => $this->secure_key,
			'id_product' => (int)Tools::getValue('id_product'),
			'id_shop' => (int)$this->context->shop->id,
		));
		return $this->display(__FILE__, $tpl );
	}
	
	function hookActionProductSave($params) {
		$objProduct = new Product($params['id_product'], true, (int)($this->context->language->id), (int)($this->context->shop->id));
		$return = $this->generateImage($objProduct);
		return $return;
	}
	
	public function hookdisplayFooterProduct()
	{
		$theme = $this->getParams()->get('theme','default');
		if(!$this->getParams()->get('show_barcode'))
			return;
		
		$this->context->controller->addCSS($this->_path.'themes/'.$theme.'/assets/styles.css');
		$id_product = (int)Tools::getValue('id_product');
		$encoding = $this->getParams()->get('type');
		$imgtype = $this->getParams()->get('imgtype');
		if($encoding=="link" || $encoding=="vcard")
			$imgtype = 'png';
		$folder = _PS_IMG_DIR_.$this->name."/";
		if(!file_exists($folder.$id_product.'-'.$this->context->shop->id.'.'.$imgtype))
			return;
			
		$this->smarty->assign(array(
			'link_barcode' => _PS_IMG_.$this->name."/".$id_product.'-'.$this->context->shop->id.'.'.$imgtype,
			'type_barcode' => $encoding,
		));
		$tpl = 'themes/default/default.tpl';
		if( file_exists(dirname(__FILE__)."/themes/".$theme.'/default.tpl') ){
			$tpl = 'themes/'.$theme.'/default.tpl';
		}
		
		return $this->display(__FILE__, $tpl );
	}
	
	public static function getCategoriesFull( $id_lang = null, $categories = array())
	{
		if (!$id_lang)
			$id_lang = Context::getContext()->language->id;
		$ret = array();
		if(!$categories || count($categories) <=0)
			return $ret;
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT cp.`id_category`, cl.`name`, cl.`link_rewrite` 
			FROM `'._DB_PREFIX_.'category` cp
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cp.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').')
			'.Shop::addSqlAssociation('category', 'cp').'
			WHERE cl.`id_lang` = '.(int)$id_lang.($categories ? ' AND  cp.`id_category` IN ('.$categories.')' : '')
		);

		foreach ($row as $val)
			$ret[$val['id_category']] = $val;

		return $ret;
	}
	
	private static function getAllIdProduct($selected_cat = array()){
		/* Add category tree */
		$default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
		$default_category = 0;
		if (Tools::isSubmit('categoryBox'))
			$selected_cat = Category::getCategoryInformations(Tools::getValue('categoryBox', array($default_category)), $default_form_language);
		else
			$selected_cat = self::getCategoriesFull( $default_form_language);	
	}
}






