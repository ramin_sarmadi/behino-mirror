<?php
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include(dirname(__FILE__).'/lofbarcode.php');

$obj = new lofbarcode();
if($obj->secure_key == Tools::getValue('secure_key') && Tools::getValue('task') == 'getImage'){
	$id_product = (int)Tools::getValue('id_product');
	$id_shop = (int)Tools::getValue('id_shop');
	$encoding = $obj->getParams()->get('type');
	$imgtype = $obj->getParams()->get('imgtype');
	if($encoding=="link" || $encoding=="vcard")
		$imgtype = 'png';

	$folder = _PS_IMG_DIR_.$obj->name."/";
	if(!file_exists($folder.$id_product.'-'.$id_shop.'.'.$imgtype))
		die('');
	$str = '<br/><br/><img src="'._PS_IMG_.$obj->name."/".$id_product.'-'.$id_shop.'.'.$imgtype.'?rand='.rand(0,100).'" alt="'.$obj->l('barcode').'"/>';
	die($str);
}

?>