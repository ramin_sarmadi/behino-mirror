<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */

if (!defined('_PS_VERSION_'))
	exit;
		$this->_html .= '<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">';
		/* Save */
		$this->_html .= '
		<div class="margin-form">
			<input type="submit" class="button" name="submitBarcode" value="'.$this->l('Save').'" />
		</div>';
		$this->_html .= '
				<fieldset><legend><img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('General configuration').'</legend>';
		
		$this->_html .= $this->getParams()->getThemesTag( $this->getParams()->get('theme') );
		$this->_html .= $params->statusTag( $this->l('Show Barcode'), 'show_barcode', $this->getParams()->get('show_barcode',1), 'show_barcode', '<p>'.$this->l('Display barcode in product page').'</p>');

		$types = array('UPC-A' => $this->l('UPC-A'),'UPC-E'=> $this->l("UPC-E"),'EAN-8'=> $this->l("EAN-8"),'EAN-13'=> $this->l("EAN-13"),'link'=> $this->l("QRCODES - Link"),'vcard'=>$this->l("QRCODES - Vcard"));
		$this->_html .= $params->selectTag( $types, $this->l('Barcode Type:'), 'type', $this->getParams()->get('type'), '' );
		$this->_html .= $params->inputTag( $this->l('Barcode Height:'), 'barcodeheight', $this->getParams()->get('barcodeheight'), 'px',' size="25" ' );	
		$this->_html .= $params->inputTag( $this->l('Scale:'), 'scale',$this->getParams()->get('scale'),'','size="25"');
		$this->_html .= $params->inputTag( $this->l('Background Color:'), 'bgcolor', $this->getParams()->get('bgcolor'), '#FFFFFF', 'size="25"');
		$this->_html .= $params->inputTag( $this->l('Barcode Color:'), 'barcolor' ,$this->getParams()->get('barcolor'), '#000000', 'size="25"');
		
			
		$imgtype = array('jpg' => $this->l('JPG'), 'png' => $this->l('PNG'));
		$this->_html .= $params->selectTag( $imgtype, $this->l('Image type:'), 'imgtype', $this->getParams()->get('imgtype'), '');
		
		$this->_html .= '<div id="optionvcard" style=" display: '.($this->getParams()->get('type') == 'vcard' ? 'block' : 'none').';">';	
		$this->_html .= $params->statusTag( $this->l('Show product name'), 'show_productname', $this->getParams()->get('show_productname',1), 'show_productname');
		$this->_html .= $params->statusTag( $this->l('Show product reference'), 'show_reference', $this->getParams()->get('show_reference',1), 'show_reference');
		$this->_html .= $params->statusTag( $this->l('Show final price'), 'show_fprice', $this->getParams()->get('show_fprice',1), 'show_fprice');
		$this->_html .= $params->statusTag( $this->l('Show price before reduced'), 'show_bprice', $this->getParams()->get('show_fprice',1), 'show_fprice');
		$this->_html .= $params->statusTag( $this->l('Show product width'), 'show_width', $this->getParams()->get('show_width',1), 'show_width');
		$this->_html .= $params->statusTag( $this->l('Show product height'), 'show_height', $this->getParams()->get('show_height',1), 'show_height');
		$this->_html .= $params->statusTag( $this->l('Show product depth'), 'show_depth', $this->getParams()->get('show_depth',1), 'show_depth');
		$this->_html .= $params->statusTag( $this->l('Show product weight'), 'show_weight', $this->getParams()->get('show_weight',1), 'show_weight');
		$this->_html .= '</div>';
		$this->_html .= '
			<script type="text/javascript">
				jQuery(document).ready(function(){
					$("#type").change(function(){
						loftoggle($(this).val());
					});
				});
				function loftoggle(value){
					if ( value == "vcard"){
						$("#optionvcard").show(500);
					}else{
						$("#optionvcard").hide(500);
					}
				}
			</script>
		';
		
		$this->_html .= '</fieldset>';
	 	
	/* Save */
		$this->_html .= '<br><br>
		<div class="margin-form">
			<input type="submit" class="button" name="submitBarcode" value="'.$this->l('Save').'" />
		</div>';
		$this->_html .= '
			<fieldset><legend class="general_barcode" style="cursor: pointer;"><img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('Generate Barcode').'</legend>';
			$default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
			$default_category = 0;
			if (Tools::isSubmit('categoryBox'))
				$selected_cat = Category::getCategoryInformations(Tools::getValue('categoryBox', array($default_category)), $default_form_language);
			else
				$selected_cat = self::getCategoriesFull( $default_form_language);
			
			$root = Category::getRootCategory();
			$tab_root = array('id_category' => $root->id, 'name' => $root->name);
			$helper = new Helper();
			$category_tree = $helper->renderCategoryTree($tab_root, $selected_cat, 'categoryBox', false, true, array(), false, true);
			
			
			$this->_html .= '
			<script type="text/javascript">
				jQuery(document).ready(function(){
					$(".general_barcode").click(function(){
						$("#general_barcode").toggle(500);
					});
				});
			</script>
			<p>'.$this->l('The module auto generates Barcode when you add or edit product. 
					In case you want to generate Barcode for many products at same time, just select categories and click on "Genarate Barcode" button').'</p>
			<div class="margin-form" id="general_barcode">
				'.$category_tree.'
				<br/>
				<input type="submit" class="button" name="submitGenerate" value="'.$this->l('Generate barcode').'" />
				<p>'.$this->l('Click here to genarate barcode for all product.').'</p>
			</div>';
		$this->_html .= '</fieldset>';		
	$this->_html .= '</form>';

	
?>