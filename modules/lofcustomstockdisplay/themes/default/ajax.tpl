{if $lofrules}
	{foreach from=$lofrules item=item}
        {if $item.show == 1 || $item.show == 3}
            {if isset($item.rules) && $item.rules}
            <div>
                <span>{$item.rules}</span>
            </div>
            {/if}
        {/if}
        {if $item.show == 2 || $item.show == 3}
            {if isset($item.img) && $item.img}
                <div>
                    <img src="{$item.img}"/>
                </div>
            {/if}
        {/if}
	{/foreach}
{/if}