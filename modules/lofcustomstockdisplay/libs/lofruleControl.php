<?php


class lofruleControl extends ObjectModel
{
	public $min;
	public $max;
	public $rules;
	public $image;
	public $show;
	public $title;
	public $active;
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'lofcustomstockdisplay_rules',
		'primary' => 'id_lofcustomstockdisplay_rules',
		'multilang' => true,
		'multishop' => true,
		'fields' => array(
			'show' => 		array('type' => self::TYPE_INT,'validate' => 'isunsignedInt', 'required' => true),
			'min' => 		array('type' => self::TYPE_INT,'validate' => 'isunsignedInt', 'required' => true),
			'max' => 		array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'active' =>			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			// Lang fields
			'rules' =>		array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'required' => true, 'size' => 255),
			'title' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
			'image' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 255),
		)
	);
	
	public	function __construct($id_lofcustomstockdisplay_rules = null, $id_lang = null, $id_shop = null, Context $context = null)
	{
		parent::__construct($id_lofcustomstockdisplay_rules, $id_lang, $id_shop);
	}

	public function add($autodate = true, $null_values = false)
	{
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$res = parent::add($autodate, $null_values);
		$res &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'lofcustomstockdisplay` (`id_shop`, `id_lofcustomstockdisplay_rules`)
			VALUES('.(int)$id_shop.', '.(int)$this->id.')'
		);
		return $res;
	}

	public function delete()
	{
		$res = true;

		$images = $this->image;
		foreach ($images as $image)
		{
			if (preg_match('/sample/', $image) === 0)
				if ($image && file_exists(dirname(__FILE__).'/images/'.$image))
					$res &= @unlink(dirname(__FILE__).'/images/'.$image);
		}
		$res &= Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'lofcustomstockdisplay`
			WHERE `id_lofcustomstockdisplay_rules` = '.(int)$this->id
		);

		$res &= parent::delete();
		return $res;
	}
}
