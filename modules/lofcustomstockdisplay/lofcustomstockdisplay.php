<?php
/**
 * lofcustomstockdisplay Module
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) September 2012 LandOfcoder.Com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
 
/**
 * @since 1.5.0
 * @version 1.2 (2012-03-14)
 */

if (!defined('_PS_VERSION_'))
	exit;	
include_once(_PS_MODULE_DIR_.'lofcustomstockdisplay/libs/lofruleControl.php');
class lofcustomstockdisplay extends Module
{
	private $_html = '';
	
	private $_configs = '';
	
	protected $params = null; 
	
	public $_thumbnaiURL='';
	
	public $_thumbnailPath='';
	public function __construct()
	{
		$this->name 	= 'lofcustomstockdisplay';
		$this->tab	    = 'LandOfcoder';
		$this->version  = '1.0';
		$this->author 	= 'LandOfcoder';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		
		parent::__construct();

		$this->displayName = $this->l('Lof Custom Stock Display');
		$this->description = $this->l('Lof Custom Stock Display.');
		$this->_prepareForm();
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/Params.php' ) && !class_exists("LofCsdParams", false) ){
			if( !defined("LOF_DISPLAY_SHOW_LOAD_LIB_PARAMS") ){				
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/Params.php' );
				define("LOF_DISPLAY_SHOW_LOAD_LIB_PARAMS",true);
			}
		}	
		$this->params =  new LofCsdParams( $this, $this->name , $this->_configs  );
	}
	
	public function _prepareForm(){
		$this->_configs = array(
			'theme'  => 'default',
		);
	}
	
	public function getParams(){
		return $this->params;
	}
	/**
	 * @see Module::install()
	 */
	public function install()
	{
		if (parent::install() && $this->registerHook('displayRightColumnProduct'))
			{
			$this->getParams()->batchUpdate( $this->_configs );
				$res=true;
				$res &= $this->createTables();
			if ($res)
				$this->installSamples();
				return $res;
			}
		return false;
	}

	private function installSamples()
	{
		$languages = Language::getLanguages(false);
		
        $rules = new lofruleControl();
        $rules->min = 0 ;
        $rules->max = 0 ;
        $rules->show = 3;
        $rules->active = 1;
        foreach ($languages as $language)
        {
            $rules->rules[$language['id_lang']] ='<b>%product% is not available now.</b>';
            $rules->title[$language['id_lang']] ='Out of stock';
            $rules->image[$language['id_lang']] = 'sample-0.png';
        }
        $rules->add();

        $rules = new lofruleControl();
        $rules->min = 1 ;
        $rules->max = 20 ;
        $rules->show = 3;
        $rules->active = 1;
        foreach ($languages as $language)
        {
            $rules->rules[$language['id_lang']] ='<b>Hurry up! The product is nearly out of stock. Only <span style="color:green">%quantity%</span> item(s) left.</b>';
            $rules->title[$language['id_lang']] ='From 1 - 20 Items in stock';
            $rules->image[$language['id_lang']] = 'sample-1.png';
        }
        $rules->add();
		
		for ($i = 1 ; $i <= 4; ++$i)
		{
			$rules = new lofruleControl();
			$rules->min = ($i*20 + 1);
			$rules->max = (($i*20) + 20);
			$rules->show = 3;
			$rules->active = 1;
			foreach ($languages as $language)
			{
				$rules->rules[$language['id_lang']] ='There are <span style="color:green">%quantity%</span> items <b>In Stock</b>.';
				$rules->title[$language['id_lang']] ='From '.($i*20 + 1).' - '.(($i*20) + 20).' Items in stock';
				$rules->image[$language['id_lang']] = 'sample-'.($i+1).'.png';
			}
			$rules->add();
		}
		$rules = new lofruleControl();
        $rules->min = 101 ;
        $rules->show = 2;
        $rules->active = 1;
        foreach ($languages as $language)
        {
            $rules->rules[$language['id_lang']] ='<b>%quantity%</b> items are in stock';
            $rules->title[$language['id_lang']] ='Over 101 Items in stock';
            $rules->image[$language['id_lang']] = 'sale.png';
        }
        $rules->add();
	}
	
	protected function createTables()
	{
		/* Rules */
		$res = (bool)Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$this->name.'` (
				`id_'.$this->name.'_rules` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_shop` int(10) unsigned NOT NULL,
				PRIMARY KEY (`id_'.$this->name.'_rules`, `id_shop`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');

		$res &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$this->name.'_rules` (
			  `id_'.$this->name.'_rules` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `min`   int(100) unsigned NOT NULL,
			  `max`   int(100) unsigned ,
			  `show` int(10) unsigned NOT NULL DEFAULT \'0\',
			  `active` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			  PRIMARY KEY (`id_'.$this->name.'_rules`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');	
		
		/* Rules lang configuration */
		$res &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.$this->name.'_rules_lang` (
			  `id_'.$this->name.'_rules` int(10) unsigned NOT NULL,
			  `id_lang` int(10) unsigned NOT NULL,
			  `title` varchar(255) NOT NULL,
			  `rules` varchar(255) NOT NULL,
			  `image` varchar(255) NOT NULL,
			  PRIMARY KEY (`id_'.$this->name.'_rules`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
		');

		return $res;
	}
	
	protected function deleteTables()
	{
		return Db::getInstance()->execute('
			DROP TABLE IF EXISTS `'._DB_PREFIX_.$this->name.'`,`'._DB_PREFIX_.$this->name.'_rules`,`'._DB_PREFIX_.$this->name.'_rules_lang`;
		');
	}
	
	public function uninstall()
	{
		if (parent::uninstall())
		{
			$res = $this->deleteTables();
			$res = $this->getParams()->delete();
			return $res;
		}
		return false;
	}
	
	public function getContent()
	{
		$this->_html .= '<h2>'.$this->displayName.'.</h2>';
		if (Tools::isSubmit('submitRule') || Tools::isSubmit('delete_id_rules') || Tools::isSubmit('submitRules') || Tools::isSubmit('changeStatus')){
			if ($this->_postValidation())
				$this->_postProcess();
			$this->_displayForm();
		}
		elseif (Tools::isSubmit('addRule') || (Tools::isSubmit('id_rules')))
			$this->_displayAddForm();
		else
			$this->_displayForm();
		return $this->_html;
	}
	
	private function _displayForm()
	{
		$params = $this->params;
		$rules = $this->getRules();
		require_once ( dirname(__FILE__).'/form.php' );
	}
	
	private function _displayAddForm()
	{
		$rules = null;
		if (Tools::isSubmit('id_rules') && $this->ruleExists((int)Tools::getValue('id_rules')))
			$rules = new lofruleControl((int)Tools::getValue('id_rules'));
		Context::getContext()->controller->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
		Context::getContext()->controller->addJS(_PS_JS_DIR_.'tinymce.inc.js');

		$this->_html .= '<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post" enctype="multipart/form-data">';
		$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
		$divLangName = 'image¤rules¤title';
		$languages = Language::getLanguages(false);
		$this->_html .= '
		<fieldset><legend><img src="'._PS_ADMIN_IMG_.'add.gif" alt="" />0 - '.$this->l('Image Manager').'</legend>';
			/* Image */
			$this->_html .= '<label>'.$this->l('Select a file:').' * </label><div class="margin-form">';
			foreach ($languages as $language)
			{
				$this->_html .= '<div id="image_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $id_lang_default ? 'block' : 'none').';float: left;">';
				$this->_html .= '<input type="file" name="image_'.$language['id_lang'].'" id="image_'.$language['id_lang'].'" size="30" value="'.(isset($rules->image[$language['id_lang']]) ? $rules->image[$language['id_lang']] : '').'"/>';
				/* Sets image as hidden in case it does not change */
				if ($rules && $rules->image[$language['id_lang']])
					$this->_html .= '<input type="hidden" name="image_old_'.$language['id_lang'].'" value="'.($rules->image[$language['id_lang']]).'" id="image_old_'.$language['id_lang'].'" />';
				/* Display image */
                $this->_html .= '<div class="clear space"></div>';
				if ($rules && $rules->image[$language['id_lang']])
					$this->_html .= '<img src="'.__PS_BASE_URI__.'modules/'.$this->name.'/images/'.$rules->image[$language['id_lang']].'" alt=""/>';
				$this->_html .= '</div>';
			}
			$this->_html .= $this->displayFlags($languages, $id_lang_default, $divLangName, 'image', true);
		$this->_html .= '</fieldset>';
		$this->_html .= '<br />';
		$this->_html .= '

		<fieldset>
			<br />
			<legend><img src="'._PS_ADMIN_IMG_.'add.gif" alt="" />1 - '.$this->l('Rule Information').'</legend>';
			
			$this->_html .= '
				<label>'.$this->l('Title:').' </label>
				<div class="margin-form">';
				foreach ($languages as $language)
				{
					$this->_html .= '<div id="title_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $id_lang_default ? 'block' : 'none').';float: left;">
						<input name="title_'.$language['id_lang'].'" value="'.(isset($rules->title[$language['id_lang']]) ? $rules->title[$language['id_lang']] : '').' " size="50">
					</div>';
				}
				$this->_html .= $this->displayFlags($languages, $id_lang_default, $divLangName, 'title', true);
				$this->_html .= '</div><div class="clear"></div><br />';	
			
			$this->_html .= '<label>'.$this->l('Maximal Quantity:').' </label>
				<div class="margin-form">
					<div id="min" style="display: block; float: left;">
					<input name="min" value="'.(isset($rules->min) ? $rules->min: '').'"/>
					</div>';
			$this->_html .= '</div><div class="clear"></div><br />';	
			
			$this->_html .= '<label>'.$this->l('Maximal Quantity:').' </label>
				<div class="margin-form">
					<div id="max" style="display: block; float: left;">
					<input name="max" value="'.(isset($rules->max) ? $rules->max: '').'"/>
					<p>'.$this->l('Max = 0 to set unlimit quantity, if Min = Max = 0 this case is out of stock').'</p>
					</div>';
			$this->_html .= '</div><div class="clear"></div><br />';	
			
			$this->_html .= '
				<label>'.$this->l('Description:').' </label>
				<div class="margin-form">';
				foreach ($languages as $language)
				{
					$this->_html .= '<div id="rules_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $id_lang_default ? 'block' : 'none').';float: left;">
						<textarea class="rte" name="rules_'.$language['id_lang'].'" rows="5" cols="35">'.(isset($rules->rules[$language['id_lang']]) ? $rules->rules[$language['id_lang']] : '').'</textarea>
					</div>';
				}
				$this->_html .= $this->displayFlags($languages, $id_lang_default, $divLangName, 'rules', true);
				$this->_html .= '<div class="clear"></div><ul><li>- '.$this->l('%quantity% :  Product quantity').'</li><li>- '.$this->l('%product% : Product name').'</li></ul>';
				$this->_html .= '</div><div class="clear"></div><br />';
			
			$this->_html .= '
				<label for="active_on">'.$this->l('Display Type:').'</label>
				<select name="show">
					<option value="3" '.((isset($rules->show) && $rules->show == 3) ? 'selected': '').'>'.$this->l('Display All').'</option>
					<option value="1" '.((isset($rules->show) && $rules->show == 1) ? 'selected': '').' >'.$this->l('Text Only').'</option>
					<option value="2" '.((isset($rules->show) && $rules->show == 2) ? 'selected': '').'>'.$this->l('Image Only').'</option>
					<option value="4" '.((isset($rules->show) && $rules->show == 4) ? 'selected': '').'>'.$this->l('Hidden').'</option>
				</select>';
				$this->_html .= '<div class="clear"></div><br />';
				$this->_html .= '
					<label for="active_on">'.$this->l('Active:').'</label>
					<div class="margin-form">
						<input type="radio" name="active" id="active_on" '.(($rules && (isset($rules->active) && (int)$rules->active == 0)) ? '' : 'checked="checked" ').' value="1" />
						<label class="t" for="active_on">'.$this->l('Yes').'</label>
						<input type="radio" name="active" id="active_off" '.(($rules && (isset($rules->active) && (int)$rules->active == 0)) ? 'checked="checked" ' : '').' value="0" />
						<label class="t" for="active_off">'.$this->l('No').'</label>
					</div>';
			$this->_html .= '</div>
				<p class="lofsave" style="margin-left:400px;">
				<input type="submit" class="button" name="submitRule" value="'.$this->l('Save').'" />
				<a class="button" style="position:relative; padding:3px 3px 4px 3px; top:1px" href="'.AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'">'.$this->l('Cancel').'</a>
			</p>
		</fieldset>';
        $isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.Context::getContext()->language->iso_code.'.js') ? Context::getContext()->language->iso_code : 'en');
        $ad = dirname($_SERVER["PHP_SELF"]);
        $this->_html .= '<script type="text/javascript">
              var iso = \''.$isoTinyMCE.'\' ;
              var pathCSS = \''._THEME_CSS_DIR_.'\' ;
              var ad = \''.$ad.'\' ;
              tinySetup();
            </script>';
		$this->_html .= '</form>';
	}
	
	private function _postValidation()
	{
		$errors = array();
		if (Tools::isSubmit('changeStatus'))
		{
			if (!Validate::isInt(Tools::getValue('id_rules')))
				$errors[] = $this->l('Invalid slide');
		}
		elseif (Tools::isSubmit('submitRule'))
		{
			$languages = Language::getLanguages(false);
			$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
			/* If edit : checks id_rules*/
			if (Tools::isSubmit('id_rules'))
			{
				if (!Validate::isInt(Tools::getValue('id_rules')))
					$errors[] = $this->l('Invalid id_rules');
			}
			else
			{
				if ((!isset($_FILES['image_'.$id_lang_default]) || empty($_FILES['image_'.$id_lang_default]['tmp_name']) ) && !Tools::getValue('id_rule'))
				    $errors[] = $this->l('Image  is not set');
				if (Tools::getValue('image_'.$id_lang_default) && !Validate::isFileName(Tools::getValue('image_'.$id_lang_default)))
				    $errors[] = $this->l('Image is not set');
			}
			/* Checks title/url/legend/description/image */
			foreach ($languages as $language)
			{
				if (strlen(Tools::getValue('rules_'.$language['id_lang'])) > 255)
					$errors[] = $this->l('Rules is too long');
				if (strlen(Tools::getValue('title_'.$language['id_lang'])) > 255)
					$errors[] = $this->l('Title is too long');	
				if (Tools::getValue('image_'.$language['id_lang']) != null && !Validate::isFileName(Tools::getValue('image_'.$language['id_lang'])))
					$errors[] = $this->l('Invalid image');
			}
			if (strlen(Tools::getValue('min')) == 0)
				$errors[] = $this->l('Min is not set');
			if (!preg_match('/^[0-9]*$/', Tools::getValue('min'))) 
				$errors[] = $this->l('Min is number');
			if (!preg_match('/^[0-9]*$/', Tools::getValue('max'))) 
				$errors[] = $this->l('Max is number');		
			if (strlen(Tools::getValue('rules_'.$id_lang_default)) == 0)
				$errors[] = $this->l('Rule is not set');
			if (strlen(Tools::getValue('title_'.$id_lang_default)) == 0)
				$errors[] = $this->l('Title is not set');	
			
		}
		elseif (Tools::isSubmit('delete_id_rule') && (!Validate::isInt(Tools::getValue('delete_id_rule')) || !$this->ruleExists((int)Tools::getValue('delete_id_rule'))))
			$errors[] = $this->l('Invalid id_rule');
			
		if (count($errors))
		{
			$this->_html .= $this->displayError(implode('<br />', $errors));
			return false;
		}

		/* Returns if validation is ok */
		return true;
	}
	
	private function _postProcess()
	{
		$errors = array();
		if (Tools::isSubmit('submitScrollr')){	
			$res = $this->getParams()->batchUpdate( $this->_configs );
			$this->getParams()->refreshConfig(); 
			if (!$res)
				$errors .= $this->displayError($this->l('Configuration could not be updated'));
			$this->_html .= $this->displayConfirmation($this->l('Configuration updated'));
		} 
		elseif (Tools::isSubmit('changeStatus') && Tools::isSubmit('id_rules'))
		{
			$rules = new lofruleControl((int)Tools::getValue('id_rules'));
			if ($rules->active == 0)
				$rules->active = 1;
			else
				$rules->active = 0;
			$res = $rules->update();
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Configuration updated')) : $this->displayError($this->l('Configuration could not be updated')));
		}
		elseif (Tools::isSubmit('submitRule')){	
			if (Tools::getValue('id_rules'))
			{
				$rules = new lofruleControl((int)Tools::getValue('id_rules'));
				if (!Validate::isLoadedObject($rules))
				{
					$this->_html .= $this->displayError($this->l('Invalid id_rules'));
					return;
				}
			}
			else
				$rules = new lofruleControl();
			$rules->min = (int)Tools::getValue('min');
			$rules->max = (int)Tools::getValue('max');
			$rules->show = (int)Tools::getValue('show');
			$rules->active = (int)Tools::getValue('active');
			$languages = Language::getLanguages(false);	
			foreach ($languages as $language)
			{
				if (Tools::getValue('rules_'.$language['id_lang']) != '')
					$rules->rules[$language['id_lang']] = Tools::getValue('rules_'.$language['id_lang']);
				if (Tools::getValue('title_'.$language['id_lang']) != '')
					$rules->title[$language['id_lang']] = pSQL(Tools::getValue('title_'.$language['id_lang']));	
				$type = strtolower(substr(strrchr($_FILES['image_'.$language['id_lang']]['name'], '.'), 1));
				$imagesize = array();
				$imagesize = @getimagesize($_FILES['image_'.$language['id_lang']]['tmp_name']);
				if (isset($_FILES['image_'.$language['id_lang']]) &&
					isset($_FILES['image_'.$language['id_lang']]['tmp_name']) &&
					!empty($_FILES['image_'.$language['id_lang']]['tmp_name']) &&
					!empty($imagesize) &&
					in_array(strtolower(substr(strrchr($imagesize['mime'], '/'), 1)), array('jpg', 'gif', 'jpeg', 'png')) &&
					in_array($type, array('jpg', 'gif', 'jpeg', 'png')))
					{
						$folder = _PS_MODULE_DIR_.$this->name.'/images/';
						
						if(!is_dir($folder))
							@mkdir($folder,0777);
						$salt = sha1(microtime());
						$file = $folder.Tools::encrypt($_FILES['image_'.$language['id_lang']]['name'].$salt).'.'.$type;
						
						if ($error = ImageManager::validateUpload($_FILES['image_'.$language['id_lang']]))
							$errors .= $error;
						elseif (!$file || !move_uploaded_file($_FILES['image_'.$language['id_lang']]['tmp_name'], $file))
							$errors .= $this->displayError($this->l('An error occurred during the image upload.'));
						$rules->image[$language['id_lang']] = pSQL(Tools::encrypt($_FILES['image_'.($language['id_lang'])]['name'].$salt).'.'.$type);
					}	
				elseif (Tools::getValue('image_old_'.$language['id_lang']) != '')
					$rules->image[$language['id_lang']] = pSQL(Tools::getValue('image_old_'.$language['id_lang']));	
			}		
			if (!$errors)
			{
				/* Adds */
				if (!Tools::getValue('id_rules'))
				{
					if (!$rules->add())
						$errors .= $this->displayError($this->l('Rules could not be added'));
				} /* Update */
				elseif (!$rules->update())
					$errors .= $this->displayError($this->l('Rules could not be updated'));
			}
		} 
		elseif (Tools::isSubmit('delete_id_rules'))
		{
			$rules = new lofruleControl((int)Tools::getValue('delete_id_rules'));
			$res = $rules->delete();
			if (!$res)
				$this->_html .= $this->displayError('Could not delete');
			else
				$this->_html .= $this->displayConfirmation($this->l('Rules deleted'));
		}
		if (count($errors))
			$this->_html .= $this->displayError(implode('<br />', $errors));
		elseif (Tools::isSubmit('submitRule') && Tools::getValue('id_rules'))
			$this->_html .= $this->displayConfirmation($this->l('Rule updated'));
		elseif (Tools::isSubmit('submitRule'))
			$this->_html .= $this->displayConfirmation($this->l('Rule added'));
	}
	
	public function ajaxGetResult($items, $id_product){
        $obj = new Product((int)$id_product, true, (int)(Context::getContext()->language->id));
		$configs = array();
		foreach( $this->_configs as $key => $config ){
			$configs[$key] = $this->getParams()->get( $key, $config );
		}
		$item = !empty($items) ? $items : 0;
		$this->smarty->assign( 'lof_item', $item );
		$lofrules = array();
		$rules = $this->getActiveRules();
        if($rules)
            foreach($rules as $rule){
                if($rule['min'] > 0 && $rule['max'] == 0)
                {
                    if($item >= $rule['min']){
                        $nlofrules = str_replace("%quantity%",$item,$rule['rules']);
                        $nlofrules = str_replace("%product%",$obj->name, $nlofrules);
                        $result["img"] = $this->_path.'images/'.$rule['image'];
                        $result["rules"] = $nlofrules;
                        $result["show"] = $rule['show'];
                        $lofrules[] = $result;
                    }
                }
                else
                {
                    if($item >= $rule['min'] && $item <= $rule['max']){
                        $nlofrules = str_replace("%quantity%",$item,$rule['rules']);
                        $nlofrules = str_replace("%product%",$obj->name, $nlofrules);
                        $result["img"] = $this->_path.'images/'.$rule['image'];
                        $result["rules"] = $nlofrules;
                        $result["show"] = $rule['show'];
                        $lofrules[] = $result;
                    }
                }
            }
		$this->smarty->assign( 'lofrules', $lofrules );
		return $this->display(__FILE__, 'themes/'.$this->getParams()->get('theme','default').'/ajax.tpl' );
	}

	public function hookdisplayRightColumnProduct()
	{
        $configs = array();
        foreach( $this->_configs as $key => $config ){
            $configs[$key] = $this->getParams()->get( $key, $config );
        }
        $theme = $this->getParams()->get('theme');
        $this->context->controller->addCSS($this->_path.'themes/'.$theme.'/assets/style.css');
        $id_product = Tools::getValue('id_product') ? (int)Tools::getValue('id_product') : 0;
        $product = new Product($id_product);
        $lofcache_default_attribute = isset($product->cache_default_attribute) ? $product->cache_default_attribute : 0 ;
        $this->smarty->assign( 'lofcache_default_attribute', $lofcache_default_attribute );
        $this->smarty->assign( 'product', $product );
        $this->smarty->assign( 'lofajax', _MODULE_DIR_.$this->name.'/ajax.php' );
        $this->smarty->assign( 'lofconfigs', $configs );

        if( file_exists(_PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/'.$this->name.'/default.tpl' ) )
            $this->smarty->assign( 'lof_ajax_default', _PS_THEME_DIR_.'/modules/'.$this->name.'/default.tpl' );
        else
            $this->smarty->assign( 'lof_ajax_default', _PS_MODULE_DIR_.$this->name.'/default.tpl' );
        return $this->display(__FILE__, 'themes/'.$theme.'/default.tpl' );
	}

	public function getRules()
	{
		$this->context = Context::getContext();
		$id_shop = $this->context->shop->id;
		$id_lang = $this->context->language->id;
		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_'.$this->name.'_rules` as id_rules, hss.`min`,hss.`max`,hss.`active`,
						hss.`show`,
					   hssl.`image`,
					   hssl.`rules`,
					   hssl.`title`
			FROM '._DB_PREFIX_.$this->name.' hs
			LEFT JOIN '._DB_PREFIX_.$this->name.'_rules hss ON (hs.id_'.$this->name.'_rules = hss.id_'.$this->name.'_rules)
			LEFT JOIN '._DB_PREFIX_.$this->name.'_rules_lang hssl ON (hss.id_'.$this->name.'_rules = hssl.id_'.$this->name.'_rules)
			WHERE (hs.id_shop = '.(int)$id_shop.')
			AND (hssl.id_lang = '.(int)$id_lang.')
			ORDER BY hss.`min` ASC
			');
	}
	
	public function getActiveRules()
	{
		$this->context = Context::getContext();
		$id_shop = $this->context->shop->id;
		$id_lang = $this->context->language->id;
		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_'.$this->name.'_rules` as id_rules, hss.`min`,hss.`max`,hss.`active`,
						hss.`show`,
					   hssl.`image`,
					   hssl.`rules`,
					   hssl.`title`
			FROM '._DB_PREFIX_.$this->name.' hs
			LEFT JOIN '._DB_PREFIX_.$this->name.'_rules hss ON (hs.id_'.$this->name.'_rules = hss.id_'.$this->name.'_rules)
			LEFT JOIN '._DB_PREFIX_.$this->name.'_rules_lang hssl ON (hss.id_'.$this->name.'_rules = hssl.id_'.$this->name.'_rules)
			WHERE (hs.id_shop = '.(int)$id_shop.')
			AND (hssl.id_lang = '.(int)$id_lang.')
			AND (hss.active = 1)
			ORDER BY hss.`min` ASC
			');
	}
	
	public function ruleExists($id_rules)
	{
		$req = 'SELECT hs.`id_'.$this->name.'_rules` as id_rules
				FROM `'._DB_PREFIX_.$this->name.'` hs
				WHERE hs.`id_'.$this->name.'_rules` = '.(int)$id_rules;
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);
		return ($row);
	}
	
	public function displayStatus($id_rules, $active)
	{
		$title = ((int)$active == 0 ? $this->l('Disabled') : $this->l('Enabled'));
		$img = ((int)$active == 0 ? 'disabled.gif' : 'enabled.gif');
		$html = '<a href="'.AdminController::$currentIndex.
				'&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&changeStatus&id_rules='.(int)$id_rules.'" title="'.$title.'"><img src="'._PS_ADMIN_IMG_.''.$img.'" alt="" /></a>';
		return $html;
	}
}
