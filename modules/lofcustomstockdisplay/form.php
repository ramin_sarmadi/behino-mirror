<?php
/**
 * lofcustomstockdisplay Module
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) September 2012 LandOfcoder.Com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
 
/**
 * @since 1.5.0
 * @version 1.2 (2012-03-14)
 */

if (!defined('_PS_VERSION_'))
	exit;
		$this->_html .= '
		<style>
		#rules li {
			list-style: none;
			margin: 0 0 4px 0;
			padding: 10px;
			background-color: #F4E6C9;
			border: #CCCCCC solid 1px;
			color:#000;
			height:30px;
		}
		#rules strong {
			width : 30px;
			float:left;
			margin-top: 6px;
		}
		.lofimg {
			width:190px;
			float:left;
		}
		</style>';
		$this->_html .= '<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">';
		/* Save */
		$this->_html .= '
		<fieldset><legend><img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('General configuration').'</legend>';
		$this->_html .= $this->getParams()->getThemesTag( $this->getParams()->get('theme') );
		$this->_html .= '<br>';
		$this->_html .= '</fieldset>';
	/* Save */
		$this->_html .= '<br>
		<fieldset class="source-group sourceimages">
			<legend><img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('Rules configuration').'</legend>
			<strong>
				<a href="'.AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&addRule">
					<img src="'._PS_ADMIN_IMG_.'add.gif" alt="" /> '.$this->l('Add Rule').'
				</a>
			</strong>';
			if (!$rules)
			$this->_html .= '<p style="margin-left: 40px;">'.$this->l('You have not added any rules yet.').'</p>';
		else /* Display rules */
		{
			$this->_html .= '
			<div id="slidesContent" style="width: 600px; margin-top: 30px;">
				<ul id="rules">';

			foreach ($rules as $rule)
			{
				$this->_html .= '
					<li id="rules_'.$rule['id_rules'].'">
						<strong>#'.$rule['id_rules'].'</strong> &nbsp;&nbsp;<div class="lofimg"><img src="'.__PS_BASE_URI__.'modules/'.$this->name.'/images/'.$rule['image'].'" alt="" style="max-height:30px ; max-width:190px;"/></div>&nbsp;&nbsp;'.$rule['title'].'&nbsp;&nbsp;
						<p style="float: right">
							'.$this->displayStatus($rule['id_rules'], $rule['active']).'
							<a href="'.AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&id_rules='.(int)($rule['id_rules']).'" title="'.$this->l('Edit').'"><img src="'._PS_ADMIN_IMG_.'edit.gif" alt="" /></a>
							<a href="'.AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&delete_id_rules='.(int)($rule['id_rules']).'" title="'.$this->l('Delete').'"><img src="'._PS_ADMIN_IMG_.'delete.gif" alt="" /></a>
						</p>
					</li>';
			}
			$this->_html .= '</ul></div>';
		}
		$this->_html .= '</fieldset><br />';
			
		$this->_html .= '<div class="margin-form">
			<input type="submit" class="button" name="submitScrollr" value="'.$this->l('    Save    ').'" />
		</div>';
		$this->_html .= '</form><br /><br />';

	
?>