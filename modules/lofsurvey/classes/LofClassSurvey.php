<?php
/*
  * @module 
  * @author 
  * @copyright 
  * @version 1.0
*/

if (!defined('_CAN_LOAD_FILES_') AND _PS_VERSION_ > '1.3')
	exit;

if(!class_exists('LofClassSurvey')) {	
	class LofClassSurvey extends ObjectModel
	{	
		public $id;
		public $title;
		public $active = 1;
		public $position;
		public $date_add;
		public $total;
		
		public static $definition = array(
		'table' => 'lofsurvey_survey',
		'primary' => 'id_lofsurvey_survey',
		'multilang' => true,
		//'multilang_shop' => true,
		'fields' => array(
			'active' =>			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			//'position' => 			array('type' => self::TYPE_INT),
			'date_add' =>		array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'total'=> array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			// Lang fields
			'title' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
		)
		);
		
		public function __construct($id = NULL, $id_lang = NULL){
			parent::__construct($id, $id_lang);
		}

		
	public function add($autodate = true, $null_values = false)
	{
			$context = Context::getContext();
			$id_shop = $context->shop->id;
			$res = parent::add($autodate, $null_values);
				$position = $this->getLastPosition($id_shop);
				if (!$position)
					$position = 1;
				$this->addPosition($position, $id_shop);
			return $res;

	}
	
	public function delete()
	{
		$res = true;
		//$res &= $this->reOrderPositions();
		$res &= Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'lofsurvey_survey`
			WHERE `id_lofsurvey_survey` = '.(int)$this->id
		);
		$res &= Db::getInstance()->execute('
			DELETE FROM `'._DB_PREFIX_.'lofsurvey_guest`
			WHERE `id_lofsurvey_survey` = '.(int)$this->id
		);
		$replies=Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT `id_lofsurvey_reply` 
			FROM `'._DB_PREFIX_.'lofsurvey_reply`
			WHERE `id_lofsurvey_survey` = '.(int)$this->id);
		foreach($replies as $reply){
			$objReply = new LofClassReply($reply['id_lofsurvey_reply']);
			$res &= $objReply->delete();
			}
		$res &= parent::delete();
		return $res;
	}
	
	public static function getTotalSurvey($id_survey)
	{
		return 	Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT `total` 
			FROM `'._DB_PREFIX_.'lofsurvey_survey`
			WHERE `id_lofsurvey_survey` = '.(int)$id_survey);
	}

	public static function getIdGuestSurvey($id_survey){
		return 	Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT `id_guest` 
			FROM `'._DB_PREFIX_.'lofsurvey_guest`
			WHERE `id_lofsurvey_survey` = '.(int)$id_survey);
	}
	
	public  function getLastPosition($id_shop)
		{
			return (int)(Db::getInstance()->getValue('
			SELECT MAX(`position`)
			FROM `'._DB_PREFIX_.'lofsurvey_survey_shop`
			WHERE `id_shop` = '.(int)$id_shop) + 1);
		}	
	
	public function addPosition($position, $id_shop = null)
	{
		$return = true;
		if (is_null($id_shop))
		{
			if (Shop::getContext() != Shop::CONTEXT_SHOP)
				foreach (Shop::getContextListShopID() as $id_shop)
					$return &= Db::getInstance()->execute('
						INSERT INTO `'._DB_PREFIX_.'lofsurvey_survey_shop` (`id_lofsurvey_survey`, `id_shop`, `position`) VALUES
						('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
						ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
			else
			{
				$id = Context::getContext()->shop->id;
				$id_shop = $id ? $id : Configuration::get('PS_SHOP_DEFAULT');
				$return &= Db::getInstance()->execute('
					INSERT INTO `'._DB_PREFIX_.'lofsurvey_survey_shop` (`id_lofsurvey_survey`, `id_shop`, `position`) VALUES
					('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
					ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
			}
		}
		else
			$return &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'lofsurvey_survey_shop` (`id_lofsurvey_survey`, `id_shop`, `position`) VALUES
			('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
			ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
		return $return;
	}
	
	public function updatePosition($way, $position)
			{
				$context = Context::getContext();
				$id_shop = $context->shop->id;
				if (!$res = Db::getInstance()->ExecuteS('
					SELECT cp.`id_lofsurvey_survey`, cp.`position`
					FROM `'._DB_PREFIX_.'lofsurvey_survey_shop` cp
					WHERE cp.`id_shop` = '.(int)$id_shop.'
					ORDER BY cp.`position` ASC'
				))
					return false;
				foreach ($res AS $survey)
					if ((int)($survey['id_lofsurvey_survey']) == (int)($this->id))
						$movedSurvey = $survey;

				if (!isset($movedSurvey) || !isset($position))
					return false;
				// < and > statements rather than BETWEEN operator
				// since BETWEEN is treated differently according to databases
				$result = (Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'lofsurvey_survey_shop`
					SET `position`= `position` '.($way ? '- 1' : '+ 1').'
					WHERE `position`
					'.($way
						? '> '.(int)($movedSurvey['position']).' AND `position` <= '.(int)($position)
						: '< '.(int)($movedSurvey['position']).' AND `position` >= '.(int)($position)).'
					')
				AND Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'lofsurvey_survey_shop`
					SET `position` = '.(int)($position).'
					WHERE `id_shop` = '.(int)$id_shop.'
					AND `id_lofsurvey_survey`='.(int)($movedSurvey['id_lofsurvey_survey'])));
				return $result;
		}
	
	}
	
}
