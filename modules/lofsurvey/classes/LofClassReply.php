<?php
/*
  * @module 
  * @author 
  * @copyright 
  * @version 1.0
*/

if (!defined('_CAN_LOAD_FILES_') AND _PS_VERSION_ > '1.3')
	exit;

if(!class_exists('LofClassReply')) {	
	class LofClassReply extends ObjectModel
	{	
		public $id;
		public $title;
		public $active = 1;
		public $position;
		public $date_add;
		public $count;
		public $id_lofsurvey_survey;
		
		public static $definition = array(
		'table' => 'lofsurvey_reply',
		'primary' => 'id_lofsurvey_reply',
		'multilang' => true,
		'fields' => array(
			'active' =>			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			'id_lofsurvey_survey'=> array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
			'date_add' =>		array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'title' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255),
		)
		);
		
		public function __construct($id = NULL, $id_lang = NULL){
			parent::__construct($id, $id_lang);
		}

	public function add($autodate = true, $null_values = false)
	{
			$context = Context::getContext();
			$id_shop = $context->shop->id;
			$res = parent::add($autodate, $null_values);
				$position = $this->getLastPosition($id_shop);
				if (!$position)
					$position = 1;
				$this->addPosition($position, $id_shop);
			return $res;

	}
	
	public function delete()
	{
		$res = true;
		$res &= parent::delete();
		return $res;
	}
	
	public  function getLastPosition($id_shop)
		{
			return (int)(Db::getInstance()->getValue('
			SELECT MAX(`position`)
			FROM `'._DB_PREFIX_.'lofsurvey_reply_shop`
			WHERE `id_shop` = '.(int)$id_shop) + 1);
		}	
		
	public function addPosition($position, $id_shop = null)
	{
		$return = true;
		if (is_null($id_shop))
		{
			if (Shop::getContext() != Shop::CONTEXT_SHOP)
				foreach (Shop::getContextListShopID() as $id_shop)
					$return &= Db::getInstance()->execute('
						INSERT INTO `'._DB_PREFIX_.'lofsurvey_reply_shop` (`id_lofsurvey_reply`, `id_shop`, `position`) VALUES
						('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
						ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
			else
			{
				$id = Context::getContext()->shop->id;
				$id_shop = $id ? $id : Configuration::get('PS_SHOP_DEFAULT');
				$return &= Db::getInstance()->execute('
					INSERT INTO `'._DB_PREFIX_.'lofsurvey_reply_shop` (`id_lofsurvey_reply`, `id_shop`, `position`) VALUES
					('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
					ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
			}
		}
		else
			$return &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'lofsurvey_reply_shop` (`id_lofsurvey_reply`, `id_shop`, `position`) VALUES
			('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
			ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
		return $return;
	}	
	
	
	public function updatePosition($way, $position)
			{
				$context = Context::getContext();
				$id_shop = $context->shop->id;
				if (!$res = Db::getInstance()->ExecuteS('
					SELECT cp.`id_lofsurvey_reply`, cp.`position`
					FROM `'._DB_PREFIX_.'lofsurvey_reply_shop` cp
					WHERE cp.`id_shop` = '.(int)$id_shop.'
					ORDER BY cp.`position` ASC'
				))
					return false;
				foreach ($res AS $reply)
					if ((int)($reply['id_lofsurvey_reply']) == (int)($this->id))
						$movedReply = $reply;

				if (!isset($movedReply) || !isset($position))
					return false;
				// < and > statements rather than BETWEEN operator
				// since BETWEEN is treated differently according to databases
				$result = (Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'lofsurvey_reply_shop`
					SET `position`= `position` '.($way ? '- 1' : '+ 1').'
					WHERE `position`
					'.($way
						? '> '.(int)($movedReply['position']).' AND `position` <= '.(int)($position)
						: '< '.(int)($movedReply['position']).' AND `position` >= '.(int)($position)).'
					')
				AND Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'lofsurvey_reply_shop`
					SET `position` = '.(int)($position).'
					WHERE `id_shop` = '.(int)$id_shop.'
					AND `id_lofsurvey_reply`='.(int)($movedReply['id_lofsurvey_reply'])));
				return $result;
		}
	
	public static function getSurveys($active = null)
	{
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$id_lang = $context->language->id;
		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_lofsurvey_survey` as id_survey, hss.`position`, hs.`active`,hs.`total`, hssl.`title`
			FROM '._DB_PREFIX_.'lofsurvey_survey hs
			LEFT JOIN '._DB_PREFIX_.'lofsurvey_survey_shop hss ON (hs.id_lofsurvey_survey = hss.id_lofsurvey_survey)
			LEFT JOIN '._DB_PREFIX_.'lofsurvey_survey_lang hssl ON (hss.id_lofsurvey_survey = hssl.id_lofsurvey_survey)
			WHERE hss.`id_shop` = '.(int)$id_shop.' AND hssl.`id_lang` = '.(int)$id_lang.($active ? ' AND hs.`active` = 1' : ' ').'
			ORDER BY hss.position');
	}
	
	public static function getReply($id_survey,$active = null)
	{
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$id_lang = $context->language->id;
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_lofsurvey_reply` as id_reply, hs.`position`, hss.`active`, hss.`count`, hssl.`title`
			FROM '._DB_PREFIX_.'lofsurvey_survey_reply_shop hs
			LEFT JOIN '._DB_PREFIX_.'lofsurvey_reply hss ON (hs.id_lofsurvey_reply = hss.id_lofsurvey_reply)
			LEFT JOIN '._DB_PREFIX_.'lofsurvey_survey_reply_lang hssl ON (hss.id_lofsurvey_reply = hssl.id_lofsurvey_reply)
			WHERE hs.`id_shop` = '.(int)$id_shop.' AND hss.`id_lofsurvey_survey`= '.(int)$id_survey.' AND hssl.`id_lang` = '.(int)$id_lang.($active ? ' AND hss.`active` = 1' : ' ').'
			ORDER BY hs.position');
	}
	public static function getAllReply($active = null)
	{
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$id_lang = $context->language->id;
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT hs.`id_lofsurvey_reply` as id_reply, hs.`position`, hss.`active`,hss.`id_lofsurvey_survey` as id_survey, hss.`count`, hssl.`title`
			FROM '._DB_PREFIX_.'lofsurvey_reply_shop hs
			LEFT JOIN '._DB_PREFIX_.'lofsurvey_reply hss ON (hs.id_lofsurvey_reply = hss.id_lofsurvey_reply)
			LEFT JOIN '._DB_PREFIX_.'lofsurvey_reply_lang hssl ON (hss.id_lofsurvey_reply = hssl.id_lofsurvey_reply)
			WHERE hs.`id_shop` = '.(int)$id_shop.'  AND hssl.`id_lang` = '.(int)$id_lang.($active ? ' AND hss.`active` = 1' : ' ').'
			ORDER BY hs.position');
	}
	public static function getCountReply($id_reply)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT `count` 
		FROM `'._DB_PREFIX_.'lofsurvey_reply`
		WHERE `id_lofsurvey_reply` = '.(int)$id_reply
		);
	}

	public static function getMaxDateReply($id_survey, $id_customer = false, $id_guest = false)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
            SELECT `date_add`
            FROM  `'._DB_PREFIX_.'lofsurvey_guest`
            WHERE 1 '.($id_guest ? ' AND `id_guest`= '.(int)$id_guest : '').($id_customer ? ' AND `id_customer`= '.(int)$id_customer : '').'
            AND `id_lofsurvey_survey`='.(int)$id_survey);
	}
		
	public static function getIdSurvey($id_reply)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT `id_lofsurvey_survey` 
		FROM `'._DB_PREFIX_.'lofsurvey_reply` 
		WHERE id_lofsurvey_reply = '.$id_reply.'
			');
	}
	
	}
}
