<?php

include_once( dirname(__FILE__)."/../../classes/LofClassReply.php");
class AdminLofReplyController extends AdminController
{
	private $_lofsurvey;
    public function __construct(){
		$this->table = 'lofsurvey_reply';
	 	$this->className = 'LofClassReply';
		$this->identifiersDnd = array('id_lofsurvey_reply' => 'id_lofsurvey_reply_to_move');
		$this->position_identifier = 'id_lofsurvey_reply_to_move';
	 	$this->identifier = 'id_lofsurvey_reply';
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		Shop::addTableAssociation('lofsurvey_reply', array('type' => 'shop'));
		$this->lang = true;
		parent::__construct(); 
		
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		$this->_select = 'hss.`position` as position';
        $this->_join = 'INNER JOIN `'._DB_PREFIX_.'lofsurvey_reply_shop` hss ON (a.id_lofsurvey_reply = hss.id_lofsurvey_reply AND hss.`id_shop` = '.(int)$id_shop.')';
        $this->fields_list = array(
		'id_lofsurvey_reply' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 20),
		'title' => array('title' => $this->l('Title'),'width' => 300),
		'date_add' => array('title' => $this->l('Date Add'), 'width' => 200),
		'count' => array('title' => $this->l('Vote'), 'width' => 200),
		'position' => array('title' => $this->l('Position'), 'width' => 40, 'filter_key' => 'sa!position', 'align' => 'center', 'position' => 'position'),
		'active' => array('title' => $this->l('Displayed'),'width' => 40, 'active' => 'status', 'align' => 'center','type' => 'bool', 'orderby' => false));
		$this->_lofsurvey = AdminLofSurveyController::getCurrentLofSurvey();
		$this->_filter = 'AND a.`id_lofsurvey_survey` = '.(int)($this->_lofsurvey->id);
		$this->getlanguages();
		$this->_defaultFormLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
    }
	public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
	{
		$alias = 'hss';
		parent::getList($id_lang, $alias.'.position', $order_way, $start, $limit);
		
	}
	
	public function postProcess($token = null)
	{
		global $currentIndex;
        if (Tools::isSubmit('submitAdd'.$this->table)){
            if ($id_lofsurvey_reply = (int)(Tools::getValue('id_lofsurvey_reply'))){
               if ($this->tabAccess['edit'] === '1'){
                    $this->validateRules();
                    $id = (int)(Tools::getValue($this->identifier));
                    if (isset($id) AND $id){
                        if (!sizeof($this->errors)){
                            $object = new $this->className($id);
                            $this->copyFromPost($object, $this->table);
                            if ( $object->update() ){
                                if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
                                    Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=3&update'.$this->table.'&token='.$token);
                                else
                                    Tools::redirectAdmin($currentIndex.'&id_lofsurvey_survey='.(!empty($_REQUEST['id_lofsurvey_survey'])?$_REQUEST['id_lofsurvey_survey']:'1').'&viewlofsurvey_survey&token='.$token);
                            }
                        }
                    }
               }else
                    $this->errors[] = Tools::displayError('You do not have permission to edit here.');
            }
            else
            {
                if ($this->tabAccess['add'] === '1'){
                    $object = new $this->className();
                    $this->copyFromPost($object, $this->table);
                    if ($object->add())
                    {
                        if (Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
                            Tools::redirectAdmin($currentIndex.'&'.$this->identifier.'='.$object->id.'&conf=3&update'.$this->table.'&token='.$token);
                        // Save and back to parent
                        else
                            Tools::redirectAdmin($currentIndex.'&id_lofsurvey_survey='.(!empty($_REQUEST['id_lofsurvey_survey'])?$_REQUEST['id_lofsurvey_survey']:'1').'&viewlofsurvey_survey&token='.$token);
                    }
                    else
                        $this->errors[] = Tools::displayError('You do not have permission to add here..');
                }
                else
                    $this->errors[] = Tools::displayError('You do not have permission to add here.');
            }
        }
		//Delete object
		elseif (Tools::isSubmit('delete'.$this->table)){
			if ($this->tabAccess['delete'] === '1'){
				if (Validate::isLoadedObject($object = $this->loadObject())){
					$id = $object->id;
					$result_survey = LofClassReply::getIdSurvey($id);
					$id_lofsurvey_survey = (count($result_survey) > 0) ?  $result_survey[0]['id_lofsurvey_survey'] : 1;
					$survey = new LofClassSurvey($id_lofsurvey_survey);
					$object = new $this->className($id);
					$replies = LofClassReply::getCountReply($id);
					$survey->total = $survey->total - $replies[0]['count'];
					if ($object->delete() && $survey->update() ){
						// process code to delete
						Tools::redirectAdmin($currentIndex.'&id_lofsurvey_survey='.(($id_lofsurvey_survey)? (int)$id_lofsurvey_survey :'1').'&viewlofsurvey_survey&token='.$token);
					}
					$this->errors[] = Tools::displayError('An error occurred during deletion.');
				}else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		
		  /* Delete multiple objects */
		elseif (Tools::getValue('submitBulkdelete'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.'Box']))
				{
					$object = new $this->className();
					$arr_id = Tools::getValue($this->table.'Box');
					$id_reply = count($arr_id) ? $arr_id[0] : 0 ;
					$result_survey = LofClassReply::getIdSurvey($id_reply);
					$id_lofsurvey_survey = (count($result_survey) > 0) ?  $result_survey[0]['id_lofsurvey_survey'] : 1;
					if (isset($object->noZeroObject) AND
						// Check if all object will be deleted
						(sizeof(call_user_func(array($this->className, $object->noZeroObject))) <= 1 OR sizeof($_POST[$this->table.'Box']) == sizeof(call_user_func(array($this->className, $object->noZeroObject)))))
						$this->errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						$result = true;
						if ($this->deleted)
						{
							foreach(Tools::getValue($this->table.'Box') as $id)
							{
								$toDelete = new $this->className($id);
								$toDelete->deleted = 1;
								$result = $result AND $toDelete->update();
							}
						}
						else
							$result = $object->deleteSelection(Tools::getValue($this->table.'Box'));

						if ($result)
						{
							Tools::redirectAdmin($currentIndex.'&id_lofsurvey_survey='.(($id_lofsurvey_survey)? (int)$id_lofsurvey_survey :'1').'&viewlofsurvey_survey&token='.$token);
						}
						$this->errors[] = Tools::displayError('An error occurred while deleting selection.');
					}
				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		
		
		elseif (isset($_GET['status'.$this->table]) && Tools::isSubmit('id_lofsurvey_reply'))
		{ 
			$id = (int)(Tools::getValue($this->identifier));
			if (isset($id) AND $id){
			$result_survey = LofClassReply::getIdSurvey($id);
			$id_lofsurvey_survey = (count($result_survey) > 0) ?  $result_survey[0]['id_lofsurvey_survey'] : 1;
			$survey = new LofClassSurvey($id_lofsurvey_survey);
			$object = new $this->className($id);
			$replies=LofClassReply::getCountReply($id);
			if ($object->active == 0){
				$object->active = 1;
				$survey->total = $survey->total + $replies[0]['count'];
			}else{
				$object->active = 0;
				$survey->total = $survey->total - $replies[0]['count'];
			}
				
			if($object->update() && $survey->update())	
				Tools::redirectAdmin($currentIndex.'&id_lofsurvey_survey='.(($id_lofsurvey_survey)? (int)$id_lofsurvey_survey :'1').'&viewlofsurvey_survey&token='.$token);
			}
		}
        if (Tools::isSubmit('submitReset'.$this->table)){
            unset($_POST);
        }

	    return	parent::postProcess(true);
	}
	
	public function initBreadcrumbs() {
		$this->breadcrumbs[] = $this->l('Replies');
	}
	
	
	public function renderList() {
		$this->initToolbar();
		return parent::renderList();
	}

	public function renderForm()
	{
		$id_lofsurvey_survey = Tools::getValue('id_lofsurvey_survey') ? (int)Tools::getValue('id_lofsurvey_survey') : 0 ;
        if (!$this->loadObject(true))
			return;
		if (Validate::isLoadedObject($this->object))
			$this->display = 'edit';
		else
			$this->display = 'add';
		$this->initToolbar();
		$this->fields_form = array(
			'tinymce' => true,
			'legend' => array(
				'title' => $this->l('Reply'),
				'image' => '../img/admin/quick.gif'
				),
			'input' => array(
				// custom template
				array(
					'type' => 'text',
					'label' => $this->l('Title:'),
					'name' => 'title',
					'lang' => true,
					'required' => true,
					'class' => '',
					'size' => 150
				),
				
				array(
					'type' => 'hidden',
					'name' => 'id_lofsurvey_survey',
					'value' => $id_lofsurvey_survey
				),
				
				array(
					'type' => 'radio',
					'label' => $this->l('Active:'),
					'name' => 'active',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						)
					),
				),

			),
			
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);
		
		$this->tpl_form_vars = array(
			'active' => $this->object->active,
			'PS_ALLOW_ACCENTED_CHARS_URL', (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL')
		);
		
        return parent::renderForm();
	}
	
	
	public function initToolbar()
	{
		$id_lofsurvey_reply = Tools::getValue('id_lofsurvey_reply') ? (int)Tools::getValue('id_lofsurvey_reply') : 0;
		$result_survey = LofClassReply::getIdSurvey($id_lofsurvey_reply);
		$id_lofsurvey_survey = (count($result_survey) > 0) ? $result_survey[0]['id_lofsurvey_survey'] : 1;
		$back_renderform = self::$currentIndex.'&id_lofsurvey_survey='.$id_lofsurvey_survey.'&viewlofsurvey_survey&token='.$this->token;
		switch ($this->display)
		{
			case 'add':
			case 'edit':
				$this->toolbar_btn['save'] = array(
					'href' => '#',
					'desc' => $this->l('Save')
				);
				$this->toolbar_btn['save-and-stay'] = array(
					'href' => '#',
					'desc' => $this->l('Save and Stay')
				);
				$this->toolbar_btn['back'] = array(
					'href' => $back_renderform,
					'desc' => $this->l('Back to list')
				);
			case 'view':
				$back = Tools::safeOutput(Tools::getValue('back', ''));
				if (empty($back))
					$back = self::$currentIndex.'&id_lofsurvey_survey='.$id_lofsurvey_survey.'&viewlofsurvey_survey&token='.$this->token;
				if (!Validate::isCleanHtml($back))
					die(Tools::displayError());
				if (!$this->lite_display)
					$this->toolbar_btn['back'] = array(
						'href' => $back,
						'desc' => $this->l('Back to list')
					);
				break;
			case 'options':
				$this->toolbar_btn['save'] = array(
					'href' => '#',
					'desc' => $this->l('Save')
				);
				break;
			case 'view':
				break;
			default: // list
				$id_survey = Tools::getValue('id_lofsurvey_survey') ? (int)Tools::getValue('id_lofsurvey_survey') : 1;
				$this->toolbar_btn['new'] = array(
					'href' => self::$currentIndex.'&amp;add'.$this->table.'&amp;id_lofsurvey_survey='.$id_survey.'&amp;token='.$this->token,
					'desc' => $this->l('Add new')
				);
		}
	}
	
}
