<?php
if(!class_exists("AdminLofReplyController"))
	require_once(dirname(__FILE__).'/AdminLofReply.php');
include_once( dirname(__FILE__)."/../../classes/LofClassSurvey.php");
class AdminLofSurveyController extends AdminController
{
	public $_defaultFormLanguage;
	protected $adminReply;
	protected static $_lofsurvey = null;
    public function __construct(){

		global $cookie;
		$id_lofsurvey_survey = (int)Tools::getValue('id_lofsurvey_survey', 1);
		self::$_lofsurvey = new LofClassSurvey($id_lofsurvey_survey);
        $this->table = 'lofsurvey_survey';
	 	$this->className = 'LofClassSurvey';
		$this->identifiersDnd = array('id_lofsurvey_survey' => 'id_lofsurvey_survey_to_move');
		$this->position_identifier = 'id_lofsurvey_survey_to_move';
	 	$this->identifier = 'id_lofsurvey_survey';
        $this->lang = true;
        $this->context = Context::getContext();
		$this->adminReply = new AdminLofReplyController();
		parent::__construct();
		
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		$this->_select = 'hss.`position` as position';
        $this->_join = 'INNER JOIN `'._DB_PREFIX_.'lofsurvey_survey_shop` hss ON (a.id_lofsurvey_survey = hss.id_lofsurvey_survey AND hss.`id_shop` = '.(int)$id_shop.')';
        $this->fields_list = array(
		'id_lofsurvey_survey' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 20),
		'title' => array('title' => $this->l('Title'),'width' => 300),
		'total' => array('title' => $this->l('Total Vote'), 'width' => 200),
		'date_add' => array('title' => $this->l('Date Add'), 'width' => 200),
		'position' => array(
				'title' => $this->l('Position'),
				'width' => 40,
				'filter_key' => 'sa!position',
				'align' => 'center',
				'position' => 'position'
				),
		'active' => array('title' => $this->l('Displayed'),'width' => 40, 'active' => 'status', 'align' => 'center', 'type' => 'bool', 'orderby' => false),);
		$this->getlanguages();
		$this->_defaultFormLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
    }

	public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
	{
		$alias = 'hss';
		parent::getList($id_lang, $alias.'.position', $order_way, $start, $limit);
		
	}

	public static function getCurrentLofSurvey()
	{
		return self::$_lofsurvey;
	}
	
	public function viewAccess($disable = false)
	{
		$result = parent::viewAccess($disable);
		$this->adminReply->tabAccess = $this->tabAccess;
		return $result;
	}
	
    public function renderList(){
		$this->initToolbar();
		$this->addRowAction('view');
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		if(!Tools::isSubmit('addlofsurvey_reply') && !Tools::isSubmit('updatelofsurvey_reply'))
			return parent::renderList();
			
    }
	
	public function renderView()
	{
		$this->initToolbar();
		$this->adminReply = new AdminLofReplyController();	
		$this->adminReply->token = $this->token;
		$id_lofsurvey_survey = (int)Tools::getValue('id_lofsurvey_survey');
		$this->adminReply->id_lofsurvey_survey = $id_lofsurvey_survey;
		$content = parent::renderView();
		
		//Poll
		$html = '';
		$html .='
            <style type="text/css">
            .lof_result{margin-left: 5px; width: 95%;}
            .lof_result li{width:100%; list-style:none; min-height:40px; margin-top:5px; display:inline-block;}
            .title_reply{width:75%; float:left; min-height: 20px;}
            .form-poll{width:100%; float:left;}
            .result_reply{border-radius: 10px 10px 10px 10px; float: left; height: 15px;}
            .lof_result li span{float:left; margin-left:5px;}
            .lof_bgreply{background:#b3a5b5; border-radius:10px 10px 10px 10px; height:15px;}
            .lof_totalreply {margin-left:20px;}
        </style>';
		$items= new LofClassSurvey($id_lofsurvey_survey);
		$item = LofClassReply::getAllReply(1);
		if ($items) {
			$lof_result=array();
            foreach($item as $rows){
                if($rows['id_survey'] == $id_lofsurvey_survey){
                    $result= array(
                        'title'=> $rows['title'],
                        'result'=> ($items->total ? round(($rows['count']/$items->total)*100,2) : 0),
                        'total'=> $items->total
                        );
                    $lof_result[]=$result;
                }
            }
			$html .= ' <ul class="lof_result" style="width: 700px;margin:20px;">';
            foreach($lof_result as $poll)
            {
                $html .= '<li><div class="lof_title_reply" style="width:100%;height:50px">
                    <div class="title_reply">'.$poll['title'].'</div>
                    <div class="form-poll">
                        <div class="lof_bgreply" style="width:75%;float:left">
                        <div class="result_reply" style="width:'.$poll['result'].'% ; background:#3f38a1;">
                        </div>
                        </div>
                        <div><span>'.$poll['result'].'%</span></div>
                    </div>
                    </div>
                </li>';
            }
		$html .= '</ul>
				<div class="lof_totalreply">
				<h3>'.$this->l('Total:').' <b>'.$items->total.'</b> '.($items->total > 1 ? $this->l('votes') : $this->l('vote')).'</h3>';
		$html .= '</div><div class="clear space"></div>';
		}		
		$content .= $html ;
		//end poll	
		$content .= $this->adminReply->renderList();
		return $content;
	}	
	
	public function initToolbarTitle()
	{
		parent::initToolbarTitle();
		$context = Context::getContext();
		$id_lang = (int)$context->cookie->id_lang;
		if ($survey = $this->loadObject(true))
			if ((bool)$survey->id && $this->display != 'list' && isset($this->toolbar_title[2]))
				$this->toolbar_title[2] = $this->toolbar_title[2].' ('.$survey->title[$id_lang].')';
	}
	
	public function postProcess()
	{
		$this->viewAccess();
		if (Tools::isSubmit('addlofsurvey_reply') || Tools::isSubmit('updatelofsurvey_reply') ){
            $this->adminReply->token = $this->token;
            $this->content .= $this->adminReply->renderForm();
		}
		if( Tools::getValue('action') == 'updatePositions'  && Tools::getValue('lofsurvey_survey') ){
			$id_survey_to_move = (int)(Tools::getValue('id'));
			$way = (int)(Tools::getValue('way'));
			$positions = Tools::getValue('lofsurvey_survey');
			if (is_array($positions))
				foreach ($positions as $key => $value)
				{
					$pos = explode('_', $value);
					if ((isset($pos[1])) && ($pos[2] == $id_survey_to_move))
					{
						$position = $key + 1;
						break;
					}
				}
			$survey = new LofClassSurvey($id_survey_to_move);
			if (Validate::isLoadedObject($survey))
			{
				if (isset($position) && $survey->updatePosition($way, $position))
				{
					die(true);
				}
				else
					die('{"hasError" : true, errors : "Can not update surveys position"}');
			}
			else
				die('{"hasError" : true, "errors" : "This survey can not be loaded"}');
		}elseif( Tools::getValue('action') == 'updatePositions'  && Tools::getValue('lofsurvey_reply') ){
			$id_reply_to_move = (int)(Tools::getValue('id'));
			$way = (int)(Tools::getValue('way'));
			$positions = Tools::getValue('lofsurvey_reply');
			if (is_array($positions))
				foreach ($positions as $key => $value)
				{
					$pos = explode('_', $value);
					if ((isset($pos[1])) && ($pos[2] == $id_reply_to_move))
					{
						$position = $key + 1;
						break;
					}
				}
			$reply = new LofClassReply($id_reply_to_move);
			if (Validate::isLoadedObject($reply))
			{
				if (isset($position) && $reply->updatePosition($way, $position))
				{
					die(true);
				}
				else
					die('{"hasError" : true, errors : "Can not update surveys position"}');
			}
			else
				die('{"hasError" : true, "errors" : "This reply can not be loaded"}');
		}
		elseif (Tools::isSubmit('submitBulkdeletelofsurvey_reply')
			|| Tools::isSubmit('previewSubmitAddcmsAndPreview')
			|| Tools::isSubmit('submitAddlofsurvey_reply')
			|| Tools::isSubmit('deletelofsurvey_reply')
			|| (Tools::isSubmit('statuslofsurvey_reply') && Tools::isSubmit('id_lofsurvey_reply'))
			|| (Tools::isSubmit('way') && Tools::isSubmit('id_lofsurvey_reply')) && (Tools::isSubmit('position')))
				$this->adminReply->postProcess($this->token);

		parent::postProcess();
	}
	
	
	public function renderForm() {
        if (!$this->loadObject(true))
			return;
		if (Validate::isLoadedObject($this->object))
			$this->display = 'edit';
		else
			$this->display = 'add';
		$this->initToolbar();
		$this->fields_form = array(
			'tinymce' => true,
			'legend' => array(
				'title' => $this->l('Survey'),
				'image' => '../img/admin/quick.gif'
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Title:'),
					'name' => 'title',
					'lang' => true,
					'required' => true,
					'class' => '',
					'size' => 150
				),
				
				array(
					'type' => 'radio',
					'label' => $this->l('Active:'),
					'name' => 'active',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						)
					),
				),
				
			),
			
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);

		$this->tpl_form_vars = array(
			'active' => $this->object->active,
			'PS_ALLOW_ACCENTED_CHARS_URL', (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL')
		);

        return parent::renderForm();
	}
	
	public function initToolbar()
	{
		if (Tools::getValue('id_lofsurvey_survey') && Tools::isSubmit('viewlofsurvey_survey'))
		{
			$this->toolbar_btn['edit'] = array(
				'href' => self::$currentIndex.'&amp;update'.$this->table.'&amp;id_lofsurvey_survey='.(int)Tools::getValue('id_lofsurvey_survey').'&amp;token='.$this->token,
				'desc' => $this->l('Edit')
			);
		}
		$id_lofsurvey_survey = Tools::getValue('id_lofsurvey_survey') ? (int)Tools::getValue('id_lofsurvey_survey') : 0;
		switch ($this->display)
		{
			case 'add':
			case 'edit':
				// Default save button - action dynamically handled in javascript
				$this->toolbar_btn['save'] = array(
					'href' => '#',
					'desc' => $this->l('Save')
				);
				$this->toolbar_btn['save-and-stay'] = array(
					'href' => '#',
					'desc' => $this->l('Save and Stay')
				);
				//no break
			case 'view':
				// Default cancel button - like old back link
				$back = Tools::safeOutput(Tools::getValue('back', ''));
				if (empty($back))
					$back = self::$currentIndex.'&token='.$this->token;
				if (!Validate::isCleanHtml($back))
					die(Tools::displayError());
				if (!$this->lite_display)
					$this->toolbar_btn['back'] = array(
						'href' => $back,
						'desc' => $this->l('Back to list')
					);
				break;
			case 'options':
				$this->toolbar_btn['save'] = array(
					'href' => '#',
					'desc' => $this->l('Save')
				);
				break;
			default: // list
				$this->toolbar_btn['new'] = array(
					'href' => self::$currentIndex.'&amp;add'.$this->table.'&amp;id_lofsurvey_survey='.$id_lofsurvey_survey.'&amp;token='.$this->token,
					'desc' => $this->l('Add new')
				);
		}
	}
	
}