<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}    
/**
 * lofdeal Class
 */
require( _PS_ROOT_DIR_.'/modules/lofsurvey/libs/Params.php' );
require( _PS_ROOT_DIR_.'/modules/lofsurvey/classes/LofClassSurvey.php' );
require( _PS_ROOT_DIR_.'/modules/lofsurvey/classes/LofClassReply.php' );
class lofsurvey extends Module
{
	private $_html = '';
	
	private $_configs = '';
	
	protected $params = null; 
	/**
	 * @var array $_postErrors;
	 *
	 * @access private;
	 */
	private $_postErrors = array();
	/**
	 * @var string $__tmpl is stored path of the layout-theme;
	 *
	 * @access private 
	 */	
	const INSTALL_SQL_FILE = 'install.sql';
    const UNINSTALL_SQL_FILE = 'uninstall.sql';
   /**
    * Constructor 
    */
	function __construct(){
        $this->name = 'lofsurvey';
        $this->secure_key = Tools::encrypt($this->name);
		parent::__construct();			
		$this->tab = 'LandOfCoder';				
		$this->author = 'LandOfCoder';
		$this->version = '1.0';
		$this->displayName = $this->l('Lof Survey Module');
		$this->description = $this->l('The module allow create, edit and manager surveys & polls for PrestaShop.');
		$this->_prepareForm();
		$this->params =  new LofSurveyParams( $this, $this->name , $this->_configs  );
	}

	public function _prepareForm(){
		$this->_configs = array(
			'theme'  => 'default',
			// source
			'source' => 'images',
			'types' =>'radio',
			'number_vote' =>'1',
			'hideform' =>'2',
			'user' => '0',
			'back' => '1',
			'result' => '1',
			'lof_color'=>'#9837a1'
		);
				
	}
	
	public function getParams(){
		return $this->params;
	}
	
	function install(){
		if (!parent::install())
			return false;
        if(!$this->installTable()) return false;
        if(!$this->installTabSurvey()) return false;
		if(!$this->registerHook('leftColumn'))
			return false;
		return true;
	}
	/**
    * Install Table
    */
    public function installTable(){
        if (!file_exists(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE))
			return (false);
		else if (!$sql = file_get_contents(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE))
			return (false);
		$sql = str_replace('ps_', _DB_PREFIX_, $sql);
		$sql = preg_split("/;\s*[\r\n]+/",$sql);
		foreach ($sql as $query){
			if(!empty($query)){
				if (!Db::getInstance()->Execute(trim($query)))
					return (false);
			}
		}
        return true;
    }
	
	 private function installTabSurvey(){
        $languages = Language::getLanguages(false);
		foreach ($languages as $language) {
			$arrName[$language['id_lang']] = 'Lof Manage Survey ';
		}
        $idTab = Tab::getIdFromClassName('AdminParentModules');
        $this->installModuleTab('AdminLofSurvey', $arrName, $idTab);
		return true;
    }
	
	 private function installModuleTab($tabClass, $tabName, $idTabParent)
	{
		@copy(_PS_MODULE_DIR_.$this->name.'/logo.gif', _PS_IMG_DIR_.'t/'.$tabClass.'.gif');
		$tab = new Tab();
		$tab->name = $tabName;
		$tab->class_name = $tabClass;
		$tab->module = $this->name;
		$tab->id_parent = $idTabParent;
		if(!$tab->save())
			return false;

		return true;
	}
	
    public function uninstall(){
        if (!parent::uninstall())return false;
        if(!$this->uninstallTable()) return false;
        if(!$this->uninstallModuleTab('AdminLofSurvey')) return false;
        return true;
	}
	  /**
    * Uninstall Table
    */
    public function uninstallTable(){
        if (!file_exists(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE))
			return (false);
		else if (!$sql = file_get_contents(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE))
			return (false);
		$sql = str_replace('ps_', _DB_PREFIX_, $sql);
		$sql = preg_split("/;\s*[\r\n]+/",$sql);
        foreach ($sql as $query)
			if (!Db::getInstance()->Execute(trim($query)))
				return (false);
        return true;
    }
    
	private function uninstallModuleTab($tabClass)
	{
		$idTab = Tab::getIdFromClassName($tabClass);
		if($idTab != 0)
		{
			$tab = new Tab($idTab);
			$tab->delete();
			return true;
		}
		return false;
	}
	
	public function getContent()
	{
		$this->_html .= '<h2>'.$this->displayName.'.</h2>';
		if (Tools::isSubmit('submitSurveyr')){
			$res = $this->getParams()->batchUpdate( $this->_configs );
			$this->getParams()->refreshConfig(); 
			if (!$res)
                $this->_html .= $this->displayError($this->l('Configuration could not be updated'));
            else
			    $this->_html .= $this->displayConfirmation($this->l('Configuration updated'));
		} 
		$this->_displayForm();
		return $this->_html;
	}

	private function _displayForm()
	{
		$this->context->controller->addCSS($this->_path.'themes/colorpicker.css');
		$params = $this->params;
		/* Gets Surveys */
		require_once ( dirname(__FILE__).'/form.php' );
	}
	
	function hookHeader($params){
		return $this->hookDisplayTop();
	}

	public function hookRightColumn($params)
	{
		return $this->hookDisplayTop();
	}

	public function hookLeftColumn($params)
	{
		return $this->hookDisplayTop();
	}

	public function ajaxGetResult($id_survey){
		$lof_config = array();
		foreach( $this->_configs as $key => $config ){
			$lof_config[$key] = $this->getParams()->get( $key, $config );
		}
		$items = LofClassReply::getSurveys(1);
        $item = LofClassReply::getAllReply(1);
        $id_customer = $this->context->customer->id;
        $id_guest = $this->context->cookie->id_guest;
        if(!$items || !$item)
            return false;
		$lof_result = array();
        $check = true;
		foreach($items as $row){
			if($row['id_survey'] == $id_survey){
                $check = $this->checkCanVote($row['id_survey'], $id_customer, $id_guest);
				$title_survey = $row['title'];
				$total_survey = $row['total'];
                foreach($item as $rows){
                    if($rows['id_survey'] == $row['id_survey']){
                        $res = ($row['total'] ? round(($rows['count']/$row['total'])*100,2) : 0);
                        $result = array(
                            'title' => $rows['title'],
                            'result' => $res,
                            'count' => $rows['count'],
                            );
                        $lof_result[] = $result;
                    }
                }
			}
		}

		$this->smarty->assign('lofsurvey_idsurvey', $id_survey);
		$this->smarty->assign('title_survey', $title_survey);
		$this->smarty->assign('total_survey', $total_survey);
		$this->smarty->assign('lofsurvey_poll', $lof_result);
		$this->smarty->assign('lofsurvey_config', $lof_config);
		$this->smarty->assign('lofcheck', $check);
		$theme = $this->getParams()->get('theme');
		$this->context->controller->addCSS($this->_path.'themes/style.css');
		return $this->display(__FILE__, 'themes/'.$theme.'/poll.tpl' );
	}
	
	public function hookDisplayTop(){
		$configs = array();
		foreach( $this->_configs as $key => $config ){
			$configs[$key] = $this->getParams()->get( $key, $config );
		}
		$items = LofClassReply::getSurveys(1);
        $item = LofClassReply::getAllReply(1);
        if(!$items || !$item)
            return false;
        $id_customer = $this->context->customer->id;
        $id_guest = Context::getContext()->cookie->id_guest;
        $lof_result = array();
        foreach($items as &$row){
            $check = $this->checkCanVote($row['id_survey'], $id_customer, $id_guest);
            $row['check'] = (int)$check;
            if(!$check){
                foreach($item as $rows){
                    if($rows['id_survey'] == $row['id_survey']){
                        $res = ($row['total'] ? round(($rows['count']/$row['total'])*100,2) : 0);
                        $result = array(
                            'title' => $rows['title'],
                            'result' => $res,
                            'total' => $row['total'],
                            'count' => $rows['count'],
                            'id' => $row['id_survey']
                        );
                        $lof_result[] = $result;
                    }
                }
            }
        }

		$this->smarty->assign('lofsurvey_poll', $lof_result);
		$this->smarty->assign('lofsurvey_items', $items);	
		$this->smarty->assign('lofsurvey_item', $item);
		$this->smarty->assign('lofsurvey_configs', $configs);
		$this->smarty->assign('mod_id', $this->id);
        if(isset($this->context->customer->id))
            $lof_customer = 1;
        else
            $lof_customer = 0;
        $this->smarty->assign('lofsurvey_customer', $lof_customer);
		$this->smarty->assign( 'lofsurvey_ajaxurl', _MODULE_DIR_.$this->name.'/ajax_lofreply.php' );
		if( file_exists(_PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/'.$this->name.'/themes/'.$this->getParams()->get('theme').'/poll.tpl' ) )
			$this->smarty->assign( 'ajaxsurvey_poll', _PS_THEME_DIR_.'/modules/'.$this->name.'/themes/'.$this->getParams()->get('theme').'/poll.tpl' );
		else
			$this->smarty->assign( 'ajaxsurvey_poll', _PS_MODULE_DIR_.$this->name.'/themes/'.$this->getParams()->get('theme').'/poll.tpl' );
		$this->smarty->assign('lofsurvey_idguest', $this->context->customer->id_guest);
		$this->smarty->assign( 'lofnfkll_secure_key', Tools::encrypt($this->name) );

		$this->context->controller->addCSS($this->_path.'themes/style.css');
        if($this->getParams()->get('theme') == 'basic')
		    $this->context->controller->addJS($this->_path.'js/highcharts.js');
		
		return $this->display(__FILE__, 'themes/default.tpl' );
	}

    public function checkCanVote($id_survey, $id_customer = 0, $id_guest = 0){
        if(!$id_guest && !$id_customer)
            return false;
        if($id_customer)
            $reply = LofClassReply::getMaxDateReply($id_survey, $id_customer);
        elseif($id_guest)
            $reply = LofClassReply::getMaxDateReply($id_survey, false, $id_guest);
        if(!$reply)
            return true;
        $number_vote = $this->getParams()->get('number_vote');
        switch($number_vote){
            default:
            case 1:
                if($reply)
                    return false;
                return true;
            break;
            case 2:
                if($reply){
                    $now = strtotime(date('Y-m-d H:i:s'));
                    $maxtime = (7*24*60*60);
                    if(($now - strtotime($reply['date_add'])) >= $maxtime)
                        return true;
                    return false;
                }
                return true;
            break;
            case 3:
                return true;
            break;
        }
    }
}