<?php
/**
 * Lof Survey Module
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) September 2012 LandOfCoder.Com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
 
/**
 * @since 1.5.0
 * @version 1.2 (2012-03-14)
 */

if (!defined('_PS_VERSION_'))
	exit;

    $this->_html .= '
    <script type="text/javascript">
        jQuery(document).ready(function(){
            $(".group").each(function(){
                var id = $(this).attr("id");
                var val = $(this).val();
                $(".group-"+id).css("display","none");
                $(".group-"+id+"-"+val).show(500);
            });
            $(".group").change(function(){
                var id = $(this).attr("id");
                var val = $(this).val();
                $(".group-"+id).hide(500);
                $(".group-"+id+"-"+val).show(500);
            });
        });
    </script>

    <form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">';

    $this->_html .= '
    <fieldset><legend><img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('General configuration').'</legend>';
    $theme = array(
        'default'=>$this->l('Default'),
        'basic'=>$this->l('Basic'),
    );
    $this->_html .= $params->selectTag( $theme, $this->l('Theme:'), 'theme', $this->getParams()->get('theme'),'','class="group"');
    $this->_html .= '<div class="group-theme group-theme-default">';
        $this->_html .= $params->inputTagColor( $this->l('Select Color:'), 'lof_color', $this->getParams()->get('lof_color'), '',' size="20" ' );
    $this->_html .= '</div>';
    $types = array(
        'radio'=>$this->l('Radio'),
        'checkbox'=>$this->l('Checkbox')
    );
    $this->_html .= $params->selectTag( $types, $this->l('Select survey type:'), 'types', $this->getParams()->get('types') );
    $number_vote = array(
        '1'=>$this->l('Only one time'),
        '2'=>$this->l('1 time a week'),
        '3'=>$this->l('Unlimited')
    );
    $this->_html .= $params->selectTag( $number_vote, $this->l('Number of votes per survey:'), 'number_vote', $this->getParams()->get('number_vote') );
    $hideform = array(
        '1'=> $this->l('Form Survey'),
        '2'=> $this->l('Result')
    );
    $this->_html .= $params->selectTag( $hideform, $this->l('Survey form type:'), 'hideform', $this->getParams()->get('hideform') );
    $this->_html .= $params->statusTag( $this->l('Only user can vote:'), 'user', $this->getParams()->get('user'), '' );
    $this->_html .= $params->statusTag( $this->l('Show "Result" buttom:'), 'result', $this->getParams()->get('result'), '' );
    $this->_html .= '
    <div class="margin-form">
        <input type="submit" class="button" name="submitSurveyr" value="'.$this->l('    Save    ').'" />
    </div>';
    $this->_html .= '</fieldset>';
    $this->_html .= '</form><br /><br />';
?>