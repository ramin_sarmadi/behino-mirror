<?php 
/**
 * $ModDesc
 * 
 * @version   $Id: file.php $Revision
 * @package   modules
 * @subpackage  $Subpackage.
 * @copyright Copyright (C) November 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */
if( !class_exists('LofSurveyParams', false) ){
class LofSurveyParams{
  
    /**
    * @var string name ;
    *
    * @access public;
    */
    public  $name   = '';	
	
    /**
    * @var string name ;
    *
    * @protected public;
    */
    protected $_data= array();
  
  
	protected $currentMod = null;
	/**
	 * Constructor
    */
	public function LofSurveyParams( $current, $name, $configs   ){
		global $cookie;
		$this->currentMod = $current; 
		$this->name = $name;
		
		
		foreach( $configs as $key => $config ){
			$d = Configuration::get( strtoupper($this->name.'_'.($key)) );
			$d = $d != ""? $d:$config;
			
			$this->_data[strtoupper($this->name.'_'.$key)] = $d ;
		}
	}
	
	public function refreshConfig(){
		foreach( $this->_data as $key => $value ){
			$this->_data[$key] = Configuration::get( $key );
		}
		return $this;
	}
	/**
	 * Get configuration's value
	 */
	public function get( $name, $default="" ){
		
		$name = strtoupper($this->name.'_'.($name));	
		if(isset($this->_data[$name])){			
			return $this->_data[$name];
		}else{
			if(Configuration::get($name) != ''){
				$this->_data[$name] = Configuration::get($name);
				return Configuration::get($name);
			}elseif( isset($this->_data[$name]) ){
				return $this->_data[$name];	
			}		
		}				
		
    	return $default;
	}
  
	/**
	 * Store configuration's value as temporary.
	 */
	public function set( $key, $value ){
	  $this->_data[strtoupper($key)] = $value;
    }
	
	public function delete(){
		$res=true;
		if( !empty($this->_data) ){
			foreach( $this->_data as $key => $value ){
				$res &= Configuration::deleteByName( $key );
			}
		}
		
		return $res;
	}
	/**
	 * Update value for single configuration.
	 */
	public function update( $name ){
		$name = strtoupper($this->name.'_'.$name);
		Configuration::updateValue($name , Configuration::updateValue($name), true);
	}
  
    /**
    * Update value for list of configurations.
    */
    public function batchUpdate( $configurations=array() ){  	
        $res = true;
		foreach( $configurations as $config => $key ){    		
			$v1 = Tools::getValue(strtoupper($this->name.'_'.$config));
			$v = is_array($v1)?implode(',',$v1):$v1;
		
          $res &= Configuration::updateValue(strtoupper($this->name.'_'.$config), $v , true);
        }  
		return $res;
    }
    
	public function l( $lang ){
		return $this->currentMod->l( $lang );
	}
	
	public function inputTag( $label, $name, $value, $note="", $attrs='size="5"' ){
		$html = '
		<label>'.$this->l( $label ).'</label>
		<div class="margin-form">
			<input type="text" name="'.strtoupper($this->name.'_'.$name).'" id="'.$name.'" '.$attrs.' value="'.$value.'" /> '.$note.'
		</div>';
		
		return $html; 
	}

    public function inputTagColor( $label, $name, $value, $note="", $attrs='size="5"' ){
        Context::getContext()->controller->addJqueryPlugin(array('colorpicker'));
        $html = '
          <label>'.$this->l( $label ).'</label>
          <div class="margin-form">
           <input type="color" name="'.strtoupper($this->name.'_'.$name).'" id="'.$name.'" '.$attrs.' value="'.$value.'" class="color mColorPickerInput" data-hex="true"/> '.$note.'
          </div>';
        return $html;
    }

    public function statusTag( $label, $name, $value, $id ){
		$html = '
			<label for="'.$name.'_on">'.$this->l($label ).'</label>
			<div class="margin-form">
				<img src="../img/admin/enabled.gif" alt="Yes" title="Yes" />
				<input type="radio" name="'.strtoupper($this->name.'_'.$name).'" id="'.$id.'_on" '.( $value == 1 ? 'checked="checked"' : '').' value="1" />
				<label class="t" for="'.$name.'_on">'.$this->l('Yes').'</label>
				<img src="../img/admin/disabled.gif" alt="No" title="No" style="margin-left: 10px;" />
				<input type="radio" name="'.strtoupper($this->name.'_'.$name).'" id="'.$id.'_off" '.( $value == 0 ? 'checked="checked" ' : '').' value="0" />
				<label class="t" for="loop_off">'.$this->l('No').'</label>
			</div>';
		return $html;	
	}
	/**
	 *
	 */
	public function getSourceDataTag( $current ){

		$path = (dirname(__FILE__)).'/sources/';
		
		if( !is_dir($path) ){
			return $this->l( "Could not found any themes in 'themes' folder" );
		}
		
		$sources = $this->getFolderList( $path );
		
		$html = '<label for="source">'.$this->l( 'Source:' ).'</label>
			<div class="margin-form">';
		$html .='<select name="'.$this->getFieldName('source').'" id="source">';
		foreach( $sources as $source ) {
			$selected = ($source == $current ) ? 'selected="selected"' : '';
			$html .= '<option value="'.$source.'" '. $selected .'>'.$source.'</option>';
		}
		
		$html .='</select>';
		$html .= '</div>'; 
		
		$html .='<div class="group_configs" id="groupconfigs">';
				foreach( $sources as $source ){
					$html .= '<div class="source-group source'.$source.'">';
						$html .= PtxBaseSource::getSource( $source )->renderForm( $this );
					$html .= '</div>';
				}
		$html .='</div>';
		
		$html .= '<script>';
			$html .= '
				$(document).ready( function(){
					$(".source-group").hide();
					$(".source"+$("#source").val() ).show();
					$("#source").change(function(){
					$(".source-group").hide();
					$(".source"+$(this).val() ).show();
				} );	
			} )';
		$html .= '</script>';
		return $html;
		
	}
	
	private function getFieldName( $name ){
		return strtoupper( $this->name.'_'.$name );
	}
	
	/**
	 *
	 */
	public function getThemesTag( $current ){
		$path = dirname(dirname(__FILE__)).'/themes/';
 
		if( !is_dir($path) ){
			return $this->l( "Could not found any themes in tmpl folder" );
		}
		
		$themes = $this->getFolderList( $path );
		$name = $this->getFieldName('theme');
		$html = '<label for="'.$name.'">'.$this->l( 'Theme:' ).'</label>
			<div class="margin-form">';
		$html .='<select name="'.$this->getFieldName('theme').'" id="'.$name.'">';
		foreach( $themes as $theme ) {
			$selected = ($theme == $current ) ? 'selected="selected"' : '';
			$html .= '<option value="'.$theme.'" '. $selected .'>'.$theme.'</option>';
		}
		
		$html .='</select>';
		$html .= '</div>'; 
		return $html;
		
	}
	
	public function selectTag( $data, $label, $name, $value, $note='', $attrs='' ){
		
		$html = '<label for="'.$name.'">'.$this->l( $label ).'</label>
			<div class="margin-form">';
		$html .='<select name="'.$this->getFieldName($name).'" id="'.$name.'" '.$attrs.'>';
		foreach( $data as $key => $item ) {
			$selected = ($key == $value ) ? 'selected="selected"' : '';
			$html .= '<option value="'.$key.'" '. $selected .'>'.$item.'</option>';
		}
		$html .='</select>'.$note;
		$html .= '</div>';
		
		return $html;
	}
 
	/**
    * Get list of sub folder's name 
    */
	public function getFolderList( $path ) {
		$items = array();
		$handle = opendir($path);
		if (! $handle) {
			return $items;
		}
		while (false !== ($file = readdir($handle))) {
			if (is_dir($path . $file))
				$items[$file] = $file;
		}
		unset($items['.'], $items['..'], $items['.svn']);
		return $items;
	}

 }
}
?>