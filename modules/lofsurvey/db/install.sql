CREATE TABLE IF NOT EXISTS `ps_lofsurvey_survey` (
			`id_lofsurvey_survey` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`date_add` date NULL,
			`active` tinyint(1) unsigned NOT NULL DEFAULT '0',
			`total` int(100) unsigned DEFAULT 0,
			`id_shop_default` int(10) unsigned DEFAULT '1',
			PRIMARY KEY (`id_lofsurvey_survey`)
			)  ENGINE=MyISAM DEFAULT CHARSET=utf8;
			
CREATE TABLE IF NOT EXISTS `ps_lofsurvey_survey_lang` (
			`id_lofsurvey_survey` int(10) unsigned NOT NULL,
			`id_lang` int(10) unsigned NOT NULL,
			`title` varchar(255) NOT NULL,
			PRIMARY KEY (`id_lofsurvey_survey`,`id_lang`)
			)  ENGINE=MyISAM DEFAULT CHARSET=utf8;
			
CREATE TABLE IF NOT EXISTS `ps_lofsurvey_survey_shop` (
			`id_lofsurvey_survey` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`id_shop` int(10) unsigned NOT NULL,
			`position` int(10) unsigned NOT NULL DEFAULT '0',
			PRIMARY KEY (`id_lofsurvey_survey`, `id_shop`)
			)  ENGINE=MyISAM DEFAULT CHARSET=utf8;			
			
CREATE TABLE IF NOT EXISTS `ps_lofsurvey_reply` (
			`id_lofsurvey_reply` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`id_lofsurvey_survey` int(10) unsigned NOT NULL,
			`date_add` date NULL,
			`count` int(50) unsigned DEFAULT 0,
			`active` tinyint(1) unsigned NOT NULL DEFAULT '0',
			`total` int(100) unsigned DEFAULT 0,
			`id_shop_default` int(10) unsigned DEFAULT '1',
			PRIMARY KEY (`id_lofsurvey_reply`)
			)  ENGINE=MyISAM DEFAULT CHARSET=utf8;		

CREATE TABLE IF NOT EXISTS `ps_lofsurvey_reply_lang` (
			`id_lofsurvey_reply` int(10) unsigned NOT NULL,
			`id_lang` int(10) unsigned NOT NULL,
			`title` varchar(255) NOT NULL,
			PRIMARY KEY (`id_lofsurvey_reply`,`id_lang`)
			)  ENGINE=MyISAM DEFAULT CHARSET=utf8;	

CREATE TABLE IF NOT EXISTS `ps_lofsurvey_reply_shop` (
			`id_lofsurvey_reply` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`id_shop` int(10) unsigned NOT NULL,
			`position` int(10) unsigned NOT NULL DEFAULT '0',
			PRIMARY KEY (`id_lofsurvey_reply`, `id_shop`)
			)  ENGINE=MyISAM DEFAULT CHARSET=utf8;				
			
CREATE TABLE IF NOT EXISTS `ps_lofsurvey_guest` (
			  `id_lofsurvey_guest` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `id_lofsurvey_survey` int(10) unsigned NOT NULL,
			  `id_customer` varchar(255)  NULL,
			  `id_guest` int(10) unsigned  NULL,
			  `date_add` datetime NULl,
			  PRIMARY KEY (`id_lofsurvey_guest`,`id_lofsurvey_survey`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8;						