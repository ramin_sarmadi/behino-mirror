DROP TABLE IF EXISTS `ps_lofsurvey_survey`;
DROP TABLE IF EXISTS `ps_lofsurvey_survey_lang`;
DROP TABLE IF EXISTS `ps_lofsurvey_survey_shop`;
DROP TABLE IF EXISTS `ps_lofsurvey_reply`;
DROP TABLE IF EXISTS `ps_lofsurvey_reply_lang`;
DROP TABLE IF EXISTS `ps_lofsurvey_reply_shop`;
DROP TABLE IF EXISTS `ps_lofsurvey_guest`;