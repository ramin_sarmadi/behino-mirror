{if isset($survey)}
<div class="lof_title_survey">
	<label>{$survey.title}</label>
</div>
<div id="container{$survey.id_survey}" style="width:200px; height:350px;">
</div>
<div class="total_reply">
	<div class="lof_backsurvey">
			<input class="submitBack{$survey.id_survey}" type="button" value="{l s='Add Vote' mod='lofsurvey'}"/>
	</div>	
	<div class="lof_totalreply">	
	{l s='Total' mod='lofsurvey'}: {$survey.total} vote <br/>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.submitBack{$survey.id_survey}').click(function(){
				jQuery(".lofsurvey_preload{$survey.id_survey}").hide();
				jQuery(".responseDiv{$survey.id_survey}").hide();
				jQuery(".poll{$survey.id_survey}").show();
		});	
	});
</script>
{else}
	<div class="lof_title_survey">
		<label>{$title_survey}</label>
	</div>
	<div id="container{$lofsurvey_idsurvey}" style="width: 200px; height:350px;">
	</div>
	<div class="total_reply">
		<div class="lof_backsurvey">
				<input class="submitBack{$lofsurvey_idsurvey}" type="button" value="{l s='Add Vote' mod='lofsurvey'}"/>
		</div>	
		<div class="lof_totalreply">	
		{l s='Total' mod='lofsurvey'}: {$total_survey} vote <br/>
	</div>
	</div>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.submitBack{$lofsurvey_idsurvey}').click(function(){
				jQuery(".lofsurvey_preload{$lofsurvey_idsurvey}").hide();
				jQuery(".lof_Div{$lofsurvey_idsurvey}").hide();
				jQuery(".responseDiv{$lofsurvey_idsurvey}").hide();
				jQuery(".poll{$lofsurvey_idsurvey}").show();
				jQuery(".lof_result_poll{$lofsurvey_idsurvey}").hide();
		});	
	});
</script>
{/if}

<script type="text/javascript">
{literal}
$(function () {
    var chart;
    
    $(document).ready(function () {
    	
    	// Build the chart
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container{/literal}{if isset($lofsurvey_idsurvey)}{$lofsurvey_idsurvey}{/if}{if isset($survey.id_survey)}{$survey.id_survey}{/if}{literal}',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
           title: {
                text: '{/literal}Result{literal}'
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            	percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Vote',
                data: [
					{/literal}
					{foreach from=$lofsurvey_poll item=poll}
					{if isset($poll.id)}
					{if $poll.id==$survey.id_survey}
					['{$poll.title}',  {$poll.result}],
					{/if}
					{else}
                    ['{$poll.title}',  {$poll.result}],
					{/if}
					{/foreach}
					{literal}
                ]
            }]
        });
    });
    
});
{/literal}
</script>
