{if isset($survey)}
	<div class="lof_title_survey">
		<label>{$survey.title}</label>
	</div>
	 <ul class="lof_result">
	 {foreach from=$lofsurvey_poll item=poll}
		{if $poll.id == $survey.id_survey}
			<li>
				<div class="lof_title_reply">
					<div class="title_reply">{$poll.title} ({$poll.count})</div>
				</div>
				<div class="form-poll">
                    <div class="lof_bgreply" style="width:75% ; float:left">
                        {if isset($lofsurvey_configs.lof_color)}
                        <div class="result_reply" style="width:{$poll.result}% ; background:{$lofsurvey_configs.lof_color}"></div>
                        {/if}
                    </div>
                    <div><span>{$poll.result} %</span></div>
				</div>
			</li>
		{/if}
	{/foreach}
	</ul>
<div class="total_reply">
	<div class="lof_backsurvey">
        {if $survey.check}
		    <input class="submitBack{$survey.id_survey} button" type="button" value="{l s='Add Vote' mod='lofsurvey'}"/>
        {/if}
	</div>
	<div class="lof_totalreply">
	{l s='Total' mod='lofsurvey'}: {$survey.total} {if $survey.total > 1}{l s='votes' mod='lofsurvey'}{else}{l s='vote' mod='lofsurvey'}{/if}
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.submitBack{$survey.id_survey}').click(function(){
				jQuery(".lofsurvey_preload{$survey.id_survey}").hide();
				jQuery(".responseDiv{$survey.id_survey}").hide();
				jQuery(".poll{$survey.id_survey}").show();
		});
	});
</script>
{else}
	<div class="lof_title_survey">
		<label>{$title_survey}</label>
	</div>
	 <ul class="lof_result">
		{foreach from=$lofsurvey_poll item=poll}
			<li>
				<div class="lof_title_reply">
					<div class="title_reply">{$poll.title} ({$poll.count})</div>
				</div>
				<div class="form-poll">
						<div class="lof_bgreply" style="width:75% ; float:left">
							{if isset($lofsurvey_config.lof_color)}
							<div class="result_reply" style="width:{$poll.result}% ; background:{$lofsurvey_config.lof_color}"></div>
							{/if}
						</div>
						<div><span>{$poll.result} %</span></div>
				</div>
			</li>
		{/foreach}
	</ul>
	<div class="total_reply">
	<div class="lof_backsurvey">
        {if $lofcheck}
		    <input class="submitBack{$lofsurvey_idsurvey} button" type="button" value="{l s='Add Vote' mod='lofsurvey'}"/>
        {/if}
	</div>
	<div class="lof_totalreply">
	{l s='Total' mod='lofsurvey'}: {$total_survey} vote <br/>
	</div>
	</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.submitBack{$lofsurvey_idsurvey}').click(function(){
				jQuery(".lofsurvey_preload{$lofsurvey_idsurvey}").hide();
				jQuery(".lof_Div{$lofsurvey_idsurvey}").hide();
				jQuery(".responseDiv{$lofsurvey_idsurvey}").hide();
				jQuery(".poll{$lofsurvey_idsurvey}").show();
				jQuery(".lof_result_poll{$lofsurvey_idsurvey}").hide();
		});
	});
</script>
{/if}