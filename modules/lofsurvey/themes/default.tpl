<div class="lof-pollsurvey-block block">
<h4 class="title-survey">{l s='Survey' mod='lofsurvey'}</h4>
{foreach from=$lofsurvey_items item=survey name=surveyname}
<div class="lof-pollsurvey{if !$smarty.foreach.surveyname.first} pollsurvey-item{/if}">
{if $survey.check}
    <div class="poll{$survey.id_survey}"{if $lofsurvey_configs.hideform == 2} style="display:none;"{/if}>
        <div class="lof-poll-{$survey.id_survey} " style="position: relative;">
            <form action="" method="post" class="form{$survey.id_survey}">
                <div class="title_survey"><label>{$survey.title}</lable></label></div>
                <div class="lof_poll_survey">
                {foreach from=$lofsurvey_item item=reply}
                    {if $reply.id_survey == $survey.id_survey}
                        <div class="lof_input">
                            <input type="{$lofsurvey_configs.types}" name="poll[]" value="{$reply.id_reply}"/><label>{$reply.title}</label>
                        </div>
                    {/if}
                {/foreach}
                </div>
                <input type="hidden" name="id_survey" value="{$survey.id_survey}"/>
                <div class="lof_submitpoll">
                <input class="submitPoll{$survey.id_survey} button" name="submitPoll" value="{l s='Vote' mod='lofsurvey'}" type="button" />
                {if $lofsurvey_configs.result == 1}
                    <input class="submitResult{$survey.id_survey} button" name="submitResult" value="{l s='Result' mod='lofsurvey'}" type="button" />
                {/if}
                </div>
            </form>
        </div>
    </div>
    <div class="lof_Div{$survey.id_survey}"></div>
{else}
    <div class="responseDiv{$survey.id_survey}" {if $lofsurvey_configs.hideform == 1 && $survey.check} style="display:none;"{/if}>
         {include file="$ajaxsurvey_poll"}
    </div>
{/if}
    <div class="lof_notice{$survey.id_survey} lof_notice"></div>

</div>
<style type="text/css">
</style>
<script type="text/javascript">
	jQuery(document).ready(function($){
        $(".lofsurvey_preload{$survey.id_survey}").hide();
        $('.submitResult{$survey.id_survey}').click(function(){
            $(".poll{$survey.id_survey}").hide();
            $(".lof_Div{$survey.id_survey}").html('<div class="lof_preload"></div>').show();
            $.ajax({
               type: "POST",
               url: "{$lofsurvey_ajaxurl}",
               data: $('.form{$survey.id_survey}').serialize()+'&secure_key={$lofnfkll_secure_key}'+'&result=1',
               success: function(msg){
                    $(".poll{$survey.id_survey}").hide();
                    $(".lof_Div{$survey.id_survey}").html(msg).show();
               }
            });
        });

        {if !$lofsurvey_configs.user || $lofsurvey_customer}
            $('.submitPoll{$survey.id_survey}').click(function(){
                $(".poll{$survey.id_survey}").hide();
                $(".lof_Div{$survey.id_survey}").html('<div class="lof_preload"></div>').show();
                $.ajax({
                   type: "POST",
                   url: "{$lofsurvey_ajaxurl}",
                   data: $('.form{$survey.id_survey}').serialize()+'&secure_key={$lofnfkll_secure_key}',
                   success: function(msg){
                        $(".poll{$survey.id_survey}").hide();
                        $(".lof_Div{$survey.id_survey}").html(msg).show();
                        $(".lof_notice{$survey.id_survey}").html("You have successfully voted");
                   }
                });
            });
        {else}
            $('.submitPoll{$survey.id_survey}').click(function(){
                 $(".lof_notice{$survey.id_survey}").html("{l s='Please login to user' mod='lofsurvey'}");
            });
        {/if}
	});
</script>
{/foreach}		
</div>