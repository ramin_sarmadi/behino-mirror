<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 13573 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once(dirname(__FILE__).'/lofsurvey.php');
global $cookie;
$context = Context::getContext();
$lof_surveyr = new lofsurvey();
$lof_data = array();
if (!Tools::isSubmit('secure_key') || Tools::getValue('secure_key') != $lof_surveyr->secure_key )
	die(1);
$id_survey = Tools::getValue('id_survey');
$lofsurvey_idguest= Tools::getValue('lofsurvey_idguest');
$string='customer_survey_'.$id_survey.'_'.$lofsurvey_idguest;
$lof_data = Tools::getValue('poll');
if(isset($_COOKIE[$string]) ||  Tools::getValue('result') || !$lof_data){
    $data = $lof_surveyr->ajaxGetResult($id_survey);
    die($data);
}
$check = $lof_surveyr->checkCanVote($id_survey, (int)Context::getContext()->customer->id, (int)Context::getContext()->cookie->id_guest);
if($lof_data && $check) {
    foreach ($lof_data as $lof_datas){
        $res = Db::getInstance()->execute('
            UPDATE `'._DB_PREFIX_.'lofsurvey_reply` SET `count` = `count`+1
            WHERE `id_lofsurvey_reply` = '.(int)$lof_datas
        );
        $res = Db::getInstance()->execute('
            UPDATE `'._DB_PREFIX_.'lofsurvey_survey` SET `total` = `total`+1
            WHERE `id_lofsurvey_survey` = '.(int)$id_survey
        );
    }
    if(Tools::getValue('lofsurvey_idguest') || Tools::getValue('lofsurvey_idcustomer') || Tools::getValue('id_survey')){
        $res &= Db::getInstance()->execute('
            INSERT INTO `'._DB_PREFIX_.'lofsurvey_guest` (`id_lofsurvey_survey`, `id_customer`,`id_guest`,`date_add`)
            VALUES('.(int)Tools::getValue('id_survey').', '.(int)Context::getContext()->customer->id.','.(int)Context::getContext()->cookie->id_guest.',NOW())'
        );
    }
    $string='customer_survey_'.$id_survey.'_'.$lofsurvey_idguest;
    $domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
    setcookie($string, 2, time()+60, '/', $domain, false);
    $data = $lof_surveyr->ajaxGetResult($id_survey,true);
    die($data);
}
