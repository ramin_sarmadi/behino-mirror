<?php


class ProductReview extends Module
{
    
	public function __construct()
	{
		$this->name = 'productreview';
		$this->tab = 'others';
		$this->version = 1.0;
		$this->author = 'Ahmad Khorshidi';
		$this->need_instance = 0;

		parent::__construct();
		
		$this->displayName = $this->l('نقد و بررسی محصول');
		$this->description = $this->l('مدیر فروشگاه میتواند نقد و بررسی خود را راجع به هر مصول درج کند');
	}
	
	public function	install()
	{
		if ( !parent::install() || !$this->registerHook('displayAdminProductsExtra') )
			return false;
        return true;
	}
	
	  public function hookDisplayAdminProductsExtra($params)
    {      
        return $this->display(__FILE__, 'productreview.tpl');
    }
	
}
	
	?>