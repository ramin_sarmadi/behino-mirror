<?php


class WikiModule extends Module
{

    public function __construct()
    {
        $this->name = 'wikimodule';
        $this->tab = 'others';
        $this->version = 1.0;
        $this->author = 'Ahmad Khorshidi';
        $this->need_instance = 0;

        parent::__construct();

        WikiModule::initProcess();

        $this->displayName = $this->l('Wiki Module');
        $this->description = $this->l('Add wiki parameter for document');
    }

    public function install()
    {
        if (!parent::install() || !$this->registerHook('header') || $this->_createTables() == false )
            return false;

    }

    public function uninstall()
    {
        $db = Db::getInstance();
        $query = 'DROP TABLE `'._DB_PREFIX_.'wiki`';
        $result = $db->Execute($query);
        if (!parent::uninstall() OR !$result )
            return false;
        return true;
    }

    public function _createTables()
    {
        $db = Db::getInstance();

        $query = '
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'wiki` (
          `id_wiki` bigint(10) NOT NULL AUTO_INCREMENT,
          `id_lang` INT( 4 ) NOT NULL DEFAULT "1" ,
          `parameter_wiki` varchar(255) NOT NULL,
          `description_wiki` text,
          `date_add` datetime NOT NULL,
          `status` int(1) DEFAULT NULL,
          PRIMARY KEY (`id_wiki`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

			';

        $result = $db->Execute($query);
        if ((!$result))
            return false;
        return true;
    }

    public function initProcess()
    {

        $this->context->smarty->assign('testReview', '<p>test</p>');


    }


    public function getContent()
    {
        $html = '';
        //$content = $this->displayForm();

        //return $content;
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $parameter_wiki = (string)Tools::getValue('parameter_wiki_'.$id_lang_default);
        $description_wiki = (string)(Tools::getValue('description_wiki_'.$id_lang_default));
        $date_add = date("Y-m-d H:i:s");
        $id_wiki = (int)Tools::getValue('id_wiki');

        if (Tools::isSubmit('insertwikimodule'))
        {
            if ($id_wiki)
            {
                //$wiki = new wikiClass((int)$id_wiki);
            }
            else
            {
                $result = $this->addWiki($parameter_wiki,$id_lang_default,$description_wiki,$date_add,1);
                if($result=='OK')
                {
                    $html .= '<div class="conf confirm">'.$this->l('Add new Wiki into database.').'</div>';
                }elseif($result=='100'){

                    $html .= '<div class="conf confirm">'.$this->l('Miss Data.').'</div>';

                }elseif($result=='200'){

                    $html .= '<div class="conf confirm">'.$this->l('Duplicate Data.').'</div>';

                }else{
                    $html .= '<div class="conf confirm">'.$this->l('You have error in add data.').'</div>';
                }
                //$wiki = new wikiClass();
                //$wiki->copyFromPost();
                //$wiki->id_shop = $this->context->shop->id;
            }


        }

        if (Tools::isSubmit('updatewikimodule') || Tools::isSubmit('addwikimodule'))
        {
            $content = $this->displayForm($id_wiki);
            return $content;

        }
        elseif (Tools::isSubmit('deletewikimodule'))
        {

            $result = $this->deleted((int)Tools::getValue('id_wiki'));

            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&action=deleted&result='.$result.'&token='.Tools::getAdminTokenLite('AdminModules'));

        }
        elseif (Tools::getValue('action')=='deleted')
        {

            $result = Tools::getValue('result');

            if($result=='OK')
            {
                $html .= '<div class="conf confirm">'.$this->l('Deleted Wiki from database.').'</div>';
            }elseif($result=='100'){

                $html .= '<div class="conf confirm">'.$this->l('Miss Data.').'</div>';

            }else{
                $html .= '<div class="conf confirm">'.$this->l('You have error in add data.').'</div>';
            }

            $helper = $this->initList();
            return $html.$helper->generateList($this->getListContent((int)Configuration::get('PS_LANG_DEFAULT')), $this->fields_list);

        }
        else
        {
            $helper = $this->initList();
            return $html.$helper->generateList($this->getListContent((int)Configuration::get('PS_LANG_DEFAULT')), $this->fields_list);
        }

        if (isset($_POST['submitModule']))
        {
            /* //Configuration::updateValue('wiki_nbs', ((isset($_POST['nbs']) && $_POST['nbs'] != '') ? (int)$_POST['nbs'] : ''));
             if ($this->removeFromDB() && $this->addToDB())
             {
                 //$this->_clearCache('wiki.tpl');
                 //$output = '<div class="conf confirm">'.$this->l('The  configuration has been updated.').'</div>';
             }
             else
             {
                 //$output = '<div class="conf error"><img src="../img/admin/disabled.gif"/>'.$this->l('An error occurred while attempting to
 save.').'</div>';
             }*/
        }



    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_wiki' => array(
                'title' => $this->l('Id'),
                'width' => 30,
                'type' => 'text',
            ),
            'parameter_wiki' => array(
                'title' => $this->l('Parameter wiki'),
                'width' => 340,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'date_add' => array(
                'title' => $this->l('Date Add'),
                'width' => 140,
                'type' => 'text'
            )
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'id_wiki';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new')
        );

        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    protected function getListContent($id_lang,$id_wiki =0)
    {
        if($id_wiki>0)
        {
            $sub_query = 'AND w.id_wiki='.$id_wiki;
        }

        $SQL = '
			SELECT w.`id_wiki`, w.parameter_wiki, w.description_wiki, w.date_add
			FROM `'._DB_PREFIX_.'wiki` w
			WHERE w.`id_lang` = '.(int)$id_lang.' '.$sub_query;

        return  Db::getInstance()->executeS($SQL);
    }

    protected function deleted($id_wiki)
    {
        if($id_wiki>0)
        {
        $SQL = '
        DELETE FROM `'._DB_PREFIX_.'wiki` WHERE  id_wiki IN ('.$id_wiki.')
        ';

            Db::getInstance()->executeS($SQL);

            return 'OK'; //success deleted
        }

        return 100; //miss data

    }

    protected function addWiki($parameter_wiki,$id_lang,$description_wiki,$date_add,$status)
    {

/*
        $SQL = '
        INSERT INTO `'._DB_PREFIX_.'wiki` (parameter_wiki,id_lang,description_wiki,date_add,status)
        VALUES("'.$parameter_wiki.'",'.(int)$id_lang.',"'.$description_wiki.'","'.$date_add.'",'.(int)$status.')
	    ';*/
        $SQL = '
			SELECT w.`id_wiki`
			FROM `'._DB_PREFIX_.'wiki` w
			WHERE w.parameter_wiki = "'.$parameter_wiki.'" ';

        $result = Db::getInstance()->executeS($SQL);

        if(count($result)==0)
        {

            $data = array(
                'parameter_wiki' => $parameter_wiki,
                'id_lang' => $id_lang,
                'description_wiki' => $description_wiki,
                'date_add' => $date_add,
                'status' => $status
            );


            if(($parameter_wiki)&&($description_wiki)&&($date_add))
            {
                if((Db::getInstance()->insert('wiki', $data)))
                {
                    return 'OK';
                }else{
                    return false;
                }

            }else{
                return '100'; //Miss data
            }

        }else{

            return '200'; //Duplicate record

        }

    }

    private function _postProcess()
    {
        $output = "";

        if ( Tools::isSubmit('updateSubmit'))
        {

        }
        elseif ( Tools::isSubmit('addSubmit'))
        {

        }
        elseif ( Tools::isSubmit('deleteSubmit'))
        {

        }

        return $output;
    }

    public function displayForm($id_wiki = 0)
    {

        // Get default Language
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $languages = Language::getLanguages(false);
        $iso = $this->context->language->iso_code;

        $output = '';
        if (Tools::isSubmit('submitCustWikiMessage'))
        {
            $message_trads = array();
            foreach ($_POST as $key => $value)
                if (preg_match('/custpriv_message_/i', $key))
                {
                    $id_lang = preg_split('/custpriv_message_/i', $key);
                    $message_trads[(int)$id_lang[1]] = $value;
                }
            //Configuration::updateValue('CUSTPRIV_MESSAGE', $message_trads, true);
            $output = '<div class="conf confirm">'.$this->l('Configuration updated').'</div>';
        }
        // Init Fields form array
        $content = '';
        if (version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
        {
            $content .= '
			<script type="text/javascript">
				var iso = \''.(file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en').'\' ;
				var pathCSS = \''._THEME_CSS_DIR_.'\' ;
				var ad = \''.dirname($_SERVER['PHP_SELF']).'\' ;
			</script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
			<script language="javascript" type="text/javascript">
				id_language = Number('.$id_lang_default.');
				tinySetup();
			</script>';
        }
        else
        {
            $content .= '
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript">
				tinyMCE.init({
					mode : "textareas",
					theme : "advanced",
					plugins : "safari,pagebreak,style,layer,table,advimage,advlink,inlinepopups,media,searchreplace,contextmenu,paste,directionality,fullscreen",
					theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
					theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
					theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
					theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
					theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : false,
					content_css : "'.__PS_BASE_URI__.'themes/'._THEME_NAME_.'/css/global.css",
					document_base_url : "'.__PS_BASE_URI__.'",
					width: "600",
					height: "auto",
					font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
					template_external_list_url : "lists/template_list.js",
					external_link_list_url : "lists/link_list.js",
					external_image_list_url : "lists/image_list.js",
					media_external_list_url : "lists/media_list.js",
					elements : "nourlconvert",
					entity_encoding: "raw",
					convert_urls : false,
					language : "'.(file_exists(_PS_ROOT_DIR_.'/js/tinymce/jscripts/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en').'"
				});
				id_language = Number('.$id_lang_default.');
			</script>';
        }

        if($id_wiki>0)
        {
            $values = $this->getListContent($id_lang_default,$id_wiki);
        }
        //print_r($values);
        $content .= $output;
        $content .= '
		<fieldset><legend><img src="../modules/'.$this->name.'/logo.gif" /> '.$this->displayName.'</legend>
			<form action="'.AdminController::$currentIndex.'&configure='.$this->name.'&insert'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'" method="post">
				'.(isset($values[$language['id_lang']]) ? $values[$language['id_lang']] : '').'
				<label>'.$this->l('Parameter Wiki').':</label>
				<div class="margin-form">';
        foreach ($languages as $language)
            $content .= '
					<div id="ccont_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $id_lang_default ? 'block' : 'none').';float: left;">
                        <input id="parameter_wiki_'.$language['id_lang'].'" value="'.(isset($values[0]['parameter_wiki']) ? $values[0]['parameter_wiki'] : '').'" name="parameter_wiki_'.$language['id_lang'].'" type="text" size="50" />
					</div>';
            $content .= $this->displayFlags($languages, $id_lang_default, 'ccont', 'ccont', true).'
					<div class="clear"></div>
				</div>
    			<div class="clear">&nbsp;</div>
				<label>'.$this->l('Description Wiki').':</label>
				<div class="margin-form">';
        foreach ($languages as $language)
            $content .= '
					<div id="ccont_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $id_lang_default ? 'block' : 'none').';float: left;">
						<textarea class="rte" cols="70" rows="30" id="description_wiki_'.$language['id_lang'].'" name="description_wiki_'.$language['id_lang'].'">
						'.(isset($values[0]['description_wiki']) ? $values[0]['description_wiki'] : '').'
						</textarea>
					</div>';
           $content .= $this->displayFlags($languages, $id_lang_default, 'ccont', 'ccont', true).'
					<div class="clear"></div>
					<p>
						'.$this->l('').'<br />
						'.$this->l('').'
					</p>
				</div>
				<div class="clear">&nbsp;</div>
				<div class="margin-form">
					<input type="submit" class="button" name="submitCustWikiMessage" value="'.$this->l('Save').'" />
				</div>
			</form>
		</fieldset>';

     return $content;
    }

    public function hookHeader($params)
    {
        //$this->context->controller->addCSS($this->_path.'WikiModules.css', 'all');
    }


}
