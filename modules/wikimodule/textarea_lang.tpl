<script type="text/javascript" src="{$ps_js_dir}tinymce.inc.js"></script>
<fieldset>
    <legend><img src="" alt="" /> </legend>
    <strong>
        <a href="">
            <img src="" alt="" />
        </a>
    </strong>
</fieldset>
<div class="translatable">
<div class="lang_{$language.id_lang}" style="{if !$language.is_default}display:none;{/if}float: left;">
	<textarea cols="100" rows="10" id="{$input_name}_{$language.id_lang}" 
		name="description"
		class="autoload_rte" >{if isset($input_value[$language.id_lang])}{$input_value[$language.id_lang]|htmlentitiesUTF8}{/if}</textarea>
	<span class="counter" max="{if isset($max)}{$max}{else}none{/if}"></span>
	<span class="hint">{$hint|default:''}<span class="hint-pointer">&nbsp;</span></span>
</div>
</div>
<script type="text/javascript">
    var iso = '{$iso_tiny_mce}';
    var pathCSS = '{$smarty.const._THEME_CSS_DIR_}';
    var ad = '{$ad}';
</script>
