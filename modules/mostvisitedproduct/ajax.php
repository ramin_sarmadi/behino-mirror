<?php
include('../../config/config.inc.php');
include('../../init.php');
include('mostvisitedproduct.php');

$object = new MostVisitedProduct();
$res = $object->getResultContent(Context::getContext()->language->id, 0, 5);
Context::getContext()->smarty->assign(array(
    'MostVisitedProductArr' => $object->getResultContent(Context::getContext()->language->id, 0, 5)
));
if (Tools::getValue('gettpl')) {
    //echo __DIR__ . '\resultpage.tpl';
    $resulthtml = Context::getContext()->smarty->fetch(__DIR__ . '\resultpage.tpl');
    //$resulthtml;
    die(Tools::jsonEncode(array(
        'resulthtml' => $resulthtml
    )));
}
