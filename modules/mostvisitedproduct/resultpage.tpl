<span class="tab-right-arrow"></span>
<div id="carousel4" class="carousel-tabs">
    {foreach from=$MostVisitedProductArr item=MostVisitedProduct}
        <div data-category="{$MostVisitedProduct['parent_category_default']}"
             class="isotope-hover element-item {$MostVisitedProduct['parent_category_default']} view effect">
            <a href="{$MostVisitedProduct['link']}" target="_blank">
                <div class="view effect">
                    <img src="{$MostVisitedProduct['address_file']}">

                    {*<div class="mask"></div>*}
                    {*<div class="content">*}
                    {*<a class="info" target="_blank" href="{$SpecialProduct['link']}">Read More</a>*}
                    {*</div>*}
                </div>
                <div class="main-text">
                    <span class="new-model">{$MostVisitedProduct['name']|truncate:26}</span>
                    {if $MostVisitedProduct['price'] > 0}
                        <span class="new-price">{convertPrice price=$MostVisitedProduct['price']}</span>
                    {/if}
                </div>
            </a>
        </div>
    {/foreach}
</div>
<span class="tab-left-arrow"></span>
{*
<script type="text/javascript">
    $(document).ready(function () {
        $('#carousel4').carouFredSel({
            items               : 6,
            direction           : "right",
            scroll : {
                items           : 1,
                easing          : "elastic",
                duration        : 1000,
                pauseOnHover    : true
            },
            prev : ".tab-left-arrow",
            next : ".tab-right-arrow"
        });
    });

</script>
*}
