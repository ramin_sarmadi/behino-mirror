<fieldset><legend><img src="{$IconMostVisitedProductImage}" /> {$MostVisitedProductImage} </legend>

    <form action="{$LinkSave}" method="post" enctype="multipart/form-data">
        <input name="id_product_special" value="{$ResultMostVisitedProductArr[0]['id_product_special']}" type="hidden">
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='MostVisited Product Name' mod='mostvisitedproduct'}:</span>
            <input name="special_product_name" value="{$ResultMostVisitedProductArr[0]['name']}" maxlength="2000" style="width: 280px" >
        </div>

        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Product Name' mod='mostvisitedproduct'}:</span>
            <select name="id_product" dir="ltr">
                <option value="-1">...</option>
                {foreach from=$ProductArr item=Product}
                    <option style="{$Product['BgColor']}" value="{$Product['id_product']}"
                            {if $Product['id_product']==$ResultMostVisitedProductArr[0]['id_product']} selected="selected" {/if}
                            >{$Product['id_product']}. {$Product['name']} //-->Count Visited: {$Product['COUNT_LOGGER']} </option>
                {/foreach}
            </select>
            {if $ResultMostVisitedProductArr[0]['id_product']>0}<span style="font-size: 10px; font-style: italic">{l s='If the option is not selected, it is possible to disable the product' mod='mostvisitedproduct'}</span>  {/if}
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='MostVisited Product Type' mod='mostvisitedproduct'}:</span>
            <select name="id_product_special_type" dir="ltr">
                <option value="-1">...</option>
                {foreach from=$MostVisitedType item=MostVisitedTypes}
                    <option value="{$MostVisitedTypes['id_product_special_type']}"
                            {if $MostVisitedTypes['id_product_special_type']==$ResultMostVisitedProductArr[0]['id_product_special_type']} selected="selected" {/if} >
                        {$MostVisitedTypes['id_product_special_type']}. {$MostVisitedTypes['name']}
                    </option>
                {/foreach}
            </select>
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Duration (Day) ' mod='mostvisitedproduct'}:</span>
            <input name="duration_days" value="{$ResultMostVisitedProductArr[0]['duration_days']}" maxlength="3" style="width: 50px" >
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Description' mod='mostvisitedproduct'}:</span>
            <textarea cols="50" rows="3" name="description">{$ResultMostVisitedProductArr[0]['description']}</textarea>
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Image File' mod='mostvisitedproduct'}:</span>
            <input type="file" name="ImageFile">
        </div>

        <div style="padding: 5px">date_start
            <span class="span_bold_12">{l s='Available from' mod='mostvisitedproduct'}:</span>
            <input class="datepicker" type="text" name="date_start" value="{$ResultMostVisitedProductArr[0]['date_start_h']}" style="text-align: center" id="sp_from" />
        </div>

        <div style="padding-left: 150px; padding-top: 10px">
            <input type="submit" value="{l s='Save' mod='mostvisitedproduct'}" class="button" name="AddProductLink">
        </div>


    </form>
</fieldset>
<script type="text/javascript">
    $(document).ready(function(){

        $('#leave_bprice').click(function() {
            if (this.checked)
                $('#sp_price').attr('disabled', 'disabled');
            else
                $('#sp_price').removeAttr('disabled');
        });

        $('.datepicker').datepicker({
            prevText: '',
            nextText: '',
            dateFormat: 'yy-mm-dd',

            // Define a custom regional settings in order to use PrestaShop translation tools
            currentText: '{l s='Now'}',
            closeText: '{l s='Done'}'
        });
    });
</script>