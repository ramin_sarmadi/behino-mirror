<span class="tab-right-arrow"></span>
<div id="carousel2" class="carousel-tabs">
    {foreach from=$SpecialProductArr item=SpecialProduct}
        <div data-category="{$SpecialProduct['parent_category_default']}"
             class="isotope-hover element-item {$SpecialProduct['parent_category_default']} view effect">
            <a href="{$SpecialProduct['link']}" target="_blank">
                <div class="view effect">
                    <img src="{$SpecialProduct['address_file']}">

                    {*<div class="mask"></div>*}
                    {*<div class="content">*}
                    {*<a class="info" target="_blank" href="{$SpecialProduct['link']}">Read More</a>*}
                    {*</div>*}
                </div>
                <div class="main-text">
                    <span class="new-model">{$SpecialProduct['name']|truncate:26}</span>
                    {if $SpecialProduct['price'] > 0}
                        <span class="new-price">{convertPrice price=$SpecialProduct['price']}</span>
                    {/if}
                </div>
            </a>
        </div>
    {/foreach}
</div>
<span class="tab-left-arrow"></span>
