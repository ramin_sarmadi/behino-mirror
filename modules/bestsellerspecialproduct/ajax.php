<?php
include('../../config/config.inc.php');
include('../../init.php');
include('bestsellerspecialproduct.php');
$object = new bestsellerspecialproduct();
$res = $object->getResultContent(Context::getContext()->language->id, 0, 3);
Context::getContext()->smarty->assign(array(
    'SpecialProductArr' => $object->getResultContent(Context::getContext()->language->id, 0, 3)
));
if (Tools::getValue('gettpl')) {
    //echo __DIR__ . '\resultpage.tpl';
    $resulthtml = Context::getContext()->smarty->fetch(__DIR__ . '\resultpage.tpl');
    //$resulthtml;
    die(Tools::jsonEncode(array(
        'resulthtml' => $resulthtml
    )));
}
