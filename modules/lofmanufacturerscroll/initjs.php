<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#lofjcarousel').jcarousel({
			scroll: <?php echo (Configuration::get( $this->param_prefix.'_num_of_page' ) ? Configuration::get( $this->param_prefix.'_num_of_page' ) : 1); ?>,
			buttonNextHTML: '<?php echo (Configuration::get($this->param_prefix.'_navigator') == 1 || Configuration::get($this->param_prefix.'_navigator') == 2 ? '<div></div>' : '');?>',
			buttonPrevHTML: '<?php echo (Configuration::get($this->param_prefix.'_navigator') == 1 || Configuration::get($this->param_prefix.'_navigator') == 2 ? '<div></div>' : '');?>'
		});
		<?php if(Configuration::get($this->param_prefix.'_navigator') == 2){ ?>
			$('#mycarouselHolder').find('.jcarousel-prev').css('display','none');
			$('#mycarouselHolder').find('.jcarousel-next').css('display','none');
			$('#mycarouselHolder').mouseover(function(){
				$(this).find('.jcarousel-prev').css('display','block');
				$(this).find('.jcarousel-next').css('display','block');
			}).mouseout(function(){
				$(this).find('.jcarousel-prev').css('display','none');
				$(this).find('.jcarousel-next').css('display','none');
			});
		<?php } ?>
	});
</script>