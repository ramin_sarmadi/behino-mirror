<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}    
/**
 * lofmanufacturerscroll Class
 */	
class lofmanufacturerscroll extends Module
{
	public $entities = array();
	private $_params = '';	
	private $_defaultFormLanguage;
	private $_languages;
	
	public $param_prefix = 'lofmnsc';
	/**
	 * @var array $_postErrors;
	 *
	 * @access private;
	 */
	private $_postErrors = array();		
   /**
    * Constructor 
    */
	function __construct()
	{
		$this->name = 'lofmanufacturerscroll';
		parent::__construct();			
		$this->tab = 'LandOfCoder';				
		$this->version = '1.0.1';
		$this->displayName = $this->l('Lof Manufacturers Scroll');
		$this->description = $this->l('Lof Manufacturers Scroll');	
		$this->Languages();   
	}
   /**
    * process installing 
    */
	function install(){
		global $cookie;
		if (!parent::install())
			return false;
		if(!$this->registerHook('home'))
			return false;
		if(!$this->registerHook('header'))
			return false;
		foreach($this->_languages as $language){
			Configuration::updateValue($this->param_prefix.'_module_title_'.$language['id_lang'], 'Manufacturers scroll', true);
		}
		Configuration::updateValue($this->param_prefix.'_show_title', 0, true);
		Configuration::updateValue($this->param_prefix.'_num_of_page', 1, true);
		/*
		$manufacturers = Manufacturer::getManufacturers(false,$cookie->id_lang, true);
		$manus =array();
		foreach($manufacturers as $m){
			$manus[] = $m['id_manufacturer'];
		}
		if($manus){
			$catList = implode(",",$manus);
			Configuration::updateValue($this->param_prefix.'_id_manufacturer', $catList, true);
		}else			
			Configuration::updateValue($this->param_prefix.'_id_manufacturer', '', true);
		*/
		Configuration::updateValue($this->param_prefix.'_navigator',1);
		Configuration::updateValue( $this->param_prefix.'_image_type', 'small_leologo' );
		return true;
	}
	public function Languages(){
		global $cookie;
		$allowEmployeeFormLang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		if ($allowEmployeeFormLang && !$cookie->employee_form_lang)
			$cookie->employee_form_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$useLangFromCookie = false;
		$this->_languages = Language::getLanguages(false);
		if ($allowEmployeeFormLang)
			foreach ($this->_languages AS $lang)
				if ($cookie->employee_form_lang == $lang['id_lang'])
					$useLangFromCookie = true;
		if (!$useLangFromCookie)
			$this->_defaultFormLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		else
			$this->_defaultFormLanguage = (int)($cookie->employee_form_lang);
	}
   /**
    * Render processing form && process saving data.
    */	
	public function getContent(){
		$html = "";
		if (Tools::isSubmit('submit'))
		{
			$this->_postValidation();
			if (!sizeof($this->_postErrors))
			{
				$definedConfigs = array(
					'show_title' => '',
					'image_type' => '',
					'num_of_page' => '',
					'navigator' => ''
				);
				foreach($this->_languages as $language){
					$definedConfigs['module_title_'.$language['id_lang']] = '';
				}
				if( $definedConfigs ){
					foreach( $definedConfigs as $config => $value ){    
						Configuration::updateValue($this->param_prefix.'_'.$config, Tools::getValue($config), true);
					}
				}
				if(Tools::getValue('id_manufacturer')){
    		        if(in_array("",Tools::getValue('id_manufacturer'))){
    		          $catList = "";
    		        }else{
    		          $catList = implode(",",Tools::getValue('id_manufacturer'));  
    		        }
                    Configuration::updateValue($this->param_prefix.'_id_manufacturer', $catList, true);
                }
				$html .= '<div class="conf confirm">'.$this->l('Settings updated success').'</div>';
				
			}else{
				foreach ($this->_postErrors AS $err)
				{
					$html .= '<div class="alert error">'.$err.'</div>';
				}
			}
		}
		return $html.$this->displayForm();
	}
	/**
     * Process vadiation before saving data 
     */
	private function _postValidation(){
		if ( Validate::isString(Tools::getValue('entity')) )
			$this->_postErrors[] = $this->l('Please, choice Entity');
	}
	/**
	* Form config
	*/
	public function displayForm()
	{
		global $smarty, $cookie;
		$divLangName = 'module_title';
		$this->site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		// META KEYWORDS
		$str = '';
		$str .= '
			<script type="text/javascript">
				id_language='.$this->_defaultFormLanguage.';
			</script>
		';
		$str .= '	<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<label>'.$this->l('Module Title').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language){
			$title = Configuration::get( $this->param_prefix.'_module_title_'.$language['id_lang'] );
			$str .= '	<div id="module_title_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="50" type="text" name="module_title_'.$language['id_lang'].'" value="'.$title.'" />
					</div>';
		}
		$str .= $this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'module_title',true);
		$str .= '	</div><div class="clear space">&nbsp;</div>';
		$str .= ' <label>'.$this->l('Scroll number').' </label>
				<div class="margin-form">';
			$num_of_page = Configuration::get( $this->param_prefix.'_num_of_page' );
			$str .= '
				<input size="20" type="text" name="num_of_page" value="'.$num_of_page.'" />
			';
		$str .= ' </div>';
		$show_title = Configuration::get( $this->param_prefix.'_show_title' );
		$str .= '	<label>'.$this->l('Show Title:').' </label>
				<div class="margin-form">
					<input type="radio" name="show_title" id="show_title_on" onclick="toggleDraftWarning(false);" value="1" '.($show_title ? 'checked="checked" ' : '').'/>
					<label class="t" for="show_title_on"> <img src="../img/admin/enabled.gif" alt="'.$this->l('Yes').'" title="'.$this->l('Yes').'" /></label>
					<input type="radio" name="show_title" id="show_title_off" onclick="toggleDraftWarning(true);" value="0" '.(!$show_title ? 'checked="checked" ' : '').'/>
					<label class="t" for="show_title_off"> <img src="../img/admin/disabled.gif" alt="'.$this->l('No').'" title="'.$this->l('No').'" /></label>
				</div>';
		$manu = Configuration::get($this->param_prefix.'_id_manufacturer');
		$selected = array();
		$isSelected = 'selected="selected"';
		if($manu){
			$selected = explode(',',$manu); 
			$isSelected = (in_array("",$selected))?'selected="selected"':""; 
		}
		$manufacturers = Manufacturer::getManufacturers(false,$language['id_lang'], true);
		$str .= '	<label>'.$this->l('Manufacturers:').' </label>
				<div class="margin-form">
					<select name="id_manufacturer[]" multiple="multiple" size="10" id="params_manufactures">
						<option value="" onclick="lofSelectAll(\'#params_manufactures\');" '.$isSelected.'>'.$this->l('All Manufacturers').'</option>';
			foreach ($manufacturers AS $manufacturer)
				$str .= '<option value="'.$manufacturer['id_manufacturer'].'"'.(in_array($manufacturer['id_manufacturer'],$selected) ? ' selected="selected"' : '').' '.$isSelected.'>&nbsp;&nbsp;&nbsp;'.$manufacturer['name'].'</option>';
				$str .= '</select> <sup>*</sup>
				</div>';
		$imagesTypes = ImageType::getImagesTypes('manufacturers');
		$str .= '	<label>'.$this->l('Image Type:').' </label>
				<div class="margin-form">
					<select name="image_type">';
			foreach ($imagesTypes AS $i)
				$str .= '<option value="'.$i['name'].'"'.($i['name'] == Configuration::get($this->param_prefix.'_image_type') ? ' selected="selected"' : '').'>&nbsp;&nbsp;'.$i['name'].'&nbsp;&nbsp;</option>';
				$str .= '</select>
				</div>';
		$navigator = array( 1=> $this->l('Enable'), 2=> $this->l('Enable When mouse over'), 3=> $this->l('Disable'));
		$str .= '	<label>'.$this->l('Navigation:').' </label>
				<div class="margin-form">
					<select name="navigator">';

			foreach ($navigator AS $key=>$i)
				$str .= '<option value="'.$key.'"'.($key == Configuration::get($this->param_prefix.'_navigator') ? ' selected="selected"' : '').'>&nbsp;&nbsp;'.$i.'&nbsp;&nbsp;</option>';
				$str .= '</select>
				</div>';
			
		$str .= '
			<center><input type="submit" name="submit" value="'.$this->l('Save').'" class="button" /></center>
		</form>
		<script type="text/javascript">
			function lofSelectAll(obj){	
				$(obj).find("option").each(function(index,Element) {
					$(Element).attr("selected","selected");
				});	
			}
		</script>';
		return $str;
	}
	/*
	 * register hook right comlumn to display slide in right column
	 */
	function hookHeader($params) {
		$params = $this->_params;
		
		if(_PS_VERSION_ <= "1.4"){
			$cssjs  = "<link href='"._MODULE_DIR_.$this->name."/assets/style.css' rel='stylesheet' type='text/css' media='all' />";
			$cssjs .= "<link href='"._MODULE_DIR_.$this->name."/assets/skin.css' rel='stylesheet' type='text/css' media='all' />";
			$cssjs .= "<script type=\"text/javascript\" src=\""._MODULE_DIR_.$this->name."/assets/jquery.jcarousel.js\"></script>";
			return $cssjs;
		}elseif(_PS_VERSION_ < "1.5"){
			Tools::addCSS(_MODULE_DIR_.$this->name."/assets/jquery.jcarousel.css", 'all');
			Tools::addCSS(_MODULE_DIR_.$this->name."/assets/skin.css", 'all');
			Tools::addJS(_MODULE_DIR_.$this->name."/assets/jquery.jcarousel.js");
		}else{
			$this->context->controller->addCSS(_MODULE_DIR_.$this->name."/assets/jquery.jcarousel.css", 'all');
			$this->context->controller->addCSS(_MODULE_DIR_.$this->name."/assets/skin.css", 'all');
			$this->context->controller->addJS(_MODULE_DIR_.$this->name."/assets/jquery.jcarousel.js");
		}
	}
	/*
	 * register hook right comlumn to display slide in right column
	 */
	function hookrightColumn($params)
	{
		return $this->processHook( $params,"rightColumn");
	}
	
	function hooklofBottom($params)
	{
		return $this->processHook( $params,"lofBottom");
	}
	
	/*
	 * register hook left comlumn to display slide in left column
	 */
	function hookleftColumn($params)
	{
		return $this->processHook( $params,"leftColumn");
	}
	
	function hooktop($params)
	{
		return '</div><div class="clearfix"></div><div>'.$this->processHook( $params,"top");
	}
	
	function hookfooter($params)
	{		
		return $this->processHook( $params,"footer");
	}
	
	function hookHome($params)
	{
		return $this->processHook( $params,"home");
	}
	public function processHook( $params, $pos = 'home' ){
		global $smarty, $cookie,$link;
		$this->site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		
		$module_title = Configuration::get( $this->param_prefix.'_module_title_'.$cookie->id_lang );
		$show_title = Configuration::get( $this->param_prefix.'_show_title' );
		$limit = Configuration::get( $this->param_prefix.'_num_of_page' );
		$id_manufacturers = Configuration::get( $this->param_prefix.'_id_manufacturer' );
		
		if(!$id_manufacturers){
			$manus = Manufacturer::getManufacturers(false,$cookie->id_lang, true);
			foreach($manus as $m){
				$id_manufacturers[] = $m['id_manufacturer'];
			}
		}else{
			$id_manufacturers = explode(',',$id_manufacturers);
		}
		$manufacturers = array();
		foreach($id_manufacturers as $id_manufacturer){
			$manufacturer = new Manufacturer($id_manufacturer,$cookie->id_lang);
			if(Validate::isLoadedObject($manufacturer)){
				$manufacturers[$id_manufacturer]['link'] = $link->getManufacturerLink($id_manufacturer,$manufacturer->link_rewrite,$cookie->id_lang);
				$image_type = Configuration::get( $this->param_prefix.'_image_type' );
				$id_images = (!file_exists(_PS_MANU_IMG_DIR_.'/'.$id_manufacturer.'-'.$image_type.'.jpg')) ? Language::getIsoById((int)$cookie->id_lang).'-default' : $id_manufacturer;
				$manufacturers[$id_manufacturer]['linkIMG'] = _THEME_MANU_DIR_.$id_images.'-'.$image_type.'.jpg';
				$manufacturers[$id_manufacturer]['id_manufacturer'] = $id_manufacturer;
				$manufacturers[$id_manufacturer]['name'] = $manufacturer->name;
			}
		}
		$smarty->assign(
			array(
				'modname' 		=> $this->name,
				'pos' 			=> $pos,	
				'limit' 		=> $limit,	
				'module_title' 	=> $module_title,	
				'show_title' 	=> $show_title,	
				'lofmanufacturers' => $manufacturers
			)
		);
		ob_start();
			require( dirname(__FILE__).'/initjs.php' );		
			$initjs = ob_get_contents();
		ob_end_clean();
		return $this->display(__FILE__,'lofmanufacturerscroll.tpl').$initjs;
	}
	
	public function displayFlags($languages, $default_language, $ids, $id, $return = false, $use_vars_instead_of_ids = false)
	{
		if (sizeof($languages) == 1)
			return false;
		$output = '
		<div class="displayed_flag">
			<img src="../img/l/'.$default_language.'.jpg" class="pointer" id="language_current_'.$id.'" onclick="toggleLanguageFlags(this);" alt="" />
		</div>
		<div id="languages_'.$id.'" class="language_flags">
			'.$this->l('Choose language:').'<br /><br />';
		foreach ($languages as $language)
			if($use_vars_instead_of_ids)
				$output .= '<img src="../img/l/'.(int)($language['id_lang']).'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="changeLanguage(\''.$id.'\', '.$ids.', '.$language['id_lang'].', \''.$language['iso_code'].'\');" /> ';
			else
				$output .= '<img src="../img/l/'.(int)($language['id_lang']).'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="changeLanguage(\''.$id.'\', \''.$ids.'\', '.$language['id_lang'].', \''.$language['iso_code'].'\');" /> ';
		$output .= '</div>';

		if ($return)
			return $output;
		echo $output;
	}
} 