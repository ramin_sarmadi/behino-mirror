<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
?>
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.css";?>" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/farbtastic/farbtastic.css";?>" type="text/css" media="screen" charset="utf-8" />
<script type="text/javascript" src="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/farbtastic/farbtastic.js";?>"></script>

<?php  
    $yesNoLang = array("0"=>$this->l('No'),"1"=>$this->l('Yes')); 
?>
<h3><?php echo $this->l('Lof Send Cart To Friends Configuration');?></h3>
<form action="<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>" enctype="multipart/form-data" method="post" id="lofform">
 <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
  <fieldset>
    <legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Global Setting'); ?></legend>
    <div class="lof_config_wrrapper clearfix">
      <ul>
        <?php 
            $optionsTheme = array(
                'shoppingCart'          =>$this->l('shoppingCart'),
                'shoppingCartExtra'     =>$this->l('shoppingCartExtra'),
                'orderDetail'           =>$this->l('orderDetail'),
                'rightColumn'           =>$this->l('rightColumn'),
                'leftColumn'            =>$this->l('leftColumn')
            );
            //echo $this->_params->selectTag("hookPage",$optionsTheme,$this->getParamValue("hookPage","shoppingCart"),$this->l('Hook Pages'),'class="inputbox"', 'class="row" title="'.$this->l('Select a Hook').'"');           
            echo $this->_params->selectTag("infocart",$yesNoLang,$this->getParamValue("infocart","1"),$this->l('Info Cart Display'),'class="inputbox"', 'class="row" title="'.$this->l('').'"');           
        ?>      	                   	        
        <li class="row module_group-product">

        </li>
        <li class="row module_group-custom">
          
        </li>                    
      </ul>
    </div>
  </fieldset>
  
<br />
  <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
  	<fieldset><legend class="lof-legend"><img src="../img/admin/comment.gif" alt="" title="" /><?php echo $this->l('Information');?></legend>    	
    	<ul>
    	     <li>+ <a target="_blank" href="http://landofcoder.com/prestashop/lof-send-cart.html"><?php echo $this->l('Detail Information');?></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/supports/forum.html?id=92"><?php echo $this->l('Forum support');?></a></li>
             <li>+ <a target="_blank" href="http://www.landofcoder.com/submit-request.html"><?php echo $this->l('Customization/Technical Support Via Email');?>.</a></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/prestashop/guides/lof-send-cart.html"><?php echo $this->l('UserGuide ');?></a></li>
        </ul>
        <br />
        @Copyright: <a href="http://wwww.landofcoder.com">LandOfCoder.com</a>
    </fieldset>
</form>