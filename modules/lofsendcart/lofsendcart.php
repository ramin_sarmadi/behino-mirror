<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}    
/**
 * lofcordion Class
 */	

class lofsendcart extends Module
{
	/**
	 * @var LofParams $_params;
	 *
	 * @access private;
	 */
	private $_params = '';	
	/**
	 * @var array $_postErrors;
	 *
	 * @access private;
	 */
	private $_postErrors = array();		
	/**
	 * @var string $__tmpl is stored path of the layout-theme;
	 *
	 * @access private 
	 */	
   /**
    * Constructor 
    */
	function __construct()
	{
		$this->name = 'lofsendcart';
		parent::__construct();			
		$this->tab = 'LandOfCoder';
		$this->module_key = "5c62ae0d443be9b1964b3fb9f51a235b";	
		$this->version = '1.1';
		$this->displayName = $this->l('Lof Send Cart To Friends Module');
		$this->description = $this->l('Lof Send Cart To Friends Module');
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' ) && !class_exists("LofParams", false) ){
			if( !defined("LOF_SENDCART_LOAD_LIB_PARAMS") ){				
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' );
				define("LOF_SENDCART_LOAD_LIB_PARAMS",true);
			}
		}		
		$this->_params = new LofParams( $this->name );	   
	}
   /**
    * process installing 
    */
	function install(){		
		if (!parent::install())
			return false;
        if(!$this->registerHook('rightColumn'))
			return false;
		if(!$this->registerHook('header'))
			return false;	
		return true;
	}
    function hookHeader($params)
	{
		if(_PS_VERSION_ <="1.4"){							
			$header = '
			<link type="text/css" rel="stylesheet" href="'.($this->_path).'assets/style.css'.'" />';			
			return $header;			
		}else{				
			Tools::addCSS( ($this->_path).'assets/style.css', 'all');
            Tools::addJS( ($this->_path).'assets/jquery.simplemodal.js', 'all');
		}		
	}
    function hookshoppingCart( $params )
	{
        $multilClass = '';
        $displayTitle = 0;
        global $smarty;
		$smarty->assign(array(
			'multilClass'     => $multilClass,
			'displayTitle'    => $displayTitle
		));
        return $this->displayFrontForm( $params,"shoppingCart");
	}
    function hookshoppingCartExtra( $params )
	{
            $multilClass = '';
            $displayTitle = 0;
            global $smarty;
    		$smarty->assign(array(
    			'multilClass'     => $multilClass,
    			'displayTitle'    => $displayTitle
    		));
    		return $this->displayFrontForm( $params,"shoppingCartExtra");
	}
	function hookExtraLeft( $params )
	{
        $multilClass = '';
        $displayTitle = 0;
        global $smarty;
		$smarty->assign(array(
			'multilClass'     => $multilClass,
			'displayTitle'    => $displayTitle
		));
        return $this->displayFrontForm( $params,"extraleft");
	}
    /*
    * Add Position for site
    */
    function hooklofPresDemo($params){
        return $this->displayFrontForm( $params,"lofPresDemo");
    }
	/*
	 * register hook right comlumn to display slide in right column
	 */
	function hookrightColumn($params)
	{	
        $multilClass = 'block';
        $displayTitle = 1;
        global $smarty;
		$smarty->assign(array(
			'multilClass'     => $multilClass,
			'displayTitle'    => $displayTitle
		));
        return $this->displayFrontForm( $params,"rightColumn");
	}
	/*
	 * register hook left comlumn to display slide in left column
	 */
	function hookleftColumn($params)
	{	
        $multilClass = 'block';
        $displayTitle = 1;
        global $smarty;
		$smarty->assign(array(
			'multilClass'     => $multilClass,
			'displayTitle'    => $displayTitle
		));
        return $this->displayFrontForm( $params,"leftColumn");

	}
	function hooktop($params)
	{	
        $multilClass = '';
        $displayTitle = 0;
        global $smarty;
		$smarty->assign(array(
			'multilClass'     => $multilClass,
			'displayTitle'    => $displayTitle
		));
        return '</div><div class="clearfix"></div><div>'.$this->displayFrontForm( $params,"top");
		
	}
	function hookfooter($params)
	{	
        $multilClass = '';
        $displayTitle = 0;
        global $smarty;
		$smarty->assign(array(
			'multilClass'     => $multilClass,
			'displayTitle'    => $displayTitle
		));
        return $this->displayFrontForm( $params,"footer");
		
	}
	function hooklofTop($params){
        return $this->displayFrontForm( $params,"lofTop");
	}
	function hookHome($params)
	{
        $multilClass = '';
        $displayTitle = 0;
        global $smarty;
		$smarty->assign(array(
			'multilClass'     => $multilClass,
			'displayTitle'    => $displayTitle
		));
        return $this->displayFrontForm( $params,"home");
	}
    function hooklofsendcart1($params){
        return $this->displayFrontForm( $params,"lofsendcart1");
	}
    function hooklofsendcart2($params){
        return $this->displayFrontForm( $params,"lofsendcart2");
	}
    function hooklofsendcart3($params){
        return $this->displayFrontForm( $params,"lofsendcart3");
	}
    function hooklofsendcart4($params){
        return $this->displayFrontForm( $params,"lofsendcart4");
	}
    
    public function displayPageForm( $params=null )
	{
	   global $params;
		if (!$this->active)
			Tools::display404Error();
		echo $this->displayFrontForm( );
	}

	public function displayFrontForm()
	{
		global $smarty, $cookie,$defaultCountry;
		$error = false;
		$confirm = false;
        $cart = new Cart((int)($cookie->id_cart));
		$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $hookPage = $this->_params->get("hookPage", "shoppingCart");
        $infocart = $this->_params->get("infocart", "1");
        $display = "none";
        $site_url = "";
        $cartlink = "";
        if( $cart ){
			$total_tax = 0;
			$total_free_ship = 0;
			$display_tax_label = 0;
            $product = $cart->getProducts();
            if( $product ){
                $site_url = Tools::htmlentitiesutf8(__PS_BASE_URI__);

                $opc = (Configuration::get("PS_ORDER_PROCESS_TYPE") == 0) ? "order.php" : "order-opc.php";
                $cartlink = $site_url.$opc;

                //display_tax_label
                $display_tax_label = $defaultCountry->display_tax_label;
        		if ($cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')})
        		{
        			$infos = Address::getCountryAndState((int)($cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));
        			$country = new Country((int)$infos['id_country']);
        			if (Validate::isLoadedObject($country))
        				$display_tax_label = $country->display_tax_label;
        		}
                //free-shipping
                $total_tax = $cart->getOrderTotal() - $cart->getOrderTotal(false);

        		if ($total_tax < 0)
        			$total_tax = 0;

        		$total_free_ship = 0;
        		if ($free_ship = Tools::convertPrice((float)(Configuration::get('PS_SHIPPING_FREE_PRICE')), new Currency((int)($cart->id_currency))))
        		{
        			$discounts = $cart->getDiscounts();
        			$total_free_ship =  $free_ship - ($cart->getOrderTotal(true, Cart::ONLY_PRODUCTS) + $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS));
        			foreach ($discounts as $discount)
        				if ($discount['id_discount_type'] == 3)
        				{
        					$total_free_ship = 0;
        					break;
        				}
        		}

        		if ( Tools::isSubmit('submitCart') && _PS_VERSION_ < "1.5")
        		{
        			global $cookie, $link;
                    $products = $cart->getProducts();
        			/* Product informations */
                    $strEmail = $_POST['address_email']; 
                    $arrEmail = preg_split('/[\s]*[,][\s]*/',$strEmail);
        			$productLink = $link->getProductLink($product);
                    $configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_MAIL_SERVER', 'PS_MAIL_USER', 'PS_MAIL_PASSWD', 'PS_SHOP_NAME'));
        			
					/*Calculator*/
                    $shippingCost = $cart->getOrderTotal(true, Cart::ONLY_SHIPPING);
                    $shippingCostTaxExc = $cart->getOrderTotal(false, Cart::ONLY_SHIPPING);
                    $total_wrapping = $cart->getOrderTotal(true, Cart::ONLY_WRAPPING);
                    $total_wrapping_tax_exc = $cart->getOrderTotal(false, Cart::ONLY_WRAPPING);
                    $total_discounts = $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS);
                    $total_discounts_tax_exc = $cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS);
                    $total_products_wt = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
                    $total_products = $cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);
                    $total_price = $cart->getOrderTotal();
                    $total_price_without_tax = $cart->getOrderTotal(false);
                    $priceDisplay = Product::getTaxCalculationMethod();
					/* Fields verifications */
                    foreach ($arrEmail as $str){
		     	       if (empty($arrEmail) OR empty($_POST['full_name']))
            				$error = $this->l('You must fill in all fields.');
                        elseif (!Validate::isEmail($str))
                            $error = $this->l('The e-mail given is invalid.');
  	        		  elseif (!Validate::isName($_POST['full_name']))
            				$error = $this->l('The name given is invalid.');
            			else
            			{
            				/* Email generation */
    						$smarty->assign(array('products', $products,
                                                'shippingCost' => $shippingCost,
                            			        'shippingCostTaxExc' => $shippingCostTaxExc,
                                                'total_wrapping' => $total_wrapping,
                            			        'total_wrapping_tax_exc' => $total_wrapping_tax_exc,
                                                'total_discounts' => $total_discounts,
                                                'total_discounts_tax_exc' => $total_discounts_tax_exc,
                                                'total_products_wt' => $total_products_wt,
                            			        'total_products' => $total_products,
                                                'total_price' => $total_price,
                            			        'total_tax' => $total_tax,
                            			        'total_price_without_tax' => $total_price_without_tax,
                            			        'free_ship' => $total_free_ship,
                                                'display_tax_label' => (bool)$display_tax_label,
                                                'priceDisplay' => $priceDisplay,
                                                'use_taxes' => (int)Configuration::get('PS_TAX')

                            ));
							
                            $htmlProducts = $this->display(__FILE__, 'products_html.tpl');
    						$txtProducts = $this->display(__FILE__, 'products_txt.tpl');
    						$templateVars = array(
                					'{products_html}' => $htmlProducts,
                					'{product_txt}'   => $txtProducts,
                					'{customer}' => ($cookie->customer_firstname ? $cookie->customer_firstname.' '.$cookie->customer_lastname : $this->l('A friend')),
                					'{name}' => Tools::safeOutput($_POST['full_name'])
                				);
                                
                        	/* Email sending */
							$iso = Language::getIsoById((int)($id_lang));
							
            				if (!Mail::Send((int)$id_lang, 'send_to_a_friend', Mail::l('A friend sent you ') . $this->l('his/her basket'), $templateVars, $arrEmail, NULL, ($configuration['PS_SHOP_EMAIL'] ? $configuration['PS_SHOP_EMAIL'] : NULL), ($configuration['PS_SHOP_NAME'] ? $configuration['PS_SHOP_NAME'] : NULL), NULL, NULL, dirname(__FILE__).'/mails/')){
            					$error = $this->l('An error occurred during the process.');
								Tools::redirect($opc);
            				}else{
            				    $display = "block";
            				    Tools::redirect($opc);
            				}   
            			}
                        
                    }
                    
        		}
            }
        		$smarty->assign(array(
        			'errors'     => $error,
        			'confirm'    => $confirm,
        			'hookPage'   => $hookPage,
        			'site_url'   => $site_url,
        			'display'    => $display,
        			'cartlink'   => $cartlink,
        			'infocart'   => $this->_params->get("infocart", "1"),
        			'product'    => $product,
                    'shippingCost' => $cart->getOrderTotal(true, Cart::ONLY_SHIPPING),
			        'shippingCostTaxExc' => $cart->getOrderTotal(false, Cart::ONLY_SHIPPING),
                    'total_wrapping' => $cart->getOrderTotal(true, Cart::ONLY_WRAPPING),
			        'total_wrapping_tax_exc' => $cart->getOrderTotal(false, Cart::ONLY_WRAPPING),
                    'total_discounts' => $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS),
                    'total_discounts_tax_exc' => $cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS),
                    'total_products_wt' => $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS),
			        'total_products' => $cart->getOrderTotal(false, Cart::ONLY_PRODUCTS),
                    'total_price' => $cart->getOrderTotal(),
			        'total_tax' => $total_tax,
			        'total_price_without_tax' => $cart->getOrderTotal(false),
			        'free_ship' => $total_free_ship,
                    'display_tax_label' => (bool)$display_tax_label,
                    'priceDisplay' => Product::getTaxCalculationMethod(),
                    'use_taxes' => (int)Configuration::get('PS_TAX')
        		));
                return $this->display(__FILE__, 'product_page.tpl');                
            
                        
        }
		
	} 	   	   	
   
    public function renderAjaxCartBlock(){
    
  		global $smarty, $cookie, $defaultCountry;
		$error = false;
		$confirm = false;
        $cart = new Cart((int)($cookie->id_cart));
        $hookPage = $this->_params->get("hookPage", "shoppingCart");
        $infocart = $this->_params->get("infocart", "1");

        $display = "none";
        if( $cart ){
            $product = $cart->getProducts();
            if( $product ){
                $site_url = Tools::htmlentitiesutf8(__PS_BASE_URI__);
                //$load_url = $_SERVER['PHP_SELF'];
                $opc = (Configuration::get("PS_ORDER_PROCESS_TYPE") == 0) ? "order.php" : "order-opc.php";
                $cartlink = $site_url.$opc;

                //display_tax_label
                $display_tax_label = $defaultCountry->display_tax_label;
        		if ($cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')})
        		{
        			$infos = Address::getCountryAndState((int)($cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')}));
        			$country = new Country((int)$infos['id_country']);
        			if (Validate::isLoadedObject($country))
        				$display_tax_label = $country->display_tax_label;
        		}
                //free-shipping
                $total_tax = $cart->getOrderTotal() - $cart->getOrderTotal(false);

        		if ($total_tax < 0)
        			$total_tax = 0;

        		$total_free_ship = 0;
        		if ($free_ship = Tools::convertPrice((float)(Configuration::get('PS_SHIPPING_FREE_PRICE')), new Currency((int)($cart->id_currency))))
        		{
        			$discounts = $cart->getDiscounts();
        			$total_free_ship =  $free_ship - ($cart->getOrderTotal(true, Cart::ONLY_PRODUCTS) + $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS));
        			foreach ($discounts as $discount)
        				if ($discount['id_discount_type'] == 3)
        				{
        					$total_free_ship = 0;
        					break;
        				}
        		}

        		$smarty->assign(array(
        			'confirm'    => $confirm,
        			'site_url'   => $site_url,
        			'cartlink'   => $cartlink,
        			'infocart'   => $this->_params->get("infocart", "1"),
        			'product'    => $product,
                    'shippingCost' => $cart->getOrderTotal(true, Cart::ONLY_SHIPPING),
			        'shippingCostTaxExc' => $cart->getOrderTotal(false, Cart::ONLY_SHIPPING),
                    'total_wrapping' => $cart->getOrderTotal(true, Cart::ONLY_WRAPPING),
			        'total_wrapping_tax_exc' => $cart->getOrderTotal(false, Cart::ONLY_WRAPPING),
                    'total_discounts' => $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS),
                    'total_discounts_tax_exc' => $cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS),
                    'total_products_wt' => $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS),
			        'total_products' => $cart->getOrderTotal(false, Cart::ONLY_PRODUCTS),
                    'total_price' => $cart->getOrderTotal(),
			        'total_tax' => $total_tax,
			        'total_price_without_tax' => $cart->getOrderTotal(false),
			        'free_ship' => $total_free_ship,
                    'display_tax_label' => (bool)$display_tax_label,
                    'priceDisplay' => Product::getTaxCalculationMethod(),
                    'use_taxes' => (int)Configuration::get('PS_TAX')

        		));
                return $this->display( __FILE__, 'ajxcart-block.tpl' );                
            }          
        }
    }
    
   /**
    * Get list of sub folder's name 
    */
	public function getFolderList( $path ) {
		$items = array();
		$handle = opendir($path);
		if (! $handle) {
			return $items;
		}
		while (false !== ($file = readdir($handle))) {
			if (is_dir($path . $file))
				$items[$file] = $file;
		}
		unset($items['.'], $items['..'], $items['.svn']);
		return $items;
	}
   /**
    * Render processing form && process saving data.
    */	
	public function getContent()
	{
		$html = "";
		if (Tools::isSubmit('submit'))
		{
			$this->_postValidation();
			if (!sizeof($this->_postErrors))
			{													
		        $definedConfigs = array(
		          /* general config */
		          'hookPage'      => '',
		          'infocart'      => ''
		        );
                $listarticle = Tools::getValue('custom-num');
               
    			 for($i=1; $i<=10; $i++){
                    $definedConfigs[$i."-enable"]    = "";
                    if(Tools::getValue($i."-enable")){
                        $definedConfigs[$i."-filetype"]  = "";                                        
                        $definedConfigs[$i."-path"]      = "";
                        $definedConfigs[$i."-link"]      = "";                                                                            
                        $definedConfigs[$i."-timer"]     = "";
                        $definedConfigs[$i."-target"]    = "";
                        $definedConfigs[$i."-imagePos"]  = "";
                        $definedConfigs[$i."-pan"]       = "";
                        $definedConfigs[$i."-desc"]      = "";
                        $definedConfigs[$i."-title"]     = ""; 
                        $definedConfigs[$i."-preview"]   = "";
                        $definedConfigs[$i."-pan"]       = "";
                        $definedConfigs[$i."-imagePos"]  = "";
                        $definedConfigs[$i."-timer"]     = "";
                    }                                                          
                }
		        foreach( $definedConfigs as $config => $key ){
		      		Configuration::updateValue($this->name.'_'.$config, Tools::getValue($config), true);
		    	}

                $linkArray = Tools::getValue('override_links');
                if($linkArray){
                    foreach ($linkArray as $key => $value) {
                        if (is_null($value) || $value == "") {
                            unset ($linkArray[$key]);
                        }
                    }
                    $override_links = implode(",",$linkArray);
                    Configuration::updateValue($this->name.'_override_links', $override_links, true);
                }
				$delText = '';	
		        if(Tools::getValue('delCaImg')){					
					if(_PS_VERSION_ <="1.4"){						
						$cacheFol = _PS_IMG_DIR_.$this->name;												
					}else{			
						$cacheFol = _PS_CACHEFS_DIRECTORY_.$this->name;							
					}
					if (LofDataSourceBase::removedir($cacheFol)){
						$delText =  $this->l('. Cache folder has been deleted');
					}else{
						$delText =  $this->l('. Cache folder can\'tdeleted');
					}  
				}
		        $html .= '<div class="conf confirm">'.$this->l('Settings updated').$delText.'</div>';
			}
			else
			{
				foreach ($this->_postErrors AS $err)
				{
					$html .= '<div class="alert error">'.$err.'</div>';
				}
			}
			// reset current values.
			$this->_params = new LofParams( $this->name );	
		}
		return $html.$this->_getFormConfig();
	}
    
    
	/**
	 * Render Configuration From for user making settings.
	 *
	 * @return context
	 */
	private function _getFormConfig(){
		$html = '';
	    //$themes=$this->getFolderList( dirname(__FILE__)."/tmpl/" );
	    ob_start();
	    include_once dirname(__FILE__).'/config/lofsendcart.php'; 
	    $html .= ob_get_contents();
	    ob_end_clean(); 
		return $html;
	}
	/**
     * Process vadiation before saving data 
     */
	private function _postValidation()
	{
		
	}
   /**
    * Get value of parameter following to its name.
    * 
	* @return string is value of parameter.
	*/
	public function getParamValue($name, $default=''){
		return $this->_params->get( $name, $default );	
	}
}



 