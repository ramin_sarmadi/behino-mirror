
{foreach from=$products item=product}

	  -----------------------------------------------
      + Image Product: <img id="thumb_{$product.id_image}" src="{$link->getImagelink($product.link_rewrite, $product.id_image, 'small')}"/>
      
	  + Name: {$product.name|escape:'htmlall':'UTF-8'}
      
	  + Ref Code: {if $product.reference}{$product.reference|escape:'htmlall':'UTF-8'}{else}--{/if}

      + Quantity: {($product.total_wt/$product.price_wt)}

	  + Unit Price: {convertPrice price=$product.price_wt}

      + Total: {convertPrice price=$product.total_wt}
      
{/foreach}