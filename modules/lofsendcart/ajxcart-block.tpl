<div id="lofblockcart">
    {if $infocart == 1}
    <div class="lof-title-cart">
		<div class="cart_product first_item">{l s='Product' mod='lofsendcart'}</div>
		<div class="cart_description item">{l s='Description' mod='lofsendcart'}</div>
		<div class="cart_ref item">{l s='Ref.' mod='lofsendcart'}</div>
        <div class="cart_qty item">{l s='Qty' mod='lofsendcart'}</div>
        <div class="cart_unit item">{l s='Unit price' mod='lofsendcart'}</div>
        <div class="cart_total item">{l s='Total' mod='lofsendcart'}</div>
	</div>

    <div class="detail-cart">
        {foreach from=$product item=cart}
		<div id="cart_block_product_{$cart.id_product}{if $cart.id_product_attribute}_{$cart.id_product_attribute}{/if}" class="lof_align_center">
            <div class="list-cart-images"><a href="{$link->getProductLink($cart.id_product)}"><img id="thumb_{$cart.id_image}" src="{$link->getImagelink($cart.link_rewrite, $cart.id_image, 'small')}"/></a></div>
			<div class="list-cart-name"><a href="{$link->getProductLink($cart.id_product)}">{$cart.name|escape:'htmlall':'UTF-8'}</a></div>
            <div class="list-cart-sku">{if $cart.reference}{$cart.reference|escape:'htmlall':'UTF-8'}{else}--{/if}</div>

            <div class="list-cart-qty">{($cart.total_wt/$cart.price_wt)}</div>
            <div class="list-cart-price">
                <span class="price" id="product_price_{$cart.id_product}_{$cart.id_product_attribute}">
    				 {convertPrice price=$cart.price_wt}
    			</span>
            </div>
            <div class="list-cart-total">
                <span class="price">{convertPrice price=$cart.total_wt} </span>
            </div>
		</div>
        {/foreach}
        <div class="lof_summary">
            <!-- cart_total_price ------------------------------------------------------------------------------------>
            {if $use_taxes}
				{if $priceDisplay}
                    <div>
                        <span class="lof_scart_title">{l s='Total products' mod='lofsendcart'}{if $display_tax_label} {l s='(tax excl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
                        <span class="lof_scart_price">{convertPrice price=$total_products}</span>
                    </div>
                {else}
                    <div>
                        <span class="lof_scart_title">{l s='Total products' mod='lofsendcart'}{if $display_tax_label} {l s='(tax incl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
                        <span class="lof_scart_price">{convertPrice price=$total_products_wt}</span>
                    </div>
                {/if}
            {else}
                <div>
                    <span class="lof_scart_title">{l s='Total products:'  mod='lofsendcart'}</span>
                    <span class="lof_scart_price">{convertPrice price=$total_products}</span>
                </div>
            {/if}
            <!-- cart_total_voucher ---------------------------------------------------------------------------------->
            {if $total_discounts != 0}
                {if $use_taxes}
			        {if $priceDisplay}
				        <div>
                            <span class="lof_scart_title">{l s='Total vouchers'  mod='lofsendcart'}{if $display_tax_label} {l s='(tax excl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
                            <span class="lof_scart_price">{displayPrice price=$total_discounts_tax_exc}</span>
                        </div>
			        {else}
				        <div>
                            <span class="lof_scart_title">{l s='Total vouchers'  mod='lofsendcart'}{if $display_tax_label} {l s='(tax incl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
                            <span class="lof_scart_price">{displayPrice price=$total_discounts}</span>
                        </div>
			        {/if}
			    {else}
			        <div>
                        <span class="lof_scart_title">{l s='Total vouchers:'  mod='lofsendcart'}</span>
                        <span class="lof_scart_price">{displayPrice price=$total_discounts_tax_exc}</span>
                    </div>
			    {/if}
            {/if}
            <!-- cart_total_voucher gift-wrapping---------------------------------------------------------------------------------->
            {if $total_wrapping != 0}
                {if $use_taxes}
			        {if $priceDisplay}
				        <div>
                            <span class="lof_scart_title">{l s='Total gift-wrapping'  mod='lofsendcart'}{if $display_tax_label} {l s='(tax excl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
                            <span class="lof_scart_price">{displayPrice price=$total_wrapping_tax_exc}</span>
                        </div>
			        {else}
				        <div>
                            <span class="lof_scart_title">{l s='Total gift-wrapping'  mod='lofsendcart'}{if $display_tax_label} {l s='(tax incl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
                            <span class="lof_scart_price">{displayPrice price=$total_wrapping}</span>
                        </div>
			        {/if}
			    {else}
			        <div>
                        <span class="lof_scart_title">{l s='Total gift-wrapping:'  mod='lofsendcart'}</span>
                        <span class="lof_scart_price">{displayPrice price=$total_wrapping_tax_exc}</span>
                    </div>
			    {/if}
            {/if}
            <!-- cart_total_delivery ---------------------------------------------------------------------------------->
            {if $use_taxes}
				{if $priceDisplay}
					<div {if $shippingCost <= 0} style="display:none;"{/if}>
						<span class="lof_scart_title">{l s='Total shipping'  mod='lofsendcart'}{if $display_tax_label} {l s='(tax excl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
						<span class="lof_scart_price">{displayPrice price=$shippingCostTaxExc}</span>
					</div>
				{else}
					<div {if $shippingCost <= 0} style="display:none;"{/if}>
						<span class="lof_scart_title">{l s='Total shipping'  mod='lofsendcart'}{if $display_tax_label} {l s='(tax incl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
						<span class="lof_scart_price">{displayPrice price=$shippingCost}</span>
					</div>
				{/if}
			{else}
				<div {if $shippingCost <= 0} style="display:none;"{/if}>
					<span class="lof_scart_title">{l s='Total shipping:'  mod='lofsendcart'}</span>
					<span class="lof_scart_price">{displayPrice price=$shippingCostTaxExc}</span>
				</div>
			{/if}
            <!-- cart_total_price ---------------------------------------------------------------------------------------->
            {if $use_taxes}
                <div class="cart_total_price">
					{if $display_tax_label}
						<span class="lof_scart_title">{l s='Total (tax excl.):'  mod='lofsendcart'}</span>
					{else}
						<span class="lof_scart_title">{l s='Subtotal:'  mod='lofsendcart'}</span>
					{/if}
				    <span class="lof_scart_price"  id="total_price_without_tax">{displayPrice price=$total_price_without_tax}</span>
			    </div>
                <div>
  					{if $display_tax_label}
  						<span class="lof_scart_title">{l s='Total tax:'  mod='lofsendcart'}</span>
  					{else}
  						<span class="lof_scart_title">{l s='Estimated Sales Tax:'  mod='lofsendcart'}</span>
  					{/if}
    				<span class="lof_scart_price" id="total_tax">{displayPrice price=$total_tax}</span>
			    </div>
                <div>
  					{if $display_tax_label}
  						<span class="lof_scart_title">{l s='Total (tax incl.):'  mod='lofsendcart'}</span>
  					{else}
  						<span class="lof_scart_title">{l s='Total:'  mod='lofsendcart'}</span>
  					{/if}
				    <span class="lof_scart_price" id="total_price">{displayPrice price=$total_price}</span>
			    </div>
            {else}
                <div>
				    <span class="lof_scart_title">{l s='Total:' mod='lofsendcart'}</span>
				    <span class="lof_scart_price">{displayPrice price=$total_price_without_tax}</span>
    			</div>
            {/if}
            <!-- cart_free_shipping ---------------------------------------------------------------------------------------->
            <div {if $free_ship <= 0 || $isVirtualCart} style="display: none;" {/if}>
                    <span class="lof_scart_title">{l s='Remaining amount to be added to your cart in order to obtain free shipping:'  mod='lofsendcart'}</span>
					<span class="lof_scart_price">{displayPrice price=$free_ship}</span>
			</div>
        </div>
    </div>


    {/if}
    <div class="form-send-email">
		<p>
			<label for="friend-name">{l s='Subject:' mod='lofsendcart'}</label>
			<input type="text" class="input-form" id="friend-name" size="24" name="full_name" value="{if isset($smarty.post.name)}{$smarty.post.name|escape:'htmlall':'UTF-8'|stripslashes}{/if}" />
		</p>
		<p>
			<label for="friend-address">{l s='Friend\'s email:' mod='lofsendcart'}</label>
			<textarea cols="46" rows="3" class="input-form" id="friend-address" name="address_email" ></textarea>
            <br/><i style="padding-left:140px;">{l s='(ex Friend\'s email: email_1@gmail.com, email_2@gmail.com)' mod='lofsendcart'}</i>
		</p>
    </div>
    
	<div class="form-submit">
		<input id="ajaxButton" type="submit" name="submitCart" value="{l s='Send' mod='lofsendcart'}" class="button" />
	</div>
 </div> 