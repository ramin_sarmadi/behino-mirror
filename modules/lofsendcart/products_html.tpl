<table style="width:100%;">
	<thead>
		<tr>
			<th class="cart_product first_item" style="width:80px;">{l s='Product' mod='lofsendcart'}</th>
			<th class="cart_description item">{l s='Description' mod='lofsendcart'}</th>
			<th class="cart_ref item">{l s='Ref.' mod='lofsendcart'}</th>
			<th class="cart_qty item">{l s='Quantity' mod='lofsendcart'}</th>
			<th class="cart_unit item">{l s='Unit price' mod='lofsendcart'}</th>
			<th class="cart_total item">{l s='Total' mod='lofsendcart'}</th>
		</tr>
		</thead>
	{foreach from=$products item=product}
	<tr id="product_{$product.id_product}_{$product.id_product_attribute}">
		<td style="width:80px;">
			<img id="thumb_{$product.id_image}" src="{$link->getImagelink($product.link_rewrite, $product.id_image, 'small')}"/>
		</td>
		<td>
			<h5><a href="{$link->getProductlink($product.id_product, $product.link_rewrite, $product.category)|escape:'htmlall':'UTF-8'}">{$product.name|escape:'htmlall':'UTF-8'}</a></h5>
			
			{if isset($product.attributes) && $product.attributes}<a href="{$link->getProductlink($product.id_product, $product.link_rewrite, $product.category)|escape:'htmlall':'UTF-8'}">{$product.attributes|escape:'htmlall':'UTF-8'}</a>{/if}
		</td>
		<td>
			{if $product.reference}{$product.reference|escape:'htmlall':'UTF-8'}{else}--{/if}
		</td>
        <td>
		    {($product.total_wt/$product.price_wt)}
		</td>
        <td>
			<span class="price" id="product_price_{$product.id_product}_{$product.id_product_attribute}">
				 {convertPrice price=$product.price_wt}
			</span>
		</td>
		<td>
			<span class="price">
				 {convertPrice price=$product.total_wt}
			</span>
		</td>
	</tr>
	{/foreach}
    <tr>
        <td colspan="6" align="right">
           <div class="lof_summary">
                    <!-- cart_total_price ------------------------------------------------------------------------------------>
                    {if $use_taxes}
        				{if $priceDisplay}
                            <div>
                                <span class="lof_scart_title">{l s='Total products'  mod='lofsendcart'}{if $display_tax_label} {l s='(tax excl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
                                <span class="lof_scart_price">{convertPrice price=$total_products}</span>
                            </div>
                        {else}
                            <div>
                                <span class="lof_scart_title">{l s='Total products'  mod='lofsendcart'}{if $display_tax_label} {l s='(tax incl.)'  mod='lofsendcart'}{/if}{l s=':'}</span>
                                <span class="lof_scart_price">{convertPrice price=$total_products_wt}</span>
                            </div>
                        {/if}
                    {else}
                        <div>
                            <span class="lof_scart_title">{l s='Total products:'  mod='lofsendcart'}</span>
                            <span class="lof_scart_price">{convertPrice price=$total_products}</span>
                        </div>
                    {/if}
                    <!-- cart_total_voucher ---------------------------------------------------------------------------------->
                    {if $total_discounts != 0}
                        {if $use_taxes}
        			        {if $priceDisplay}
        				        <div>
                                    <span class="lof_scart_title">{l s='Total vouchers' mod='lofsendcart'}{if $display_tax_label} {l s='(tax excl.)' mod='lofsendcart'}{/if}{l s=':'}</span>
                                    <span class="lof_scart_price">{displayPrice price=$total_discounts_tax_exc}</span>
                                </div>
        			        {else}
        				        <div>
                                    <span class="lof_scart_title">{l s='Total vouchers' mod='lofsendcart'}{if $display_tax_label} {l s='(tax incl.)' mod='lofsendcart'}{/if}{l s=':'}</span>
                                    <span class="lof_scart_price">{displayPrice price=$total_discounts}</span>
                                </div>
        			        {/if}
        			    {else}
        			        <div>
                                <span class="lof_scart_title">{l s='Total vouchers:' mod='lofsendcart'}</span>
                                <span class="lof_scart_price">{displayPrice price=$total_discounts_tax_exc}</span>
                            </div>
        			    {/if}
                    {/if}
                    <!-- cart_total_voucher gift-wrapping---------------------------------------------------------------------------------->
                    {if $total_wrapping != 0}
                        {if $use_taxes}
        			        {if $priceDisplay}
        				        <div>
                                    <span class="lof_scart_title">{l s='Total gift-wrapping' mod='lofsendcart'}{if $display_tax_label} {l s='(tax excl.)' mod='lofsendcart'}{/if}{l s=':'}</span>
                                    <span class="lof_scart_price">{displayPrice price=$total_wrapping_tax_exc}</span>
                                </div>
        			        {else}
        				        <div>
                                    <span class="lof_scart_title">{l s='Total gift-wrapping' mod='lofsendcart'}{if $display_tax_label} {l s='(tax incl.)' mod='lofsendcart'}{/if}{l s=':'}</span>
                                    <span class="lof_scart_price">{displayPrice price=$total_wrapping}</span>
                                </div>
        			        {/if}
        			    {else}
        			        <div>
                                <span class="lof_scart_title">{l s='Total gift-wrapping:' mod='lofsendcart'}</span>
                                <span class="lof_scart_price">{displayPrice price=$total_wrapping_tax_exc}</span>
                            </div>
        			    {/if}
                    {/if}
                    <!-- cart_total_delivery ---------------------------------------------------------------------------------->
                    {if $use_taxes}
        				{if $priceDisplay}
        					<div {if $shippingCost <= 0} style="display:none;"{/if}>
        						<span class="lof_scart_title">{l s='Total shipping' mod='lofsendcart'}{if $display_tax_label} {l s='(tax excl.)' mod='lofsendcart'}{/if}{l s=':'}</span>
        						<span class="lof_scart_price">{displayPrice price=$shippingCostTaxExc}</span>
        					</div>
        				{else}
        					<div {if $shippingCost <= 0} style="display:none;"{/if}>
        						<span class="lof_scart_title">{l s='Total shipping' mod='lofsendcart'}{if $display_tax_label} {l s='(tax incl.)' mod='lofsendcart'}{/if}{l s=':'}</span>
        						<span class="lof_scart_price">{displayPrice price=$shippingCost}</span>
        					</div>
        				{/if}
        			{else}
        				<div {if $shippingCost <= 0} style="display:none;"{/if}>
        					<span class="lof_scart_title">{l s='Total shipping:' mod='lofsendcart'}</span>
        					<span class="lof_scart_price">{displayPrice price=$shippingCostTaxExc}</span>
        				</div>
        			{/if}
                    <!-- cart_total_price ---------------------------------------------------------------------------------------->
                    {if $use_taxes}
                        <div class="cart_total_price">
        					{if $display_tax_label}
        						<span class="lof_scart_title">{l s='Total (tax excl.):' mod='lofsendcart'}</span>
        					{else}
        						<span class="lof_scart_title">{l s='Subtotal:' mod='lofsendcart'}</span>
        					{/if}
        				    <span class="lof_scart_price"  id="total_price_without_tax">{displayPrice price=$total_price_without_tax}</span>
        			    </div>
                        <div>
          					{if $display_tax_label}
          						<span class="lof_scart_title">{l s='Total tax:' mod='lofsendcart'}</span>
          					{else}
          						<span class="lof_scart_title">{l s='Estimated Sales Tax:' mod='lofsendcart'}</span>
          					{/if}
            				<span class="lof_scart_price" id="total_tax">{displayPrice price=$total_tax}</span>
        			    </div>
                        <div>
          					{if $display_tax_label}
          						<span class="lof_scart_title">{l s='Total (tax incl.):' mod='lofsendcart'}</span>
          					{else}
          						<span class="lof_scart_title">{l s='Total:' mod='lofsendcart'}</span>
          					{/if}
        				    <span class="lof_scart_price" id="total_price">{displayPrice price=$total_price}</span>
        			    </div>
                    {else}
                        <div>
        				    <span class="lof_scart_title">{l s='Total:' mod='lofsendcart'}</span>
        				    <span class="lof_scart_price">{displayPrice price=$total_price_without_tax}</span>
            			</div>
                    {/if}
                    <!-- cart_free_shipping ---------------------------------------------------------------------------------------->
                    <div {if $free_ship <= 0 || $isVirtualCart} style="display: none;" {/if}>
                            <span class="lof_scart_title">{l s='Remaining amount to be added to your cart in order to obtain free shipping:' mod='lofsendcart'}</span>
        					<span class="lof_scart_price">{displayPrice price=$free_ship}</span>
        			</div>
                  </div>
        </td>
    </tr>
</table>
