<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14206 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
include_once(dirname(__FILE__).'/../../lofsendcart.php');
class LofsendcartDefaultModuleFrontController extends ModuleFrontController
{
	public $display_column_left = false;
	public $module;
	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
		$this->module = new lofsendcart();
	}
	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess()
	{
		global $cookie, $link,$defaultCountry;
		$error = false;
		$confirm = false;
        $cart = new Cart((int)($cookie->id_cart));
		$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $hookPage = $this->module->getParamValue("hookPage", "shoppingCart");
        $infocart = $this->module->getParamValue("infocart", "1");
		if ( Tools::isSubmit('submitCart') ){
			
			$products = $cart->getProducts();
			/* Product informations */
			$strEmail = $_POST['address_email']; 
			$arrEmail = preg_split('/[\s]*[,][\s]*/',$strEmail);
			$productLink = $link->getProductLink($product);
			$configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_MAIL_SERVER', 'PS_MAIL_USER', 'PS_MAIL_PASSWD', 'PS_SHOP_NAME'));
			
			/*Calculator*/
			$shippingCost = $cart->getOrderTotal(true, Cart::ONLY_SHIPPING);
			$shippingCostTaxExc = $cart->getOrderTotal(false, Cart::ONLY_SHIPPING);
			$total_wrapping = $cart->getOrderTotal(true, Cart::ONLY_WRAPPING);
			$total_wrapping_tax_exc = $cart->getOrderTotal(false, Cart::ONLY_WRAPPING);
			$total_discounts = $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS);
			$total_discounts_tax_exc = $cart->getOrderTotal(false, Cart::ONLY_DISCOUNTS);
			$total_products_wt = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
			$total_products = $cart->getOrderTotal(false, Cart::ONLY_PRODUCTS);
			$total_price = $cart->getOrderTotal();
			$total_price_without_tax = $cart->getOrderTotal(false);
			$priceDisplay = Product::getTaxCalculationMethod();
			/* Fields verifications */
			foreach ($arrEmail as $str){
			   if (empty($arrEmail) OR empty($_POST['full_name']))
					$error = $this->module->l('You must fill in all fields.');
				elseif (!Validate::isEmail($str))
					$error = $this->module->l('The e-mail given is invalid.');
			  elseif (!Validate::isName($_POST['full_name']))
					$error = $this->module->l('The name given is invalid.');
				else
				{
					/* Email generation */
					$this->context->smarty->assign(array('products', $products,
										'shippingCost' => $shippingCost,
										'shippingCostTaxExc' => $shippingCostTaxExc,
										'total_wrapping' => $total_wrapping,
										'total_wrapping_tax_exc' => $total_wrapping_tax_exc,
										'total_discounts' => $total_discounts,
										'total_discounts_tax_exc' => $total_discounts_tax_exc,
										'total_products_wt' => $total_products_wt,
										'total_products' => $total_products,
										'total_price' => $total_price,
										'total_tax' => $total_tax,
										'total_price_without_tax' => $total_price_without_tax,
										'free_ship' => $total_free_ship,
										'display_tax_label' => (bool)$display_tax_label,
										'priceDisplay' => $priceDisplay,
										'use_taxes' => (int)Configuration::get('PS_TAX')

					));
					
					$htmlProducts = $this->module->display(dirname(__FILE__).'/../../lofsendcart.php', 'products_html.tpl');
					$txtProducts = $this->module->display(dirname(__FILE__).'/../../lofsendcart.php', 'products_txt.tpl');
					$templateVars = array(
							'{products_html}' => $htmlProducts,
							'{product_txt}'   => $txtProducts,
							'{customer}' => ($cookie->customer_firstname ? $cookie->customer_firstname.' '.$cookie->customer_lastname : $this->module->l('A friend')),
							'{name}' => Tools::safeOutput($_POST['full_name'])
						);
						
					/* Email sending */
					$iso = Language::getIsoById((int)($id_lang));
					if (!Mail::Send((int)$id_lang, 'send_to_a_friend', Mail::l('A friend sent you ') . $this->module->l('his/her basket'), $templateVars, $arrEmail, NULL, ($configuration['PS_SHOP_EMAIL'] ? $configuration['PS_SHOP_EMAIL'] : NULL), ($configuration['PS_SHOP_NAME'] ? $configuration['PS_SHOP_NAME'] : NULL), NULL, NULL, _PS_MODULE_DIR_.$this->module->name.'/mails/')){
						$error = $this->module->l('An error occurred during the process.');
						Tools::redirect($opc);
					}else{
						$display = "block";
						Tools::redirect($opc);
					}   
				}
				
			}
		}
	}
	public function getTemplatePath(){
		return _PS_MODULE_DIR_.$this->module->name.'/tmpl/';
	}
}
