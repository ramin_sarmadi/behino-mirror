<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
define('PS_ADMIN_DIR',(dirname(  getcwd() ))); 
include(PS_ADMIN_DIR.'/../config/config.inc.php');
/* Getting cookie or logout */  
if(  Tools::getValue('baseURI') ){
	$_SERVER['SCRIPT_NAME'] = 	Tools::getValue('baseURI')."/index.php";
}
 
require_once(dirname(__FILE__).'/init.php');
require_once(dirname(__FILE__).'/libs/defined.php');
require_once( dirname(__FILE__).'/classes/QuickUpload.php' );	
if (Tools::isSubmit('quickUpload') && Tools::getValue('quickUpload') && defined("_LOFDOWNLOAD_DIR_UPLOAD_") && $token = Tools::getValue('token') ) {
	$return = false;
 
	if( trim(Configuration::get( "lofdownload_uptoken" )) == trim($token) || trim(Configuration::get( "lofdownload_token" )) ==  trim($token) ) { 
		
		$lof_mode = (isset($_GET["mode"]) ? $_GET["mode"] : 'upload');
		
		if($lof_mode == 'preview'){
			$configExtensions = Configuration::get("lofdownload_file_pre","jpg|png|gif|zip|rar|pdf|tarz|7z|bmp|doc|txt|jpeg|xml|bmp|docx");
		}elseif($lof_mode == 'icons'){
			$configExtensions = Configuration::get("lofdownload_file_icon","jpg|png");
		}else{
			$configExtensions = Configuration::get("lofdownload_file_exts","jpg|png");
		}
		
		if( !empty($configExtensions) ){
			$configExtensions = preg_split("#\|#", preg_replace( "#\s*#", "", $configExtensions ) );
			$tmp = array_flip( $configExtensions );
			$removeExtensions = array("php","cgi");
			foreach( $removeExtensions as $re ){
				if( isset($tmp[$re]) ){
					unset( $configExtensions[$tmp[$re]] );
				}
			}
			$allowedExtensions = $configExtensions;
		}else{
			$allowedExtensions = array( "jpg", 'png', "gif", "zip", "rar", "pdf", "tarz", "7z", "bmp", "doc", "txt", "jpeg", "xml", "bmp","docx" );
		}
		// max file size in bytes
		$sizeLimit = (Configuration::get("lofdownload_sizelimit") ? Configuration::get("lofdownload_sizelimit") : (int)(ini_get('upload_max_filesize'))) * 1024 * 1024;
		
		$readFolder = _LOFDOWNLOAD_DIR_UPLOAD_;
		if( isset($_GET["mode"]) && $mode = $_GET["mode"] ){
			switch( $mode ){
				case "preview":
						$readFolder = _LOFDOWNLOAD_DIR_UPLOAD_PREVIEWS_;
						break;
				case "icons" :
						$readFolder = _LOFDOWNLOAD_DIR_UPLOAD_ICONS_;
						break;
							
			}
		}

		// concat folder ans subfolder.
		if( isset($_GET["folder"]) && ($explorFolder = $_GET["folder"]) ){
			if( is_dir($readFolder.$explorFolder) ){
				$readFolder = $readFolder.$explorFolder."/";	
			} 
		}

		$uploader = new qqFileUploader( $allowedExtensions, $sizeLimit );
		$result = $uploader->handleUpload(  $readFolder );
	}
	// to pass data through iframe you will need to encode all html tags
	echo htmlspecialchars(json_encode($result), ENT_NOQUOTES); die;
}
echo json_encode("false"); die;
?>