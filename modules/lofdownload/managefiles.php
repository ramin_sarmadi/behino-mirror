<?php 
if( isset($_GET["action"]) && ($_GET["action"] == "delete" || $_GET["action"] == "managefiles") && isset($_GET['token']) ) {
define('PS_ADMIN_DIR',(dirname(  getcwd() ))); 
//include(PS_ADMIN_DIR.'/../config/config.inc.php');
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
require( dirname(__FILE__)."/libs/defined.php" );
require( dirname(__FILE__)."/lofdownload.php" );
require( dirname(__FILE__)."/classes/LofDownloadHelper.php" );
 
if( Configuration::get( "lofdownload_token" ) != $_GET['token'] ){	die( "Nothing Here........" ); }
// create variables for global processing.
$token = $_GET['token'];
$module = new lofdownload();

/* processing to create folder */
$restrictFolders = array(  ); 
if( isset($_POST["createfolder"]) && $_POST["createfolder"] && isset($_POST['folder_name']) && ($newFolder=$_POST['folder_name']) ){
	
	$createInFolder = _LOFDOWNLOAD_DIR_UPLOAD_;
	if( isset($_GET["mode"]) && $mode = $_GET["mode"] ){
		switch( $mode ){
			case "preview":
					$createInFolder = _LOFDOWNLOAD_DIR_UPLOAD_PREVIEWS_;
					break;
			case "icons" :
					$createInFolder = _LOFDOWNLOAD_DIR_UPLOAD_ICONS_;
					break;
						
		}
	}
	if(  isset($_GET["folder"]) && ($inFolder = $_GET["folder"]) ){
	 	$createInFolder = str_replace("//","/",$createInFolder.$inFolder."/");
	}
	if( !is_dir($createInFolder.$newFolder) ){
		@mkdir( $createInFolder.$newFolder,777 );
	}
}
// init list of base URL;
$readFolder = _LOFDOWNLOAD_DIR_UPLOAD_;
if( !isset($_GET['field']) ){
	$_GET['field'] = "file_path";
}
$baseURI  =  __PS_BASE_URI__."modules/"._LOFDOWNLOAD_MODULE_NAME_."/managefiles.php?action=managefiles&token=".$token."&field=".$_GET['field'];
$baseDelURI  =  __PS_BASE_URI__."modules/"._LOFDOWNLOAD_MODULE_NAME_."/managefiles.php?action=delete&token=".$token."&field=".$_GET['field'];
$uploadURI = __PS_BASE_URI__."modules/"._LOFDOWNLOAD_MODULE_NAME_."/quickupload.php?quickUpload=1&token=".$token."&field".$_GET['field'];
 
if( isset($_GET["mode"]) && $mode = $_GET["mode"] ){
	switch( $mode ){
		case "preview":
				$readFolder = _LOFDOWNLOAD_DIR_UPLOAD_PREVIEWS_;
				$baseURI .="&mode=".$mode;
				$uploadURI .="&mode=".$mode;
				$baseDelURI .="&mode=".$mode;
				break;
		case "icons" :
				$readFolder = _LOFDOWNLOAD_DIR_UPLOAD_ICONS_;
				$baseURI .="&mode=".$mode;
				$uploadURI .="&mode=".$mode;
				$baseDelURI .="&mode=".$mode;
				break;
	}
}
 
$backFolderLink = "";
$explorFolder = "";
// concat folder ans subfolder.
if( isset($_GET["folder"]) && ($explorFolder = $_GET["folder"]) ){
	if( is_dir($readFolder.$explorFolder) ){
		$tmp = explode("/", str_replace("\\","/",$explorFolder) );
		if( count($tmp) > 1 ){
			unset($tmp[count($tmp)-1]);
			$backFolderLink = $baseURI."&folder=".implode("/",$tmp);
		}elseif( count($tmp) == 1) {
			$backFolderLink =$baseURI;
		}
		$readFolder = $readFolder.$explorFolder."/";	
		$explorFolder=$explorFolder."/";
	}else {
		$explorFolder="";	
	}
}
$explorFolder = str_replace( "//", "/",$explorFolder);
$readFolder = str_replace( "//", "/", $readFolder );
if( isset($_GET["action"]) && $_GET["action"] == "delete" ){
	if( isset($_GET["file"]) ){
		$file = $_GET["file"];  
		if( $file != "" && is_file($readFolder.$file) && file_exists($readFolder.$file) ){  
			@unlink( $readFolder.$file );	
		}
	}
	if( isset($_GET["delfolder"]) ){
		$delfolder = $_GET["delfolder"];
		if( is_dir($readFolder.$delfolder) && !in_array($delfolder,$restrictFolders) ){  
			LofDownloadHelper::deleteFolder( $readFolder.$delfolder );
		}
	}
}
$mfile = Tools::getValue("file"); 
$files = LofDownloadHelper::getFileList( $readFolder );
$folders =  LofDownloadHelper::getFolderList( $readFolder );

$site_url = '';
echo '		<script type="text/javascript" src="'._PS_JS_DIR_.'jquery/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="'._PS_JS_DIR_.'jquery/plugins/jquery.hoverIntent.js"></script> 
 
	<link href="'.__PS_BASE_URI__.'modules/lofdownload/assets/admin/form.css" type="text/css" rel="stylesheet"/>
		';
?>
<div id="file-management">
    <div> <span><?php echo $module->l( "Found") ." : " . count( $files ); ?> <?php echo $module->l("Files"); ?></div>
	<div class="path-explain">
		<?php echo $module->l( "Listed File In The Folder" ); ?> : <b><?php echo $readFolder;?></b>
   
    </div>
    <?php if( $backFolderLink ) { ?>
    
    	<a href="<?php echo $backFolderLink;?>"><?php echo $module->l("Back Previous Folder");?></a>
    <?php } ?>
    <table  class="lof-list-files" cellpadding="0" cellspacing="1">
    	<tr class="tbl-header">
        	<th></th>
        	<th><?php echo $module->l( "Name" ) ; ?></th>
            <th><?php echo $module->l( "Size" ) ; ?></th>
            <th><?php echo $module->l( "Type" ) ; ?></th>
            <th><?php echo $module->l( "Delete" ) ; ?></th>
        </tr>
        <?php if( is_array($folders) ) : ?>
        	
        <?php foreach( $folders as $folder ): ?>
        <tr>
        	<td></td>
        	<td><a href="<?php echo $baseURI;?>&folder=<?php echo $explorFolder.$folder?>"><?php echo $folder ; ?></a></td>
            <td><?php echo $module->l( "x" ) ; ?></td>
            <td><?php echo $module->l( "Folder" ) ; ?></td>
               <td>
               <?php if( !in_array($folder,$restrictFolders) ) { ?>
               <a href="<?php echo $baseDelURI;?>&folder=<?php echo $explorFolder;?>&delfolder=<?php echo $folder?>" class="delete-file"  id="<?php echo $folder?>"><span><?php echo $module->l( "Delete" ); ?></span></a>
               
               <?php } ?>
               </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
        <?php if( is_array($files) ) : ?>
       	<?php $i = 0; foreach( $files as $file ): $i++;?>
    	<tr class="<?php echo ( $i%2==0? "odd":"even") ?>">
            <td></td>
            <td class="file-item">
				<a href="<?php echo $explorFolder.$file;?>" id="<?php echo $explorFolder.$file;?>" onClick="return false;" class="select-file"><span class="<?php echo($mfile==$file?"active":"");?>"><?php echo $file; ?></span></a>
            
            </td>
        	<td><?php echo @filesize( $readFolder.$file ); ?> K</td>
			<td><?php 
					preg_match( "#\w{3}$#", $file, $match );
					echo ( isset($match[0]) ? LofDownloadHelper::getMineType($match[0]): "unknown" ); 
					
			?></td>
            <td><a href="<?php echo $baseDelURI;?>&file=<?php echo $file?>" class="delete-file"  id="<?php echo $file?>"><span><?php echo $module->l( "Delete" ); ?></span></a></td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
    </table>
    <div></div>
    
    <Div>
    	<?php echo '<script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'._LOFDOWNLOAD_MODULE_NAME_.'/assets/admin/fileuploader.js"></script>';  ?>
        <link type="text/css" rel="stylesheet" href="<?php echo __PS_BASE_URI__.'modules/'._LOFDOWNLOAD_MODULE_NAME_.'/assets/admin/fileuploader.css';?>" />
        	<div id="files-container">
                <div align="create-folder">
                	<span class="text-explain"><?php echo $module->l("Create a subfolder in this folder");?></span>
                    <form action="<?php echo $baseURI;?>&folder=<?php echo $explorFolder;?>" method="post">
                             <label><?php echo $module->l("Folder Name:"); ?></label>
                            <input name="folder_name" value="" />
                            <input type="hidden" name="createfolder" value="1"/>
                            <input type="submit" name="btnCreateFolder" value="<?php echo $module->l("Create"); ?>" />
                    </form>
                </div>
                <div class="quickupload-cotaniner"> 
                	<span class="text-explain"><?php echo $module->l("Select one or multiple files to upload in this folder");?></span>
                    <div id="file-uploader">		
                        <noscript>			
                            <p>Please enable JavaScript to use file uploader.</p>
                            <!-- or put a simple form for upload here -->
                        </noscript>         
                    </div>
                      <a href="" id="refreshFolders"><?php echo $module->l("Please Refresh to see New Files");?></a>
                </div>
                <div class="clear"></div>
          </div>  
        <script type="text/javascript">        
            function createUploader(){     
				$("#refreshFolders").hide();
                var uploader = new qq.FileUploader({
                    element: document.getElementById('file-uploader'),
                    action: '<?php echo $uploadURI;?>&folder=<?php echo $explorFolder;?>',
                    debug: false,
					onComplete:function(){  $("#refreshFolders").show(1000); }
                });           
            }
            // in your app create uploader as soon as the DOM is ready
            // don't wait for the window to load  
            window.onload = createUploader;     
             $("#quickuploadForm").hide();
             $("#showquickuploadForm").click( function(){ $("#quickuploadForm").fadeToggle();  } );
                     
        </script> 
    </Div>
</div>

<script type="text/javascript"> 
	var parentWindow = $( parent.document.body );
 <?php 
 		$field = $_GET["field"];
	echo '$("body .select-file").click(function(){ 
			$(".select-file span").removeClass("active");
			$(this).find("span").addClass("active");
			parentWindow.find("#'.$field.'").val(  $(this).attr("href") ); 
			parent.$.fancybox.close();
	});
	';?>
	$(".delete-file").click( function(){
		if( confirm("<?php echo $module->l("Are you sure to delete this!!!");?>") )
			return true;
		else 
			return false;
	} );
</script>
<?php } else { die("Nothing Here..."); } ?>