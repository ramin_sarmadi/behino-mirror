<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */

if (!defined('_CAN_LOAD_FILES_'))
	exit;
$site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);

?>
<script type="text/javascript">
	var ajaxUrl = "<?php echo $site_url.'modules/'.$this->name.'/process.php';?>";
	var id_language = <?php echo $this->_defaultFormLanguage ;?>;
</script>
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.css";?>" type="text/css" media="screen" charset="utf-8" />
<script type="text/javascript" src="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.js";?>"></script>

<h3><?php echo $this->l('Lof Customer Upload Configuration');?></h3>
<?php 
$yesNoLang = array("0"=>$this->l('No'),"1"=>$this->l('Yes'));
?>
<form action="<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>" method="post" id="lofform">
 <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
  <fieldset>
    <legend><img src="../img/admin/contact.gif" /><?php echo $this->l('BackEnd Setting'); ?></legend>
    <div class="lof_config_wrrapper clearfix">
      <ul>
        <?php
			echo $this->_params->inputTag("dir_upload",$this->getParamValue("dir_upload","lofdownloads"),$this->l('Directory'),'class="text_area"','class="row"',
																		'','Enter folder name where store files from you upload via Download Management');
			
			echo $this->_params->inputTag("catimg_height",$this->getParamValue("catimg_height",45),$this->l('Category Image Height'),'class="text_area"','class="row"',
																												'','Enter Width of Category Image In numberic');
			echo $this->_params->inputTag("catimg_width",$this->getParamValue("catimg_width",45),$this->l('Category Image Width'),'class="text_area"','class="row"','','Enter Width of Category Image In numberic');
			echo $this->_params->textareaTag("file_exts",$this->getParamValue("file_exts","jpg|png|gif|zip|rar|pdf|tarz|7z|bmp|doc|txt|jpeg|xml|bmp|docx"),$this->l('File Extension Upload'),'class="text_area" cols="68"','class="row"',
																																								   'Enter Name Of Extension file you would like to the sytem process when upload in the Manage Download');         
			echo $this->_params->textareaTag("file_pre",$this->getParamValue("file_pre","jpg|png"),$this->l('File Preview Extension Upload'),'class="text_area" cols="68"','class="row"',
																																								   'Enter Name Of Extension file you would like to the sytem process when upload in the Manage Download');           
			echo $this->_params->textareaTag("file_icon",$this->getParamValue("file_icon","jpg|png"),$this->l('File Icon Extension Upload'),'class="text_area" cols="68"','class="row"',
																																								   'Enter Name Of Extension file you would like to the sytem process when upload in the Manage Download');         
			echo $this->_params->inputTag("sizelimit",$this->getParamValue("sizelimit",(int)(ini_get('upload_max_filesize'))),$this->l('Size Limit (MB)'),'class="text_area"','class="row"','',$this->l('The max size can not exceed the max size of Host'));
        ?>
      </ul>
    </div>
  </fieldset>
  
  <fieldset>
    <legend><img src="../img/admin/contact.gif" /><?php echo $this->l('Front End Setting'); ?></legend>
    <div class="lof_config_wrrapper clearfix">
      <ul>
        <?php
			$mod_title = array();
			$meta_title = array();
			$meta_des = array();
			$meta_key = array();
			foreach($this->_languages as $language){
				$mod_title[$language['id_lang']] = $this->getParamValue("mod_title_".$language['id_lang'],'Download');
				$meta_title[$language['id_lang']] = $this->getParamValue("meta_title_".$language['id_lang'],'download manager');
				$meta_des[$language['id_lang']] = $this->getParamValue("meta_des_".$language['id_lang'],'download manager');
				$meta_key[$language['id_lang']] = $this->getParamValue("meta_key_".$language['id_lang'],'download manager');
			}
			
			echo $this->_params->inputTagLang($this->languages,$this->_divLangName,'mod_title',$mod_title,$this->l('Module title'),'class="text_area"','class="row"','','');
			echo $this->_params->inputTag("catimg_height",$this->getParamValue("catimg_height",45),$this->l('Category Image Height'),'class="text_area"','class="row"',''); 
			echo $this->_params->inputTagLang($this->languages,$this->_divLangName,'meta_title',$meta_title,$this->l('Meta title'),'class="text_area"','class="row"','','');
			echo $this->_params->inputTagLang($this->languages,$this->_divLangName,'meta_des',$meta_des,$this->l('Meta description'),'class="text_area"','class="row"','','');
			echo $this->_params->inputTagLang($this->languages,$this->_divLangName,'meta_key',$meta_key,$this->l('Meta keywords'),'class="text_area"','class="row"','','');
			
 
        	
		 
        ?>
      </ul>
    </div>
  </fieldset>
  <fieldset>
    <legend><img src="../img/admin/contact.gif" /><?php echo $this->l('Uninstall Setting'); ?></legend>
    <div class="lof_config_wrrapper clearfix">
      <ul>
        <?php
 
			echo $this->_params->radioBooleanTag("remove_db", $yesNoLang, $this->getParamValue("remove_db",0),$this->l('Remove Database'),'class="select-option"','class="row"','');
        ?>
      </ul>
    </div>
  </fieldset>
<br />
  <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
  	<fieldset><legend><img src="../img/admin/comment.gif" alt="" title="" /><?php echo $this->l('Information');?></legend>    	
    	<ul>
             <li>+ <a target="_blank" href="http://landofcoder.com/supports/forum.html"><?php echo $this->l('Forum support');?></a></li>
             <li>+ <a target="_blank" href="http://landofcoder.com"><?php echo $this->l('Customization/Technical Support Via Email');?>.</a></li>
        </ul>
        <br />
        @copyright: <a href="http://landofcoder.com">LandOfCoder.com</a>
    </fieldset>
</form>