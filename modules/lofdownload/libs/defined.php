<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */

$downloaddir =  Configuration::get("lofdownload_dir_upload")? Configuration::get("lofdownload_dir_upload"):"lofdownloads";

$downloaddir = _PS_ROOT_DIR_.'/'.$downloaddir."/";
$categorydir =  _PS_IMG_DIR_."lofcategory/";
$iconsdir = _PS_UPLOAD_DIR_."icons/" ;
$previewsdir = _PS_UPLOAD_DIR_."previews/" ;

if( !is_dir($downloaddir) ){
	mkdir( $downloaddir,0777 );	
}
if( !is_dir($categorydir) ){
	mkdir( $categorydir,0777 );	
}
if( !is_dir($previewsdir) ){
	mkdir( $previewsdir,0777 );	
}

if( !is_dir($iconsdir) ){
	mkdir( $iconsdir,0777 );	
}

define( "_LOFDOWNLOAD_DIR_PREVIEWS_FOLDER_", "upload/previews" );
define( "_LOFDOWNLOAD_DIR_CATNAME_", "lofcategory"  );
define( "_LOFDOWNLOAD_DIR_UPLOAD_", $downloaddir );
define( "_LOFDOWNLOAD_DIR_UPLOAD_ICONS_", $iconsdir );
define( "_LOFDOWNLOAD_DIR_UPLOAD_PREVIEWS_", $previewsdir );
define( "_LOFDOWNLOAD_DIR_CATIMAGE_", $categorydir );
define( "_LOFDOWNLOAD_MODULE_NAME_", "lofdownload" );
define( "_LOFDOWNLOAD_MODULE_HOT_HITS_NUM_", 100 );
define( "_LOFDOWNLOAD_MODULE_NEW_DAY_", 1 );
define( "_LOFDOWNLOAD_MODULE_UPDATE_DAY_", 1 );

?>