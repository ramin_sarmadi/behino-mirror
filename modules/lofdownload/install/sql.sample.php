<?php 

$query = "
 
CREATE TABLE IF NOT EXISTS `ps_lofdownload` (
  `id_lofdownload` int(11) NOT NULL AUTO_INCREMENT,
  `id_lofdcategory` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `preview_file` varchar(255) NOT NULL,
  `icon_file` varchar(255) NOT NULL,
  `link_video` varchar(255) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `title` text NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `file_path` varchar(255) NOT NULL,
  `version` varchar(10) NOT NULL,
  `download` int(11) NOT NULL,
  `file_size` varchar(50) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` varchar(255) NOT NULL,
  `file_type` varchar(10) NOT NULL,
  PRIMARY KEY (`id_lofdownload`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8  ;


CREATE TABLE IF NOT EXISTS `ps_lofdcategory` (
  `id_lofdcategory` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) unsigned NOT NULL,
  `level_depth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `nleft` int(10) unsigned NOT NULL DEFAULT '0',
  `nright` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lofdcategory`),
  KEY `category_parent` (`id_parent`),
  KEY `nleftright` (`nleft`,`nright`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;


";
?>