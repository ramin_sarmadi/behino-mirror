<?php 

 $query = " 
 
 	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_lofdcategory` (
	  `id_lofdcategory` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `id_parent` int(10) unsigned NOT NULL,
	  `level_depth` tinyint(3) unsigned NOT NULL DEFAULT '0',
	  `nleft` int(10) unsigned NOT NULL DEFAULT '0',
	  `nright` int(10) unsigned NOT NULL DEFAULT '0',
	  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
	  `date_add` datetime NOT NULL,
	  `date_upd` datetime NOT NULL,
	  `position` int(10) unsigned NOT NULL DEFAULT '0',
	  PRIMARY KEY (`id_lofdcategory`),
	  KEY `category_parent` (`id_parent`),
	  KEY `nleftright` (`nleft`,`nright`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
	
	
	
	INSERT INTO `_DB_PREFIX_lofdcategory` (`id_lofdcategory`, `id_parent`, `level_depth`, `nleft`, `nright`, `active`, `date_add`, `date_upd`, `position`) VALUES
	(1, 0, 0, 1, 8, 1, '2012-03-07 11:34:11', '2012-03-07 11:34:16', 0);
	
	
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_lofdcategory_group` (
	  `id_lofdcategory` int(10) unsigned NOT NULL,
	  `id_group` int(10) unsigned NOT NULL,
	  PRIMARY KEY (`id_lofdcategory`,`id_group`),
	  KEY `id_lofdcategory` (`id_lofdcategory`),
	  KEY `id_group` (`id_group`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
	
	INSERT INTO `_DB_PREFIX_lofdcategory_group` (`id_lofdcategory`, `id_group`) VALUES
	(1, 1);
	
	
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_lofdcategory_lang` (
	  `id_lofdcategory` int(10) unsigned NOT NULL,
	  `id_lang` int(10) unsigned NOT NULL,
	  `name` varchar(128) NOT NULL,
	  `description` text,
	  `link_rewrite` varchar(128) NOT NULL,
	  `meta_title` varchar(128) DEFAULT NULL,
	  `meta_keywords` varchar(255) DEFAULT NULL,
	  `meta_description` varchar(255) DEFAULT NULL,
	  PRIMARY KEY (`id_lofdcategory`,`id_lang`),
	  KEY `category_name` (`name`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
	
	
	INSERT INTO `_DB_PREFIX_lofdcategory_lang` (`id_lofdcategory`, `id_lang`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
	(1, 1, 'Home', NULL, '', NULL, NULL, NULL),
	(1, 2, 'Home', NULL, '', NULL, NULL, NULL),
	(1, 3, 'Home', NULL, '', NULL, NULL, NULL),
	(1, 4, 'Home', NULL, '', NULL, NULL, NULL),
	(1, 5, 'Home', NULL, '', NULL, NULL, NULL) ;
	
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_lofdownload` (
	  `id_lofdownload` int(11) NOT NULL AUTO_INCREMENT,
	  `id_lofdcategory` int(11) NOT NULL,
	  `id_customer` int(11) NOT NULL,
	  `preview_file` varchar(255) NOT NULL,
	  `icon_file` varchar(255) NOT NULL,
	  `link_video` varchar(255) NOT NULL,
	  `date_add` datetime NOT NULL,
	  `date_upd` datetime NOT NULL,
	  `active` tinyint(1) NOT NULL DEFAULT '0',
	  `title` text NOT NULL,
	  `ip_address` varchar(100) NOT NULL,
	  `file_path` varchar(255) NOT NULL,
	  `version` varchar(10) NOT NULL,
	  `download` int(11) NOT NULL,
	  `file_size` varchar(50) NOT NULL,
	  `filename` varchar(255) NOT NULL,
	  `filesize` varchar(255) NOT NULL,
	  `file_type` varchar(10) NOT NULL,
	  PRIMARY KEY (`id_lofdownload`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
	
	
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_lofdownload_lang` (
	  `id_lang` int(11) NOT NULL,
	  `id_lofdownload` int(11) NOT NULL,
	  `description` text NOT NULL,
	  `information` text NOT NULL,
	  `name` varchar(255) NOT NULL
	) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	
	
	
	 
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_lofdownload_rating` (
	  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
	  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
	  `lastip` varchar(50) NOT NULL DEFAULT '',
	  `id_lofdownload` varchar(11) NOT NULL
	) ENGINE=MyISAM DEFAULT CHARSET=utf8;
	
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_lofproducts_download` (
	  `id_lofdownload` int(11) NOT NULL,
	  `id_product` int(11) NOT NULL
	) ENGINE=MyISAM DEFAULT CHARSET=utf8;

 
 ";
?>