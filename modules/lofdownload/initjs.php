<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
?>
<script type="text/javascript">
	<?php if(isset($showformedit) || isset($producttabcontent)){?>
    jQuery(document).ready(function() {
        $("area[rel^='prettyPhoto']").prettyPhoto();
		$(".lofcontent a[rel^='prettyPhoto'],.formupload a[rel^='prettyPhoto']").prettyPhoto({
			animation_speed:'<?php echo $animaS;?>',
			slideshow:<?php echo $slideshow;?>, 
			autoplay_slideshow: <?php echo ($autoSL ? 'true' : 'false');?>,
			opacity: <?php echo $opacity;?>,
			show_title: <?php echo ($show_title ? 'true' : 'false');?>,
			allow_resize: <?php echo ($allow_rez ? 'true' : 'false');?>,
			default_width: <?php echo $de_width;?>,
			default_height: <?php echo $de_height;?>,
			modal: <?php echo ($modal ? 'true' : 'false');?>,
			deeplinking: <?php echo ($deeplink ? 'true' : 'false');?>,
			overlay_gallery: <?php echo ($overlayG ? 'true' : 'false');?>
		});
		jQuery('#sendFile').uploadProgress({
			progressURL:'<?php echo _MODULE_DIR_.$this->name; ?>/process.php',
			debugDisplay:'#lofmess', 
			displayFields : ['result'],
			start: function() { jQuery('input[type=submit]', this).val('Submit').attr('disabled',true); },
			success: function(o) {
				jQuery('input', this).removeAttr('disabled');
				if($('#lofcaptcha').length != 0){
					$('#lofcaptcha').attr('src','<?php echo _MODULE_DIR_.$this->name; ?>/captcha.php?rand=' + Math.random());
				}
				<?php if(isset($showformedit)){ ?>
				//window.setTimeout(window.parent.location="<?php echo _MODULE_DIR_.$this->name; ?>/upload-program.php",5000);
				
				var id_lofcustomerupload = jQuery('#id_lofcustomerupload').val();
				var trId = $('#inputId').val();
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo _MODULE_DIR_.$this->name; ?>/process.php",
					data: "task=getData&id_lofcustomerupload="+id_lofcustomerupload+"&trId="+trId,
					success: function(json_data){
						if(json_data['product']){
							var str = '<td><img src="'+json_data['product']['thumb']+'" alt="'+json_data['product']['title']+'" /></td>\
								<td>'+json_data['product']['title']+'</td>\
								<td>'+json_data['product']['date']+'</td>\
								<td>\
								<a href="javascript:void(0)" title="edit" onclick="lofprocess(\'edit\','+id_lofcustomerupload+',\''+trId+'\')"><img src="../../img/admin/edit.gif" alt="edit"/></a>\
								<a href="javascript:void(0)" title="delete" onclick="lofprocess(\'delete\','+id_lofcustomerupload+',\''+trId+'\')"><img src="../../img/admin/delete.gif" alt="delete"/></a>\
								</td>\
								';
							document.getElementById(trId).innerHTML = str;
							var img = $('#imgEdit').attr('src',json_data['product']['thumbsmall']);
						}
					}
				});
				<?php } ?>
			}
		});
		$('#lofrefresh').click(function(){
			$('#lofcaptcha').attr('src','<?php echo _MODULE_DIR_.$this->name; ?>/captcha.php?rand=' + Math.random());
		});
    });
	<?php } ?>
	<?php if(isset($showfileupload) || isset($producttabcontent)) { ?>
	function lofpaging(id_product,page){
		$.ajax({
			type: "POST",
			url: "<?php echo _MODULE_DIR_.$this->name; ?>/process.php",
			data: "task=paging&id_product="+id_product+"&page="+page,
			success: function(msg){
				$("#idTabLofUpload .lofcontent").html(msg);
			}
		});
	}
	<?php } ?>
	<?php if(isset($showfileupload)) {?>
	function lofprocess(loftask,id,trId){
		if(loftask == 'delete'){
			var answer = confirm ("Are you sure you want to delete?")
			if(answer){
				$.ajax({
					type: "POST",
					dataType: 'json',
					url: "<?php echo _MODULE_DIR_.$this->name; ?>/process.php",
					data: "task="+loftask+"&id_lofcustomerupload="+id+"&trId="+trId,
					success: function(json_data){
						if(json_data['result'] == 1){
							var divid = document.getElementById( trId );
							divid.parentNode.removeChild( divid );
						}
					}
				});
			}else{
				return true;
			}
		}else{
			$.ajax({
				type: "POST",
				url: "<?php echo _MODULE_DIR_.$this->name; ?>/process.php",
				data: "task="+loftask+"&id_lofcustomerupload="+id+"&trId="+trId,
				success: function(msg){
					$("#lofedit").html(msg);
					$("html,body").animate({scrollTop: $("#lofedit").offset().top},"slow");
				}
			});
		}
	}
	<?php } ?>
</script>
<?php if( isset($producttabcontent)){?>
	<div id="fb-root"></div>
	<script type="text/javascript" src="http://connect.facebook.net/vi_VN/all.js#xfbml=1"></script>
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	<script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>
<?php } ?>