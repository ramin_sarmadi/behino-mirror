<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
session_start();
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
require_once(_PS_ROOT_DIR_.'/images.inc.php');
global $cookie;
include_once(dirname(__FILE__).'/lofdownload.php');
include_once(dirname(__FILE__).'/classes/LofDownloadCategory.php');
include_once(dirname(__FILE__).'/classes/LofDownloadHelper.php');
include_once(dirname(__FILE__).'/classes/LofDownloadRating.php');

include_once(dirname(__FILE__).'/classes/LofClassDownload.php');
include_once(dirname(__FILE__).'/libs/defined.php');
$context = Context::getContext();
$context->controller->addCSS(_PS_CSS_DIR_.'jquery.fancybox-1.3.4.css', 'screen');
$context->controller->addJS(array(_PS_JS_DIR_.'jquery/jquery.fancybox-1.3.4.js' ));
$lofdownload = new lofdownload();
?>
<?php 
	global $cookie, $link;
	$id_category   = (int)Tools::getValue("id_category");
	$id_download   = (int)Tools::getValue("id_download"); 
	$helper 	   = new LofDownloadHelper();
	
	if( ($id_download) > 0 && $id_category <= 0 ){
		
		$context->controller->addJS( _MODULE_DIR_.'lofdownload/assets/rate/behavior.js' );
		$context->controller->addJS( _MODULE_DIR_.'lofdownload/assets/rate/rating.js' );
		$error = Tools::getValue( "error" );
		
 
		$rating  = new LofDownloadRating( $id_download );
 		$file = LofClassDownload::getDownloadInfo( $id_download, $cookie->id_lang );
		if($file){
			if($file['filesize'] < 1024){
				$file['filesize_dv'] = 'Byte';
			}elseif($file['filesize']/1024 < 1024){
				$file['filesize'] = Tools::ps_round($file['filesize']/1024,2);
				$file['filesize_dv'] = 'Kb';
			}elseif($file['filesize']/(1024*1024) < 1024){
				$file['filesize'] = Tools::ps_round($file['filesize']/(1024*1024),2);
				$file['filesize_dv'] = 'Mb';
			}
		}
		if(  (int) $rating->id > 0  ){
			$file['rating_sum']   = $rating->rating_sum;
			$file['rating_count'] = $rating->rating_count;
		}
		
	
	 	$avariable   = $file != null ? true : false;
		$category    = new LofDownloadCategory( $file['id_lofdcategory'], $cookie->id_lang );
	 	$checkAccess = $category->checkAccess( $cookie->id_customer );
	
 		$fileError = false; 
		if( (int)$file['id_lofdcategory'] <= 0 ){
			$error = true;
			$fileError = true;
		}
		
		$smarty->assign(array(
			'pageTitle' => "Downloads",		
			'file'      => $file,
			'avariable' => $avariable,
			'helper'    => $helper,
			'checkAccess' => $checkAccess,
			'error'     => $error,
			'fileError' => $fileError,
			'link' 		=> $link,
			
			'meta_title'  		=> $file['name'],
			
			'HOTHIT_NUMBER' => _LOFDOWNLOAD_MODULE_HOT_HITS_NUM_,
			'NEW_DAY' => _LOFDOWNLOAD_MODULE_NEW_DAY_,
			'UPDATE_DAY' => _LOFDOWNLOAD_MODULE_UPDATE_DAY_
		));	
		include(dirname(__FILE__).'/../../header.php');
		echo $lofdownload->display(dirname(__FILE__).'/lofdownload.php', 'tmpl/frontend/detail.tpl');
		
	}else {
		
		$id_category   = $id_category ? $id_category:1;	
		$categories    = LofDownloadCategory::getChildren( $id_category, $cookie->id_lang );
		
		foreach( $categories as $key => $category ){
			/*$categories[$key]['count_download'] = LofDownloadCategory::countDownloadByCategoryId( $category['id_lofdcategory'], $cookie->id_lang, true );*/
			$objCate = new LofDownloadCategory($category['id_lofdcategory']);
			$categories[$key]['count_sub'] = count($objCate->getSubCategories($cookie->id_lang, true));
		}

		$parent = new LofDownloadCategory( $id_category );
		$page = ((int)(Tools::getValue('p')) > 0 ? (int)(Tools::getValue('p')) : 1);
		$limit = ((int)(Tools::getValue('n')) > 0 ? (int)(Tools::getValue('n')) : 30);
		$orderby    = LofDownloadHelper::getCategoryOrderBy( Tools::getValue("orderby") );
		$orderway   = LofDownloadHelper::getCategoryOrderway( Tools::getValue("orderway") );
		$allfiles	= $parent->getFilesByCategoryId( $id_category, $cookie->id_lang, true, $orderby, $orderway );
		$files		= $parent->getFilesByCategoryId( $id_category, $cookie->id_lang, true, $orderby,$orderway, $limit, $page );
		if($files)
			foreach($files as &$f){
				if($f['filesize'] < 1024){
					$f['filesize_dv'] = 'Byte';
				}elseif($f['filesize']/1024 < 1024){
					$f['filesize'] = Tools::ps_round($f['filesize']/1024,2);
					$f['filesize_dv'] = 'Kb';
				}elseif($f['filesize']/(1024*1024) < 1024){
					$f['filesize'] = Tools::ps_round($f['filesize']/(1024*1024),2);
					$f['filesize_dv'] = 'Mb';
				}
			}
		
		$sortby  = Tools::getValue("orderby").":". Tools::getValue("orderway");
		$lofdownload = new lofdownload();
		if($id_category == 1){
			$meta_title = $lofdownload->getParamValue('meta_title','download manager');
			$meta_description = $lofdownload->getParamValue('meta_description','download manager');
			$meta_keywords = $lofdownload->getParamValue('meta_keywords','download manager');
		}else{
			$meta_title = $parent->getMetaTitle();
			$meta_description = $parent->getMetaDescription();
			$meta_keywords = $parent->getMetaKeyWords();
		}
		$smarty->assign(array( 
			'pageTitle' => "Downloads",		
			'files'     => $files,
			'allfiles'  => $allfiles,
			'sortby'	=> $sortby,
			'id_category' 	  => $id_category,
			'helper'      	  => $helper,
			'parent'  	  	  => $parent,
			'categories'      => $categories,
			'countCategories' => count($categories),
			'meta_title'  		=> $meta_title,
			'meta_description'  => $meta_description,
			'meta_keywords'  => $meta_keywords,
			'HOTHIT_NUMBER' => _LOFDOWNLOAD_MODULE_HOT_HITS_NUM_,
			'NEW_DAY' => _LOFDOWNLOAD_MODULE_NEW_DAY_,
			'UPDATE_DAY' => _LOFDOWNLOAD_MODULE_UPDATE_DAY_,
			
			'pagination_link' => __PS_BASE_URI__.'modules/lofdownload/view.php?id_category='.$id_category,
			'page' => ((int)(Tools::getValue('p')) > 0 ? (int)(Tools::getValue('p')) : 1),
			'nbpagination' => ((int)(Tools::getValue('n') > 0) ? (int)(Tools::getValue('n')) : 30),
			'nArray' => array(30, 50, 100),
			'max_page' => floor(sizeof($allfiles) / ((int)(Tools::getValue('n') > 0) ? (int)(Tools::getValue('n')) : 30))
		
		));	
		include(dirname(__FILE__).'/../../header.php');
		
		echo $lofdownload->display(dirname(__FILE__).'/lofdownload.php', 'tmpl/frontend/lofdownload.tpl');
	
	}?>
<?php  
include(dirname(__FILE__).'/../../footer.php');
?>