<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */


define( "_HELPDESK" , 1 );
include_once(   dirname(dirname(__FILE__))."/libs/defined.php" );
class AdminHelpDeskTab  extends AdminTab {
	
	public function __construct() {
		$this->moduleBasePath = dirname(dirname(__FILE__));
		parent::__construct();
	}
	
	public function renderLayout( $layout, $args=array() ){
		extract( $args );
		ob_start();
		include $this->moduleBasePath.'/tmpl/'.$layout.'.php';
		$data = ob_get_clean();
		return $data;
	}
}