<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */


/**
 * Class LofDownloadHelper
 */
class LofDownloadRating  extends ObjectModel{
	
	public $rating_sum=0;
	public $rating_count;
	public $lastip ;
	public $id_lofdownload ;
	public $table="lofdownload_rating" ;
	protected 	$identifier = 'id_lofdownload';

	public function __construct($id_lofdownload = NULL, $id_lang = NULL)
	{
		parent::__construct($id_lofdownload, $id_lang);
	}
	public function getFields()
	{
		parent::validateFields();
		
		$fields['rating_sum'] = (int)($this->rating_sum);
		$fields['rating_count'] = (int)($this->rating_count);
	    $fields['id_lofdownload'] = (int)($this->id_lofdownload);
		$fields['lastip'] = pSQL($this->lastip);
 
		return $fields;
	}
	
		
}

?>