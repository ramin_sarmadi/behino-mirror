<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */


if(! class_exists("LofDownloadHelper") ) {
	/**
	 * Class LofDownloadHelper
	 */
	class LofDownloadHelper{
		
		var $previewExts = array( "jpg","png","gif", "swf" );
		
		public static function getInstance(){
			static $_instance; 
			if( !$_instance ){
				$_instance = new LofDownloadHelper();
			}
			
			return $_instance;
		}
		
		public static function renderAdminCategorieTree($trads, $selected_cat = array(), $input_name = 'categoryBox', $use_radio = false)
		{
			if (!$use_radio)
				$input_name = $input_name.'[]';
			global $currentIndex;
			
			$path = pathinfo($currentIndex);
            Context::getContext()->controller->addJS(__PS_BASE_URI__.'js/jquery/plugins/treeview-categories/jquery.treeview-categories.js');
            Context::getContext()->controller->addJS(__PS_BASE_URI__.'/js/jquery/plugins/treeview-categories/jquery.treeview-categories.async.js');
            Context::getContext()->controller->addJS(__PS_BASE_URI__.'/js/jquery/plugins/treeview-categories/jquery.treeview-categories.edit.js');
            Context::getContext()->controller->addJS(__PS_BASE_URI__.'/modules/'._LOFDOWNLOAD_MODULE_NAME_.'/assets/admin/admin-categories-tree.js');

            Context::getContext()->controller->addCSS(__PS_BASE_URI__.'/js/jquery/plugins/treeview-categories/jquery.treeview-categories.css');
			$html = '
				<script type="text/javascript">
				 var _LOF_BASE_ADMIN_URI_PATH_ = "'.$path['dirname'].'";
			</script>
			<script type="text/javascript">
				var inputName = "'.$input_name.'";
			';
			if (sizeof($selected_cat) > 0)
			{
				if (isset($selected_cat[0]))
					$html .= 'var selectedCat = "'.implode(',', $selected_cat).'"';
				else
					$html .= 'var selectedCat = "'.implode(',', array_keys($selected_cat)).'"';
			} else {
				$html .= 'var selectedCat = ""';
			}
			$html .= '
				var selectedLabel = \''.$trads['selected'].'\';
				var home = \''.$trads['Home'].'\';
				var use_radio = '.(int)$use_radio.';
			</script>
			';
			
			$html .= '
			<div style="background-color:#F4E6C9; width:99%;padding:5px 0 5px 5px;">
				<a href="#" id="collapse_all" >'.$trads['Collapse All'].'</a>
				 - <a href="#" id="expand_all" >'.$trads['Expand All'].'</a>
				'.(!$use_radio ? '
				 - <a href="#" id="check_all" >'.$trads['Check All'].'</a>
				 - <a href="#" id="uncheck_all" >'.$trads['Uncheck All'].'</a>
				' : '').'
			</div>
			';
			
			$home_is_selected = false;
			foreach($selected_cat AS $cat)
			{
				if (is_array($cat))
				{
					if  ($cat['id_category'] != 1)
						$html .= '<input type="hidden" name="'.$input_name.'" value="'.$cat['id_category'].'" >';
					else
						$home_is_selected = true;
				}
				else
				{
					if  ($cat != 1)
						$html .= '<input type="hidden" name="'.$input_name.'" value="'.$cat.'" >';
					else
						$home_is_selected = true;
				}
			}
			$html .= '
				<ul id="categories-treeview" class="filetree">
					<li id="1" class="hasChildren">
						<span class="folder"> <input type="'.(!$use_radio ? 'checkbox' : 'radio').'" name="'.$input_name.'" value="1" '.($home_is_selected ? 'checked' : '').' onclick="clickOnCategoryBox($(this));" /> '.$trads['Home'].'</span>
						<ul>
							<li><span class="placeholder">&nbsp;</span></li>	
					  </ul>
					</li>
				</ul>';
			return $html;
		}
		
		 /**
		* Get list of sub folder's name 
		*/
		public static function getFolderList( $path ) {
			$items = array();
			$handle = opendir($path);
			if (! $handle) {
				return $items;
			}
			while (false !== ($file = readdir($handle))) {
				if (is_dir($path . $file))
					$items[$file] = $file;
			}
			unset($items['.'], $items['..'], $items['.svn']);
			
			return $items;
		}
		
		
		static function deleteFolder($path) {
			if (is_dir($path)) {
				$path = rtrim($path, '/').'/';
				$handle = opendir($path);
				while(false !== ($file = readdir($handle))) {
					if($file != '.' and $file != '..' ) {
						$fullpath = $path.$file; 
						if(is_dir($fullpath)) self::deleteFolder($fullpath); else unlink($fullpath);
					}
				}
				closedir($handle);
				rmdir($path);
			}
		 }
		 /**
		* Get list of sub folder's name 
		*/
		public static function getFileList( $path ) {
			$items = array();
			$handle = opendir($path);
			if (! $handle) {
				return $items;
			}
			while (false !== ($file = readdir($handle))) {
				if (is_file($path . $file))
					$items[$file] = $file;
			}
			unset($items['.'], $items['..'], $items['.svn']);
			
			return $items;
		}
		
		public static function getMineType( $filetype ) {
		
			switch ($filetype) {
				case "ez":  $mime="application/andrew-inset"; break;
				case "hqx": $mime="application/mac-binhex40"; break;
				case "cpt": $mime="application/mac-compactpro"; break;
				case "doc": $mime="application/msword"; break;
				case "bin": $mime="application/octet-stream"; break;
				case "dms": $mime="application/octet-stream"; break;
				case "lha": $mime="application/octet-stream"; break;
				case "lzh": $mime="application/octet-stream"; break;
				case "exe": $mime="application/octet-stream"; break;
				case "class": $mime="application/octet-stream"; break;
				case "dll": $mime="application/octet-stream"; break;
				case "oda": $mime="application/oda"; break;
				case "pdf": $mime="application/pdf"; break;
				case "ai":  $mime="application/postscript"; break;
				case "eps": $mime="application/postscript"; break;
				case "ps":  $mime="application/postscript"; break;
				case "xls": $mime="application/vnd.ms-excel"; break;
				case "ppt": $mime="application/vnd.ms-powerpoint"; break;
				case "wbxml": $mime="application/vnd.wap.wbxml"; break;
				case "wmlc": $mime="application/vnd.wap.wmlc"; break;
				case "wmlsc": $mime="application/vnd.wap.wmlscriptc"; break;
				case "vcd": $mime="application/x-cdlink"; break;
				case "pgn": $mime="application/x-chess-pgn"; break;
				case "csh": $mime="application/x-csh"; break;
				case "dvi": $mime="application/x-dvi"; break;
				case "spl": $mime="application/x-futuresplash"; break;
				case "gtar": $mime="application/x-gtar"; break;
				case "hdf": $mime="application/x-hdf"; break;
				case "js":  $mime="application/x-javascript"; break;
				case "nc":  $mime="application/x-netcdf"; break;
				case "cdf": $mime="application/x-netcdf"; break;
				case "swf": $mime="application/x-shockwave-flash"; break;
				case "tar": $mime="application/x-tar"; break;
				case "tcl": $mime="application/x-tcl"; break;
				case "tex": $mime="application/x-tex"; break;
				case "texinfo": $mime="application/x-texinfo"; break;
				case "texi": $mime="application/x-texinfo"; break;
				case "t":   $mime="application/x-troff"; break;
				case "tr":  $mime="application/x-troff"; break;
				case "roff": $mime="application/x-troff"; break;
				case "man": $mime="application/x-troff-man"; break;
				case "me":  $mime="application/x-troff-me"; break;
				case "ms":  $mime="application/x-troff-ms"; break;
				case "ustar": $mime="application/x-ustar"; break;
				case "src": $mime="application/x-wais-source"; break;
				case "zip": $mime="application/x-zip"; break;
				case "au":  $mime="audio/basic"; break;
				case "snd": $mime="audio/basic"; break;
				case "mid": $mime="audio/midi"; break;
				case "midi": $mime="audio/midi"; break;
				case "kar": $mime="audio/midi"; break;
				case "mpga": $mime="audio/mpeg"; break;
				case "mp2": $mime="audio/mpeg"; break;
				case "mp3": $mime="audio/mpeg"; break;
				case "aif": $mime="audio/x-aiff"; break;
				case "aiff": $mime="audio/x-aiff"; break;
				case "aifc": $mime="audio/x-aiff"; break;
				case "m3u": $mime="audio/x-mpegurl"; break;
				case "ram": $mime="audio/x-pn-realaudio"; break;
				case "rm":  $mime="audio/x-pn-realaudio"; break;
				case "rpm": $mime="audio/x-pn-realaudio-plugin"; break;
				case "ra":  $mime="audio/x-realaudio"; break;
				case "wav": $mime="audio/x-wav"; break;
				case "pdb": $mime="chemical/x-pdb"; break;
				case "xyz": $mime="chemical/x-xyz"; break;
				case "bmp": $mime="image/bmp"; break;
				case "gif": $mime="image/gif"; break;
				case "ief": $mime="image/ief"; break;
				case "jpeg": $mime="image/jpeg"; break;
				case "jpg": $mime="image/jpeg"; break;
				case "jpe": $mime="image/jpeg"; break;
				case "png": $mime="image/png"; break;
				case "tiff": $mime="image/tiff"; break;
				case "tif": $mime="image/tiff"; break;
				case "wbmp": $mime="image/vnd.wap.wbmp"; break;
				case "ras": $mime="image/x-cmu-raster"; break;
				case "pnm": $mime="image/x-portable-anymap"; break;
				case "pbm": $mime="image/x-portable-bitmap"; break;
	
				case "pgm": $mime="image/x-portable-graymap"; break;
				case "ppm": $mime="image/x-portable-pixmap"; break;
				case "rgb": $mime="image/x-rgb"; break;
				case "xbm": $mime="image/x-xbitmap"; break;
				case "xpm": $mime="image/x-xpixmap"; break;
				case "xwd": $mime="image/x-xwindowdump"; break;
				case "msh": $mime="model/mesh"; break;
				case "mesh": $mime="model/mesh"; break;
				case "silo": $mime="model/mesh"; break;
				case "wrl": $mime="model/vrml"; break;
				case "vrml": $mime="model/vrml"; break;
				case "css": $mime="text/css"; break;
				case "asc": $mime="text/plain"; break;
				case "txt": $mime="text/plain"; break;
				case "gpg": $mime="text/plain"; break;
				case "rtx": $mime="text/richtext"; break;
				case "rtf": $mime="text/rtf"; break;
				case "wml": $mime="text/vnd.wap.wml"; break;
				case "wmls": $mime="text/vnd.wap.wmlscript"; break;
				case "etx": $mime="text/x-setext"; break;
				case "xsl": $mime="text/xml"; break;
				case "flv": $mime="video/x-flv"; break;
				case "mpeg": $mime="video/mpeg"; break;
				case "mpg": $mime="video/mpeg"; break;
				case "mpe": $mime="video/mpeg"; break;
				case "qt":  $mime="video/quicktime"; break;
				case "mov": $mime="video/quicktime"; break;
				case "mxu": $mime="video/vnd.mpegurl"; break;
				case "avi": $mime="video/x-msvideo"; break;
				case "movie": $mime="video/x-sgi-movie"; break;
				case "asf": $mime="video/x-ms-asf"; break;
				case "asx": $mime="video/x-ms-asf"; break;
				case "wm":  $mime="video/x-ms-wm"; break;
				case "wmv": $mime="video/x-ms-wmv"; break;
				case "wvx": $mime="video/x-ms-wvx"; break;
				case "ice": $mime="x-conference/x-cooltalk"; break;
				case "rar": $mime="application/x-rar"; break;
				default:    $mime="application/octet-stream"; break; 
			}
			return $mime;
		}
		
		public static function readFile($filename, $incpath = false, $amount = 0, $chunksize = 8192, $offset = 0)
		{
			// Initialise variables.
			$data = null;
			if ($amount && $chunksize > $amount) {
				$chunksize = $amount;
			}
	
			if (false === $fh = fopen($filename, 'rb', $incpath)) {
				die( "Error file" );
	
				return false;
			}
	
			clearstatcache();
	
			if ($offset) {
				fseek($fh, $offset);
			}
	
			if ($fsize = @ filesize($filename)) {
				if ($amount && $fsize > $amount) {
					$data = fread($fh, $amount);
				} else {
					$data = fread($fh, $fsize);
				}
			} else {
				$data = '';
				$x = 0;
				// While it's:
				// 1: Not the end of the file AND
				// 2a: No Max Amount set OR
				// 2b: The length of the data is less than the max amount we want
				while (!feof($fh) && (!$amount || strlen($data) < $amount)) {
					$data .= fread($fh, $chunksize);
				}
			}
			fclose($fh);
	
			return $data;
		}
		
		function getCategoryImage( $id, $size="small" ){  
			if( file_exists(_LOFDOWNLOAD_DIR_CATIMAGE_.$id."-".$size.".jpg") ){
				return _PS_IMG_._LOFDOWNLOAD_DIR_CATNAME_."/".$id."-".$size.".jpg";	 
			} 
			return __PS_BASE_URI__."modules/"._LOFDOWNLOAD_MODULE_NAME_."/assets/images/folder.png";	 
		}
		
		function getCategoryLink( $id ){
			return __PS_BASE_URI__.'index.php?process=list&fc=module&module=lofdownload&controller=default&id_category='.$id;
			//return __PS_BASE_URI__."modules/"._LOFDOWNLOAD_MODULE_NAME_."/view.php?id_category=".$id;
		}
		
		function getDownloadLink( $id ){ 
			//return __PS_BASE_URI__.'index.php?process=download&fc=module&module=lofdownload&controller=default&id_download='.$id.'&rand='.md5(time());
			return __PS_BASE_URI__."modules/"._LOFDOWNLOAD_MODULE_NAME_."/getfile.php?id_download=".$id."&rand=".md5(time());
		}
		
		function getViewDownloadLink( $id ){
			return __PS_BASE_URI__.'index.php?process=list&fc=module&module=lofdownload&controller=default&id_download='.$id;
			//return __PS_BASE_URI__."modules/"._LOFDOWNLOAD_MODULE_NAME_."/view.php?id_download=".$id ;
		}
		
		public static function getCategoryOrderBy( $orderby){
			$orderby = trim(strtolower($orderby));
			$values = array('name'=>'name',"created"=>"date_add","download" => "download" );
			if( array_key_exists($orderby,$values) ){
				return $values[$orderby];
			}
			return "name";
		}
		
		public static function getCategoryOrderway( $orderway ){
			$values = array("desc"=>"DESC","asc"=>"ASC");
			$orderway = trim(strtolower($orderway));
			if( array_key_exists($orderway, $values) ){
				return $values[$orderway];
			}
			return "DESC";
		}
		
		public function getIconURL( $filename, $filetype ){
			if(file_exists(_PS_MODULE_DIR_._LOFDOWNLOAD_MODULE_NAME_.'/assets/icons/'.$filetype.'.png')){
				return _MODULE_DIR_._LOFDOWNLOAD_MODULE_NAME_."/assets/icons/".$filetype.".png";
			}
			return '';
		}
		
		public function getCustomIcon(  $icon, $attrs="" ){
			if( $icon &&  file_exists(_LOFDOWNLOAD_DIR_UPLOAD_ICONS_).$icon ){
				return '<img src="'.__PS_BASE_URI__.'upload/icons/'.str_replace("\\","/",$icon).'"'.$attrs.'/>';
			}
			return ;
		}
		
	
		function renderRating( $id, $rating_sum, $rating_count, $voted=false,  $units='5'  ) { 
		
	 
				$rating_unitwidth = 10;
				$units = 5;
				$count=(int)$rating_sum; //how many votes total
	
				$current_rating=(int)$rating_count; //total number of rating added together and stored
				$rating_width = @number_format($current_rating/$count,2)*$rating_unitwidth;
				$rating1 = @number_format($current_rating/$count,1);
				$rating2 = @number_format($current_rating/$count,2);
		
				$tense = ($count==1) ? "vote" : "votes"; //plural form votes/vote
			  $rater ='';	  
			  $rater.='<div class="ratingblock">';
				
			  $width = 	@number_format($current_rating/$count,2)*100/$units;
	 
			  $rater.='<div id="unit_long'.$id.'">';
			  $rater.='  <ul id="unit_ul'.$id.'" class="unit-rating w-rating'.$units.'" >';
			  $rater.='     <li class="current-rating" style="width:'.$width.'%">Currently '.$rating2.'/'.$units.'</li>';
			  $ip = $_SERVER['SERVER_ADDR'];
 
			  for ($ncount = 1; $ncount <= $units; $ncount++) { // loop from 1 to the number of units
				   if(!$voted) { // if the user hasn't yet voted, draw the voting stars
					  $rater.='<li><a href="'.__PS_BASE_URI__.'modules/lofdownload/file-vote.php?j='.$ncount.'&amp;q='.$id.'&amp;t='.$ip.'&amp;c='.$units.'" title="'.$ncount.' out of '.$units.'" class="r'.$ncount.'-unit rater" rel="nofollow">'.$ncount.'</a></li>';
				   } else {
						$rater.='<li>'.$ncount.'</li>';   
				   }
			  }
			  $ncount=0; // resets the count
	
			  $rater.='  </ul>';
			  $rater.='  <span';
			  $rater.=' class="voted"';  
			  $rater.='>Rating: <strong> '.$rating1.'</strong>/'.$units.' ('.$count.' '.$tense.')';
			  $rater.='  </span>';
			  if( $voted ){
				  $rater.= '<span class="thanks">Thanks for voting!</span></span>';
			  }
			  $rater.='</div>';
			  $rater.='</div>';
			  return $rater;  
		}
		
		public function getPreviewTag( $preview_file, $title="" ){ 
		
			preg_match("#\w{3}$#",$preview_file,$match);
			if( isset($match[0]) && in_array($match[0],$this->previewExts)  ){
				$url =  __PS_BASE_URI__._LOFDOWNLOAD_DIR_PREVIEWS_FOLDER_."/".str_replace("\\","/",$preview_file);
				return '<a class="fancybox-preview" href="'.$url.'"><span class="preview-icon">&nbsp;</span></a>';
			} else {
				$url =  __PS_BASE_URI__."modules/"._LOFDOWNLOAD_MODULE_NAME_."/preview.php?file=".$preview_file;
				return '<a class="fancybox-preview-iframe" href="'.$url.'"><span class="preview-icon">&nbsp;</span></a>';
			}
		}
		function format_size($size) {
		  $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
		  if ($size == 0) { return('n/a'); } else {
		  return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]); }
		}
	}
}
?>