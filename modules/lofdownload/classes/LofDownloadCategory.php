<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */

/**
 * Class LofDownloadCategory
 */
class LofDownloadCategory extends ObjectModel
{
	public 		$id;

	/** @var integer category ID */
	public 		$id_lofdcategory;

	/** @var string Name */
	public 		$name;

	/** @var boolean Status for display */
	public 		$active = 1;

	/** @var  integer category position */
	public 		$position;

	/** @var string Description */
	public 		$description;

	/** @var integer Parent category ID */
	public 		$id_parent;

	/** @var integer Parents number */
	public 		$level_depth;

	/** @var integer Nested tree model "left" value */
	public 		$nleft;

	/** @var integer Nested tree model "right" value */
	public 		$nright;

	/** @var string string used in rewrited URL */
	public 		$link_rewrite;

	/** @var string Meta title */
	public 		$meta_title;

	/** @var string Meta keywords */
	public 		$meta_keywords;

	/** @var string Meta description */
	public 		$meta_description;

	/** @var string Object creation date */
	public 		$date_add;

	/** @var string Object last modification date */
	public 		$date_upd;

	public		$groupBox;

	protected static $_links = array();
	
	
	public static $definition = array(
		'table' => 'lofdcategory',
		'primary' => 'id_lofdcategory',
		'multilang' => true,
		'fields' => array(
			'nleft' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'nright' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'level_depth' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'active' => 			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			'id_parent' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'position' => 			array('type' => self::TYPE_INT),
			'date_add' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'date_upd' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),

			// Lang fields
			'name' => 				array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCatalogName', 'required' => true, 'size' => 64),
			'link_rewrite' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isLinkRewrite', 'required' => true, 'size' => 64),
			'description' => 		array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
			'meta_title' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
			'meta_description' => 	array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
			'meta_keywords' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
		),
	);
	
	/** @var string id_image is the category ID when an image exists and 'default' otherwise */
	public		$id_image = 'default';

	protected	$webserviceParameters = array(
		'objectsNodeName' => 'categories',
		'hidden_fields' => array('nleft', 'nright', 'groupBox'),
		'fields' => array(
			'id_parent' => array('xlink_resource'=> 'categories'),
			'level_depth' => array('setter' => false),
			'nb_products_recursive' => array('getter' => 'getWsNbProductsRecursive', 'setter' => false),
		),
		'associations' => array(
				'categories' => array('getter' => 'getChildrenWs', 'resource' => 'category', ),
				'products' => array('getter' => 'getProductsWs', 'resource' => 'product', ),
		),
	);

	public function __construct($id_lofdcategory = NULL, $id_lang = NULL)
	{
		parent::__construct($id_lofdcategory, $id_lang);
		$this->id_image = ($this->id AND file_exists(_LOFDOWNLOAD_DIR_CATIMAGE_.(int)($this->id).'.jpg')) ? (int)($this->id) : false;
		$this->image_dir = _LOFDOWNLOAD_DIR_CATIMAGE_;
	}

	/**
	  * Check then return multilingual fields for database interaction
	  *
	  * @return array Multilingual fields
	  */
	
	public	function add($autodate = true, $nullValues = false)
	{
		$this->position = self::getLastPosition((int)$this->id_parent);
		if (!isset($this->level_depth))
			$this->level_depth = $this->calcLevelDepth();
		$ret = parent::add($autodate);
		if (!isset($this->doNotRegenerateNTree) OR !$this->doNotRegenerateNTree)
			self::regenerateEntireNtree();
		$this->updateGroup($this->groupBox);
		return $ret;
	}

	/**
	 * update category positions in parent
	 *
	 * @param mixed $nullValues
	 * @return void
	 */
	public function update($nullValues = false)
	{
		// Update group selection
		
		 
		$this->updateGroup($this->groupBox);
		$this->level_depth = $this->calcLevelDepth();
		$this->cleanPositions((int)$this->id_parent);
		// If the parent category was changed, we don't want to have 2 categories with the same position
		if ($this->getDuplicatePosition())
			$this->position = self::getLastPosition((int)$this->id_parent);
		$ret = parent::update($nullValues);
		if (!isset($this->doNotRegenerateNTree) OR !$this->doNotRegenerateNTree)
		{
			self::regenerateEntireNtree();
			$this->recalculateLevelDepth($this->id_lofdcategory);
		}
		return $ret;
	}

	/**
	 * @see ObjectModel::toggleStatus()
	 */
	public function toggleStatus()
	{
		$result = parent::toggleStatus();
		return $result;
	}

	/**
	  * Recursive scan of subcategories
	  *
	  * @param integer $maxDepth Maximum depth of the tree (i.e. 2 => 3 levels depth)
 	  * @param integer $currentDepth specify the current depth in the tree (don't use it, only for rucursivity!)
	  * @param array $excludedIdsArray specify a list of ids to exclude of results
 	  * @param integer $idLang Specify the id of the language used
	  *
 	  * @return array Subcategories lite tree
	  */
	function recurseLiteCategTree($maxDepth = 3, $currentDepth = 0, $id_lang = NULL, $excludedIdsArray = NULL)
	{
		global $link;

		if (!(int)$id_lang)
			$id_lang = _USER_ID_LANG_;

		$children = array();
		if (($maxDepth == 0 OR $currentDepth < $maxDepth) AND $subcats = $this->getSubCategories((int)$id_lang, true) AND sizeof($subcats))
			foreach ($subcats AS &$subcat)
			{
				if (!$subcat['id_lofdcategory'])
					break;
				elseif (!is_array($excludedIdsArray) || !in_array($subcat['id_lofdcategory'], $excludedIdsArray))
				{
					$categ = new Category((int)$subcat['id_lofdcategory'], (int)$id_lang);
					$children[] = $categ->recurseLiteCategTree($maxDepth, $currentDepth + 1, (int)$id_lang, $excludedIdsArray);
				}
			}

		return array(
			'id' => (int)$this->id_lofdcategory,
			'link' => $link->getCategoryLink((int)$this->id, $this->link_rewrite),
			'name' => $this->name,
			'desc'=> $this->description,
			'children' => $children
		);
	}

	public static function recurseCategory($categories, $current, $id_lofdcategory = 1, $id_selected = 1)
	{
		echo '<option value="'.$id_lofdcategory.'"'.(($id_selected == $id_lofdcategory) ? ' selected="selected"' : '').'>'.
		str_repeat('&nbsp;', $current['infos']['level_depth'] * 5).stripslashes($current['infos']['name']).'</option>';
		if (isset($categories[$id_lofdcategory]))
			foreach (array_keys($categories[$id_lofdcategory]) AS $key)
				self::recurseCategory($categories, $categories[$id_lofdcategory][$key], $key, $id_selected);
	}


	/**
	  * Recursively add specified category childs to $toDelete array
	  *
	  * @param array &$toDelete Array reference where categories ID will be saved
	  * @param array $id_lofdcategory Parent category ID
	  */
	protected function recursiveDelete(&$toDelete, $id_lofdcategory)
	{
	 	if (!is_array($toDelete) OR !$id_lofdcategory)
	 		die(Tools::displayError());

		$result = Db::getInstance()->ExecuteS('
		SELECT `id_lofdcategory`
		FROM `'._DB_PREFIX_.'lofdcategory`
		WHERE `id_parent` = '.(int)($id_lofdcategory));
		foreach ($result AS $row)
		{
			$toDelete[] = (int)($row['id_lofdcategory']);
			$this->recursiveDelete($toDelete, (int)($row['id_lofdcategory']));
		}
	}

	public function delete()
	{
		if ((int)($this->id) === 0 OR (int)($this->id) === 1) return false;

		$this->clearCache();

		/* Get childs categories */
		$toDelete = array((int)($this->id));
		$this->recursiveDelete($toDelete, (int)($this->id));
		$toDelete = array_unique($toDelete);
		if(sizeof($toDelete))
			foreach($toDelete as $delete){
				$cate = new LofDownloadCategory($delete);
				$cate->deleteImage();
			}
		$this->deleteImage();
		/* Delete category and its child from database */
		$list = sizeof($toDelete) > 1 ?  implode(',', array_map('intval',$toDelete)) : (int)($this->id);
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'lofdcategory` WHERE `id_lofdcategory` IN ('.$list.')');
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'lofdcategory_lang` WHERE `id_lofdcategory` IN ('.$list.')');
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'lofdcategory_group` WHERE `id_lofdcategory` IN ('.$list.')');

		self::cleanPositions($this->id_parent);

		/* Delete products which were not in others categories */
		$result = Db::getInstance()->ExecuteS('
		SELECT `id_lofdownload`
		FROM `'._DB_PREFIX_.'lofdownload`
		WHERE `id_lofdcategory` IN ('.$list.')');
		foreach ($result as $p) {
			$product = new LofClassDownload((int)$p['id_lofdownload']);
			if (Validate::isLoadedObject($product))
				$product->delete();
		}

		/* Rebuild the nested tree */
		if (!isset($this->doNotRegenerateNTree) OR !$this->doNotRegenerateNTree)
			self::regenerateEntireNtree();

		return true;
	}
	/**
	 * Delete several categories from database
	 *
	 * return boolean Deletion result
	 */
	public function deleteSelection($categories)
	{
		$return = 1;
		foreach ($categories AS $id_lofdcategory)
		{
			$category = new LofDownloadCategory((int)($id_lofdcategory));
			$return &= $category->delete();
		}
		return $return;
	}

	/**
	  * Get the depth level for the category
	  *
	  * @return integer Depth level
	  */
	public function calcLevelDepth()
	{
		/* Root category */
		if (!$this->id_parent)
			return 0;

		$parentCategory = new LofDownloadCategory((int)($this->id_parent));
		if (!Validate::isLoadedObject($parentCategory))
			die('parent category does not exist');
		return $parentCategory->level_depth + 1;
	}

	/**
	  * Re-calculate the values of all branches of the nested tree
	  */
	public static function regenerateEntireNtree()
	{
		$categories = Db::getInstance()->ExecuteS('SELECT id_lofdcategory, id_parent FROM '._DB_PREFIX_.'lofdcategory ORDER BY id_lofdcategory ASC');
		$categoriesArray = array();
		foreach ($categories AS $category)
			$categoriesArray[(int)$category['id_parent']]['subcategories'][(int)$category['id_lofdcategory']] = 1;
		$n = 1;
		self::_subTree($categoriesArray, 1, $n);
	}

	protected static function _subTree(&$categories, $id_lofdcategory, &$n)
	{
		$left = (int)$n++;
		if (isset($categories[(int)$id_lofdcategory]['subcategories']))
			foreach (array_keys($categories[(int)$id_lofdcategory]['subcategories']) AS $id_subcategory)
				self::_subTree($categories, (int)$id_subcategory, $n);
		$right = (int)$n++;

		Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'lofdcategory SET nleft = '.(int)$left.', nright = '.(int)$right.' WHERE id_lofdcategory = '.(int)$id_lofdcategory.' LIMIT 1');
	}

	/**
	  * Updates level_depth for all children of the given id_lofdcategory
	  *
	  * @param integer $id_lofdcategory parent category
	  */
	public function recalculateLevelDepth($id_lofdcategory)
	{
		/* Gets all children */
		$categories = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT id_lofdcategory, id_parent, level_depth
			FROM '._DB_PREFIX_.'lofdcategory
			WHERE id_parent = '.(int)$id_lofdcategory);
		/* Gets level_depth */
		$level = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT level_depth
			FROM '._DB_PREFIX_.'lofdcategory
			WHERE id_lofdcategory = '.(int)$id_lofdcategory);
		/* Updates level_depth for all children */
		foreach ($categories as $sub_category)
		{
			Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('
				UPDATE '._DB_PREFIX_.'lofdcategory
				SET level_depth = '.(int)($level['level_depth'] + 1).'
				WHERE id_lofdcategory = '.(int)$sub_category['id_lofdcategory']);
			/* Recursive call */
			$this->recalculateLevelDepth($sub_category['id_lofdcategory']);
		}
	}

	/**
	  * Return available categories
	  *
	  * @param integer $id_lang Language ID
	  * @param boolean $active return only active categories
	  * @return array Categories
	  */
	public static function getCategories($id_lang = false, $active = true, $order = true, $sql_filter = '', $sql_sort = '',$sql_limit = '')
	{
	 	if (!Validate::isBool($active))
	 		die(Tools::displayError());

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT *
		FROM `'._DB_PREFIX_.'lofdcategory` c
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_lofdcategory` = cl.`id_lofdcategory`
		WHERE 1 '.$sql_filter.' '.($id_lang ? 'AND `id_lang` = '.(int)($id_lang) : '').'
		'.($active ? 'AND `active` = 1' : '').'
		'.(!$id_lang ? 'GROUP BY c.id_lofdcategory' : '').'
		'.($sql_sort != '' ? $sql_sort : 'ORDER BY c.`level_depth` ASC, c.`position` ASC').'
		'.($sql_limit != '' ? $sql_limit : '')
		);

		if (!$order)
			return $result;

		$categories = array();
		foreach ($result AS $row)
			$categories[$row['id_parent']][$row['id_lofdcategory']]['infos'] = $row;

		return $categories;
	}

	public static function getSimpleCategories($id_lang)
	{
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT c.`id_lofdcategory`, cl.`name`
		FROM `'._DB_PREFIX_.'lofdcategory` c
		LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory`)
		WHERE cl.`id_lang` = '.(int)($id_lang).'
		ORDER BY c.`position`');
	}

	/**
	  * Return current category childs
	  *
	  * @param integer $id_lang Language ID
	  * @param boolean $active return only active categories
	  * @return array Categories
	  */
	public function getSubCategories($id_lang, $active = true)
	{
	 	if (!Validate::isBool($active))
	 		die(Tools::displayError());

		$groups = FrontController::getCurrentCustomerGroups();
		$sqlGroups = (count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1');

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT c.*, cl.id_lang, cl.name, cl.description, cl.link_rewrite, cl.meta_title, cl.meta_keywords, cl.meta_description
		FROM `'._DB_PREFIX_.'lofdcategory` c
		LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory` AND `id_lang` = '.(int)($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'lofdcategory_group` cg ON (cg.`id_lofdcategory` = c.`id_lofdcategory`)
		WHERE `id_parent` = '.(int)($this->id).'
		'.($active ? 'AND `active` = 1' : '').'
		AND cg.`id_group` '.$sqlGroups.'
		GROUP BY c.`id_lofdcategory`
		ORDER BY `level_depth` ASC, c.`position` ASC');

		foreach ($result AS &$row)
		{
			$row['id_image'] = (file_exists(_LOFDOWNLOAD_DIR_CATIMAGE_.$row['id_lofdcategory'].'.jpg')) ? (int)($row['id_lofdcategory']) : Language::getIsoById($id_lang).'-default';
			$row['legend'] = 'no picture';
		}
		return $result;
	}


	
	public static function countDownloadByCategoryId( $category_id, $id_lang ){
		$query = 'SELECT  count(d.id_lofdownload) as total
					FROM  `'._DB_PREFIX_.'lofdownload` d 
					LEFT JOIN `'._DB_PREFIX_.'lofdcategory` c ON (d.`id_lofdcategory`=c.`id_lofdcategory` )';
		$query .= ' WHERE d.id_lofdcategory='.(int)$category_id. " and d.active=1 " ;			
		$data = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS( $query );	
		return $data[0]['total'];
	}
	
   public  function getFilesByCategoryId( $category_id, $id_lang, $checkAccess=true, $orderby, $orderway,$n = false, $p = 1 ){
	
	    global $cookie;
		if (!$checkAccess OR !$this->checkAccess($cookie->id_customer) )
			return false;
		   
		if ($p < 1) $p = 1;
		
		$query = 'SELECT ROUND( dr.rating_sum / dr.rating_count ) AS rating, dr.rating_count as rating_count, dr.rating_sum,
					cl.id_lang, d.*, cl.name as category_name , dl.name,dl.description,dl.information, DATEDIFF( now(), d.date_add) as newdays,DATEDIFF( now(), d.date_upd) as updatedays
					FROM  `'._DB_PREFIX_.'lofdownload` d 
					LEFT JOIN `'._DB_PREFIX_.'lofdownload_rating` dr ON (d.`id_lofdownload`=dr.`id_lofdownload` ) 	
					LEFT JOIN `'._DB_PREFIX_.'lofdownload_lang` dl ON (d.`id_lofdownload`=dl.`id_lofdownload` AND dl.`id_lang`='.(int)$id_lang.') 
					LEFT JOIN `'._DB_PREFIX_.'lofdcategory` c ON (d.`id_lofdcategory`=c.`id_lofdcategory` )
					LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory` AND cl.`id_lang` = '.(int)($id_lang).')	';
		$query .= ' WHERE d.id_lofdcategory='.$category_id. " and d.active=1 " ;	
		$query .= " ORDER BY ".$orderby." ".$orderway;
		$query .= ($n ? ' LIMIT '.(((int)($p) - 1) * (int)($n)).','.(int)($n) : '');
		return  Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS( $query );
 
 	}
	/**
	  * Hide category prefix used for position
	  *
	  * @param string $name Category name
	  * @return string Name without position
	  */
	public static function hideCategoryPosition($name)
	{
		Tools::displayAsDeprecated();
		return preg_replace('/^[0-9]+\./', '', $name);
	}

	/**
	  * Return main categories
	  *
	  * @param integer $id_lang Language ID
	  * @param boolean $active return only active categories
	  * @return array categories
	  */
	public static function getHomeCategories($id_lang, $active = true)
	{
		return self::getChildren(1, $id_lang, $active);
	}

	public static function getRootCategory($id_lang = NULL)
	{
		return new LofDownloadCategory (1, is_null($id_lang) ? (int)_USER_ID_LANG_ : (int)($id_lang));
	}

	/**
	 *
	 * @param int $id_parent
	 * @param int $id_lang
	 * @param bool $active
	 * @return array
	 */
	public static function getChildren($id_parent, $id_lang, $active = true)
	{
		if (!Validate::isBool($active))
	 		die(Tools::displayError());
		
		$query = '
		SELECT c.`id_lofdcategory`, cl.`name`, cl.`description`, cl.`link_rewrite`
		FROM `'._DB_PREFIX_.'lofdcategory` c
		LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON c.`id_lofdcategory` = cl.`id_lofdcategory`
		WHERE `id_lang` = '.(int)($id_lang).'
		AND c.`id_parent` = '.(int)($id_parent).'
		'.($active ? 'AND `active` = 1' : '').'
		ORDER BY `position` ASC' ;
	
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS( $query );
	}

	/**
	 * This method allow to return children categories with the number of sub children selected for a product
	 *
	 * @param int $id_parent
	 * @param int $id_product
	 * @param int $id_lang
	 * @return array
	 */
	public static function getChildrenWithNbSelectedSubCat($id_parent, $selectedCat,  $id_lang)
	{
		$selectedCat = explode(',', str_replace(' ', '', $selectedCat));
		$query = '
		SELECT c.`id_lofdcategory` as id_category, c.`level_depth`, cl.`name`, IF((
			SELECT COUNT(*)
			FROM `'._DB_PREFIX_.'lofdcategory` c2
			WHERE c2.`id_parent` = c.`id_lofdcategory`
		) > 0, 1, 0) AS has_children, '.($selectedCat ? '(
			SELECT count(c3.`id_lofdcategory`)
			FROM `'._DB_PREFIX_.'lofdcategory` c3
			WHERE c3.`nleft` > c.`nleft`
			AND c3.`nright` < c.`nright`
			AND c3.`id_lofdcategory`  IN ('.implode(',', array_map('intval', $selectedCat)).')
		)' : '0').' AS nbSelectedSubCat
		FROM `'._DB_PREFIX_.'lofdcategory` c
		LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON c.`id_lofdcategory` = cl.`id_lofdcategory`
		WHERE `id_lang` = '.(int)($id_lang).'
		AND c.`id_parent` = '.(int)($id_parent).'
		ORDER BY `position` ASC';
	
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS( $query ) ;
	}

	/**
	  * Copy products from a category to another
	  *
	  * @param integer $id_old Source category ID
	  * @param boolean $id_new Destination category ID
	  * @return boolean Duplication result
	  */
	public static function duplicateProductCategories($id_old, $id_new)
	{
		$result = Db::getInstance()->ExecuteS('
		SELECT `id_lofdcategory`
		FROM `'._DB_PREFIX_.'category_product`
		WHERE `id_product` = '.(int)($id_old));

		$row = array();
		if ($result)
			foreach ($result AS $i)
				$row[] = '('.implode(', ', array((int)($id_new), $i['id_lofdcategory'], '(SELECT tmp.max + 1 FROM (SELECT MAX(cp.`position`) AS max FROM `'._DB_PREFIX_.'category_product` cp WHERE cp.`id_lofdcategory`='.(int)($i['id_lofdcategory']).') AS tmp)')).')';

		$flag = Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'category_product` (`id_product`, `id_lofdcategory`, `position`) VALUES '.implode(',', $row));
		return $flag;
	}

	/**
	  * Check if category can be moved in another one.
		* The category cannot be moved in a child category.
	  *
		* @param integer $id_lofdcategory current category
	  * @param integer $id_parent Parent candidate
	  * @return boolean Parent validity
	  */
	public static function checkBeforeMove($id_lofdcategory, $id_parent)
	{
		if ($id_lofdcategory == $id_parent) return false;
		if ($id_parent == 1) return true;
		$i = (int)($id_parent);

		while (42)
		{
			$result = Db::getInstance()->getRow('SELECT `id_parent` FROM `'._DB_PREFIX_.'lofdcategory` WHERE `id_lofdcategory` = '.(int)($i));
			if (!isset($result['id_parent'])) return false;
			if ($result['id_parent'] == $id_lofdcategory) return false;
			if ($result['id_parent'] == 1) return true;
			$i = $result['id_parent'];
		}
	}

	public static function getLinkRewrite($id_lofdcategory, $id_lang)
	{
		if (!Validate::isUnsignedId($id_lofdcategory) OR !Validate::isUnsignedId($id_lang))
			return false;

		if (isset(self::$_links[$id_lofdcategory.'-'.$id_lang]))
			return self::$_links[$id_lofdcategory.'-'.$id_lang];

		$result = Db::getInstance()->getRow('
		SELECT cl.`link_rewrite`
		FROM `'._DB_PREFIX_.'lofdcategory` c
		LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON c.`id_lofdcategory` = cl.`id_lofdcategory`
		WHERE `id_lang` = '.(int)($id_lang).'
		AND c.`id_lofdcategory` = '.(int)($id_lofdcategory));
		self::$_links[$id_lofdcategory.'-'.$id_lang] = $result['link_rewrite'];
		return $result['link_rewrite'];
	}

	public function getLink()
	{
		global $link;
		return $link->getCategoryLink($this->id, $this->link_rewrite);
	}

	public function getName($id_lang = NULL){
		if (!$id_lang){
			global $cookie;
			if (isset($this->name[$cookie->id_lang]))
				$id_lang = $cookie->id_lang;
			else
				$id_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
		}
		return isset($this->name[$id_lang]) ? $this->name[$id_lang] : '';
	}
	
	public function getMetaDescription($id_lang = NULL){
		if (!$id_lang){
			global $cookie;
			if (isset($this->meta_description[$cookie->id_lang]))
				$id_lang = $cookie->id_lang;
			else
				$id_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
		}
		return isset($this->meta_description[$id_lang]) ? $this->meta_description[$id_lang] : '';
	}
	public function getMetaKeywords($id_lang = NULL){
		if (!$id_lang){
			global $cookie;
			if (isset($this->meta_keywords[$cookie->id_lang]))
				$id_lang = $cookie->id_lang;
			else
				$id_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
		}
		return isset($this->meta_keywords[$id_lang]) ? $this->meta_keywords[$id_lang] : '';
	}
	
	public function getMetaTitle($id_lang = NULL){
		if (!$id_lang){
			global $cookie;
			if (isset($this->meta_title[$cookie->id_lang]))
				$id_lang = $cookie->id_lang;
			else
				$id_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
		}
		return isset($this->meta_title[$id_lang]) && !empty($this->meta_title[$id_lang]) ? $this->meta_title[$id_lang] :  $this->getName( $id_lang );
	}
	/**
	  * Light back office search for categories
	  *
	  * @param integer $id_lang Language ID
	  * @param string $query Searched string
	  * @param boolean $unrestricted allows search without lang and includes first category and exact match
	  * @return array Corresponding categories
	  */
	public static function searchByName($id_lang, $query, $unrestricted = false)
	{
		if ($unrestricted === true)
			return Db::getInstance()->getRow('
			SELECT c.*, cl.*
			FROM `'._DB_PREFIX_.'lofdcategory` c
			LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory`)
			WHERE `name` LIKE \''.pSQL($query).'\'');
		else
			return Db::getInstance()->ExecuteS('
			SELECT c.*, cl.*
			FROM `'._DB_PREFIX_.'lofdcategory` c
			LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory` AND `id_lang` = '.(int)($id_lang).')
			WHERE `name` LIKE \'%'.pSQL($query).'%\' AND c.`id_lofdcategory` != 1');
	}

	/**
	  * Retrieve category by name and parent category id
	  *
	  * @param integer $id_lang Language ID
	  * @param string  $category_name Searched category name
	  * @param integer $id_parent_category parent category ID
	  * @return array Corresponding category
	  */
	public static function searchByNameAndParentCategoryId($id_lang, $category_name, $id_parent_category)
	{
		return Db::getInstance()->getRow('
		SELECT c.*, cl.*
	    FROM `'._DB_PREFIX_.'lofdcategory` c
	    LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory` AND `id_lang` = '.(int)($id_lang).')
	    WHERE `name`  LIKE \''.pSQL($category_name).'\'
		AND c.`id_lofdcategory` != 1
		AND c.`id_parent` = '.(int)($id_parent_category));
	}

	/**
	  * Get Each parent category of this category until the root category
	  *
	  * @param integer $id_lang Language ID
	  * @return array Corresponding categories
	  */
	public function getParentsCategories($idLang = null)
	{
		//get idLang
		$idLang = is_null($idLang) ? _USER_ID_LANG_ : (int)($idLang);

		$categories = null;
		$idCurrent = (int)($this->id);
		while (true)
		{
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
				SELECT c.*, cl.*
				FROM `'._DB_PREFIX_.'lofdcategory` c
				LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory` AND `id_lang` = '.(int)($idLang).')
				WHERE c.`id_lofdcategory` = '.(int)$idCurrent.' AND c.`id_parent` != 0
			');

			$categories[] = $result[0];
			if (!$result OR $result[0]['id_parent'] == 1)
				return $categories;
			$idCurrent = $result[0]['id_parent'];
		}
	}
	/**
	* Specify if a category already in base
	*
	* @param $id_lofdcategory Category id
	* @return boolean
	*/
	public static function categoryExists($id_lofdcategory)
	{
		$row = Db::getInstance()->getRow('
		SELECT `id_lofdcategory`
		FROM '._DB_PREFIX_.'lofdcategory c
		WHERE c.`id_lofdcategory` = '.(int)($id_lofdcategory));

		return isset($row['id_lofdcategory']);
	}


	public function cleanGroups()
	{
		Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'lofdcategory_group` WHERE `id_lofdcategory` = '.(int)($this->id));
	}

	public function addGroups($groups)
	{
		foreach ($groups as $group)
		{
			$row = array('id_lofdcategory' => (int)($this->id), 'id_group' => (int)($group));
			
			Db::getInstance()->AutoExecute(_DB_PREFIX_.'lofdcategory_group', $row, 'INSERT');
		}
	}

	public function getGroups()
	{
		$groups = array();
		$result = Db::getInstance()->ExecuteS('
		SELECT cg.`id_group`
		FROM '._DB_PREFIX_.'lofdcategory_group cg
		WHERE cg.`id_lofdcategory` = '.(int)($this->id));
		foreach ($result as $group)
			$groups[] = $group['id_group'];
		return $groups;
	}

	/**
	 * checkAccess return true if id_customer is in a group allowed to see this category.
	 *
	 * @param mixed $id_customer
	 * @access public
	 * @return boolean true if access allowed for customer $id_customer
	 */
	public function checkAccess($id_customer)
	{
		if (!$id_customer)
		{
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT ctg.`id_group`
			FROM '._DB_PREFIX_.'lofdcategory_group ctg
			WHERE ctg.`id_lofdcategory` = '.(int)($this->id).' AND ctg.`id_group` = 1');
		} else {
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT ctg.`id_group`
			FROM '._DB_PREFIX_.'lofdcategory_group ctg
			INNER JOIN '._DB_PREFIX_.'customer_group cg on (cg.`id_group` = ctg.`id_group` AND cg.`id_customer` = '.(int)($id_customer).')
			WHERE ctg.`id_lofdcategory` = '.(int)($this->id));
		}
		
		 
		if ($result AND isset($result['id_group']) AND $result['id_group'])
			return true;
		return false;
	}

	public function updateGroup($list)
	{
		$this->cleanGroups();
		if ($list AND sizeof($list))
			$this->addGroups($list);
		else
			$this->addGroups(array(1));
	}

	public static function setNewGroupForHome($id_group)
	{
		if (!(int)($id_group))
			return false;
		return Db::getInstance()->Execute('
		INSERT INTO `'._DB_PREFIX_.'lofdcategory_group`
		VALUES (1, '.(int)($id_group).')
		');
	}

	public function updatePosition($way, $position)
	{
		if (!$res = Db::getInstance()->ExecuteS('
			SELECT cp.`id_lofdcategory`, cp.`position`, cp.`id_parent`
			FROM `'._DB_PREFIX_.'lofdcategory` cp
			WHERE cp.`id_parent` = '.(int)$this->id_parent.'
			ORDER BY cp.`position` ASC'
		))
			return false;

		foreach ($res AS $category)
			if ((int)($category['id_lofdcategory']) == (int)($this->id))
				$movedCategory = $category;

		if (!isset($movedCategory) || !isset($position))
			return false;
		// < and > statements rather than BETWEEN operator
		// since BETWEEN is treated differently according to databases
		$result = (Db::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'lofdcategory`
			SET `position`= `position` '.($way ? '- 1' : '+ 1').'
			WHERE `position`
			'.($way
				? '> '.(int)($movedCategory['position']).' AND `position` <= '.(int)($position)
				: '< '.(int)($movedCategory['position']).' AND `position` >= '.(int)($position)).'
			AND `id_parent`='.(int)($movedCategory['id_parent']))
		AND Db::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'lofdcategory`
			SET `position` = '.(int)($position).'
			WHERE `id_parent` = '.(int)($movedCategory['id_parent']).'
			AND `id_lofdcategory`='.(int)($movedCategory['id_lofdcategory'])));
		return $result;
	}

	/**
	 * cleanPositions keep order of category in $id_lofdcategory_parent,
	 * but remove duplicate position. Should not be used if positions
	 * are clean at the beginning !
	 *
	 * @param mixed $id_lofdcategory_parent
	 * @return boolean true if succeed
	 */
	public static function cleanPositions($id_lofdcategory_parent)
	{
		$return = true;

		$result = Db::getInstance()->ExecuteS('
		SELECT `id_lofdcategory`
		FROM `'._DB_PREFIX_.'lofdcategory`
		WHERE `id_parent` = '.(int)($id_lofdcategory_parent).'
		ORDER BY `position`');
		$sizeof = sizeof($result);
		for ($i = 0; $i < $sizeof; $i++){
				$sql = '
				UPDATE `'._DB_PREFIX_.'lofdcategory`
				SET `position` = '.(int)($i).'
				WHERE `id_parent` = '.(int)($id_lofdcategory_parent).'
				AND `id_lofdcategory` = '.(int)($result[$i]['id_lofdcategory']);
				$return &= Db::getInstance()->Execute($sql);
			}
		return $return;
	}

	/** this function return the number of category + 1 having $id_lofdcategory_parent as parent.
	 * 
	 * @todo rename that function to make it understandable (getNewLastPosition for example)
	 * @param int $id_lofdcategory_parent the parent category
	 * @return int
	 */
	public static function getLastPosition($id_lofdcategory_parent)
	{
		return (Db::getInstance()->getValue('SELECT MAX(position)+1 FROM `'._DB_PREFIX_.'lofdcategory` WHERE `id_parent` = '.(int)($id_lofdcategory_parent)));
	}

    public static function getUrlRewriteInformations($id_lofdcategory)
	{
		return Db::getInstance()->ExecuteS('
		SELECT l.`id_lang`, c.`link_rewrite`
		FROM `'._DB_PREFIX_.'lofdcategory_lang` AS c
		LEFT JOIN  `'._DB_PREFIX_.'lang` AS l ON c.`id_lang` = l.`id_lang`
		WHERE c.`id_lofdcategory` = '.(int)$id_lofdcategory.'
		AND l.`active` = 1'
		);

	}

	public function getChildrenWs()
	{
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT c.`id_lofdcategory` as id
		FROM `'._DB_PREFIX_.'lofdcategory` c
		WHERE c.`id_parent` = '.(int)($this->id).'
		AND `active` = 1
		ORDER BY `position` ASC');
		return $result;
	}

	public function getProductsWs()
	{
		return ;
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT cp.`id_product` as id
		FROM `'._DB_PREFIX_.'category_product` cp
		WHERE cp.`id_lofdcategory` = '.(int)($this->id).'
		ORDER BY `position` ASC');
		return $result;
	}

	/**
	 * Search for another category with the same parent and the same position
	 *
	 * @return array first category found
	 */
	public function getDuplicatePosition()
	{
		return Db::getInstance()->getRow('
		SELECT c.`id_lofdcategory` as id
		FROM `'._DB_PREFIX_.'lofdcategory` c
		WHERE c.`id_parent` = '.(int)($this->id_parent).'
		AND `position` = '.(int)($this->position).'
		AND c.`id_lofdcategory` != '.(int)($this->id));
	}

	public function getWsNbProductsRecursive()
	{
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT count(distinct(id_product)) as nb_product_recursive FROM  `'._DB_PREFIX_.'category_product`
		WHERE id_lofdcategory IN (SELECT id_lofdcategory
		FROM `'._DB_PREFIX_.'lofdcategory`
		WHERE nleft > '.(int)$this->nleft.
		' AND nright < '.(int)$this->nright.' AND active = 1 UNION SELECT '.(int)$this->id.')');
		if (!$result)
			return -1;
		return $result[0]['nb_product_recursive'];
	}

	/**
	 *
	 * @param Array $ids_category
	 * @param int $id_lang
	 * @return Array
	 */
	public static function getCategoryInformations($ids_category, $id_lang = null)
	{
		if ($id_lang === null)
		{
			global $cookie;
			$id_lang = $cookie->id_lang;
		}

		if (!is_array($ids_category) || !sizeof($ids_category))
			return;

		$categories = array();
		$results = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT c.`id_lofdcategory`, cl.`name`, cl.`link_rewrite`, cl.`id_lang`
			FROM `'._DB_PREFIX_.'lofdcategory` c
			LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory`)
			WHERE cl.`id_lang` = '.(int)$id_lang.'
			AND c.`id_lofdcategory` IN ('.implode(',', array_map('intval', $ids_category)).')
		');

		foreach($results as $category)
			$categories[$category['id_lofdcategory']] = $category;

		return $categories;
	}
	
}

