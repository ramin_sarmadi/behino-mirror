<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */


if (!defined('_CAN_LOAD_FILES_'))
	exit;
if(!class_exists('LofClassDownload')) {	
	/**
	 * Class LofClassDownload
	 */	
	class LofClassDownload extends ObjectModel {
		public $id;
		public $id_lofdownload;
		public $id_lofdcategory;
		public $video_mine_type;
		public $image_mine_type;
		public $file_path;
		public $date_add;
		public $date_upd;
		public $active;
		public $name;
		public $description;
		public $information;
		public $ip_address;
		public $download;
		public $version;
		public $filename;
		public $filesize;
		public $icon_file;
		public $preview_file;
		public $file_type;
		
		
		public static $definition = array(
			'table' => 'lofdownload',
			'primary' => 'id_lofdownload',
			'multilang' => true,
			'fields' => array(
				/* Classic fields */
				'id_lofdcategory' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
				'icon_file' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'preview_file' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'file_path' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
				'date_add' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
				'date_upd' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
				'active' => 			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
				'ip_address' => 		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'download' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
				'version' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'filename' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'file_type' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'filesize' => 			array('type' => self::TYPE_STRING),
				

				/* Lang fields */
				'name' => 						array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCatalogName', 'required' => true, 'size' => 128),
				'description' => 				array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
				'information' => 			array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString')
			)
		);
	
		public function __construct($id = NULL, $id_lang = NULL)
		{
			parent::__construct($id, $id_lang);
			
		}
		
		
		public static function getDownloadInfo( $id, $id_lang ){
			$query = 'SELECT ROUND( dr.rating_sum / dr.rating_count ) AS rating, dr.rating_count as rating_count, dr.rating_sum,
						cl.id_lang, d.*, cl.name as category_name , dl.name,dl.description,dl.information, DATEDIFF( now(), d.date_add) as newdays,DATEDIFF( now(), d.date_upd) as updatedays
						FROM  `'._DB_PREFIX_.'lofdownload` d 
						LEFT JOIN `'._DB_PREFIX_.'lofdownload_rating` dr ON (d.`id_lofdownload`=dr.`id_lofdownload` ) 	
						LEFT JOIN `'._DB_PREFIX_.'lofdownload_lang` dl ON (d.`id_lofdownload`=dl.`id_lofdownload` AND dl.`id_lang`='.(int)$id_lang.') 
						LEFT JOIN `'._DB_PREFIX_.'lofdcategory` c ON (d.`id_lofdcategory`=c.`id_lofdcategory` )
						LEFT JOIN `'._DB_PREFIX_.'lofdcategory_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory` AND cl.`id_lang` = '.(int)($id_lang).')	';
			$query .= ' WHERE d.id_lofdownload='.$id. " and d.active=1 " ;
			
			$data =  Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS( $query );
			return isset($data[0])?$data[0]:""; 
		}
		
		
		public function add($autodate = true, $nullValues = false){
			if( !empty($this->file_path) ){
				$tmp =  explode("/", str_replace("\\","/",$this->file_path) );
				$this->filename = $tmp[count($tmp)-1];
				
				$path = _LOFDOWNLOAD_DIR_UPLOAD_.$this->file_path;
				if( is_file($path) && file_exists($path) ){
					$this->filesize = pSQL( filesize($path) );
				}
				$tmp = explode(".",$this->filename);
				if( count($tmp) > 0 ){
					$this->file_type = strtolower( $tmp[count($tmp)-1] );
				}
			}
			//echo "<pre>".print_r($this,1); die;
			return parent::add($autodate);
		}

		public function update($nullValues = false){
	 
			return parent::update($nullValues);
		}
		
		public function delete(){
			$id_lofdownload = $this->id;
			$return = 1;
			if(parent::delete()){
				$return &= Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'lofdownload_rating` WHERE `id_lofdownload` = '.(int)($id_lofdownload));
				$return &= Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'lofproducts_download` WHERE `id_lofdownload` = '.(int)($id_lofdownload));
			}
			return $return;
		}
		/* delete image*/
		public function deleteIMG($id,$thumb=null){
			$fileimg = _PS_IMG_DIR_.'lofcustomerupload/img/'.$id.($thumb ? '-'.$thumb : '').'.'.$this->image_mine_type;
			if(is_file($fileimg))
				unlink($fileimg);
			return true;
		}
		static function  isImageExists( $file ){
			return file_exists( _PS_IMG_DIR_.'lofcustomerupload/img/'.$file);
		}
		/* delete Video*/
		public function deleteVideo($id){
			$filevideo = _PS_IMG_DIR_.'lofcustomerupload/video/'.$id.'.'.$this->video_mine_type;
			if(is_file($filevideo))
				unlink($filevideo);
			return true;
		}
		
		public function deleteSelection($fileuploads,$typethumbs = array()){
			$return = 1;
			foreach ($fileuploads AS $id_lofdownload){
				$customerupload = new LofClassDownload((int)($id_lofdownload));
				$id = $customerupload->id;
				$return &= $customerupload->delete();
				foreach($typethumbs as $name => $size)
					$customerupload->deleteIMG($id,$name);
				$customerupload->deleteIMG($id);
				$customerupload->deleteVideo($id);
			}
			return $return;
		}
		
		public static function getProductDownloads(   $id_product=false,$id_lang, $orderby=false, $start=0, $limit=false ){
		 
			$sql = '
				SELECT d.*, dl.name, dl.description FROM `'._DB_PREFIX_.'lofdownload` d 
				LEFT JOIN `'._DB_PREFIX_.'lofdownload_lang` dl ON (d.`id_lofdownload` = dl.`id_lofdownload` AND dl.`id_lang` = '.(int)($id_lang).') 
				LEFT JOIN '._DB_PREFIX_.'lofproducts_download pd ON d.id_lofdownload=pd.id_lofdownload
				
				WHERE active = 1 '.
				($id_product ? ' AND pd.id_product = '.(int)($id_product) : '').
			 
				($limit ? ' LIMIT '.(int)($start).','.(int)($limit) : '') ;
			
			$results = Db::getInstance()->ExecuteS($sql);

			return $results;
		}
		
		public static function getImageLink($id_lofdownload,$type='jpg',$thumb=null){
		 
			if( self::isImageExists( $id_lofdownload.($thumb ? '-'.$thumb : '').'.'.$type ) ){
				$file = __PS_BASE_URI__.'img/lofcustomerupload/img/'.$id_lofdownload.($thumb ? '-'.$thumb : '').'.'.$type.'?rand='.rand(10,100);
				return '<img src="'.$file.'"/>';
			}
			return ;
		}
		
		public static function getVideoLink($id_lofdownload,$type='flv'){
			return __PS_BASE_URI__.'img/lofcustomerupload/video/'.$id_lofdownload.'.'.$type;
		}
		
		/**
		 * Delete product accessories
		 *
		 * @return mixed Deletion result
		 */
		public function deleteAccessories()
		{
			return Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'lofproducts_download` WHERE `id_lofdownload` = '.(int)($this->id));
		}
		
		/**
		 * Link accessories with product
		 *
		 * @param array $accessories_id Accessories ids
		 */
		public function changeAccessories($accessories_id)
		{
			foreach ($accessories_id as $id_product)
				Db::getInstance()->AutoExecute(_DB_PREFIX_.'lofproducts_download', array('id_lofdownload' => (int)($this->id), 'id_product' => (int)($id_product)), 'INSERT');
		}
		
		/**
		 * Get product accessories (only names)
		 *
		 * @param integer $id_lang Language id
		 * @param integer $id_product Product id
		 * @return array Product accessories
		 */
		public static function getAccessoriesLight( $id_lang, $id_lofdownload ){
			
			return Db::getInstance()->ExecuteS('
			SELECT p.`id_product`, p.`reference`, pl.`name`
			FROM `'._DB_PREFIX_.'lofproducts_download` pd
			LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.`id_product`= pd.`id_product`)
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
			WHERE `id_lofdownload` = '.(int)($id_lofdownload));
		}
	}
}