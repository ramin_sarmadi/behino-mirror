<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */

if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}
require_once( dirname(__FILE__)."/libs/defined.php" );
require_once( dirname(__FILE__)."/classes/LofDownloadHelper.php" );
/**
 * lofdownload Class
 */	
class lofdownload extends Module
{
	/**
	* @var LofParams $_params;
	*
	* @access private;
	*/
	private $_params = '';	
	/**
	* @var array $_postErrors;
	*
	* @access private;
	*/
	private $_postErrors = array();		
	/**
	* @var array $_languages;
	*
	* @access private;
	*/
	public $_languages = NULL;
	public $_defaultFormLanguage = NULL;
	/**
	* @var string $site_url;
	*
	* @access public;
	*/
	public $site_url;
	
	public $languages;

	private $_divLangName;
	/**
    * Constructor 
    */
	function __construct(){
		$this->name = 'lofdownload';
		parent::__construct();			
		$this->tab = 'LandOfCoder';
		$this->version = '1.1';
		$this->author = 'LandOfCoder';
		$this->module_key = "61cc3727d5113f5a2b08e154a30a6c71";
		$this->displayName = $this->l('Lof Download Module');
		$this->description = $this->l('Lof Download Module');
		$this->Languages();
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' )){
			if( !defined("LOF_LOAD_LIB_PARAMS_DOWNLOAD") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' );
				define("LOF_LOAD_LIB_PARAMS_DOWNLOAD",true);
			}
		}
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofClassDownload.php' ) ){
			if( !defined("LOF_LOAD_CLASSES_CUSTOMER_UPLOAD") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofClassDownload.php' );
				define("LOF_LOAD_CLASSES_CUSTOMER_UPLOAD",true);
			}
		}
		$this->_divLangName = 'mod_title-meta_title-meta_des-meta_key';
		$this->_initLanguages();
		$this->_params = new LofDownloadParams( $this->name );
		$this->site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
	}
   /**
    * process installing 
    */
	function install(){  
		if (!parent::install() || !$this->installTradDone() || !$this->registerHook('productfooter')|| !$this->registerHook('header') || !$this->installAdminRights()   )
			return false;

		return true;
	}
	public function hookUpdateproduct(){
	//	die;
	//	echo '<pre>'.print_r( $_POST,1 ); die;
		return true;
	}
	public function uninstall(){
		if ( !parent::uninstall() )
			return false;
			
 		$this->uninstallModuleTab('AdminLofManageDownloads');
		if($this->_params->get('remove_db',0)){
			if(!$this->uninstallTradDone())
				echo "<br/>".$this->l("Can't Delete Database");
		}
		
		return true;
	}
	
	private function installAdminRights() {
		$languages = Language::getLanguages(false);
		foreach ($languages as $language) {
			$arrName[$language['id_lang']] = 'Manage Downloads';
		}
		$this->installModuleTab('AdminLofManageDownloads', $arrName, 9);
 
		return true;
	}
	
	private function installModuleTab($tabClass, $tabName, $idTabParent){
		@copy(_PS_MODULE_DIR_.$this->name.'/logo.gif', _PS_IMG_DIR_.'t/'.$tabClass.'.gif');	
		$tab = new Tab();
		$tab->name = $tabName;
		$tab->class_name = $tabClass;
		$tab->module = $this->name;
		$tab->id_parent = $idTabParent;
		if(!$tab->save())
			return false;
		return true;
	}
	
	private function uninstallModuleTab($tabClass){
		$idTab = Tab::getIdFromClassName($tabClass);
		if($idTab != 0){
			$tab = new Tab($idTab);
			$tab->delete();
			return true;
		}
		return false;
	}
	
	/**
	 *
	 */
	private function installTradDone() { 
		require_once( dirname(__FILE__)."/install/sql.tables.php" );
	 	$error=true;
		if( isset($query) && !empty($query) ){  
			if(  !($data=Db::getInstance()->ExecuteS( "SHOW TABLES LIKE '"._DB_PREFIX_."lofdownload'" )) ){		
				$query = str_replace( "_DB_PREFIX_", _DB_PREFIX_, $query );
				$db_data_settings = preg_split("/;\s*[\r\n]+/",$query);
				foreach ($db_data_settings as $query){
					$query = trim($query);
					if (!empty($query))	{
			 
						if (!Db::getInstance()->Execute($query)){
							 $error = false;
						}
					}
				}// endforeach;
			} 
		} else { $error = false; }
		return $error;
	}
	
   /**
	* DROP Table
	*/
	private function uninstallTradDone() {
		require_once( dirname(__FILE__)."/install/sql.uninstall.php" );
		if( isset($query) && !empty($query) ){ 
			$query = str_replace( "_DB_PREFIX_", _DB_PREFIX_,$query );
			$db_data_settings = preg_split("/;\s*[\r\n]+/",$query);

			foreach ($db_data_settings as $query)
			{
				$query = trim($query);
				if (!empty($query))
				{
					if (!Db::getInstance()->Execute($query))
					{
						 return false;
					}
				}
			}
		}
		return true;
	}
	
	/**
	 *
	 */
	public function Languages(){
		global $cookie;
		$allowEmployeeFormLang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		if ($allowEmployeeFormLang && !$cookie->employee_form_lang)
			$cookie->employee_form_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$useLangFromCookie = false;
		$this->_languages = Language::getLanguages(false);
		if ($allowEmployeeFormLang)
			foreach ($this->_languages AS $lang)
				if ($cookie->employee_form_lang == $lang['id_lang'])
					$useLangFromCookie = true;
		if (!$useLangFromCookie)
			$this->_defaultFormLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		else
			$this->_defaultFormLanguage = (int)($cookie->employee_form_lang);
	}
	 /**
	  *
	  */
	private function _initLanguages(){
		$this->languages = array(
			'edit' => $this->l('edit'),
			'choose_language' => $this->l('choose language'),
			'delete' => $this->l('delete')
		);
	}
	
	function hookHeader($params){ 
		global $cookie;
		if(!$this->active)return;
		if(!isset($_SESSION)) session_start();
 		
		$this->context->controller->addCSS(_MODULE_DIR_.$this->name.'/assets/style.css', 'all');
		$this->context->controller->addJS(_MODULE_DIR_.$this->name.'/assets/script.js');

	}
	
	function hookLeftColumn($params){
		return $this->processHook($params,'left');
	}
	
	function hookRightColumn($params){
		return $this->processHook($params,'right');
	}
	
	function hookHome($params){
		return $this->processHook($params,'home');
	}
	
	function processHook($params,$pos='left'){
		global $smarty,$cookie,$link;
		$params = $this->_params;
		
	}
	
	function hookCustomerAccount($params){
		return $this->display(__FILE__, 'tmpl/frontend/my-account.tpl');
	}
	
	function hookMyAccountBlock($params){		
		return $this->display(__FILE__, 'tmpl/frontend/my-account.tpl');
	}
	
	public function hookProductTab($params){
		global $smarty,$cookie;
		$params = $this->_params;
		$id_product = Tools::getValue('id_product');
		$module_title = $params->get('mod_title_'.$cookie->id_lang,'Customer upload');
 
		$smarty->assign(array(
			'nbUpload' => "",
			'module_title' => $module_title
		));
		return ($this->display(__FILE__, '/tmpl/frontend/hooktab.tpl'));
	}
	
	public function hookProductfooter(){
		
		global $smarty,$cookie,$link;
		$params = $this->_params;
		
		$id_product = Tools::getValue('id_product');
	
		$orderby = ' ORDER BY id_lofdownload DESC ';
		$files = LofClassDownload::getProductDownloads($id_product, $cookie->id_lang,$orderby );
		$helper = LofDownloadHelper::getInstance();
 
		/* Smarty */
		$smarty->assign(array(
		 	'files' => $files,
			'helper' => $helper
		));
		return ($this->display(__FILE__, '/tmpl/frontend/hooktabcontent.tpl'));
		
	}
	
   /**
    * Render processing form && process saving data.
    */	
	public function getContent(){
		$html = "";
		if (Tools::isSubmit('submit')){
			$this->_postValidation();
			if (!sizeof($this->_postErrors)){
		        $definedConfigs = array(
		          /* general config */
				  'dir_upload'    	 => 'lofdownloads',
				  'catimg_height'    => '45',
				  'catimg_width'   	 => '45',
				  'remove_db'		 => '',
				  'file_exts'		=>'',
				  'meta_title'		=>'',
				  'meta_des'		=>'',
				  'meta_key'		=>'',
				  'sizelimit'		=>'',
				  'file_pre'		=>'',
				  'file_icon'		=>'',
		        );
				foreach($this->_languages as $language){
					$definedConfigs['mod_title_'.$language['id_lang']] = '';
					$definedConfigs['last_title_'.$language['id_lang']] = '';
				}
		        foreach( $definedConfigs as $config => $key ){
		      		Configuration::updateValue($this->name.'_'.$config, Tools::getValue($config), true);
		    	}
		        $html .= '<div class="conf confirm">'.$this->l('Settings updated').'</div>';
			}else{
				foreach ($this->_postErrors AS $err){
					$html .= '<div class="alert error">'.$err.'</div>';
				}
			}
			$this->_params = new LofDownloadParams( $this->name );	
		}
		return $html.$this->_getFormConfig();
	}
	 
	/**
	 * Render Configuration From for user making settings.
	 *
	 * @return context
	 */
	private function _getFormConfig(){
		$html = '';
		
	    ob_start();
	    include_once dirname(__FILE__).'/config/lofdownload.php'; 
	    $html .= ob_get_contents();
	    ob_end_clean(); 
		return $html;
	}
	/**
     * Process vadiation before saving data 
     */
	private function _postValidation(){
		if (!Validate::isCleanHtml(Tools::getValue('limit')) || !Validate::isInt(Tools::getValue('limit')))
			$this->_postErrors[] = $this->l('The Limit you entered was not allowed, sorry');
	}
   /**
    * Get value of parameter following to its name.
    * 
	* @return string is value of parameter.
	*/
	public function getParamValue($name, $default = ''){
		return $this->_params->get( $name, $default );	
	}
 
	
 
} 