﻿$(document).ready(function() {
	
	$('.select-option').each(function() {		
		name = $(this).attr("name");		
		elemens = $.find('input[name="'+name+'"]');		
		for(i =0; i< elemens.length; i++){			
			if(!$(elemens[i]).attr("checked")){				
				$('.' + name + '-' +$(elemens[i]).val()).hide();
			}					
			
			$(elemens[i]).click(function() {
				subNameb   = $(this).attr("name");
				subElemens = $.find('input[name="'+subNameb+'"]');
				for(j =0; j< subElemens.length; j++){					
					if(!$(subElemens[j]).attr("checked")){						
						$('.' + $(subElemens[j]).attr("name") + '-' +$(subElemens[j]).val()).hide();
					}else{						
						$('.' + $(subElemens[j]).attr("name") + '-' +$(subElemens[j]).val()).show();
					}
				}				
			});
		}			
	});	
		
	
	$('.select-group').each(function() {
		currentValue = $(this).val();
		name = $(this).attr("name");		
		$(this).find("option").each(function(index,Element) {		
		    if($(Element).val() == currentValue){		    	
		    	$('.' + name + '-' + $(Element).val()).show();
		    }else{		    	
		    	$('.' + name + '-' + $(Element).val()).hide();
		    }
		});
	});	
	
	$('.select-group').change(function() {	   
		currentValue = $(this).val();
		name = $(this).attr("name");        		
		$(this).find("option").each(function(index,Element) {		
		    if($(Element).val() == currentValue){		          
		    	$('.' + name + '-' + $(Element).val()).show();
		    }else{
		    	$('.' + name + '-' + $(Element).val()).hide();
		    }
		});		
	});
});
function lofSelectAll(obj){	
	$(obj).find("option").each(function(index,Element) {
		$(Element).attr("selected","selected");
	});	
}

function changeLanguage1(field, fieldsString, id_language_new, iso_code)
{
	var fields = fieldsString.split('-');
	for (var i = 0; i < fields.length; ++i)
	{
		getE(fields[i] + '_' + id_language).style.display = 'none';
		getE(fields[i] + '_' + id_language_new).style.display = 'block';
		getE('language_current_' + fields[i]).src = '../img/l/' + id_language_new + '.jpg';
	}
	getE('languages_' + field).style.display = 'none';
	id_language = id_language_new;
}
function lofDelete( id, id_reward ) {
	$.ajax({
		type: "POST",
		async: true,
		cache: false,
		dataType: 'json',
		url: ajaxUrl,
		data: "id_reward="+id_reward+"&lofajax=1&task=deleteReward",
		success: function(jsonData){
			if (jsonData.hasError){
				var errors = '';
				for(error in jsonData.errors)
					//IE6 bug fix
					if(error != 'indexOf')
						errors += jsonData.errors[error] + "\n";
			}else{
				var divid = document.getElementById( id );
				divid.parentNode.removeChild (divid);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {alert("TECHNICAL ERROR: unable to save update quantity \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);}
	});
}

function changeCustomer(){
	var id_customer = document.getElementById("id_customer").value;
	$.ajax({
		type: "POST",
		url: ajaxUrl,
		data: "id_customer="+id_customer+"&lofajax=1&task=getcustomer",
		success: function(msg){
			$("#cus-detail").html(msg);
		}
	});
}
function lofpaging(id_customer,page,limit){
	$.ajax({
		type: "POST",
		url: ajaxUrl,
		data: "id_customer="+id_customer+"&page="+page+"&limit="+limit+"&lofajax=1&task=getPaging",
		success: function(msg){
			$("#container-table").html(msg);
		}
	});
}

function delDownloadProducts( id )
{
	var div = getE('divAccessories');
	var input = getE('inputAccessories');
	var name = getE('nameAccessories');

	// Cut hidden fields in array
	var inputCut = input.value.split('-');
	var nameCut = name.value.split('¤');
 
	if (inputCut.length != nameCut.length)
		return alert('Bad size');

	// Reset all hidden fields
	input.value = '';
	name.value = '';
	div.innerHTML = '';
	for (i in inputCut)
	{
		// If empty, error, next
		if (!inputCut[i] || !nameCut[i])
			continue ;

		// Add to hidden fields no selected products OR add to select field selected product
		if (inputCut[i] != id)
		{
			input.value += inputCut[i] + '-';
			name.value += nameCut[i] + '¤';
			div.innerHTML += nameCut[i] + ' <span onclick="delAccessory(' + inputCut[i] + ');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />';
		}
		else
			$('#selectAccessories').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
	}

	$('#product_autocomplete_input').setOptions({
		extraParams: {excludeIds : getAccessorieIds() }
	});
}
 
function _addProductDownloads(event, data, formatted)
{
	if (data == null)
		return false;
	var productId = data[1];
	var productName = data[0];

	var $divAccessories = $('#divAccessories');
	var $inputAccessories = $('#inputAccessories');
	var $nameAccessories = $('#nameAccessories');

	/* delete product from select + add product line to the div, input_name, input_ids elements */
	$divAccessories.html($divAccessories.html() + productName + ' <span onclick="_delProductDownloads(' + productId + ');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />');
	$nameAccessories.val($nameAccessories.val() + productName + '¤');
	$inputAccessories.val($inputAccessories.val() + productId + '-');
	$('#product_autocomplete_input').val('');
	$('#product_autocomplete_input').setOptions({
		extraParams: {excludeIds : getAccessorieIds()}
	});
}

function _delProductDownloads(id)
{
	var div = getE('divAccessories');
	var input = getE('inputAccessories');
	var name = getE('nameAccessories');

	// Cut hidden fields in array
	var inputCut = input.value.split('-');
	var nameCut = name.value.split('¤');
 	
	if (inputCut.length != nameCut.length)
		return alert('Bad size');

	// Reset all hidden fields
	input.value = '';
	name.value = '';
	div.innerHTML = '';
	for (i in inputCut)
	{
		// If empty, error, next
		if (!inputCut[i] || !nameCut[i])
			continue ;

		// Add to hidden fields no selected products OR add to select field selected product
		if (inputCut[i] != id)
		{
			input.value += inputCut[i] + '-';
			name.value += nameCut[i] + '¤';
			div.innerHTML += nameCut[i] + ' <span onclick="_delProductDownloads(' + inputCut[i] + ');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />';
		}
		else
			$('#selectAccessories').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
	}

	$('#product_autocomplete_input').setOptions({
		extraParams: {excludeIds : getAccessorieIds()}
	});
}