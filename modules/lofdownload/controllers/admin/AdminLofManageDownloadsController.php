<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
//include_once(PS_ADMIN_DIR . '/../../classes/AdminTab.php');
include_once( dirname(__FILE__)."/../../classes/admintab.php" );
include_once( dirname(__FILE__)."/../../classes/LofClassDownload.php" );
include_once( dirname(__FILE__)."/../../classes/LofDownloadCategory.php" );
include_once( dirname(__FILE__)."/../../classes/LofDownloadHelper.php" );


include_once(dirname(__FILE__).'/AdminLofCategoriesController.php');
include_once(dirname(__FILE__).'/AdminLofDownloadsController.php');
include_once(dirname(__FILE__).'/AdminLDController.php');

class AdminLofManageDownloadsController extends AdminLDController {
	public $imagesThumbs = array();
	public $allowVideoMineType = array();
	public $allowImageMineType = array();
	
	private $adminDownloads ;
	private $adminCategories;
	private static $_category = NULL;
	
	public function __construct(){
		global $cookie;
			
		$id_lofdcategory = abs((int)(Tools::getValue('id_lofdcategory')));
		$id_lofdcategory = abs((int)(Tools::getValue('id_lofdcategory')));
		if (!$id_lofdcategory) $id_lofdcategory = 1;
		self::$_category = new LofDownloadCategory($id_lofdcategory);
		 
		if (!Validate::isLoadedObject(self::$_category))
			die('Category cannot be loaded');


	 	$this->table = "lofdownload";
		
		$this->adminDownloads = new AdminLofDownloadsController();
		$this->adminCategories = new AdminLofCategoriesController();
		
 
		return parent::__construct();
	}
	
	public function viewAccess($disable = false){
		$result = parent::viewAccess($disable);
		$this->adminCategories->tabAccess = $this->tabAccess;
		$this->adminDownloads->tabAccess = $this->tabAccess;
		
		return $result;
	}
	
	public function postProcess()
	{
		$this->viewAccess();
		if (Tools::isSubmit('submitDellofdownload')
			|| Tools::isSubmit('submitAddlofdownload')
			|| Tools::isSubmit('submitAddlofdownloadAndOther')
			|| Tools::isSubmit('submitAddlofdownloadAndStay')
			|| Tools::isSubmit('deletelofdownload')
			|| Tools::isSubmit('viewlofdownload')
			|| (Tools::isSubmit('statuslofdownload') && Tools::isSubmit('id_lofdownload'))
			|| (Tools::isSubmit('way') && Tools::isSubmit('id_lofdownload')) && (Tools::isSubmit('position'))){
			$this->adminDownloads->postProcess();
		}else if (Tools::isSubmit('submitDellofdcategory')
			|| Tools::isSubmit('submitAddlofdcategoryAndBackToParent')
			|| Tools::isSubmit('submitAddlofdcategory') || isset($_GET['position'])
			|| Tools::isSubmit('deletelofdcategory') || (Tools::isSubmit('deleteImage') && Tools::getValue('id_lofdcategory'))
			|| (Tools::isSubmit('statuslofdcategory') && Tools::isSubmit('id_lofdcategory'))){
			$this->adminCategories->postProcess();
		}
		if (((Tools::isSubmit('submitAddlofdcategory') || Tools::isSubmit('submitAddlofdcategoryAndStay')) && count($this->adminCategories->errors))
			|| Tools::isSubmit('updatelofdcategory')
			|| Tools::isSubmit('addlofdcategory'))
			$this->display = 'edit_lofdcategory';
		else if (((Tools::isSubmit('submitAddlofdownload') || Tools::isSubmit('submitAddlofdownloadAndStay')) && count($this->adminDownloads->errors))
			|| Tools::isSubmit('updatelofdownload')
			|| Tools::isSubmit('addlofdownload')){
			$this->display = 'edit_lofdownload';
		}else
		{
			$this->display = 'list';
			$this->id_lofdcategory = (int)Tools::getValue('id_lofdcategory');
		}

		if (isset($this->adminDownloads->errors))
			$this->errors = array_merge($this->errors, $this->adminDownloads->errors);

		if (isset($this->adminCategories->errors))
			$this->errors = array_merge($this->errors, $this->adminCategories->errors);

		parent::postProcess();
	}

	public function initContent()
	{
		$currentIndex = AdminController::$currentIndex;
		$this->viewAccess();
		$this->adminCategories->token = $this->token;
		$this->adminDownloads->token = $this->token;
		
		if ($this->display == 'edit_lofdcategory'){
			$this->content .= $this->adminCategories->renderForm();
		}else if ($this->display == 'edit_lofdownload'){
			$this->content .= $this->adminDownloads->renderForm();
		}else
		{
			$id_lofdcategory = (int)(Tools::getValue('id_lofdcategory'));
			if (!$id_lofdcategory)
				$id_lofdcategory = 1;
			$catalog_tabs = array('lofdcategory', 'lofdownload');
			// Cleaning links
			$catBarIndex = $currentIndex;
			foreach ($catalog_tabs AS $tab)
				if (Tools::getValue($tab.'Orderby') && Tools::getValue($tab.'Orderway')) 
					$catBarIndex = preg_replace('/&'.$tab.'Orderby=([a-z _]*)&'.$tab.'Orderway=([a-z]*)/i', '', $currentIndex);
			
			$this->content .= '<div class="cat_bar"><span style="color: #3C8534;">'.$this->l('Current category').' :</span>&nbsp;&nbsp;&nbsp;'.$this->getPath($catBarIndex, $id_lofdcategory).'</div>';
			$this->content .= '<script type="text/javascript">
					jQuery(document).ready(function(){
						$("#showquickuploadForm,.showquickuploadForm span").click(function(){
							$("#quickuploadForm").toggle(500);
						});
					});
				</script>';
		 
			$this->content .= '<div class="clearfix showquickuploadForm"><h2 id="showquickuploadForm" style="float:left">'.$this->l('Download Tools').'</h2> <span style="margin-left:12px; line-height:6.6em;font-size:10px; color:#888">'.$this->l( "Click the text to see tools" ).'</span></div>';
			$this->content .= '<div id="quickuploadForm">'.$this->showQuickUploadForm( )."</div><br/><br/>";	
				
				
			$this->content .= $this->adminCategories->renderList();
			$this->adminDownloads->id_lofdcategory = $id_lofdcategory;
			$this->content .= $this->adminDownloads->renderList();
			
			
			$this->context->smarty->assign(array(
				'breadcrumb' => $this->getPath($catBarIndex, $id_lofdcategory),
			));
			
		}
		
		$this->context->smarty->assign(array(
			'content' => $this->content,
			'title' => array('Category')
		));
	}
	
	/* function getPath */
	function getPath($urlBase, $id_lofdcategory, $path = '', $highlight = '')
	{
		global $cookie;
			
		$lofdcategory = Db::getInstance()->getRow('
		SELECT id_lofdcategory, level_depth, nleft, nright
		FROM '._DB_PREFIX_.'lofdcategory
		WHERE id_lofdcategory = '.(int)$id_lofdcategory);

		if (isset($lofdcategory['id_lofdcategory']))
		{
			$categories = Db::getInstance()->ExecuteS('
			SELECT c.id_lofdcategory, cl.name, cl.link_rewrite
			FROM '._DB_PREFIX_.'lofdcategory c
			LEFT JOIN '._DB_PREFIX_.'lofdcategory_lang cl ON (cl.id_lofdcategory = c.id_lofdcategory)
			WHERE c.nleft <= '.(int)$lofdcategory['nleft'].' AND c.nright >= '.(int)$lofdcategory['nright'].' AND cl.id_lang = '.(int)($cookie->id_lang).'
			ORDER BY c.level_depth ASC
			LIMIT '.(int)($lofdcategory['level_depth'] + 1));
			
			$fullPath = '';
			$n = 1;
			$nCategories = (int)sizeof($categories);
			foreach ($categories AS $lofdcategory)
			{
				$edit = '<a href="'.$urlBase.'&id_lofdcategory='.(int)$lofdcategory['id_lofdcategory'].'&'.($lofdcategory['id_lofdcategory'] == 1 ? 'viewcategory' : 'addcategory').'&token='.Tools::getAdminToken('AdminLofManageDownloads'.(int)(Tab::getIdFromClassName('AdminLofManageDownloads')).(int)($cookie->id_employee)).'" title="'.($lofdcategory['id_lofdcategory'] == 1 ? 'Home' : 'Modify').'"><img src="../img/admin/'.($lofdcategory['id_lofdcategory'] == 1 ? 'home' : 'edit').'.gif" alt="" /></a> ';
				$fullPath .= $edit.
				($n < $nCategories ? '<a href="'.$urlBase.'&id_lofdcategory='.(int)$lofdcategory['id_lofdcategory'].'&viewcategory&token='.Tools::getAdminToken('AdminLofManageDownloads'.(int)(Tab::getIdFromClassName('AdminLofManageDownloads')).(int)($cookie->id_employee)).'" title="'.htmlentities($lofdcategory['name'], ENT_NOQUOTES, 'UTF-8').'">' : '').
				(!empty($highlight) ? str_ireplace($highlight, '<span class="highlight">'.htmlentities($highlight, ENT_NOQUOTES, 'UTF-8').'</span>', $lofdcategory['name']) : $lofdcategory['name']).
				($n < $nCategories ? '</a>' : '').
				(($n++ != $nCategories OR !empty($path)) ? ' > ' : '');
			}
				
			return $fullPath.$path;
		}
		return '';
	}

	public function displayErrors()
	{
		parent::displayErrors();
		$this->adminCategories->displayErrors();
		$this->adminDownloads->displayErrors();
	}


	
	public static function getCurrentCategory(){
		return self::$_category;	
	}
	
	/**
	* function upload Image
	*/
	public function uploadIMG($fileimg){
		$maxImageSize = (!Configuration::get('lofcustomerupload_max_size') ? 2000048 : Configuration::get('lofcustomerupload_max_size'));
		if ($error = $this->checkImage($fileimg, $maxImageSize)){
			$this->_errors[] = $error;
			return false;
		}
		$allowImageMineType = $this->allowImageMineType;
		$image_mine_type = $fileimg['type'];
		$imgMimeType = false;
		$image_type = '';
		foreach($allowImageMineType as $typekey => $value)
			if (strstr($image_mine_type, $typekey)){
				$imgMimeType = true;
				$image_type = $value;
			}
		if(!$tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS') OR !move_uploaded_file($fileimg['tmp_name'], $tmpName)){
			$this->_errors[] = 'Image can not upload.';
			return false;
		}
		
		$results = array('tmpName'=>$tmpName,'image_type'=>$image_type);
		return $results;
	}
	
	public function checkImage($file, $maxFileSize)
    {
    	if ($file['size'] > $maxFileSize)
    		return Tools::displayError('Image is too large').' ('.($file['size'] / 1000).Tools::displayError('KB').'). '.Tools::displayError('Maximum allowed:').' '.($maxFileSize / 1000).Tools::displayError('KB');
    	if (!isPicture($file))
    		return Tools::displayError('Image format not recognized, allowed formats are: .gif, .jpg, .png');
    	if ($file['error'])
    		return Tools::displayError('Error while uploading image; please change your server\'s settings.').'('.Tools::displayError('Error code: ').$file['error'].')';
    	return false;
    }
	/**
	* function create thumbs
	*
	*/
	/* $tmpName,$folderIMG,$image_type,$id*/
	public function resizeImg($args = array()){
		if(!$args)
			return false;
		$fileimg['tmp_name'] = $args['tmpName'];
		// Copy new image
		if (!imageResize($args['tmpName'], $args['folderIMG'].$args['id'].'.'.$args['image_type'], NULL, NULL, $args['image_type'])){
			return false;
		}
		/* create thumbs */
		$imagesThumbs = $this->imagesThumbs;
		foreach ($imagesThumbs AS $name => $size)
			imageResize($args['folderIMG'].$args['id'].'.'.$args['image_type'], $args['folderIMG'].$args['id'].'-'.stripslashes($name).'.'.$args['image_type'], (int)($size['width']), (int)($size['height']));
		unlink($args['tmpName']);
		return true;
	}
	/* $fileVideo,$folderVIDEO,$id,$video_type,$tmp_name */
	public function uploadVideo($args = array()){
		if(!$args)
			return false;
		if(!move_uploaded_file($args['filevideo']['tmp_name'],$args['folderVIDEO'].$args['id'].'.'.$args['video_type'])){
			return false;
		}
		return true;
	}
	
	public function displayForm($isMainTab = true){
		global $currentIndex, $cookie;
		parent::displayForm();
		if (!($obj = $this->loadObject(true)))
			return;
		$categories = array();
		$children = $this->getIndexedCategories();
        $this->treeCategory( 0, $categories , $children );
		$site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		echo $this->renderLayout("admin/formcustomerupload", array("obj" => $obj, "currentIndex"=>$currentIndex, "identifier"=>$this->identifier,'categories'=>$categories,'site_url'=>$site_url) );
	}
	
	public function viewlofdownload($token = NULL){
		$id_lofdownload = Tools::getValue('id_lofdownload');
		if(!Validate::isLoadedObject($obj = new LofClassDownload($id_lofdownload)))
			return '';
			
				
		echo $this->renderLayout("admin/viewcustomerupload", array("obj" => $obj) );
	}
	
	public function getIndexedCategories(){		
		global $cookie;
		$id_lang = intval($cookie->id_lang);
		
		$allCat = Db::getInstance()->ExecuteS('
		SELECT c.*, cl.id_lang, cl.name, cl.description, cl.link_rewrite, cl.meta_title, cl.meta_keywords, cl.meta_description
		FROM `'._DB_PREFIX_.'category` c
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_lofdcategory` = cl.`id_lofdcategory` AND `id_lang` = '.intval($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'category_group` cg ON (cg.`id_lofdcategory` = c.`id_lofdcategory`)
		WHERE `active` = 1 
		GROUP BY c.`id_lofdcategory`
		ORDER BY `name` DESC');
		$children = array();
		if ( $allCat ){
			foreach ( $allCat as $v ){
				$pt 	= $v["id_parent"];
				$list 	= @$children[$pt] ? $children[$pt] : array();
				array_push( $list, $v );
				$children[$pt] = $list;
			}
			return $children;
		}
		return array();
	}
	
	public static function treeCategory($id, &$list, $children, $tree=""){
		if (isset($children[$id])){
			if($id != 0){
				$tree = $tree." - ";
			}
			foreach ($children[$id] as $v){
				$v["tree"] = $tree;
				$list[] = $v;
				self::treeCategory( $v["id_lofdcategory"], $list, $children,$tree);
			}
		}
	}
	
	public function getProducts($id_lofdcategory){
		global $cookie;
		$results = Db::getInstance()->ExecuteS('
		SELECT p.`id_product`,pl.`name`
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($cookie->id_lang).')
		WHERE p.`active` = 1 AND p.`id_lofdcategory_default` ='.(int)($id_lofdcategory).'
		ORDER BY p.`id_product` DESC');	
		return $results;
	}
	
 	function showQuickUploadForm(){
		global $currentIndex;
		$site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__); 
		Configuration::updateValue("lofdownload_uptoken", $this->token );
		return $this->renderLayout("admin/quickupload_form", array( 'currentIndex'=>$currentIndex, 'site_url'=>$site_url ) );
	}
}


?>