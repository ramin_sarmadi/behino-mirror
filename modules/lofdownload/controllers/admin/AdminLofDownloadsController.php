<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
include_once(dirname(__FILE__).'/AdminLDController.php');

class AdminLofDownloadsController extends AdminLDController {
	
	public $imagesThumbs = array();
	public $allowVideoMineType = array();
	public $allowImageMineType = array();
	public $_category ;
	
	public function __construct(){
	 	$this->table = 'lofdownload';
		$this->className = 'LofClassDownload';
		$this->identifier = 'id_lofdownload';
	 	$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->lang = true;
		$this->_category = AdminLofManageDownloadsController::getCurrentCategory();

		
		$this->fields_list  = array(
			'id_lofdownload' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 10),
			'name' => array('title' => $this->l('Name'), 'width' => 180),
			'description' => array('title' => $this->l('Description'), 'width' => 300, 'maxlength' => 90, 'orderby' => false),
			'download' => array('title' => $this->l('Download'), 'width' => 56),
			'date_add' => array('title' => $this->l('Date add'), 'align' => 'center', 'width' => 100,'havingFilter'=>true),
			'active' => array('title' => $this->l('Displayed'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false)
		);
 
				
		if( (int) $this->_category->id != 1 ){
			$this->_filter = ' AND a.`id_lofdcategory` = '.(int)($this->_category->id);
		}
		
		
	
		return parent::__construct();
	}
	
	public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
	{
		if ($order_by && $this->context->cookie->__get($this->table.'Orderby'))
			$order_by = $this->context->cookie->__get($this->table.'Orderby');
		else
			$order_by = 'id_lofdownload';

		parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
	}
	
	public function renderList()
	{
		if (($id_lofdcategory = (int)$this->id_lofdcategory))
			self::$currentIndex .= '&id_lofdcategory='.$id_lofdcategory;
		$this->toolbar_title = $this->l('File Downloads in Category');
		$this->toolbar_btn['new'] = array(
			'href' => self::$currentIndex.'&amp;add'.$this->table.'&amp;token='.$this->token,
			'desc' => $this->l('Add new')
		);

		return parent::renderList();
	}
	public function displayList($token = null)
	{
		/* Display list header (filtering, pagination and column names) */
		$this->displayListHeader($token);
		if (!count($this->_list))
			echo '<tr><td class="center" colspan="'.(count($this->fields_list) + 2).'">'.$this->l('No items found').'</td></tr>';

		/* Show the content of the table */
		$this->displayListContent($token);

		/* Close list table and submit button */
		$this->displayListFooter($token);
	}
	/**
	 * Update product accessories
	 *
	 * @param object $product Product
	 */
	public function updateAccessories($object)
	{
		$object->deleteAccessories();
		if ($accessories = Tools::getValue('inputAccessories'))
		{
			$accessories_id = array_unique(explode('-', $accessories));

			if (sizeof($accessories_id))
			{
				array_pop($accessories_id);
				$object->changeAccessories($accessories_id);
			}
		}
	}
	
	public function postProcess($token = NULL){
		global $currentIndex;
		$cookie = $this->context->cookie;
		$token = Tools::getValue('token');
		if (Tools::isSubmit('submitAdd'.$this->table) OR Tools::isSubmit('submitAddOther'.$this->table) OR Tools::isSubmit('submitAddlofdownloadAndStay')){ 
			$this->validateRules();
			if (!sizeof($this->errors)){
				$id = (int)(Tools::getValue($this->identifier));
				if (isset($id) AND !empty($id)){
					if($this->tabAccess['edit'] === '1'){ 
						if (!sizeof($this->errors)){
							
							$object = new $this->className($id);
							$this->copyFromPost($object, $this->table); 
							if ($object->update()){
								$this->updateAccessories( $object );
								if(Tools::isSubmit('submitAddlofdownload')){
									Tools::redirectAdmin($currentIndex.'&token='.$token);
								}else{
									Tools::redirectAdmin($currentIndex.'&id_lofdownload='.$object->id.'&updatelofdownload&token='.$token);
								}
								
							}else{
								$this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.'</b>';
							}
						}
					}else{
						$this->errors[] = Tools::displayError('You do not have permission to add here.');
					}
				}else{
					if ($this->tabAccess['add'] === '1'){ 
						if (!sizeof($this->errors)){
							$object = new $this->className();
							$this->copyFromPost($object, $this->table);
							if ($object->add()){ 
								$this->updateAccessories( $object );
								$id_lofdcategory = Tools::getValue('id_lofdcategory');
								if(Tools::isSubmit('submitAddlofdownload'))
									Tools::redirectAdmin($currentIndex.($id_lofdcategory ? '&id_lofdcategory='.$id_lofdcategory : '').'&token='.$token);
								Tools::redirectAdmin($currentIndex.($id_lofdcategory ? '&id_lofdcategory='.$id_lofdcategory : '').'&id_lofdownload='.$object->id.'&updatelofdownload&token='.$token);
								
							}else{
								$this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.'</b>';
							}
						}
					}else{
						$this->errors[] = Tools::displayError('You do not have permission to add here.');
					}
				}
			}else{
				$this->errors = array_unique($this->errors);
			}
		}
		/* Delete object */
		elseif (isset($_GET['delete'.$this->table])){
			if ($this->tabAccess['delete'] === '1'){
				if (Validate::isLoadedObject($object = $this->loadObject())){
					$id = $object->id;
					if ($object->delete()){
						// process code to delete
						Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token'));
					}
					$this->errors[] = Tools::displayError('An error occurred during deletion.');
				}else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		/* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table)){
			if ($this->tabAccess['delete'] === '1'){
				if (isset($_POST[$this->table.'Box'])){
					$lofcustomerupload = new LofClassDownload();
					$result = true;
					$result = $lofcustomerupload->deleteSelection(Tools::getValue($this->table.'Box'));
					if ($result){
						Tools::redirectAdmin($currentIndex.'&conf=2&token='.Tools::getValue('token'));
					}
					$this->errors[] = Tools::displayError('An error occurred while deleting selection.');

				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
			return;
		}elseif (isset($_GET['statuslofdownload'])) {
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->toggleStatus())
						Tools::redirectAdmin($currentIndex.'&conf=5'.((int)Tools::getValue('id_lofdcategory') ? '&id_lofdcategory='.(int)Tools::getValue('id_lofdcategory') : '').'&token='.Tools::getValue('token'));
					else
						$this->errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		parent::postProcess();
	}
    
	/**
	 * display edit or add form 
	 */
	public function renderForm($token = true){
		global $currentIndex;
		$cookie = $this->context->cookie;
		parent::renderForm();
		if (!($obj = $this->loadObject(true)))
			return;
		$site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		$products_download = LofClassDownload::getAccessoriesLight( (int)($cookie->id_lang), $obj->id );
		Configuration::updateValue("lofdownload_uptoken", $this->token );
		$id_lofdcategory = Tools::getValue('id_lofdcategory');
		$quickuploadForm = $this->renderLayout("admin/quickupload_form", array( 'currentIndex'=>$currentIndex, 'site_url'=>$site_url ) );
		$categories = array();
		return $this->renderLayout("admin/lofdownload_form", 
		   array( "obj" => $obj,
				"currentIndex"    => $currentIndex, 
				"identifier"      => $this->identifier,
				'categories'      => $categories,
				'site_url'	      => $site_url,
				'accessories'     => $products_download,
				'id_lofdcategory'     => $id_lofdcategory,
				'quickuploadForm' => $quickuploadForm
			) 
		);
	}
	
 
}
