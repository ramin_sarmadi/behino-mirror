<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/../../classes/admintab.php" );

class AdminLDController extends AdminController {
	
	public $_defaultFormLanguage;
	public $_errors;
	public function __construct() {
		parent::__construct();
		$this->getlanguages();
		$this->_defaultFormLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
		
	}
	
	public function displayFlags($languages, $default_language, $ids, $id, $return = false, $use_vars_instead_of_ids = false)
	{
		if (count($languages) == 1)
			return false;

		$output = '
		<div class="displayed_flag">
			<img src="../img/l/'.$default_language.'.jpg" class="pointer" id="language_current_'.$id.'" onclick="toggleLanguageFlags(this);" alt="" />
		</div>
		<div id="languages_'.$id.'" class="language_flags">
			'.$this->l('Choose language:').'<br /><br />';
		foreach ($languages as $language)
			if ($use_vars_instead_of_ids)
				$output .= '<img src="../img/l/'.(int)$language['id_lang'].'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="changeLanguage(\''.$id.'\', '.$ids.', '.$language['id_lang'].', \''.$language['iso_code'].'\');" /> ';
			else
			$output .= '<img src="../img/l/'.(int)$language['id_lang'].'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="changeLanguage(\''.$id.'\', \''.$ids.'\', '.$language['id_lang'].', \''.$language['iso_code'].'\');" /> ';
		$output .= '</div>';

		if ($return)
			return $output;
		echo $output;
	}
	public function displayImage($id, $image, $size, $id_image = NULL, $token = NULL, $disableCache = false)
	{
		if (!isset($token) || empty($token))
			$token = $this->token;
		$html = '';
		if ($id && file_exists($image))
			$html .= '
			<div id="image" >
				'.ImageManager::thumbnail($image, $this->table.'_'.(int)($id).'.'.$this->imageType, $size, $this->imageType, $disableCache).'
				<p align="center">'.$this->l('File size').' '.(filesize($image) / 1000).'kb</p>
				<a href="'.self::$currentIndex.'&'.$this->identifier.'='.(int)($id).'&token='.$token.($id_image ? '&id_image='.(int)($id_image) : '').'&deleteImage=1">
				<img src="../img/admin/delete.gif" alt="'.$this->l('Delete').'" /> '.$this->l('Delete').'</a>
			</div>';
		return $html;
	}
	public function renderLayout( $layout, $args=array() ){
		extract( $args );
		$moduleBasePath = _PS_MODULE_DIR_.'lofdownload/';
		ob_start();
		include $moduleBasePath.'/tmpl/'.$layout.'.php';
		$data = ob_get_clean();
		return $data;
	}
}
