<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
 include_once(dirname(__FILE__).'/AdminLDController.php');
class AdminLofCategoriesController extends AdminLDController
{
	public $lofid_parent;
	protected $maxImageSize = 300000;

	/** @var object Category() instance for navigation*/
	private $_category;

	public function __construct()
	{
		global $cookie;
		
		$this->lofid_parent = 'id_lofdcategory_parent';
		$this->identifiersDnd = array('id_lofdcategory' => 'id_lofdcategory_to_move');
		$this->position_identifier = 'id_lofdcategory_to_move';
		$this->table = 'lofdcategory';
	 	$this->className = 'LofDownloadCategory';
	 	$this->identifier = 'id_lofdcategory';
	 	$this->lang = true;
		$this->addRowAction('view');
	 	$this->addRowAction('edit');
		$this->addRowAction('delete');

 		$this->fieldImageSettings  = array('name' => 'image', 'dir' => 'lofcategory');

		$this->fields_list = array(
				'id_lofdcategory' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 10),
				'name'			  => array('title' => $this->l('Name'), 'width' => 100),
				'description'     => array('title' => $this->l('Description'), 'width' => 500, 'maxlength' => 90, 'orderby' => false),
				'position' 		  => array('title' => $this->l('Position'), 'width' => 40, 'filter_key' => 'position', 'align' => 'center', 'position' => 'position'),
				'active' => array('title' => $this->l('Displayed'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false));
		
	 	$this->_category =   AdminLofManageDownloadsController::getCurrentCategory();
		
		 
 	 	$this->_filter = 'AND `id_parent` = '.(int)($this->_category->id);
 	  	$this->_select = 'position ';

		parent::__construct();
	}
	
	public function initBreadcrumbs()
	{
		$this->breadcrumbs[] = $this->l('Categories');
	}
	
	public function getList($id_lang, $order_by = null, $order_way = 'ASC', $start = 0, $limit = null, $id_lang_shop = false) {
		parent::getList($id_lang, 'position', $order_way, $start, $limit);
	}
	
	public function initToolbar(){
		
		if (empty($this->display))
		{
			$id_lofdcategory = (Tools::isSubmit('id_lofdcategory')) ? '&amp;id_parent='.(int)Tools::getValue('id_lofdcategory') : '';
			$this->toolbar_btn['new'] = array(
				'href' => self::$currentIndex.'&amp;add'.$this->table.'&amp;token='.$this->token.$id_lofdcategory,
				'desc' => $this->l('Add new')
			);

			if (Tools::isSubmit('id_lofdcategory'))
			{
				$back = Tools::safeOutput(Tools::getValue('back', ''));
				if (empty($back))
					$back = self::$currentIndex.'&token='.$this->token;
				$this->toolbar_btn['back'] = array(
					'href' => $back,
					'desc' => $this->l('Back to list')
				);
			}
		}
		parent::initToolbar();
	}
	
	public function renderList()
	{
		$this->initToolbar();
		return parent::renderList();
	}

	public function postProcess($token = NULL)
	{
		global $cookie, $currentIndex;
		$this->token = Tools::getValue('token');
		//$this->tabAccess = Profile::getProfileAccess($cookie->profile, $this->id);

		if (Tools::isSubmit('submitAdd'.$this->table)){
			$this->processAdd();
		}
		/* Delete object */
		elseif (isset($_GET['delete'.$this->table]))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()) AND isset($this->fieldImageSettings))
				{
					// check if request at least one object with noZeroObject
					if (isset($object->noZeroObject) AND sizeof($taxes = call_user_func(array($this->className, $object->noZeroObject))) <= 1)
						$this->errors[] = Tools::displayError('You need at least one object.').' <b>'.$this->table.'</b><br />'.Tools::displayError('You cannot delete all of the items.');
					else
					{
						if ($this->deleted)
						{
							$object->deleteImage();
							$object->deleted = 1;
							if ($object->update())
								Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_lofdcategory='.(int)($object->id_parent));
						}
						elseif ($object->delete())
							Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_lofdcategory='.(int)($object->id_parent));
						$this->errors[] = Tools::displayError('An error occurred during deletion.');
					}
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		elseif (isset($_GET['position']))
		{
			if ($this->tabAccess['edit'] !== '1')
				$this->errors[] = Tools::displayError('You do not have permission to edit here.');
			elseif (!Validate::isLoadedObject($object = new LofDownloadCategory((int)(Tools::getValue($this->identifier, Tools::getValue('id_lofdcategory_to_move', 1))))))
				$this->errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			if (!$object->updatePosition((int)(Tools::getValue('way')), (int)(Tools::getValue('position'))))
				$this->errors[] = Tools::displayError('Failed to update the position.');
			else{
				
				Tools::redirectAdmin($currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_lofdcategory = (int)(Tools::getValue($this->identifier, Tools::getValue('id_lofdcategory_parent', 1)))) ? ('&'.$this->identifier.'='.$id_lofdcategory) : '').'&token='.Tools::getValue('token'));
			}
		}
		/* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table))
		{
			if ($this->tabAccess['delete'] === '1')
			{
				if (isset($_POST[$this->table.'Box']))
				{
					$category = new LofDownloadCategory();
					$result = true;
					$result = $category->deleteSelection(Tools::getValue($this->table.'Box'));
					if ($result)
					{
						$category->cleanPositions((int)(Tools::getValue('id_lofdcategory')));
						Tools::redirectAdmin($currentIndex.'&conf=2&token='.Tools::getValue('token').'&id_lofdcategory='.(int)(Tools::getValue('id_lofdcategory')));
					}
					$this->errors[] = Tools::displayError('An error occurred while deleting selection.');

				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
			return;
		}elseif (isset($_GET['deleteImage'])) {
			if ($this->tabAccess['edit'] === '1'){
				$id_lofdcategory = (int)Tools::getValue('id_lofdcategory');
				if (Validate::isLoadedObject($object = new LofDownloadCategory((int)$id_lofdcategory)) AND isset($this->fieldImageSettings)) {
					if ($object->deleteImage())
						Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_lofdcategory='.(int)($id_lofdcategory).'&updatelofdcategory');
					$this->errors[] = Tools::displayError('An error occurred during deletion.');
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}elseif (isset($_GET['statuslofdcategory'])) {
			if ($this->tabAccess['edit'] === '1')
			{
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->toggleStatus())
						Tools::redirectAdmin($currentIndex.'&conf=5'.((int)$object->id_parent ? '&id_lofdcategory='.(int)$object->id_parent : '').'&token='.Tools::getValue('token'));
					else
						$this->errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to edit here.');
		}
		parent::postProcess();
	}
	
	public function processAdd()
	{
		$id_lofdcategory = (int)Tools::getValue('id_lofdcategory');
		$id_parent = (int)Tools::getValue('id_parent');
		// if true, we are in a root category creation
		if (!$id_parent && !Tools::isSubmit('is_root_category'))
			$_POST['is_root_category'] = $_POST['level_depth'] = $_POST['id_parent'] = $id_parent = 1;
		if ($id_lofdcategory)
		{
			if ($id_lofdcategory != $id_parent)
			{
				if (!LofDownloadCategory::checkBeforeMove($id_lofdcategory, $id_parent))
					$this->errors[] = Tools::displayError($this->l('Category cannot be moved here'));
			}
			else
				$this->errors[] = Tools::displayError($this->l('Category cannot be parent of itself.'));
			$object = $this->processUpdate();
		}else
			$object = parent::processAdd();
		
		if($object)
			$this->redirect();
	}
	
	protected function postImage($id)
	{
		 
		$ret = parent::postImage($id);
		if (($id_lofdcategory = (int)(Tools::getValue('id_lofdcategory'))) AND isset($_FILES) AND sizeof($_FILES) AND $_FILES['image']['name'] != NULL AND file_exists(_LOFDOWNLOAD_DIR_CATIMAGE_.$id_lofdcategory.'.jpg'))
		{
			$imagesTypes = ImageType::getImagesTypes('categories');
			$imageHeight = (int)Configuration::get("lofdownload_catimg_height");
			$imageHeight = $imageHeight?$imageHeight:45;
			$imageWidth = (int)Configuration::get("lofdownload_catimg_width");
			$imageWidth = $imageWidth?$imageWidth:45;

			foreach ($imagesTypes AS $k => $imageType)
				ImageManager::resize(_LOFDOWNLOAD_DIR_CATIMAGE_.$id_lofdcategory.'.jpg', _LOFDOWNLOAD_DIR_CATIMAGE_.$id_lofdcategory.'-'.stripslashes($imageType['name']).'.jpg', $imageWidth, $imageHeight );
		}
		return $ret;
	}

	public function renderForm($token = NULL)
	{
		global $currentIndex, $cookie;
		parent::renderForm();
		$divLangName = 'name�cdescription�meta_title�meta_description�meta_keywords�link_rewrite';
		if (!($obj = $this->loadObject(true)))
			return;
		$active = $this->getFieldValue($obj, 'active');
		$customer_groups = $obj->getGroups();

		$html = '
		<script type="text/javascript">
			var PS_ALLOW_ACCENTED_CHARS_URL = 0;
		</script>
		<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.($token!=NULL ? $token : $this->token).'" method="post" enctype="multipart/form-data">
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/admin/tab-categories.gif" />'.$this->l('Category').'</legend>
				<label>'.$this->l('Name:').' </label>
				<div class="margin-form translatable">';
		foreach ($this->_languages AS $language)
			$html .= '
					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input type="text" style="width: 260px" name="name_'.$language['id_lang'].'" id="name_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'name', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" '.((!$obj->id) ? ' class="copy2friendlyUrl"' : '').' /><sup> *</sup>
						<span class="hint" name="help_box">'.$this->l('Invalid characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
					</div>';
		$html .= $this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'name', true);
		$html .= '	<p class="clear"></p>
				</div>
				<label>'.$this->l('Displayed:').' </label>
				<div class="margin-form">
					<input type="radio" name="active" id="active_on" value="1" '.($active ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"><img src="../img/admin/enabled.gif" alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" /></label>
					<input type="radio" name="active" id="active_off" value="0" '.(!$active ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"><img src="../img/admin/disabled.gif" alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" /></label>
				</div>
				<label>'.$this->l('Parent category:').' </label>
				<div class="margin-form">';
				// Translations are not automatic for the moment ;)
				$trads = array(
					 'Home' => $this->l('Home'), 
					 'selected' => $this->l('selected'), 
					 'Collapse All' => $this->l('Collapse All'), 
					 'Expand All' => $this->l('Expand All')
				);
				$html .= LofDownloadHelper::renderAdminCategorieTree($trads, array(isset($obj->id_parent) ? $obj->id_parent : Tools::getValue('id_parent', 1)), 'id_parent', true);
				$html .= '</div>
				<label>'.$this->l('Description:').' </label>
				<div class="margin-form translatable">';
		foreach ($this->_languages AS $language)
			$html .= '
					<div id="cdescription_'.$language['id_lang'].'" class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<textarea id="description_'.$language['id_lang'].'" name="description_'.$language['id_lang'].'" rows="10" cols="100">'.htmlentities($this->getFieldValue($obj, 'description', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
					</div>';
		$html .= $this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'cdescription', true);
		$html .= '	<p class="clear"></p>
				</div>
				<label>'.$this->l('Image:').' </label>
				<div class="margin-form">';
		$html .= 		$this->displayImage($obj->id, _LOFDOWNLOAD_DIR_CATIMAGE_.'/'.$obj->id.'.jpg', 350, NULL, Tools::getAdminToken('AdminLofManageDownloads'.(int)(Tab::getIdFromClassName('AdminLofManageDownloads')).(int)($cookie->id_employee)), true);
		$html .= '	<br /><input type="file" name="image" />
					<p>'.$this->l('Upload category logo from your computer').'</p>
				</div>
				<div class="clear"><br /></div>	
				<label>'.$this->l('Meta title:').' </label>
				<div class="margin-form translatable">';
		foreach ($this->_languages AS $language)
			$html .= '
					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input type="text" name="meta_title_'.$language['id_lang'].'" id="meta_title_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'meta_title', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" />
						<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
					</div>';
			$html .= $this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'meta_title', true);
		$html .= '	<p class="clear"></p>
				</div>
				<label>'.$this->l('Meta description:').' </label>
				<div class="margin-form translatable">';
		foreach ($this->_languages AS $language)
			$html .= '<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input type="text" name="meta_description_'.$language['id_lang'].'" id="meta_description_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'meta_description', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" />
						<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
				</div>';
		$html .= $this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'meta_description', true);
		$html .= '	<p class="clear"></p>
				</div>
				<label>'.$this->l('Meta keywords:').' </label>
				<div class="margin-form translatable">';
		foreach ($this->_languages AS $language)
			$html .= '
					<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input type="text" name="meta_keywords_'.$language['id_lang'].'" id="meta_keywords_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'meta_keywords', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" />
						<span class="hint" name="help_box">'.$this->l('Forbidden characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
					</div>';
		$html .= $this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'meta_keywords', true);
		$html .= '	<p class="clear"></p>
				</div>
				<label>'.$this->l('Friendly URL:').' </label>
				<div class="margin-form translatable">';
		foreach ($this->_languages AS $language)
			$html .= '<div class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input type="text" name="link_rewrite_'.$language['id_lang'].'" id="link_rewrite_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'link_rewrite', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" /><sup> *</sup>
						<span class="hint" name="help_box">'.$this->l('Only letters and the minus (-) character are allowed').'<span class="hint-pointer">&nbsp;</span></span>
					</div>';
		$html .= $this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'link_rewrite', true);
		$html .= '	<p class="clear"></p>
				</div>
				<label>'.$this->l('Groups access:').' </label>
				<div class="margin-form">';
					$groups = Group::getGroups((int)($cookie->id_lang));
					if (sizeof($groups))
					{
						$html .= '
					<table cellspacing="0" cellpadding="0" class="table" style="width: 28em;">
						<tr>
							<th><input type="checkbox" name="checkme" class="noborder" onclick="checkDelBoxes(this.form, \'groupBox[]\', this.checked)"'.(!isset($obj->id) ? 'checked="checked" ' : '').' /></th>
							<th>'.$this->l('ID').'</th>
							<th>'.$this->l('Group name').'</th>
						</tr>';
						$irow = 0;
						foreach ($groups AS $group)
							$html .= '
							<tr class="'.($irow++ % 2 ? 'alt_row' : '').'">
								<td><input type="checkbox" name="groupBox[]" class="groupBox" id="groupBox_'.$group['id_group'].'" value="'.$group['id_group'].'" '.((in_array($group['id_group'], $customer_groups) OR (!isset($obj->id))) ? 'checked="checked" ' : '').'/></td>
								<td>'.$group['id_group'].'</td>
								<td><label for="groupBox_'.$group['id_group'].'" class="t">'.$group['name'].'</label></td>
							</tr>';
						$html .= '
					</table>
					<p style="padding:0px; margin:10px 0px 10px 0px;">'.$this->l('Mark all groups you want to give access to this category').'</p>
					';
					} else
						$html .= '<p>'.$this->l('No group created').'</p>';
				$html .= '
				</div>
				<div class="margin-form">
					<input type="submit" value="'.$this->l('Save and back to parent category').'" name="submitAdd'.$this->table.'AndBackToParent" class="button" />
					&nbsp;<input type="submit" class="button" name="submitAdd'.$this->table.'" value="'.$this->l('Save').'"/>
					&nbsp;<input type="submit" class="button" name="submitAdd'.$this->table.'AndStay" value="'.$this->l('Save and Stay').'"/>
				</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>
		<p class="clear"></p>';
		
		return $html;
	}
	
}
