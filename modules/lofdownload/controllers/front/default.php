<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14206 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
class LofdownloadDefaultModuleFrontController extends ModuleFrontController
{
	public $display_column_left = false;

	public function __construct()
	{
		parent::__construct();

		$this->context = Context::getContext();
		include_once($this->module->getLocalPath().'libs/defined.php');
		include_once($this->module->getLocalPath().'classes/LofClassDownload.php');
		include_once($this->module->getLocalPath().'classes/LofDownloadCategory.php');
		include_once($this->module->getLocalPath().'classes/LofDownloadHelper.php');
		include_once($this->module->getLocalPath().'classes/LofDownloadRating.php');
		
		// Declare smarty function to render pagination link
		smartyRegisterFunction($this->context->smarty, 'function', 'summarypaginationlink', array('LoyaltyDefaultModuleFrontController', 'getSummaryPaginationLink'));
	}
	
	public function setMedia()
	{
		parent::setMedia();
		$this->addJqueryPlugin('fancybox');
		$this->addJS( _MODULE_DIR_.'lofdownload/assets/rate/behavior.js' );
		$this->addJS( _MODULE_DIR_.'lofdownload/assets/rate/rating.js' );
	}
	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess()
	{
		if (Tools::getValue('process') == 'transformpoints')
			$this->processTransformPoints();
	}
	
	/**
	 * Transform loyalty point to a voucher
	 */
	public function processTransformPoints()
	{
		$customerPoints = (int)LoyaltyModule::getPointsByCustomer((int)$this->context->customer->id);
		if ($customerPoints > 0)
		{
			/* Generate a voucher code */
			
			Tools::redirect($this->context->link->getModuleLink('loyalty', 'default', array('process' => 'summary')));
		}
	}

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
		$this->context->controller->addJqueryPlugin(array('dimensions', 'cluetip'));
		
		if (Tools::getValue('process') == 'list')
			$this->assignListExecution();
	}
	
	/**
	 * Render pagination link for summary
	 *
	 * @param (array) $params Array with to parameters p (for page number) and n (for nb of items per page)
	 * @return string link
	 */
	public static function getSummaryPaginationLink($params, &$smarty)
	{
		if (!isset($params['p']))
			$p = 1;
		else
			$p = $params['p'];
			
		if (!isset($params['n']))
			$n = 10;
		else
			$n = $params['n'];
		
		return Context::getContext()->link->getModuleLink(
			'lofdownload',
			'default',
			array(
				'process' => 'list',
				'p' => $p,
				'n' => $n,
			)
		);
	}
	/**
	 * Assign summary template
	 */
	public function assignListExecution()
	{
		global $cookie, $link;
		$id_category   = (int)Tools::getValue("id_category");
		$id_download   = (int)Tools::getValue("id_download"); 
		$helper 	   = new LofDownloadHelper();
		if( ($id_download) > 0 && $id_category <= 0 ){
			$error = Tools::getValue( "error" );
			$rating  = new LofDownloadRating( $id_download );
			$file = LofClassDownload::getDownloadInfo( $id_download, $cookie->id_lang );
			if($file){
				if($file['filesize'] < 1024){
					$file['filesize_dv'] = 'Byte';
				}elseif($file['filesize']/1024 < 1024){
					$file['filesize'] = Tools::ps_round($file['filesize']/1024,2);
					$file['filesize_dv'] = 'Kb';
				}elseif($file['filesize']/(1024*1024) < 1024){
					$file['filesize'] = Tools::ps_round($file['filesize']/(1024*1024),2);
					$file['filesize_dv'] = 'Mb';
				}
			}
			if(  (int) $rating->id > 0  ){
				$file['rating_sum']   = $rating->rating_sum;
				$file['rating_count'] = $rating->rating_count;
			}
			
			$avariable   = $file != null ? true : false;
			$category    = new LofDownloadCategory( $file['id_lofdcategory'], $cookie->id_lang );
			$checkAccess = $category->checkAccess( $cookie->id_customer );
		
			$fileError = false; 
			if( (int)$file['id_lofdcategory'] <= 0 ){
				$error = true;
				$fileError = true;
			}
			
			$this->context->smarty->assign(array(
				'pageTitle' => "Downloads",		
				'file'      => $file,
				'avariable' => $avariable,
				'helper'    => $helper,
				'checkAccess' => $checkAccess,
				'error'     => $error,
				'fileError' => $fileError,
				'link' 		=> $link,
				
				'meta_title'  		=> $file['name'],
				
				'HOTHIT_NUMBER' => _LOFDOWNLOAD_MODULE_HOT_HITS_NUM_,
				'NEW_DAY' => _LOFDOWNLOAD_MODULE_NEW_DAY_,
				'UPDATE_DAY' => _LOFDOWNLOAD_MODULE_UPDATE_DAY_
			));	
			
			$this->setTemplate('detail.tpl');
		}else {
			
			$id_category   = $id_category ? $id_category:1;	
			$categories    = LofDownloadCategory::getChildren( $id_category, $cookie->id_lang );
			
			foreach( $categories as $key => $category ){
				/*$categories[$key]['count_download'] = LofDownloadCategory::countDownloadByCategoryId( $category['id_lofdcategory'], $cookie->id_lang, true );*/
				$objCate = new LofDownloadCategory($category['id_lofdcategory']);
				$categories[$key]['count_sub'] = count($objCate->getSubCategories($cookie->id_lang, true));
			}

			$parent = new LofDownloadCategory( $id_category );
			$page = ((int)(Tools::getValue('p')) > 0 ? (int)(Tools::getValue('p')) : 1);
			$limit = ((int)(Tools::getValue('n')) > 0 ? (int)(Tools::getValue('n')) : 10);
			$orderby    = LofDownloadHelper::getCategoryOrderBy( Tools::getValue("orderby") );
			$orderway   = LofDownloadHelper::getCategoryOrderway( Tools::getValue("orderway") );
			$allfiles	= $parent->getFilesByCategoryId( $id_category, $cookie->id_lang, true, $orderby, $orderway );
			$files		= $parent->getFilesByCategoryId( $id_category, $cookie->id_lang, true, $orderby,$orderway, $limit, $page );
			if($files)
				foreach($files as &$f){
					if($f['filesize'] < 1024){
						$f['filesize_dv'] = 'Byte';
					}elseif($f['filesize']/1024 < 1024){
						$f['filesize'] = Tools::ps_round($f['filesize']/1024,2);
						$f['filesize_dv'] = 'Kb';
					}elseif($f['filesize']/(1024*1024) < 1024){
						$f['filesize'] = Tools::ps_round($f['filesize']/(1024*1024),2);
						$f['filesize_dv'] = 'Mb';
					}
				}
			
			$sortby  = Tools::getValue("orderby").":". Tools::getValue("orderway");
			$lofdownload = new lofdownload();
			if($id_category == 1){
				$meta_title = $lofdownload->getParamValue('meta_title','download manager');
				$meta_description = $lofdownload->getParamValue('meta_description','download manager');
				$meta_keywords = $lofdownload->getParamValue('meta_keywords','download manager');
			}else{
				$meta_title = $parent->getMetaTitle();
				$meta_description = $parent->getMetaDescription();
				$meta_keywords = $parent->getMetaKeyWords();
			}
			$this->context->smarty->assign(array( 
				'pageTitle' => "Downloads",		
				'files'     => $files,
				'allfiles'  => $allfiles,
				'sortby'	=> $sortby,
				'id_category' 	  => $id_category,
				'helper'      	  => $helper,
				'parent'  	  	  => $parent,
				'categories'      => $categories,
				'countCategories' => count($categories),
				'meta_title'  		=> $meta_title,
				'meta_description'  => $meta_description,
				'meta_keywords'  => $meta_keywords,
				'HOTHIT_NUMBER' => _LOFDOWNLOAD_MODULE_HOT_HITS_NUM_,
				'NEW_DAY' => _LOFDOWNLOAD_MODULE_NEW_DAY_,
				'UPDATE_DAY' => _LOFDOWNLOAD_MODULE_UPDATE_DAY_,
				
				'pagination_link' => __PS_BASE_URI__.'index.php?process=list&fc=module&module=lofdownload&controller=default&id_category='.(int)$id_category,
				'page' => ((int)(Tools::getValue('p')) > 0 ? (int)(Tools::getValue('p')) : 1),
				'nbpagination' => ((int)(Tools::getValue('n') > 0) ? (int)(Tools::getValue('n')) : 10),
				'nArray' => array(10,30, 50, 100),
				'max_page' => floor(sizeof($allfiles) / ((int)(Tools::getValue('n') > 0) ? (int)(Tools::getValue('n')) : 10))
			
			));
			$this->setTemplate('lofdownload.tpl');
		}
	}
	
	
	public function getTemplatePath(){
		return _PS_MODULE_DIR_.$this->module->name.'/tmpl/frontend/';
	}
}
