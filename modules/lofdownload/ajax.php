<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
define('PS_ADMIN_DIR',(dirname(  getcwd() ))); 
include(PS_ADMIN_DIR.'/../config/config.inc.php');
/* Getting cookie or logout */  
if(  Tools::getValue('baseURI') ){
	$_SERVER['SCRIPT_NAME'] = 	Tools::getValue('baseURI')."/index.php";
}
require_once(dirname(__FILE__).'/init.php');
require_once( dirname(__FILE__).'/classes/LofDownloadCategory.php' );
 

if (Tools::isSubmit('getChildrenCategories') && Tools::getValue('id_category_parent'))
{
	$children_categories = LofDownloadCategory::getChildrenWithNbSelectedSubCat(Tools::getValue('id_category_parent'), Tools::getValue('selectedCat'), $cookie->id_lang);
	die(Tools::jsonEncode($children_categories));
}
if (array_key_exists('ajaxLofdcategoryPositions', $_POST)){
	$id_lofdcategory = (int)(Tools::getValue('id_lofdcategory'));
	$id_lofdcategory_parent = (int)(Tools::getValue('id_lofdcategory_parent'));
	$way = (int)(Tools::getValue('way'));
	$positions = Tools::getValue('lofdcategory');
	if (is_array($positions))
		foreach ($positions AS $key => $value)
		{
			$pos = explode('_', $value);
			if ((isset($pos[1]) AND isset($pos[2])) AND ($pos[1] == $id_lofdcategory_parent AND $pos[2] == $id_lofdcategory))
			{
				$position = $key;
				break;
			}
		}
	$lofdcategory = new LofDownloadCategory($id_lofdcategory);
	if (Validate::isLoadedObject($lofdcategory))
	{
		if (isset($position) && $lofdcategory->updatePosition($way, $position)){
			die(true);
		}
		else
			die('{"hasError" : true, errors : "Can not update categories position"}');
	}
	else
		die('{"hasError" : true, "errors" : "This category can not be loaded"}');
}
