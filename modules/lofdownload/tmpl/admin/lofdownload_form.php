﻿<?php
global $cookie;
$iso = Language::getIsoById((int)($cookie->id_lang));
$mytoken =   $this->token;

$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
$site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
$manageFilesURL = $site_url."modules/"._LOFDOWNLOAD_MODULE_NAME_."/managefiles.php?action=managefiles&token=".$mytoken."&file=".$obj->file_path."&field=file_path";
$previewFilesURL = $site_url."modules/"._LOFDOWNLOAD_MODULE_NAME_."/managefiles.php?action=managefiles&token=".$mytoken."&file=".$obj->preview_file."&mode=preview&field=preview_file";
$iconFilesURL = $site_url."modules/"._LOFDOWNLOAD_MODULE_NAME_."/managefiles.php?action=managefiles&token=".$mytoken."&file=".$obj->icon_file."&mode=icons&field=icon_file";

$quickUploadURL = $site_url."modules/"._LOFDOWNLOAD_MODULE_NAME_."/managefiles.php";

$ad = dirname($_SERVER["PHP_SELF"]);
Configuration::updateValue( "lofdownload_token", $mytoken  );

	echo '
		<script type="text/javascript" src="'._PS_JS_DIR_.'tiny_mce/tiny_mce.js"></script>
		<script type="text/javascript" src="'._PS_JS_DIR_.'tinymce.inc.js"></script>
		<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			tinySetup();
		</script>
		
		<script type="text/javascript" src="'.__PS_BASE_URI__.'modules/lofdownload/assets/admin/form.js"></script>
		<script src="'.__PS_BASE_URI__.'js/jquery/plugins/fancybox/jquery.fancybox.js" type="text/javascript"></script>	
		<link href="'.__PS_BASE_URI__.'js/jquery/plugins/fancybox/jquery.fancybox.css"  type="text/css" rel="stylesheet"/>
		
		<script src="'.__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.js" type="text/javascript"></script>	
		<link href="'.__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.css"  type="text/css" rel="stylesheet"/>
		
		<link href="'.__PS_BASE_URI__.'modules/'._LOFDOWNLOAD_MODULE_NAME_.'/assets/admin/form.css" type="text/css" rel="stylesheet"/>
		<script type="text/javascript">
			$(document).ready( function(){ 
				$(".fancy-ajax").fancybox({
					"width"				: 800,
					"height"			: \'750\',	
					\'type\'				: \'iframe\',
					"scrolling" : "no"
				});
			} );
			
		</script>
		';

?>

<form action="<?php echo $currentIndex;?>&token=<?php echo Tools::getAdminTokenLite('AdminLofManageDownloads');?>" method="post" enctype="multipart/form-data" name="customerupload" id="customerupload">
	<?php echo ($obj->id ? '<input type="hidden" name="'.$identifier.'" value="'.$obj->id.'" />' : '');?>

	<fieldset><legend><img src="../img/admin/cms.gif" /><?php echo $this->l('File upload page');?></legend>
    	
		<label><?php echo $this->l('Title:');?> </label>
		<div class="margin-form">
			 
         <?php
			$divLangName = 'name¤cdescription¤cinformation';		 
			foreach ($this->_languages AS $language) {
				echo '
						<div id="name_'.$language['id_lang'].'"  class="lang_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
							<input type="text" style="width: 260px" name="name_'.$language['id_lang'].'" id="name_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'name', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" '.((!$obj->id) ? ' onkeyup="copy2friendlyURL();"' : '').' /><sup> *</sup>
							<span class="hint" name="help_box">'.$this->l('Invalid characters:').' <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
						</div> ';
			}
			$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'name');			
		?>
        <p class="clear"></p>
        
		</div>
        
        <?php 
				echo '<label>'.$this->l('Parent category:').' </label>
				<div class="margin-form">';
				// Translations are not automatic for the moment ;)
				$trads = array(
					 'Home' => $this->l('Home'), 
					 'selected' => $this->l('selected'), 
					 'Collapse All' => $this->l('Collapse All'), 
					 'Expand All' => $this->l('Expand All')
				);
				echo LofDownloadHelper::renderAdminCategorieTree($trads, array(isset($obj->id_lofdcategory) ? $obj->id_lofdcategory : Tools::getValue('id_lofdcategory', 1)), 'id_lofdcategory', true);
				echo '</div>';
				
		?>
		<label><?php echo $this->l('Version:');?> </label>
		<div class="margin-form">
			<input  name="version" value="<?php echo htmlentities($this->getFieldValue($obj, 'version'), ENT_COMPAT, 'UTF-8');?>"  />
			<p><?php echo $this->l('For Example: 1.0.2');?></p>
		</div>	 
        <label><?php echo $this->l('Download:');?> </label>
		<div class="margin-form">
			<input  name="download" value="<?php echo htmlentities($this->getFieldValue($obj, 'download'), ENT_COMPAT, 'UTF-8');?>"  />
			<p><?php echo $this->l('Count Download Times');?></p>
		</div>	 
		<label><?php echo $this->l('File Path:');?> </label>
		<div class="margin-form">
        	 <Div style="font-size:11px; color:#666">
            	 <?php echo $this->l("This file puts into the folder") . " <b>". _LOFDOWNLOAD_DIR_UPLOAD_."</b>"; ?>
             </Div>
			 <div><input type="input" size="68" name="file_path" value="<?php echo $obj->file_path;?>" id="file_path"/> <sup>*</sup> 
             	<a href="<?php echo $manageFilesURL; ?>" class="fancy-ajax" onclick="return false;">
			 		<?php echo $this->l( "Select File" );?>
                </a>  
             </div>
		</div>
        
       	<label><?php echo $this->l('Preview File:');?> </label>
		<div class="margin-form">
        	 <Div style="font-size:11px; color:#666">
            	 <?php echo $this->l("This file puts into the folder") . " <b>". _LOFDOWNLOAD_DIR_UPLOAD_PREVIEWS_."</b>"; ?>
             </Div>
			 <div><input type="input" size="68" name="preview_file" value="<?php echo $obj->preview_file;?>" id="preview_file"/>
             	<a href="<?php echo $previewFilesURL; ?>" class="fancy-ajax" onclick="return false;">
			 		<?php echo $this->l( "Select File" );?>
                </a>  
             </div>
		</div>
         <?php
		echo '	<label>'.$this->l('Description').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '	<div id="cdescription_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
						<textarea cols="80" rows="9" id="description_'.$language['id_lang'].'" name="description_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($obj, 'description', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
					</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'cdescription');
		echo '</div>';
		?>
		<div class="clear space">&nbsp;</div><div class="clear space">&nbsp;</div>
             <?php
		echo '	<label>'.$this->l('Full Information').' </label>
				<div class="margin-form">';
		foreach ($this->_languages as $language)
			echo '	<div id="cinformation_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
						<textarea class="rte" cols="80" rows="9" id="information_'.$language['id_lang'].'" name="information_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($obj, 'information', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
					</div>';
			$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'cinformation');
		echo '</div>';
		?>
		<div class="clear space">&nbsp;</div>
		<label><?php echo $this->l('Active:');?> </label>
		<div class="margin-form">
			<input type="radio" name="active" id="active_on" onclick="toggleDraftWarning(false);" value="1" <?php echo (($this->getFieldValue($obj, 'active') || !isset($obj->active)) ? 'checked="checked" ' : '');?>/>
			<label class="t" for="active_on"> <img src="../img/admin/enabled.gif" alt="<?php echo $this->l('Enabled');?>" title="<?php echo $this->l('Enabled'); ?>" /></label>
			<input type="radio" name="active" id="active_off" onclick="toggleDraftWarning(true);" value="0" <?php echo ((!$this->getFieldValue($obj, 'active') && isset($obj->active)) ? 'checked="checked" ' : '');?>/>
			<label class="t" for="active_off"> <img src="../img/admin/disabled.gif" alt="<?php echo $this->l('Disabled');?>" title="<?php echo $this->l('Disabled');?>" /></label>
		</div>
		<!--label><?php echo $this->l('Image Preview:');?> </label>
		<div class="margin-form">
			<input type="file" name="imageupload" id="lofimageupload"/> <sup>*</sup>
			<p><?php echo $this->l('.jpg .png .gif');?></p>
			<?php if(isset($obj) && $obj->image_mine_type ){ ?>
				<?php echo LofClassDownload::getImageLink($obj->id,$this->getFieldValue($obj, 'image_mine_type'),'small',htmlentities($this->getFieldValue($obj, 'title'), ENT_COMPAT, 'UTF-8'));?> 
                <div>
                	<label class="t" for="delimage"><?php echo $this->l('Delete Old Image Preview:');?> <input id="delimage" type="checkbox" name="delImage" value="1" /></label>
                </div>
			<?php } ?>
		</div--->
        
        <label><?php echo $this->l('Icon:');?> </label>
		<div class="margin-form">
        	 <Div style="font-size:11px; color:#666">
            	 <?php echo $this->l("This file puts into the folder") . " <b>". _LOFDOWNLOAD_DIR_UPLOAD_ICONS_."</b>"; ?>
             </Div>
			 <div><input type="input" size="68" name="icon_file" value="<?php echo $obj->icon_file;?>" id="icon_file"/>
             	<a href="<?php echo $iconFilesURL; ?>" class="fancy-ajax" onclick="return false;">
			 		<?php echo $this->l( "Select File" );?>
                </a>  
             </div>
		</div>
        
        <Div class="clear"></Div>
		<div>
        <?php echo '<table>
            <tr>
						<td class="col-left">'.$this->l('Asign to Products:').'<br /><br /><i>'.$this->l('(Do not forget to Save the product afterward)').'</i></td>
						<td style="padding-bottom:5px;">
							<div id="divAccessories">';
					foreach ($accessories as $accessory)
						echo htmlentities($accessory['name'], ENT_COMPAT, 'UTF-8').(!empty($accessory['reference']) ? ' ('.$accessory['reference'].')' : '').' <span onclick="_delProductDownloads('.$accessory['id_product'].');" style="cursor: pointer;"><img src="../img/admin/delete.gif" class="middle" alt="" /></span><br />';
					echo '</div>
							<input type="hidden" name="inputAccessories" id="inputAccessories" value="';
					foreach ($accessories as $accessory)
						echo (int)$accessory['id_product'].'-';
					echo '" />
							<input type="hidden" name="nameAccessories" id="nameAccessories" value="';
					foreach ($accessories as $accessory)
						echo htmlentities($accessory['name'], ENT_COMPAT, 'UTF-8').'¤';

					echo '" />
							
							<script type="text/javascript">
								var formProduct;
								var accessories = new Array();
							</script>

							<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:600px;">
								<p>'.$this->l('Begin typing the first letters of the product name, then select the product from the drop-down list:').'</p>
								<input type="text" value="" id="product_autocomplete_input" />
								<img onclick="$(this).prev().search();" style="cursor: pointer;" src="../img/admin/add.gif" alt="'.$this->l('Add an accessory').'" title="'.$this->l('Add an accessory').'" />
							</div>
							
							<script type="text/javascript">
								urlToCall = null;
								/* function autocomplete */
								$(function() {
									$(\'#product_autocomplete_input\')
										.autocomplete(\'ajax_products_list.php\', {
											minChars: 1,
											autoFill: true,
											max:20,
											matchContains: true,
											mustMatch:true,
											scroll:false,
											cacheLength:0,
											formatItem: function(item) {
												return item[1]+\' - \'+item[0];
											}
										}).result(_addProductDownloads);
									$(\'#product_autocomplete_input\').setOptions({
										extraParams: {excludeIds : getAccessorieIds()}
									});
								});
								
							</script>
						</td>
					</tr>
            </table>';?>
        </div>
	  
		<div class="margin-form space">
			<input type="submit" value="<?php echo $this->l('   Save  And Back To List ');?>" name="submitAdd<?php echo $this->table;?>" class="button" />
            <input type="submit" value="<?php echo $this->l('   Save And Stay ');?>" name="submitAdd<?php echo $this->table;?>AndStay" class="button" />
		</div>
		<div class="small"><sup>*</sup> <?php echo $this->l('Required field');?></div>
	</fieldset>
</form>

<script type="text/javascript">
	 
	<?php echo ' function getAccessorieIds()
				{
					var ids = '. $obj->id.'+\',\';
					ids += $(\'#inputAccessories\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
					ids = ids.replace(/\,$/,\'\');
			
					return ids;
				}';
	
	?>
								
</script>