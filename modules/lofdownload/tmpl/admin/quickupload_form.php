<?php 
$mytoken =  $this->token ;
$site_url = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
$manageFilesURL = $site_url."modules/"._LOFDOWNLOAD_MODULE_NAME_."/managefiles.php?action=managefiles&token=".$mytoken."&file="."&field=file_path";
$previewFilesURL = $site_url."modules/"._LOFDOWNLOAD_MODULE_NAME_."/managefiles.php?action=managefiles&token=".$mytoken."&file="."&mode=preview&field=preview_file";
$iconFilesURL = $site_url."modules/"._LOFDOWNLOAD_MODULE_NAME_."/managefiles.php?action=managefiles&token=".$mytoken."&file="."&mode=icons&field=icon_file";
Configuration::updateValue( "lofdownload_token", $mytoken  );

if(_PS_VERSION_ < "1.5"){
	echo '
		<script src="'.__PS_BASE_URI__.'js/jquery/jquery.fancybox-1.3.4.js" type="text/javascript"></script>	
		<link href="'.__PS_BASE_URI__.'css/jquery.fancybox-1.3.4.css"  type="text/css" rel="stylesheet"/>
		<link href="'.__PS_BASE_URI__.'modules/'._LOFDOWNLOAD_MODULE_NAME_.'/assets/admin/form.css"  type="text/css" rel="stylesheet"/>';
}else{
	echo '
		<script src="'.__PS_BASE_URI__.'js/jquery/plugins/fancybox/jquery.fancybox.js" type="text/javascript"></script>	
		<link href="'.__PS_BASE_URI__.'js/jquery/plugins/fancybox/jquery.fancybox.css"  type="text/css" rel="stylesheet"/>
		<link href="'.__PS_BASE_URI__.'modules/'._LOFDOWNLOAD_MODULE_NAME_.'/assets/admin/form.css"  type="text/css" rel="stylesheet"/>';
}
echo '
	<script type="text/javascript">
			$(document).ready( function(){ 
				$(".fancy-ajax").fancybox({
					"width"				: 800,
					"height"			: \'750\',	
					\'type\'				: \'iframe\',
					"scrolling" : "no"
				});
			} );
		</script>
	';
?>
<ul class="loffilesmanager">
<li>
<a href="<?php echo $manageFilesURL; ?>" class="fancy-ajax" onclick="return false;">
    <?php echo $this->l( "Manage Download Files" );?>
</a>
</li>

<li>
<a href="<?php echo $previewFilesURL; ?>" class="fancy-ajax" onclick="return false;">
    <?php echo $this->l( "Manage Preview Files" );?>
</a>
</li>

<li>
<a href="<?php echo $iconFilesURL; ?>" class="fancy-ajax" onclick="return false;">
    <?php echo $this->l( "Manage Icon Files" );?>
</a>
</li>
</ul> 
<div class="clear"></div> 