<div class="page-category">
	<h2>{$pageTitle} {if count($files) > 0}<span class="category-product-count">{if count($files) == 1}{l s='There is' mod='lofdownload'}{else}{l s='There are' mod='lofdownload'}{/if} {count($files)} {l s='files' mod='lofdownload'}</span>{/if}</h2>
     
    <div class="download-header">
    	  <h4>{l s='Category' mod='lofdownload'} : {$parent->getName()} </h4>  
          {if $countCategories>0}
    	  <div>{l s='Number of Categories'} : {$countCategories}<em></div>
    	  {/if}	
    </div>
   
    {if is_array($categories)}
   		<div class="category-blocks">
        {foreach from=$categories item=category name=category}
      		<div class="category-item ">
            	<a href="{$helper->getCategoryLink($category.id_lofdcategory)}" title="{$category.name}">
                <img align="left" src="{$helper->getCategoryImage($category.id_lofdcategory)}"></a>
            	<h4><a href="{$helper->getCategoryLink($category.id_lofdcategory)}" title="{$category.name}">{$category.name}</a></h4>
                <span>{l s='Sub-categories' mod='lofdownload'}: <em>{$category.count_sub}</em></span>
                <div class="clearfix"></div>
                <div class="">{$category.description}</div>
            </div>  
            <div class="clearfix"></div>    
        {/foreach}
    	 </div>
    {/if}
   {if is_array($files) && count($files)>0}
     <div class="download-tools">
     	<form action="{$helper->getCategoryLink($parent->id_lofdcategory)}" method="get">
        <div>
            {l s='Sort By' mod='lofdownload'} :
            <select name="sortby" id="sortDownloads">
            	<option value="">---</option>
                <option value="name:asc" {if $sortby=="name:asc"} selected="selected" {/if}>{l s='Download Name: A to Z' mod='lofdownload'}</option>
                <option value="name:desc" {if $sortby=="name:desc"} selected="selected" {/if}>{l s='Download Name: Z to A' mod='lofdownload'}</option>
                <option value="created:desc" {if $sortby=="created:asc"} selected="selected" {/if}>{l s='Created Date First' mod='lofdownload'}</option>
                <option value="download:desc" {if $sortby=="download:asc"} selected="selected" {/if}>{l s='Top Download First' mod='lofdownload'}</option>
            </select>
            <script type="text/javascript">
			
				$('#sortDownloads').change(function()
					{literal} { {/literal}
					var requestSortProducts = '{$helper->getCategoryLink($parent->id_lofdcategory)}';
					var splitData = $(this).val().split(':');
					document.location.href = requestSortProducts + ((requestSortProducts.indexOf('?') < 0) ? '?' : '&') + 'orderby=' + splitData[0] + '&orderway=' + splitData[1];
				{literal} });  {/literal}
				
			</script>
        </div>
        </form>
     </div>
 	  <div class="category-files">
      	{foreach from=$files item=file name=file key=i}
        	<div class="file-item clearfix clear {if $i%2==0}even{else}odd{/if}">
            	<div  class="clearfix">
                    <div class="file-title">
						{if {$helper->getIconURL($file.icon_file, $file.file_type)}}
                    	<img src="{$helper->getIconURL($file.icon_file, $file.file_type)}" title="{$file.name}" class="file-image" align="left" />
						{/if}
                    	<h4>                 	
                    		<a href="{$helper->getViewDownloadLink($file.id_lofdownload)}">{$file.name}</a>
                             {if $file.download > $HOTHIT_NUMBER}       
                                <span class="file-icon icon-hot"></span>
                            {/if}
                            {if $file.newdays <= $NEW_DAY}
                           		<span class="file-icon icon-new"></span>
                            {elseif $file.updatedays <= $UPDATE_DAY}
                        	    <span class="file-icon icon-update"></span>
                        	{/if}
                             <span class="custom-icon">{$helper->getCustomIcon($file.icon_file)}</span>
                        </h4>
                          <div class="file-vote">
                    		{$helper->renderRating( $file.id_lofdownload,  $file.rating_sum, $file.rating_count )}
                    	   </div>
                    
                   	</div>
                  
                    <div class="file-download"><a class="button-img but-silver" href="{$helper->getDownloadLink($file.id_lofdownload)}"> <span class="icon icon-download"> </span>  <span>{l s='Download' mod='lofdownload'}</span></a></div>
                </div>
                <div class="clearfix">
                	<div class="file-description">{$file.description}</div>
                    <div class="file-meta">
                    	{if !empty($file.preview_file)}
                    	<div><span>{l s='Preview' mod='lofdownload'} : </span> {$helper->getPreviewTag($file.preview_file)}</div>
                        {/if}
                    	<div>{l s='Created' mod='lofdownload'} : <span>{$file.date_add}</span></div>
                        <div>{l s='File Size' mod='lofdownload'} : <span>{$file.filesize}</span> <span>{$file.filesize_dv}</span></div>
                        <div class="count-download">{l s='Downloads' mod='lofdownload'} : <span>{$file.download}</span></div>
                    </div>
                </div>    
            </div>
        {/foreach}
      </div>
	<!-- Paging -->
	<div id="pagination" class="pagination">
		{if $nbpagination < $allfiles|@count}
			<ul class="pagination">
			{if $page != 1}
				{assign var='p_previous' value=$page-1}
				<li id="pagination_previous"><a href="{$pagination_link}&p={$p_previous}&n={$nbpagination}">
				&laquo;&nbsp;{l s='Previous' mod='lofdownload'}</a></li>
			{else}
				<li id="pagination_previous" class="disabled"><span>&laquo;&nbsp;{l s='Previous' mod='lofdownload'}</span></li>
			{/if}
			{if $page > 2}
				<li><a href="{$pagination_link}&p=1&n={$nbpagination}">1</a></li>
				{if $page > 3}
					<li class="truncate">...</li>
				{/if}
			{/if}
			{section name=pagination start=$page-1 loop=$page+2 step=1}
				{if $page == $smarty.section.pagination.index}
					<li class="current"><span>{$page|escape:'htmlall':'UTF-8'}</span></li>
				{elseif $smarty.section.pagination.index > 0 && $allfiles|@count+$nbpagination > ($smarty.section.pagination.index)*($nbpagination)}
					<li><a href="{$pagination_link}&p={$smarty.section.pagination.index}&n={$nbpagination}">{$smarty.section.pagination.index|escape:'htmlall':'UTF-8'}</a></li>
				{/if}
			{/section}
			{if $max_page-$page > 1}
				{if $max_page-$page > 2}
					<li class="truncate">...</li>
				{/if}
				<li><a href="{$pagination_link}&p={$max_page}&n={$nbpagination}">{$max_page}</a></li>
			{/if}
			{if $allfiles|@count > $page * $nbpagination}
				{assign var='p_next' value=$page+1}
				<li id="pagination_next"><a href="{$pagination_link}&p={$p_next}&n={$nbpagination}">{l s='Next' mod='lofdownload'}&nbsp;&raquo;</a></li>
			{else}
				<li id="pagination_next" class="disabled"><span>{l s='Next' mod='lofdownload'}&nbsp;&raquo;</span></li>
			{/if}
			</ul>
		{/if}
		{if $allfiles|@count > 30}
			<form action="{$pagination_link}" method="get" class="pagination">
				<p>
					<input type="submit" class="button_mini" value="{l s='OK'}" />
					<label for="nb_item">{l s='items:' mod='lofdownload'}</label>
					<select name="n" id="nb_item">
					{foreach from=$nArray item=nValue}
						{if $nValue <= $allfiles|@count}
							<option value="{$nValue|escape:'htmlall':'UTF-8'}" {if $nbpagination == $nValue}selected="selected"{/if}>{$nValue|escape:'htmlall':'UTF-8'}</option>
						{/if}
					{/foreach}
					</select>
					<input type="hidden" name="p" value="1" />
				</p>
			</form>
		{/if}
	</div>

   {else}
   	{if $id_category>1&& $countCategories==0}
    	{if $files==false} 
      		 <div class="message-notice">{l s='We are updating data for this category' mod='lofdownload'}</div>
        {else}
   		    <div class="message-error">{l s='You have not permission to access this category' mod='lofdownload'}</div>	
        {/if}
    {/if}
   {/if}
   	{if $parent->id_lofdcategory>1}
    <div><a href="{$helper->getCategoryLink($parent->id_parent)}">{l s='Back To Parent Category' mod='lofdownload'}</a></div>
    {/if}
</div>