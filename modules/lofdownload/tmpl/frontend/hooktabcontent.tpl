{if count($files)>0}
<div class="lofdownload-productfiles">
	<fieldset>
    	<legend>{l s='Downloads' mod='lofdownload'}</legend>
    {foreach from=$files item=file name=file}
    	<div class="file-item clearfix">
       		 <div class="file-img">
            	<img width="24" src="{$helper->getIconURL($file.icon_file, $file.file_type)}" title="{$file.name}" class="file-image" align="left" />
        		<a href="{$helper->getDownloadLink($file.id_lofdownload)}"><b>{$file.name}</b></a>
                {if $file.download > 100} <span class="file-icon icon-hot"></span> {/if}
             </div> 
             <div class="file-meta">
             <span>{l s='File Size' mod='lofdownload'} : <span>{$helper->format_size($file.filesize)}</span></span>, 	
             <span class="count-download">{l s='Downloads' mod='lofdownload'}: <span>{$file.download}</span></span>  
             {if !empty($file.preview_file)}
              , <span>{l s='Preview' mod='lofdownload'}: {$helper->getPreviewTag($file.preview_file)} </span> 
             {/if}
            </div>      
        </div>
    
    {/foreach}
    </fieldset>
</div>
{/if}