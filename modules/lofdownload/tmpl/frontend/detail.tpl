<div class="page-download file-item">
	<h2>{$pageTitle}</h2>
    <h3>{l s='View Download Detail' mod='lofdownload'}</h3>
     {if $error && !empty($error)}
     	 <div class="message-error">{l s='The file has problem, So you could not download it' mod='lofdownload'} <br />
       		 <a href="{$link->getPageLink('contact-form.php', true)}">{l s='Please Inform us about this problem' mod='lofdownload'}</a>
         </div>	
      {/if}
   {if $checkAccess===false}
   		{if $fileError===false}
    		<div class="message-error">{l s='You have not permission to access this category' mod='lofdownload'}</div>	
        {/if}
   {else}		     
      {if $avariable==true}
         <div class="download-header">
              <img src="{$helper->getIconURL($file.icon_file, $file.file_type)}" title="{$file.name}" class="file-image" align="left" />
        	 <h4>{$file.name} </h4> 
             {if $file.download > $HOTHIT_NUMBER}<span class="file-icon icon-hot"></span>{/if}
             {if $file.newdays<=$NEW_DAY}
                <span class="file-icon icon-new"></span>
              {elseif $file.updatedays<=$UPDATE_DAY}
                <span class="file-icon icon-update"></span>
              {/if}              
              <span class="custom-icon">{$helper->getCustomIcon($file.icon_file)}</span>
             - <span>{l s='Version' mod='lofdownload'} : <span>{$file.version}</span></span>
             
             <div class="file-vote">{$helper->renderRating( $file.id_lofdownload,  $file.rating_sum, $file.rating_count )}</div> 
          </div>    
      {/if}      
        <div class="clear"></div>
        <div class="download-wrapper">
            {if $avariable==true}
            <div class="download-information">
                {$file.information}
            </div>
            <div class="download-meta-wrapper">
                <h4>{l s='Meta Information' mod='lofdownload'}</h4>
                <div class="download-meta">
                	{if !empty($file.preview_file)}
             		 <div>- {l s='Preview' mod='lofdownload'}:  <span>{$helper->getPreviewTag($file.preview_file)} </span></div>
             		{/if}
             
                    <div>- {l s='File Name' mod='lofdownload'} : <span>{$file.filename}</span></div>
                    <div>- {l s='File Size' mod='lofdownload'} : <span>{$file.filesize}</span> <span>{$file.filesize_dv}</span></div>
                    <div class="count-download">- {l s='Downloads' mod='lofdownload'} : <span>{$file.download}</span></div>
                    <div>- {l s='Created' mod='lofdownload'} : <span>{$file.date_add}</span></div>
                    <div>- {l s='Updated' mod='lofdownload'} : <span>{$file.date_upd}</span></div>
                </div>
                <div class="download-button"><a class="button-img but-silver" href="{$helper->getDownloadLink($file.id_lofdownload)}"> <span class="icon icon-download"> </span> <span>{l s='Download' mod='lofdownload'}</span></a></div>
             </div>   
            {else}
                <div class="download-message">{l s='File is not avariable for downloading'}</div>
            {/if}
        </div>
        <div class="clearfix"></div>
        <div><a href="{$helper->getCategoryLink($file.id_lofdcategory)}">{l s='Back To Category' mod='lofdownload'}</a></div>
        {/if}
</div>