<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
session_start();
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
require_once(_PS_ROOT_DIR_.'/images.inc.php');
global $cookie;
include(dirname(__FILE__).'/libs/defined.php');

?>
<?php if( isset($_GET['file']) && $file=trim($_GET['file']) ){
		$url =  __PS_BASE_URI__._LOFDOWNLOAD_DIR_PREVIEWS_FOLDER_."/".str_replace("\\","/",$file);
		preg_match("#\w{3}$#",$file,$match);
		if( isset($match[0]) && in_array($match[0],array("flv","mp3","mp4","dat")) ){ ?>
<script type="text/javascript" src="<?php echo __PS_BASE_URI__;?>modules/<?php echo _LOFDOWNLOAD_MODULE_NAME_;?>/assets/jwplayer/jwplayer.js"></script>
<div class="preview-container">
	<div id="container" style="margin:0 auto">Loading the player ...</div>
		<script type="text/javascript">
            jwplayer("container").setup({
            flashplayer: "<?php echo __PS_BASE_URI__;?>modules/<?php echo _LOFDOWNLOAD_MODULE_NAME_;?>/assets/jwplayer/player.swf",
            file: "<?php echo $url;?>",
            height: 300,
            width: 530,
			controlbar: 'bottom',
			autostart:true
            });
        </script>
</div>
	<?php } else if( isset($match[0]) && $match[0] == "pdf" ){ ?> 
		 <iframe src="<?php echo $url;?>" style="width:1000px; height:100%"></iframe>
	<?php } else { $error = true; } ?>
    
<?php }  else { $error = true; }  ?>
<?php if( isset($error) ) { ?>
<div class="preview-container">
	<div class="message-error">Sorry, the file is not supported or not available</div>
</div>
<?php } ?>
 