<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads, 
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com> 
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
define('PS_ADMIN_DIR',(dirname(  getcwd() ))); 
include(PS_ADMIN_DIR.'/../config/config.inc.php');
/* Getting cookie or logout */  
if(  Tools::getValue('baseURI') ){
	$_SERVER['SCRIPT_NAME'] = 	Tools::getValue('baseURI')."/index.php";
}
require( dirname(__FILE__)."/libs/defined.php" );
require_once(dirname(__FILE__).'/init.php');
require_once( dirname(__FILE__).'/classes/LofDownloadHelper.php' );
require_once( dirname(__FILE__).'/classes/LofClassDownload.php' );

 
if( $id = Tools::getValue("id_download") ){
	  clearstatcache(); 	
	$download = new LofClassDownload( $id );

	if( is_object($download) && $download->id == $id ){
		$path = _LOFDOWNLOAD_DIR_UPLOAD_.$download->file_path;
		if( is_file($path) && file_exists($path) ){
			ob_end_clean();
			if (ini_get('zlib.output_compression')){
				ini_set('zlib.output_compression', 'Off');
			}
			$download->download = $download->download+1;
			$download->update();
			
			$filesize	  = filesize( $path );
			$filename = basename( $path );
		 	
			preg_match( "#\w{3}$#", $path, $match );
			$contentType = ( isset($match[0]) ? LofDownloadHelper::getMineType($match[0]): "unknown" ); 
		 
			header("Cache-Control: public, must-revalidate");
			header('Cache-Control: pre-check=0, post-check=0, max-age=0');
			header("Content-Description: File Transfer");
			header('Pragma: public');
			header('Expires: 0'); 
			header('Cache-Contro:must-revalidate, post-check=0, pre-check=0');
			header('Content-Type:'. $contentType);
			header('Content-Disposition:attachment; filename="'.$filename.'";');
			header("Content-Transfer-Encoding: binary\n");
			header('Content-Length:'.(string) ( $filesize) ); 
			echo LofDownloadHelper::readFile( $path );
			if (!ini_get('safe_mode')){ 
				@set_time_limit(0);
			}

			exit;
		}
	} 
	header("location:" . __PS_BASE_URI__."modules/"._LOFDOWNLOAD_MODULE_NAME_."/view.php?id_download=".$id."&error=1" ); 
}
die( "No File Exists" );	
?>