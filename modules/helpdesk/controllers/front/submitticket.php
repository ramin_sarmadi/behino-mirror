<?php


/**
 * @since 1.5.0
 */
class HelpDeskSubmitticketModuleFrontController extends ModuleFrontController
{
	public $display_column_left = true;
	public $errors;

	public function __construct()
	{
		parent::__construct();

		$this->context = Context::getContext();
		$link = new Link();
		if(!$this->context->cookie->logged)
			Tools::redirect($link->getPageLink('authentication'));
			
		require_once( $this->module->getLocalPath().'/../../config/config.inc.php');
		require_once( $this->module->getLocalPath().'/../../init.php');
		require_once( $this->module->getLocalPath().'/defines.helpdesk.php');
		require_once( $this->module->getLocalPath().'/helpdesk.php');
		require_once( $this->module->getLocalPath().'/libs/helper.php' );
		require_once( $this->module->getLocalPath().'/classes/HdConfiguration.php' );
		require_once( $this->module->getLocalPath().'/classes/HdCustomFields.php' );
		require_once( $this->module->getLocalPath().'/classes/HdDepartment.php' );
		require_once( $this->module->getLocalPath().'/classes/HdEmailTemplate.php' );
		require_once( $this->module->getLocalPath().'/classes/HDMail.php' );
		require_once( $this->module->getLocalPath().'/classes/HdParams.php' );
		require_once( $this->module->getLocalPath().'/classes/HdPriority.php' );
		require_once( $this->module->getLocalPath().'/classes/HdStaff.php' );
		require_once( $this->module->getLocalPath().'/classes/Hdstatus.php' );
		require_once( $this->module->getLocalPath().'/classes/HdTicket.php' );
		require_once( $this->module->getLocalPath().'/classes/HdTicketFile.php' );
		require_once( $this->module->getLocalPath().'/classes/HdTicketMessage.php' );
		require_once( $this->module->getLocalPath().'/libs/securimage/securimage.php' );
		
	}

	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess()
	{
		if (Tools::isSubmit('submitTicket'))
			$this->processSubmitForm();
	}

	/**
	 * Transform loyalty point to a voucher
	 */
	public function processSubmitForm()
	{
		global $lof;
		$cookie = $this->context->cookie;
		$params = new HdParams(LOF_MODULE_NAME_HELPDESK);
		if($params->get('captcha_enabled')){
			$captcha = Tools::getValue('captcha');
			$securimage = new Securimage();
			if ($securimage->check($captcha) == false) {
				$this->errors[''] = $this->module->l('Please make sure you have entered the correct code from the image.');
			}
		}
		$id_customer = (Tools::getValue('id_customer') ? Tools::getValue('id_customer') : $cookie->id_customer);
		$id_hd_department = Tools::getValue('id_hd_department');
		$id_hd_priority = Tools::getValue('id_hd_priority');
		$message = Tools::getValue('message');
		$subject = Tools::getValue('subject');
		$files = @$_FILES['lof_files'];
		$customFieldsValue = array();
		if(!$id_hd_department)
			$this->errors[] = $this->module->l('Department is required');
		else{
			$customfields = HdCustomFields::listFields($cookie->id_lang,true,$id_hd_department);
			foreach($customfields as $cusField){
				if(Validate::isLoadedObject($objcus = new HdCustomFields($cusField['id_hd_custom_field'],$cookie->id_lang))){
					if($objcus->type != 'freetext'){
						$cusValue = Tools::getValue($objcus->name);
						if($objcus->required && !$cusValue){
							$this->errors[] = Tools::displayError($objcus->title.' is required');
						}
						if(!$cusValue && !Validate::isCleanHtml($cusValue)){
							$this->errors[] = Tools::displayError($objcus->title.' is not validate');
						}
						$customFieldsValue[] = array('value' => $cusValue,'id_hd_custom_field'=> $objcus->id);
					}
				}
			}
		}
		if(!$id_hd_priority)
			$this->errors[] = $this->module->l('Priority is required');
		if(!$message)
			$this->errors[] = $this->module->l('Message is required');
		if(!$subject)
			$this->errors[] = $this->module->l('Subject is required');
		$helper = new HelpDeskHelper();
		$objDepart = new HdDepartment($id_hd_department);
		if(is_array($files))
			if(!$correct_files = $helper->checkFileUpload($files, $objDepart)){
				if (sizeof($helper->errors)) {
					foreach($helper->errors as $row){
						$this->errors[] = $row;
					}
				}
			}
		$langs = Language::getLanguages(false);
		$arrSubjects = array();
		foreach($langs as $lang){
			$arrSubjects[$lang['id_lang']] = $subject;
		}
		if (!sizeof($this->errors)) {
			$object = new HdTicket();
			$object->id_customer = $id_customer;
			$object->id_hd_department = $id_hd_department;
			$object->id_hd_priority = $id_hd_priority;
			$object->subject = $arrSubjects;
			$object->id_hd_staff = HelpDeskHelper::assignStaff($objDepart);
			$type = ($objDepart->generation_rule ? 'random' : 'sequential');
			$object->code = HelpDeskHelper::generateCode($objDepart, 5, $type);
			$object->id_order = Tools::getValue('id_order',0);
			$object->id_product = Tools::getValue('id_product',0);
			
			if($object->add()){
				if($customFieldsValue)
					foreach($customFieldsValue as $v){
						if(is_array($v['value']))
							$value = implode('::',$v['value']);
						else $value = $v['value'];
						HdCustomFields::deteleCustomFieldValue($v['id_hd_custom_field'], $object->id );
						HdCustomFields::addCustomFieldValue($v['id_hd_custom_field'], $object->id ,$value);
					}
				$objMessage = new HdTicketMessage();
				$objMessage->message = Tools::htmlentitiesUTF8($message);
				$objMessage->id_hd_ticket = $object->id;
				$objMessage->id_customer = $id_customer;
				
				if(!$objMessage->add()){
					$this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>hd_ticket_messages</b>';
				}else{
					$result = true;
					if(isset($correct_files) && $correct_files){
						$result = $helper->addAttachments($object->id, $objMessage->id , $objDepart, $correct_files);
					}
					if(!$result){
						$this->errors[] = Tools::displayError('can\'t upload file.').'</b>';
					}else{
						/* START SEND EMAIL */
						/* customer */
						$objCus = new Customer($object->id_customer);
						$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_TICKET, $cookie->id_lang);
						$subject = $mailTMPL->subject;
						if(in_array(LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_TICKET,$lof['skip_edit_subject'])){
							$subject = $object->subject[$cookie->id_lang];
						}
						HelpDeskHelper::sendMail( array($objCus->email), array($objCus->firstname.' '.$objCus->lastname), $subject, $mailTMPL->message, $objMessage->id, 'message');
						/* staff */
						if($objDepart->notify_assign && $object->id_hd_staff){
							$objStaff = new HdStaff($object->id_hd_staff);
							$objEmp = new Employee($objStaff->id_employee);
							$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_TO_STAFF_WHEN_ASSIGNED,$cookie->id_lang);
							$subject = $mailTMPL->subject;
							if(in_array(LOF_EMAILS_SEND_TO_STAFF_WHEN_ASSIGNED,$lof['skip_edit_subject'])){
								$subject = $object->subject[$cookie->id_lang];
							}
							HelpDeskHelper::sendMail( array($objEmp->email), array($objEmp->firstname.' '.$objEmp->lastname), $subject, $mailTMPL->message, $objMessage->id, 'attach');
						}
						/* department notify section */
						if($objDepart->notify_new_tickets_to){
							$emails = str_replace("\r\n", "\n", $objDepart->notify_new_tickets_to);
							$toNotify = explode("\n", $emails);
							$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_TO_DEPARTMENT_NOTIFY_SECTION,$cookie->id_lang);
							$subject = $mailTMPL->subject;
							if(in_array(LOF_EMAILS_SEND_TO_DEPARTMENT_NOTIFY_SECTION,$lof['skip_edit_subject'])){
								$subject = $object->subject[$cookie->id_lang];
							}
							$arremails = array();
							foreach($toNotify as $t){
								if(Validate::isEmail($t)){
									$arremails[] = $t;
								}
							}
							if($arremails)
								HelpDeskHelper::sendMail( $toNotify, array(), $subject, $mailTMPL->message, $objMessage->id, 'attach');
						}
						/* Notify keywords */
						$params = new HdParams('');
						if($params->get('notice_email_address')){
							$to = explode(',',$params->get('notice_email_address'));
							$emails = array();
							foreach($to as $t){
								if(Validate::isEmail($t)){
									$emails[] = $t;
								}
							}
							if($emails){
								if($params->get('notice_not_allowed_keywords')){
									$keywords = explode(',',$params->get('notice_not_allowed_keywords'));
									$a = str_replace($keywords,' ', $objMessage->message,$count);
									if($count){
										$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_WHEN_TICKET_CONTAIN_KEYWORDS,$cookie->id_lang);
										$subject = $mailTMPL->subject;
										if(in_array(LOF_EMAILS_SEND_NOTIFICATION_WHEN_TICKET_CONTAIN_KEYWORDS,$lof['skip_edit_subject'])){
											$subject = $object->subject[$cookie->id_lang];
										}
										HelpDeskHelper::sendMail( $emails, array(), $subject, $mailTMPL->message, $objMessage->id, 'other');
									}
								}
								if($params->get('notice_max_replies_nr') && Validate::isInt($params->get('notice_max_replies_nr')) && $params->get('notice_max_replies_nr') == 1 && !$object->id_hd_staff){
									$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_UNASSIGNED,$cookie->id_lang);
									$subject = $mailTMPL->subject;
									if(in_array(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_UNASSIGNED,$lof['skip_edit_subject'])){
										$subject = $object->subject[$cookie->id_lang];
									}
									HelpDeskHelper::sendMail( $emails, array(), $subject, $mailTMPL->message, $objMessage->id, 'other');
								}
								if($params->get('notice_replies_with_no_response_nr') && Validate::isInt($params->get('notice_replies_with_no_response_nr')) && $params->get('notice_replies_with_no_response_nr') == 1 && $object->id_hd_staff){
									$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_ASSIGNED,$cookie->id_lang);
									$subject = $mailTMPL->subject;
									if(in_array(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_ASSIGNED,$lof['skip_edit_subject'])){
										$subject = $object->subject[$cookie->id_lang];
									}
									HelpDeskHelper::sendMail( $emails, array(), $subject, $mailTMPL->message, $objMessage->id, 'other');
								}
							}
						}
						/* END SEND EMAIL */
					}
				}
			}
		}
		if($params->get('submit_redirect') && !sizeof($this->errors)){
			Tools::redirect('index.php?process=tickets&fc=module&module=helpdesk&controller=tickets');
		}
	}
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent() {
		parent::initContent();
		$this->assignFormSubmit();
	}
	/**
	 * Assign summary template
	 */
	public function assignFormSubmit() {
		$params = new HdParams(LOF_MODULE_NAME_HELPDESK);
		$this->context->controller->addJS(array(_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/assets/formtickets.js'));
		$this->context->controller->addCSS(__PS_BASE_URI__.'js/jquery/datepicker/datepicker.css');
		$cookie = $this->context->cookie;
		
		$ticket_url = 'index.php?process=ticket&fc=module&module=helpdesk&controller=submitticket';
		$customer = new Customer($cookie->id_customer);
		$departments = HdDepartment::getDepartments($cookie->id_lang);
		$priorities = HdPriority::getPriorities($cookie->id_lang);
		$objdepartments = array();

		if($departments)
			foreach($departments as &$department){
				if(Validate::isLoadedObject($objdepartment = new HdDepartment($department['id_hd_department'],$cookie->id_lang))){
					$objdepartments[] = $objdepartment;
				}
				$arrCusFields = array();
				$customfields = HdCustomFields::listFields($cookie->id_lang,true,$department['id_hd_department']);
				foreach($customfields as $cusField){
					if(Validate::isLoadedObject($objcus = new HdCustomFields($cusField['id_hd_custom_field'],$cookie->id_lang))){
						$objcus->lofValues = '';
						$objcus->renderFrom = HelpDeskHelper::renderField($objcus);
						$arrCusFields[] = $objcus;
					}
				}
				$department['customfields'] = $arrCusFields;
			}
		
			$orders = Order::getCustomerOrders($cookie->id_customer);
		$this->context->smarty->assign(array(
			'customer' => $customer,
			'ticket_url' => $ticket_url,
			'departments' => $departments,
			'priorities' => $priorities,
			'orders' => $orders,
			'errors' => $this->errors,
			'cookie' => $cookie,
			'captcha_enabled' => $params->get('captcha_enabled'),
			'captcha' => _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/libs/securimage/securimage_show.php?rand='.rand(10,1000),
			'url_captcha' => _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/libs/securimage/',
			'use_editor' => $params->get('allow_rich_editor'),
			'message_error' => $this->module->l('The maximum number of attachments has been reached. You cannot add any more attachments.'),
			'ajaxUrl' => _MODULE_DIR_.'helpdesk/lofajax.php',
			'objdepartments' => $objdepartments,
		));
		$this->setTemplate('submit-ticket.tpl');
	}
	
	public function getTemplatePath(){
		return _PS_MODULE_DIR_.$this->module->name.'/tmpl/frontend/';
	}
}
