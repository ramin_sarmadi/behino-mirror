<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 17015 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
class HelpdeskTicketsModuleFrontController extends ModuleFrontController
{
	public $errors;
	public $expandForm = false;

	public function __construct()
	{
		parent::__construct();
		
		$this->context = Context::getContext();
		$link = new Link();
		if(!$this->context->cookie->logged)
			Tools::redirect($link->getPageLink('authentication'));
			
		require_once($this->module->getLocalPath().'/../../config/config.inc.php');
		require_once($this->module->getLocalPath().'/../../init.php');
		require_once($this->module->getLocalPath().'/defines.helpdesk.php');
		require_once($this->module->getLocalPath().'/helpdesk.php');
		require_once( $this->module->getLocalPath().'/libs/helper.php' );
		require_once( $this->module->getLocalPath().'/classes/HdConfiguration.php' );
		require_once( $this->module->getLocalPath().'/classes/HdCustomFields.php' );
		require_once( $this->module->getLocalPath().'/classes/HdDepartment.php' );
		require_once( $this->module->getLocalPath().'/classes/HdEmailTemplate.php' );
		require_once( $this->module->getLocalPath().'/classes/HDMail.php' );
		require_once( $this->module->getLocalPath().'/classes/HdParams.php' );
		require_once( $this->module->getLocalPath().'/classes/HdPriority.php' );
		require_once( $this->module->getLocalPath().'/classes/HdStaff.php' );
		require_once( $this->module->getLocalPath().'/classes/Hdstatus.php' );
		require_once( $this->module->getLocalPath().'/classes/HdTicket.php' );
		require_once( $this->module->getLocalPath().'/classes/HdTicketFile.php' );
		require_once( $this->module->getLocalPath().'/classes/HdTicketMessage.php' );

		// Declare smarty function to render pagination link
		smartyRegisterFunction($this->context->smarty, 'function', 'summarypaginationlink', array('LoyaltyDefaultModuleFrontController', 'getSummaryPaginationLink'));
	}

	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess() {
		if (Tools::isSubmit('submitMessage'))
			$this->submitMessage();
	}
	/**
	 * Transform loyalty point to a voucher
	 */
	public function submitMessage()
	{
		$cookie = $this->context->cookie;
		$message = Tools::getValue('message');
		$helper = new HelpDeskHelper();
		$files = @$_FILES['lof_files'];
		if(!Tools::getValue('id_hd_ticket'))
			$this->errors[] = $helpdesk->l('id_ticket is empty');
		if(!Tools::getValue('message'))
			$this->errors[] = $helpdesk->l('message is empty');
		if (!sizeof($this->errors)) {
			$id_hd_ticket = (int)Tools::getValue('id_hd_ticket');
			$objTicket = new HdTicket($id_hd_ticket);	
			$id_hd_department = $objTicket->id_hd_department;
			$objDepart = new HdDepartment($id_hd_department);
			if(is_array($files))
				if(!$correct_files = $helper->checkFileUpload($files, $objDepart)){
					if (sizeof($helper->errors)) {
						foreach($helper->errors as $row){
							$this->errors[] = $row;
						}
					}
				}
		}
		$langs = Language::getLanguages(false);
		if (!sizeof($this->errors)) {
			$obj = new HdTicketMessage();
			$obj->message = Tools::htmlentitiesUTF8($message);
			$obj->id_hd_ticket = Tools::getValue('id_hd_ticket');
			$obj->id_customer = $cookie->id_customer;
			$obj->active = 1;
			if($obj->add()){
				$result = true;
				if(isset($correct_files) && $correct_files)
					$result = $helper->addAttachments($obj->id_hd_ticket,$obj->id,$objDepart, $correct_files);
				
				if(!$result){
					$this->errors[] = $helpdesk->l('Can not upload files.');
				}else{
					/* START SEND MAIL */
					/* send to staff */
					if($objTicket->id_hd_staff && $objDepart->notify_assign){
						$objStaff = new HdStaff($objTicket->id_hd_staff);
						$objEmp = new Employee($objStaff->id_employee);
						$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_TO_STAFF_ADD_NEW_REPLY, $cookie->id_lang);
						$subject = $mailTMPL->subject;
						if(in_array(LOF_EMAILS_SEND_TO_STAFF_ADD_NEW_REPLY,$lof['skip_edit_subject'])){
							$subject = $objTicket->subject[$cookie->id_lang];
						}
						HelpDeskHelper::sendMail( array($objEmp->email), array($objEmp->firstname.' '.$objEmp->lastname), $subject, $mailTMPL->message, $obj->id, 'message');
					}
					/* Notify keywords */
					$messages = HdTicketMessage::getTicketMessages($obj->id_hd_ticket);
					$params = new HdParams('');
					if($params->get('notice_email_address')){
						$to = explode(',',$params->get('notice_email_address'));
						$emails = array();
						foreach($to as $t){
							if(Validate::isEmail($t)){
								$emails[] = $t;
							}
						}
						if($emails){
							if($params->get('notice_not_allowed_keywords')){
								$keywords = explode(',',$params->get('notice_not_allowed_keywords'));
								$a = str_replace($keywords,' ', $obj->message, $count);
								if($count){
									$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_WHEN_TICKET_CONTAIN_KEYWORDS,$cookie->id_lang);
									$subject = $mailTMPL->subject;
									if(in_array(LOF_EMAILS_SEND_NOTIFICATION_WHEN_TICKET_CONTAIN_KEYWORDS,$lof['skip_edit_subject'])){
										$subject = $objTicket->subject[$cookie->id_lang];
									}
									HelpDeskHelper::sendMail( $emails, array(), $subject, $mailTMPL->message, $obj->id, 'other');
								}
							}
							if($params->get('notice_max_replies_nr') && Validate::isInt($params->get('notice_max_replies_nr')) && $params->get('notice_max_replies_nr') >= count($messages) && !$object->id_hd_staff){
								$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_UNASSIGNED,$cookie->id_lang);
								$subject = $mailTMPL->subject;
								if(in_array(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_UNASSIGNED,$lof['skip_edit_subject'])){
									$subject = $objTicket->subject[$cookie->id_lang];
								}
								HelpDeskHelper::sendMail( $emails, array(), $subject, $mailTMPL->message, $obj->id, 'other');
							}
							if($params->get('notice_replies_with_no_response_nr') && Validate::isInt($params->get('notice_replies_with_no_response_nr')) && $params->get('notice_replies_with_no_response_nr') >= count($messages) && $object->id_hd_staff){
								$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_ASSIGNED,$cookie->id_lang);
								$subject = $mailTMPL->subject;
								if(in_array(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_ASSIGNED, $lof['skip_edit_subject'])){
									$subject = $objTicket->subject[$cookie->id_lang];
								}
								HelpDeskHelper::sendMail( $emails, array(), $subject, $mailTMPL->message, $obj->id, 'other');
							}
						}
					}
					/* END SEND MAIL */
				}
			}
		}else{
			$this->expandForm = true;
		}
	
	}
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent() {
		parent::initContent();
		
		if (Tools::getValue('task') == 'reopen' || Tools::getValue('task') == 'close')
			$this->executionStatus();
		if(!($id_hd_ticket = Tools::getValue('id_ticket'))){
			$this->viewList();
		}else{
			$this->viewDetail();
		}
	}
	public function executionStatus() {
		$id_hd_ticket = Tools::getValue('id_ticket');
		$cookie = $this->context->cookie;
		$objTicket = new HdTicket($id_hd_ticket);
		if($objTicket->id_customer != $cookie->id_customer)
			Tools::redirect('index.php?process=tickets&fc=module&module=helpdesk&controller=tickets');
		
		if($id_hd_ticket && HdTicket::getTickets($cookie->id_lang, $id_hd_ticket, $cookie->id_customer)){
			if(Validate::isLoadedObject($obj = new HdTicket($id_hd_ticket))){
				$obj->id_hd_status = (Tools::getValue('task') == 'reopen' ? LOF_STATUS_OPEN : LOF_STATUS_CLOSE);
				if(!$obj->update()){
					$objHistory = new HdTicketHistory();
					$objHistory->change_status = $obj->id_hd_status;
					$objHistory->id_hd_ticket = $id_hd_ticket;
					$objHistory->ip = $_SERVER['REMOTE_ADDR'];
					$objHistory->id_customer = $cookie->id_customer;
					$objHistory->add();
					
					$this->errors[] = $helpdesk->l('update ticket is error');
				}else{
					//Tools::redirect('index.php?process=tickets&fc=module&module=helpdesk&controller=tickets&id_ticket='.$id_hd_ticket);
				}
			}else
				$this->errors[] = $helpdesk->l('can not load object ticket');
		}
	}
	/**
	 * Assign summary template
	 */
	public function viewList() {
		$params = new HdParams('');
		$ticket_url = 'index.php?process=tickets&fc=module&module=helpdesk&controller=tickets';
		$cookie = $this->context->cookie;
		
		$orderBy = 'id_hd_ticket';
		$orderWay = $params->get('messages_direction','ASC');
		$page = ((int)(Tools::getValue('p')) > 0 ? (int)(Tools::getValue('p')) : 1);
		$limit = ((int)(Tools::getValue('n')) > 0 ? (int)(Tools::getValue('n')) : 30);
		$tickets = HdTicket::getTickets($cookie->id_lang, false, $cookie->id_customer, false, false, false, false, $orderBy, $orderWay, $limit, $page);
		$alltickets = HdTicket::getTickets($cookie->id_lang, false, $cookie->id_customer, false, false, false, false, $orderBy, $orderWay,false, $page); 

		$this->context->smarty->assign(array(
			'alltickets' => $alltickets,
			'tickets' => $tickets,
			'pagination_link' => $ticket_url,
			'page' => ((int)(Tools::getValue('p')) > 0 ? (int)(Tools::getValue('p')) : 1),
			'nbpagination' => ((int)(Tools::getValue('n') > 0) ? (int)(Tools::getValue('n')) : 30),
			'nArray' => array(30, 50, 100),
			'ticket_url' => $ticket_url,
			'lof_mod_url' => _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK,
			'max_page' => floor(sizeof($alltickets) / ((int)(Tools::getValue('n') > 0) ? (int)(Tools::getValue('n')) : 30))
		));
		$this->setTemplate('tickets.tpl');
	}
	/**
	 * Assign summary template
	 */
	public function viewDetail() {
		$cookie = $this->context->cookie;
		$this->context->controller->addJS(_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/assets/frommessage.js');
		$this->context->controller->addJqueryPlugin('fancybox');
		$params = new HdParams('');
		$ticket_url = 'index.php?process=tickets&fc=module&module=helpdesk&controller=tickets';
		$id_hd_ticket = Tools::getValue('id_ticket');
		$ticket = HdTicket::getTickets($cookie->id_lang, $id_hd_ticket, $cookie->id_customer);
		if(!$ticket)
			//echo 'error message 1';
			return false;
		else{
			$messages = HdTicketMessage::getTicketMessages($id_hd_ticket, true);
			if($messages){
				foreach($messages as &$mess){
					$mess['files'] = HdTicketMessage::getFiles($mess['id_hd_ticket_message']);
					$mess['objEmployee'] = new Employee($mess['id_employee']);
					$mess['objCustomer'] = new Customer($mess['id_customer']);
				}
				$fields = HdCustomFields::listFields($cookie->id_lang, true, $ticket[0]['id_hd_department']);
				if($fields){
					$this->context->smarty->assign('fields',HelpDeskHelper::renderDisplayFields($fields, $id_hd_ticket));
				}
				if(Validate::isLoadedObject($objDepart = new HdDepartment($ticket[0]['id_hd_department']))){
					echo '<script type="text/javascript">
						var mess_data_init = new Array('.$objDepart->id.','.$objDepart->id_hd_priority.','.$objDepart->upload.','.$objDepart->upload_files.',\''.$objDepart->upload_extensions.'\');
						var nb_file = 0;
						var message_error = "'.$this->module->l('The maximum number of attachments has been reached. You cannot add any more attachments.').'";
					</script>';
				}
				$Topmessage = $messages[(count($messages) - 1)];
				array_pop($messages);
				$this->context->smarty->assign(array(
					'ticket' => $ticket[0],
					'ticket_url' => $ticket_url,
					'lof_mod_url' => _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK,
					'Topmessage' => $Topmessage,
					'messages' => $messages,
					'expandForm' => $this->expandForm,
					'errors' => $this->errors,
					'LOF_STATUS_OPEN' => LOF_STATUS_OPEN,
					'LOF_STATUS_ON_HOLD' => LOF_STATUS_ON_HOLD,
					'LOF_STATUS_CLOSE' => LOF_STATUS_CLOSE,
					'show_submiter' => $params->get('show_ticket_info',1),
					'show_user_info' => $params->get('show_user_info','name'),
					'show_email_link' => $params->get('show_email_link',0),
					'allow_ticket_closing' => $params->get('allow_ticket_closing',1),
					'allow_ticket_reopening' => $params->get('allow_ticket_reopening',1),
					'show_ticket_voting' => $params->get('show_ticket_voting',1),
					'use_editor' => $params->get('allow_rich_editor')
				));
				if (file_exists(_PS_THEME_DIR_.'modules/'.LOF_MODULE_NAME_HELPDESK.'/tmpl/frontend/formmessage.tpl'))
					$this->context->smarty->assign('form_tpl_message', _PS_THEME_DIR_.'modules/'.LOF_MODULE_NAME_HELPDESK.'/tmpl/frontend/formmessage.tpl');
				else
					$this->context->smarty->assign('form_tpl_message', _PS_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/tmpl/frontend/formmessage.tpl');
					
				$this->setTemplate('detail-ticket.tpl');
			}else{
				//echo 'error message 2';
				return false;
			}
		}
	}
	/**
	 * Render pagination link for summary
	 *
	 * @param (array) $params Array with to parameters p (for page number) and n (for nb of items per page)
	 * @return string link
	 */
	public static function getSummaryPaginationLink($params, &$smarty) {
		if (!isset($params['p']))
			$p = 1;
		else
			$p = $params['p'];

		if (!isset($params['n']))
			$n = 10;
		else
			$n = $params['n'];

		return Context::getContext()->link->getModuleLink(
			'loyalty',
			'default',
			array(
				'process' => 'summary',
				'p' => $p,
				'n' => $n,
			)
		);
	}
	
	public function getTemplatePath(){
		return _PS_MODULE_DIR_.$this->module->name.'/tmpl/frontend/';
	}
}
