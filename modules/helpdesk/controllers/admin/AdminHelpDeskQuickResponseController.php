<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskQuickResponseController extends AdminHDController {
	
	public function __construct() {
	  	global $lof;
	 	$this->table 	 = "hd_quickresponse";
 		$this->className = 'HdQuickResponse';
	 	$this->addRowAction('edit');
		$this->addRowAction('delete');
		
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		$this->fields_list = array(
			'id_hd_quickresponse' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'title' => array('title' => $this->l('Title'), 'align' => 'left', 'width' => 325) ,
			'active' => array('title' => $this->l('Displayed'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false)
		);

		return parent::__construct();
	}
	
	public function renderForm() {
		parent::renderForm();	
		if (!($obj = $this->loadObject(true)))
			return;
		$agrs = array("obj"=>$obj, 'l' => $this->translateLanguages());
		return $this->renderLayout("admin/quickresponseform", $agrs );
	}
	
	public function translateLanguages(){
		return array(
			'Quick Response' => $this->l('Quick Response'),
			'Title' => $this->l('Title'),
			'Enable' => $this->l('Enable'),
			'Enabled' => $this->l('Enabled'),
			'Disabled' => $this->l('Disabled'),
			'Content' => $this->l('Content'),
			'Save' => $this->l('Save'),
			'Required field' => $this->l('Required field')
		);
	}
	
}

