<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskEmailTemplateController extends AdminHDController {
	public function __construct() {
	  	
	 	$this->table 	 = "hd_email";
 		$this->className = 'HdEmailTemplate';
		$this->lang 	 = true;
	 	$this->addRowAction('edit');
		
		$this->fields_list = array(
			'id_hd_email' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 5),
			'type' => array('title' => $this->l('Email Title'), 'align' => 'left', 'width' => 100) ,
			'subject' => array('title' => $this->l('Email Subject'), 'align' => 'left', 'width' => 150) 
			);
		
		return parent::__construct();
	}
	
	public function renderList()
	{
		$this->toolbar_btn = array();
		return parent::renderList();
	}
	
	public function renderForm($isMainTab = true) {
		global $lof;
		parent::renderForm();	
		if (!($obj = $this->loadObject(true)))
			return;
			
		$agrs = array("obj"=>$obj, 'skip_edit_subject' => $lof['skip_edit_subject'], 'l' => $this->translateLanguages());
		return $this->renderLayout("admin/emailform", $agrs );
	}
	
	
	public function postProcess($token = NULL) {
		global $currentIndex;
		$cookie = $this->context->cookie;
		$this->tabAccess = Profile::getProfileAccess($cookie->profile, $this->id);

		if (Tools::isSubmit('submitAdd'.$this->table) OR Tools::isSubmit('submitAdd'.$this->table.'AndOther') OR Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
		{
			$id = (int)(Tools::getValue($this->identifier));
			if (isset($id) AND $id){
				if ($this->tabAccess['edit'] === '1'){
					$this->validateRules();
					if (!sizeof($this->errors)) {
						$object = new $this->className($id);
						if (Validate::isLoadedObject($object)){
							$this->copyFromPost($object, $this->table);
							$result = $object->update();
							if (!$result){
								$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b>';
							}else{
								Tools::redirectAdmin($currentIndex.'&conf=3&token='.Tools::getValue('token'));
							}
						}else
							$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
					}	
				}else
					$this->errors[] = Tools::displayError('You do not have permission to edit here.');
			}else{
				if ($this->tabAccess['add'] === '1'){
					$this->validateRules();
					if (!sizeof($this->errors)) {
						$object = new $this->className();
						$this->copyFromPost($object, $this->table);
						if (!$object->add()){
							$this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.'</b>';
						}else{
							Tools::redirectAdmin($currentIndex.'&conf=3&token='.Tools::getValue('token'));
						}
					}
				}else
					$this->errors[] = Tools::displayError('You do not have permission to add here.');
			}
		
			$this->errors = array_unique($this->errors);
		}
		parent::postProcess();
	}
	
	public function translateLanguages(){
		return array(
			'Type' => $this->l('Type'),
			'Email Template' => $this->l('Email Template'),
			'Email Subject' => $this->l('Email Subject'),
			'Email Message' => $this->l('Email Message'),
			'Save' => $this->l('Save'),
			'Required field' => $this->l('Required field'),
		);
	}
}