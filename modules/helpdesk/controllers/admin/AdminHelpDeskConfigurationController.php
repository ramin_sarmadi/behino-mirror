<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskConfigurationController extends AdminHDController {
	public $default = array();
	public function __construct() {
	  	
	 	$this->table 	 = "hd_status";
 		$this->className = 'HdConfiguration';
		$this->colorOnBackground = true;
		
		
		$employees = self::getEmployees();
		$this->default = array(
			"allow_rich_editor"		=> "0",
			"show_signature"		=>"1",
			"submit_redirect"		=>"1",
			"staff_force_departments"=>"1",
			"show_ticket_voting"	=>"1",
			"allow_ticket_closing"	=>"1",
			"allow_ticket_reopening"=>"1",
			/* captcha */
			"captcha_enabled"		=>"1",
			"captcha_type"			=>"DEFAULT",
			"captcha_img_type"		=>"default",
			"captcha_characters"	=>"5",
			"num_lines"				=>"8",
			"image_height"			=>"50",
			"image_bg_color"		=>"#fff8f4",
			"text_color"			=>"#000a14",
			"line_color"			=>"#6254ff",
			"captcha_case_sensitive"=>"0",
			"perturbation"			=>"0.75",
			/* email */
			"email_address"			=> ($employees ? $employees[0]['email'] : ''),
			"email_address_fullname"=> ($employees ? $employees[0]['name'] : ''),
			/* auto close ticket*/
			"autoclose_enabled"		=>"1",
			"autoclose_cron_interval" =>"10",
			"autoclose_email_interval" =>"1",
			"autoclose_interval"	=>"1",
			/* notice */
			"notice_email_address"	=>"",
			"notice_max_replies_nr"	=>"0",
			"notice_replies_with_no_response_nr"=>"0",
			"notice_not_allowed_keywords"=>"",
			
			"show_user_info"	=>"name",
			"show_ticket_info"	=>"0",
			"messages_direction"=>"DESC",
			"show_email_link"	=>"0",
			"ticket_viewing_history"=>"",
		); 
		$obj = HdParams::getInstance();
		$this->_params = $obj;
		return parent::__construct();
	}
	
	
	public function postProcess(){
		
		if( Tools::getValue('updateConfiguration') ){	
			$db = Db::getInstance();
			
			foreach( $this->default as $param=>$value  ){
				if( ($this->_params->isParamsExist($param)) ){
					$result = $db->Execute(
					"UPDATE "._DB_PREFIX_.'hd_configuration SET `value`="'.( Tools::getValue($param,$value)).'" WHERE name="'.($param).'"'
					);
				} else {
					$result = $db->Execute(
						"INSERT INTO "._DB_PREFIX_.'hd_configuration(`name`, `value`) VALUES("'.($param).'","'.pSQL( Tools::getValue($param,$value)).'")'
					);
				}
				$this->_params->set($param, Tools::getValue($param,$value));
			}
			Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.Tools::getValue('token'));
		}
	}
	
	public function initContent()
	{
		$this->content .= $this->renderForm().'<br />';
		$this->show_toolbar = false;

		$this->context->smarty->assign(array(
			'content' => $this->content
		));
	}
	
	public function renderForm()
	{
		parent::renderForm();
		$employees = self::getEmployees();
		$employee = array();
		if($employees){
			$employee = $employees[0];
		}
		$args = array('employee' => $employee, 'l' => $this->translateLanguages());
		return $this->renderLayout("admin/configurationform", $args );
	}
	
	/**
    * Get value of parameter following to its name.
    * 
	* @return string is value of parameter.
	*/
	public function getParamValue($name, $default=''){
		return $this->_params->get( $name, $default );	
	}	
	/**
	 * Return all employee id and email
	 *
	 * @return array Employees
	 * @deprecated
	 */
	public static function getEmployees() {
		return Db::getInstance()->ExecuteS('
		SELECT `id_employee`, CONCAT(`firstname`, \' \', `lastname`) AS "name", email
		FROM `'._DB_PREFIX_.'employee`
		WHERE id_profile = 1 AND `active` = 1
		ORDER BY `id_employee` ASC');
	}
	
	public function translateLanguages(){
		return array(
			'No' => $this->l('No'),
			'Yes' => $this->l('Yes'),
			'General' => $this->l('General'),
			'Tickets' => $this->l('Ticket'),
			'CAPTCHA' => $this->l('CAPTCHA'),
			'Email' => $this->l('Email'),
			'Autoclose' => $this->l('Auto close'),
			'Notices' => $this->l('Notice'),
			'Use editor for writing messages' => $this->l('Enable editor in message form'),
			'Show signature checkbox when staff is replying' => $this->l('Show signature checkbox when staff is replying'),
			'Redirect to URL' => $this->l('Redirect to URL (after successful submission)'),
			'Ascending' => $this->l('Ascending'),
			'Descending' => $this->l('Descending'),
			'First name_Last name' => $this->l('First name - Last name'),
			'Email' => $this->l('Email'),
			'Enable and Allow Staff Members to View' => $this->l('Enable and Allow Staff Members to View'),
			'Enable and Allow Staff Members and Customers to View' => $this->l('Enable and Allow Staff Members and Customers to View'),
			'minutes' => $this->l('minutes'),
			'hours' => $this->l('hours'),
			'days' => $this->l('days'),
			'Sort Ticket Messages' => $this->l('Sort Ticket Messages'),
			'Show Submitter Information' => $this->l('Show author Information of Ticket'),
			'Show Submitter Field' => $this->l('Show author of ticket Field'),
			'Submitter Field is an Email Link' => $this->l('Author of ticket Field is an Email Link'),
			'Allow Ticket Voting' => $this->l('Allow Ticket Voting'),
			'Allow Customers to Close Tickets' => $this->l('Allow Customers to Close Tickets'),
			'Allow Customers to Re-open Closed Tickets' => $this->l('Allow Customers to Re-open Closed Tickets'),
			'Use Built_in Captcha' => $this->l('Use Built-in Captcha'),
			'Red' => $this->l('Red'),
			'White' => $this->l('White'),
			'Blackglass' => $this->l('Blackglass'),
			'Clean' => $this->l('Clean'),
			'Mathematic' => $this->l('Mathematic'),
			'Characters' => $this->l('Characters'),
			'image_jpeg' => $this->l('image/jpeg (jpg)'),
			'image_gif' => $this->l('image/gif (gif)'),
			'image_png' => $this->l('image/png (png)'),
			'Enable CAPTCHA' => $this->l('Enable CAPTCHA'),
			'CAPTCHA Type' => $this->l('CAPTCHA Type'),
			'The type of captcha to create, either alphanumeric, or a math problem' => $this->l('The type of captcha to create, either alphanumeric, or a math problem.'),
			'Image Type' => $this->l('Image Type'),
			'The type of the image, default_png' => $this->l('The type of the image, default = png.'),
			'Characters' => $this->l('Characters'),
			'The length of the captcha code' => $this->l('The length of the captcha code.'),
			'The height of the captcha image' => $this->l('The height of the captcha image.'),
			'Image Height' => $this->l('Image Height'),
			'How many lines to draw over the captcha code to increase security' => $this->l('How many lines to draw over the captcha code to increase security.'),
			'number lines' => $this->l('Number of lines'),
			'Perturbation' => $this->l('Perturbation'),
			'The level of distortion' => $this->l('The level of distortion, 0.75 = normal, 1.0 = very high distortion.'),
			'Image Background Color' => $this->l('Image Background Color'),
			'The background color of the captcha' => $this->l('The background color of the captcha.'),
			'The color of the captcha text' => $this->l('The color of the captcha text.'),
			'Text Color' => $this->l('Text Color'),
			'Line Color' => $this->l('Line Color'),
			'The color of the lines over the captcha' => $this->l('The color of the lines over the captcha.'),
			'Whether the captcha should be case sensitive' => $this->l('Whether the captcha should be case sensitive (not recommended, use only for maximum protection).'),
			'Case Sensitive' => $this->l('Case Sensitive'),
			'From Email Address' => $this->l('From Email Address'),
			'This is email address of email sender' => $this->l('This is email address of email sender.'),
			'From Name' => $this->l('From Name'),
			'This id full name of email sender_as it appears on outgoing emails' => $this->l('This id full name of email sender, as it appears on outgoing emails.'),
			'Enable Autoclose' => $this->l('Enable Autoclose'),
			'Enable the auto_close of tickets when they exceeds a pre_set number of day width no replies' => $this->l('Enable the auto-close of tickets when they exceeds a pre-set number of day width no replies.'),
			'check for tickets marked for autoclosing every' => $this->l('Check for tickets to marke as auto-close every (minute).'),
			'Days a ticket should go into autoclose status' => $this->l('Duration before ticket status change to auto-close(day)'),
			'Days a ticket should be autoclosed (after notification is sent)' => $this->l('Days a ticket should be autoclosed (after notification is sent)'),
			'Email Address' => $this->l('Email Address'),
			'Enter email address where you wish to receive the notifications' => $this->l('Enter email address where you wish to receive the notifications. You can enter mutiple address separates by comma(,). Please notice that if there is a staff member assigned to the ticket he will receive the notice as well'),
			'A notice will be sent when the number of consecutive customer' => $this->l('A notice will be sent when the number of consecutive customer replies exceeds this limit and the ticket is unassigned'),
			'Number of Replies' => $this->l('Number of Replies (Unassigned Ticket)'),
			'Number of Replies_Assigned' => $this->l('Number of Replies (Assigned Ticket)'),
			'A notice will be sent when the number' => $this->l('A notice will be sent when the number of consecutive customer replies exceeds this limit and the ticket is assigned. An email will be sent to the staff member as well.'),
			'Enter the keywords separates by comma' => $this->l('Enter the keywords separates by comma(,). A notice will be sent when any of above keyworks are detected in the message. An email will be sent to the staff member as well.'),
			'Triggered Keywords' => $this->l('Triggered Keywords'),
			'Global Configuration' => $this->l('Global Configuration'),
			'Update' => $this->l('Update'),
			'Appearance' => $this->l('Appearance'),
			'Disable' => $this->l('Disable'),
			'Customer Input' => $this->l('Customer Input'),
			'Days a ticket should be autoclosed' => $this->l('Duration before ticket is auto-closed(day)'),
		);
	}
}


?>