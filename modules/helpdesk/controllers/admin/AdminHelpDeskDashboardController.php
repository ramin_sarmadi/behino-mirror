<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskDashboardController extends AdminHDController {
	
	public function __construct() {
	  	global $lof;
		return parent::__construct();
	}
	
	
	public function initContent(){
		global $currentIndex;
		$cookie = $this->context->cookie;
		$status = HdStatus::getStatus( $cookie->id_lang );
		foreach($status as &$s){
			$s['tickets'] = HdTicket::getTickets($cookie->id_lang, false, false, false, false, $s['id_hd_status'], false, false, 'DESC', false, 1, false);
		}
		/* Recent ticket */
		$messages = HdTicketMessage::getTicketMessages( false, true, $cookie->id_lang, 10 );
		if($messages){
			foreach($messages as &$m){
				if($m['id_employee'])
					$m['objEmp'] = new Employee($m['id_employee']);
				elseif($m['id_customer'])
					$m['objCus'] = new Customer($m['id_customer']);
				$m['files'] = HdTicketMessage::getFiles($m['id_hd_ticket_message']);
			
			}
		}
		$permissions = array();
		$tabs = array("AdminHelpDesk", "AdminHelpDeskCatalog", "AdminHelpDeskDepartField", "AdminHelpDeskStaff", "AdminHelpDeskPriority", "AdminHelpDeskStatus", "AdminHelpDeskEmailTemplate", "AdminHelpDeskConfiguration" );
		foreach($tabs as $tab){
			$id_tab = Tab::getIdFromClassName($tab);
			$permissions[$tab] = Profile::getProfileAccess($cookie->profile, $id_tab);
		}
		$params = new HdParams('');
		$args = array('status' => $status, 'cookie'=>$cookie, 'messages' => $messages, 'params' => $params, 'permissions' => $permissions, 'l'=> $this->translateLanguages());

		$this->content .= $this->renderLayout("admin/dashboard", $args );
		$this->show_toolbar = false;
		$this->context->smarty->assign(array(
			'content' => $this->content
		));
	}
	
	
	public function translateLanguages(){
		return array(
			'Dashboard' => $this->l('Dashboard'),
			'Tickets Manager' => $this->l('Tickets Manager'),
			'Department Manager' => $this->l('Department Manager'),
			'Priority Manager' => $this->l('Priority Manager'),
			'Staff Manager' => $this->l('Staff Manager'),
			'Status Manager' => $this->l('Status Manager'),
			'Manager Status Manager' => $this->l('Manager Status Manager'),
			'Email Template Manager' => $this->l('Email Template Manager'),
			'Configuration Manager' => $this->l('Configuration Manager'),
			'Status' => $this->l('Status'),
			'Recent Messages' => $this->l('Recent Messages'),
			'has sent a reply to' => $this->l('has sent a reply to'),
			'seconds' => $this->l('seconds'),
			'second' => $this->l('second'),
			'ago' => $this->l('ago'),
			'minutes' => $this->l('minutes'),
			'minute' => $this->l('minute'),
			'hours' => $this->l('hours'),
			'hour' => $this->l('hour'),
			'days' => $this->l('days'),
			'day' => $this->l('day'),
			'months' => $this->l('months'),
			'month' => $this->l('month'),
			'years' => $this->l('years'),
			'year' => $this->l('year'),
		);
	}
}


?>