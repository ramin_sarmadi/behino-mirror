<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskStatusController extends AdminHDController {
	public $_listSkipDelete = array();
	public $_listSkipActive = array();
	public function __construct() {
	  	global $lof;
	 	$this->table 	 = "hd_status";
 		$this->className = 'HdStatus';
		$this->lang 	 = true;
	 	$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->colorOnBackground = true;
		$this->_listSkipDelete = $this->_listSkipActive = $lof['un_remove_status'];
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		foreach($this->_listSkipDelete as $skip){
			$this->addRowActionSkipList('delete', $skip);
			//$this->addRowActionSkipList('edit', $skip);
		}
		$this->fields_list = array(
			'id_hd_status' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'name' => array('title' => $this->l('Name'), 'align' => 'left', 'width' => 325),
			'active' => array('title' => $this->l('Displayed'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false)
		);

		return parent::__construct();
	}
	
	public function renderForm($isMainTab = true) {
		parent::renderForm();	
		if (!($obj = $this->loadObject(true)))
			return;
		$this->addJS(array( _PS_JS_DIR_.'jquery/plugins/jquery.colorpicker.js'));
		$agrs = array("obj"=>$obj,'skipactive'=>$this->_listSkipActive, 'l' => $this->translateLanguages());
		return $this->renderLayout("admin/statusform", $agrs );
	}
	
	public function translateLanguages(){
		return array(
			'Statuses' => $this->l('Statuses'),
			'Status name' => $this->l('Status name'),
			'Invalid characters: numbers and' => $this->l('Invalid characters: numbers and'),
			'Status Name_e' => $this->l('Status Name (e.g., Pending)'),
			'Enable' => $this->l('Enable'),
			'Disabled' => $this->l('Disabled'),
			'Color' => $this->l('Color'),
			'Status will be highlighted' => $this->l('Status will be highlighted in this color. HTML colors only (e.g.,'),
			'Save' => $this->l('Save'),
			'Required field' => $this->l('Required field'),
			'Enabled' => $this->l('Enabled'),
		);
	}
	
}


?>