<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 15168 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
include_once(dirname(__FILE__).'/AdminHelpDeskDepartmentController.php');
include_once(dirname(__FILE__).'/AdminHelpDeskCustomFieldController.php');
include_once(dirname(__FILE__).'/AdminHDController.php');
class AdminHelpDeskDepartFieldController extends AdminHDController
{
	/** @var object adminDepartment() instance */
	private $adminDepartment;

	/** @var object adminCustomfield() instance */
	private $adminCustomfield;
	
	/** @var object Category() instance for navigation*/
	private static $_hd_department = NULL;

	public function __construct()
	{
		/* Get current category */
		parent::__construct();
		$cookie = $this->context->cookie;
		$id_hd_department = abs((int)(Tools::getValue('id_hd_department')));
		if ($id_hd_department){
			self::$_hd_department = new HdDepartment($id_hd_department,$cookie->id_lang);
			if (!Validate::isLoadedObject(self::$_hd_department))
				die('Department cannot be loaded');
		}

		//$this->table = array('hd_department', 'hd_custom_fields');
		$this->table = 'hd_custom_fields';
		$this->className = 'HdDepartment';
		$this->adminDepartment = new AdminHelpDeskDepartmentController();
		$this->adminCustomfield = new AdminHelpDeskCustomFieldController();
		$this->adminDepartment->langs = $this->translateDepartmentLanguages();
		$this->adminCustomfield->langs = $this->translateCustomLanguages();
		
	}

	/**
	 * Return current category
	 *
	 * @return object
	 */
	public static function getCurrentDepartment() {
		return self::$_hd_department;
	}

	public function viewAccess($disable = false){
		$result = parent::viewAccess($disable);
		$this->adminDepartment->tabAccess = $this->tabAccess;
		$this->adminCustomfield->tabAccess = $this->tabAccess;
		return $result;
	}

	public function initContent()
	{
		$currentIndex = self::$currentIndex;
		$this->viewAccess();
		$this->adminDepartment->token = $this->token;
		$this->adminCustomfield->token = $this->token;

		if ($this->display == 'edit_hd_department')
			$this->content .= $this->adminDepartment->renderForm();
		else if ($this->display == 'edit_hd_custom_fields')
			$this->content .= $this->adminCustomfield->renderForm();
		else
		{
			$id_hd_department = (int)(Tools::getValue('id_hd_department'));
			if (!$id_hd_department)
				$id_hd_department = 1;
			$catalog_tabs = array('hd_department', 'hd_custom_fields');
			// Cleaning links
			$catBarIndex = $currentIndex;
			foreach ($catalog_tabs AS $tab)
				if (Tools::getValue($tab.'Orderby') && Tools::getValue($tab.'Orderway')) 
					$catBarIndex = preg_replace('/&'.$tab.'Orderby=([a-z _]*)&'.$tab.'Orderway=([a-z]*)/i', '', $currentIndex);
			if(!Tools::getValue('id_hd_department'))
				$this->content .= $this->adminDepartment->renderList();
			if(Tools::getValue('id_hd_department')){
				$this->adminCustomfield->id_hd_department = $id_hd_department;
				$this->content .= $this->adminCustomfield->renderList();
			}
			$this->context->smarty->assign(array(
				'breadcrumb' => $this->getPath($catBarIndex, $id_hd_department),
				
			));
			
		}
		$this->context->smarty->assign(array(
			'content' => $this->content,
			'title' => array('Department')
		));
	}
	
	public function postProcess()
	{
		$this->viewAccess();
		if (Tools::isSubmit('submitDelhd_custom_fields')
			|| Tools::isSubmit('submitAddhd_custom_fields')
			|| Tools::isSubmit('submitAddhd_custom_fieldsAndOther')
			|| Tools::isSubmit('submitAddhd_custom_fieldsAndStay')
			|| Tools::isSubmit('deletehd_custom_fields')
			|| Tools::isSubmit('viewhd_custom_fields')
			|| (Tools::isSubmit('statushd_custom_fields') && Tools::isSubmit('id_hd_custom_field'))
			|| (Tools::isSubmit('way') && Tools::isSubmit('id_hd_custom_field')) && (Tools::isSubmit('position'))){
			$this->adminCustomfield->postProcess();
		}else if (Tools::isSubmit('submitDelhd_department')
			|| Tools::isSubmit('submitAddhd_departmentAndBackToParent')
			|| Tools::isSubmit('submitAddhd_department')
			|| Tools::isSubmit('deletehd_department')
			|| (Tools::isSubmit('statushd_department') && Tools::isSubmit('id_hd_department'))){
			$this->adminDepartment->postProcess();
		}
		if (((Tools::isSubmit('submitAddhd_department') || Tools::isSubmit('submitAddhd_departmentAndStay')) && count($this->adminDepartment->errors))
			|| Tools::isSubmit('updatehd_department')
			|| Tools::isSubmit('addhd_department'))
			$this->display = 'edit_hd_department';
		else if (((Tools::isSubmit('submitAddhd_custom_fields') || Tools::isSubmit('submitAddhd_custom_fieldsAndStay')) && count($this->adminCustomfield->errors))
			|| Tools::isSubmit('updatehd_custom_fields')
			|| Tools::isSubmit('addhd_custom_fields'))
			$this->display = 'edit_hd_custom_fields';
		else
		{
			$this->display = 'list';
			$this->id_hd_department = (int)Tools::getValue('id_hd_department');
		}

		if (isset($this->adminCustomfield->errors))
			$this->errors = array_merge($this->errors, $this->adminCustomfield->errors);

		if (isset($this->adminDepartment->errors))
			$this->errors = array_merge($this->errors, $this->adminDepartment->errors);

		parent::postProcess();
	}
	
	function getPath($catBarIndex, $id_hd_department = false){
		$cookie = $this->context->cookie;
		if(!$id_hd_department){
			return $fullPath = $this->l('Departments');
		}
		$path = $catBarIndex.'&token='.Tools::getAdminTokenLite('AdminHelpDeskDepartField');
		$fullPath = '<a href="'.$path.'" title="'.$this->l('Departments').'">'.$this->l('Departments').'</a>';
		if(Validate::isLoadedObject($department = new HdDepartment($id_hd_department,$cookie->id_lang))){
			$fullPath .= ' &gt; '.$department->name;
		}
		return $fullPath;
	}
	
	public function setMedia()
	{
		parent::setMedia();
		$cookie = $this->context->cookie;
		$this->addJqueryUi('ui.widget');
		$this->addJqueryPlugin('tagify');
		$this->addCSS(_MODULE_DIR_."helpdesk/assets/admin/css/customfield.css");
		$jss = array(
			_MODULE_DIR_."helpdesk/assets/admin/js/customfield.js",
			_PS_JS_DIR_.'tiny_mce/tiny_mce.js',
			_PS_JS_DIR_.'tinymce.inc.js',
			_MODULE_DIR_."helpdesk/assets/tinymce.inc.js",
			__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js'
		);
		$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)($cookie->id_lang));
		if ($iso != 'en'){
			$jss[] = __PS_BASE_URI__.'js/jquery/ui/i18n/ui.datepicker-'.$iso.'.js';
		}
		$this->addJS($jss);
	}

	public function ajaxProcessUpdateCmsPositions()
	{
		$id_cms = (int)Tools::getValue('id_cms');
		$id_category = (int)Tools::getValue('id_cms_category');
		$way = (int)Tools::getValue('way');
		$positions = Tools::getValue('cms');
		if (is_array($positions))
			foreach ($positions as $key => $value)
			{
				$pos = explode('_', $value);
				if ((isset($pos[1]) && isset($pos[2])) && ($pos[1] == $id_category && $pos[2] == $id_cms))
				{
					$position = $key;
					break;
				}
			}
		$cms = new CMS($id_cms);
		if (Validate::isLoadedObject($cms))
		{
			if (isset($position) && $cms->updatePosition($way, $position))
				die(true);
			else
				die('{"hasError" : true, "errors" : "Can not update cms position"}');
		}
		else
			die('{"hasError" : true, "errors" : "This cms can not be loaded"}');
	}

	public function ajaxProcessUpdateCmsCategoriesPositions()
	{
		$id_cms_category_to_move = (int)Tools::getValue('id_cms_category_to_move');
		$id_cms_category_parent = (int)Tools::getValue('id_cms_category_parent');
		$way = (int)Tools::getValue('way');
		$positions = Tools::getValue('cms_category');
		if (is_array($positions))
			foreach ($positions as $key => $value)
			{
				$pos = explode('_', $value);
				if ((isset($pos[1]) && isset($pos[2])) && ($pos[1] == $id_cms_category_parent && $pos[2] == $id_cms_category_to_move))
				{
					$position = $key;
					break;
				}
			}
		$cms_category = new CMSCategory($id_cms_category_to_move);
		if (Validate::isLoadedObject($cms_category))
		{
			if (isset($position) && $cms_category->updatePosition($way, $position))
				die(true);
			else
				die('{"hasError" : true, "errors" : "Can not update cms categories position"}');
		}
		else
			die('{"hasError" : true, "errors" : "This cms category can not be loaded"}');
	}
	
	public function translateDepartmentLanguages(){
		return array(
			'General' => $this->l('General'),
			'Save' => $this->l('Save'),
			'Save And Another' => $this->l('Save And Another'),
			'Save And Stay' => $this->l('Save And Stay'),
			'Required field' => $this->l('Required field'),
			'Departement Title' => $this->l('Department Title'),
			'Invalid characters' => $this->l('Invalid characters: numbers and'),
			'Department Title' => $this->l('Department Title (e.g., "Pending")'),
			'Enable' => $this->l('Enable'),
			'Disabled' => $this->l('Disabled'),
			'Prefix' => $this->l('Prefix'),
			'This is the department prefix' => $this->l('This is the department prefix, it will be added to ticket code'),
			'Enable uploading files' => $this->l('Enable uploading files'),
			'Allowed extensions' => $this->l('Allowed extensions'),
			'Please note that allowing extensions such as php' => $this->l('Please note that allowing extensions such as php to be uploaded you compromise your server security!'),
			'Maximun Upload Size' => $this->l('Maximun Upload Size'),
			'Your server allows uploads up to' => $this->l('Your server allows uploads up to'),
			'Maximum files allowed' => $this->l('Maximum files allowed'),
			'Users can submit multiple files by clicking' => $this->l('Users can submit multiple files by clicking on the Add another attchment button. if you want to limit this, set a value greater than 0.'),
			'Tickets Setting' => $this->l('Tickets Setting'),
			'ticket_assignment_type' => $this->l('Ticket Assignment Type'),
			'Ticket Code Generation Rule' => $this->l('Ticket Code Generation Rule'),
			'Priority' => $this->l('Priority'),
			'Email Setting' => $this->l('Email Setting'),
			'Use Email in Global Configuration' => $this->l('Use Email in Global Configuration'),
			'From Email Address' => $this->l('From Email Address'),
			'This is the email address of the email sender' => $this->l('This is the email address of the email sender'),
			'From Name' => $this->l('From Name'),
			'This is the full name of the email sender, as it appears on outging emails' => $this->l('This is the full name of the email sender, as it appears on outging emails.'),
			'Send emails to customer when staff members reply' => $this->l('Send emails to customer when staff members replied'),
			'Send email to customer with a copy of his ticket' => $this->l('Send copy of ticket in the email to customer'),
			'Send attachments with customer' => $this->l('Allow attachments of ticket to send in email.'),
			'Send emails to the staff member when customers reply' => $this->l('Send emails to the staff member when customers reply'),
			'Send attachments with staff member' => $this->l('Send attachments with staff member\'s email'),
			'Notify for email' => $this->l('Send notification email to email address.'),
			'Notify the following email addresses when a new ticket arrives' => $this->l('Notify the following email addresses when a new ticket arrives (each email on a new line)'),
			'CC' => $this->l('CC'),
			'each email on a new line' => $this->l('each email on a new line'),
			'BCC' => $this->l('BCC'),
			'each email on a new line' => $this->l('each email on a new line'),
			'Notify staff members' => $this->l('Notify staff members'),
			'Notify staff members when a ticket has been manually assigned to them' => $this->l('Notify staff members when a ticket has been manually assigned to them'),
			'Enabled' => $this->l('Enabled'),
			'Disabled' => $this->l('Disabled'),
		);
	}
	
	public function translateCustomLanguages(){
		return array(
			'General' => $this->l('General'),
			'Add' => $this->l('add'),
			'Save' => $this->l('Save'),
			'Save And Another' => $this->l('Save And Another'),
			'Save And Stay' => $this->l('Save And Stay'),
			'Required field' => $this->l('Required field'),
			'Name' => $this->l('Name'),
			'Title' => $this->l('Title'),
			'Active' => $this->l('Active'),
			'Required' => $this->l('Required'),
			'Enabled' => $this->l('Enabled'),
			'Disabled' => $this->l('Disabled'),
			'Type' => $this->l('Type'),
			'Description' => $this->l('Description'),
			'Size' => $this->l('Size'),
			'Cols' => $this->l('Cols'),
			'Rows' => $this->l('Rows'),
			'Value' => $this->l('Value'),
			'Value' => $this->l('Value'),
			'Text' => $this->l('Text'),
			'each Text on a new line and each Value on a new line' => $this->l('each Text on a new line and each Value on a new line.'),
		);
	}
}
