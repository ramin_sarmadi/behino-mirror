<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if(!isset($_SESSION)) session_start();
define( "_HELPDESK" , 1 );

include_once( dirname(__FILE__)."/../../defines.helpdesk.php" );
include_once( dirname(__FILE__)."/../../classes/HdConfiguration.php" );
include_once( dirname(__FILE__)."/../../classes/HdCustomFields.php" );
include_once( dirname(__FILE__)."/../../classes/HdDepartment.php" );
include_once( dirname(__FILE__)."/../../classes/HdEmailTemplate.php" );
include_once( dirname(__FILE__)."/../../classes/HDMail.php" );
include_once( dirname(__FILE__)."/../../classes/HdParams.php" );
include_once( dirname(__FILE__)."/../../classes/HdPriority.php" );
include_once( dirname(__FILE__)."/../../classes/HdStaff.php" );
include_once( dirname(__FILE__)."/../../classes/Hdstatus.php" );
include_once( dirname(__FILE__)."/../../classes/HdTicket.php" );
include_once( dirname(__FILE__)."/../../classes/HdTicketFile.php" );
include_once( dirname(__FILE__)."/../../classes/HdTicketMessage.php" );
include_once( dirname(__FILE__)."/../../classes/HdTicketNotice.php" );
include_once( dirname(__FILE__)."/../../classes/HdTicketHistory.php" );
include_once( dirname(__FILE__)."/../../classes/HdQuickResponse.php" );
include_once( dirname(__FILE__)."/../../libs/helper.php" );
class AdminHDController extends AdminController {
	
	public $_defaultFormLanguage;
	public function __construct() {
		parent::__construct();
		$cookie = $this->context->cookie;
		$this->getlanguages();
		$this->_defaultFormLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
		if($cookie->id_employee){
			$k = Configuration::get('_LOF_HELPDESK_SESSION_KEY_');
			if($k){
				$folderAdmin = HelpDeskHelper::getFolderAdmin();
				$this->lofToken = md5($cookie->id_employee.$folderAdmin);
				$_SESSION[$k.'_EMPLOYEE'] = $cookie->id_employee;
			}
		}
		return ;
	}
	
	public function displayFlags($languages, $default_language, $ids, $id, $return = false, $use_vars_instead_of_ids = false)
	{
		if (count($languages) == 1)
			return false;

		$output = '
		<div class="displayed_flag">
			<img src="../img/l/'.$default_language.'.jpg" class="pointer" id="language_current_'.$id.'" onclick="toggleLanguageFlags(this);" alt="" />
		</div>
		<div id="languages_'.$id.'" class="language_flags">
			'.$this->l('Choose language:').'<br /><br />';
		foreach ($languages as $language)
			if ($use_vars_instead_of_ids)
				$output .= '<img src="../img/l/'.(int)$language['id_lang'].'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="changeLanguage(\''.$id.'\', '.$ids.', '.$language['id_lang'].', \''.$language['iso_code'].'\');" /> ';
			else
			$output .= '<img src="../img/l/'.(int)$language['id_lang'].'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="changeLanguage(\''.$id.'\', \''.$ids.'\', '.$language['id_lang'].', \''.$language['iso_code'].'\');" /> ';
		$output .= '</div>';

		if ($return)
			return $output;
		echo $output;
	}
	
	public function renderLayout( $layout, $args=array() ){
		extract( $args );
		$moduleBasePath = _PS_MODULE_DIR_.'helpdesk/';
		ob_start();
		include $moduleBasePath.'/tmpl/'.$layout.'.php';
		$data = ob_get_clean();
		return $data;
	}
}


?>