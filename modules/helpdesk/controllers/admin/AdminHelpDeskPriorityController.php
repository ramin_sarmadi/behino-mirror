<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskPriorityController extends AdminHDController {
	public $_listSkipDelete = array();
	public $_listSkipActive = array();
	public $list_skip_actions = array();
	public function __construct() {
	  	global $lof;
	 	$this->table 	 = "hd_priority";
 		$this->className = 'HdPriority';
		$this->lang 	 = true;
	 	$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->colorOnBackground = true;
		
		foreach($lof['un_remove_priority'] as $skip){
			$this->addRowActionSkipList('delete', $skip);
			//$this->addRowActionSkipList('edit', $skip);
		}
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		$this->fields_list = array(
			'id_hd_priority' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'name' => array('title' => $this->l('Name'), 'align' => 'left', 'width' => 325) ,
			'active' => array('title' => $this->l('Displayed'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false)
		);
		return parent::__construct();
	}
 
	public function renderForm() {
		parent::renderForm();
		if (!($obj = $this->loadObject(true)))
			return;
		$this->addJS(array( _PS_JS_DIR_.'jquery/plugins/jquery.colorpicker.js'));
		   return $this->renderLayout("admin/priorityform",
				array("obj"=>$obj, 'skipactive' => $this->_listSkipActive, 'l' => $this->translateLanguages()) );
	}
	
	public function translateLanguages(){
		return array(
			'Priority' => $this->l('Priority'),
			'Priority Title' => $this->l('Priority Title (e.g., Pending)'),
			'Enable' => $this->l('Enable'),
			'Enabled' => $this->l('Enabled'),
			'Disabled' => $this->l('Disabled'),
			'Background Color' => $this->l('Background Color'),
			'Font Color' => $this->l('Font Color'),
			'Priority will be highlighted in this color' => $this->l('Priority will be highlighted in this color. HTML colors only (e.g.,'),
			'Save' => $this->l('Save'),
			'Required field' => $this->l('Required field'),
			'Status Title' => $this->l('Status Title'),
			'Invalid characters: numbers and' => $this->l('Invalid characters: numbers and'),
		);
	}
}


?>