<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskDepartmentController  extends AdminHDController {
	public $assignment_type = array();
	public $generation_rule = array();
	public $langs = array();
	public function __construct() {
	 	$this->table 	 = "hd_department";
 		$this->className = 'HdDepartment';
 		$this->identifier = 'id_hd_department';
		$this->lang 	 = true;
	 	$this->addRowAction('view');
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		//$this->colorOnBackground = true;
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		
		$this->_select = '
			(SELECT COUNT(jcg.`id_hd_custom_field`)
			FROM `'._DB_PREFIX_.'hd_custom_fields` jcg 
			WHERE jcg.`id_hd_department` = a.`id_hd_department`) AS nb
		';
		$this->fields_list = array(
			'id_hd_department' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'name' => array('title' => $this->l('Name'), 'align' => 'center', 'width' => 200) ,
			'prefix' => array('title' => $this->l('Prefix'), 'align' => 'center', 'width' => 50) ,
			'nb' => array('title' => $this->l('Custom Fields'), 'width' => 25, 'align' => 'center','havingFilter'=>true),
			//'position' => array('title' => $this->l('Position'), 'width' => 40,'filter_key' => 'position', 'align' => 'center', 'position' => 'position'),
			'active' => array('title' => $this->l('Displayed'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false));
		
		$this->assignment_type = array( '0' =>$this->l('Static'), '1' => $this->l('Automatic'));
		$this->generation_rule = array( '0' =>$this->l('Sequential'), '1' => $this->l('Random'));
		return parent::__construct();
	}
	
	public function initBreadcrumbs()
	{
		$this->breadcrumbs[] = $this->l('Departments');
	}
	
	public function renderList()
	{
		$this->initToolbar();
		return parent::renderList();
	}
	
	public function renderLayout( $layout, $args=array() ){
		extract( $args );
		$moduleBasePath = _PS_MODULE_DIR_.'helpdesk/';
		ob_start();
		include $moduleBasePath.'/tmpl/'.$layout.'.php';
		$data = ob_get_clean();
		return $data;
	}
	
	public function renderForm($token = NULL) {
		$cookie = $this->context->cookie;
		parent::renderForm();
		if (!($obj = $this->loadObject(true)))
			return;
		$priorities = HdPriority::getPriorities($cookie->id_lang);
		$token = ($token ? $token : $this->token);
		$args =  array(
				'obj' => $obj, 'assignment_type' => $this->assignment_type, 'token' => $token,
				'generation_rule' => $this->generation_rule, 'priorities' => $priorities, 'l' => $this->translateLanguages()
			);
		return $this->renderLayout("admin/departmentform", $args);
	}
	
	public function postProcess($token = NULL) {
		global $currentIndex;
		$cookie = $this->context->cookie;
		// $this->tabAccess = Profile::getProfileAccess($cookie->profile, $this->id);
//echo "<pre>".print_r($_POST,1); die;
		if (Tools::isSubmit('submitAdd'.$this->table) OR Tools::isSubmit('submitAdd'.$this->table.'AndOther') OR Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
		{
			$id = (int)(Tools::getValue($this->identifier));
			if (isset($id) AND $id){
				if ($this->tabAccess['edit'] === '1'){
					$this->validateRules();
					if (!sizeof($this->errors)) {
						$object = new $this->className($id);
						if (Validate::isLoadedObject($object)){
							$this->copyFromPost($object, $this->table);
							$object->prefix = strtoupper($object->prefix);
							$result = $object->update();
							if (!$result){
								$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b>';
							}else{
								if(Tools::isSubmit('submitAdd'.$this->table.'AndOther')){
									Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&add'.$this->table);
								}elseif(Tools::isSubmit('submitAdd'.$this->table.'AndStay')){
									Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&add'.$this->table.'&'.$this->identifier.'='.$object->id);
								}else{
									Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token'));
								}
							}
						}else
							$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
					}
				}else
					$this->errors[] = Tools::displayError('You do not have permission to edit here.');
			}else{
				if ($this->tabAccess['add'] === '1'){
					$this->validateRules();
					if (!sizeof($this->errors)) {
						$object = new $this->className();
						$this->copyFromPost($object, $this->table);
						$object->prefix = strtoupper($object->prefix);
						if (!$object->add()){
							$this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.'</b>';
						}else{
							if(Tools::isSubmit('submitAdd'.$this->table.'AndOther')){
								Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&add'.$this->table);
							}elseif( Tools::isSubmit('submitAdd'.$this->table.'AndStay') ){
								Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&add'.$this->table.'&'.$this->identifier.'='.$object->id);
							}else{
								Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token'));
							}
						}
					}
				}else
					$this->errors[] = Tools::displayError('You do not have permission to add here.');
			}
		}
		/* Delete object */
		elseif (isset($_GET['delete'.$this->table]))
		{
			if ($this->tabAccess['delete'] === '1'){
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->delete()){
						Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')));
					}
					$this->errors[] = Tools::displayError('An error occurred during deletion.');
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		/* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table)){
			if ($this->tabAccess['delete'] === '1'){
				if (isset($_POST[$this->table.'Box'])){
					$customfield = new HdCustomFields();
					$result = true;
					$result = $customfield->deleteSelection(Tools::getValue($this->table.'Box'));
					if ($result){
						$customfield->cleanPositions((int)(Tools::getValue('id_hd_department')));
						Tools::redirectAdmin($currentIndex.'&conf=2&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')));
					}
					$this->errors[] = Tools::displayError('An error occurred while deleting selection.');

				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
			return;
		}
		/* Change object statuts (active, inactive) */
		elseif (Tools::isSubmit('status'.$this->table) AND Tools::isSubmit($this->identifier)){
			if ($this->tabAccess['edit'] === '1') {
				if (Validate::isLoadedObject($object = $this->loadObject())){
					if ($object->toggleStatus()){
						Tools::redirectAdmin($currentIndex.'&conf=5&token='.Tools::getValue('token'));
					}
					else
						$this->errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else{
				$this->errors[] = Tools::displayError('You do not have permission to edit here.');
			}
		}
		
		parent::postProcess();
	}
	
	public function translateLanguages(){
		return $this->langs;
	}
}


?>