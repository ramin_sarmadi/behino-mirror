<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskCustomFieldController  extends AdminHDController {
	protected $position_identifier = 'id_hd_custom_field';
	public $langs = array();
	public function __construct() {
	 	$this->table 	 = "hd_custom_fields";
 		$this->className = 'HdCustomFields';
		$this->identifier = 'id_hd_custom_field';
		$this->lofid_parent = 'id_hd_department';
		$this->identifiersDnd = array('id_hd_custom_field' => 'id_hd_custom_field');
		$this->lang 	 = true;
	 	$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		
		
		$id_hd_department = Tools::getValue('id_hd_department');
		$this->_where = ' AND a.`id_hd_department` = '.(int)($id_hd_department);
		$this->fields_list = array(
			'id_hd_custom_field' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'title' => array('title' => $this->l('Title'), 'align' => 'left', 'width' => 120) ,
			'description' => array('title' => $this->l('Description'), 'align' => 'left', 'width' => 350) ,
			'type' => array('title' => $this->l('Type'), 'align' => 'center', 'width' => 50) ,
			'position' => array('title' => $this->l('Position'), 'width' => 40,'filter_key' => 'position', 'align' => 'center', 'position' => 'position'),
			'active' => array('title' => $this->l('Displayed'), 'width' => 25, 'align' => 'center', 'active' => 'status', 'type' => 'bool', 'orderby' => false)
			);
		$this->_department = AdminHelpDeskDepartFieldController::getCurrentDepartment();
		$this->fiedsType = array(
			'freetext' => $this->l('Custom Html'),
			'textbox' => $this->l('Text Box'),
			'textarea' => $this->l('Text Area'),
			'select' => $this->l('Select List'),
			'multipleselect' => $this->l('Multiple Select List'),
			'checkbox' => $this->l('Checkbox Group'),
			'radio' => $this->l('Radio Group'),
			'calendar' => $this->l('Calendar'),
			'calendartime' => $this->l('Calendar and Time'),
		);
		return parent::__construct();
	}
	
	public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
	{
		if ($order_by && $this->context->cookie->__get($this->table.'Orderby'))
			$order_by = $this->context->cookie->__get($this->table.'Orderby');
		else
			$order_by = 'position';

		parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
	}
	
	public function renderList()
	{
		$cookie = $this->context->cookie;
		if (($id_hd_department = (int)Tools::getValue('id_hd_department')))
			self::$currentIndex .= '&id_hd_department='.$id_hd_department;
		$objDepartment = new HdDepartment($id_hd_department, $cookie->id_lang);
		$this->toolbar_title = $this->l('Custom fields in department').' "'.$objDepartment->name.'"';
		$this->toolbar_btn['new'] = array(
			'href' => self::$currentIndex.'&amp;add'.$this->table.'&amp;token='.$this->token,
			'desc' => $this->l('Add new')
		);

		return parent::renderList();
	}
	
	
	public function renderForm($token = NULL){
		parent::renderForm();
		if (!($obj = $this->loadObject(true)))
			return;
		$token = ($token ? $token : $this->token);
		$data = array(
				"obj" => $obj, 
				'fiedsType' => $this->fiedsType, 
				'token' => $token, 
				'id_hd_department'=>Tools::getValue('id_hd_department'), 
				'l'=> $this->translateLanguages()
			);
		return $this->renderLayout("admin/customfieldform", $data );
	}
	
	public function postProcess($token = NULL) {
		global $currentIndex;
		$cookie = $this->context->cookie;
		if (Tools::isSubmit('submitAdd'.$this->table) OR Tools::isSubmit('submitAdd'.$this->table.'AndOther') OR Tools::isSubmit('submitAdd'.$this->table.'AndStay'))
		{
			$this->validateRules();
			if (!sizeof($this->errors)) {
				$id = (int)(Tools::getValue($this->identifier));
				if (isset($id) AND $id){
					if ($this->tabAccess['edit'] === '1'){
						$this->validateRules();
						if (!sizeof($this->errors)) {
							$object = new $this->className($id);
							if (Validate::isLoadedObject($object)){
								$this->copyFromPost($object, $this->table);
								if($object->type == 'calendar')
									$object->values = Tools::getValue('calendar');
								if($object->type == 'calendartime')
									$object->values = Tools::getValue('calendartime');
								$result = $object->update();
								if (!$result){
									$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b>';
								}else{
									if(Tools::isSubmit('submitAdd'.$this->table)){
										Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')));
									}elseif(Tools::isSubmit('submitAdd'.$this->table.'AndOther')){
										Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')).'&add'.$this->table);
									}else{
										Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')).'&add'.$this->table.'&'.$this->identifier.'='.$object->id);
									}
								}
							}else
								$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
						}
					}else
						$this->errors[] = Tools::displayError('You do not have permission to edit here.');
				}else{
					if ($this->tabAccess['add'] === '1'){
						$this->validateRules();
						
						if (!sizeof($this->errors)) {
							
							$object = new $this->className();
							$this->copyFromPost($object, $this->table);
							if($object->type == 'calendar')
								$object->values = Tools::getValue('calendar');
							if($object->type == 'calendartime')
								$object->values = Tools::getValue('calendartime');
							//echo "<pre>".print_r($object,1); die;
							if (!$object->add()){
								$this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.'</b>';
							}else{
								if(Tools::isSubmit('submitAdd'.$this->table)){
									Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')));
								}elseif(Tools::isSubmit('submitAdd'.$this->table.'AndOther')){
									Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')).'&add'.$this->table);
								}else{
									Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')).'&add'.$this->table.'&'.$this->identifier.'='.$object->id);
								}
							}
						}
					}else
						$this->errors[] = Tools::displayError('You do not have permission to add here.');
				}
			}
			$this->errors = array_unique($this->errors);
		}
		/* Delete object */
		elseif (isset($_GET['delete'.$this->table]))
		{
			if ($this->tabAccess['delete'] === '1'){
				if (Validate::isLoadedObject($object = $this->loadObject()))
				{
					if ($object->delete()){
						Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')));
					}
					$this->errors[] = Tools::displayError('An error occurred during deletion.');
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}elseif (isset($_GET['position'])){
			$id_hd_department = Tools::getValue('id_hd_department');
			if ($this->tabAccess['edit'] !== '1')
				$this->errors[] = Tools::displayError('You do not have permission to edit here.');
			elseif (!Validate::isLoadedObject($object = $this->loadObject() ))
				$this->errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			if (!$object->updatePosition((int)(Tools::getValue('way')), (int)(Tools::getValue('position'))))
				$this->errors[] = Tools::displayError('Failed to update the position.');
			else{
				Tools::redirectAdmin($currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5&id_hd_department='.(int)($id_hd_department).'&token='.Tools::getValue('token'));
			}
		}
		/* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table)){
			if ($this->tabAccess['delete'] === '1'){
				if (isset($_POST[$this->table.'Box'])){
					$customfield = new HdCustomFields();
					$result = true;
					$result = $customfield->deleteSelection(Tools::getValue($this->table.'Box'));
					if ($result){
						$customfield->cleanPositions((int)(Tools::getValue('id_hd_department')));
						Tools::redirectAdmin($currentIndex.'&conf=2&token='.Tools::getValue('token').'&id_hd_department='.(int)(Tools::getValue('id_hd_department')));
					}
					$this->errors[] = Tools::displayError('An error occurred while deleting selection.');

				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
			return;
		}
		/* Change object statuts (active, inactive) */
		elseif (Tools::isSubmit('status'.$this->table) AND Tools::isSubmit($this->identifier)){
			$id_hd_department = Tools::getValue('id_hd_department');
			if ($this->tabAccess['edit'] === '1') {
				if (Validate::isLoadedObject($object = $this->loadObject())){
					if ($object->toggleStatus()){
						Tools::redirectAdmin($currentIndex.'&conf=5&id_hd_department='.(int)($id_hd_department).'&token='.Tools::getValue('token'));
					}
					else
						$this->errors[] = Tools::displayError('An error occurred while updating status.');
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else{
				$this->errors[] = Tools::displayError('You do not have permission to edit here.');
			}
		}
		parent::postProcess();
	}
	
	public function translateLanguages(){
		return $this->langs;
	}
}


?>