<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskStaffController extends AdminHDController {
	
	public function __construct() {
	  	
	 	$this->table 	 = "hd_staff";
 		$this->className = "HdStaff";
	 	$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		
		$this->_select = " CONCAT(e.firstname, ' ' ,e.lastname) as fullname, a.id_hd_staff, e.email ";
		$this->_join = "
			LEFT JOIN "._DB_PREFIX_."employee e ON e.id_employee = a.id_employee
		";
 
		$this->fields_list = array(
			'id_hd_staff' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'fullname' => array('title' => $this->l('Employee'), 'align' => 'center', 'width' => 150),
			'email' => array('title' => $this->l('Email'), 'align' => 'center', 'width' => 300),
			);
 
		return parent::__construct();
	}
	
	public function renderForm($isMainTab = true) {
		parent::renderForm();
		if (!($obj = $this->loadObject(true)))
			return;
		
		
		$cookie = $this->context->cookie; 
	   
		$employees   = Employee::getEmployees();
		$departments = HdDepartment::getDepartments( $cookie->id_lang );
		$priorities  = HdPriority::getPriorities( $cookie->id_lang );
		return $this->renderLayout("admin/staffform", 
				array( "obj" 		=> $obj,
					  "employees"   => $employees,
					  'departments' => $departments,
					  'priorities' => $priorities, 'l' => $this->translateLanguages() 
					) );
	}
	
	public function translateLanguages(){
		return array(
			'Staff' => $this->l('Staff'),
			'Departments' => $this->l('Departments'),
			'Employee' => $this->l('Employee'),
			'Priority' => $this->l('Priority'),
			'Signature' => $this->l('Signature'),
			'Save' => $this->l('Save'),
			'Required field' => $this->l('Required field'),
		);
	}
}


?>