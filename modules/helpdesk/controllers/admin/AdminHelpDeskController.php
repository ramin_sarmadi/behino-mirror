<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include_once( dirname(__FILE__)."/AdminHDController.php" );

class AdminHelpDeskController extends AdminHDController {
	
	private $departmentsArray = array();
	private $prioritiesArray = array();
	private $statusArray = array();
	private $employeeArray = array();
	public $_defaultFormLanguage;
	public function __construct() {
		
	 	$this->table ="hd_ticket";
		$this->className = 'HdTicket';
	 	$this->addRowAction('view');
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		parent::__construct();
		$this->getlanguages();
		$this->_defaultFormLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
		$cookie = $this->context->cookie;
		
		$priorities = HdPriority::getPriorities($cookie->id_lang);
		if($priorities)
			foreach($priorities as $d){
				$this->prioritiesArray[$d['id_hd_priority']] = $d['name'];
			}
		$status = HdStatus::getStatus($cookie->id_lang);
		if($status)
			foreach($status as $d){
				$this->statusArray[$d['id_hd_status']] = $d['name'];
			}
		$staffs = HdStaff::getStaffByEmployee($cookie->id_employee);
		$departments = HdStaff::getDepartmentByStaff($staffs);
		if($cookie->profile != LOF_ID_PROFILE_ADMIN)
			$employees = HdStaff::getEmployeeByDepartment($departments);
		else
			$employees = HdStaff::getEmployeeByStaff();
		if($employees)
			foreach($employees as $e){
				$this->employeeArray[$e['id_employee']] = $e['name'];
			}
		
		if($cookie->profile != LOF_ID_PROFILE_ADMIN)
			$adepartments = HdStaff::getDepartmentByStaff($staffs, 'a');
		else
			$adepartments = HdDepartment::getDepartments($cookie->id_lang);
		if($adepartments)
			foreach($adepartments as $d)
				$this->departmentsArray[$d['id_hd_department']] = $d['name'];
		
		$this->fields_list = array(
			'code' => array('title' => $this->l('Ticket ID'), 'align' => 'center', 'width' => 20,'search' => true),
			'date_add' => array('title' => $this->l('Date Add'), 'align' => 'center', 'width' => 70, 'type' => 'date', 'filter_key' => 'a!date_add'),
			'last_reply' => array('title' => $this->l('Last Reply'), 'align' => 'center', 'width' => 70, 'type' => 'date', 'filter_key' => 'a!last_reply'),
			'flagged' => array('title' => $this->l('Flagged'), 'align' => 'center', 'width' => 40, 'flagged'=>true, 'align' => '', 'type' => 'bool'),
			'subject_nb' => array('title' => $this->l('Subject'), 'align' => 'center', 'width' => 150,'havingFilter'=>true, 'align' => 'left'),
			//'nb' => array('title' => $this->l('Replies'), 'align' => 'center', 'width' => 15,'havingFilter'=>true),
			'department' => array('title' => $this->l('Department'), 'align' => 'center', 'width' => 75, 'type' => 'select', 'list' => $this->departmentsArray, 'filter_key' => 'hdp!id_hd_department'),
			'customer' => array('title' => $this->l('Customer'), 'align' => 'center', 'width' => 75,'havingFilter'=>true),
			'priority' => array('title' => $this->l('Priority'), 'align' => 'center', 'width' => 40,'color' => 'color_priority', 'type' => 'select', 'list' => $this->prioritiesArray, 'filter_key' => 'hpo!id_hd_priority'),
			'status' => array('title' => $this->l('Status'), 'align' => 'center', 'width' => 40, 'color' => 'color_status', 'type' => 'select', 'list' => $this->statusArray, 'filter_key' => 'hs!id_hd_status'),
			'assigned' => array('title' => $this->l('Assigned To'), 'align' => 'center', 'width' => 50, 'type' => 'select', 'list' => $this->employeeArray, 'filter_key' => 'e!id_employee'),
		);
		
		$this->_select = '
			(SELECT COUNT(jcg.`id_hd_ticket_message`)
			FROM `'._DB_PREFIX_.'hd_ticket_messages` jcg 
			WHERE jcg.`id_hd_ticket` = a.`id_hd_ticket`) AS nb,
			CONCAT(cu.firstname, \' \', cu.lastname) as customer,
			CONCAT(e.firstname, \' \', e.lastname) as assigned, hdpl.`name` as department,hpol.`name` as priority,
			hsl.`name` as status,tl.subject,CONCAT(tl.subject,\' (\', a.replies,\')\') as subject_nb, hs.`color` as color_status,hpo.`color` as color_priority
		';
		
		$this->_join = '
		LEFT JOIN `'._DB_PREFIX_.'hd_ticket_lang` tl ON (tl.`id_hd_ticket` = a.`id_hd_ticket` AND tl.`id_lang` = '.$cookie->id_lang.')
		LEFT JOIN `'._DB_PREFIX_.'customer` cu ON (cu.`id_customer` = a.`id_customer`)
		LEFT JOIN `'._DB_PREFIX_.'hd_department` hdp ON (hdp.`id_hd_department` = a.`id_hd_department`)
		LEFT JOIN `'._DB_PREFIX_.'hd_department_lang` hdpl ON (hdp.`id_hd_department` = hdpl.`id_hd_department` AND hdpl.`id_lang` = '.$cookie->id_lang.')
		LEFT JOIN `'._DB_PREFIX_.'hd_priority` hpo ON (hpo.`id_hd_priority` = a.`id_hd_priority`)
		LEFT JOIN `'._DB_PREFIX_.'hd_priority_lang` hpol ON (hpo.`id_hd_priority` = hpol.`id_hd_priority` AND hpol.`id_lang` = '.$cookie->id_lang.')
		LEFT JOIN `'._DB_PREFIX_.'hd_status` hs ON (hs.`id_hd_status` = a.`id_hd_status`)
		LEFT JOIN `'._DB_PREFIX_.'hd_status_lang` hsl ON (hs.`id_hd_status` = hsl.`id_hd_status` AND hsl.`id_lang` = '.$cookie->id_lang.')
		LEFT JOIN `'._DB_PREFIX_.'hd_staff` st ON (st.`id_hd_staff` = a.`id_hd_staff`)
		LEFT JOIN `'._DB_PREFIX_.'employee` e ON (st.`id_employee` = e.`id_employee`)
		';
		$this->_where = '';
		if(isset($_GET['hd_ticketFilter_status']) && $id_hd_status = Tools::getValue('id_hd_status'))
			$this->_where .= ' AND hs.`id_hd_status`='.$id_hd_status; 
		if(isset($_GET['hd_ticketFilter_department']) && $id_hd_department = Tools::getValue('id_hd_department'))
			$this->_where .= ' AND hdp.`id_hd_department`='.$id_hd_department; 
		if(isset($_GET['hd_ticketFilter_priority']) && $id_hd_priority = Tools::getValue('id_hd_priority'))
			$this->_where .= ' AND hpo.`id_hd_priority`='.$id_hd_priority; 
			
		if($cookie->profile != LOF_ID_PROFILE_ADMIN){
			if(sizeOf($departments)){
				if(count($departments) == 1){
					$this->_where .= ' AND a.`id_hd_department`='.(int)($departments[0]);
				}else{
					$this->_where .= ' AND a.`id_hd_department` IN ('.implode(',', $departments).')';
				}
			}
		}
		
		return ;
	}
	
	
	
	public function renderForm() {
		global $currentIndex;
		$cookie = $this->context->cookie;
		
		if (!($obj = $this->loadObject(true)))
			return;
		$token = $this->token;
		if($obj->id){
			if(!HelpDeskHelper::checkPermission($cookie->id_employee, $obj))
				Tools::redirectAdmin($currentIndex.'&token='.$token);
		}
		$customers = Customer::getCustomers();
		$departments = HdDepartment::getDepartments( $cookie->id_lang );
		$objdepartments = array();
		if($departments)
			foreach($departments as &$department){
				if(Validate::isLoadedObject($objdepartment = new HdDepartment($department['id_hd_department'],$cookie->id_lang))){
					$objdepartments[] = $objdepartment;
				}
				$arrCusFields = array();
				$customfields = HdCustomFields::listFields($cookie->id_lang,true,$department['id_hd_department']);
				foreach($customfields as $cusField){
					if(Validate::isLoadedObject($objcus = new HdCustomFields($cusField['id_hd_custom_field'],$cookie->id_lang))){
						$valueFields = HdCustomFields::getCustomFields($objcus->id, $obj->id);
						$objcus->lofValues = ($valueFields ? $valueFields[0] : '');
						$arrCusFields[] = $objcus;
					}
				}
				$department['customfields'] = $arrCusFields;
			}
		$priorities = HdPriority::getPriorities($cookie->id_lang);
		
		$agrs = array('obj' => $obj, 'customers' => $customers, 'departments' => $departments, 'objdepartments' => $objdepartments,
					'priorities' => $priorities, 'token' => $token, 'l' => $this->translateLanguages());
		return $this->renderLayout( "admin/ticketform", $agrs );
	}
	
	public function postProcess($token = NULL) {
		global $currentIndex, $lof;
		$cookie = $this->context->cookie;
		$this->tabAccess = Profile::getProfileAccess($cookie->profile, $this->id);
		
		if (Tools::isSubmit('submitAdd'.$this->table) OR Tools::isSubmit('submitAdd'.$this->table.'AndOther') OR Tools::isSubmit('submitAdd'.$this->table.'AndStay') OR Tools::isSubmit('editTicket')){
			$id = (int)(Tools::getValue($this->identifier));
			if (isset($id) AND $id){
				if ($this->tabAccess['edit'] === '1'){
					$customFieldsValue = array();
					if(!$id_hd_department = Tools::getValue('id_hd_department')){
						$this->errors[] = Tools::displayError('Department is required');
					}else{
						$customfields = HdCustomFields::listFields($cookie->id_lang,true,$id_hd_department);
						foreach($customfields as $cusField){
							if(Validate::isLoadedObject($objcus = new HdCustomFields($cusField['id_hd_custom_field'],$cookie->id_lang))){
								if($objcus->type != 'freetext'){
									$cusValue = Tools::getValue($objcus->name);
									if($objcus->required && !$cusValue){
										$this->errors[] = Tools::displayError($objcus->title.' is required');
									}
									if(!$cusValue && !Validate::isCleanHtml($cusValue)){
										$this->errors[] = Tools::displayError($objcus->title.' is not validate');
									}
									$customFieldsValue[] = array('value' => $cusValue,'id_hd_custom_field'=> $objcus->id);
								}
							}
						}
					}
					
					$this->validateRules();
					if (!sizeof($this->errors)) {
						$object = new $this->className($id);
						if (Validate::isLoadedObject($object)){
							$id_staff = $object->id_hd_staff;
							$id_status = $object->id_hd_status;
							$id_department = $object->id_hd_department;
							$id_customer = $object->id_customer;
							$id_priority = $object->id_hd_priority;
							
							$this->copyFromPost($object, $this->table);
							
							$result = $object->update();
							if (!$result){
								$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b>';
							}else{
								HdCustomFields::deteleCustomFieldValue(false, $object->id );
								if($customFieldsValue)
									foreach($customFieldsValue as $v){
										if(is_array($v['value'])){
											$value = implode('::',$v['value']);
										}else{
											$value = $v['value'];
										}
										HdCustomFields::addCustomFieldValue($v['id_hd_custom_field'], $object->id ,$value);
									}
								/* ADD History */
								$objHistory = new HdTicketHistory();
								$exists = false;
								if($id_staff != $object->id_hd_staff){
									$objHistory->change_staff = $object->id_hd_staff;
									$exists = true;
								}
								if($id_status != $object->id_hd_status){
									$objHistory->change_status = $object->id_hd_status;
									$exists = true;
								}
								if($id_department != $object->id_hd_department){
									$objHistory->change_department = $object->id_hd_department;
									$exists = true;
								}
								if($id_customer != $object->id_customer){
									$objHistory->change_customer = $object->id_customer;
									$exists = true;
								}
								if($id_priority != $object->id_hd_priority){
									$objHistory->change_priority = $object->id_hd_priority;
									$exists = true;
								}
								if($exists){
									$objHistory->id_hd_ticket = $object->id_hd_ticket;
									$objHistory->ip = $_SERVER['REMOTE_ADDR'];
									$objHistory->id_employee = $cookie->id_employee;
									
									$objHistory->add();
								}
								/* SEND EMAIL WHEN STAFF ASSSIGNED */
								$objDepart = new HdDepartment($object->id_hd_department);
								if($objDepart->notify_assign && $object->id_hd_staff && $object->id_hd_staff != $id_staff){
									$objStaff = new HdStaff($object->id_hd_staff);
									$objEmp = new Employee($objStaff->id_employee);
									$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_TO_STAFF_WHEN_ASSIGNED,$cookie->id_lang);
									$subject = $mailTMPL->subject;
									if(in_array(LOF_EMAILS_SEND_TO_STAFF_WHEN_ASSIGNED, $lof['skip_edit_subject'])){
										$subject = $object->subject[$cookie->id_lang];
									}
									$messages = HdTicketMessage::getTicketMessages($object->id, true ,$cookie->id_lang);
									$Topmessage = $messages[(count($messages) - 1)];
									HelpDeskHelper::sendMail( array($objEmp->email), array($objEmp->firstname.' '.$objEmp->lastname), $subject, $mailTMPL->message, $Topmessage['id_hd_ticket_message'], 'attach', $object->id, $objDepart->id);
								}
								
								if(Tools::isSubmit('editTicket')){
									Tools::redirectAdmin($currentIndex.'&id_hd_ticket='.$object->id.'&viewhd_ticket&conf=4&token='.Tools::getValue('token').'&lofTab=fragment-2');
								}
								if(Tools::isSubmit('submitAdd'.$this->table)){
									Tools::redirectAdmin($currentIndex.'&conf=4&token='.Tools::getValue('token'));
								}elseif(Tools::isSubmit('submitAdd'.$this->table.'AndOther')){
									Tools::redirectAdmin($currentIndex.'&conf=4&token='.Tools::getValue('token').'&add'.$this->table);
								}else{
									Tools::redirectAdmin($currentIndex.'&conf=4&token='.Tools::getValue('token').'&add'.$this->table.'&'.$this->identifier.'='.$object->id);
								}
							}
						}else
							$this->errors[] = Tools::displayError('An error occurred while updating object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
					}
				}else
					$this->errors[] = Tools::displayError('You do not have permission to edit here.');
			}else{
				if ($this->tabAccess['add'] === '1'){
					if(!Tools::getValue('message'))
						$this->errors[] = Tools::displayError('Field message is required.');
					if (!Validate::isString(Tools::getValue('message')))
						$this->errors[] = Tools::displayError('Invalid message');
						
					$customFieldsValue = array();
					if(!$id_hd_department = Tools::getValue('id_hd_department')){
						$this->errors[] = Tools::displayError('Department is required');
					}else{
						$customfields = HdCustomFields::listFields($cookie->id_lang,true,$id_hd_department);
						foreach($customfields as $cusField){
							if(Validate::isLoadedObject($objcus = new HdCustomFields($cusField['id_hd_custom_field'],$cookie->id_lang))){
								if($objcus->type != 'freetext'){
									$cusValue = Tools::getValue($objcus->name);
									if($objcus->required && !$cusValue){
										$this->errors[] = Tools::displayError($objcus->title.' is required');
									}
									if(!$cusValue && !Validate::isCleanHtml($cusValue)){
										$this->errors[] = Tools::displayError($objcus->title.' is not validate');
									}
									$customFieldsValue[] = array('value' => $cusValue,'id_hd_custom_field'=> $objcus->id);
								}
							}
						}
					}
					$helper = new HelpDeskHelper();
					$files = @$_FILES['lof_files'];
					$id_hd_department = Tools::getValue('id_hd_department');
					$objDepart = new HdDepartment($id_hd_department);
					if(is_array($files))
						if(!$correct_files = $helper->checkFileUpload($files, $objDepart)){
							if (sizeof($helper->errors)) {
								foreach($helper->errors as $row){
									$this->errors[] = $row;
								}
							}
						}
					$this->validateRules();
					if (!sizeof($this->errors)) {
						$object = new $this->className();
						$this->copyFromPost($object, $this->table);
						$object->id_hd_staff = HelpDeskHelper::assignStaff($objDepart);
						$type = ($objDepart->generation_rule ? 'random' : 'sequential');
						$object->code = HelpDeskHelper::generateCode($objDepart, 5, $type);
						
						if (!$object->add()){
							$this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>'.$this->table.'</b>';
						}else{
							if($customFieldsValue)
								foreach($customFieldsValue as $v){
									if(is_array($v['value']))
										$value = implode('::',$v['value']);
									else $value = $v['value'];
									HdCustomFields::addCustomFieldValue($v['id_hd_custom_field'], $object->id ,$value);
								}
							
							$objMessage = new HdTicketMessage();
							$objMessage->message = Tools::getValue('message');
							$objMessage->id_hd_ticket = $object->id;
							$objMessage->id_employee = $cookie->id_employee;
							
							if(!$objMessage->add()){
								$this->errors[] = Tools::displayError('An error occurred while creating object.').' <b>hd_ticket_messages</b>';
							}else{
								$result = true;
								if(isset($correct_files) && $correct_files){
									$result = $helper->addAttachments($object->id,$objMessage->id,$objDepart,$correct_files);
								}
								if($result){
									/* START SEND EMAIL */
									/* customer */
									global $lof;
									$objCus = new Customer($object->id_customer);
									$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_TICKET, $cookie->id_lang);
									$subject = $mailTMPL->subject;
									if(in_array(LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_TICKET,$lof['skip_edit_subject'])){
										$subject = $object->subject[$cookie->id_lang];
									}
									HelpDeskHelper::sendMail( array($objCus->email), array($objCus->firstname.' '.$objCus->lastname), $subject, $mailTMPL->message, $objMessage->id, 'message');
									/* staff */
									if($objDepart->notify_assign && $object->id_hd_staff){
										$objStaff = new HdStaff($object->id_hd_staff);
										$objEmp = new Employee($objStaff->id_employee);
										$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_TO_STAFF_WHEN_ASSIGNED,$cookie->id_lang);
										$subject = $mailTMPL->subject;
										if(in_array(LOF_EMAILS_SEND_TO_STAFF_WHEN_ASSIGNED,$lof['skip_edit_subject'])){
											$subject = $object->subject[$cookie->id_lang];
										}
										HelpDeskHelper::sendMail( array($objEmp->email), array($objEmp->firstname.' '.$objEmp->lastname), $subject, $mailTMPL->message, $objMessage->id, 'attach');
									}
									/* department notify section */
									if($objDepart->notify_new_tickets_to){
										$emails = str_replace("\r\n", "\n", $objDepart->notify_new_tickets_to);
										$toNotify = explode("\n", $emails);
										$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_TO_DEPARTMENT_NOTIFY_SECTION,$cookie->id_lang);
										$subject = $mailTMPL->subject;
										if(in_array(LOF_EMAILS_SEND_TO_DEPARTMENT_NOTIFY_SECTION,$lof['skip_edit_subject'])){
											$subject = $object->subject[$cookie->id_lang];
										}
										$arremails = array();
										foreach($toNotify as $t){
											if(Validate::isEmail($t)){
												$arremails[] = $t;
											}
										}
										if($arremails)
											HelpDeskHelper::sendMail( $arremails, array(), $subject, $mailTMPL->message, $objMessage->id, 'attach');
									}
									/* Notify keywords */
									$params = new HdParams('');
									if($params->get('notice_email_address')){
										$to = explode(',',$params->get('notice_email_address'));
										$emails = array();
										foreach($to as $t){
											if(Validate::isEmail($t)){
												$emails[] = $t;
											}
										}
										if($emails){
											if($params->get('notice_not_allowed_keywords')){
												$keywords = explode(',',$params->get('notice_not_allowed_keywords'));
												$a = str_replace($keywords,' ', $objMessage->message,$count);
												if($count){
													$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_WHEN_TICKET_CONTAIN_KEYWORDS,$cookie->id_lang);
													$subject = $mailTMPL->subject;
													if(in_array(LOF_EMAILS_SEND_NOTIFICATION_WHEN_TICKET_CONTAIN_KEYWORDS,$lof['skip_edit_subject'])){
														$subject = $object->subject[$cookie->id_lang];
													}
													HelpDeskHelper::sendMail( $emails, array(), $subject, $mailTMPL->message, $objMessage->id, 'other');
												}
											}
											if($params->get('notice_max_replies_nr') && Validate::isInt($params->get('notice_max_replies_nr')) && $params->get('notice_max_replies_nr') == 1 && !$object->id_hd_staff){
												$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_UNASSIGNED,$cookie->id_lang);
												$subject = $mailTMPL->subject;
												if(in_array(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_UNASSIGNED,$lof['skip_edit_subject'])){
													$subject = $object->subject[$cookie->id_lang];
												}
												HelpDeskHelper::sendMail( $emails, array(), $subject, $mailTMPL->message, $objMessage->id, 'other');
											}
											if($params->get('notice_replies_with_no_response_nr') && Validate::isInt($params->get('notice_replies_with_no_response_nr')) && $params->get('notice_replies_with_no_response_nr') == 1 && $object->id_hd_staff){
												$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_ASSIGNED,$cookie->id_lang);
												$subject = $mailTMPL->subject;
												if(in_array(LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_ASSIGNED,$lof['skip_edit_subject'])){
													$subject = $object->subject[$cookie->id_lang];
												}
												HelpDeskHelper::sendMail( $emails, array(), $subject, $mailTMPL->message, $objMessage->id, 'other');
											}
										}
									}
									/* END SEND EMAIL */
									
									if(Tools::isSubmit('submitAdd'.$this->table)){
										Tools::redirectAdmin($currentIndex.'&conf=3&token='.Tools::getValue('token'));
									}elseif(Tools::isSubmit('submitAdd'.$this->table.'AndOther')){
										Tools::redirectAdmin($currentIndex.'&conf=3&token='.Tools::getValue('token').'&add'.$this->table);
									}else{
										Tools::redirectAdmin($currentIndex.'&conf=3&token='.Tools::getValue('token').'&add'.$this->table.'&'.$this->identifier.'='.$object->id);
									}
								}else{
									$this->errors[] = Tools::displayError('can\'t upload file.').'</b>';
								}
							}
						}
					}
				}else
					$this->errors[] = Tools::displayError('You do not have permission to add here.');
			}
		}
		/* Delete object */
		elseif (isset($_GET['delete'.$this->table]))
		{
			if ($this->tabAccess['delete'] === '1'){
				if (Validate::isLoadedObject($object = $this->loadObject())) {
					if ($object->delete()){
						Tools::redirectAdmin($currentIndex.'&conf=1&token='.Tools::getValue('token'));
					}
					$this->errors[] = Tools::displayError('An error occurred during deletion.');
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while deleting object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
		}
		/* Delete multiple objects */
		elseif (Tools::getValue('submitDel'.$this->table)){
			if ($this->tabAccess['delete'] === '1'){
				if (isset($_POST[$this->table.'Box'])){
					$obj = new HdTicket();
					$result = true;
					$result = $obj->deleteSelection(Tools::getValue($this->table.'Box'));
					if ($result){
						Tools::redirectAdmin($currentIndex.'&conf=2&token='.Tools::getValue('token'));
					}
					$this->errors[] = Tools::displayError('An error occurred while deleting selection.');
				}
				else
					$this->errors[] = Tools::displayError('You must select at least one element to delete.');
			}
			else
				$this->errors[] = Tools::displayError('You do not have permission to delete here.');
			return;
		}/* Flagged */
		elseif (Tools::isSubmit('flagged'.$this->table) AND Tools::isSubmit($this->identifier)){
			$value = Tools::getValue('value');
			$id_hd_ticket = Tools::getValue('id_hd_ticket');
			if ($this->tabAccess['edit'] === '1') {
				if (Validate::isLoadedObject($object = $this->loadObject())){
					if ($object->updateFlagg($value,$id_hd_ticket)){
						Tools::redirectAdmin($currentIndex.'&conf=5&token='.Tools::getValue('token'));
					}
					else
						$this->errors[] = Tools::displayError('An error occurred while updating Flagged.');
				}
				else
					$this->errors[] = Tools::displayError('An error occurred while updating Flagged for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
			}
			else{
				$this->errors[] = Tools::displayError('You do not have permission to edit here.');
			}
		}
		parent::postProcess();
	}
	public function renderView(){
		global $currentIndex, $lof;
		$cookie = $this->context->cookie;
		
		$token = Tools::getValue('token');
		$expandform = false;
		$loferrors = array();
		$id_hd_ticket = Tools::getValue('id_hd_ticket');
		$objTicket = new HdTicket($id_hd_ticket);
		if(!HelpDeskHelper::checkPermission($cookie->id_employee, $objTicket))
			Tools::redirectAdmin($currentIndex.'&token='.$token);
		if(Tools::isSubmit('submitAddMessage')){
			if(!Tools::getValue('message'))
				$loferrors[] = Tools::displayError('Field message is required.');
			if (!Validate::isString(Tools::getValue('message')))
				$loferrors[] = Tools::displayError('Invalid message');
						
			$helper = new HelpDeskHelper();
			$files = @$_FILES['lof_files'];
			$id_hd_department = Tools::getValue('id_hd_department');
			$objDepart = new HdDepartment($id_hd_department);
			if(is_array($files))
				if(!$correct_files = $helper->checkFileUpload($files, $objDepart)){
					if (sizeof($helper->errors)) {
						foreach($helper->errors as $row){
							$loferrors[] = $row;
						}
					}
				}
			if(!sizeof($loferrors)){
				$objMessage = new HdTicketMessage();
				$objMessage->message = Tools::getValue('message');
				$objMessage->id_hd_ticket = Tools::getValue('id_hd_ticket');;
				$objMessage->id_employee = $cookie->id_employee;
				if(!$objMessage->add()){
					$loferrors[] = Tools::displayError('An error occurred while creating object.').' <b>hd_ticket_messages</b>';
				}else{
					$result = true;
					if(isset($correct_files) && $correct_files){
						$result = $helper->addAttachments($objMessage->id_hd_ticket, $objMessage->id, $objDepart, $correct_files);
					}
					if(!$result){
						$loferrors[] = Tools::displayError('can\'t upload file.');
					}else{
						/* START SEND MAIL */
						/* send to customer */
						$objTicket = new HdTicket($objMessage->id_hd_ticket);
						$objCus = new Customer($objTicket->id_customer);
						$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_REPLY, $cookie->id_lang);
						$subject = $mailTMPL->subject;
						if(in_array(LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_REPLY,$lof['skip_edit_subject'])){
							$subject = $objTicket->subject[$cookie->id_lang];
						}
						HelpDeskHelper::sendMail( array($objCus->email), array($objCus->firstname.' '.$objCus->lastname), $subject, $mailTMPL->message, $objMessage->id, 'other');
						/* END SEND MAIL */
					}
				}
			}
			if(sizeof($loferrors)){
				$expandform = true;
			}
		}elseif(isset($_GET['deleteMessage'])){
			$id_hd_ticket_message = Tools::getValue('id_hd_ticket_message');
			if(Validate::isLoadedObject($objMess = new HdTicketMessage($id_hd_ticket_message))){
				$message = $objMess->message;
				if($objMess->delete()){
					$objHistory = new HdTicketHistory();
					$objHistory->change_message = CHANGE_MESSAGE_DELETE;
					$objHistory->id_hd_ticket_message = $id_hd_ticket_message;
					$objHistory->message = $message;
					
					$objHistory->id_hd_ticket = $id_hd_ticket;
					$objHistory->ip = $_SERVER['REMOTE_ADDR'];
					$objHistory->id_employee = $cookie->id_employee;
					$objHistory->add();
					Tools::redirectAdmin($currentIndex.'&conf=1&viewhd_ticket&id_hd_ticket='.$id_hd_ticket.'&token='.$token);
				}
			}
		}elseif(isset($_GET['reopenMessage']) || isset($_GET['closeMessage'])){
			$arrStaff = HdStaff::getStaffByEmployee($cookie->id_employee);
			if(Validate::isLoadedObject($objTicket = new HdTicket($id_hd_ticket))){
				if(in_array($objTicket->id_hd_staff, $arrStaff) || $cookie->profile == LOF_ID_PROFILE_ADMIN){
					$objTicket->id_hd_status = (isset($_GET['reopenMessage']) ? LOF_STATUS_OPEN : LOF_STATUS_CLOSE);
					if(!$objTicket->update()){
						$loferrors[] = Tools::displayError('update ticket is error.');
					}else{
						$objHistory = new HdTicketHistory();
						$objHistory->change_status = $objTicket->id_hd_status;
						$objHistory->id_hd_ticket = $id_hd_ticket;
						$objHistory->ip = $_SERVER['REMOTE_ADDR'];
						$objHistory->id_employee = $cookie->id_employee;
						$objHistory->add();
					
						Tools::redirectAdmin($currentIndex.'&viewhd_ticket&id_hd_ticket='.$id_hd_ticket.'&token='.$token);
					}
				}else{
					$loferrors[] = Tools::displayError('You do not have permission to edit here.');
				}
			}else{
				$loferrors[] = Tools::displayError('Object is can not load');
			}
		}
		//parent::renderForm();
		
		$ticket = HdTicket::getTickets($cookie->id_lang, $id_hd_ticket );
		if(!$ticket)
			return '';
		$obj = new HdTicket($id_hd_ticket);
		$objDepartment = new HdDepartment($obj->id_hd_department);
		$token = $this->token;
		$fields = HdCustomFields::listFields($cookie->id_lang, true, $obj->id_hd_department);
		if($fields){
			foreach($fields as &$field){
				$lofvalue = HdCustomFields::getCustomFields($field['id_hd_custom_field'], $id_hd_ticket);
				$value = '';
				if($lofvalue){
					$values = explode('::',$lofvalue[0]);
					foreach($values as &$row)
						$row = trim($row);
					$value = implode(', ',$values);
				}
				$field['lofvalue'] = $value;
			}
		}
		$notices = HdTicketNotice::getNotices($cookie->id_lang,$id_hd_ticket);	
		$messages = HdTicketMessage::getTicketMessages($obj->id);
		
		foreach($messages as &$m){
			if($m['id_employee'])
				$m['objEmp'] = new Employee($m['id_employee']);
			elseif($m['id_customer'])
				$m['objCus'] = new Customer($m['id_customer']);
			$m['files'] = HdTicketMessage::getFiles($m['id_hd_ticket_message']);
		}
		$params = new HdParams('');
		$Topmessage = ($messages ? $messages[(count($messages) - 1)] : array());
		array_pop($messages);
		//echo "<pre>".print_r($Topmessage,1); die;
		$departments = HdDepartment::getDepartments( $cookie->id_lang );
		$objdepartments = array();
		if($departments)
			foreach($departments as &$department){
				if(Validate::isLoadedObject($objdepartment = new HdDepartment($department['id_hd_department'],$cookie->id_lang))){
					$objdepartments[] = $objdepartment;
				}
				$arrCusFields = array();
				$customfields = HdCustomFields::listFields($cookie->id_lang,true,$department['id_hd_department']);
				foreach($customfields as $cusField){
					if(Validate::isLoadedObject($objcus = new HdCustomFields($cusField['id_hd_custom_field'],$cookie->id_lang))){
						$valueFields = HdCustomFields::getCustomFields($objcus->id, $obj->id);
						$objcus->lofValues = ($valueFields ? $valueFields[0] : '');
						$arrCusFields[] = $objcus;
					}
				}
				$department['customfields'] = $arrCusFields;
				$department['employees'] = HdDepartment::getEmployeeByDepartment($department['id_hd_department']);
			}
		$priorities = HdPriority::getPriorities($cookie->id_lang);
		$statuss = HdStatus::getStatus($cookie->id_lang);
		$customers = Customer::getCustomers();
		$objStaff = new HdStaff($obj->id_hd_staff);
		/* get History */
		$histoties = HdTicketHistory::getHistories($id_hd_ticket, $cookie->id_lang);
		if($histoties)
			foreach($histoties as &$h){
				if($h['id_customer'])
					$h['objCus'] = new Customer($h['id_customer']);
				else
					$h['objEmp'] = new Employee($h['id_employee']);
			}
		
		$agrs = array('obj' => $obj,'objStaff' => $objStaff, 'priorities' => $priorities, 'objdepartments' => $objdepartments, 'departments' => $departments, 'statuss' => $statuss,
				'token' => $token, 'messages' => $messages, 'Topmessage' => $Topmessage, 'notices' => $notices, 'params' => $params, 'currentIndex' => $currentIndex, 'fields' => $fields,
				'cookie' => $cookie, 'ticket' => $ticket[0], 'objDepartment' => $objDepartment, 'expandform' => $expandform, 'errors' => $loferrors, 'lofTab' => Tools::getValue('lofTab','fragment-1'), 
				'lofToken' => $this->lofToken, 'customers'=> $customers, 'histoties'=> $histoties, 'l'=> $this->translateLanguages()
			);
		
		return $this->renderLayout( "admin/viewticket", $agrs );
	}
	
	public function translateLanguages(){
		return array(
			'The maximum number of attachments has been reached. You cannot add any more attachments' => $this->l('The maximum number of attachments has been reached. You cannot add any more attachments.'),
			'Edit' => $this->l('Edit'),
			'Create' => $this->l('Create'),
			'New Ticket' => $this->l('New Ticket'),
			'Customer' => $this->l('Customer'),
			'Filter' => $this->l('Filter'),
			'Departments' => $this->l('Departments'),
			'Subject' => $this->l('Subject'),
			'Priority' => $this->l('Priority'),
			'Message' => $this->l('Message'),
			'Attachments' => $this->l('Attachments'),
			'Allowed attachments are' => $this->l('Allowed attachments are'),
			'Save' => $this->l('Save'),
			'Save And Another' => $this->l('Save And Add New'),
			'Save And Stay' => $this->l('Save And Stay'),
			'Required field' => $this->l('Required field'),
			'Add attachment' => $this->l('Add attachment'),
			'Replies' => $this->l('Replies'),
			'Ticket Information' => $this->l('Ticket Information'),
			'Ticket History' => $this->l('Ticket History'),
			'Submitter Information' => $this->l('Submitter Information'),
			'avatar' => $this->l('avatar'),
			'Attachment' => $this->l('Attachment'),
			'download' => $this->l('download'),
			'times' => $this->l('times'),
			'reply' => $this->l('reply'),
			'Click here to reply' => $this->l('Click here to reply'),
			'view notices' => $this->l('View notices'),
			'Reopen Ticket' => $this->l('Reopen Ticket'),
			'Close Ticket' => $this->l('Close Ticket'),
			'Print' => $this->l('Print'),
			'There are' => $this->l('There are'),
			'There is' => $this->l('There is'),
			'errors' => $this->l('errors'),
			'error' => $this->l('error'),
			'This ticket is marked as closed. No new ticket replies can be added' => $this->l('This ticket is marked as closed. No new ticket replies can be added.'),
			'Please give us your feedback so we can improve our Customer Support. How would you rate this answer' => $this->l('Please give us your feedback so we can improve our Customer Support. How would you rate this answer?'),
			'one star' => $this->l('one star'),
			'two star' => $this->l('two star'),
			'three star' => $this->l('three star'),
			'four star' => $this->l('four star'),
			'five star' => $this->l('five star'),
			'avatar' => $this->l('avatar'),
			'wrote' => $this->l('wrote'),
			'Edit' => $this->l('Edit'),
			'Delete' => $this->l('Delete'),
			'General' => $this->l('General'),
			'Update' => $this->l('Update'),
			'Department' => $this->l('Department'),
			'Date' => $this->l('Date'),
			'Status' => $this->l('Status'),
			'Code' => $this->l('Code'),
			'Priority' => $this->l('Priority'),
			'Assigned To' => $this->l('Assigned To'),
			'Unassigned' => $this->l('Unassigned'),
			'Customer' => $this->l('Customer'),
			'Custom Fields' => $this->l('Custom Fields'),
			'Update' => $this->l('Update'),
			'changed' => $this->l('changed'),
			'deleted message id' => $this->l('deleted message id'),
			'edit message id' => $this->l('edit message id'),
			'seconds' => $this->l('seconds'),
			'second' => $this->l('second'),
			'ago' => $this->l('ago'),
			'minutes' => $this->l('minutes'),
			'minute' => $this->l('minute'),
			'hours' => $this->l('hours'),
			'hour' => $this->l('hour'),
			'days' => $this->l('days'),
			'day' => $this->l('day'),
			'months' => $this->l('months'),
			'month' => $this->l('month'),
			'years' => $this->l('years'),
			'year' => $this->l('year'),
			'User Agent' => $this->l('User Agent'),
			'HTTP Referrer' => $this->l('HTTP Referrer'),
			'IP Address' => $this->l('IP Address'),
			'Employee' => $this->l('Employee'),
			'to' => $this->l('to'),
			'Customer' => $this->l('Customer'),
			'Priority' => $this->l('Priority'),
			'Department' => $this->l('Department'),
			'Status' => $this->l('Status'),
			'in this ticket' => $this->l('in this ticket'),
			'Assign Order' => $this->l('Assign order'),
			'select order' => $this->l('select order'),
			'Assign Product' => $this->l('Assign product'),
			'select product' => $this->l('select product'),
			'view order' => $this->l('view order'),
			'Order' => $this->l('Order #'),
			'view product' => $this->l('view product'),
			'Use template' => $this->l('Use template'),
			'No Template' => $this->l('--- No Template ---'),
			'Product' => $this->l('Product')
		);
	}
}


?>