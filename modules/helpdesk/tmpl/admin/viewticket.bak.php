<?php

if (!defined('_CAN_LOAD_FILES_'))
	exit;
?>
	<link rel="stylesheet" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/css/global.css";?>" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/css/jquery-ui.css";?>" type="text/css" media="screen" charset="utf-8" />
	<script type="text/javascript" src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/js/form.js";?>"></script>
	<?php
	echo '
	<script src="'.__PS_BASE_URI__.'js/jquery/jquery.fancybox-1.3.4.js" type="text/javascript"></script>	
	<link href="'.__PS_BASE_URI__.'css/jquery.fancybox-1.3.4.css"  type="text/css" rel="stylesheet"/>
	<link href="'._MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/assets/admin/form.css" type="text/css" rel="stylesheet"/>
	<script type="text/javascript">
		$(document).ready( function(){ 
			$(".fancy-ajax").fancybox({
				"width"				: 920,
				"height"			: 350,	
				\'type\'				: \'iframe\',
				"scrolling" : "no"
			});
			$(".fancy-ajax1").fancybox({
				"width"				: 920,
				"height"			: 615,	
				\'type\'				: \'iframe\',
				"scrolling" : "no"
			});
		} );
	</script>'; ?>
	
	<div class="clearfix ui-tabs ui-widget ui-widget-content ui-corner-all" id="lof-pdf-tab">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-1"><span><?php echo $this->l("Replies");?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-2"><span><?php echo $this->l("Ticket Information");?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-3"><span><?php echo $this->l("Submitter Information");?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-4"><span><?php echo $this->l("Custom Fields");?></span></a></li>
		</ul>
		
		<div id="fragment-1" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
		
		<!-- top -->
		<div class="topmessage lofitem">
			<div class="lofcontainer">
				<div class="lofheader">
					<?php if($Topmessage['id_employee']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/images/staff-icon.png" alt="{l s='avatar' mod='helpdesk'}"/></div>
					<?php }elseif($Topmessage['id_customer']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/images/user-icon.png" alt="{l s='avatar' mod='helpdesk'}"/></div>
					<?php } ?>
					<div class="lofname">
						<?php if(isset($Topmessage['objEmp'])){ ?>
							<?php if($params->get('show_email_link')){?>
								<a href="mailto:<?php echo $Topmessage['objEmp']->email;?>"><?php 
							}?>
								<span class="name_email">
								<?php if($params->get('show_user_info') == 'name'){
									echo $Topmessage['objEmp']->firstname.' '.$Topmessage['objEmp']->lastname;
								}else{ 
									echo $Topmessage['objEmp']->email;
								}?>
								</span>
							<?php 
							if($params->get('show_email_link')){?>
								</a>
							<?php } ?>
						<?php }elseif(isset($Topmessage['objCus'])){ ?>
								<?php if($params->get('show_email_link')){?>
									<a href="mailto:<?php echo $Topmessage['objCus']->email;?>">
								<?php }?>
								<span class="name_email">
								<?php
								if($params->get('show_user_info') == 'name'){ 
									echo $Topmessage['objCus']->firstname.' '.$Topmessage['objCus']->lastname ;
								}else{
									echo $Topmessage['objCus']->email;
								}
								?>
								</span>
							<?php
								if($params->get('show_email_link')){?>
									</a>
								<?php }?>
						<?php } ?>
					</div>
					<div class="lofdate">
						<span class="lof_date"><?php echo Tools::displayDate($Topmessage['date_add'],$cookie->id_lang, true, ':');?></span>
					</div>
				</div>
				<div class="lofcontent">
					<h2><?php echo $ticket['subject'];?></h2>
					<?php echo $Topmessage['message'] ?>
				</div>
				<?php if($Topmessage['files']){ ?>
					<div class="files">
						<?php foreach($Topmessage['files'] as $file){ ?>
							<div class="file"><a href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/download.php?id_file=<?php echo $file['id_hd_ticket_files'];?>" title=""><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/images/attach.png" alt="<?php echo $this->l('Attachment');?>"/>&nbsp;<?php echo $file['filename'];?></a>
							<span><?php echo $this->l('download').' '.$file['downloads'].' '.$this->l('times');?></span></div>	
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
		
		<div class="lofaction">
			<?php if($ticket['id_hd_status'] != LOF_STATUS_CLOSE) { ?>
			<div class="lofaddnew">
				<a <?php echo ($expandform ? ' style="display:none"' : '');?> class="click_reply button" href="javascript:void(0)" title="<?php echo $this->l('reply');?>">
					<img border="0" src="../img/admin/add.gif">
					<?php echo $this->l('Click here to reply');?>
				</a>
			</div>
			<?php } ?>
			<div class="lofnotice"><a class="fancy-ajax1 button" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/viewNotices.php?id_hd_ticket='.$obj->id_hd_ticket.'&viewNotice&lofToken='.$lofToken;?>"><?php echo $this->l('view notices');?> (<?php echo count($notices); ?>)</a></div>
			<?php if($ticket['id_hd_status'] == LOF_STATUS_CLOSE) { ?>
				<div class="lofac"><a class="button" href="<?php echo $currentIndex.'&viewhd_ticket&id_hd_ticket='.$obj->id_hd_ticket.'&reopenMessage&token='.$token;?>" title="<?php echo $this->l('Reopen Ticket');?>"><?php echo $this->l('Reopen Ticket');?></a></div>
			<?php }else{ ?>
				<div class="lofac"><a class="button" href="<?php echo $currentIndex.'&viewhd_ticket&id_hd_ticket='.$obj->id_hd_ticket.'&closeMessage&token='.$token;?>"><?php echo $this->l('Close Ticket');?></a></div>
			<?php } ?>
		</div>
		
		<div class="lofformmessage"<?php echo (!$expandform ? ' style="display:none"' : '');?>>
		<?php if($errors){ ?> 
			<div class="error">
				<p><?php echo (count($errors) > 1 ? $this->l('There are') : $this->l('There is')).' '.(count($errors) > 1 ? $this->l('errors') : $this->l('error'));?></p>
				<ol>
				<?php foreach($errors as $e){ ?>
					<li><?php echo $e;?></li>	
				<?php }?>	
				</ol>
			</div>
		<?php } ?>	
			<?php include_once( dirname(__FILE__)."/messagefrom.php" );?>
		</div>
		
		<?php if($ticket['id_hd_status'] != LOF_STATUS_CLOSE){ ?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					$('.click_reply').click(function(){
						$(this).css({'display':'none'});
						$('.lofformmessage').css({'display':'block'});
					});
				});
			</script>
		<?php }else{
			echo '<p style="margin-left: 6px;">'.$this->l('This ticket is marked as closed. No new ticket replies can be added.').'</p>';
		} ?>
		
		<?php if($ticket['id_hd_status'] == LOF_STATUS_CLOSE){ ?>
			<div class="lofmessa">
				<p><?php echo $this->l('Please give us your feedback so we can improve our Customer Support. How would you rate this answer?');?></p>
				<!-- feedback -->
				<ul class="lof_star_rating">
					<li id="lof_current_rating" class="lof_feedback_selected_<?php echo $obj->feedback; ?>">&nbsp;</li>
					<?php if ($obj->feedback == 0 && $obj->id_hd_staff) { ?>
					<li title="<?php echo $this->l('one star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(1, <?php echo $obj->id; ?>);" class="lof_one_star" id="lof_feedback_1">&nbsp;</a></li>
					<li title="<?php echo $this->l('two star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(2, <?php echo $obj->id; ?>);" class="lof_two_stars" id="lof_feedback_2">&nbsp;</a></li>
					<li title="<?php echo $this->l('three star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(3, <?php echo $obj->id; ?>);" class="lof_three_stars" id="lof_feedback_3">&nbsp;</a></li>
					<li title="<?php echo $this->l('four star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(4, <?php echo $obj->id; ?>);" class="lof_four_stars" id="lof_feedback_4">&nbsp;</a></li>
					<li title="<?php echo $this->l('five star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(5, <?php echo $obj->id; ?>);" class="lof_five_stars" id="lof_feedback_5">&nbsp;</a></li>
					<?php } ?>
				</ul>
			</div>
		<?php }?>
	<?php if($messages)
			foreach($messages as $m){ ?>
				<div class="lofitem">
					<div class="lofheader">
						<?php if($m['id_employee']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/staff-icon.png";?>" alt="<?php echo $this->l('avatar'); ?>"/></div>
						<? }elseif($m['id_customer']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/user-icon.png";?>" alt="<?php echo $this->l('avatar'); ?>"/></div>
						<?php } ?>
						<div class="lofname">
						<?php if(isset($m['objEmp'])){ ?>
							<?php echo ($params->get('show_email_link') ? '<a href="mailto:'.$m['objEmp']->email.'">' : '').'<span class="name_email">'.($params->get('show_user_info') == 'name' ? $m['objEmp']->firstname.' '.$m['objEmp']->lastname : $m['objEmp']->email ).'</span>'.($params->get('show_email_link') ? '</a>' : '')?>
						<?php }elseif($m['objCus']){ ?>
							<?php echo ($params->get('show_email_link') ? '<a href="mailto:'.$m['objCus']->email.'">' : '').'<span class="name_email">'.($params->get('show_user_info') == 'name' ? $m['objCus']->firstname.' '.$m['objCus']->lastname : $m['objCus']->email ).'</span>'.($params->get('show_email_link') ? '</a>' : '')?>
						<?php } ?>
							<span class="wrote"><?php echo $this->l('wrote');?>:</span>
						</div>
						<div class="lofdate">
						<?php echo Tools::displayDate($m['date_add'],$cookie->id_lang, true, ':');?>
						</div>
					</div>
					<div class="clear"></div>
					
					<div class="lofcontent" id="lofcontent_<?php echo $m['id_hd_ticket_message'];?>">
						<p><?php echo $m['message'];?></p>
					</div>
					<div class="lofaction itemlofaction" align="right">
						<a  class="fancy-ajax" onclick="return false;" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/editMessage.php?id_hd_ticket='.$obj->id_hd_ticket.'&editMessage&id_hd_ticket_message='.$m['id_hd_ticket_message'].'&id_lang='.(int)($cookie->id_lang).'&id_tag=lofcontent_'.$m['id_hd_ticket_message'].'&lofToken='.$lofToken;?>" title="">
							<img src="<?php echo _PS_IMG_.'admin/edit.gif';?>" alt="<?php echo $this->l('Edit');?>"/>
						</a>
						<a href="<?php echo $currentIndex.'&viewhd_ticket&id_hd_ticket='.$obj->id_hd_ticket.'&deleteMessage&id_hd_ticket_message='.$m['id_hd_ticket_message'].'&token='.$token;?>" title="">
							<img src="<?php echo _PS_IMG_.'admin/delete.gif';?>" alt="<?php echo $this->l('Delete');?>"/>
						</a>
					</div>
					<?php
					if($m['files']){?>
						<hr/>
						<?php foreach($m['files'] as $f) { ?>
						<div class="file"><a href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/download.php?id_file='.$f['id_hd_ticket_files'];?>" title="<?php echo 'download';?>"><img src="<?php echo __PS_BASE_URI__."modules/".LOF_MODULE_NAME_HELPDESK.'/assets/images/attach.png'; ?>" alt="<?php echo $this->l('Attachment');?>"/>&nbsp;<?php echo $f['filename']; ?></a>
							<span><?php echo $this->l('download').' '.$f['downloads'].' '.$this->l('times');?></span>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
	<?php 	} ?>
		</div>
		<!-- Ticket Infor -->
		<?php 
			$divLangName = 'subject';
		?>
		<div id="fragment-2" style="display:none;" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			<form action="" method="post" enctype="multipart/form-data">
				<fieldset class="loffieldset">
					<legend><?php echo $this->l('General');?></legend>
					<table class="lof">
						<tr><td width="200" class="left"><?php echo $this->l('Subject');?>: </td><td>
						<?php
						foreach ($this->_languages as $language)
							echo '
							<div id="subject_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
								<input size="40" type="text" name="subject_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'subject', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" style="width: 150px;" /><sup> *</sup>
							</div>';
						$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'subject');
						?>
						<?php //echo $ticket['subject'];?></td></tr>
						<tr><td class="left"><?php echo $this->l('Department');?>: </td><td><?php echo $ticket['de_part_name'];?></td></tr>
						<tr><td class="left"><?php echo $this->l('Date');?>: </td><td><?php echo Tools::displayDate($ticket['date_add'],$cookie->id_lang,true,':');?></td></tr>
						<tr><td class="left"><?php echo $this->l('Status');?>: </td><td><?php echo $ticket['status_name'];?></td></tr>
						<tr><td class="left"><?php echo $this->l('Code');?>: </td><td><?php echo $ticket['code'];?></td></tr>
						<tr><td class="left"><?php echo $this->l('Priority');?>: </td><td><?php echo $ticket['priority_name'];?></td></tr>
						<tr><td class="left"><?php echo $this->l('Assigned To');?>: </td><td><?php echo $ticket['staff'];?></td></tr>
						<tr><td class="left"><?php echo $this->l('Customer');?>: </td><td><?php echo $ticket['customer'];?></td></tr>
					</table>
				</fieldset>
				<?php if(isset($fields)){ ?>
					<div class="clear">&nbsp;</div>
					<fieldset class="loffieldset">
						<legend><?php echo $this->l('General');?></legend>
						<table class="lof">
						<?php foreach($fields as $field){ ?>
							<tr><td width="200" class="left"><?php echo $field['title'];?>: </td><td><?php echo $field['lofvalue'];?></td></tr>
						<?php } ?>
						</table>
					</fieldset>
				<?php } ?>
			</form>
		</div>
		<!-- Submitter Information  -->
		<div id="fragment-3" style="display:none;" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<div class="information">
				<div class="row">
					<span><?php echo $this->l('User Agent');?>: </span>
					<p><?php echo $ticket['agent'];?></p>
				</div>
				<div>
					<span><?php echo $this->l('HTTP Referrer');?>: </span>
					<p><?php echo $ticket['referer'];?></p>
				</div>
				<div class="row">
					<span><?php echo $this->l('IP Address');?>: </span>
					<p><?php echo $ticket['ip'];?></p>
				</div>
			</div>
		</div>
		<!--Custom Fields  -->
		<div id="fragment-4" style="display:none;" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<?php if(isset($fields)){ ?>
				<table class="lof">
				<?php foreach($fields as $field){ ?>
					<tr><td width="200" class="left"><?php echo $field['title'];?>: </td><td><?php echo $field['lofvalue'];?></td></tr>
				<?php } ?>
				</table>
			<?php } ?>
		</div>
	</div>