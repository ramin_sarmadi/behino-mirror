<?php 
defined('_HELPDESK') or die('Restricted access');
// add style and js
global $currentIndex;
$cookie = $this->context->cookie;
// TinyMCE

$iso = Language::getIsoById((int)($cookie->id_lang));
$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
$ad = dirname($_SERVER["PHP_SELF"]);
echo '
	<script type="text/javascript">	
	var iso = \''.$isoTinyMCE.'\' ;
	var pathCSS = \''._THEME_CSS_DIR_.'\' ;
	var ad = \''.$ad.'\' ;
	var ajaxUrl = "'._MODULE_DIR_.'helpdesk/lofajax.php";
	</script>';

echo '<script type="text/javascript">
		var dateObj = new Date();
		var hours = dateObj.getHours();
		var mins = dateObj.getMinutes();
		var secs = dateObj.getSeconds();
		if (hours < 10) { hours = "0" + hours; }
		if (mins < 10) { mins = "0" + mins; }
		if (secs < 10) { secs = "0" + secs; }
		var time = " "+hours+":"+mins+":"+secs;

		$(function() {
			$("#calendartime").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"yy-mm-dd"+time});
			$("#calendar").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"yy-mm-dd"});
		});
	</script>';

$divLangName = 'title¤cdescription¤clabels¤ctextarea¤textbox';
?>
<script type="text/javascript">
<?php 
		$str = '';
		foreach($this->_languages as $language){
			$str .= $language['id_lang'].',';
		}
		$str = rtrim($str,',');
	?>
var id_language_default = <?php echo $this->_defaultFormLanguage;?>;
var texttext = '<?php echo $l['Text'];?>';
var textvalue = '<?php echo $l['Value'];?>';
var textadd = '<?php echo $l['Add'];?>';
var languages = new Array(<?php echo $str; ?>);
</script>
<?php
echo '<form action="'.$currentIndex.'&id_hd_department='.($id_hd_department ? $id_hd_department : $obj->id_hd_department).'&token='.$token.'" method="post" enctype="multipart/form-data">
		'.($obj->id ? '<input type="hidden" name="'.$this->identifier.'" value="'.$obj->id.'" />' : '').'
			<input type="hidden" name="id_hd_department" value="'.($id_hd_department ? $id_hd_department : $obj->id_hd_department).'"/>
			<fieldset><legend><img src="../img/admin/time.gif" />'.$l['General'].'</legend>
				<div class="margin-form">
					<input type="submit" value="'.$l['Save'].'" name="submitAdd'.$this->table.'" class="button" />
					<input type="submit" value="'.$l['Save And Another'].'" name="submitAdd'.$this->table.'AndOther" class="button" />
					<input type="submit" value="'.$l['Save And Stay'].'" name="submitAdd'.$this->table.'AndStay" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$l['Required field'].'</div>
				
				<label>'.$l['Name'].': </label>
				<div class="margin-form">';
				echo '<input size="40" id="idname" type="text" name="name" value="'.htmlentities($this->getFieldValue($obj, 'name'), ENT_COMPAT, 'UTF-8').'" style="width: 150px;" /><sup> *</sup>';
		echo '	</div>
				
				<label>'.$l['Title'].': </label>
				<div class="margin-form">';
				foreach ($this->_languages as $language)
					echo '
					<div id="title_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="40" type="text" name="title_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'title', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" style="width: 150px;" /><sup> *</sup>
					</div>';							
				$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'title');
		echo '	</div>
				<div class="clear"></div>';
		echo 	'<label>'.$l['Active'].': </label>
				<div class="margin-form">
					<input type="radio" name="active" id="active_on" onclick="toggleDraftWarning(false);" value="1" '.(!$obj->id || $this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="active" id="active_off" onclick="toggleDraftWarning(true);" value="0" '.($obj->id && !$this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
				</div>';
		echo	'<label>'.$l['Required'].': </label>
				<div class="margin-form">
					<input type="radio" name="required" id="required_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'required') ? 'checked="checked" ' : '').'/>
					<label class="t" for="required_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="required" id="required_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'required') ? 'checked="checked" ' : '').'/>
					<label class="t" for="required_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
				</div>';
		echo 	'<label>'.$l['Type'].': </label>
				<div class="margin-form">
					<select name="type" id="lofchosetype">';
				foreach($fiedsType as $Type=>$text){
					echo '<option value="'.$Type.'"'.($Type == $obj->type ? ' selected="selected"' : '').'>'.$text.'</option>';
				}
		echo 	'	</select>
				</div>';
	
		echo '	<label>'.$l['Description'].': </label>
				<div class="margin-form">';
			foreach ($this->_languages as $language)
				echo '	<div id="cdescription_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
						<textarea class="rte" cols="80" rows="10" id="description_'.$language['id_lang'].'" name="description_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($obj, 'description', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
					</div>';
		$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'cdescription');
		echo '	</div><div class="clear space">&nbsp;</div>';
			/* LINH TINH */
			/* textarea */
		echo '	<div class="container-value">
				<div class="valuegeneral select multipleselect textbox calendar calendartime">
					<label>'.$l['Size'].': </label>
					<div class="margin-form">
						<input type="text" name="size" value="'.$obj->size.'">
					</div>
				</div>
				</div>';
		echo '	<div class="container-value">
				<div class="valuegeneral textarea">
					<label>'.$l['Cols'].': </label>
					<div class="margin-form">
						<input type="text" name="cols" value="'.$obj->cols.'">
					</div>
					<label>'.$l['Rows'].': </label>
					<div class="margin-form">
						<input type="text" name="rows" value="'.$obj->rows.'">
					</div>
				</div>
				</div>';
			/* ADD VALUES */
		echo	'<label>'.$l['Value'].': </label>
				<div class="margin-form">';
				
			$typeArr = array('checkbox','radio','select','multipleselect');
			/*
			if($obj->id)
				if(in_array($obj->type,$typeArr)){
			*/
			/* START checkbox','radio','select','multipleselect */
		echo '  <div class="container-value">';
		echo '  	<div class="valuegeneral select multipleselect checkbox radio"><br/>';
		echo '			<div class="lof-left">';
		echo '				<span class="lof">'.$l['Text'].': </span>';
							foreach ($this->_languages as $language)
					echo '	<div id="clabels_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
								<textarea cols="30" rows="10" id="labels_'.$language['id_lang'].'" name="labels_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($obj, 'labels', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
							</div>';
				$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'clabels');
				echo '	</div>';
		echo '			<div class="lof-right">';
		echo '				<span class="lof">'.$l['Value'].': </span>';
					echo '	<div style="float: left;">
								<textarea cols="30" rows="15" id="values" name="values">'.(in_array($obj->type,$typeArr) ? htmlentities(stripslashes($this->getFieldValue($obj, 'values')), ENT_COMPAT, 'UTF-8') : '').'</textarea>
							</div>';
				echo '	</div>
						<div class="clear">'.$l['each Text on a new line and each Value on a new line'].'</div>';
			echo '	</div>';
				/* END checkbox','radio','select','multipleselect */
					/* START textarea, freetext */
				echo '
					<div class="valuegeneral textarea freetext">';
						foreach ($this->_languages as $language)
					echo '	<div id="ctextarea_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').';float: left;">
							<textarea class="rte" cols="80" rows="10" id="textarea_'.$language['id_lang'].'" name="textarea_'.$language['id_lang'].'">'.htmlentities(stripslashes($this->getFieldValue($obj, 'textarea', $language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea>
						</div>';
			$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'ctextarea');
				echo '</div>';
					/* END textarea, freetext */
					/* START textbox*/
				echo '
					<div class="valuegeneral textbox">';
						foreach ($this->_languages as $language)
					echo '
							<div id="textbox_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
								<input size="40" type="text" name="textbox_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'textbox', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" style="width: 150px;" />
							</div>';
			$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'textbox');
				echo '</div>';
					/* END calendar, calendartime */
					/* START calendar*/
				echo '<div class="valuegeneral calendar">';
					echo '   <input size="40" type="text" name="calendar" id="calendar" value="'.($obj->type == 'calendar' ? $this->getFieldValue($obj, 'values') : '').'" />';
				echo '</div>';
				echo '<div class="valuegeneral calendartime">';
					echo '   <input size="40" type="text" name="calendartime" id="calendartime" value="'.($obj->type == 'calendartime' ? $this->getFieldValue($obj, 'values') : '').'" />';
				echo '</div>';
					/* END textare, freetext */
				echo '
				</div>
				</div>';
			/* END ADD VALUE */
			
		echo   '
			
			<div class="clear"></div><br>
			<div class="margin-form">
				<input type="submit" value="'.$l['Save'].'" name="submitAdd'.$this->table.'" class="button" />
				<input type="submit" value="'.$l['Save And Another'].'" name="submitAdd'.$this->table.'AndOther" class="button" />
				<input type="submit" value="'.$l['Save And Stay'].'" name="submitAdd'.$this->table.'AndStay" class="button" />
			</div>
			<div class="small"><sup>*</sup> '.$l['Required field'].'</div>
			</fieldset>
		</form>';
?>