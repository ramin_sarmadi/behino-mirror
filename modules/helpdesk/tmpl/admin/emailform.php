<?php 
defined('_HELPDESK') or die('Restricted access');

$this->addJS(array(
	__PS_BASE_URI__."js/tiny_mce/tiny_mce.js",
	_PS_JS_DIR_.'tinymce.inc.js',
	_MODULE_DIR_."helpdesk/assets/tinymce.inc.js",
));
// TinyMCE
global $currentIndex;
$cookie = $this->context->cookie;
	$iso = Language::getIsoById((int)($cookie->id_lang));
	$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
	$ad = dirname($_SERVER["PHP_SELF"]);
	echo '
		<script type="text/javascript">	
		var iso = \''.$isoTinyMCE.'\' ;
		var pathCSS = \''._THEME_CSS_DIR_.'\' ;
		var ad = \''.$ad.'\' ;
		</script>';
	
if($obj->id){
	$divLang = 'subject¤cmessage';
}else{
	$divLang = 'type¤subject¤cmessage';
}
echo '<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.$this->token.'" method="post" enctype="multipart/form-data">
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/admin/time.gif" />'.$l['Email Template'].'</legend>';
				
		echo 	'<label>'.$l['Type'].': </label>
				<div class="margin-form">';
				if(!$obj->id){
				foreach ($this->_languages as $language)
					echo '
					<div id="type_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="40" type="text" name="type_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'type', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" style="width: 150px;" /><sup> *</sup>
					</div>';							
				$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLang, 'type');
				}else{
		echo 		'<p>'.htmlentities($this->getFieldValue($obj, 'type', (int)($this->_defaultFormLanguage)), ENT_COMPAT, 'UTF-8').'</p>';
				}
		echo '  </div>';
				
		echo 	'<label>'.$l['Email Subject'].': </label>
				<div class="margin-form">
				<div'.(!in_array($obj->id, $skip_edit_subject) ? '' : ' style="display:none;"').'>';
				foreach ($this->_languages as $language)
					echo '
					<div id="subject_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input  type="text" name="subject_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'subject', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" style="width: 500px;" /><sup> *</sup>
					</div>';							
				$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLang, 'subject');
				echo '</div>';
				if(in_array($obj->id,$skip_edit_subject)){
			echo 	'<p>'.htmlentities($this->getFieldValue($obj, 'subject', (int)($this->_defaultFormLanguage)), ENT_COMPAT, 'UTF-8').'</p>';			
				}
		echo '  </div>
				<div class="clear space"></div>';
		echo '  <label>'.$l['Email Message'].': </label>
				<div class="margin-form">';
				foreach ($this->_languages as $language)
					echo '
					<div id="cmessage_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<textarea class="rte" id="message_'.$language['id_lang'].'" name="message_'.$language['id_lang'].'" cols="30" row="5">'.htmlentities($this->getFieldValue($obj, 'message', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'</textarea><sup> *</sup>
					</div>';
				$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLang, 'cmessage');
		echo '  </div>
				<div class="clear space"></div>
				<div class="margin-form">
					<input type="submit" value="'.$l['Save'].'" name="submitAdd'.$this->table.'" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$l['Required field'].'</div>
			</fieldset>
		</form>';
?>