<?php 
defined('_HELPDESK') or die('Restricted access');
global $currentIndex;

echo '<form action="'.$currentIndex.'&token='.$token.'&submitAdd'.$this->table.'" method="post" enctype="multipart/form-data">
		'.($obj->id ? '<input type="hidden" name="'.$this->identifier.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/admin/time.gif" />'.$l['General'].'</legend>
				<div class="margin-form">
					<input type="submit" value="'.$l['Save'].'" name="submitAdd'.$this->table.'" class="button" />
					<input type="submit" value="'.$l['Save And Another'].'" name="submitAdd'.$this->table.'AndOther" class="button" />
					<input type="submit" value="'.$l['Save And Stay'].'" name="submitAdd'.$this->table.'AndStay" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$l['Required field'].'</div>
				
				<label>'.$l['Departement Title'].': </label>
				<div class="margin-form">';
				foreach ($this->_languages as $language)
					echo '
					<div id="name_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="40" type="text" name="name_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'name', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" style="width: 150px;" /><sup> *</sup>
						<span class="hint" name="help_box">'.$l['Invalid characters'].' !<>,;?=+()@#"�{}_$%:<span class="hint-pointer">&nbsp;</span></span>
						</div>';							
				$this->displayFlags($this->_languages, $this->_defaultFormLanguage, 'name', 'name');
		
		echo '		<p class="clear">'.$l['Department Title'].'</p>
				</div>
				
				<div class="clear"></div>
				<label>'.$l['Enable'].': </label>
				<div class="margin-form">
					<input type="radio" name="active" id="active_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="active" id="active_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
				</div> 		
				<label>'.$l['Prefix'].': </label>
				<div class="margin-form">
					<input width="20px" type="text" name="prefix" value="'.htmlentities($this->getFieldValue($obj, 'prefix'), ENT_COMPAT, 'UTF-8').'" /> <sup>*</sup>
					<p>'.$l["This is the department prefix"].'</p>
				</div>
				
				<div class="clear"></div>
				<label>'.$l['Enable uploading files'].': </label>
				<div class="margin-form">
					<input type="radio" name="upload" id="upload_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'upload') ? 'checked="checked" ' : '').'/>
					<label class="t" for="upload_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="upload" id="upload_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'upload') ? 'checked="checked" ' : '').'/>
					<label class="t" for="upload_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
				</div> 
				
				<div class="clear"></div>
				<label>'.$l['Allowed extensions'].': </label>
				<div class="margin-form">
					<textarea    name="upload_extensions" cols="50" rows="5"  >'.htmlentities($this->getFieldValue($obj, 'upload_extensions'), ENT_COMPAT, 'UTF-8').'</textarea>
					<p>'.$l["Please note that allowing extensions such as php"].'</p>
				</div>
				<div class="clear"></div>
				<label>'.$l['Maximun Upload Size'].': </label>
				<div class="margin-form">
					<input width="20px" type="text" name="upload_size" value="'.htmlentities($this->getFieldValue($obj, 'upload_size'), ENT_COMPAT, 'UTF-8').'" />
					<p>'.$l[ "Your server allows uploads up to"].' <b>'.ini_get('upload_max_filesize').'</b></p>
				</div>
				<div class="clear"></div>
				<label>'.$l['Maximum files allowed'].': </label>
				<div class="margin-form">
					<input width="20px" type="text" name="upload_files" value="'.($obj->id ? $this->getFieldValue($obj, 'upload_files') : 10).'" />
					<p>'.$l['Users can submit multiple files by clicking'].'</p>
				</div>
				
			</fieldset>
			<div class="clear"></div><br>
			<fieldset><legend><img src="../img/admin/time.gif" />'.$l['Tickets Setting'].'</legend>
				<label>'.$l['ticket_assignment_type'].': </label>
				<div class="margin-form">
					<select name="assignment_type">';
					foreach($assignment_type as $key=>$type){
				echo '<option value="'.$key.'"'.($obj->id ? ($key == $obj->assignment_type ? 'selected="selected"' : '') : ($key == 1 ? 'selected="selected"' : '')).'>'.$type.'</option>';	
					}
			echo ' </select>
				</div>
				
				<label>'.$l['Ticket Code Generation Rule'].': </label>
				<div class="margin-form">
					<select name="generation_rule">';
					foreach($generation_rule as $key=>$rule){
				echo '<option value="'.$key.'"'.($obj->id ? ($key == $obj->generation_rule ? 'selected="selected"' : '') : ($key == 1 ? 'selected="selected"' : '')).'>'.$rule.'</option>';	
					}
			echo ' </select>
				</div>
				
				<label>'.$l['Priority'].': </label>
				<div class="margin-form">
					<select name="id_hd_priority">';
					foreach($priorities as $priority){
				echo '<option value="'.$priority['id_hd_priority'].'"'.($priority['id_hd_priority'] == $obj->id_hd_priority ? 'selected="selected"' : '').'>'.$priority['name'].'</option>';	
					}
			echo ' </select>
				</div>
				
			</fieldset>
			<div class="clear"></div><br>
			<fieldset><legend><img src="../img/admin/time.gif" />'.$l['Email Setting'].': </legend>
				<label>'.$l['Use Email in Global Configuration'].': </label>
				<div class="margin-form">
					<input type="radio" name="email_use_global" id="notify_assign_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'email_use_global') ? 'checked="checked" ' : '').'/>
					<label class="t" for="email_use_global_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="email_use_global" id="email_use_global_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'email_use_global') ? 'checked="checked" ' : '').'/>
					<label class="t" for="email_use_global_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>				 
				</div>
				<div class="clear"></div>
				
				<label>'.$l['From Email Address'].': </label>
				<div class="margin-form">
					<input type="text"   name="email_address" value="'.htmlentities($this->getFieldValue($obj, 'email_address'), ENT_COMPAT, 'UTF-8').'" />
					<p>'.$l[ "This is the email address of the email sender" ].'</p>
				</div>
				
				<label>'.$l['From Name'].': </label>
				<div class="margin-form">
					<input type="text" name="email_address_fullname" value="'.htmlentities($this->getFieldValue($obj, 'email_address_fullname'), ENT_COMPAT, 'UTF-8').'" />
					<p>'.$l[ "This is the full name of the email sender, as it appears on outging emails" ].'</p>
				</div>
				
				<label>'.$l['Send emails to customer when staff members reply'].': </label>
				<div class="margin-form">
					<input type="radio" name="customer_send_email" id="notify_assign_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'customer_send_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="customer_send_email_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="customer_send_email" id="customer_send_email_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'customer_send_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="customer_send_email_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>				 
				</div>
				<div class="clear"></div>
				<label>'.$l['Send email to customer with a copy of his ticket'].': </label>
			 
				<div class="margin-form">
					<input type="radio" name="customer_send_copy_email" id="notify_assign_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'customer_send_copy_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="customer_send_copy_email_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="customer_send_copy_email" id="customer_send_copy_email_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'customer_send_copy_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="customer_send_copy_email_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>				 
				</div>
					
				<div class="clear"></div>
				<label>'.$l['Send attachments with customer'].' </label>
			
					<div class="margin-form">
					<input type="radio" name="customer_attach_email" id="notify_assign_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'customer_attach_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="customer_attach_email_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="customer_attach_email" id="customer_attach_email_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'customer_attach_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="customer_attach_email_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
				 
					</div>
					 <div class="clear"></div>
				<label>'.$l['Send emails to the staff member when customers reply'].' </label>
				<div class="margin-form">
					<input type="radio" name="staff_send_email" id="notify_assign_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'staff_send_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="staff_send_email_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="staff_send_email" id="staff_send_email_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'staff_send_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="staff_send_email_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
				</div>
				
				<div class="clear"></div>
				
				<label>'.$l['Send attachments with staff member'].' </label>
				
				<div class="margin-form">
					<input type="radio" name="staff_attach_email" id="notify_assign_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'staff_attach_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="staff_attach_email_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="staff_attach_email" id="staff_attach_email_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'staff_attach_email') ? 'checked="checked" ' : '').'/>
					<label class="t" for="staff_attach_email_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
				</div>
		 		<div class="clear"></div>
				
				<label>'.$l['Notify for email'].' </label>
				<div class="margin-form">
					<textarea name="notify_new_tickets_to" cols="50" rows="5">'.htmlentities($this->getFieldValue($obj, 'notify_new_tickets_to'), ENT_COMPAT, 'UTF-8').'</textarea>
					<p>'.$l[ "Notify the following email addresses when a new ticket arrives" ].'</p>
				</div>
				<label>'.$l['CC'].': </label>
				<div class="margin-form">
					<textarea    name="cc" cols="50" rows="5"  >'.htmlentities($this->getFieldValue($obj, 'cc'), ENT_COMPAT, 'UTF-8').'</textarea>
					<p>'.$l[ "each email on a new line" ].'</p>
				</div>
				<label>'.$l['BCC'].': </label>
				<div class="margin-form">
					<textarea    name="bcc" cols="50" rows="5"  >'.htmlentities($this->getFieldValue($obj, 'bcc'), ENT_COMPAT, 'UTF-8').'</textarea>
					<p>'.$l[ "each email on a new line" ].'</p>
				</div>
				
				
				<label>'.$l['Notify staff members'].': </label>
				<div class="margin-form">
					<input type="radio" name="notify_assign" id="notify_assign_on" onclick="toggleDraftWarning(false);" value="1" '.($this->getFieldValue($obj, 'notify_assign') ? 'checked="checked" ' : '').'/>
					<label class="t" for="notify_assign_on"> <img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="notify_assign" id="notify_assign_off" onclick="toggleDraftWarning(true);" value="0" '.(!$this->getFieldValue($obj, 'notify_assign') ? 'checked="checked" ' : '').'/>
					<label class="t" for="notify_assign_off"> <img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
					<p>'.$l['Notify staff members when a ticket has been manually assigned to them'].'</p>
				</div>
<div class="clear"></div><br>
			<div class="margin-form">
					<input type="submit" value="'.$l['Save'].'" name="submitAdd'.$this->table.'" class="button" />
					<input type="submit" value="'.$l['Save And Another'].'" name="submitAdd'.$this->table.'AndOther" class="button" />
					<input type="submit" value="'.$l['Save And Stay'].'" name="submitAdd'.$this->table.'AndStay" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$l['Required field'].'</div>

			</fieldset>
		 
		</form>';
		
		
?>