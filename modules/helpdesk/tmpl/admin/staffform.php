<?php 
defined('_HELPDESK') or die('Restricted access');

/*add style and JS */
$this->addJS(array(
	__PS_BASE_URI__."js/tiny_mce/tiny_mce.js",
	_PS_JS_DIR_.'tinymce.inc.js',
	_MODULE_DIR_."helpdesk/assets/tinymce.inc.js",
));
// TinyMCE
global $cookie, $currentIndex;
	$iso = Language::getIsoById((int)($cookie->id_lang));
	$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
	$ad = dirname($_SERVER["PHP_SELF"]);
	echo '
		<script type="text/javascript">	
		var iso = \''.$isoTinyMCE.'\' ;
		var pathCSS = \''._THEME_CSS_DIR_.'\' ;
		var ad = \''.$ad.'\' ;
		var ajaxUrl = "'._MODULE_DIR_.'helpdesk/lofajax.php";
		</script>';
		
$id_employee = $this->getFieldValue($obj, 'id_employee');
$vdepartments = (array)$this->getFieldValue($obj, 'departments');
$id_hd_priority   = (int)$this->getFieldValue($obj, 'id_hd_priority');
echo '<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.$this->token.'" method="post" enctype="multipart/form-data">
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/admin/time.gif" />'.$l['Staff'].'</legend>
		 
				<div class="clear"></div>
			
				<label>'.$l['Employee'].': </label>
				<div class="margin-form">
					<select name="id_employee">
						<option value="">  -----  </option>';
						foreach ($employees as $v)
							echo '<option value="'.$v['id_employee'].'" '.($id_employee ==$v['id_employee'] ? 'selected="selected"' : '').'>'.$v['firstname'].' '.$v['lastname'].'</option>';
					echo '
					</select> <sup>*</sup>
				</div>
				
				<label>'.$l['Departments'].': </label>
				<div class="margin-form">
					<select name="id_hd_department[]" multiple="multiple" size="5">';
						foreach ($departments as $v)
							echo '<option value="'.$v['id_hd_department'].'" '.(in_array($v['id_hd_department'],$vdepartments) ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
					echo '
					</select> <sup>*</sup>
				</div>
				
				 <label>'.$l['Priority'].': </label>
				<div class="margin-form">
					<select name="id_hd_priority">
						<option value="">-</option>';
						foreach ($priorities as $p)
							echo '<option value="'.$p['id_hd_priority'].'" '.($id_hd_priority == $p['id_hd_priority'] ? 'selected="selected"' : '').'>'.$p['name'].'</option>';
					echo '
					</select> <sup>*</sup>
				</div>
			
				<label>'.$l['Signature'].': </label>
				<div class="margin-form">
					<textarea class="rte" rows="6" cols="50" name="signature">'.htmlentities($this->getFieldValue($obj, 'signature'), ENT_COMPAT, 'UTF-8').'</textarea>
				</div>
				
				<div class="margin-form">
					<input type="submit" value="'.$l['Save'].'" name="submitAdd'.$this->table.'" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$l['Required field'].'</div>
			</fieldset>
		</form>';
?>