<?php

if (!defined('_CAN_LOAD_FILES_'))
	exit;
?>
	<div class="lofaddnew">
		<script type="text/javascript">
			jQuery(document).ready(function(){
				$('.click_reply').click(function(){
					$(this).css({'display':'none'});
					$('.lofformmessage').css({'display':'block'});
				});
			});
		</script>
		<a <?php echo ($expandform ? ' style="display:none"' : '');?> class="click_reply" href="javascript:void(0)" title="<?php echo $helpdesk->l('add notice');?>">
			<img border="0" src="../../img/admin/add.gif">
			<?php echo $helpdesk->l('Click here to add notice');?>
		</a>
		<div class="lofformmessage"<?php echo (!$expandform ? ' style="display:none"' : '');?>>
		<?php if($errors){ ?> 
			<div class="error">
				<p><?php echo (count($errors) > 1 ? $helpdesk->l('There are') : $helpdesk->l('There is')).' '.(count($errors) > 1 ? $helpdesk->l('errors') : $helpdesk->l('error'));?></p>
				<ol>
				<?php foreach($errors as $e){ ?>
					<li><?php echo $e;?></li>	
				<?php }?>	
				</ol>
			</div>
		<?php } ?>	
			<?php include_once( dirname(__FILE__)."/noticefrom.php" ); ?>
		</div>
	</div>
	<?php
	if($notices){
		foreach($notices as $m){ ?>
		<div class="lofitem">
			<div class="lofheader">
				<?php if($m['id_employee']){ ?>
				<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/staff-icon.png";?>" alt="<?php echo $helpdesk->l('avatar'); ?>"/></div>
				<?php } ?>
				<div class="lofname">
				<?php if(isset($m['objEmp'])){ ?>
					<?php echo ($params->get('show_email_link') ? '<a href="mailto:'.$m['objEmp']->email.'">' : '').'<span class="name_email">'.($params->get('show_user_info') == 'name' ? $m['objEmp']->firstname.' '.$m['objEmp']->lastname : $m['objEmp']->email ).'</span>'.($params->get('show_email_link') ? '</a>' : '')?>
				<?php } ?>
					<span class="wrote"><?php echo $helpdesk->l('wrote');?>:</span>
				</div>
				<div class="lofdate">
				<?php echo Tools::displayDate($m['date_add'],$cookie->id_lang, true, ':');?>
				</div>
			</div>
			<div class="clear"></div>
			<div class="lofcontent" id="lofcontent_<?php echo $m['id_hd_ticket_notes'];?>">
				<p><?php echo $m['text'];?></p>
			</div>
			<div class="lofaction itemlofaction" align="right">
				<a href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/viewNotices.php?id_hd_ticket='.$m['id_hd_ticket'].'&task=edit&id_hd_ticket_notes='.$m['id_hd_ticket_notes'].'&lofToken='.$lofToken;?>" title="<?php echo $helpdesk->l('edit notice');?>">
					<img src="<?php echo _PS_IMG_.'admin/edit.gif';?>" alt="<?php echo $helpdesk->l('Edit');?>"/>
				</a>
				<a href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/viewNotices.php?id_hd_ticket='.$m['id_hd_ticket'].'&task=delete&id_hd_ticket_notes='.$m['id_hd_ticket_notes'].'&lofToken='.$lofToken;?>" title="<?php echo $helpdesk->l('delete notice');?>">
					<img src="<?php echo _PS_IMG_.'admin/delete.gif';?>" alt="<?php echo $helpdesk->l('Delete');?>"/>
				</a>
			</div>
		</div>

	<?php }
	}
	?>