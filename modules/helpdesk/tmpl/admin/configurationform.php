<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
$mname = "helpdesk";
// add style and js
$this->addCSS(array(
	__PS_BASE_URI__."modules/".$mname."/assets/admin/css/form.css",
	__PS_BASE_URI__."modules/".$mname."/assets/admin/css/jquery-ui.css",
));
$this->addJS(array(
	__PS_BASE_URI__."modules/".$mname."/assets/admin/js/form.js",
	_PS_JS_DIR_."jquery/jquery-colorpicker.js",
	__PS_BASE_URI__."js/tiny_mce/tiny_mce.js",
	_MODULE_DIR_."helpdesk/assets/tinymce.inc.js",
));

// TinyMCE
global  $currentIndex;
$cookie = $this->context->cookie;
	$iso = Language::getIsoById((int)($cookie->id_lang));
	$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
	$ad = dirname($_SERVER["PHP_SELF"]);
	echo '
		<script type="text/javascript">	
		var iso = \''.$isoTinyMCE.'\' ;
		var pathCSS = \''._THEME_CSS_DIR_.'\' ;
		var ad = \''.$ad.'\' ;
		var ajaxUrl = "'._MODULE_DIR_.'helpdesk/lofajax.php";
		</script>';
		
?>
<?php 
	$yesNoLang = array("0"=>$l['No'],"1"=>$l['Yes']);
	$divLangName = 'cglobal_message�csubmit_message';
?>
<h3><?php echo $l['Global Configuration'];?></h3>
<form action="<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>" method="post" id="lofform">
	<input type="submit" name="submit" value="<?php echo $l['Update'];?>" class="button" />
	<br/>
    <br/>
	<div class="clearfix ui-tabs ui-widget ui-widget-content ui-corner-all" id="lof-pdf-tab">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-1"><span><?php echo $l["General"];?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-3"><span><?php echo $l["Tickets"];?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-4"><span><?php echo $l["CAPTCHA"];?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-5"><span><?php echo $l["Email"];?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-6"><span><?php echo $l["Autoclose"];?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-7"><span><?php echo $l["Notices"];?></span></a></li>
		</ul>
		<!-- general -->
		<div id="fragment-1" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<ul>
				<?php 
				echo '<li class="row">'. $this->_params->radioBooleanTag("allow_rich_editor", $yesNoLang,$this->getParamValue("allow_rich_editor",0),$l['Use editor for writing messages'],'','','class="module_group-product"','')."</li>";
				echo '<li class="row">'. $this->_params->radioBooleanTag("show_signature", $yesNoLang,$this->getParamValue("show_signature",1),$l['Show signature checkbox when staff is replying'],'','','class="module_group-product"','')."</li>";
				echo '<li class="row">'. $this->_params->radioBooleanTag("submit_redirect", $yesNoLang,$this->getParamValue("submit_redirect",1),$l['Redirect to URL'],'','','class="module_group-product"','')."</li>";
				//echo '<li class="row">'.$this->_params->radioBooleanTag("staff_force_departments", $yesNoLang,$this->getParamValue("staff_force_departments",1),$l['Staff members see only their departments'),'','','class="module_group-product"','')."</li>";
				?>
			</ul>
		</div>
		<!-- ticket -->
		<div id="fragment-3" style="display:none;" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<fieldset>
				<legend><?php echo $l['Appearance']; ?></legend>
				<ul>
				<?php 
					$arrSort = array('ASC' => $l['Ascending'], 'DESC' => $l['Descending']);
					$arrSubmiter = array('name' => $l['First name_Last name'], 'email' => $l['Email']);
					$arrHistory = array('0' => $l['Disable'], '1' => $l['Enable and Allow Staff Members to View'], '2' => $l['Enable and Allow Staff Members and Customers to View']);
					$arrTime = array('minutes' => $l['minutes'], 'hours' => $l['hours'], 'days' => $l['days']);
					
					echo $this->_params->selectTag("messages_direction",$arrSort,$this->getParamValue("messages_direction","DESC"),$l['Sort Ticket Messages'],'class="inputbox"','class="row"','');
					echo '<li class="row">'. $this->_params->radioBooleanTag("show_ticket_info", $yesNoLang,$this->getParamValue("show_ticket_info",1),$l['Show Submitter Information'],'','','class="module_group-product"','')."</li>";
					echo $this->_params->selectTag("show_user_info",$arrSubmiter,$this->getParamValue("show_user_info","name"),$l['Show Submitter Field'],'class="inputbox"','class="row"','');
					echo '<li class="row">'. $this->_params->radioBooleanTag("show_email_link", $yesNoLang,$this->getParamValue("show_email_link",0),$l['Submitter Field is an Email Link'],'','','class="module_group-product"','')."</li>";
					//echo $this->_params->selectTag("ticket_viewing_history",$arrHistory,$this->getParamValue("ticket_viewing_history","2"),$l['Ticket Viewing History'),'class="inputbox"','class="row"','');
				?>
				</ul>
			</fieldset>
			<fieldset>
				<legend><?php echo $l['Customer Input']; ?></legend>
				<ul>
				<?php
					echo '<li class="row">'. $this->_params->radioBooleanTag("show_ticket_voting", $yesNoLang,$this->getParamValue("show_ticket_voting",1),$l['Allow Ticket Voting'],'','','class="module_group-product"','')."</li>";
					echo '<li class="row">'. $this->_params->radioBooleanTag("allow_ticket_closing", $yesNoLang,$this->getParamValue("allow_ticket_closing",1),$l['Allow Customers to Close Tickets'],'','','class="module_group-product"','')."</li>";
					echo '<li class="row">'.$this->_params->radioBooleanTag("allow_ticket_reopening", $yesNoLang,$this->getParamValue("allow_ticket_reopening",1),$l['Allow Customers to Re-open Closed Tickets'],'','','class="module_group-product"','')."</li>";
				?>
				</ul>
			</fieldset>
		</div>
		<!--Captcha  -->
		<div id="fragment-4" style="display:none;" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<ul>
			<?php 
				$arrUseCaptcha = array('0' => $l['No'], '1' => $l['Use Built_in Captcha']);
				$arrTheme = array('red' => $l['Red'], 'white' => $l['White'], 'blackglass' => $l['Blackglass'], 'clean' => $l['Clean']);
				$arrType = array('SI_CAPTCHA_MATHEMATIC'=>$l['Mathematic'], 'DEFAULT' => $l['Characters']);
				$arrImgType = array('SI_IMAGE_JPEG'=>$l['image_jpeg'], 'SI_IMAGE_GIF' => $l['image_gif'], 'default' => $l['image_png']);
				
				echo $this->_params->selectTag("captcha_enabled",$arrUseCaptcha,$this->getParamValue("captcha_enabled","1"),$l['Enable CAPTCHA'],'class="inputbox"','class="row"','');
				echo $this->_params->selectTag("captcha_type",$arrType,$this->getParamValue("captcha_type","DEFAULT"),$l['CAPTCHA Type'],'class="inputbox"','class="row"','',$l['The type of captcha to create, either alphanumeric, or a math problem']);
				echo $this->_params->selectTag("captcha_img_type",$arrImgType,$this->getParamValue("captcha_img_type","default"),$l['Image Type'],'class="inputbox"','class="row"','',$l['The type of the image, default_png']);
				
				echo $this->_params->inputTag("captcha_characters",$this->getParamValue("captcha_characters","5"),$l['Characters'],'class="text_area"','class="row"','',$l['The length of the captcha code']);
				echo $this->_params->inputTag("image_height",$this->getParamValue("image_height",50),$l['Image Height'],'class="text_area"','class="row"','',$l['The height of the captcha image']);
				echo $this->_params->inputTag("num_lines",$this->getParamValue("num_lines",8),$l['number lines'],'class="text_area"','class="row"','',$l['How many lines to draw over the captcha code to increase security']);
				echo $this->_params->inputTag("perturbation",$this->getParamValue("perturbation",0.75),$l['Perturbation'],'class="text_area"','class="row"','',$l['The level of distortion']);
				echo $this->_params->inputColorTag("image_bg_color",$this->getParamValue("image_bg_color","#fff8f4"),$l['Image Background Color'],'class="text_area color mColorPickerInput"','class="row"','',$l['The background color of the captcha']);
				echo $this->_params->inputColorTag("text_color",$this->getParamValue("text_color","#000a14"),$l['Text Color'],'class="text_area color mColorPickerInput"','class="row"','',$l['The color of the captcha text']);
				echo $this->_params->inputColorTag("line_color",$this->getParamValue("line_color","#6254ff"),$l['Line Color'],'class="text_area color mColorPickerInput"','class="row"','',$l['The color of the lines over the captcha']);
				echo '<li class="row">'. $this->_params->radioBooleanTag("captcha_case_sensitive", $yesNoLang,$this->getParamValue("captcha_case_sensitive",0),$l['Case Sensitive'],'','','class="module_group-product"',$l['Whether the captcha should be case sensitive'])."</li>";
			?>
			</ul>
		</div>
		<!-- email -->
		<div id="fragment-5" style="display:none;" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<ul>
			<?php 
				echo $this->_params->inputTag("email_address",$this->getParamValue("email_address",(isset($employee['email']) ? $employee['email'] : '')),$l['From Email Address'],'class="text_area"','class="row"','',$l['This is email address of email sender']);
				echo $this->_params->inputTag("email_address_fullname",$this->getParamValue("email_address_fullname",(isset($employee['name']) ? $employee['name'] : '')),$l['From Name'],'class="text_area"','class="row"','',$l['This id full name of email sender_as it appears on outgoing emails']);
			?>
			</ul>
		</div>
		<!-- auto close -->
		<div id="fragment-6" style="display:none;" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<ul>
			<?php 
				echo '<li class="row">'. $this->_params->radioBooleanTag("autoclose_enabled", $yesNoLang,$this->getParamValue("autoclose_enabled",1),$l['Enable Autoclose'],'','','class="module_group-product"',$l['Enable the auto_close of tickets when they exceeds a pre_set number of day width no replies'])."</li>";
				echo $this->_params->inputTag("autoclose_cron_interval",$this->getParamValue("autoclose_cron_interval","10"),$l['check for tickets marked for autoclosing every'],'class="text_area"','class="row"','');
				echo $this->_params->inputTag("autoclose_email_interval",$this->getParamValue("autoclose_email_interval","1"),$l['Days a ticket should go into autoclose status'],'class="text_area"','class="row"','');
				echo $this->_params->inputTag("autoclose_interval",$this->getParamValue("autoclose_interval","1"),$l['Days a ticket should be autoclosed'],'class="text_area"','class="row"','');
			?>
			</ul>
		</div>
		<!-- notice -->
		<div id="fragment-7" style="display:none;" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<ul>
			<?php 
				echo $this->_params->inputTag("notice_email_address",$this->getParamValue("notice_email_address",""),$l['Email Address'],'class="text_area"','class="row"','',$l['Enter email address where you wish to receive the notifications']);
				echo $this->_params->inputTag("notice_max_replies_nr",$this->getParamValue("notice_max_replies_nr","0"),$l['Number of Replies'],'class="text_area"','class="row"','',$l['A notice will be sent when the number of consecutive customer']);
				echo $this->_params->inputTag("notice_replies_with_no_response_nr",$this->getParamValue("notice_replies_with_no_response_nr","0"),$l['Number of Replies_Assigned'],'class="text_area"','class="row"','',$l['A notice will be sent when the number']);
				echo $this->_params->textareaTag("notice_not_allowed_keywords",$this->getParamValue("notice_not_allowed_keywords",""),$l['Triggered Keywords'],'class="text_area" cols="50" rows="7"','class="row"','',$l['Enter the keywords separates by comma']);
			?>
			</ul>
		</div>
		<input type="hidden" name="updateConfiguration" value="1"/>
	</div>
</form>
