<?php 
defined('_HELPDESK') or die('Restricted access');
// add style and js
$this->addCSS(array(
	_MODULE_DIR_."helpdesk/assets/admin/css/formtickets.css",
));
$this->addJS(array(
	_MODULE_DIR_."helpdesk/assets/admin/js/formticketsmessage.js",
	__PS_BASE_URI__."js/tiny_mce/tiny_mce.js",
	__PS_BASE_URI__."js/tinymce.inc.js",
	_MODULE_DIR_."helpdesk/assets/tinymce.inc.js",
));
// TinyMCE
global $cookie, $currentIndex;
	$iso = Language::getIsoById((int)($cookie->id_lang));
	$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
	$ad = dirname($_SERVER["PHP_SELF"]);
	echo '
		<script type="text/javascript">	
		var iso = \''.$isoTinyMCE.'\' ;
		var pathCSS = \''._THEME_CSS_DIR_.'\' ;
		var ad = \''.$ad.'\' ;
		var ajaxUrl = "'._MODULE_DIR_.'helpdesk/lofajax.php";
		</script>';
?>
<script type="text/javascript">
	var mess_data_init = new Array(<?php echo $objDepartment->id; ?>,<?php echo $objDepartment->id_hd_priority; ?>,<?php echo $objDepartment->upload; ?>,<?php echo $objDepartment->upload_files; ?>,"<?php echo $objDepartment->upload_extensions; ?>");
	var nb_file = <?php echo (isset($files) ? count($files) : 0);?>;
	var message_error = "<?php echo $l['The maximum number of attachments has been reached. You cannot add any more attachments'];?>";
</script>
<?php
$divLangName = 'cmessage';
echo '<form action="'.$currentIndex.'&viewhd_ticket&id_hd_ticket='.(isset($id_hd_ticket) &&  $id_hd_ticket ? $id_hd_ticket : $obj->id_hd_ticket).'&token='.$token.'" method="post" enctype="multipart/form-data">
		<input type="hidden" id="id_hd_ticket" name="id_hd_ticket" value="'.(isset($id_hd_ticket) &&  $id_hd_ticket ? $id_hd_ticket : $obj->id_hd_ticket).'"/>
		<input type="hidden" name="id_hd_department" value="'.($objDepartment->id).'"/>';
		$templs = HdQuickResponse::getQuickresponses();
	echo '<label>'.$l['Use template'].':</label>
			<div class="margin-form">
				<select name="use_tmpl" id="use_tmpl">
					<option value="0">'.$l['No Template'].'</option>';
				if($templs)
					foreach($templs as $t){
					echo '<option value="'.$t['id_hd_quickresponse'].'">'.$t['title'].'</option>';
					}
			echo '</select>
			</div>';
	echo '	<label>'.$l['Message'].': </label>
			<div class="margin-form">';
			echo '	<div id="cmessage">
					<textarea class="rte" cols="80" rows="30" id="message" name="message"></textarea>
				</div>';
			echo '<sup>*</sup>';
	echo '	</div>';
	
	echo '<div class="lofattackment">';
		echo '<label>'.$l['Attachments'].': </label>
			<div class="margin-form">';
			if(isset($files) && $files)
				foreach($files as $file){
				echo '<div class="file" id="file-'.$file['id_hd_ticket_files'].'"><img src="'._MODULE_DIR_.'helpdesk/assets/images/attach.png'.'" alt="'.$l['Attachment'].'"/>'.$file['filename'].'<a href="javascript:void(0)" title="'.$l['Delete'].'" onclick="deleteFile(\'file-'.$file['id_hd_ticket_files'].'\','.$file['id_hd_ticket_files'].')"><img src="../img/admin/delete.gif" alt="'.$l['Delete'].'"/></a></div>';	
				}
		echo '	<div class="add_attachment">
					<input type="file" name="lof_files[]" id="input_lof_files"/>
					<input type="button" onclick="lof_add_attachments();" value="'.$l['Add attachment'].'" class="button" id="lof_add_attachment"/>
				</div>
				<div id="lof_files"></div>
				<p>'.$l['Allowed attachments are'].': <b class="lof_extension"></b></p>
			</div>
		</div>';
	echo '
		<div class="clear"></div>
		<div class="margin-form">
			<input type="submit" value="'.$l['Save'].'" name="submitAddMessage" class="button" />
		</div>';
echo  ' </form>';
?>