<?php 
defined('_HELPDESK') or die('Restricted access');
/*add style and JS */
$this->addJS(array(
	_PS_JS_DIR_."jquery/jquery-colorpicker.js",
));
global $currentIndex;
echo '<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.$this->token.'" method="post" enctype="multipart/form-data">
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/admin/time.gif" />'.$l['Statuses'].'</legend>
				<label>'.$l['Status name'].': </label>
				<div class="margin-form">';

				foreach ($this->_languages as $language)
					echo '
					<div id="name_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
						<input size="40" type="text" name="name_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'name', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" style="width: 150px;" /><sup> *</sup>
						<span class="hint" name="help_box">'.$l['Invalid characters: numbers and'].' !<>,;?=+()@#"�{}_$%:<span class="hint-pointer">&nbsp;</span></span>
						</div>';							
				$this->displayFlags($this->_languages, $this->_defaultFormLanguage, 'name¤template', 'name');

		echo '		<p class="clear">'.$l['Status Name_e'].'</p>
				</div>
				<div class="clear"></div>';
			if(!in_array($obj->id,$skipactive)){
		echo '
				<label>'.$l['Enable'].': </label>
				<div class="margin-form">
					<input type="radio" name="active" id="active_on" value="1" '.($this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"><img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="active" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"><img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
				</div>	
				<div class="clear"></div>';
			}
		echo '
				<label>'.$l['Color'].': </label>
				<div class="margin-form">
					<input width="20px" type="color" data-hex="true" class="color mColorPickerInput" name="color" value="'.htmlentities($this->getFieldValue($obj, 'color'), ENT_COMPAT, 'UTF-8').'" />
					<p>'.$l['Status will be highlighted'].' "lightblue", "#CC6600")</p>
				</div>
			
				
				<script type="text/javascript">if (getE(\'send_email\').checked) getE(\'tpl\').style.display = \'block\'; else getE(\'tpl\').style.display = \'none\';</script>
				<div class="margin-form">
					<input type="submit" value="'.$l['Save'].'" name="submitAdd'.$this->table.'" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$l['Required field'].'</div>
			</fieldset>
		</form>';
