<?php 
defined('_HELPDESK') or die('Restricted access');
$context = Context::getContext();
?>
<script type="text/javascript" src="<?php echo __PS_BASE_URI__."js/tiny_mce/tiny_mce.js";?>"></script>	
<script type="text/javascript" src="<?php echo _MODULE_DIR_."helpdesk/assets/tinymce.inc.js";?>"></script>	

<?php
// TinyMCE
global $cookie, $currentIndex;
	$iso = Language::getIsoById((int)($cookie->id_lang));
	$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
	$ad = dirname($_SERVER["PHP_SELF"]);
	echo '
		<script type="text/javascript">	
		var iso = \''.$isoTinyMCE.'\' ;
		var pathCSS = \''._THEME_CSS_DIR_.'\' ;
		var ad = \''.$ad.'\' ;
		var ajaxUrl = "'._MODULE_DIR_.'helpdesk/lofajax.php";
		</script>';
?>
<script type="text/javascript">
	var id_language = <?php echo $helpdesk->defaultFormLanguage;?>;
	function changeLanguageLof(field, fieldsString, id_language_new, iso_code) {
		var fields = fieldsString.split('¤');
		for (var i = 0; i < fields.length; ++i) {
			getE(fields[i] + '_' + id_language).style.display = 'none';
			getE(fields[i] + '_' + id_language_new).style.display = 'block';
			getE('language_current_' + fields[i]).src = '../../img/l/' + id_language_new + '.jpg';
		}
		getE('languages_' + field).style.display = 'none';
		id_language = id_language_new;
	}
</script>
<?php
$divLangName = 'ctext';

echo '<form action="#" method="post" enctype="multipart/form-data">
			<input type="hidden" name="id_hd_ticket" value="'.($id_hd_ticket ? $id_hd_ticket : $obj->id_hd_ticket).'"/>';
			
		echo '	<label>'.$helpdesk->l('Notice').': </label>
				<div class="margin-form">';
			foreach ($helpdesk->languages as $language)
				echo '	<div id="ctext_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $helpdesk->defaultFormLanguage ? 'block' : 'none').';float: left;">
						<textarea class="rte" cols="80" rows="30" id="text_'.$language['id_lang'].'" name="text_'.$language['id_lang'].'">'.htmlentities(stripslashes($obj->text[$language['id_lang']]), ENT_COMPAT, 'UTF-8').'</textarea>
					</div>';
				echo ' <sup>*</sup>';
		$helpdesk->displayFlags($helpdesk->languages, $helpdesk->defaultFormLanguage, $divLangName, 'ctext');
		echo '	</div>';
		
		echo '
			<div class="clear"></div>
			<div class="margin-form">';
		if(!$obj->id){
			echo '<input type="submit" value="'.$helpdesk->l('Save').'" name="submitAddNotice" class="button" />';
		}else{
			echo '<input type="submit" value="'.$helpdesk->l('Save').'" name="submitEditNotice" class="button" />';
		}
		echo'</div>';
		
	echo  ' </form>';