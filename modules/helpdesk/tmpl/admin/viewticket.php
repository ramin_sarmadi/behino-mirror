<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
	
// add style and js
$this->addCSS(array(
	_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/css/global.css",
	_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/css/jquery-ui.css",
	_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/css/jquery-ui.css",
	_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/form.css",
));
$this->addJS(array(
	_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/js/form.js",
	_MODULE_DIR_."helpdesk/assets/admin/js/viewticket.js",
));
$this->addjQueryPlugin(array(
	'fancybox',
));
	echo '
	<script type="text/javascript">
		var iddiv = "'.$lofTab.'";
		var ajaxUrl = "'._MODULE_DIR_.'helpdesk/lofajax.php";
		$(document).ready( function(){ 
			$(".fancy-ajax").fancybox({
				"width"				: 920,
				"height"			: 350,	
				\'type\'				: \'iframe\',
				"scrolling" : "no"
			});
			$(".fancy-ajax1").fancybox({
				"width"				: 920,
				"height"			: 615,	
				\'type\'				: \'iframe\',
				"scrolling" : "no"
			});
		} );
	</script>'; ?>
	
	
	<div class="clearfix ui-tabs ui-widget ui-widget-content ui-corner-all" id="lof-pdf-tab">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-1"><span><?php echo $l["Replies"];?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-2"><span><?php echo $l["Ticket Information"];?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-3"><span><?php echo $l["Ticket History"];?></span></a></li>
			<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-4"><span><?php echo $l["Submitter Information"];?></span></a></li>
		</ul>
		
		<div id="fragment-1" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
		
		<!-- top -->
		<div class="topmessage lofitem">
			<div class="lofcontainer">
				<div class="lofheader">
					<?php if($Topmessage && $Topmessage['id_employee']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/images/staff-icon.png" alt="<?php echo $l['avatar'];?>"/></div>
					<?php }elseif($Topmessage && $Topmessage['id_customer']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/images/user-icon.png" alt="<?php echo $l['avatar'];?>"/></div>
					<?php } ?>
					<div class="lofname">
						<?php if(isset($Topmessage['objEmp'])){ ?>
							<?php if($params->get('show_email_link')){?>
								<a href="mailto:<?php echo $Topmessage['objEmp']->email;?>"><?php 
							}?>
								<span class="name_email">
								<?php if($params->get('show_user_info') == 'name'){
									echo $Topmessage['objEmp']->firstname.' '.$Topmessage['objEmp']->lastname;
								}else{ 
									echo $Topmessage['objEmp']->email;
								}?>
								</span>
							<?php 
							if($params->get('show_email_link')){?>
								</a>
							<?php } ?>
						<?php }elseif(isset($Topmessage['objCus'])){ ?>
								<?php if($params->get('show_email_link')){?>
									<a href="mailto:<?php echo $Topmessage['objCus']->email;?>">
								<?php }?>
								<span class="name_email">
								<?php
								if($params->get('show_user_info') == 'name'){ 
									echo $Topmessage['objCus']->firstname.' '.$Topmessage['objCus']->lastname ;
								}else{
									echo $Topmessage['objCus']->email;
								}
								?>
								</span>
							<?php
								if($params->get('show_email_link')){?>
									</a>
								<?php } ?>
						<?php } ?>
					</div>
					<div class="lofdate">
						<span class="lof_date"><?php echo ($Topmessage ? Tools::displayDate($Topmessage['date_add'],$cookie->id_lang, true, ':') : '');?></span>
					</div>
				</div>
				<div class="lofcontent">
					<h2><?php echo $ticket['subject'];?></h2>
					<?php
						if($ticket['id_order']){
							$tokenOrder = Tools::getAdminToken('AdminOrders'.(int)(Tab::getIdFromClassName('AdminOrders')).(int)($cookie->id_employee));
							echo '<p>'.$l['Order'].': <a href="?tab=AdminOrders&id_order='.$ticket['id_order'].'&vieworder&token='.$tokenOrder.'" title="'.$l['view order'].'">'.sprintf('%06d', $ticket['id_order']).'</a></p>';
						}
						if($ticket['id_product']  && Validate::isLoadedObject($objP = new Product($ticket['id_product'], true, $cookie->id_lang))){
							$tokencatalog = Tools::getAdminToken('AdminCatalog'.(int)(Tab::getIdFromClassName('AdminCatalog')).(int)($cookie->id_employee));
							echo '<p>'.$l['Product'].': <a href="?tab=AdminCatalog&id_product='.$ticket['id_product'].'&updateproduct&token='.$tokencatalog.'" title="'.$l['view product'].'">'.$objP->name.'</a></p>';
						}
					?>
					<?php echo ($Topmessage ? html_entity_decode($Topmessage['message'], ENT_QUOTES, 'UTF-8') : '') ?>
				</div>
				<?php if($Topmessage && $Topmessage['files']){ ?>
					<div class="files">
						<?php foreach($Topmessage['files'] as $file){ ?>
							<div class="file"><a href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/download.php?id_file=<?php echo $file['id_hd_ticket_files'];?>" title=""><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/images/attach.png" alt="<?php echo $l['Attachment'];?>"/>&nbsp;<?php echo $file['filename'];?></a>
							<span><?php echo $l['download'].' '.$file['downloads'].' '.$l['times'];?></span></div>	
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
		
		<div class="lofaction">
			<?php if($ticket['id_hd_status'] != LOF_STATUS_CLOSE) { ?>
			<div class="lofaddnew">
				<a <?php echo ($expandform ? ' style="display:none"' : '');?> class="click_reply button" href="javascript:void(0)" title="<?php echo $l['reply'];?>">
					<img border="0" src="../img/admin/add.gif">
					<?php echo $l['Click here to reply'];?>
				</a>
			</div>
			<?php } ?>
			<div class="lofnotice"><a class="fancy-ajax1 button" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/viewNotices.php?id_hd_ticket='.$obj->id_hd_ticket.'&viewNotice&lofToken='.$lofToken;?>"><?php echo $l['view notices'];?> (<?php echo count($notices); ?>)</a></div>
			<?php if($ticket['id_hd_status'] == LOF_STATUS_CLOSE) { ?>
				<div class="lofac"><a class="button" href="<?php echo $currentIndex.'&viewhd_ticket&id_hd_ticket='.$obj->id_hd_ticket.'&reopenMessage&token='.$token;?>" title="<?php echo $l['Reopen Ticket'];?>"><?php echo $l['Reopen Ticket'];?></a></div>
			<?php }else{ ?>
				<div class="lofac"><a class="button" href="<?php echo $currentIndex.'&viewhd_ticket&id_hd_ticket='.$obj->id_hd_ticket.'&closeMessage&token='.$token;?>"><?php echo $l['Close Ticket'];?></a></div>
			<?php } ?>
				
			<div class="lof-print">
				<a class="button" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); 
				return false;" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/print.php?id_hd_ticket='.$obj->id_hd_ticket; ?>"><?php echo $l['Print'];?></a>
			</div>
		</div>
		<div style="clear:both;">&nbsp;</div>
		<?php if($ticket['id_hd_status'] != LOF_STATUS_CLOSE) { ?>
		<div class="lofformmessage"<?php echo (!$expandform ? ' style="display:none"' : '');?>>
		<?php if($errors){ ?> 
			<div class="error">
				<p><?php echo (count($errors) > 1 ? $l['There are'] : $l['There is']).' '.(count($errors) > 1 ? $l['errors'] : $l['error']);?></p>
				<ol>
				<?php foreach($errors as $e){ ?>
					<li><?php echo $e;?></li>	
				<?php }?>	
				</ol>
			</div>
		<?php } ?>	
			<?php include_once( dirname(__FILE__)."/messagefrom.php" );?>
		</div>
		<?php } ?>
		<?php if($ticket['id_hd_status'] != LOF_STATUS_CLOSE){ ?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					$('.click_reply').click(function(){
						$('.lofformmessage').css({'display':'block'});
					});
				});
			</script>
		<?php }else{
			echo '<p class="mess_close">'.$l['This ticket is marked as closed. No new ticket replies can be added'].'</p>';
		} ?>
		
		<?php if($ticket['id_hd_status'] == LOF_STATUS_CLOSE && $params->get('show_ticket_voting',1)){ ?>
			<div class="lofmessa">
				<p><?php echo $l['Please give us your feedback so we can improve our Customer Support. How would you rate this answer'];?></p>
				<!-- feedback -->
				<ul class="lof_star_rating">
					<li id="lof_current_rating" class="lof_feedback_selected_<?php echo $obj->feedback; ?>">&nbsp;</li>
					<?php if ($obj->feedback == 0 && $obj->id_hd_staff) { ?>
					<li title="<?php echo $l['one star']; ?>"><a href="javascript: void(0);" onclick="lof_feedback(1, <?php echo $obj->id; ?>);" class="lof_one_star" id="lof_feedback_1">&nbsp;</a></li>
					<li title="<?php echo $l['two star']; ?>"><a href="javascript: void(0);" onclick="lof_feedback(2, <?php echo $obj->id; ?>);" class="lof_two_stars" id="lof_feedback_2">&nbsp;</a></li>
					<li title="<?php echo $l['three star']; ?>"><a href="javascript: void(0);" onclick="lof_feedback(3, <?php echo $obj->id; ?>);" class="lof_three_stars" id="lof_feedback_3">&nbsp;</a></li>
					<li title="<?php echo $l['four star']; ?>"><a href="javascript: void(0);" onclick="lof_feedback(4, <?php echo $obj->id; ?>);" class="lof_four_stars" id="lof_feedback_4">&nbsp;</a></li>
					<li title="<?php echo $l['five star']; ?>"><a href="javascript: void(0);" onclick="lof_feedback(5, <?php echo $obj->id; ?>);" class="lof_five_stars" id="lof_feedback_5">&nbsp;</a></li>
					<?php } ?>
				</ul>
			</div>
		<?php }?>
	<?php if($messages)
			foreach($messages as $m){ ?>
				<div class="lofitem">
					<div class="lofheader">
						<?php if($m['id_employee']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/staff-icon.png";?>" alt="<?php echo $l['avatar']; ?>"/></div>
						<?php }elseif($m['id_customer']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/user-icon.png";?>" alt="<?php echo $l['avatar']; ?>"/></div>
						<?php } ?>
						<div class="lofname">
						<?php if(isset($m['objEmp'])){ ?>
							<?php echo ($params->get('show_email_link') ? '<a href="mailto:'.$m['objEmp']->email.'">' : '').'<span class="name_email">'.($params->get('show_user_info') == 'name' ? $m['objEmp']->firstname.' '.$m['objEmp']->lastname : $m['objEmp']->email ).'</span>'.($params->get('show_email_link') ? '</a>' : '')?>
						<?php }elseif($m['objCus']){ ?>
							<?php echo ($params->get('show_email_link') ? '<a href="mailto:'.$m['objCus']->email.'">' : '').'<span class="name_email">'.($params->get('show_user_info') == 'name' ? $m['objCus']->firstname.' '.$m['objCus']->lastname : $m['objCus']->email ).'</span>'.($params->get('show_email_link') ? '</a>' : '')?>
						<?php } ?>
							<span class="wrote"><?php echo $l['wrote'];?>:</span>
						</div>
						<div class="lofdate">
						<?php echo Tools::displayDate($m['date_add'],$cookie->id_lang, true, ':');?>
						</div>
					</div>
					<div class="clear"></div>
					
					<div class="lofcontent" id="lofcontent_<?php echo $m['id_hd_ticket_message'];?>">
						<p><?php echo html_entity_decode($m['message'], ENT_QUOTES, 'UTF-8');?></p>
					</div>
					<div class="lofaction itemlofaction" align="right">
						<a  class="fancy-ajax" onclick="return false;" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/editMessage.php?id_hd_ticket='.$obj->id_hd_ticket.'&editMessage&id_hd_ticket_message='.$m['id_hd_ticket_message'].'&id_lang='.(int)($cookie->id_lang).'&id_tag=lofcontent_'.$m['id_hd_ticket_message'].'&lofToken='.$lofToken;?>" title="">
							<img src="<?php echo _PS_IMG_.'admin/edit.gif';?>" alt="<?php echo $l['Edit'];?>"/>
						</a>
						<a href="<?php echo $currentIndex.'&viewhd_ticket&id_hd_ticket='.$obj->id_hd_ticket.'&deleteMessage&id_hd_ticket_message='.$m['id_hd_ticket_message'].'&token='.$token;?>" title="">
							<img src="<?php echo _PS_IMG_.'admin/delete.gif';?>" alt="<?php echo $l['Delete'];?>"/>
						</a>
					</div>
					<?php
					if($m['files']){?>
						<hr/>
						<?php foreach($m['files'] as $f) { ?>
						<div class="file"><a href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/download.php?id_file='.$f['id_hd_ticket_files'];?>" title="<?php echo 'download';?>"><img src="<?php echo __PS_BASE_URI__."modules/".LOF_MODULE_NAME_HELPDESK.'/assets/images/attach.png'; ?>" alt="<?php echo $l['Attachment'];?>"/>&nbsp;<?php echo $f['filename']; ?></a>
							<span><?php echo $l['download'].' '.$f['downloads'].' '.$l['times'];?></span>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
	<?php 	} ?>
		</div>
		
		<!-- Ticket Information -->
		<?php 
			$divLangName = 'subject';
		?>
		<div id="fragment-2" style="display:none;" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
			<script type="text/javascript">
				var lofinit_data = new Array();
				var nb_file = 0;
				var message_error = "<?php echo $l['The maximum number of attachments has been reached. You cannot add any more attachments'];?>";
				<?php
				if($objdepartments)
					foreach($objdepartments as $objde){?>
						lofinit_data[<?php echo $objde->id;?>] = new Array(<?php echo "".$objde->id.",".$objde->id_hd_priority.",".$objde->upload.",".$objde->upload_files.",'".$objde->upload_extensions."'";?>);
				<?php } ?>
			</script>
			<form action="<?php echo $currentIndex.'&token='.$token.'&lofTab=fragment-2';?>" method="post" enctype="multipart/form-data">
				<fieldset class="loffieldset">
					<legend><?php echo $l['General'];?></legend>
					<div align="center">
						<input type="hidden" name="id_hd_ticket" value="<?php echo $obj->id;?>" />
						<input type="submit" name="editTicket" value="<?php echo $l['Update']; ?>" class="button"/>
						<div class="clear">&nbsp;</div>
					</div>
					<table class="lof">
						<tr><td width="200" class="left"><?php echo $l['Subject'];?>: </td><td>
						<?php
						foreach ($this->_languages as $language)
							echo '
							<div id="subject_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $this->_defaultFormLanguage ? 'block' : 'none').'; float: left;">
								<input size="40" type="text" name="subject_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'subject', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" style="width: 150px;" /><sup> *</sup>
							</div>';
						$this->displayFlags($this->_languages, $this->_defaultFormLanguage, $divLangName, 'subject');
						?>
						</td></tr>
						<tr><td class="left"><?php echo $l['Department'];?>: </td><td>
						<select name="id_hd_department" id="id_hd_department">
					<?php
						if($departments)
							foreach ($departments as $v)
								echo '<option value="'.$v['id_hd_department'].'" '.( $v['id_hd_department'] == $obj->id_hd_department ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
					?>
						</select>
						</td></tr>
						<tr><td class="left"><?php echo $l['Date'];?>: </td><td><?php echo Tools::displayDate($ticket['date_add'],$cookie->id_lang,true,':');?></td></tr>
						<tr><td class="left"><?php echo $l['Status'];?>: </td><td>
						<select name="id_hd_status" id="id_hd_status">
						<?php 
							if($statuss)
								foreach($statuss as $v){
									echo '<option value="'.$v['id_hd_status'].'" '.( $v['id_hd_status'] == $obj->id_hd_status ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
								}
						?>
						</select>
						</td></tr>
						<tr><td class="left"><?php echo $l['Code'];?>: </td><td><?php echo $ticket['code'];?></td></tr>
						<tr><td class="left"><?php echo $l['Priority'];?>: </td><td>
						<select name="id_hd_priority" id="id_hd_priority">
						<?php 
							foreach ($priorities as $v)
							echo '<option value="'.$v['id_hd_priority'].'" '.($obj->id_hd_priority == $v['id_hd_priority'] ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
						?>
						</select>
						</td></tr>
						<tr><td class="left"><?php echo $l['Assigned To'];?>: </td><td>
						<select name="id_hd_staff" id="id_hd_staff">
							<option value="0"<?php echo (!$obj->id_hd_staff ? ' selected="selected"' : '');?>><?php echo $l['Unassigned'];?></option>
						<?php
							
							foreach ($departments as $v){
								echo '<optgroup label="'.$v['name'].'">';
								if($v['employees'])
									foreach($v['employees'] as $e)
									echo '<option value="'.$e['id_hd_staff'].'" '.($obj->id_hd_staff == $e['id_hd_staff'] ? 'selected="selected"' : '').'>'.$e['name'].'</option>';
								echo '</optgroup>';
							}
						?>
						</select>
						</td></tr>
						<tr><td class="left"><?php echo $l['Customer'];?>: </td><td>
						<select name="id_customer" id="id_customer">
						<?php 
						if($customers)
							foreach($customers as $v){
							echo '<option value="'.$v['id_customer'].'" '.($obj->id_customer == $v['id_customer'] ? 'selected="selected"' : '').'>'.$v['firstname'].' '.$v['lastname'].' ('.$v['email'].')'.'</option>';	
							}
						?>
						</select><br/>
						<span><?php echo $l['Filter'];?>: </span><input type="text" name="filtercustomer" id="cus_name" onkeyup="filterCustomer()"/>
						</td></tr>
						<!-- order -->
						<tr><td class="left"><?php echo $l['Assign Order'];?>: </td><td>
						<select name="id_order" id="id_order">
							<option value=""><?php echo $l['select order'];?></option>
						<?php 
						$orders = Order::getCustomerOrders($obj->id_customer);
						if($orders)
							foreach($orders as $o){
							echo '<option value="'.$o['id_order'].'" '.($obj->id_order == $o['id_order'] ? 'selected="selected"' : '').'>#'.$o['id_order'].' -- '.$o['date_add'].'</option>';	
							}
						?>
						</select>
						</td></tr>
						<!-- product -->
						
						<tr><td class="left"><?php echo $l['Assign Product'];?>: </td><td>
						<select name="id_product" id="id_product">
							<option value=""><?php echo $l['select product'];?></option>
						<?php 
							$objP = new Product($obj->id_product, true,$cookie->id_lang);
						if($objP->id)
							echo '<option value="'.$objP->id_product.'" selected="selected">'.$objP->name.'</option>';
						?>
						</select><br/>
						<span><?php echo $l['Filter'];?>: </span><input type="text" name="filterproduct" id="pro_name" onkeyup="filterProduct(<?php echo $cookie->id_lang;?>)"/>
						</td></tr>
						
					</table>
				</fieldset>
				<?php if(isset($fields) && $fields){ ?>
					<div class="clear">&nbsp;</div>
					<fieldset class="loffieldset">
						<legend><?php echo $l['Custom Fields'];?></legend>
						<table class="lof">
						<?php
						/* START Custom fields */
						foreach ($departments as $v){ ?>
							<?php
							if($v['customfields']){ 
								foreach($v['customfields'] as $field){
									echo '<tr class="custom-field custom-'.$v['id_hd_department'].'" id="custom-'.$field->id.'">';
										echo '<td width="200" class="left">'.$field->title.': </td>';
										echo '<td width="670">';
										echo HelpDeskHelper::renderField($field);
										echo '</td>';
									echo '</tr>';
								}
							}
							?>
							<?php
						}
						/* END Custom field */
						?>
						</table>
						<div align="center">
							<div class="clear">&nbsp;</div>
							<input type="submit" name="editTicket" value="<?php echo $l['Update']; ?>" class="button"/>
						</div>
					</fieldset>
				<?php } ?>
			</form>
		</div>
		<!-- ticket history -->
		<?php if($histoties):  ?>
		<div id="fragment-3" style="display:none;" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<div class="lofhistory">
				<ul class="lof">
				<?php 
					$tokenTicket = Tools::getAdminToken('AdminHelpDesk'.(int)(Tab::getIdFromClassName('AdminHelpDesk')).(int)($cookie->id_employee));
					foreach($histoties as $h):
					$objUser = (isset($h['objEmp']) ? $h['objEmp'] : $h['objCus']); ?>
					<li class="item">
						<div class="lofimgage image<?php echo (isset($h['objEmp']) ? '-emp' : (isset($h['objCus']) ? '-cus' : '')); ?>">&nbsp;</div>
						<div class="lofcontent">
							<a href="mailto:<?php echo $objUser->email;?>">
								<?php if($params->get('show_user_info') == 'name'){ echo $objUser->firstname.' '.$objUser->lastname; }else{ echo $objUser->email;} ?>
							</a> <?php 
								$changed = true;
								if($h['status_name']){
									echo (!$changed ? ' ,' : ' ').$l['changed'].' ';
									$changed = false;
									echo $l['Status'].' '.$l['to'].' <a href="?tab=AdminHelpDesk&token='.$tokenTicket.'&hd_ticketFilter_status&id_hd_status='.$h['id_hd_status'].'" title="'.$h['status_name'].'">'.$h['status_name'].'</a>';
								}
								if($h['department_name']){
									echo (!$changed ? ' ,' : ' ').$l['changed'].' ';
									$changed = false;
									echo $l['Department'].' '.$l['to'].' <a href="?tab=AdminHelpDesk&token='.$tokenTicket.'" title="'.$h['department_name'].'">'.$h['department_name'].'</a>';
								}
								if($h['priority_name']){
									echo (!$changed ? ' ,' : ' ').$l['changed'].' ';
									$changed = false;
									echo $l['Priority'].' '.$l['to'].' <a href="?tab=AdminHelpDesk&token='.$tokenTicket.'&hd_ticketFilter_priority&id_hd_priority='.$h['id_hd_priority'].'" title="'.$h['priority_name'].'">'.$h['priority_name'].'</a>';
								}
								if($h['customer_name']){
									echo (!$changed ? ' ,' : ' ').$l['changed'].' ';
									$changed = false;
									echo $l['Customer'].' '.$l['to'].' <a href="mailto:'.$h['customer_email'].'" title="'.$h['customer_name'].'">'.$h['customer_name'].'</a>';
								}
								if($h['employee_name']){
									echo (!$changed ? ' ,' : ' ').$l['changed'].' ';
									$changed = false;
									echo $l['Employee'].' '.$l['to'].' <a href="mailto:'.$h['employee_email'].'" title="'.$h['employee_name'].'">'.$h['employee_name'].'</a>';
								}
								if($h['change_message'] == CHANGE_MESSAGE_DELETE){
									echo (!$changed ? ' ,' : ' ').$l['deleted message id'].' ';
									$changed = false;
									echo '<a href="#show-ful-info'.$h['id_hd_ticket_history'].'" title="'.$h['id_hd_ticket_message'].'" id="lofshowinfo'.$h['id_hd_ticket_history'].'">'.' #'.$h['id_hd_ticket_message'].'</a> ';
									$changed = false;
								}elseif($h['change_message'] == CHANGE_MESSAGE_EDIT){
									echo (!$changed ? ' ,' : ' ').$l['edit message id'].' ';
									echo '<a href="#show-ful-info'.$h['id_hd_ticket_history'].'" title="'.$h['id_hd_ticket_message'].'" id="lofshowinfo'.$h['id_hd_ticket_history'].'">'.' #'.$h['id_hd_ticket_message'].'</a> ';
									$changed = false;
								}
							?>
							<?php echo ' '.$l['in this ticket']; ?>
							<div class="activity_time" title="<?php echo Tools::displayDate($h['date_add'],$cookie->id_lang, true, ':');?>"><?php 
								$date = strtotime(date('Y-m-d H:i:s')) - strtotime($h['date_add']);
								if($date < 60){
									echo $date.' '.($date > 1 ? $l['seconds'] : $l['second']).' '.$l['ago'];
								}elseif($date < 60*60){
									echo floor($date/(60)).' '.(floor($date/(60)) > 1 ? $l['minutes'] : $l['minute']).' '.$l['ago'];
								}elseif($date < 24*60*60){
									echo floor($date/(3600)).' '.(floor($date/(3600)) > 1 ? $l['hours'] : $l['hour']).' '.$l['ago'];
								}elseif($date < 30*24*60*60){
									echo floor($date/(24*3600)).' '.(floor($date/(24*3600)) > 1 ? $l['days'] : $l['day']).' '.$l['ago'];
								}elseif($date < 12*30*24*60*60){
									echo floor($date/(30*24*3600)).' '.(floor($date/(30*24*3600)) > 1 ? $l['months'] : $l['month']).' '.$l['ago'];
								}else{
									echo floor($date/(12*30*24*3600)).' '.(floor($date/(12*30*24*3600)) > 1 ? $l['years'] : $l['year']).' '.$l['ago'];
								}
							?></div>
							<?php if($h['change_message']){ ?>
							<script type="text/javascript">
								$("#lofshowinfo<?php echo $h['id_hd_ticket_history']; ?>").fancybox({
									'scrolling'		: 'no',
									'titleShow'		: false,
								});	
							</script>
							<div style="display:none;">
							<div id="show-ful-info<?php echo $h['id_hd_ticket_history']; ?>">
								<?php echo html_entity_decode($h['message'], ENT_QUOTES, 'UTF-8');?>
							</div>
							</div>
							<?php }	?>
						</div>
					</li>
				<?php endforeach;?>
				</ul>
			</div>
		</div>
		<?php endif; ?>
		<!-- Submitter Information  -->
		<div id="fragment-4" style="display:none;" class="lof_config_wrrapper ui-tabs-panel ui-widget-content ui-corner-bottom">
			<div class="information">
				<div class="row">
					<span><?php echo $l['User Agent'];?>: </span>
					<p><?php echo $ticket['agent'];?></p>
				</div>
				<div>
					<span><?php echo $l['HTTP Referrer'];?>: </span>
					<p><?php echo $ticket['referer'];?></p>
				</div>
				<div class="row">
					<span><?php echo $l['IP Address'];?>: </span>
					<p><?php echo $ticket['ip'];?></p>
				</div>
			</div>
		</div>
	</div>