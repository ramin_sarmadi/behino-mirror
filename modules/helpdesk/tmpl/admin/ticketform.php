<?php 
defined('_HELPDESK') or die('Restricted access');
// add style and js
$this->addCSS(array(
	_MODULE_DIR_."helpdesk/assets/admin/css/formtickets.css",
));
$this->addJS(array(
	_MODULE_DIR_."helpdesk/assets/admin/js/formtickets.js",
	__PS_BASE_URI__."js/tiny_mce/tiny_mce.js",
	_PS_JS_DIR_.'tinymce.inc.js',
	_MODULE_DIR_."helpdesk/assets/tinymce.inc.js"
));

// TinyMCE
global $cookie, $currentIndex;
	$iso = Language::getIsoById((int)($cookie->id_lang));
	$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
	$ad = dirname($_SERVER["PHP_SELF"]);
	echo '
		<script type="text/javascript">	
		var iso = \''.$isoTinyMCE.'\' ;
		var pathCSS = \''._THEME_CSS_DIR_.'\' ;
		var ad = \''.$ad.'\' ;
		var ajaxUrl = "'._MODULE_DIR_.'helpdesk/lofajax.php";
		</script>';
			
$vdepartments = $this->getFieldValue($obj, 'departments');
$id_hd_priority   = (int)$this->getFieldValue($obj, 'id_hd_priority');
if(!$obj->id){
	$divLangName = 'subject';
}else{
	$divLangName = 'subject';
}
?>
<script type="text/javascript">
	var lofinit_data = new Array();
	var nb_file = 0;
	var message_error = "<?php echo $l['The maximum number of attachments has been reached. You cannot add any more attachments'];?>";
	<?php
	if($objdepartments)
		foreach($objdepartments as $objde){?>
			lofinit_data[<?php echo $objde->id;?>] = new Array(<?php echo "".$objde->id.",".$objde->id_hd_priority.",".$objde->upload.",".$objde->upload_files.",'".$objde->upload_extensions."'";?>);
	<?php } ?>
</script>
<?php
global $cookie;
echo '  <form action="'.$currentIndex.'&token='.$token.'" method="post" enctype="multipart/form-data">
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/admin/time.gif" />'.($obj->id ? $l['Edit'] : $l['Create']).' '.$l['New Ticket'].'</legend>
		 
			<div class="clear"></div>
			<label>'.$l['Customer'].': </label>
			<div class="margin-form">
				<select name="id_customer" id="id_customer">
					<option value="">------</option>';
					foreach ($customers as $customer){
					echo '<option value="'.$customer['id_customer'].'"'.($customer['id_customer'] == $obj->id_customer ? 'selected="selected"' : '').'>'.$customer['firstname'].' '.$customer['lastname'].' ('.$customer['email'].')'.'</option>';
					}
		echo' 	</select> <sup>*</sup><br/>
				<span>'.$l['Filter'].': <input type="text" size="30px" onkeyup="filterCustomer()" id="cus_name" name="filtercustomer"></span>
			</div> 
				<div class="clear"></div>
				<label>'.$l['Departments'].': </label>
				<div class="margin-form">
					<select name="id_hd_department" id="id_hd_department">
						<option value="">------</option>';
					if($departments)
						foreach ($departments as $v)
							echo '<option value="'.$v['id_hd_department'].'" '.( $v['id_hd_department'] == $obj->id_hd_department ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
					echo '
					</select> <sup>*</sup>
				</div>
				
				<label>'.$l['Subject'].': </label>
				<div class="margin-form">';
				$defaultLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
				$languages = Language::getLanguages(false);
				echo '<script type="text/javascript">id_language = Number('.$defaultLanguage.');</script>';
		
				foreach ($languages as $language)
					echo '
					<div id="subject_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
						<input size="40" type="text" name="subject_'.$language['id_lang'].'" value="'.htmlentities($this->getFieldValue($obj, 'subject', (int)($language['id_lang'])), ENT_COMPAT, 'UTF-8').'" style="width: 150px;" /><sup> *</sup>
					</div>';
				$this->displayFlags($languages, $defaultLanguage, $divLangName, 'subject');
		echo '	</div>
				<div class="clear">&nbsp;</div>';
				/* order */
				if($obj->id_customer)
					$orders = Order::getCustomerOrders($obj->id_customer);
				else
					$orders = array();
		echo '  <label>'.$l['Assign Order'].':</label>
				<div class="margin-form">
					<select id="id_order" name="id_order">
					<option value="0">'.$l['select order'].'</option>';
					if($orders)
					foreach($orders as $o){
						echo '<option value="'.$o['id_order'].'"'.($o['id_order'] == $obj->id_order ? ' selected="selected"' : '').'>#'.$o['id_order'].' -- '.$o['date_add'].'</option>';
					}
		echo '
					</select>
				</div>
				';
				/* product */
				$objP = new Product($obj->id_product, true,$cookie->id_lang);
					
		echo '  <label>'.$l['Assign Product'].':</label>
				<div class="margin-form">
					<select id="id_product" name="id_product">
					<option value="0">'.$l['select product'].'</option>';
					if($objP->id_product)
						echo '<option value="'.$objP->id_product.'" selected="selected">'.$objP->name.'</option>';
		echo '
					</select></br>
					<span>'.$l['Filter'].': <input type="text" size="30px" onkeyup="filterProduct('.$cookie->id_lang.')" id="pro_name" name="filterproduct"></span>
				</div>
				';
				
				/* START Custom fields */
				foreach ($departments as $v){
					if($v['customfields'])
						foreach($v['customfields'] as $field){
							echo '<div class="custom-field custom-'.$v['id_hd_department'].'" id="custom-'.$field->id.'">';
								echo '<label>'.$field->title.': </label>';
								echo '<div class="margin-form">';
								echo HelpDeskHelper::renderField($field);
								echo '</div>';
							echo '</div>';
							echo '<div class="clear"></div>';
						}
				}
				/* END Custom field */
		echo '  <label>'.$l['Priority'].': </label>
				<div class="margin-form">
					<select name="id_hd_priority" id="id_hd_priority">
						<option value="">------</option>';
						foreach ($priorities as $v)
							echo '<option value="'.$v['id_hd_priority'].'" '.($obj->id_hd_priority == $v['id_hd_priority'] ? 'selected="selected"' : '').'>'.$v['name'].'</option>';
					echo '
					</select> <sup>*</sup>
				</div>';
				if(!$obj->id){
		echo '	<label>'.$l['Message'].': </label>
				<div class="margin-form">';
			echo '	<div id="cmessage">
						<textarea class="rte" cols="80" rows="15" id="message" name="message"></textarea>
					</div>';
			echo '<sup>*</sup>';
		echo '	</div>';
		
		echo '<div class="lofattackment">';
			echo '<label>'.$l['Attachments'].':</label>
				<div class="margin-form">
					<input type="file" name="lof_files[]" id="input_lof_files"/>
					<input type="button" onclick="lof_add_attachments();" value="'.$l['Add attachment'].'" class="button" id="lof_add_attachment"/>
					<div id="lof_files"></div>
					<p>'.$l['Allowed attachments are'].': <b class="lof_extension"></b></p>
				</div>
			</div>';
				}
			echo'<div class="margin-form">
					<input type="submit" value="'.$l['Save'].'" name="submitAdd'.$this->table.'" class="button" />
					<input type="submit" value="'.$l['Save And Another'].'" name="submitAdd'.$this->table.'AndOther" class="button" />
					<input type="submit" value="'.$l['Save And Stay'].'" name="submitAdd'.$this->table.'AndStay" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$l['Required field'].'</div>
			</fieldset>
		</form>';
		