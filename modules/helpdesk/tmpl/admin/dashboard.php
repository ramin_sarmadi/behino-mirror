<?php 
defined('_HELPDESK') or die('Restricted access');
$this->addCSS(array(
	_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/css/dashboard.css",
));
global $currentIndex;
$cookie = $this->context->cookie;
$tokenTicket = Tools::getAdminToken('AdminHelpDesk'.(int)(Tab::getIdFromClassName('AdminHelpDesk')).(int)($cookie->id_employee));
$tokenDepartment = Tools::getAdminToken('AdminHelpDeskDepartField'.(int)(Tab::getIdFromClassName('AdminHelpDeskDepartField')).(int)($cookie->id_employee));
$tokenpriority = Tools::getAdminToken('AdminHelpDeskPriority'.(int)(Tab::getIdFromClassName('AdminHelpDeskPriority')).(int)($cookie->id_employee));
$tokenstaff = Tools::getAdminToken('AdminHelpDeskStaff'.(int)(Tab::getIdFromClassName('AdminHelpDeskStaff')).(int)($cookie->id_employee));
$tokenstatus = Tools::getAdminToken('AdminHelpDeskStatus'.(int)(Tab::getIdFromClassName('AdminHelpDeskStatus')).(int)($cookie->id_employee));
$tokenemail = Tools::getAdminToken('AdminHelpDeskEmailTemplate'.(int)(Tab::getIdFromClassName('AdminHelpDeskEmailTemplate')).(int)($cookie->id_employee));
$tokenconfig = Tools::getAdminToken('AdminHelpDeskConfiguration'.(int)(Tab::getIdFromClassName('AdminHelpDeskConfiguration')).(int)($cookie->id_employee));
?>

<div class="lof-dashboard">
	<fieldset>
		<legend><?php echo $l['Dashboard'];?></legend>
		<div class="dashboard status">
			<ul>
				<?php if($permissions['AdminHelpDesk']['view']){ ?>
				<li><a href="<?php echo '?controller=AdminHelpDesk&token='.$tokenTicket; ?>" title="<?php echo $l['Tickets Manager'];?>">
					<img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/tickets.png";?>" alt="<?php echo $l['Tickets Manager'];?>"/>
					<span><?php echo $l['Tickets Manager'];?></span></a>
				</li>
				<?php }?>
				<?php if($permissions['AdminHelpDeskDepartField']['view']){ ?>
				<li><a href="<?php echo '?controller=AdminHelpDeskDepartField&token='.$tokenDepartment; ?>" title="<?php echo $l['Department Manager'];?>">
					<img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/departments.png";?>" alt="<?php echo $l['Department Manager'];?>"/>
					<span><?php echo $l['Department Manager'];?></span></a>
				</li>
				<?php }?>
				<?php if($permissions['AdminHelpDeskPriority']['view']){ ?>
				<li><a href="<?php echo '?controller=AdminHelpDeskPriority&token='.$tokenpriority; ?>" title="<?php echo $l['Priority Manager'];?>">
					<img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/priorities.png";?>" alt="<?php echo $l['Priority Manager'];?>"/>
					<span><?php echo $l['Priority Manager'];?></span></a>
				</li>
				<?php }?>
				<?php if($permissions['AdminHelpDeskStaff']['view']){ ?>
				<li><a href="<?php echo '?controller=AdminHelpDeskStaff&token='.$tokenstaff; ?>" title="<?php echo $l['Staff Manager'];?>">
					<img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/staff.png";?>" alt="<?php echo $l['Staff Manager'];?>"/>
					<span><?php echo $l['Staff Manager'];?></span></a>
				</li>
				<?php }?>
				<?php if($permissions['AdminHelpDeskStatus']['view']){ ?>
				<li><a href="<?php echo '?controller=AdminHelpDeskStatus&token='.$tokenstatus; ?>" title="<?php echo $l['Status Manager'];?>">
					<img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/statuses.png";?>" alt="<?php echo $l['Manager Status Manager'];?>"/>
					<span><?php echo $l['Status Manager'];?></span></a>
				</li>
				<?php } ?>
				<?php if($permissions['AdminHelpDeskEmailTemplate']['view']){ ?>
				<li><a href="<?php echo '?controller=AdminHelpDeskEmailTemplate&token='.$tokenemail; ?>" title="<?php echo $l['Email Template Manager'];?>">
					<img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/emails.png";?>" alt="<?php echo $l['Email Template Manager'];?>"/>
					<span><?php echo $l['Email Template Manager'];?></span></a>
				</li>
				<?php } ?>
				<?php if($permissions['AdminHelpDeskConfiguration']['view']){ ?>
				<li class="last"><a href="<?php echo '?controller=AdminHelpDeskConfiguration&token='.$tokenconfig; ?>" title="<?php echo $l['Configuration Manager'];?>">
					<img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/configuration.png";?>" alt="<?php echo $l['Configuration Manager'];?>"/>
					<span><?php echo $l['Configuration Manager'];?></span></a>
				</li>
				<?php } ?>
			</ul>
		</div>
	</fieldset>
	<div class="clear">&nbsp;</div>
	<fieldset>
		<legend><?php echo $l['Status'];?></legend>
		<div class="status">
			<?php
			if($status){
			echo '<ul>';
				$i = 1;
				foreach($status as $key=>$s){
				echo '<li'.($i == count($status) ? ' class="last"' : '').'><a href="?controller=AdminHelpDesk&viewhd_ticket&token='.$tokenTicket.'&hd_ticketFilter_status&id_hd_status='.$s['id_hd_status'].'" title="'.$s['name'].'"><span class="name">'.$s['name'].'</span><h4'.($s['id_hd_status'] == LOF_STATUS_OPEN ? ' class="lofselected"' : '').'>'.count($s['tickets']).'</h4></a></li>';
				$i++;
				}
			echo '</ul>';
			}
			?>
		</div>
	</fieldset>
	<div class="clear">&nbsp;</div>
	<fieldset>
		<legend><?php echo $l['Recent Messages'];?></legend>
		<div class="message">
			<?php
			if($messages){
			echo '<ul>';
				foreach($messages as $key=>$m){
					$objUser = (isset($m['objEmp']) ? $m['objEmp'] : $m['objCus']);
					?>
					<li class="item">
						<div class="lofimgage image<?php echo (isset($m['objEmp']) ? '-emp' : (isset($m['objCus']) ? '-cus' : '')); ?>">&nbsp;</div>
						<div class="lofcontent">
							<a href="mailto:<?php echo $objUser->email;?>">
								<?php 
									if($params->get('show_user_info') == 'name'){
										echo $objUser->firstname.' '.$objUser->lastname;
									}else{ 
										echo $objUser->email;
									}
								?>
							</a> <?php echo $l['has sent a reply to'] ;?>  
							<a href="<?php echo '?controller=AdminHelpDesk&viewhd_ticket&token='.$tokenTicket.'&id_hd_ticket='.$m['id_hd_ticket']; ?>" title="<?php echo $m['subject'];?>">
								<?php echo $m['subject']; ?>
							</a>
							<div class="activity_time" title="<?php echo Tools::displayDate($m['date_add'],$cookie->id_lang, true, ':');?>"><?php 
								$date = strtotime(date('Y-m-d H:i:s')) - strtotime($m['date_add']);
								if($date < 60){
									echo $date.' '.($date > 1 ? $l['seconds'] : $l['second']).' '.$l['ago'];
								}elseif($date < 60*60){
									echo floor($date/(60)).' '.(floor($date/(60)) > 1 ? $l['minutes'] : $l['minute']).' '.$l['ago'];
								}elseif($date < 24*60*60){
									echo floor($date/(3600)).' '.(floor($date/(3600)) > 1 ? $l['hours'] : $l['hour']).' '.$l['ago'];
								}elseif($date < 30*24*60*60){
									echo floor($date/(24*3600)).' '.(floor($date/(24*3600)) > 1 ? $l['days'] : $l['day']).' '.$l['ago'];
								}elseif($date < 12*30*24*60*60){
									echo floor($date/(30*24*3600)).' '.(floor($date/(30*24*3600)) > 1 ? $l['months'] : $l['month']).' '.$l['ago'];
								}else{
									echo floor($date/(12*30*24*3600)).' '.(floor($date/(12*30*24*3600)) > 1 ? $l['years'] : $l['year']).' '.$l['ago'];
								}
							?></div>
						</div>
					</li>
					<?php
				}
			echo '</ul>';
			}
			?>
		</div>
	</fieldset>
</div>