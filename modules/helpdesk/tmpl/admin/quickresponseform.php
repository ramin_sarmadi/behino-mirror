<?php 
defined('_HELPDESK') or die('Restricted access');
/*add style and JS */
$this->addJS(array(
	_PS_JS_DIR_."jquery/jquery-colorpicker.js",
	__PS_BASE_URI__."js/tiny_mce/tiny_mce.js",
	_PS_JS_DIR_.'tinymce.inc.js',
	_MODULE_DIR_."helpdesk/assets/tinymce.inc.js",
));
// TinyMCE
global $cookie, $currentIndex;
	$iso = Language::getIsoById((int)($cookie->id_lang));
	$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
	$ad = dirname($_SERVER["PHP_SELF"]);
	echo '
		<script type="text/javascript">	
		var iso = \''.$isoTinyMCE.'\' ;
		var pathCSS = \''._THEME_CSS_DIR_.'\' ;
		var ad = \''.$ad.'\' ;
		var ajaxUrl = "'._MODULE_DIR_.'helpdesk/lofajax.php";
		</script>';
		
echo '
		<form action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.$this->token.'" method="post" enctype="multipart/form-data">
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/admin/time.gif" />'.$l['Quick Response'].'</legend>
				<label>'.$l['Title'].': </label>
				<div class="margin-form">';
					echo '<input size="40" type="text" name="title" value="'.htmlentities($obj->title, ENT_COMPAT, 'UTF-8').'" style="width: 150px;" /><sup> *</sup>';
		echo '	</div>
				<div class="clear"></div>';
		echo '  <label>'.$l['Enable'].': </label>
				<div class="margin-form">
					<input type="radio" name="active" id="active_on" value="1" '.($this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_on"><img src="../img/admin/enabled.gif" alt="'.$l['Enabled'].'" title="'.$l['Enabled'].'" /></label>
					<input type="radio" name="active" id="active_off" value="0" '.(!$this->getFieldValue($obj, 'active') ? 'checked="checked" ' : '').'/>
					<label class="t" for="active_off"><img src="../img/admin/disabled.gif" alt="'.$l['Disabled'].'" title="'.$l['Disabled'].'" /></label>
				</div>	
				<div class="clear"></div>';
		echo '
				<label>'.$l['Content'].': </label>
				<div class="margin-form">
					<textarea class="rte" cols="80" rows="30" id="content_lof" name="content">'.htmlentities(stripslashes($obj->content), ENT_COMPAT, 'UTF-8').'</textarea>
				</div>
				<div class="margin-form">
					<input type="submit" value="'.$l['Save'].'" name="submitAdd'.$this->table.'" class="button" />
				</div>
				<div class="small"><sup>*</sup> '.$l['Required field'].'</div>
			</fieldset>
		</form>';
?>