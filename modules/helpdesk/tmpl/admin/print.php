<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
// add style and js

?>
	<link href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/admin/css/print.css"; ?>" rel="stylesheet" type="text/css" media="all" />
	<div class="lof_container">
		<fieldset class="loffieldset loffieldset1">
			<legend><?php echo $helpdesk->l('Replies');?></legend>
		<!-- top -->
		<div class="topmessage lofitem">
			<div class="lofcontainer">
				<div class="lofheader">
					<?php if($Topmessage['id_employee']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/images/staff-icon.png" alt="<?php echo $helpdesk->l('avatar'); ?>"/></div>
					<?php }elseif($Topmessage['id_customer']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/images/user-icon.png" alt="<?php echo $helpdesk->l('avatar'); ?>"/></div>
					<?php } ?>
					<div class="lofname">
						<?php if(isset($Topmessage['objEmp'])){ ?>
							<?php if($params->get('show_email_link')){?>
								<a href="mailto:<?php echo $Topmessage['objEmp']->email;?>"><?php 
							}?>
								<span class="name_email">
								<?php if($params->get('show_user_info') == 'name'){
									echo $Topmessage['objEmp']->firstname.' '.$Topmessage['objEmp']->lastname;
								}else{ 
									echo $Topmessage['objEmp']->email;
								}?>
								</span>
							<?php 
							if($params->get('show_email_link')){?>
								</a>
							<?php } ?>
						<?php }elseif(isset($Topmessage['objCus'])){ ?>
								<?php if($params->get('show_email_link')){?>
									<a href="mailto:<?php echo $Topmessage['objCus']->email;?>">
								<?php }?>
								<span class="name_email">
								<?php
								if($params->get('show_user_info') == 'name'){ 
									echo $Topmessage['objCus']->firstname.' '.$Topmessage['objCus']->lastname ;
								}else{
									echo $Topmessage['objCus']->email;
								}
								?>
								</span>
							<?php
								if($params->get('show_email_link')){?>
									</a>
								<?php }?>
						<?php } ?>
					</div>
					<div class="lofdate">
						<span class="lof_date"><?php echo Tools::displayDate($Topmessage['date_add'],$cookie->id_lang, true, ':');?></span>
					</div>
				</div>
				<div class="lofcontent">
					<h2><?php echo $ticket['subject'];?></h2>
					<?php echo $Topmessage['message'] ?>
				</div>
				<?php if($Topmessage['files']){ ?>
					<div class="files">
						<?php foreach($Topmessage['files'] as $file){ ?>
							<div class="file"><a href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/download.php?id_file=<?php echo $file['id_hd_ticket_files'];?>" title=""><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/images/attach.png" alt="<?php echo $helpdesk->l('Attachment');?>"/>&nbsp;<?php echo $file['filename'];?></a>
							<span><?php echo $helpdesk->l('download').' '.$file['downloads'].' '.$helpdesk->l('times');?></span></div>	
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
		
		<?php if($ticket['id_hd_status'] == LOF_STATUS_CLOSE){ 
			echo '<p style="margin-left: 6px;">'.$helpdesk->l('This ticket is marked as closed. No new ticket replies can be added.').'</p>';
		} ?>
		
		<?php if($ticket['id_hd_status'] == LOF_STATUS_CLOSE){ ?>
			<div class="lofmessa">
				<p><?php echo $helpdesk->l('Please give us your feedback so we can improve our Customer Support. How would you rate this answer?');?></p>
				<!-- feedback -->
				<ul class="lof_star_rating">
					<li id="lof_current_rating" class="lof_feedback_selected_<?php echo $obj->feedback; ?>">&nbsp;</li>
					<?php if ($obj->feedback == 0 && $obj->id_hd_staff) { ?>
					<li title="<?php echo $helpdesk->l('one star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(1, <?php echo $obj->id; ?>);" class="lof_one_star" id="lof_feedback_1">&nbsp;</a></li>
					<li title="<?php echo $helpdesk->l('two star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(2, <?php echo $obj->id; ?>);" class="lof_two_stars" id="lof_feedback_2">&nbsp;</a></li>
					<li title="<?php echo $helpdesk->l('three star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(3, <?php echo $obj->id; ?>);" class="lof_three_stars" id="lof_feedback_3">&nbsp;</a></li>
					<li title="<?php echo $helpdesk->l('four star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(4, <?php echo $obj->id; ?>);" class="lof_four_stars" id="lof_feedback_4">&nbsp;</a></li>
					<li title="<?php echo $helpdesk->l('five star'); ?>"><a href="javascript: void(0);" onclick="lof_feedback(5, <?php echo $obj->id; ?>);" class="lof_five_stars" id="lof_feedback_5">&nbsp;</a></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	<?php if($messages)
			foreach($messages as $m){ ?>
				<div class="lofitem">
					<div class="lofheader">
						<?php if($m['id_employee']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/staff-icon.png";?>" alt="<?php echo $helpdesk->l('avatar'); ?>"/></div>
						<? }elseif($m['id_customer']){ ?>
						<div class="logavatar"><img src="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK."/assets/images/user-icon.png";?>" alt="<?php echo $helpdesk->l('avatar'); ?>"/></div>
						<?php } ?>
						<div class="lofname">
						<?php if(isset($m['objEmp'])){ ?>
							<?php echo ($params->get('show_email_link') ? '<a href="mailto:'.$m['objEmp']->email.'">' : '').'<span class="name_email">'.($params->get('show_user_info') == 'name' ? $m['objEmp']->firstname.' '.$m['objEmp']->lastname : $m['objEmp']->email ).'</span>'.($params->get('show_email_link') ? '</a>' : '')?>
						<?php }elseif($m['objCus']){ ?>
							<?php echo ($params->get('show_email_link') ? '<a href="mailto:'.$m['objCus']->email.'">' : '').'<span class="name_email">'.($params->get('show_user_info') == 'name' ? $m['objCus']->firstname.' '.$m['objCus']->lastname : $m['objCus']->email ).'</span>'.($params->get('show_email_link') ? '</a>' : '')?>
						<?php } ?>
							<span class="wrote"><?php echo $helpdesk->l('wrote');?>:</span>
						</div>
						<div class="lofdate">
						<?php echo Tools::displayDate($m['date_add'],$cookie->id_lang, true, ':');?>
						</div>
					</div>
					<div class="clear"></div>
					
					<div class="lofcontent" id="lofcontent_<?php echo $m['id_hd_ticket_message'];?>">
						<p><?php echo $m['message'];?></p>
					</div>
					<?php
					if($m['files']){?>
						<hr/>
						<?php foreach($m['files'] as $f) { ?>
						<div class="file"><a href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/download.php?id_file='.$f['id_hd_ticket_files'];?>" title="<?php echo 'download';?>"><img src="<?php echo __PS_BASE_URI__."modules/".LOF_MODULE_NAME_HELPDESK.'/assets/images/attach.png'; ?>" alt="<?php echo $helpdesk->l('Attachment');?>"/>&nbsp;<?php echo $f['filename']; ?></a>
							<span><?php echo $helpdesk->l('download').' '.$f['downloads'].' '.$helpdesk->l('times');?></span>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
	<?php 	} ?>
		<!-- Ticket Infor -->
		</fieldset>
		<fieldset class="loffieldset loffieldset2">
			<legend><?php echo $helpdesk->l('Ticket Information');?></legend>
			<table class="lof">
				<tr><td width="200" class="left"><?php echo $helpdesk->l('Subject');?>: </td><td><?php echo $ticket['subject'];?></td></tr>
				<tr><td class="left"><?php echo $helpdesk->l('Department');?>: </td><td><?php echo $ticket['de_part_name'];?></td></tr>
				<tr><td class="left"><?php echo $helpdesk->l('Date');?>: </td><td><?php echo Tools::displayDate($ticket['date_add'],$cookie->id_lang,true,':');?></td></tr>
				<tr><td class="left"><?php echo $helpdesk->l('Status');?>: </td><td><?php echo $ticket['status_name'];?></td></tr>
				<tr><td class="left"><?php echo $helpdesk->l('Code');?>: </td><td><?php echo $ticket['code'];?></td></tr>
				<tr><td class="left"><?php echo $helpdesk->l('Priority');?>: </td><td><?php echo $ticket['priority_name'];?></td></tr>
				<tr><td class="left"><?php echo $helpdesk->l('Assigned To');?>: </td><td><?php echo $ticket['staff'];?></td></tr>
				<tr><td class="left"><?php echo $helpdesk->l('Customer');?>: </td><td><?php echo $ticket['customer'];?></td></tr>
			
		<?php if(isset($fields)){ ?>
				<?php foreach($fields as $field){ ?>
					<tr><td width="200" class="left"><?php echo $field['title'];?>: </td><td><?php echo $field['lofvalue'];?></td></tr>
				<?php } ?>
		<?php } ?>
			</table>
		</fieldset>
		<div style="clear:both;"></div>
	</div>