<div class="block-center">
	<div class="lofcontents">
		<!-- top -->
		<div class="topmessage lofitem">
			<div class="lofcontainer">
				<div class="lofheader">
					{if $Topmessage.id_employee}
						<div class="logavatar"><img src="{$lof_mod_url}/assets/images/staff-icon.png" alt="{l s='avatar' mod='helpdesk'}"/></div>
					{elseif $Topmessage.id_customer}
						<div class="logavatar"><img src="{$lof_mod_url}/assets/images/user-icon.png" alt="{l s='avatar' mod='helpdesk'}"/></div>
					{/if}
					<div class="lofname">
						{if $Topmessage.objEmployee->id}
							<span class="name_email">{if $show_email_link}<a href="mailto:{$Topmessage.objEmployee->email}">{/if}{if $show_user_info == 'name'}{$Topmessage.objEmployee->firstname} {$Topmessage.objEmployee->lastname}{else}{$Topmessage.objEmployee->email}{/if}{if $show_email_link}</a>{/if}</span>
						{elseif $Topmessage.objCustomer->id}
							<span class="name_email">{if $show_email_link}<a href="mailto:{$Topmessage.objCustomer->email}">{/if}{if $show_user_info == 'name'}{$Topmessage.objCustomer->firstname} {$Topmessage.objCustomer->lastname}{else}{$Topmessage.objCustomer->email}{/if}{if $show_email_link}</a>{/if}</span>
						{/if}
					</div>
					<div class="lofdate">
						<span class="lof_date">{dateFormat date=$Topmessage.date_add full=1}</span>
					</div>
					<a href="#show-ful-info" id="lofshowinfo" class="lofbutton">{l s='Full information' mod='helpdesk'}</a>
				</div>
				<div class="lofcontent">
					<h2>{$ticket.subject}</h2>
					{$Topmessage.message|html_entity_decode:2:'UTF-8'}
				</div>
				{if $Topmessage.files}
					<div class="files">
						{foreach from=$Topmessage.files item=file}
							<div class="file"><a href="{$lof_mod_url}/download.php?id_file={$file.id_hd_ticket_files}" title=""><img src="{$lof_mod_url}/assets/images/attach.png" alt="{l s='Attachment' mod='helpdesk'}"/>&nbsp;{$file.filename}</a>
							<span>{l s='download' mod='helpdesk'} {$file.downloads} {l s='times' mod='helpdesk'}</span></div>	
						{/foreach}
					</div>
				{/if}
			</div>
		</div>
		<!-- action -->
		<div class="lofaction">
			<!--add new -->
			{if $ticket.id_hd_status != $LOF_STATUS_CLOSE}
				<a {if $expandForm} style="display:none;"{/if} class="lofbutton lofbutton_status" href="javascript:void(0)" title="{l s='Click here to reply' mod='helpdesk'}" onclick="formmessage()">{l s='Click here to reply' mod='helpdesk'}</a>
			{/if}
			{if $ticket.id_hd_status == $LOF_STATUS_CLOSE}
				{if $allow_ticket_reopening}
					<a class="lofbutton" href="{$ticket_url}&id_ticket={$ticket.id_hd_ticket}&task=reopen" title="{l s='Reopen Ticket' mod='helpdesk'}">{l s='Reopen Ticket' mod='helpdesk'}</a>
				{/if}
			{else}
				{if $allow_ticket_closing}
					<a class="lofbutton" href="{$ticket_url}&id_ticket={$ticket.id_hd_ticket}&task=close" title="{l s='Close Ticket' mod='helpdesk'}">{l s='Close Ticket' mod='helpdesk'}</a>
				{/if}
			{/if}
			<a class="lofbutton" href="{$lof_mod_url}/print.php?id_hd_ticket={$ticket.id_hd_ticket}" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no');return false;">{l s='Print' mod='helpdesk'}</a>
		</div>
		{if $ticket.id_hd_status == $LOF_STATUS_CLOSE}
			<p>{l s='This ticket is marked as closed. No new ticket replies can be added.' mod='helpdesk'}</p>
		{/if}
		{if $ticket.id_hd_status == $LOF_STATUS_CLOSE && $show_ticket_voting}
			<div class="lofmessa">
				<p id="lof_feedback_message">{if $ticket.feedback}{l s='Thank you for your feedback!' mod='helpdesk'}{else}{l s='Please give us your feedback so we can improve our Customer Support. How would you rate this answer?' mod='helpdesk'}{/if}</p>
				<!-- feedback -->
				<ul class="lof_star_rating lof_stars">
					{if $ticket.feedback == 0}
						<li><a href="javascript: void(0);" onclick="lof_feedback(1, {$ticket.id_hd_ticket},'five');" class="one two three four five" rel="five">&nbsp;</a></li>
						<li><a href="javascript: void(0);" onclick="lof_feedback(2, {$ticket.id_hd_ticket},'four');" class="one two three four" rel="four">&nbsp;</a></li>
						<li><a href="javascript: void(0);" onclick="lof_feedback(3, {$ticket.id_hd_ticket},'three');" class="one two three" rel="three">&nbsp;</a></li>
						<li><a href="javascript: void(0);" onclick="lof_feedback(4, {$ticket.id_hd_ticket},'two');" class="one two" rel="two">&nbsp;</a></li>
						<li><a href="javascript: void(0);" onclick="lof_feedback(5, {$ticket.id_hd_ticket},'one');" class="one" rel="one">&nbsp;</a></li>
						<script type="text/javascript">
							{literal}
							jQuery(document).ready(function($){
								$('ul.lof_stars').find('li a').mouseenter(function(){
									var lofclass = $(this).attr('rel');
									$('ul.lof_stars').find('li .'+lofclass).parent().addClass('hover');
								}).mouseleave(function(){
									$('ul.lof_stars').find('li').removeClass('hover');
								});
							});
							var loffeedback = 1;
							function lof_feedback(value,id_hd_ticket,lofclass){
								if(loffeedback == 0){
									return false;
								}
								$.ajax({
									type: "POST",
									async: true,
									cache: false,
									dataType: 'json',
									url: "{/literal}{$lof_mod_url}{literal}/lofajax.php",
									data: "id_hd_ticket="+id_hd_ticket+"&value="+value+"&task=feedback",
									success: function(jsonData){
										if(jsonData.result == 1){
											loffeedback = 0;
											$('ul.lof_star_rating').removeClass('lof_stars');
											$('ul.lof_star_rating').find('li a.'+lofclass).parent().addClass('hover');
											$('#lof_feedback_message').text("{/literal}{l s='Thank you for your feedback!' mod='helpdesk'}{literal}");
										}
									},
									error: function(XMLHttpRequest, textStatus, errorThrown) {alert("TECHNICAL ERROR: unable to save update quantity \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);}
								});
							}
							{/literal}
						</script>
					{else}
						<li id="lof_current_rating" class="lof_feedback_selected_{$ticket.feedback}">&nbsp;</li>
					{/if}
				</ul>
			</div>
		{/if}
		
		<!-- error -->
		<div class="lofform"{if $expandForm == false} style="display:none;"{/if}>
			{if $errors}
				<div class="error">
					<p>{if $errors|@count > 1}{l s='There are'}{else}{l s='There is'}{/if} {$errors|@count} {if $errors|@count > 1}{l s='errors'}{else}{l s='error'}{/if} :</p>
					<ol>
					{foreach from=$errors item=error}
						<li>{$error}</li>
					{/foreach}
					</ol>
				</div>
			{/if}
			{include file="$form_tpl_message"}
		</div>
		<!-- form -->
		{if $messages|@count >= 1}
		<h3 class="comments-meta">{if $messages|@count == 1}{l s='There is (1) reply' mod='helpdesk'}{else}{l s='There are' mod='helpdesk'} ({$messages|@count}) {l s='replies' mod='helpdesk'}{/if}</h3>
			{foreach from=$messages item=mess key=key}
				<div class="lofitem">
					<div class="lofheader">
						{if $mess.id_employee}
							<div class="logavatar"><img src="{$lof_mod_url}/assets/images/staff-icon.png" alt="{l s='avatar' mod='helpdesk'}"/></div>
						{elseif $mess.id_customer}
							<div class="logavatar"><img src="{$lof_mod_url}/assets/images/user-icon.png" alt="{l s='avatar' mod='helpdesk'}"/></div>
						{/if}
						<div class="lofname">
							{if $mess.objEmployee->id}
								<span class="name_email">{if $show_email_link}<a href="mailto:{$mess.objEmployee->email}">{/if}{if $show_user_info == 'name'}{$mess.objEmployee->firstname} {$mess.objEmployee->lastname}{else}{$mess.objEmployee->email}{/if}{if $show_email_link}</a>{/if}</span>
							{elseif $mess.objCustomer->id}
								<span class="name_email">{if $show_email_link}<a href="mailto:{$mess.objCustomer->email}">{/if}{if $show_user_info == 'name'}{$mess.objCustomer->firstname} {$mess.objCustomer->lastname}{else}{$mess.objCustomer->email}{/if}{if $show_email_link}</a>{/if}</span>
							{/if}
						</div>
						<div class="lofdate">
							{dateFormat date=$mess.date_add full=1}
						</div>
					</div>
					<div class="clear"></div>
					<div class="lofcontent">
						{$mess.message|html_entity_decode:2:'UTF-8'}
					</div>
					{if $mess.files}
						<div class="files">
						{foreach from=$mess.files item=file}
							<div class="file"><a href="{$lof_mod_url}/download.php?id_file={$file.id_hd_ticket_files}" title=""><img src="{$lof_mod_url}/assets/images/attach.png" alt="{l s='Attachment' mod='helpdesk'}"/>&nbsp;{$file.filename}</a>
							<span>{l s='download' mod='helpdesk'} {$file.downloads} {l s='times' mod='helpdesk'}</span></div>	
						{/foreach}
						</div>
					{/if}
				</div>
			{/foreach}
		{/if}
		<script type="text/javascript">
			jQuery(document).ready(function(){
				$("#lofshowinfo").fancybox({
					'scrolling'		: 'no',
					'titleShow'		: false,
				});
			});
		</script>
		<div class="show-ful-info" style="display:none;">
			<div id="show-ful-info">
				<fieldset>
					<legend>{l s='Ticket Information' mod='helpdesk'}</legend>
					<table class="lof">
						<tr><td width="200" class="left">{l s='Subject' mod='helpdesk'}</td><td>{$ticket.subject}</td></tr>
						<tr><td class="left">{l s='Department' mod='helpdesk'}</td><td>{$ticket.de_part_name}</td></tr>
						<tr><td class="left">{l s='Date' mod='helpdesk'}</td><td>{dateFormat date=$ticket.date_add full=1}</td></tr>
						<tr><td class="left">{l s='Status' mod='helpdesk'}</td><td>{$ticket.status_name}</td></tr>
						<tr><td class="left">{l s='Code' mod='helpdesk'}</td><td>{$ticket.code}</td></tr>
						<tr><td class="left">{l s='Priority' mod='helpdesk'}</td><td>{$ticket.priority_name}</td></tr>
						<tr><td class="left">{l s='Assigned To' mod='helpdesk'}</td><td>{$ticket.staff}</td></tr>
						<tr><td class="left">{l s='Customer' mod='helpdesk'}</td><td>{$ticket.customer}</td></tr>
					</table>
				</fieldset>
				{if isset($fields)}
					<fieldset>
						<legend>{l s='Custom Fields' mod='helpdesk'}</legend>
							<table class="lof">
							{foreach from=$fields item=field key=key}
								<tr><td width="200" class="left">{$field.title}</td><td>{$field.lofvalue}</td></tr>
							{/foreach}
							</table>
					</fieldset>
				{/if}
				{if $show_submiter}
					<fieldset>
						<legend>{l s='Submitter Information' mod='helpdesk'}</legend>
						<div class="information">
							<div class="row">
								<span>{l s='User Agent' mod='helpdesk'}</span>
								<p>{$ticket.agent}</p>
							</div>
							<div>
								<span>{l s='HTTP Referrer' mod='helpdesk'}</span>
								<p>{$ticket.referer}</p>
							</div>
							<div class="row">
								<span>{l s='IP Address' mod='helpdesk'}</span>
								<p>{$ticket.ip}</p>
							</div>
						</div>
					</fieldset>
				{/if}
			</div>
		</div>
	</div>
</div>