<div class="block-center">
	<h2>{$ticket.subject}</h2>
	<div class="clearfix ui-tabs ui-widget ui-widget-content ui-corner-all" id="lof-pdf-tab">
		<div class="loftabs">
			<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
				<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-1"><span>{l s='Messages' mod='helpdesk'}</span></a></li>
				<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-2"><span>{l s='Ticket Information' mod='helpdesk'}</span></a></li>
				<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-3"><span>{l s='Custom Fields' mod='helpdesk'}</span></a></li>
				{if $show_submiter}
					<li class="ui-state-default ui-corner-top"><a class="lof-tab" href="javascript:void(0)" rel="#fragment-4"><span>{l s='Submitter Information' mod='helpdesk'}</span></a></li>
				{/if}
			</ul>
		</div>
		<div class="lofcontents">
			<div id="fragment-1" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
				<!--add new -->
				<div class="lofaddnew">
					{if $ticket.id_hd_status != $LOF_STATUS_CLOSE}
						<a {if $expandForm} style="display:none;"{/if} class="button lofbutton" href="javascript:void(0)" title="{l s='Click here to reply' mod='helpdesk'}" onclick="formmessage()">{l s='Click here to reply' mod='helpdesk'}</a>
						{if $errors}
							<div class="error">
								<p>{if $errors|@count > 1}{l s='There are'}{else}{l s='There is'}{/if} {$errors|@count} {if $errors|@count > 1}{l s='errors'}{else}{l s='error'}{/if} :</p>
								<ol>
								{foreach from=$errors item=error}
									<li>{$error}</li>
								{/foreach}
								</ol>
							</div>
						{/if}
						{include file="$form_tpl_message"}
					{else}
						{l s='This ticket is marked as closed. No new ticket replies can be added.' mod='helpdesk'}
					{/if}
				</div>
				<!-- action -->
				{if $ticket.id_hd_status == $LOF_STATUS_CLOSE}
					{if $allow_ticket_reopening}
					<div class="toplofaction">
						<a class="button" href="{$ticket_url}?id_ticket={$ticket.id_hd_ticket}&task=reopen" title="{l s='Reopen Ticket' mod='helpdesk'}">{l s='Reopen Ticket' mod='helpdesk'}</a>
					</div>
					{/if}
				{else}
					{if $allow_ticket_closing}
					<div class="toplofaction">
						<a class="button" href="{$ticket_url}?id_ticket={$ticket.id_hd_ticket}&task=close" title="{l s='Close Ticket' mod='helpdesk'}">{l s='Close Ticket' mod='helpdesk'}</a>
					</div>
					{/if}
				{/if}
				{if $ticket.id_hd_status == $LOF_STATUS_CLOSE}
					<div class="lofmessa">
						<p id="lof_feedback_message">{if $ticket.feedback}{l s='Thank you for your feedback!' mod='helpdesk'}{else}{l s='Please give us your feedback so we can improve our Customer Support. How would you rate this answer?' mod='helpdesk'}{/if}</p>
						<!-- feedback -->
						<ul class="lof_star_rating lof_stars">
							{if $ticket.feedback == 0}
								<li><a href="javascript: void(0);" onclick="lof_feedback(1, {$ticket.id_hd_ticket});" class="one two three four five" rel="five">&nbsp;</a></li>
								<li><a href="javascript: void(0);" onclick="lof_feedback(2, {$ticket.id_hd_ticket});" class="one two three four" rel="four">&nbsp;</a></li>
								<li><a href="javascript: void(0);" onclick="lof_feedback(3, {$ticket.id_hd_ticket});" class="one two three" rel="three">&nbsp;</a></li>
								<li><a href="javascript: void(0);" onclick="lof_feedback(4, {$ticket.id_hd_ticket});" class="one two" rel="two">&nbsp;</a></li>
								<li><a href="javascript: void(0);" onclick="lof_feedback(5, {$ticket.id_hd_ticket});" class="one" rel="one">&nbsp;</a></li>
								<script type="text/javascript">
									{literal}
									jQuery(document).ready(function($){
										$('ul.lof_stars').find('li a').mouseenter(function(){
											var lofclass = $(this).attr('rel');
											$('ul.lof_stars').find('li .'+lofclass).parent().addClass('hover');
										}).mouseleave(function(){
											$('ul.lof_stars').find('li').removeClass('hover');
										});
									});
									var loffeedback = 1;
									function lof_feedback(value,id_hd_ticket){
										if(loffeedback == 0){
											return false;
										}
										$.ajax({
											type: "POST",
											async: true,
											cache: false,
											dataType: 'json',
											url: "{/literal}{$mod_url}{literal}/lofajax.php",
											data: "id_hd_ticket="+id_hd_ticket+"&value="+value+"&task=feedback",
											success: function(jsonData){
												if(jsonData.result == 1){
													loffeedback = 0;
													$('ul.lof_star_rating').removeClass('lof_stars');
													$('#lof_feedback_message').text("{/literal}{l s='Thank you for your feedback!' mod='helpdesk'}{literal}");
												}
											},
											error: function(XMLHttpRequest, textStatus, errorThrown) {alert("TECHNICAL ERROR: unable to save update quantity \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);}
										});
									}
									{/literal}
								</script>
							{else}
								<li id="lof_current_rating" class="lof_feedback_selected_{$ticket.feedback}">&nbsp;</li>
							{/if}
						</ul>
					</div>
				{/if}
				
				{foreach from=$messages item=mess key=key}
					<div class="lofitem">
						<div class="lofheader">
							{if $mess.id_employee}
								<div class="logavatar"><img src="{$mod_url}/assets/images/staff-icon.png" alt="{l s='avatar' mod='helpdesk'}"/></div>
							{elseif $mess.id_customer}
								<div class="logavatar"><img src="{$mod_url}/assets/images/user-icon.png" alt="{l s='avatar' mod='helpdesk'}"/></div>
							{/if}
							<div class="lofname">
								{if $mess.objEmployee->id}
									<span class="name_email">{if $show_email_link}<a href="mailto:{$mess.objEmployee->email}">{/if}{if $show_user_info == 'name'}{$mess.objEmployee->firstname} {$mess.objEmployee->lastname}{else}{$mess.objEmployee->email}{/if}{if $show_email_link}</a>{/if}</span>
								{elseif $mess.objCustomer->id}
									<span class="name_email">{if $show_email_link}<a href="mailto:{$mess.objEmployee->email}">{/if}{if $show_user_info == 'name'}{$mess.objCustomer->firstname} {$mess.objCustomer->lastname}{else}{$mess.objCustomer->email}{/if}{if $show_email_link}</a>{/if}</span>
								{/if}
								<span class="wrote">{l s='wrote' mod='helpdesk'}:</span>
							</div>
							<div class="lofdate">
								<span class="lof_date">{dateFormat date=$mess.date_add full=1}</span>
							</div>
						</div>
						<div class="clear"></div>
						<div class="lofcontent">
							{$mess.message}
						</div>
						{if $mess.files}
							<div class="hr"></div>
							{foreach from=$mess.files item=file}
								<div class="file"><a href="{$mod_url}/download.php?id_file={$file.id_hd_ticket_files}" title=""><img src="{$mod_url}/assets/images/attach.png" alt="{l s='Attachment' mod='helpdesk'}"/>{$file.filename}</a>
								<span>{l s='download' mod='helpdesk'} {$file.downloads} {l s='times' mod='helpdesk'}</span></div>	
							{/foreach}
						{/if}
					</div>
				{/foreach}
			</div>
			<div id="fragment-2" class="ui-tabs-panel ui-widget-content ui-corner-bottom" style="display:none;" >
				<table class="lof">
					<tr><td width="200" class="left">{l s='Subject' mod='helpdesk'}</td><td>{$ticket.subject}</td></tr>
					<tr><td class="left">{l s='Department' mod='helpdesk'}</td><td>{$ticket.de_part_name}</td></tr>
					<tr><td class="left">{l s='Date' mod='helpdesk'}</td><td>{dateFormat date=$ticket.date_add full=1}</td></tr>
					<tr><td class="left">{l s='Status' mod='helpdesk'}</td><td>{$ticket.status_name}</td></tr>
					<tr><td class="left">{l s='Code' mod='helpdesk'}</td><td>{$ticket.code}</td></tr>
					<tr><td class="left">{l s='Priority' mod='helpdesk'}</td><td>{$ticket.priority_name}</td></tr>
					<tr><td class="left">{l s='Assigned To' mod='helpdesk'}</td><td>{$ticket.staff}</td></tr>
					<tr><td class="left">{l s='Customer' mod='helpdesk'}</td><td>{$ticket.customer}</td></tr>
				</table>
			</div>
			<div id="fragment-3" class="ui-tabs-panel ui-widget-content ui-corner-bottom" style="display:none;" >
				{if isset($fields)}
					<table class="lof">
					{foreach from=$fields item=field key=key}
						<tr><td width="200" class="left">{$field.title}</td><td>{$field.lofvalue}</td></tr>
					{/foreach}
					</table>
				{/if}
			</div>
			{if $show_submiter}
				<div id="fragment-4" class="ui-tabs-panel ui-widget-content ui-corner-bottom" style="display:none;" >
					<div class="information">
						<div class="row">
							<span>{l s='User Agent' mod='helpdesk'}</span>
							<p>{$ticket.agent}</p>
						</div>
						<div>
							<span>{l s='HTTP Referrer' mod='helpdesk'}</span>
							<p>{$ticket.referer}</p>
						</div>
						<div class="row">
							<span>{l s='IP Address' mod='helpdesk'}</span>
							<p>{$ticket.ip}</p>
						</div>
					</div>
				</div>
			{/if}
		</div>
	</div>
</div>