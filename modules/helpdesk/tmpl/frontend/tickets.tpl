<div class="block-center">
	<h2 class="lof_h2">{l s='My tickets' mod='helpdesk'}</h2>
	<div class="lof-tickets">
		{if $alltickets && count($alltickets)}
			<table id="order-list" class="lof_std">
				<thead>
					<tr>
						<th class="first_item" width="200">{l s='Subject' mod='helpdesk'}</th>
						<th class="item">{l s='Date' mod='helpdesk'}</th>
						<th class="item">{l s='Last Reply' mod='helpdesk'}</th>
						<th class="item">{l s='Priority' mod='helpdesk'}</th>
						<th class="item">{l s='Status' mod='helpdesk'}</th>
						<th class="last_item">{l s='Assigned' mod='helpdesk'}</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$tickets item=ticket key=key name=ticket_name}
					<tr class="ticket-row {if $smarty.foreach.ticket_name.index % 2}even{else}old{/if}">
						<td class="subject">{if $ticket.has_files}<img src="{$lof_mod_url}/assets/images/attach.png" alt="attachment"/>{/if}
							<a href="{$ticket_url}&id_ticket={$ticket.id_hd_ticket}" title="{$ticket.subject}">{$ticket.code}</a> ({$ticket.replies})<br/>
							<a href="{$ticket_url}&id_ticket={$ticket.id_hd_ticket}" title="{$ticket.subject}">{$ticket.subject}</a>
						</td>
						<td class="date">{dateFormat date=$ticket.date_add full=0}</td>
						<td class="date">{dateFormat date=$ticket.last_reply full=0}</td>
						<td class="priority_name" style="background-color:{$ticket.color_priority}">{$ticket.priority_name}</td>
						<td class="status_name" style="background-color:{$ticket.color_status}">{$ticket.status_name}</td>
						<td class="staff last">{if $ticket.staff}{$ticket.staff}{else}{l s='Not assigned' mod='helpdesk'}{/if}</td>
					</tr>
				{/foreach}
				</tbody>
			</table>
			<div id="block-order-detail" class="hidden">&nbsp;</div>
		{else}
			<p class="warning">{l s='You have no ticket.' mod='helpdesk'}</p>
		{/if}
	</div>
</div>
<div id="pagination" class="pagination">
	{if $nbpagination < $alltickets|@count}
		<ul class="pagination">
		{if $page != 1}
			{assign var='p_previous' value=$page-1}
			<li id="pagination_previous"><a href="{$pagination_link}&p={$p_previous}&n={$nbpagination}">
			&laquo;&nbsp;{l s='Previous' mod='helpdesk'}</a></li>
		{else}
			<li id="pagination_previous" class="disabled"><span>&laquo;&nbsp;{l s='Previous' mod='helpdesk'}</span></li>
		{/if}
		{if $page > 2}
			<li><a href="{$pagination_link}&p=1&n={$nbpagination}">1</a></li>
			{if $page > 3}
				<li class="truncate">...</li>
			{/if}
		{/if}
		{section name=pagination start=$page-1 loop=$page+2 step=1}
			{if $page == $smarty.section.pagination.index}
				<li class="current"><span>{$page|escape:'htmlall':'UTF-8'}</span></li>
			{elseif $smarty.section.pagination.index > 0 && $alltickets|@count+$nbpagination > ($smarty.section.pagination.index)*($nbpagination)}
				<li><a href="{$pagination_link}&p={$smarty.section.pagination.index}&n={$nbpagination}">{$smarty.section.pagination.index|escape:'htmlall':'UTF-8'}</a></li>
			{/if}
		{/section}
		{if $max_page-$page > 1}
			{if $max_page-$page > 2}
				<li class="truncate">...</li>
			{/if}
			<li><a href="{$pagination_link}&p={$max_page}&n={$nbpagination}">{$max_page}</a></li>
		{/if}
		{if $alltickets|@count > $page * $nbpagination}
			{assign var='p_next' value=$page+1}
			<li id="pagination_next"><a href="{$pagination_link}&p={$p_next}&n={$nbpagination}">{l s='Next' mod='helpdesk'}&nbsp;&raquo;</a></li>
		{else}
			<li id="pagination_next" class="disabled"><span>{l s='Next' mod='helpdesk'}&nbsp;&raquo;</span></li>
		{/if}
		</ul>
	{/if}
	{if $alltickets|@count > 30}
		<form action="{$pagination_link}" method="get" class="pagination">
			<p>
				<input type="submit" class="button_mini" value="{l s='OK'}" />
				<label for="nb_item">{l s='items:' mod='helpdesk'}</label>
				<select name="n" id="nb_item">
				{foreach from=$nArray item=nValue}
					{if $nValue <= $alltickets|@count}
						<option value="{$nValue|escape:'htmlall':'UTF-8'}" {if $nbpagination == $nValue}selected="selected"{/if}>{$nValue|escape:'htmlall':'UTF-8'}</option>
					{/if}
				{/foreach}
				</select>
				<input type="hidden" name="p" value="1" />
			</p>
		</form>
	{/if}
</div>