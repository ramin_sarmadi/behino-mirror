<div class="formmessage">
	<form action="{$ticket_url}&id_ticket={$ticket.id_hd_ticket}" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><textarea cols="50" rows="7" name="message"{if $use_editor} class="rte"{/if}></textarea></td>
			</tr>
			<tr class="lofattackment">
				<td>
					<div class="add_attachment"><br/>
						<input type="file" name="lof_files[]" id="input_lof_files"/>
						<input type="button" onclick="lof_add_attachments();" value="{l s='Add attachment' mod='helpdesk'}" id="lof_add_attachment" class="lofbutton"/>
					</div>
					<div id="lof_files"></div>
					<p>{l s='Allowed attachments are' mod='helpdesk'}: <b class="lof_extension"></b></p>
				</td>
			</tr>
			<tr><td colspan="2">
				<br/>
				<br/>
				<input type="hidden" name="id_hd_ticket" value="{$ticket.id_hd_ticket}"/>
				<input type="submit" name="submitMessage" value="{l s='Submit' mod='helpdesk'}" class="lofbutton"/>
				<input type="button" value="{l s='Cancel' mod='helpdesk'}" onclick="closeform()" class="lofbutton"/>
			</td></tr>
		</table>
	</form>
</div>