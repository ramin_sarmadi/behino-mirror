<li>
	<a href="{$link->getModuleLink('helpdesk', 'tickets', ['process' => 'tickets'])}" title="{l s='My tickets' mod='helpdesk'}">
		<img src="{$module_template_dir}logo.gif" alt="{l s='My tickets' mod='helpdesk'}" class="icon" />
	</a>
	<a href="{$link->getModuleLink('helpdesk', 'tickets', ['process' => 'tickets'])}" title="{l s='My tickets' mod='helpdesk'}">
		{l s='My tickets' mod='helpdesk'} ({$nb})
	</a>
</li>
<li>
	<a href="{$link->getModuleLink('helpdesk', 'submitticket', ['process' => 'ticket'])}">
		<img src="{$module_template_dir}logo.gif" alt="{l s='Submit ticket' mod='helpdesk'}" class="icon" />
	</a>
	<a href="{$link->getModuleLink('helpdesk', 'submitticket', ['process' => 'ticket'])}" title="{l s='Submit ticket' mod='helpdesk'}">
		{l s='Submit ticket' mod='helpdesk'}
	</a>
</li>
