<div class="clear">&nbsp;</div>
<div style="width:100%;">
<h2>Help Desk ({count($tickets)})</h2>

	<table cellspacing="0" cellpadding="0" style="width:100%;" class="table">
		<thead>
			<tr>
				<th class="left" width="200">{l s='Subject' mod='helpdesk'}</th>
				<th class="left">{l s='Date' mod='helpdesk'}</th>
				<th class="left">{l s='Last Reply' mod='helpdesk'}</th>
				<th class="left">{l s='Priority' mod='helpdesk'}</th>
				<th class="left">{l s='Status' mod='helpdesk'}</th>
				<th class="left">{l s='Assigned' mod='helpdesk'}</th>
			</tr>
		</thead>
		{if $tickets && count($tickets)}
		<tbody>
		{foreach from=$tickets item=ticket key=key name=ticket_name}
			<tr class="ticket-row {if $smarty.foreach.ticket_name.index % 2}even{else}old{/if}">
				<td class="subject">{if $ticket.has_files}<img src="{$lof_mod_url}/assets/images/attach.png" alt="attachment"/>{/if}
					<a href="{$view_ticket_url}&id_ticket={$ticket.id_hd_ticket}" title="{$ticket.subject}">{$ticket.code}</a> ({$ticket.replies})<br/>
					<a href="{$view_ticket_url}&id_ticket={$ticket.id_hd_ticket}" title="{$ticket.subject}">{$ticket.subject}</a>
				</td>
				<td class="date">{dateFormat date=$ticket.date_add full=0}</td>
				<td class="date">{dateFormat date=$ticket.last_reply full=0}</td>
				<td class="priority_name" style="background-color:{$ticket.color_priority}">{$ticket.priority_name}</td>
				<td class="status_name" style="background-color:{$ticket.color_status}">{$ticket.status_name}</td>
				<td class="staff last">{if $ticket.staff}{$ticket.staff}{else}{l s='Not assigned' mod='helpdesk'}{/if}</td>
			</tr>
		{/foreach}
		</tbody>
		{/if}
	</table>
<div class="clear">&nbsp;</div>
</div>