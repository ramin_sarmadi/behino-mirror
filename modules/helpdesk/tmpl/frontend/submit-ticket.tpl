<script type="text/javascript">
	var ajaxUrl= "{$ajaxUrl}";
	var lofinit_data = new Array();
	var nb_file = 0;
	var message_error = "{$message_error}";
	{if $objdepartments}
		{foreach from=$objdepartments item=objde}
			lofinit_data[{$objde->id}] = new Array({$objde->id},"{$objde->id_hd_priority}","{$objde->upload}","{$objde->upload_files}","{$objde->upload_extensions}");
		{/foreach}
	{/if}
</script>
<script type="text/javascript">
	{literal}
	function reloadCaptcha() {
		document.getElementById('siimage').src = '{/literal}{$url_captcha}{literal}securimage_show.php?sid=' + Math.random();
	}
	{/literal}
</script>
<div class="formticket">
	<h2 class="lof_h2">{l s='Submit Ticket' mod='helpdesk'}</h2>
	{if $errors}
		<div class="error">
			<p>{if $errors|@count > 1}{l s='There are'}{else}{l s='There is'}{/if} {$errors|@count} {if $errors|@count > 1}{l s='errors'}{else}{l s='error'}{/if} :</p>
			<ol>
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ol>
		</div>
	{/if}
	<form action="{$ticket_url}" method="post" enctype="multipart/form-data">
		<div style="display:none;">
		<span>{l s='Customer' mod='helpdesk'}: </span><span class="editable">{$customer->firstname} {$customer->lastname}</span>
		</div>
		<input type="hidden" name="id_customer" value="{$customer->id}"/>
		<label>{l s='Department' mod='helpdesk'}<sup>*</sup>: </label>
		<div class="margin-form">
			<select name="id_hd_department" id="id_hd_department" class="lof">
				<option value="">{l s='------------' mod='helpdesk'}</option>
				{foreach from=$departments item=department}
					<option value="{$department.id_hd_department}"{if isset($smarty.post.id_hd_department) && $smarty.post.id_hd_department == {$department.id_hd_department}} selected="selected"{/if}>{$department.name}</option>
				{/foreach}
			</select>
		</div>
		<label>{l s='Subject' mod='helpdesk'}<sup>*</sup>: </label>
		<div class="margin-form">
			<input type="text" name="subject" id="subject" value="{if isset($smarty.post.subject)}{$smarty.post.subject}{/if}"/>
		</div>
		<!-- order -->
		<label>{l s='Order ID' mod='helpdesk'}: </label>
		<div class="margin-form">
			<select name="id_order" id="id_order">
				<option value="0">{l s='------------' mod='helpdesk'}</option>
				{if $orders}
					{foreach from=$orders item=o}
						<option value="{$o.id_order}">#{$o.id_order} -- {$o.date_add}</option>
					{/foreach}
				{/if}
			</select>
		</div>
		<label>{l s='Product ID' mod='helpdesk'}: </label>
		<div class="margin-form">
			<select name="id_product" id="id_product">
				<option value="0">{l s='------------' mod='helpdesk'}</option>
			</select><br/>
			<div class="div_filter">
			<span>{l s='Filter' mod='helpdesk'}: </span><input type="text" name="filterproduct" onkeyup="filterProduct({$cookie->id_lang})" id="pro_name" class="loffilter"/>
			</div>
		</div>
		<!-- Custom Fields -->
		{foreach from=$departments item=department}
			{if $department.customfields}
				{foreach from=$department.customfields item=field}
					<div class="custom-field custom-{$department.id_hd_department}" id="custom-{$field->id}">
						<label>{$field->title}: </label>
						<div class="margin-form">
							{$field->renderFrom}
						</div>
					</div>
					<div class="clear"></div>
				{/foreach}
			{/if}
		{/foreach}
		<!-- END Custom Fields -->
		<label>{l s='Priority' mod='helpdesk'}<sup>*</sup>: </label>
		<div class="margin-form">	
			<select name="id_hd_priority" id="id_hd_priority" class="lof">
				<option value="">{l s='------------' mod='helpdesk'}</option>
				{foreach from=$priorities item=priority}
					<option value="{$priority.id_hd_priority}"{if isset($smarty.post.id_hd_priority) && $smarty.post.id_hd_priority == {$department.id_hd_priority}} selected="selected"{/if}>{$priority.name}</option>
				{/foreach}
			</select>
		</div>
		<label>{l s='Message' mod='helpdesk'}<sup>*</sup>: </label>
		<div class="margin-form">	
			<textarea cols="50" rows="7" name="message"{if $use_editor} class="rte"{/if}></textarea>
		</div>
		<div class="lofattackment">
			<label>{l s='Attachment(s)' mod='helpdesk'}: </label>
			<div class="margin-form">
				<div class="add_attachment">
					<input type="file" name="lof_files[]" id="input_lof_files" class="lof"/>
					<input type="button" onclick="lof_add_attachments();" value="{l s='Add attachment' mod='helpdesk'}" id="lof_add_attachment" class="exclusive"/>
				</div>
				<div id="lof_files"></div>
				<p>{l s='Allowed attachments are' mod='helpdesk'}: <b class="lof_extension"></b></p>
			</div>
		</div>
		{if $captcha_enabled}
			<label>{l s='Captcha' mod='helpdesk'}<sup>*</sup>: </label>
			<div class="margin-form">
				<img src="{$url_captcha}securimage_show.php?rand={rand(10,1000)}" alt="captcha" id="siimage"/>
				<object type="application/x-shockwave-flash" data="{$url_captcha}securimage_play.swf?audio_file={$url_captcha}securimage_play.php&amp;bgColor1=#fff&amp;bgColor2=#fff&amp;iconColor=#777&amp;borderWidth=1&amp;borderColor=#000" height="32" width="32">
					<param name="movie" value="{$url_captcha}securimage_play.swf?audio_file={$url_captcha}securimage_play.php&amp;bgColor1=#fff&amp;bgColor2=#fff&amp;iconColor=#777&amp;borderWidth=1&amp;borderColor=#000">
				</object>
				&nbsp;
				<a tabindex="-1" style="border-style: none;" href="#" title="Refresh Image" onclick="reloadCaptcha(); this.blur(); return false"><img src="{$url_captcha}images/refresh.png" alt="Reload Image" onclick="this.blur()" align="bottom" border="0"></a>
				</br>
				<input name="captcha" type="text" id="captcha"/>
			</div>
		{/if}
		<br/>
		<br/>
		<input type="submit" name="submitTicket" value="{l s='Submit' mod='helpdesk'}" class="lofbutton"/>
	</form>
</div>