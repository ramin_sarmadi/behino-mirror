<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/defines.helpdesk.php');
require_once(dirname(__FILE__).'/helpdesk.php');
require_once(dirname(__FILE__).'/libs/helper.php' );
require_once(dirname(__FILE__).'/classes/HdConfiguration.php' );
require_once(dirname(__FILE__).'/classes/HdCustomFields.php' );
require_once(dirname(__FILE__).'/classes/HdDepartment.php' );
require_once(dirname(__FILE__).'/classes/HdEmailTemplate.php' );
require_once(dirname(__FILE__).'/classes/HDMail.php' );
require_once(dirname(__FILE__).'/classes/HdParams.php' );
require_once(dirname(__FILE__).'/classes/HdPriority.php' );
require_once(dirname(__FILE__).'/classes/HdStaff.php' );
require_once(dirname(__FILE__).'/classes/Hdstatus.php' );
require_once(dirname(__FILE__).'/classes/HdTicket.php' );
require_once(dirname(__FILE__).'/classes/HdTicketFile.php' );
require_once(dirname(__FILE__).'/classes/HdTicketMessage.php' );

$params = new HdParams(LOF_MODULE_NAME_HELPDESK);
$helpdesk = new helpdesk();
$errors = array();
global $cookie;

$id_hd_ticket_files = Tools::getValue('id_file');
if(!Validate::isLoadedObject($objFile = new HdTicketFile($id_hd_ticket_files)))
	Tools::redirect('index.php');
$hash = md5($objFile->id.' '.$objFile->id_hd_ticket_message);
$path = LOF_UPLOAD_FOLDER.$hash;
if (!file_exists($path)) {
	Tools::redirect('index.php');
}
	
$sql = 'UPDATE `'._DB_PREFIX_.'hd_ticket_files` SET downloads = downloads+1 WHERE id_hd_ticket_files='.$objFile->id.' LIMIT 1 ';
Db::getInstance()->Execute($sql);

@ob_end_clean();
$filename = $objFile->filename;
header("Cache-Control: public, must-revalidate");
header('Cache-Control: pre-check=0, post-check=0, max-age=0');
if (strstr(@$_SERVER["HTTP_USER_AGENT"],"MSIE")==false) {
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
}
header("Expires: 0"); 
header("Content-Description: File Transfer");
header("Expires: Sat, 01 Jan 2000 01:00:00 GMT");
header("Content-Type: application/octet-stream; charset=utf-8");
header("Content-Length: ".(string) filesize($path));
header('Content-Disposition: attachment; filename="'.$filename.'"');
header("Content-Transfer-Encoding: binary\n");
@readfile($path);

