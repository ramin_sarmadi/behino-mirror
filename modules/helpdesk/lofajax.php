<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include(dirname(__FILE__).'/defines.helpdesk.php');
include(dirname(__FILE__).'/classes/HdCustomFields.php');
include(dirname(__FILE__).'/classes/HdTicketMessage.php');
include(dirname(__FILE__).'/classes/HdTicket.php');
include(dirname(__FILE__).'/classes/HdTicketFile.php');
include(dirname(__FILE__).'/classes/HdQuickResponse.php');
include(dirname(__FILE__).'/helpdesk.php');
global $cookie;
$helpdesk = new helpdesk();
if (array_key_exists('ajaxHd_custom_fieldPositions', $_POST)){
	$id_hd_custom_field = (int)(Tools::getValue('id_hd_custom_field'));
	$id_hd_department = (int)(Tools::getValue('id_hd_department'));
	$way = (int)(Tools::getValue('way'));
	$positions = Tools::getValue('hd_custom_field');
	if (is_array($positions))
		foreach ($positions AS $key => $value)
		{
			$pos = explode('_', $value);
			if ((isset($pos[1]) AND isset($pos[2])) AND ($pos[1] == $id_hd_department AND $pos[2] == $id_hd_custom_field))
			{
				$position = $key;
				break;
			}
		}
	$hd_custom_field = new HdCustomFields($id_hd_custom_field);
	if (Validate::isLoadedObject($hd_custom_field))
	{
		if (isset($position) && $hd_custom_field->updatePosition($way, $position)){
			die(true);
		}
		else
			die('{"hasError" : true, errors : "Can not update categories position"}');
	}
	else
		die('{"hasError" : true, "errors" : "This category can not be loaded"}');
}

if (array_key_exists('ajaxHd_ticket_messagePositions', $_POST)){
	$id_hd_ticket_message = (int)(Tools::getValue('id_hd_ticket_message'));
	$id_hd_ticket = (int)(Tools::getValue('id_hd_ticket'));
	$way = (int)(Tools::getValue('way'));
	$positions = Tools::getValue('hd_ticket_message');
	if (is_array($positions))
		foreach ($positions AS $key => $value)
		{
			$pos = explode('_', $value);
			if ((isset($pos[1]) AND isset($pos[2])) AND ($pos[1] == $id_hd_ticket AND $pos[2] == $id_hd_ticket_message))
			{
				$position = $key;
				break;
			}
		}
	$ticketMessage = new HdTicketMessage($id_hd_ticket_message);
	if (Validate::isLoadedObject($ticketMessage))
	{
		if (isset($position) && $ticketMessage->updatePosition($way, $position)){
			die(true);
		}
		else
			die('{"hasError" : true, errors : "Can not update categories position"}');
	}
	else
		die('{"hasError" : true, "errors" : "This category can not be loaded"}');
}

if(Tools::getValue('task') == 'filterCusName'){
	$customer_name = Tools::getValue('customer_name');
	$customers = Customer::searchByName($customer_name);
	$str = '';
	if($customers){
		$i = 0;
		$str .= '<option value="">------</option>';
		foreach($customers as $customer){
			$str .= '<option value="'.$customer['id_customer'].'"'.($i == 0 ? ' selected="selected"' : '').'>'.$customer['firstname'].' '.$customer['lastname'].' ('.$customer['email'].')'.'</option>';
			$i++;
		}
	}
	die($str);
}

if(Tools::getValue('task') == 'filterProName'){
	$pro_name = Tools::getValue('pro_name');
	$id_lang = Tools::getValue('id_lang');
	$products = Product::searchByName($id_lang, $pro_name);
	$str = '';
	$str .= '<option value="0">'.$helpdesk->l('select product').'</option>';
	if($products){
		$i = 0;
		foreach($products as $p){
			$str .= '<option value="'.$p['id_product'].'">'.$p['name'].'</option>';
			$i++;
		}
	}
	die($str);
}

if(Tools::getValue('task') == 'filterOrder'){
	$id_customer = Tools::getValue('id_customer');
	$orders = Order::getCustomerOrders($id_customer);
	$str = '';
	$str .= '<option value="0">'.$helpdesk->l('select order').'</option>';
	if($orders){
		$i = 0;
		foreach($orders as $o){
			$str .= '<option value="'.$o['id_order'].'">#'.$o['id_order'].' -- '.$o['date_add'].'</option>';
			$i++;
		}
	}
	die($str);
}

if(Tools::getValue('task') == 'deleteFile'){
	$id_hd_ticket_files = Tools::getValue('id_hd_ticket_files');
	$result = array();
	$result['result'] = 0;
	if(Validate::isLoadedObject($objFile = new HdTicketFile($id_hd_ticket_files))){
		$result['result'] = (int)($objFile->delete());
	}
	die(json_encode($result));
}

if(Tools::getValue('task') == 'feedback'){
	$id_hd_ticket = Tools::getValue('id_hd_ticket');
	$value = Tools::getValue('value');
	if(!Validate::isInt($value) || $value < 0){
		$value = 0;
	}elseif($value > 5){
		$value = 5;
	}
	$result['result'] = 1;
	if($id_hd_ticket && Validate::isLoadedObject($obj = new HdTicket($id_hd_ticket))){
		$obj->feedback = $value;
		if(!$obj->update()){
			$result['result'] = 0;
		}
	}else{
		$result['result'] = 0;
	}
	die(json_encode($result));
}
if(Tools::getValue('task') == 'useTemplate'){
	global $cookie;
	$id_hd_quickresponse = Tools::getValue('id_hd_quickresponse');
	$id_hd_ticket = Tools::getValue('id_hd_ticket');
	if(Validate::isLoadedObject($obj = new HdQuickResponse($id_hd_quickresponse)) && Validate::isLoadedObject($objTicket = new HdTicket($id_hd_ticket))){
		$id_product = $objTicket->id_product;
		$content = $obj->content;
		if($objTicket->id_order && Validate::isLoadedObject($objO = new Order($objTicket->id_order))){
			$id_order = '#'.sprintf('%06d', $objO->id);
			$content = str_replace('{id_order}', $id_order, $content);
		}
		if($objTicket->id_product && Validate::isLoadedObject($objP = new Product($objTicket->id_product, true, $cookie->id_lang))){
			$content = str_replace('{product_name}', $objP->name, $content);
		}
		if($objTicket->id_customer && Validate::isLoadedObject($objC = new Customer($objTicket->id_customer))){
			$content = str_replace('{customer_name}', $objC->firstname.' '.$objC->lastname, $content);
		}
		die($content);
	}
	die('');
}
die('haha');
