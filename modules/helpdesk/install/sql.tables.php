<?php 
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
 $query = " 
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_configuration` (
	  `name` varchar(255) NOT NULL,
	  `value` text NOT NULL,
	  `id_hd_configuration` int(11) NOT NULL AUTO_INCREMENT,
	  PRIMARY KEY (`id_hd_configuration`),
	  UNIQUE KEY `name` (`name`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8;
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_custom_fields` (
	  `id_hd_custom_field` int(11) NOT NULL AUTO_INCREMENT,
	  `id_hd_department` int(11) NOT NULL,
	  `name` varchar(255) NOT NULL,
	  `type` varchar(255) NOT NULL,
	  `values` text NOT NULL,
	  `required` tinyint(1) NOT NULL,
	  `active` tinyint(1) NOT NULL,
	  `position` int(11) NOT NULL,
	  `rows` int(11) DEFAULT NULL,
	  `cols` int(11) DEFAULT NULL,
	  `size` int(11) DEFAULT NULL,
	  PRIMARY KEY (`id_hd_custom_field`),
	  UNIQUE KEY `id` (`id_hd_custom_field`),
	  KEY `department_id` (`id_hd_department`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8;
	
	INSERT INTO `_DB_PREFIX_hd_custom_fields` (`id_hd_custom_field`, `id_hd_department`, `name`, `type`, `values`, `required`, `active`, `position`, `rows`, `cols`, `size`) VALUES
		(1, 1, 'lof_name', 'textbox', '', 1, 1, 0, 0, 0, 30),
		(2, 1, 'lof_email', 'textbox', '', 1, 1, 1, 0, 0, 30),
		(3, 1, 'lof_company', 'textbox', '', 0, 1, 2, 0, 0, 30);

	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_custom_fields_lang` (
	  `id_hd_custom_field` int(11) NOT NULL,
	  `id_lang` int(11) NOT NULL,
	  `title` varchar(255) NOT NULL,
	  `description` text NOT NULL,
	  `labels` text NOT NULL,
	  `textarea` text NOT NULL,
	  `textbox` text NOT NULL
	) ENGINE=_MYSQL_ENGINE_ DEFAULT CHARSET=utf8;
		
	INSERT INTO `_DB_PREFIX_hd_custom_fields_lang` (`id_hd_custom_field`, `id_lang`, `title`, `description`, `labels`, `textarea`, `textbox`) VALUES
		(1, 1, 'Name', '', '', '', ''),
		(1, 2, 'Name', '', '', '', ''),
		(1, 3, 'Name', '', '', '', ''),
		(1, 4, 'Name', '', '', '', ''),
		(1, 5, 'Name', '', '', '', ''),
		(1, 6, 'Name', '', '', '', ''),
		(2, 1, 'Email', '', '', '', ''),
		(2, 2, 'Email', '', '', '', ''),
		(2, 3, 'Email', '', '', '', ''),
		(2, 4, 'Email', '', '', '', ''),
		(2, 5, 'Email', '', '', '', ''),
		(2, 6, 'Email', '', '', '', ''),
		(3, 1, 'Company', '', '', '', ''),
		(3, 2, 'Company', '', '', '', ''),
		(3, 3, 'Company', '', '', '', ''),
		(3, 4, 'Company', '', '', '', ''),
		(3, 5, 'Company', '', '', '', ''),
		(3, 6, 'Company', '', '', '', '');
		
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_custom_fields_values` (
	  `id_hd_custom_field` int(11) NOT NULL,
	  `id_hd_ticket` int(11) NOT NULL,
	  `value` text NOT NULL,
	  PRIMARY KEY (`id_hd_custom_field`,`id_hd_ticket`)
	) ENGINE=_MYSQL_ENGINE_ DEFAULT CHARSET=utf8;
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_department` (
	  `id_hd_department` int(11) NOT NULL AUTO_INCREMENT,
	  `prefix` varchar(32) NOT NULL,
	  `assignment_type` tinyint(1) NOT NULL,
	  `generation_rule` tinyint(1) NOT NULL,
	  `next_number` int(11) NOT NULL DEFAULT '1',
	  `email_address` varchar(255) NOT NULL,
	  `email_address_fullname` varchar(255) NOT NULL,
	  `email_use_global` tinyint(1) unsigned NOT NULL DEFAULT '1',
	  `customer_send_email` tinyint(1) NOT NULL,
	  `customer_send_copy_email` tinyint(1) NOT NULL DEFAULT '1',
	  `customer_attach_email` tinyint(1) NOT NULL DEFAULT '1',
	  `staff_send_email` tinyint(1) NOT NULL,
	  `staff_attach_email` tinyint(1) NOT NULL DEFAULT '1',
	  `upload` tinyint(1) NOT NULL,
	  `upload_extensions` text NOT NULL,
	  `upload_size` decimal(10,2) unsigned NOT NULL,
	  `upload_files` int(11) NOT NULL,
	  `notify_new_tickets_to` text NOT NULL,
	  `notify_assign` tinyint(1) NOT NULL,
	  `id_hd_priority` int(11) NOT NULL,
	  `cc` text NOT NULL,
	  `bcc` text NOT NULL,
	  `predefined_subjects` text NOT NULL,
	  `active` tinyint(1) NOT NULL,
	  `position` int(11) NOT NULL,
	  PRIMARY KEY (`id_hd_department`),
	  UNIQUE KEY `id` (`id_hd_department`),
	  KEY `customer_send_email` (`customer_send_email`),
	  KEY `staff_send_email` (`staff_send_email`),
	  KEY `upload` (`upload`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 ;
	
	INSERT INTO `_DB_PREFIX_hd_department` (`id_hd_department`, `prefix`, `assignment_type`, `generation_rule`, `next_number`, `email_address`, `email_address_fullname`, `email_use_global`, `customer_send_email`, `customer_send_copy_email`, `customer_attach_email`, `staff_send_email`, `staff_attach_email`, `upload`, `upload_extensions`, `upload_size`, `upload_files`, `notify_new_tickets_to`, `notify_assign`, `id_hd_priority`, `cc`, `bcc`, `predefined_subjects`, `active`, `position`) VALUES
		(1, 'TECH', 1, 1, 0, '', '', 1, 1, 1, 1, 1, 1, 1, 'zip,rar,jpg', '10.00', 0, '', 1, 1, '', '', '', 1, 0),
		(2, 'LICENSE', 0, 1, 0, '', '', 1, 1, 1, 1, 1, 1, 1, 'zip,rar,gif,png,jpg', '128.00', 5, '', 1, 1, '', '', '', 1, 0);

	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_department_lang` (
	  `id_hd_department` int(10) unsigned NOT NULL,
	  `id_lang` int(10) unsigned NOT NULL,
	  `name` varchar(32) NOT NULL,
	  PRIMARY KEY (`id_hd_department`,`id_lang`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
	INSERT INTO `_DB_PREFIX_hd_department_lang` (`id_hd_department`, `id_lang`, `name`) VALUES
		(1, 1, 'Tech Support'),
		(1, 2, 'Tech Support'),
		(1, 3, 'Tech Support'),
		(1, 4, 'Tech Support'),
		(1, 5, 'Tech Support'),
		(1, 6, 'Tech Support'),
		(2, 1, 'Licensing'),
		(2, 2, 'Licensing'),
		(2, 3, 'Licensing'),
		(2, 4, 'Licensing'),
		(2, 5, 'Licensing'),
		(2, 6, 'Licensing');

	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_email` (
	  `id_hd_email` int(11) NOT NULL AUTO_INCREMENT,
	  `active` tinyint(1) NOT NULL,
	  PRIMARY KEY (`id_hd_email`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 ;
	
	INSERT INTO `_DB_PREFIX_hd_email` (`id_hd_email`, `active`) VALUES
		(1, 1),
		(2, 1),
		(3, 1),
		(4, 1),
		(5, 1),
		(6, 1),
		(7, 1),
		(8, 1),
		(9, 1),
		(10, 1),
		(11, 1);

	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_email_lang` (
	  `id_hd_email` int(11) NOT NULL,
	  `id_lang` int(11) NOT NULL,
	  `type` varchar(255) NOT NULL,
	  `subject` varchar(255) NOT NULL,
	  `message` text NOT NULL
	) ENGINE=_MYSQL_ENGINE_ DEFAULT CHARSET=utf8;
	
	INSERT INTO `_DB_PREFIX_hd_email_lang` (`id_hd_email`, `id_lang`, `type`, `subject`, `message`) VALUES
	(6, 2, 'Email sent when a new user is created', 'New user details', '<p>Dear {username}.<br /><br />Thanks for registering. Here is your login details: Username: {username} Password: {password}. <br /><br />Please note that this is your temporary password. Please change it when you login.<br /><br />If you have any concent, please log in to <a href=\"{link_site}\">Our Support Center</a> to raise issue and go to <a href=\"{link_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Note: This is auto email, please do not reply.</p>'),
	(6, 3, 'Email sent when a new user is created', 'New user details', '<p>Dear {username}.<br /><br />Thanks for registering. Here is your login details: Username: {username} Password: {password}. <br /><br />Please note that this is your temporary password. Please change it when you login.<br /><br />If you have any concent, please log in to <a href=\"{link_site}\">Our Support Center</a> to raise issue and go to <a href=\"{link_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Note: This is auto email, please do not reply.</p>'),
	(6, 4, 'Email sent when a new user is created', 'New user details', '<p>Dear {username}.<br /><br />Thanks for registering. Here is your login details: Username: {username} Password: {password}. <br /><br />Please note that this is your temporary password. Please change it when you login.<br /><br />If you have any concent, please log in to <a href=\"{link_site}\">Our Support Center</a> to raise issue and go to <a href=\"{link_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Note: This is auto email, please do not reply.</p>'),
	(6, 5, 'Email sent when a new user is created', 'New user details', '<p>Dear {username}.<br /><br />Thanks for registering. Here is your login details: Username: {username} Password: {password}. <br /><br />Please note that this is your temporary password. Please change it when you login.<br /><br />If you have any concent, please log in to <a href=\"{link_site}\">Our Support Center</a> to raise issue and go to <a href=\"{link_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Note: This is auto email, please do not reply.</p>'),
	(6, 6, 'Email sent when a new user is created', 'New user details', '<p>Dear {username}.<br /><br />Thanks for registering. Here is your login details: Username: {username} Password: {password}. <br /><br />Please note that this is your temporary password. Please change it when you login.<br /><br />If you have any concent, please log in to <a href=\"{link_site}\">Our Support Center</a> to raise issue and go to <a href=\"{link_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Note: This is auto email, please do not reply.</p>'),
	(1, 1, 'Sent to the customer when he submits a new ticket', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket.', '<p>Dear {customer_name},<br /><br />Thank you for contacting us. We will check your issue reply as soon as possible.<br /><br />Message: {message}<br /><br />You can view your ticket and our Reply once sent to you here:<br /><br /><a href=\"{link_ticket}\">{link_ticket}</a><br /><br />Thanks and Best Regards!</p>'),
	(1, 2, 'Sent to the customer when he submits a new ticket', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket.', '<p>Dear {customer_name},<br /><br />Thank you for contacting us. We will check your issue reply as soon as possible.<br /><br />Message: {message}<br /><br />You can view your ticket and our Reply once sent to you here:<br /><br /><a href=\"{link_ticket}\">{link_ticket}</a><br /><br />Thanks and Best Regards!</p>'),
	(1, 3, 'Sent to the customer when he submits a new ticket', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket.', '<p>Dear {customer_name},<br /><br />Thank you for contacting us. We will check your issue reply as soon as possible.<br /><br />Message: {message}<br /><br />You can view your ticket and our Reply once sent to you here:<br /><br /><a href=\"{link_ticket}\">{link_ticket}</a><br /><br />Thanks and Best Regards!</p>'),
	(1, 4, 'Sent to the customer when he submits a new ticket', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket.', '<p>Dear {customer_name},<br /><br />Thank you for contacting us. We will check your issue reply as soon as possible.<br /><br />Message: {message}<br /><br />You can view your ticket and our Reply once sent to you here:<br /><br /><a href=\"{link_ticket}\">{link_ticket}</a><br /><br />Thanks and Best Regards!</p>'),
	(1, 5, 'Sent to the customer when he submits a new ticket', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket.', '<p>Dear {customer_name},<br /><br />Thank you for contacting us. We will check your issue reply as soon as possible.<br /><br />Message: {message}<br /><br />You can view your ticket and our Reply once sent to you here:<br /><br /><a href=\"{link_ticket}\">{link_ticket}</a><br /><br />Thanks and Best Regards!</p>'),
	(1, 6, 'Sent to the customer when he submits a new ticket', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket.', '<p>Dear {customer_name},<br /><br />Thank you for contacting us. We will check your issue reply as soon as possible.<br /><br />Message: {message}<br /><br />You can view your ticket and our Reply once sent to you here:<br /><br /><a href=\"{link_ticket}\">{link_ticket}</a><br /><br />Thanks and Best Regards!</p>'),
	(2, 1, 'Sent to the emails added in the department notify section', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Hello,<br /><br />A new ticket has been posted:<br /><br />Subject: {subject}<br /><br />From {customer_email}:<br /><br />Message: {message}<br /><br />Other information: {custom_fields}</p>'),
	(2, 2, 'Sent to the emails added in the department notify section', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Hello,<br /><br />A new ticket has been posted:<br /><br />Subject: {subject}<br /><br />From {customer_email}:<br /><br />Message: {message}<br /><br />Other information: {custom_fields}</p>'),
	(2, 3, 'Sent to the emails added in the department notify section', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Hello,<br /><br />A new ticket has been posted:<br /><br />Subject: {subject}<br /><br />From {customer_email}:<br /><br />Message: {message}<br /><br />Other information: {custom_fields}</p>'),
	(2, 4, 'Sent to the emails added in the department notify section', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Hello,<br /><br />A new ticket has been posted:<br /><br />Subject: {subject}<br /><br />From {customer_email}:<br /><br />Message: {message}<br /><br />Other information: {custom_fields}</p>'),
	(2, 5, 'Sent to the emails added in the department notify section', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Hello,<br /><br />A new ticket has been posted:<br /><br />Subject: {subject}<br /><br />From {customer_email}:<br /><br />Message: {message}<br /><br />Other information: {custom_fields}</p>'),
	(2, 6, 'Sent to the emails added in the department notify section', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Hello,<br /><br />A new ticket has been posted:<br /><br />Subject: {subject}<br /><br />From {customer_email}:<br /><br />Message: {message}<br /><br />Other information: {custom_fields}</p>'),
	(3, 1, 'Sent to the customer when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {customer_name}.<br /><br />Our staff: {staff_name} has just replied your ticket, please check the reply detail below.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view your ticket here:{code}<br /><br />Thanks and Best Regards!</p>'),
	(3, 2, 'Sent to the customer when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {customer_name}.<br /><br />Our staff: {staff_name} has just replied your ticket, please check the reply detail below.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view your ticket here:{code}<br /><br />Thanks and Best Regards!</p>'),
	(3, 3, 'Sent to the customer when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {customer_name}.<br /><br />Our staff: {staff_name} has just replied your ticket, please check the reply detail below.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view your ticket here:{code}<br /><br />Thanks and Best Regards!</p>'),
	(3, 4, 'Sent to the customer when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {customer_name}.<br /><br />Our staff: {staff_name} has just replied your ticket, please check the reply detail below.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view your ticket here:{code}<br /><br />Thanks and Best Regards!</p>'),
	(3, 5, 'Sent to the customer when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {customer_name}.<br /><br />Our staff: {staff_name} has just replied your ticket, please check the reply detail below.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view your ticket here:{code}<br /><br />Thanks and Best Regards!</p>'),
	(3, 6, 'Sent to the customer when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {customer_name}.<br /><br />Our staff: {staff_name} has just replied your ticket, please check the reply detail below.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view your ticket here:{code}<br /><br />Thanks and Best Regards!</p>'),
	(4, 1, 'Sent to the staff member when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />You have a new message sent from {customer_name}.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view the ticket assigned to you here: {code}<br /><br />Thanks and Best Regards!</p>'),
	(4, 2, 'Sent to the staff member when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />You have a new message sent from {customer_name}.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view the ticket assigned to you here: {code}<br /><br />Thanks and Best Regards!</p>'),
	(4, 3, 'Sent to the staff member when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />You have a new message sent from {customer_name}.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view the ticket assigned to you here: {code}<br /><br />Thanks and Best Regards!</p>'),
	(4, 4, 'Sent to the staff member when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />You have a new message sent from {customer_name}.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view the ticket assigned to you here: {code}<br /><br />Thanks and Best Regards!</p>'),
	(4, 5, 'Sent to the staff member when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />You have a new message sent from {customer_name}.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view the ticket assigned to you here: {code}<br /><br />Thanks and Best Regards!</p>'),
	(4, 6, 'Sent to the staff member when a new reply is added', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />You have a new message sent from {customer_name}.<br /><br />Re: {subject}<br /><br />Message: {message}<br /><br />You can view the ticket assigned to you here: {code}<br /><br />Thanks and Best Regards!</p>'),
	(5, 1, 'Sent to the staff member when a new ticket is submitted and gets auto-assigned to him', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />A new ticket is assigned to you, please check it then give solution to our customer as soon as possible:<br /><br />Subject: {subject}<br /><br />Customer Email:{customer_email}<br /><br />Message: {message}<br /><br />Other infomation: {custom_fields}<br /><br />Thanks and Best Regards!</p>'),
	(5, 2, 'Sent to the staff member when a new ticket is submitted and gets auto-assigned to him', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />A new ticket is assigned to you, please check it then give solution to our customer as soon as possible:<br /><br />Subject: {subject}<br /><br />Customer Email:{customer_email}<br /><br />Message: {message}<br /><br />Other infomation: {custom_fields}<br /><br />Thanks and Best Regards!</p>'),
	(5, 3, 'Sent to the staff member when a new ticket is submitted and gets auto-assigned to him', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />A new ticket is assigned to you, please check it then give solution to our customer as soon as possible:<br /><br />Subject: {subject}<br /><br />Customer Email:{customer_email}<br /><br />Message: {message}<br /><br />Other infomation: {custom_fields}<br /><br />Thanks and Best Regards!</p>'),
	(5, 4, 'Sent to the staff member when a new ticket is submitted and gets auto-assigned to him', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />A new ticket is assigned to you, please check it then give solution to our customer as soon as possible:<br /><br />Subject: {subject}<br /><br />Customer Email:{customer_email}<br /><br />Message: {message}<br /><br />Other infomation: {custom_fields}<br /><br />Thanks and Best Regards!</p>'),
	(5, 5, 'Sent to the staff member when a new ticket is submitted and gets auto-assigned to him', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />A new ticket is assigned to you, please check it then give solution to our customer as soon as possible:<br /><br />Subject: {subject}<br /><br />Customer Email:{customer_email}<br /><br />Message: {message}<br /><br />Other infomation: {custom_fields}<br /><br />Thanks and Best Regards!</p>'),
	(5, 6, 'Sent to the staff member when a new ticket is submitted and gets auto-assigned to him', 'The email subject cannot be edited since it''s automatically added from the subject of the ticket. ', '<p>Dear {staff_name}.<br /><br />A new ticket is assigned to you, please check it then give solution to our customer as soon as possible:<br /><br />Subject: {subject}<br /><br />Customer Email:{customer_email}<br /><br />Message: {message}<br /><br />Other infomation: {custom_fields}<br /><br />Thanks and Best Regards!</p>'),
	(6, 1, 'Email sent when a new user is created', 'New user details', '<p>Dear {username}.<br /><br />Thanks for registering. Here is your login details: Username: {username}, Password: {password}. <br /><br />Please note that this is your temporary password. Please change it when you login.<br /><br />If you have any concern, please log in to <a href=\"{link_site}\">Our Support Center</a> to raise issue and go to <a href=\"{link_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Note: This is auto email, please do not reply.</p>'),
	(7, 1, 'Notification email sent to the customer when his ticket goes into auto-close status', 'Your ticket will be closed', '<p>Dear {customer_name}.<br /><br />Your ticket with subject \"{subject}\" had no activity for {inactive_interval} days. It will be automatically closed in {close_interval} days if no additional action is performed.<br /><br />Please log in to <a href=\"{shop_url}\">Our Support Center</a>, and go to <a href=\"{link_my_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Thanks and Best Regards!</p>'),
	(7, 2, 'Notification email sent to the customer when his ticket goes into auto-close status', 'Your ticket will be closed', '<p>Dear {customer_name}.<br /><br />Your ticket with subject \"{subject}\" had no activity for {inactive_interval} days. It will be automatically closed in {close_interval} days if no additional action is performed.<br /><br />Please log in to <a href=\"{shop_url}\">Our Support Center</a>, and go to <a href=\"{link_my_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Thanks and Best Regards!</p>\r\n<p></p>'),
	(7, 3, 'Notification email sent to the customer when his ticket goes into auto-close status', 'Your ticket will be closed', '<p>Dear {customer_name}.<br /><br />Your ticket with subject \"{subject}\" had no activity for {inactive_interval} days. It will be automatically closed in {close_interval} days if no additional action is performed.<br /><br />Please log in to <a href=\"{shop_url}\">Our Support Center</a>, and go to <a href=\"{link_my_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Thanks and Best Regards!</p>'),
	(7, 4, 'Notification email sent to the customer when his ticket goes into auto-close status', 'Your ticket will be closed', '<p>Dear {customer_name}.<br /><br />Your ticket with subject \"{subject}\" had no activity for {inactive_interval} days. It will be automatically closed in {close_interval} days if no additional action is performed.<br /><br />Please log in to <a href=\"{shop_url}\">Our Support Center</a>, and go to <a href=\"{link_my_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Thanks and Best Regards!</p>'),
	(7, 5, 'Notification email sent to the customer when his ticket goes into auto-close status', 'Your ticket will be closed', '<p>Dear {customer_name}.<br /><br />Your ticket with subject \"{subject}\" had no activity for {inactive_interval} days. It will be automatically closed in {close_interval} days if no additional action is performed.<br /><br />Please log in to <a href=\"{shop_url}\">Our Support Center</a>, and go to <a href=\"{link_my_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Thanks and Best Regards!</p>'),
	(7, 6, 'Notification email sent to the customer when his ticket goes into auto-close status', 'Your ticket will be closed', '<p>Dear {customer_name}.<br /><br />Your ticket with subject \"{subject}\" had no activity for {inactive_interval} days. It will be automatically closed in {close_interval} days if no additional action is performed.<br /><br />Please log in to <a href=\"{shop_url}\">Our Support Center</a>, and go to <a href=\"{link_my_ticket}\">My Tickets</a> in order to view the status of your support request.<br /><br />Thanks and Best Regards!</p>'),
	(8, 1, 'This notification will be sent when the number of customer replies exceeds the setting in the Notice Configuration (and the ticket is unassigned)', '{code} This unassigned ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without a staff member being assigned to it.<br /><br />Customer Information:<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Unassigned Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(8, 2, 'This notification will be sent when the number of customer replies exceeds the setting in the Notice Configuration (and the ticket is unassigned)', '{code} This unassigned ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without a staff member being assigned to it.<br /><br />Customer Information:<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Unassigned Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(8, 3, 'This notification will be sent when the number of customer replies exceeds the setting in the Notice Configuration (and the ticket is unassigned)', '{code} This unassigned ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without a staff member being assigned to it.<br /><br />Customer Information:<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Unassigned Ticket Information Subject: {subject}<br /><br />Message:{message}</p>\r\n<p></p>'),
	(8, 4, 'This notification will be sent when the number of customer replies exceeds the setting in the Notice Configuration (and the ticket is unassigned)', '{code} This unassigned ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without a staff member being assigned to it.<br /><br />Customer Information:<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Unassigned Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(8, 5, 'This notification will be sent when the number of customer replies exceeds the setting in the Notice Configuration (and the ticket is unassigned)', '{code} This unassigned ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without a staff member being assigned to it.<br /><br />Customer Information:<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Unassigned Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(8, 6, 'This notification will be sent when the number of customer replies exceeds the setting in the Notice Configuration (and the ticket is unassigned)', '{code} This unassigned ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without a staff member being assigned to it.<br /><br />Customer Information:<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Unassigned Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(9, 1, 'This notification will be sent when a ticket contains any of the keywords in the Notice Configuration', 'This ticket contains a keyword', '<p>The ticket <a href=\"{link_ticket}\">{code}</a> contains a keyword.<br /><br />Customer Information<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Name: {staff_name}<br /><br />Email: {staff_email}<br /><br />Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(9, 2, 'This notification will be sent when a ticket contains any of the keywords in the Notice Configuration', 'This ticket contains a keyword', '<p>The ticket <a href=\"{link_ticket}\">{code}</a> contains a keyword.<br /><br />Customer Information<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Name: {staff_name}<br /><br />Email: {staff_email}<br /><br />Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(9, 3, 'This notification will be sent when a ticket contains any of the keywords in the Notice Configuration', 'This ticket contains a keyword', '<p>The ticket <a href=\"{link_ticket}\">{code}</a> contains a keyword.<br /><br />Customer Information<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Name: {staff_name}<br /><br />Email: {staff_email}<br /><br />Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(9, 4, 'This notification will be sent when a ticket contains any of the keywords in the Notice Configuration', 'This ticket contains a keyword', '<p>The ticket <a href=\"{link_ticket}\">{code}</a> contains a keyword.<br /><br />Customer Information<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Name: {staff_name}<br /><br />Email: {staff_email}<br /><br />Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(9, 5, 'This notification will be sent when a ticket contains any of the keywords in the Notice Configuration', 'This ticket contains a keyword', '<p>The ticket <a href=\"{link_ticket}\">{code}</a> contains a keyword.<br /><br />Customer Information<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Name: {staff_name}<br /><br />Email: {staff_email}<br /><br />Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(9, 6, 'This notification will be sent when a ticket contains any of the keywords in the Notice Configuration', 'This ticket contains a keyword', '<p>The ticket <a href=\"{link_ticket}\">{code}</a> contains a keyword.<br /><br />Customer Information<br /><br />Name: {customer_name}<br /><br />Email: {customer_email}<br /><br />Staff Information Name: {staff_name}<br /><br />Email: {staff_email}<br /><br />Ticket Information Subject: {subject}<br /><br />Message:{message}</p>'),
	(10, 1, 'This notification will be sent when the number of replies with no staff response exceeds the setting in the Notice Configuration (and the ticket is assigned)', '{code} This ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without any response from the designated staff member.</p>\r\n<p>Customer Information</p>\r\n<p>Name: {customer_name}</p>\r\n<p>Email: {customer_email}</p>\r\n<p>Staff Information Name: {staff_name}</p>\r\n<p>Username: {staff_username}</p>\r\n<p>Email: {staff_email}</p>\r\n<p>Ticket Information Subject: {subject}</p>\r\n<p>Message:{message} </p>'),
	(10, 2, 'This notification will be sent when the number of replies with no staff response exceeds the setting in the Notice Configuration (and the ticket is assigned)', '{code} This ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without any response from the designated staff member.</p>\r\n<p>Customer Information</p>\r\n<p>Name: {customer_name}</p>\r\n<p>Email: {customer_email}</p>\r\n<p>Staff Information Name: {staff_name}</p>\r\n<p>Username: {staff_username}</p>\r\n<p>Email: {staff_email}</p>\r\n<p>Ticket Information Subject: {subject}</p>\r\n<p>Message:{message} </p>\r\n<p></p>'),
	(10, 3, 'This notification will be sent when the number of replies with no staff response exceeds the setting in the Notice Configuration (and the ticket is assigned)', '{code} This ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without any response from the designated staff member.</p>\r\n<p>Customer Information</p>\r\n<p>Name: {customer_name}</p>\r\n<p>Email: {customer_email}</p>\r\n<p>Staff Information Name: {staff_name}</p>\r\n<p>Username: {staff_username}</p>\r\n<p>Email: {staff_email}</p>\r\n<p>Ticket Information Subject: {subject}</p>\r\n<p>Message:{message} </p>\r\n<p></p>'),
	(10, 4, 'This notification will be sent when the number of replies with no staff response exceeds the setting in the Notice Configuration (and the ticket is assigned)', '{code} This ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without any response from the designated staff member.</p>\r\n<p>Customer Information</p>\r\n<p>Name: {customer_name}</p>\r\n<p>Email: {customer_email}</p>\r\n<p>Staff Information Name: {staff_name}</p>\r\n<p>Username: {staff_username}</p>\r\n<p>Email: {staff_email}</p>\r\n<p>Ticket Information Subject: {subject}</p>\r\n<p>Message:{message} </p>\r\n<p></p>'),
	(10, 5, 'This notification will be sent when the number of replies with no staff response exceeds the setting in the Notice Configuration (and the ticket is assigned)', '{code} This ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without any response from the designated staff member.</p>\r\n<p>Customer Information</p>\r\n<p>Name: {customer_name}</p>\r\n<p>Email: {customer_email}</p>\r\n<p>Staff Information Name: {staff_name}</p>\r\n<p>Username: {staff_username}</p>\r\n<p>Email: {staff_email}</p>\r\n<p>Ticket Information Subject: {subject}</p>\r\n<p>Message:{message} </p>\r\n<p></p>'),
	(10, 6, 'This notification will be sent when the number of replies with no staff response exceeds the setting in the Notice Configuration (and the ticket is assigned)', '{code} This ticket has received too many replies', '<p>The ticket {code} has received {replies} replies without any response from the designated staff member.</p>\r\n<p>Customer Information</p>\r\n<p>Name: {customer_name}</p>\r\n<p>Email: {customer_email}</p>\r\n<p>Staff Information Name: {staff_name}</p>\r\n<p>Username: {staff_username}</p>\r\n<p>Email: {staff_email}</p>\r\n<p>Ticket Information Subject: {subject}</p>\r\n<p>Message:{message} </p>\r\n<p></p>'),
	(11, 1, 'Email sent when a ticket has been rejected based on current settings', 'Re: {subject}', '<p>Dear {customer_name},<br /><br />Unfortunately your email for department {department} could not be processed. Only registered users can submit tickets by emai<br />We are sorry for the inconvenience. You can visit our website and register to execute the action. .<br /><br />Thanks and Best Regards!</p>'),
	(11, 2, 'Email sent when a ticket has been rejected based on current settings', 'Re: {subject}', '<p>Dear {customer_name},<br /><br />Unfortunately your email for department {department} could not be processed. Only registered users can submit tickets by emai<br />We are sorry for the inconvenience. You can visit our website and register to execute the action. .<br /><br />Thanks and Best Regards!</p>'),
	(11, 3, 'Email sent when a ticket has been rejected based on current settings', 'Re: {subject}', '<p>Dear {customer_name},<br /><br />Unfortunately your email for department {department} could not be processed. Only registered users can submit tickets by emai<br />We are sorry for the inconvenience. You can visit our website and register to execute the action. .<br /><br />Thanks and Best Regards!</p>'),
	(11, 4, 'Email sent when a ticket has been rejected based on current settings', 'Re: {subject}', '<p>Dear {customer_name},<br /><br />Unfortunately your email for department {department} could not be processed. Only registered users can submit tickets by emai<br />We are sorry for the inconvenience. You can visit our website and register to execute the action. .<br /><br />Thanks and Best Regards!</p>'),
	(11, 5, 'Email sent when a ticket has been rejected based on current settings', 'Re: {subject}', '<p>Dear {customer_name},<br /><br />Unfortunately your email for department {department} could not be processed. Only registered users can submit tickets by emai<br />We are sorry for the inconvenience. You can visit our website and register to execute the action. .<br /><br />Thanks and Best Regards!</p>'),
	(11, 6, 'Email sent when a ticket has been rejected based on current settings', 'Re: {subject}', '<p>Dear {customer_name},<br /><br />Unfortunately your email for department {department} could not be processed. Only registered users can submit tickets by emai<br />We are sorry for the inconvenience. You can visit our website and register to execute the action. .<br /><br />Thanks and Best Regards!</p>');

	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_priority` (
	  `id_hd_priority` int(11) NOT NULL AUTO_INCREMENT,
	  `color` varchar(7) NOT NULL,
	  `fg_color` varchar(7) NOT NULL,
	  `active` tinyint(1) NOT NULL,
	  `position` int(11) NOT NULL,
	  PRIMARY KEY (`id_hd_priority`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8;
	
	INSERT INTO `_DB_PREFIX_hd_priority` (`id_hd_priority`, `color`, `fg_color`, `active`, `position`) VALUES
		(1, '', '', 1, 0),
		(2, '', '', 1, 0),
		(3, '#ffd0b4', '', 1, 0);

	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_priority_lang` (
	  `id_hd_priority` int(10) unsigned NOT NULL,
	  `id_lang` int(10) unsigned NOT NULL,
	  `name` varchar(32) NOT NULL,
	  PRIMARY KEY (`id_hd_priority`,`id_lang`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
	INSERT INTO `_DB_PREFIX_hd_priority_lang` (`id_hd_priority`, `id_lang`, `name`) VALUES
		(1, 1, 'Normal'),
		(1, 2, 'normal'),
		(1, 3, 'normal'),
		(1, 4, 'normal'),
		(1, 5, 'normal'),
		(1, 6, 'normal'),
		(2, 1, 'Low'),
		(2, 2, 'Low'),
		(2, 3, 'Low'),
		(2, 4, 'Low'),
		(2, 5, 'Low'),
		(2, 6, 'Low'),
		(3, 1, 'High'),
		(3, 2, 'High'),
		(3, 3, 'High'),
		(3, 4, 'High'),
		(3, 5, 'High'),
		(3, 6, 'High');
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_staff` (
	  `id_hd_staff` int(11) NOT NULL AUTO_INCREMENT,
	  `id_employee` int(11) NOT NULL,
	  `id_hd_priority` int(11) NOT NULL,
	  `signature` text,
	  UNIQUE KEY `id` (`id_hd_staff`),
	  KEY `group_id` (`id_employee`),
	  KEY `priority_id` (`id_hd_priority`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8;
	
	INSERT INTO `_DB_PREFIX_hd_staff` (`id_hd_staff`, `id_employee`, `id_hd_priority`, `signature`) VALUES
		(1, 1, 2, '');
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_staff_hd_department` (
	  `id_hd_staff` int(11) NOT NULL,
	  `id_hd_department` int(11) NOT NULL,
	  `id_employee` int(11) NOT NULL,
	  KEY `department_id` (`id_hd_department`),
	  KEY `user_id` (`id_hd_staff`)
	) ENGINE=_MYSQL_ENGINE_ DEFAULT CHARSET=utf8;
	
	INSERT INTO `_DB_PREFIX_hd_staff_hd_department` (`id_hd_staff`, `id_hd_department`, `id_employee`) VALUES
		(1, 2, 1),
		(1, 1, 1);
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_status` (
	  `id_hd_status` int(11) NOT NULL AUTO_INCREMENT,
	  `color` varchar(7) NOT NULL,
	  `active` tinyint(1) NOT NULL,
	  PRIMARY KEY (`id_hd_status`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;
	
	INSERT INTO `_DB_PREFIX_hd_status` (`id_hd_status`, `color`, `active`) VALUES
		(1, '#3A6DA1', 1),
		(2, '#E4AC10', 1),
		(3, '#DDDDDD', 1),
		(4, '#C13D2C', 1),
		(5, '#55B449', 1);
		
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_status_lang` (
	  `id_hd_status` int(10) unsigned NOT NULL,
	  `id_lang` int(10) unsigned NOT NULL,
	  `name` varchar(32) NOT NULL,
	  PRIMARY KEY (`id_hd_status`,`id_lang`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
	INSERT INTO `_DB_PREFIX_hd_status_lang` (`id_hd_status`, `id_lang`, `name`) VALUES
		(1, 1, 'Open'),
		(1, 2, 'Open'),
		(1, 3, 'Open'),
		(1, 4, 'Open'),
		(1, 5, 'Open'),
		(1, 6, 'Open'),
		(2, 1, 'On-hold'),
		(2, 2, 'On-hold'),
		(2, 3, 'On-hold'),
		(2, 4, 'On-hold'),
		(2, 5, 'On-hold'),
		(2, 6, 'On-hold'),
		(3, 1, 'Close'),
		(3, 2, 'Close'),
		(3, 3, 'Close'),
		(3, 4, 'Close'),
		(3, 5, 'Close'),
		(3, 6, 'Close'),
		(4, 1, 'Declined'),
		(4, 2, 'Declined'),
		(4, 3, 'Declined'),
		(4, 4, 'Declined'),
		(4, 5, 'Declined'),
		(4, 6, 'Declined'),
		(5, 1, 'In Progress'),
		(5, 2, 'In Progress'),
		(5, 3, 'In Progress'),
		(5, 4, 'In Progress'),
		(5, 5, 'In Progress'),
		(5, 6, 'In Progress');
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_ticket` (
	  `id_hd_ticket` int(11) NOT NULL AUTO_INCREMENT,
	  `id_hd_department` int(11) NOT NULL,
	  `id_hd_staff` int(11) NOT NULL,
	  `id_customer` int(11) NOT NULL,
	  `code` varchar(255) NOT NULL,
	  `id_hd_status` int(11) NOT NULL,
	  `id_hd_priority` int(11) NOT NULL,
	  `date_add` datetime NOT NULL,
	  `last_reply` datetime NOT NULL,
	  `last_reply_customer` tinyint(1) NOT NULL,
	  `replies` int(11) NOT NULL,
	  `autoclose_sent` int(11) NOT NULL DEFAULT '0',
	  `flagged` tinyint(1) NOT NULL DEFAULT '0',
	  `agent` text NOT NULL,
	  `referer` text NOT NULL,
	  `ip` varchar(16) NOT NULL,
	  `logged` tinyint(1) NOT NULL,
	  `feedback` tinyint(1) NOT NULL,
	  `has_files` tinyint(1) unsigned NOT NULL,
	  `time_spent` decimal(10,2) NOT NULL,
	  `id_order` int(11) NOT NULL,
	  `id_product` int(11) NOT NULL,
	  PRIMARY KEY (`id_hd_ticket`),
	  KEY `department_id` (`id_hd_department`),
	  KEY `staff_id` (`id_hd_staff`),
	  KEY `customer_id` (`id_customer`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 ;
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_ticket_files` (
	  `id_hd_ticket_files` int(11) NOT NULL AUTO_INCREMENT,
	  `id_hd_ticket` int(11) NOT NULL,
	  `id_hd_ticket_message` int(11) NOT NULL,
	  `filename` varchar(255) NOT NULL,
	  `mime` varchar(255) NOT NULL,
	  `downloads` int(11) NOT NULL,
	  PRIMARY KEY (`id_hd_ticket_files`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 ;
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_ticket_history` (
	  `id_hd_ticket_history` int(11) NOT NULL AUTO_INCREMENT,
	  `id_hd_ticket` int(11) NOT NULL,
	  `id_customer` int(11) NOT NULL,
	  `id_employee` int(11) NOT NULL,
	  `ip` varchar(16) NOT NULL,
	  `date_add` datetime NOT NULL,
	  `change_status` int(11) NOT NULL,
	  `change_department` int(11) NOT NULL,
	  `change_customer` int(11) NOT NULL,
	  `change_staff` int(11) NOT NULL,
	  `change_priority` int(11) NOT NULL,
	  `change_message` varchar(10) DEFAULT NULL,
	  `id_hd_ticket_message` int(11) NOT NULL,
	  `message` text,
	  PRIMARY KEY (`id_hd_ticket_history`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 ;
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_ticket_lang` (
	  `id_hd_ticket` int(11) NOT NULL,
	  `id_lang` int(11) NOT NULL,
	  `subject` varchar(255) NOT NULL
	) ENGINE=_MYSQL_ENGINE_ DEFAULT CHARSET=utf8;
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_ticket_messages` (
	  `id_hd_ticket_message` int(11) NOT NULL AUTO_INCREMENT,
	  `id_hd_ticket` int(11) NOT NULL,
	  `id_customer` int(11) NOT NULL,
	  `date_add` datetime NOT NULL,
	  `id_employee` int(11) NOT NULL,
	  `position` int(11) NOT NULL,
	  `active` tinyint(1) NOT NULL,
	  `message` text NOT NULL,
	  PRIMARY KEY (`id_hd_ticket_message`),
	  KEY `ticket_id` (`id_hd_ticket`),
	  KEY `user_id` (`id_customer`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 ;
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_ticket_notes` (
	  `id_hd_ticket_notes` int(11) NOT NULL AUTO_INCREMENT,
	  `id_hd_ticket` int(11) NOT NULL,
	  `id_employee` int(11) NOT NULL,
	  `date_add` datetime NOT NULL,
	  PRIMARY KEY (`id_hd_ticket_notes`),
	  KEY `ticket_id` (`id_hd_ticket`),
	  KEY `user_id` (`id_employee`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 ;
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_ticket_notes_lang` (
	  `id_hd_ticket_notes` int(11) NOT NULL,
	  `id_lang` int(11) NOT NULL,
	  `text` text NOT NULL
	) ENGINE=_MYSQL_ENGINE_ DEFAULT CHARSET=utf8;
	
	CREATE TABLE IF NOT EXISTS `_DB_PREFIX_hd_quickresponse` (
	  `id_hd_quickresponse` int(11) NOT NULL AUTO_INCREMENT,
	  `title` varchar(255) NOT NULL,
	  `content` text NOT NULL,
	  `active` tinyint(4) NOT NULL,
	  PRIMARY KEY (`id_hd_quickresponse`)
	) ENGINE=_MYSQL_ENGINE_  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;
	
	INSERT INTO `_DB_PREFIX_hd_quickresponse` (`id_hd_quickresponse`, `title`, `content`, `active`) VALUES
	(1, 'Variables Template', '<p>Order {id_order} - Item Temporarily Out of StockL</p>\r\n<p>Dear {customer_name},</p>\r\n<p>Thank you for your order with us</p>\r\n<p>Order {id_order} is currently out of stock, and will not ship until XX/XX/XXXX. We apologize for this inconvenience.</p>\r\n<p></p>', 1),
	(2, 'Yes', '<p>Hello,</p>\r\n<p>Yes we can do that.</p>\r\n<p>Thanks</p>', 1);
 ";
?>