<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
session_start();

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');

require_once(dirname(__FILE__).'/defines.helpdesk.php');
require_once(dirname(__FILE__).'/helpdesk.php');

require_once( dirname(__FILE__).'/libs/helper.php' );
require_once( dirname(__FILE__).'/classes/HdConfiguration.php' );
require_once( dirname(__FILE__).'/classes/HdCustomFields.php' );
require_once( dirname(__FILE__).'/classes/HdDepartment.php' );
require_once( dirname(__FILE__).'/classes/HdEmailTemplate.php' );
require_once( dirname(__FILE__).'/classes/HDMail.php' );
require_once( dirname(__FILE__).'/classes/HdParams.php' );
require_once( dirname(__FILE__).'/classes/HdPriority.php' );
require_once( dirname(__FILE__).'/classes/HdStaff.php' );
require_once( dirname(__FILE__).'/classes/Hdstatus.php' );
require_once( dirname(__FILE__).'/classes/HdTicket.php' );
require_once( dirname(__FILE__).'/classes/HdTicketFile.php' );
require_once( dirname(__FILE__).'/classes/HdTicketMessage.php' );
/** security */
$params = new HdParams(LOF_MODULE_NAME_HELPDESK);
$helpdesk = new helpdesk();
$errors = array();
$k = Configuration::get('_LOF_HELPDESK_SESSION_KEY_');
if(!$k)
	die( $helpdesk->l("Nothing Here........") );
$lofToken = Tools::getValue('lofToken');
$folderAdmin = HelpDeskHelper::getFolderAdmin();
$id_employee = $_SESSION[$k.'_EMPLOYEE'];
if(md5($id_employee.$folderAdmin) != $lofToken)
	die( $helpdesk->l("Nothing Here........") );
/** end security */	
global $cookie;
?>
	<link type="text/css" rel="stylesheet" href="<?php echo __PS_BASE_URI__;?>css/admin.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo _MODULE_DIR_;?>helpdesk/assets/admin/css/admin.css" />
	<script type="text/javascript">
			var helpboxes = 1;
			var roundMode = 2;
		</script>
	<script type="text/javascript" src="<?php echo __PS_BASE_URI__;?>js/jquery/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<?php echo __PS_BASE_URI__;?>js/admin.js"></script>
<?php
$id_hd_ticket_message = Tools::getValue('id_hd_ticket_message');
$id_hd_ticket = Tools::getValue('id_hd_ticket');
$objTicket = new HdTicket($id_hd_ticket);
if(!HelpDeskHelper::checkPermission($id_employee, $objTicket)){
	die( $helpdesk->l("You do not have permission to edit here.") );
}
if(!Validate::isLoadedObject($obj = new HdTicketMessage($id_hd_ticket_message)))
	die($helpdesk->l('Object is cant not load.'));
if(Tools::isSubmit('saveMessage')){
	$oldMessage = $obj->message;
	$message = Tools::getValue('message');
	if(!$message)
		$errors[] = $helpdesk->l("Field message is required.");
	if(!sizeof($errors)){
		$obj->message = Tools::htmlentitiesUTF8($message);
		
		if(!$obj->update()){
			die($helpdesk->l('can not update message.'));
		}else{
			$objHistory = new HdTicketHistory();
			$objHistory->change_message = CHANGE_MESSAGE_EDIT;
			$objHistory->id_hd_ticket_message = $id_hd_ticket_message;
			$objHistory->message = $oldMessage;
			
			$objHistory->id_hd_ticket = $id_hd_ticket;
			$objHistory->ip = $_SERVER['REMOTE_ADDR'];
			$objHistory->id_employee = $id_employee;
			$objHistory->add();
			
			$id = Tools::getValue('id_tag');
			?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					var parentWindow = $( parent.document.body );  
					parentWindow.find("#<?php echo $id;?>").html($('#message').val()); 
					parent.$.fancybox.close();
				});
			</script>
			<?php
		}
	}
}
?>

<div id="content">
	<?php if($errors){ ?>
	<div class="error">
		<p><?php echo (count($errors) > 1 ? $helpdesk->l('There are') : $helpdesk->l('There is')).' '.count($errors).' '.(count($errors) > 1 ? $helpdesk->l('errors') : $helpdesk->l('error'));?> :</p>
		<ol>
		<?php foreach($errors as $e){ ?>
			<li><?php echo $e;?></li>
		<?php } ?>
		</ol>
	</div>
	<?php } ?>
	<form action="#" method="post" id="lofeditmessage">
		<input type="hidden" name="id_hd_ticket_message" value="<?php echo $obj->id;?>"/>
		<?php
		echo '	<label>'.$helpdesk->l('Message').': </label>
				<div class="margin-form">
					<textarea class="rte" cols="80" rows="30" id="message" name="message">'.$obj->message.'</textarea>
					<sup>*</sup>';
		echo '	</div>';
		echo '<input type="submit" value="'.$helpdesk->l('Save').'" name="saveMessage" class="button"/>';
		

		// TinyMCE
			global $cookie;
			$iso = Language::getIsoById((int)($cookie->id_lang));
			$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
			$ad = dirname($_SERVER["PHP_SELF"]);
			
			echo '
				<script type="text/javascript">	
					var iso = \''.$isoTinyMCE.'\' ;
					var pathCSS = \''._THEME_CSS_DIR_.'\' ;
					var ad = \''.$ad.'\' ;
				</script>
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
				<script type="text/javascript" src="'._MODULE_DIR_.'helpdesk/assets/tinymce.inc.js"></script>';
		?>
	</form>
</div>