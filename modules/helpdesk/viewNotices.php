<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
session_start();

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');

require_once(dirname(__FILE__).'/defines.helpdesk.php');
require_once(dirname(__FILE__).'/helpdesk.php');

require_once( dirname(__FILE__).'/libs/helper.php' );
require_once( dirname(__FILE__).'/classes/HdEmailTemplate.php' );
require_once( dirname(__FILE__).'/classes/HDMail.php' );
require_once( dirname(__FILE__).'/classes/HdParams.php' );
require_once( dirname(__FILE__).'/classes/HdStaff.php' );
require_once( dirname(__FILE__).'/classes/HdTicket.php' );
require_once( dirname(__FILE__).'/classes/HdTicketNotice.php' );
/** security */
if(!defined('_HELPDESK'))
	define('_HELPDESK',1);
$params = new HdParams(LOF_MODULE_NAME_HELPDESK);
$helpdesk = new helpdesk();
$errors = array();
$k = Configuration::get('_LOF_HELPDESK_SESSION_KEY_');
if(!$k)
	die( $helpdesk->l("Nothing Here........") );
$lofToken = Tools::getValue('lofToken');
$folderAdmin = HelpDeskHelper::getFolderAdmin();
$id_employee = $_SESSION[$k.'_EMPLOYEE'];
if(md5($id_employee.$folderAdmin) != $lofToken)
	die( $helpdesk->l("Nothing Here........") );
/** end security */	
global $cookie;
?>
	<link type="text/css" rel="stylesheet" href="<?php echo __PS_BASE_URI__;?>css/admin.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/admin/css/admin.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/admin/css/global.css" />
	<script type="text/javascript">
		var helpboxes = 1;
		var roundMode = 2;
	</script>
	<script type="text/javascript" src="<?php echo __PS_BASE_URI__;?>js/jquery/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="<?php echo __PS_BASE_URI__;?>js/admin.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo _MODULE_DIR_.LOF_MODULE_NAME_HELPDESK;?>/assets/admin/css/formtickets.css" />
<?php
$id_hd_ticket = Tools::getValue('id_hd_ticket');
if(!$id_hd_ticket)
	die('error');
$objTicket = new HdTicket($id_hd_ticket);
if(!HelpDeskHelper::checkPermission($id_employee, $objTicket)){
	die( $helpdesk->l("You do not have permission to edit here.") );
}
$task = Tools::getValue('task','view');
if(Tools::isSubmit('submitAddNotice') || Tools::isSubmit('submitEditNotice')){
	$text = array();
	foreach($helpdesk->languages as $l){
		if(!Validate::isString(Tools::getValue('text_'.$l['id_lang'])))
			$errors[] = $helpdesk->l('field Text').' <b>'.$l['name'].'</b> '.$helpdesk->l('is not validate.');
		$text[$l['id_lang']] = Tools::getValue('text_'.$l['id_lang']);
	}
	if(!Tools::getValue('text_'.$helpdesk->defaultFormLanguage))
		$errors[] = $helpdesk->l('text is empty for default language.');
	if(Tools::isSubmit('submitAddNotice')){	
		$obj = new HdTicketNotice();
		$obj->id_hd_ticket = $id_hd_ticket;
		$obj->id_employee = $id_employee;
		$obj->text = $text;
		if(!sizeOf($errors)){
			if(!$obj->add()){
				$errors[] = $helpdesk->l('add notice is error.');
			}
		}else{
			$errors[] = $helpdesk->l('add notice is error.');
		}
	}else{
		$id_hd_ticket_notes = Tools::getValue('id_hd_ticket_notes');
		if(Validate::isLoadedObject($obj = new HdTicketNotice($id_hd_ticket_notes))){
			$obj->text = $text;
			if(!sizeOf($errors)){
				if(!$obj->update()){
					$errors[] = $helpdesk->l('add notice is error.');
				}else{
					Tools::redirect(_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/viewNotices.php?id_hd_ticket='.$id_hd_ticket.'&lofToken='.$lofToken);
				}
			}else{
				$errors[] = $helpdesk->l('add notice is error.');
			}
		}else{
			$errors[] = $helpdesk->l('object can not create.');
		}
	}
}
if($task == 'view'){
	$expandform = false;
	$notices = HdTicketNotice::getNotices($cookie->id_lang, $id_hd_ticket);
	if($notices)
		foreach($notices as &$n){
			$n['objEmp'] = new Employee($n['id_employee']);
		}
	$obj = new HdTicketNotice();
	echo "<div id='content'>";
	require_once(dirname(__FILE__).'/tmpl/admin/listNotices.php' );
	echo "</div>";
}elseif($task == 'edit'){
	$id_hd_ticket_notes = Tools::getValue('id_hd_ticket_notes');
	if(Validate::isLoadedObject($obj = new HdTicketNotice($id_hd_ticket_notes))){
		echo '<div id="content">
			<div class="lofformmessage">';
		require_once(dirname(__FILE__).'/tmpl/admin/noticefrom.php' );
		echo '</div>
			</div>';
	}else{
		die('errors');
	}
} ?>