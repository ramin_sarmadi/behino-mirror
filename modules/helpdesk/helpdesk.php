<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}   
require_once(dirname(__FILE__).'/defines.helpdesk.php');
require_once( dirname(__FILE__).'/classes/HdParams.php' );
require_once( dirname(__FILE__).'/classes/HdTicket.php' );
require_once( dirname(__FILE__).'/classes/HdEmailTemplate.php' );
require_once( dirname(__FILE__).'/libs/helper.php' );
require_once( dirname(__FILE__).'/controllers/admin/AdminHelpDeskConfigurationController.php' );
/**
 * helpdesk Class
 */	
class helpdesk extends Module
{
	/**
	 * @var array $_postErrors;
	 *
	 * @access private;
	 */
	private $_postErrors = array();
	public $languages;
	public $defaultFormLanguage;
    /**
    * Constructor 
    */
	function __construct() {
		$this->name = 'helpdesk';
        $this->tab  = 'administration';
        $this->version = '1.2';
		$this->module_key = "ac8936fdbc50d76aa63b56138a06a570";
        $this->author = 'www.landofcoder.com';
        
        parent::__construct();
        $this->confirmUninstall = $this->l('Are you sure you want me to keep away from displaying content?');
        $this->displayName 		= $this->l('HelpDesk');
        $this->description 		= $this->l('Adss HelpDesk functionality to the system'); 
		
		HelpDeskHelper::createKey($this->name, true);
		$this->Languages();
	}
    /**
    * process installing 
    */
	function install(){		
		if (!parent::install())
			return false;
		if(!$this->_installAdminRights() || !$this->registerHook('header') || !$this->registerHook('customerAccount') || !$this->registerHook('myAccountBlock') || !$this->registerHook('displayAdminCustomers') || !$this->_installTradDone())
			return false;
		$this->_installConfig();
		HelpDeskHelper::createKey($this->name,false);
		return true;
	}
	/**
	* process uninstall module
	*/
	public function uninstall() {
		if (!parent::uninstall() || !$this->_unInstallAdminRights())
			return false;
		$this->_uninstallTradDone();
		HelpDeskHelper::deleteKey($this->name);
		return true;
	}
	/**
	* get default language and languages
	*/
	public function Languages(){
		//global $cookie;
		$cookie = $this->context->cookie;
		$allowEmployeeFormLang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		if ($allowEmployeeFormLang && !$cookie->employee_form_lang)
			$cookie->employee_form_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$useLangFromCookie = false;
		$this->languages = Language::getLanguages(false);
		if ($allowEmployeeFormLang)
			foreach ($this->languages AS $lang)
				if ($cookie->employee_form_lang == $lang['id_lang'])
					$useLangFromCookie = true;
		if (!$useLangFromCookie)
			$this->defaultFormLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		else
			$this->defaultFormLanguage = (int)($cookie->employee_form_lang);
	}
	/**
	* install Tab
	*/
	private function _installAdminRights() {
		$return = 1;
		$languages = Language::getLanguages(false);
		foreach ($languages as $key=>$language) {
			$arrName[$language['id_lang']] = $this->l('HelpDesk');
		}
		
		$this->_installModuleTab('AdminHelpDeskCatalog', $arrName, 0);	
		$id_tab = Tab::getIdFromClassName("AdminHelpDeskCatalog");
		if(!$id_tab)
			return false;
		$subTabs 	 = array("AdminHelpDeskDashboard","AdminHelpDesk", "AdminHelpDeskDepartField", "AdminHelpDeskStaff", "AdminHelpDeskPriority", "AdminHelpDeskStatus", "AdminHelpDeskQuickResponse", "AdminHelpDeskEmailTemplate", "AdminHelpDeskConfiguration" );
		$subTabsName = array( $this->l("Dashboard"), $this->l("Tickets"), $this->l("Departments"), $this->l("Manage Staff"),$this->l("Manage Priority"), $this->l("Manage Status"), $this->l("Quick response"), $this->l("Email template"), $this->l("Configuration") );
		foreach($subTabs as $key=>$subTab){
			$languages = Language::getLanguages(false);
			foreach ($languages as $language) {
				$arrName[$language['id_lang']] = $subTabsName[$key];
			}
			$return &= $this->_installModuleTab($subTab, $arrName, $id_tab);	
		}
		return $return;
	}
	/**
	* unInstall Tab
	*/
	private function _unInstallAdminRights(){
		$return = 1;
		$return &= $this->_uninstallModuleTab('AdminHelpDeskCatalog');
		$subTabs = array("AdminHelpDeskDashboard", "AdminHelpDesk", "AdminHelpDeskDepartField", "AdminHelpDeskStaff", "AdminHelpDeskPriority", "AdminHelpDeskStatus", "AdminHelpDeskQuickResponse", "AdminHelpDeskEmailTemplate", "AdminHelpDeskConfiguration" );
		foreach($subTabs as $subtab){
			$return &= $this->_uninstallModuleTab( $subtab );
		}
		return $return;
	}
	
	private function _installModuleTab($tabClass, $tabName, $idTabParent){
		@copy(_PS_MODULE_DIR_.$this->name.'/logo.gif', _PS_IMG_DIR_.'t/'.$tabClass.'.gif');	
		$tab = new Tab();
		$tab->name = $tabName;
		$tab->class_name = $tabClass;
		$tab->module = $this->name;
		$tab->id_parent = $idTabParent;
		if(!$tab->save())
			return false;
		return true;
	}
	
	private function _uninstallModuleTab($tabClass){
		$idTab = Tab::getIdFromClassName($tabClass);
		if($idTab != 0){
			$tab = new Tab($idTab);
			return $tab->delete();
		}
		return false;
	}
	/**
	* install sql
	*/
	private function _installTradDone() {
		require_once( dirname(__FILE__)."/install/sql.tables.php" );
	 	$error=true;
		if( isset($query) && !empty($query) ){
			if(  !($data=Db::getInstance()->ExecuteS( "SHOW TABLES LIKE '"._DB_PREFIX_."hd_ticket'" )) ){
				$query = str_replace( "_DB_PREFIX_", _DB_PREFIX_, $query );
				$query = str_replace( "_MYSQL_ENGINE_", _MYSQL_ENGINE_, $query );
				$db_data_settings = preg_split("/;\s*[\r\n]+/",$query);
				foreach ($db_data_settings as $query){
					$query = trim($query);
					if (!empty($query))	{
						if (!Db::getInstance()->Execute($query)){
							 $error = false;
						}
					}
				}
			}
		} else { $error = false; }
		return $error;
	}
	/**
	* DROP Table
	*/
	private function _uninstallTradDone() {
		$query = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'hd_configuration`,`'._DB_PREFIX_.'hd_custom_fields`,`'._DB_PREFIX_.'hd_custom_fields_lang`,`'._DB_PREFIX_.'hd_custom_fields_values`
			,`'._DB_PREFIX_.'hd_department`,`'._DB_PREFIX_.'hd_department_lang`,`'._DB_PREFIX_.'hd_email`,`'._DB_PREFIX_.'hd_email_lang`,`'._DB_PREFIX_.'hd_kb_category`
			,`'._DB_PREFIX_.'hd_kb_rule`,`'._DB_PREFIX_.'hd_priority`,`'._DB_PREFIX_.'hd_priority_lang`,`'._DB_PREFIX_.'hd_searches`,`'._DB_PREFIX_.'hd_staff`
			,`'._DB_PREFIX_.'hd_staff_hd_department`,`'._DB_PREFIX_.'hd_status`,`'._DB_PREFIX_.'hd_status_lang`,`'._DB_PREFIX_.'hd_ticket`,`'._DB_PREFIX_.'hd_ticket_files`
			,`'._DB_PREFIX_.'hd_ticket_history`,`'._DB_PREFIX_.'hd_ticket_lang`,`'._DB_PREFIX_.'hd_ticket_messages`,`'._DB_PREFIX_.'hd_ticket_notes`
			,`'._DB_PREFIX_.'hd_ticket_notes_lang`,`'._DB_PREFIX_.'hd_quickresponse`;';
		return Db::getInstance()->Execute($query);
	}
	/** 
	* install default config
	*/
	private function _installConfig(){
		$objParams = HdParams::getInstance();
		$configs = new AdminHelpDeskConfigurationController();
		foreach($configs->default as $param => $value){
			if( (!$objParams->isParamsExist($param)) ){
				$result = Db::getInstance()->Execute(
					"INSERT INTO "._DB_PREFIX_.'hd_configuration(`name`, `value`) VALUES("'.($param).'","'.$value.'")'
				);
			}
			$objParams->set($param, $value);
		}
		return true;
	}
	
	public function hookHeader($params) {
		//global $cookie;
		$cookie = $this->context->cookie;
		$params = new HdParams(LOF_MODULE_NAME_HELPDESK);
		$this->autoClose();
		$this->context->controller->addCSS(_MODULE_DIR_.$this->name.'/assets/admin/css/jquery-ui.css', 'all');
		$this->context->controller->addCSS(_MODULE_DIR_.$this->name.'/assets/style.css', 'all');
		if($params->get('allow_rich_editor')){
			$iso = Language::getIsoById((int)($cookie->id_lang));
			$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
			$ad = dirname($_SERVER["PHP_SELF"]);
			return '
				<script type="text/javascript">	
					var iso = \''.$isoTinyMCE.'\' ;
					var pathCSS = \''._THEME_CSS_DIR_.'\' ;
					var ad = \''.$ad.'\' ;
				</script>
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
				<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
				<script type="text/javascript" src="'._MODULE_DIR_.'helpdesk/assets/tinymce.inc.js"></script>';
		}
	}
	
	public function hookdisplayAdminCustomers($params){
		$cookie = $this->context->cookie;
		$id_customer = $params['id_customer'];
		$tickets = HdTicket::getTickets($cookie->id_lang, false, $id_customer, false, false, false, false, 'id_hd_ticket', 'ASC',false, 1);
		$token = Tools::getAdminToken('AdminHelpDesk'.(int)(Tab::getIdFromClassName('AdminHelpDesk')).(int)($cookie->id_employee));
		$this->context->smarty->assign( array(
			'tickets'  => $tickets,
			'view_ticket_url' => 'index.php?controller=AdminHelpDesk&viewhd_ticket&token='.$token
		));
		return $this->display(__FILE__, 'tmpl/frontend/admin-customer.tpl');
	}
	
	public function hookCustomerAccount($params){
		return $this->hookMyAccountBlock($params);
	}
	
	public function hookMyAccountBlock($params){
		$cookie = $this->context->cookie;
		$alltickets = HdTicket::getTickets($cookie->id_lang, false, $cookie->id_customer, false, false, false, false, 'id_hd_ticket', 'ASC',false, 1); 
		$this->context->smarty->assign( array(
			'nb'  => count($alltickets)
		));
		return $this->display(__FILE__, 'tmpl/frontend/my-account.tpl');
	}
	
	public function displayFlags($languages, $default_language, $ids, $id, $return = false, $use_vars_instead_of_ids = false) {
		if (sizeof($languages) == 1)
			return false;
		$output = '
		<div class="displayed_flag">
			<img src="../../img/l/'.$default_language.'.jpg" class="pointer" id="language_current_'.$id.'" onclick="toggleLanguageFlags(this);" alt="" />
		</div>
		<div id="languages_'.$id.'" class="language_flags">
			'.$this->l('Choose language:').'<br /><br />';
		foreach ($languages as $language)
			if($use_vars_instead_of_ids)
				$output .= '<img src="../../img/l/'.(int)($language['id_lang']).'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="changeLanguageLof(\''.$id.'\', '.$ids.', '.$language['id_lang'].', \''.$language['iso_code'].'\');" /> ';
			else
				$output .= '<img src="../../img/l/'.(int)($language['id_lang']).'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="changeLanguageLof(\''.$id.'\', \''.$ids.'\', '.$language['id_lang'].', \''.$language['iso_code'].'\');" /> ';
		$output .= '</div>';

		if ($return)
			return $output;
		echo $output;
	}
	
	public function autoClose(){
		
		global $lof;
		$cookie = $this->context->cookie;
		$params = new HdParams(LOF_MODULE_NAME_HELPDESK);
		if(!$params->get('autoclose_enabled'))
			return ;
		$autoclose_cron_lastcheck = $params->get('autoclose_cron_lastcheck',0); 
		if(strtotime('now') < ($autoclose_cron_lastcheck + $params->get('autoclose_cron_interval',10) * 60))
			return;
		
		$tickets = HdTicket::getTicketsByNotStatus($cookie->id_lang, LOF_STATUS_CLOSE);
		$interval = $params->get('autoclose_email_interval') * 86400;
		if ($interval < 86400)
			$interval = 86400;
		$time = strtotime('now') + $interval;
		if(!$tickets)
			return ;
		
		foreach($tickets as $t){
			$last_reply_interval = strtotime($t['last_reply']) + $interval;
			if($last_reply_interval <= strtotime('now')){
				Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'hd_ticket SET id_hd_status='.LOF_STATUS_CLOSE.' WHERE id_hd_ticket='.$t['id_hd_ticket']);
				/* SEND EMAIL */
				$objCus = new Customer($t['id_customer']);
				$mailTMPL = new HdEmailTemplate(LOF_EMAILS_SEND_NOTIFICATION_TO_CUSTOMER_WHEN_AUTO_CLOSE, $cookie->id_lang);
				$subject = $mailTMPL->subject;
				if(in_array(LOF_EMAILS_SEND_NOTIFICATION_TO_CUSTOMER_WHEN_AUTO_CLOSE,$lof['skip_edit_subject'])){
					$subject = $t['subject'];
				}
				HelpDeskHelper::sendMail( array($objCus->email), array($objCus->firstname.' '.$objCus->lastname), $subject, $mailTMPL->message, false, 'other');
				/* END SEND MAIL */
			}
		}
		
		if(!$params->isParamsExist('autoclose_cron_lastcheck')){
			Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'hd_configuration(name,value) VALUES(\'autoclose_cron_lastcheck\','.strtotime('now').')');
		}else{
			Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'hd_configuration SET value=\''.strtotime('now').'\' WHERE name=\'autoclose_cron_lastcheck\'');
		}
		
	}
} 