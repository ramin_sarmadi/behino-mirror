<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
define('LOF_UPLOAD_FOLDER', _PS_UPLOAD_DIR_.'helpdesk/');
define('LOF_MODULE_NAME_HELPDESK', 'helpdesk');
define('LOF_STATUS_OPEN', 1);
define('LOF_STATUS_ON_HOLD', 2);
define('LOF_STATUS_CLOSE', 3);
define('LOF_PRIORITY_NORMAL', 1);
define('LOF_PRIORITY_LOW', 2);
define('LOF_PRIORITY_HIGH', 3);
define('LOF_ID_PROFILE_ADMIN', 1);
define('LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_TICKET', 1);
define('LOF_EMAILS_SEND_TO_DEPARTMENT_NOTIFY_SECTION', 2);
define('LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_REPLY', 3);
define('LOF_EMAILS_SEND_TO_STAFF_ADD_NEW_REPLY', 4);
define('LOF_EMAILS_SEND_TO_STAFF_WHEN_ASSIGNED', 5);
define('LOF_EMAILS_SEND_WHEN_NEW_USER', 6);
define('LOF_EMAILS_SEND_NOTIFICATION_TO_CUSTOMER_WHEN_AUTO_CLOSE', 7);
define('LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_UNASSIGNED', 8);
define('LOF_EMAILS_SEND_NOTIFICATION_WHEN_TICKET_CONTAIN_KEYWORDS', 9);
define('LOF_EMAILS_SEND_NOTIFICATION_WHEN_NUMBER_REPLY_ASSIGNED', 10);
define('LOF_EMAILS_SEND_TICKET_REJECT', 11);/**/
define('CHANGE_MESSAGE_DELETE', 'delete');/**/
define('CHANGE_MESSAGE_EDIT', 'edit');/**/

global $lof;
$lof['skip_edit_subject'] = array(
	LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_TICKET, 
	LOF_EMAILS_SEND_TO_DEPARTMENT_NOTIFY_SECTION, 
	LOF_EMAILS_SEND_TO_CUSTOMER_ADD_NEW_REPLY,
	LOF_EMAILS_SEND_TO_STAFF_ADD_NEW_REPLY,
	LOF_EMAILS_SEND_TO_STAFF_WHEN_ASSIGNED
);
$lof['un_remove_status'] = array(
	LOF_STATUS_OPEN, 
	LOF_STATUS_ON_HOLD, 
	LOF_STATUS_CLOSE
);
$lof['un_remove_priority'] = array(
	LOF_PRIORITY_HIGH, 
	LOF_PRIORITY_LOW, 
	LOF_PRIORITY_NORMAL
);