<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdEmailTemplate extends ObjectModel
{
 	/** @var string Name */
 	public $id;
	
	public		$id_hd_email;
	public		$active=1;
	/* lang */
	public	 	$type;
	public	 	$subject;
	public	 	$message;
	
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'hd_email',
		'primary' => 'id_hd_email',
		'multilang' => true,
		'fields' => array(
			'active' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			
			// Lang fields
			'type' => 		array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'size' => 255),
			'subject' => 	array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'size' => 255, 'required' => true),
			'message' => 	array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'size' => 3999999999999, 'required' => true),
		),
	);
	
	/**
	* function get id item unremove
	* return array() id_hd_status
	*/
	public static function getUnRemove(){
		return array(1,2,3,4,5,6,7,8,9,10,11);
	}
	
	public function add($autodate = true, $nullValues = false){ 
		return parent::add($autodate, false);
	}
	
	public function update($nullValues = false){
		return parent::update($nullValues);
	}
	
	public function delete() {
	 	return parent::delete();
	}
	
	public static function getEmails($id_lang = null){
		if (!$id_lang)
			$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'hd_email` e
		LEFT JOIN `'._DB_PREFIX_.'hd_email_lang` el ON ( e.`id_hd_email` = el.`id_hd_email` )
		WHERE el.`id_lang` = '.(int)($id_lang);
		return Db::getInstance()->ExecuteS($sql);
	}
}


