<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdTicketMessage extends ObjectModel
{
 	/** @var string Name */
 	public $id;
	
	public		$id_hd_ticket_message;
	public		$id_hd_ticket;
	public		$id_customer;
	public  	$date_add;
	public 		$id_employee;
	public 		$position;
	public 		$active=1;
	public	 	$message;
	
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'hd_ticket_messages',
		'primary' => 'id_hd_ticket_message',
		'fields' => array(
			'id_hd_ticket' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'id_customer' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'date_add' => 		array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'id_employee' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'position' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'active' => 		array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'message' => 		array('type' => self::TYPE_HTML, 'validate' => 'isString', 'required' => true)
			
		),
	);
	
	
	public function add($autodate = true, $nullValues = false){ 
		$this->position = HdTicketMessage::getLastPosition((int)$this->id_hd_ticket);
		if (parent::add($autodate, false)){
			return HdTicketMessage::updateTicket((int)$this->id_hd_ticket);
		}
	}
	
	public function update($nullValues = false){
		if (parent::update($nullValues))
			return $this->cleanPositions($this->id_hd_ticket);
		return false;
	}
	
	public function delete() {
		$id_hd_message = $this->id;
		$id_hd_ticket = $this->id_hd_ticket;
	 	if (parent::delete()){
			Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'hd_ticket` SET replies = replies-1 WHERE id_hd_ticket='.(int)($id_hd_ticket).' LIMIT 1 ');
			$files = HdTicketFile::getFiles(false,$id_hd_message);
			foreach($files as $f){
				$objFile = new HdTicketFile($f['id_hd_ticket_files']);
				$objFile->delete();
			}
			return $this->cleanPositions($this->id_hd_ticket);
		}
		return false;
	}
	
	public function updatePosition($way, $position){
		if (!$res = Db::getInstance()->ExecuteS('
			SELECT cp.`id_hd_ticket_message`, cp.`position`, cp.`id_hd_ticket` 
			FROM `'._DB_PREFIX_.'hd_ticket_messages` cp
			WHERE cp.`id_hd_ticket` = '.(int)$this->id_hd_ticket.' 
			ORDER BY cp.`position` ASC'
		))
			return false;
		
		foreach ($res AS $messages)
			if ((int)($messages['id_hd_ticket_message']) == (int)($this->id))
				$movedticket_messages = $messages;
		
		if (!isset($movedticket_messages) || !isset($position))
			return false;
		
		// < and > statements rather than BETWEEN operator
		// since BETWEEN is treated differently according to databases
		return (Db::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'hd_ticket_messages`
			SET `position`= `position` '.($way ? '- 1' : '+ 1').'
			WHERE `position` 
			'.($way 
				? '> '.(int)($movedticket_messages['position']).' AND `position` <= '.(int)($position)
				: '< '.(int)($movedticket_messages['position']).' AND `position` >= '.(int)($position)).'
			AND `id_hd_ticket`='.(int)($movedticket_messages['id_hd_ticket']))
		AND Db::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'hd_ticket_messages`
			SET `position` = '.(int)($position).'
			WHERE `id_hd_ticket_message` = '.(int)($movedticket_messages['id_hd_ticket_message']).'
			AND `id_hd_ticket`='.(int)($movedticket_messages['id_hd_ticket'])));
	}
	
	public static function cleanPositions($id_hd_ticket) {
		$result = Db::getInstance()->ExecuteS('
		SELECT `id_hd_ticket_message`
		FROM `'._DB_PREFIX_.'hd_ticket_messages`
		WHERE `id_hd_ticket` = '.(int)($id_hd_ticket).'
		ORDER BY `position`');
		$sizeof = sizeof($result);
		for ($i = 0; $i < $sizeof; ++$i){
				$sql = '
				UPDATE `'._DB_PREFIX_.'hd_ticket_messages`
				SET `position` = '.(int)($i).'
				WHERE `id_hd_ticket` = '.(int)($id_hd_ticket).'
				AND `id_hd_ticket_message` = '.(int)($result[$i]['id_hd_ticket_message']);
				Db::getInstance()->Execute($sql);
			}
		return true;
	}
	
	public static function getLastPosition($id_hd_ticket) {
		return (Db::getInstance()->getValue('SELECT MAX(position)+1 FROM `'._DB_PREFIX_.'hd_ticket_messages` WHERE `id_hd_ticket` = '.(int)($id_hd_ticket)));
	}
	
	public static function updateTicket($id_hd_ticket) {
		if(!$id_hd_ticket)
			return false;
		if(!Validate::isLoadedObject($hdticket = new HdTicket($id_hd_ticket)))
			return false;
		$hdticket->last_reply = date('Y-m-d H:i:s');
		$hdticket->replies = count(HdTicketMessage::getTicketMessages($id_hd_ticket, false));
		return $hdticket->update();
	}
	
	public function deleteSelection($ids){
		$return = 1;
		foreach ($ids AS $id){
			$obj = new HdTicketMessage((int)($id));
			$return &= $obj->delete();
		}
		return $return;
	}
	
	public static function getTicketMessages($id_hd_ticket = false, $active = true, $id_lang = false, $n = false, $p = 1) {
		if(!$id_lang)
			$id_lang = Configuration::get('PS_LANG_DEFAULT');
		$sql = '
		SELECT tm.*, t.*, tl.`subject`, tm.`date_add` 
		FROM `'._DB_PREFIX_.'hd_ticket_messages` tm 
		LEFT JOIN `'._DB_PREFIX_.'hd_ticket` t ON(t.`id_hd_ticket` = tm.`id_hd_ticket`)
		LEFT JOIN `'._DB_PREFIX_.'hd_ticket_lang` tl ON(tl.`id_hd_ticket` = t.`id_hd_ticket` AND tl.`id_lang` = '.(int)($id_lang).')
		WHERE  1 '.($active ? ' AND tm.`active` = 1' : '').($id_hd_ticket ? ' AND tm.`id_hd_ticket` = '.(int)($id_hd_ticket) : '')
		.' GROUP BY id_hd_ticket_message' 
		.' ORDER BY id_hd_ticket_message DESC'
		.($n ? ' LIMIT '.(int)(($p - 1)*$n).','.(int)$n : '');
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
	
	public static function getFiles($id_hd_ticket_message){
		if(!$id_hd_ticket_message)
			return array();
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'hd_ticket_files` WHERE id_hd_ticket_message = '.(int)($id_hd_ticket_message);
		return Db::getInstance()->ExecuteS($sql);
	}
}


