<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdConfiguration extends ObjectModel
{
 	/** @var string Name */
	public 		$name;
	
 
	
	/** @var boolean Allow customer to view and download invoice when order is at this state */
	public 		$active = true;
	
	/** @var string Display state in the specified color */
	public		$color;
	
	public		$ordering;
 


	
	protected 	$table = 'hd_configuration';
	protected 	$identifier = 'id_hd_configuration';
	protected 	$fieldsRequiredLang = array('name');
 	protected 	$fieldsSizeLang = array('name' => 32);
 	protected 	$fieldsValidateLang = array('name' => 'isGenericName', 'description' => 'isCleanHtml');
	
	protected 	$fieldsRequired = array();
	
	protected	$webserviceParameters = array(
		'fields' => array(
			'active' => array()
		)
	);
	
	public function getFields()
	{
		parent::validateFields();
		
		$fields['color'] = pSQL($this->color);
		$fields['active'] = (int)$this->active;
		return $fields;
	}
	
	
	
	/**
	  * Check then return multilingual fields for database interaction
	  *
	  * @return array Multilingual fields
	  */
	public function getTranslationsFieldsChild()
	{
		parent::validateFieldsLang();
		return parent::getTranslationsFields(array('name'));
	}
	
	public function isRemovable()
	{
	 	return !($this->unremovable);
	}
}


