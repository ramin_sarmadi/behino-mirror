<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdTicketNotice extends ObjectModel
{
 	/** @var string Name */
 	public $id;
	
	/** @var string Display state in the specified color */
	public		$id_hd_ticket_notes;
	public		$id_hd_ticket;
	public		$id_employee;
	public		$date_add;
	/* lang */
	public		$text;
	
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'hd_ticket_notes',
		'primary' => 'id_hd_ticket_notes',
		'multilang' => true,
		'fields' => array(
			'id_hd_ticket' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'id_employee' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'date_add' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			
			// Lang fields
			'text' => 		array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'size' => 3999999999999999999),
		),
	);
	
	public function add($autodate = true, $nullValues = false) {
		
		return parent::add($autodate, $nullValues);
	}
	
	public function update($nullValues = false){
		return parent::update($nullValues);
	}
	
	public function delete() {
	 	return parent::delete();
	}
	
	public function deleteSelection($ids){
		$return = 1;
		foreach ($ids AS $id){
			$obj = new HdTicketNotice((int)($id));
			$return &= $obj->delete();
		}
		return $return;
	}
	
	public static function getNotices($id_lang = false, $id_hd_ticket = false, $id_employee = false, $orderBy = 'id_hd_ticket_notes', $orderWay = 'DESC', $nb = false, $page = 1){
		if (!$id_lang)
			$id_lang = Configuration::get('PS_LANG_DEFAULT');
		
		$sql = 'SELECT t.*,tl.`text`
				FROM `'._DB_PREFIX_.'hd_ticket_notes` t 
				LEFT JOIN `'._DB_PREFIX_.'hd_ticket_notes_lang` tl ON(t.`id_hd_ticket_notes` = tl.`id_hd_ticket_notes` AND tl.`id_lang` = '.(int)($id_lang).')
				
				WHERE 1 '.($id_hd_ticket ? ' AND t.`id_hd_ticket` = '.(int)($id_hd_ticket) : '')
				.($id_employee ? ' AND t.`id_employee` = '.(int)($id_employee) : '')
				.($orderBy ? ' ORDER BY '.$orderBy.' '.$orderWay : '')
				.($nb ? ' LIMIT '.(((int)($page) - 1) * (int)($nb)).','.(int)($nb) : '');
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
	
	
}

