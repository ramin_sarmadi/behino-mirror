<?php

class HdTicketFile extends ObjectModel {
 	
	public $id;
	
	public		$id_hd_ticket_files;
	public		$id_hd_ticket;
	public		$id_hd_ticket_message;
	public  	$filename;
	public  	$mime;
	public 		$downloads;
	
	protected 	$table = 'hd_ticket_files';
	protected 	$identifier = 'id_hd_ticket_files';
	
	protected $fieldsRequired = array('id_hd_ticket','id_hd_ticket_message');
	protected $fieldsValidates = array( 'id_hd_ticket_files' => 'isUnsignedId','id_hd_ticket'=>'isUnsignedId','id_hd_ticket_message'=>'isUnsignedId' );
	
	
	public function getFields() {
		parent::validateFields();
		
		if (isset($this->id))
			$fields['id_hd_ticket_files'] = (int)($this->id);
		
		$fields['id_hd_ticket'] = intval($this->id_hd_ticket);
		$fields['id_hd_ticket_message'] = intval($this->id_hd_ticket_message);
		$fields['filename'] 	= pSQL($this->filename);
		$fields['mime'] 	= pSQL($this->mime);
		$fields['downloads'] = intval($this->downloads);
		
		return $fields;
	}
	
	public function add($autodate = true, $nullValues = false){ 
		if(parent::add($autodate, false)){
			$id_hd_ticket = $this->id_hd_ticket;
			if(Validate::isLoadedObject($obj = new HdTicket($this->id_hd_ticket))){
				if(self::getFiles($this->id_hd_ticket))
					$obj->has_files = 1;
				else
					$obj->has_files = 0;
				return $obj->update();
			}
		}
		return false;
	}
	
	public function update($nullValues = false){
		return parent::update($nullValues);
	}
	
	public function delete() {
		$id_hd_ticket_files = $this->id;
		$id_hd_ticket_message = $this->id_hd_ticket_message;
		$id_hd_ticket = $this->id_hd_ticket;
	 	if( parent::delete()){
			$hash = md5($id_hd_ticket_files.' '.$id_hd_ticket_message);
			@unlink(LOF_UPLOAD_FOLDER.$hash);
			
			if(Validate::isLoadedObject($obj = new HdTicket($id_hd_ticket))){
				if(self::getFiles($id_hd_ticket))
					$obj->has_files = 1;
				else
					$obj->has_files = 0;
				return $obj->update();
			}
			return true;
		}
		return false;
	}
	
	
	public static function getFiles($id_hd_ticket=false, $id_hd_ticket_message=false){
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'hd_ticket_files` WHERE 1 '.($id_hd_ticket_message ? ' AND id_hd_ticket_message = '.(int)($id_hd_ticket_message) : '')
		.($id_hd_ticket ? ' AND id_hd_ticket = '.(int)($id_hd_ticket) : '');
		return Db::getInstance()->ExecuteS($sql);
	}
	
}






