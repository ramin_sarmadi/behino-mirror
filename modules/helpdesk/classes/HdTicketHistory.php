<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdTicketHistory extends ObjectModel
{
 	/** @var string Name */
 	public $id;
	
	/** @var string Display state in the specified color */
	public		$id_hd_ticket_history;
	public		$id_hd_ticket;
	public		$id_customer;
	public		$id_employee;
	public		$ip;
	public		$date_add;
	public		$change_status;
	public		$change_department;
	public		$change_customer;
	public		$change_staff;
	public		$change_priority;
	public		$change_message; /* edit|delete */
	public		$id_hd_ticket_message;
	public		$message;
	
	
	protected 	$table = 'hd_ticket_history';
	protected 	$identifier = 'id_hd_ticket_history';
	
	protected	$fieldsRequired = array('id_hd_ticket');
	protected	$fieldsValidates = array( 'id_hd_ticket'=>'isUnsignedId','id_hd_ticket_history'=>'isUnsignedId','id_employee'=>'isUnsignedId','id_customer'=>'isUnsignedId'
	,'date_add'=>'isDate','ip'=>'isIp','change_status'=>'isUnsignedId','change_department'=>'isUnsignedId','change_customer'=>'isUnsignedId','change_staff'=>'isUnsignedId'
	,'change_priority'=>'isUnsignedId','change_message'=>'isString','id_hd_ticket_message'=>'isUnsignedId','message'=>'isString');
	
	
	public function getFields() {
		parent::validateFields();
		if (isset($this->id))
			$fields['id_hd_ticket_history'] = (int)($this->id);
		$fields['id_hd_ticket'] = intval($this->id_hd_ticket);
		$fields['id_customer'] = intval($this->id_customer);
		$fields['id_employee'] = intval($this->id_employee);
		$fields['date_add'] = pSQL($this->date_add);
		$fields['ip'] = pSQL($this->ip);
		$fields['change_status'] = intval($this->change_status);
		$fields['change_department'] = intval($this->change_department);
		$fields['change_customer'] = intval($this->change_customer);
		$fields['change_staff'] = intval($this->change_staff);
		$fields['change_priority'] = intval($this->change_priority);
		$fields['change_message'] = pSQL($this->change_message);
		$fields['id_hd_ticket_message'] = intval($this->id_hd_ticket_message);
		$fields['message'] = pSQL($this->message);
		
		return $fields;
	}
	
	public function add($autodate = true, $nullValues = false) {
		return parent::add($autodate, $nullValues);
	}
	
	public function update($nullValues = false){
		return parent::update($nullValues);
	}
	
	public function delete() {
	 	return parent::delete();
	}
	
	public function deleteSelection($ids){
		$return = 1;
		foreach ($ids AS $id){
			$obj = new HdTicketHistory((int)($id));
			$return &= $obj->delete();
		}
		return $return;
	}
	
	public static function getHistories($id_hd_ticket = false, $id_lang = false, $id_customer = false,  $id_employee = false, $orderBy = 'id_hd_ticket_history', $orderWay = 'DESC', $nb = false, $page = 1){
		if (!$id_lang)
			$id_lang = Configuration::get('PS_LANG_DEFAULT');
		
		$sql = 'SELECT h.*,tl.`subject`,sl.`name` AS status_name,sl.`id_hd_status`,dl.`name` AS department_name,dl.`id_hd_department`,pl.`name` AS priority_name,pl.`id_hd_priority`,CONCAT(c.`firstname`,\' \',c.`lastname`) AS customer_name
				,c.`email` AS customer_email, CONCAT(e.`firstname`,\' \',e.`lastname`) AS employee_name,e.`email` AS employee_email
				FROM `'._DB_PREFIX_.'hd_ticket_history` h 
				LEFT JOIN `'._DB_PREFIX_.'hd_ticket` t ON (h.`id_hd_ticket` = t.`id_hd_ticket`)
				LEFT JOIN `'._DB_PREFIX_.'hd_ticket_lang` tl ON (t.`id_hd_ticket` = tl.`id_hd_ticket` AND tl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'hd_status` s ON (h.`change_status` = s.`id_hd_status`)
				LEFT JOIN `'._DB_PREFIX_.'hd_status_lang` sl ON (sl.`id_hd_status` = s.`id_hd_status` AND sl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'hd_department` d ON (h.`change_department` = d.`id_hd_department`)
				LEFT JOIN `'._DB_PREFIX_.'hd_department_lang` dl ON (dl.`id_hd_department` = d.`id_hd_department` AND dl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'customer` c ON (h.`change_customer` = c.`id_customer`)
				LEFT JOIN `'._DB_PREFIX_.'hd_staff` sf ON (h.`change_staff` = sf.`id_hd_staff`)
				LEFT JOIN `'._DB_PREFIX_.'employee` e ON (e.`id_employee` = sf.`id_employee`)
				LEFT JOIN `'._DB_PREFIX_.'hd_priority` p ON (h.`change_priority` = p.`id_hd_priority`)
				LEFT JOIN `'._DB_PREFIX_.'hd_priority_lang` pl ON (pl.`id_hd_priority` = p.`id_hd_priority` AND pl.`id_lang` = '.(int)($id_lang).')
				
				WHERE 1 '.($id_hd_ticket ? ' AND h.`id_hd_ticket` = '.(int)($id_hd_ticket) : '')
				.($id_customer ? ' AND h.`id_customer` = '.(int)($id_customer) : '')
				.($id_employee ? ' AND h.`id_employee` = '.(int)($id_employee) : '')
				.' GROUP BY h.`id_hd_ticket_history` '
				.($orderBy ? ' ORDER BY '.$orderBy.' '.$orderWay : '')
				.($nb ? ' LIMIT '.(((int)($page) - 1) * (int)($nb)).','.(int)($nb) : '');
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
	
}

