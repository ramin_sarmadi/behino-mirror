<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdStaff extends ObjectModel
{
 	/** @var string Name */
 	public $id;
 
	
	/** @var boolean Allow customer to view and download invoice when order is at this state */
	public 		$id_employee;
	
	/** @var string Display state in the specified color */
	public		$id_hd_department;
	
	public		$id_hd_priority;
 
	public		$signature;
 
	public	 	$departments = array();
	
	protected 	$table = 'hd_staff';
	protected 	$identifier = 'id_hd_staff';
	
	protected 	$fieldsRequired = array('id_employee', 'id_hd_department', 'id_hd_priority');
	protected	$fieldsValidates = array( 'id_employee' => 'isUnsignedId', 'signature' => 'isCleanHtml' );
	
	protected	$webserviceParameters = array(
		'objectsNodeName' => 'HdStaff',
		'fields' => array(
			'id_hd_department' => array('xlink_resource'=> 'hd_department')
		),
	);
	
	/**
	 * Build an address
	 *
	 * @param integer $id_address Existing address id in order to load object (optional)
	 */
	public	function __construct($id_address = NULL, $id_lang = NULL)
	{
		parent::__construct($id_address);

		/* Get and cache address country name */
		if ($this->id)
		{
			$result = Db::getInstance()->ExecuteS('
			SELECT * FROM `'._DB_PREFIX_.'hd_staff_hd_department` WHERE id_hd_staff='.(int)$this->id );
			
		 	$tmp = array();
			if( !empty($result) ){
				foreach( $result as $r ){
					$tmp[] = $r['id_hd_department'];
				}
			}
			$this->departments = $tmp;
		}
	}
	
	public function getFields()
	{
		parent::validateFields();
		
		$fields['id_hd_priority'] = intval($this->id_hd_priority);
		$fields['signature']	  = pSQL($this->signature);
		$fields['id_employee'] 	  = intval($this->id_employee);
		
		return $fields;
	}
	
	public function add($autodate = true, $nullValues = true)
	{
		if( !empty($this->id_hd_department) ){
			if (!parent::add($autodate, $nullValues))
			return false;

			 if ( Validate::isUnsignedId($this->id) ){ 
				return $this->addDepartment( $this->id_hd_department );
			 }	
			 return true;
		}
		return false;
	}
	public function delete()
	{
		if (!Db::getInstance()->Execute('DELETE FROM  `'._DB_PREFIX_.'hd_staff_hd_department` WHERE id_hd_staff='.(int) $this->id))
				$result = false;
		return parent::delete();		
	}
	
	public function update($nullValues = false)
	{
		if( !empty($this->id_hd_department)){
			if(!parent::update())
				return false;
				
			if (!Db::getInstance()->Execute('DELETE FROM  `'._DB_PREFIX_.'hd_staff_hd_department` WHERE id_hd_staff='.(int) $this->id))
				$result = false;
			
			if ( Validate::isUnsignedId($this->id) ){ 
				return $this->addDepartment( $this->id_hd_department );
			}
		}
		return false; 
	}
	public function addDepartment( $departments ){
		$result = true;
		foreach ($departments AS $department )
		{
			if (!Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'hd_staff_hd_department` ( `id_hd_staff` , `id_hd_department`, `id_employee` ) 
																			VALUES ('.(int)($this->id).', '.(int)($department).','.$this->id_employee.')'))
				$result = false;
		}
		return $result;
	}
	
 
	public function isRemovable()
	{
	 	return !($this->unremovable);
	}
	
	public static function getStaffs($id_hd_department = false){
		$sql = 'SELECT *
			FROM `'._DB_PREFIX_.'hd_staff` s 
			LEFT JOIN `'._DB_PREFIX_.'hd_staff_hd_department` sd ON(s.`id_hd_staff` = sd.`id_hd_staff`) 
			WHERE 1 '.($id_hd_department ? ' AND sd.`id_hd_department` = '.(int)($id_hd_department) : '');
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
	
	public static function getStaffByEmployee($id_employee, $type='id_hd_staff'){
		if(!$id_employee)
			return array();
		$sql = 'SELECT *
			FROM `'._DB_PREFIX_.'hd_staff` s 
			WHERE 1 AND s.`id_employee` = '.(int)($id_employee);
		$result = Db::getInstance()->ExecuteS($sql);
		if(!$result)
			return array();
		if($type != 'id_hd_staff'){
			return $result;
		}
		$return = array();
		foreach($result as $r){
			$return[] = $r['id_hd_staff'];
		}
		return $return;
	}
	
	public static function getDepartmentByStaff($id_hd_staff = false, $type='id_hd_department'){
		if(is_array($id_hd_staff) && $id_hd_staff){
			if(count($id_hd_staff) == 1){
				$where = ' AND sd.`id_hd_staff`='.(int)($id_hd_staff[0]);
			}else{
				$where = ' AND sd.`id_hd_staff` IN ('.implode(',',$id_hd_staff).')';
			}
		}
		$sql = 'SELECT d.*,dl.`name`
			FROM `'._DB_PREFIX_.'hd_department` d
			LEFT JOIN `'._DB_PREFIX_.'hd_department_lang` dl ON(dl.`id_hd_department` = d.`id_hd_department`)
			LEFT JOIN `'._DB_PREFIX_.'hd_staff_hd_department` sd ON(sd.`id_hd_department` = d.`id_hd_department`)
			WHERE 1 '.(isset($where) ? $where : ($id_hd_staff ? ' AND sd.`id_hd_staff` = '.(int)($id_hd_staff) : ''))
			.' GROUP BY d.`id_hd_department`';
		$result = Db::getInstance()->ExecuteS($sql);
		if(!$result)
			return array();
		if($type != 'id_hd_department'){
			return $result;
		}
		$return = array();
		foreach($result as $r){
			$return[] = $r['id_hd_department'];
		}
		return $return;
	}
	
	public static function getEmployeeByStaff($id_hd_staff = false){
		if(is_array($id_hd_staff) && $id_hd_staff){
			if(count($id_hd_staff) == 1){
				$where = ' AND hs.`id_hd_staff`='.(int)($id_hd_staff[0]);
			}else{
				$where = ' AND hs.`id_hd_staff` IN ('.implode(',',$id_hd_staff).')';
			}
		}
		$sql = 'SELECT e.`id_employee`, CONCAT(e.`firstname`,\' \', e.`lastname`) as name
			FROM `'._DB_PREFIX_.'employee` e 
			LEFT JOIN `'._DB_PREFIX_.'hd_staff` hs ON(hs.`id_employee` = e.`id_employee`)
			WHERE 1 '.(isset($where) ? $where : ($id_hd_staff ? ' AND hs.`id_hd_staff` = '.(int)($id_hd_staff) : ''))
			.' GROUP BY e.`id_employee`';
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
	
	public static function getEmployeeByDepartment($id_hd_department = false){
		if(is_array($id_hd_department) && $id_hd_department){
			if(count($id_hd_department) == 1){
				$where = ' AND shd.`id_hd_department`='.(int)($id_hd_department[0]);
			}else{
				$where = ' AND shd.`id_hd_department` IN ('.implode(',',$id_hd_department).')';
			}
		}
		$sql = 'SELECT e.`id_employee`, CONCAT(e.`firstname`,\' \', e.`lastname`) as name
			FROM `'._DB_PREFIX_.'employee` e 
			LEFT JOIN `'._DB_PREFIX_.'hd_staff` hs ON(hs.`id_employee` = e.`id_employee`)
			LEFT JOIN `'._DB_PREFIX_.'hd_staff_hd_department` shd ON(shd.`id_hd_staff` = hs.`id_hd_staff`)
			WHERE 1 '.(isset($where) ? $where : ($id_hd_staff ? ' AND hs.`id_hd_staff` = '.(int)($id_hd_staff) : ''))
			.' GROUP BY e.`id_employee`';
		//echo "<pre>".print_r($sql,1); die;
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
	
}


