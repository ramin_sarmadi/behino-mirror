<?php

class HDMail extends MailCore
{
	public static function Send($id_lang, $template, $subject, $templateVars, $to, $toName = NULL, $from = NULL, $fromName = NULL, $fileAttachments = NULL, $modeSMTP = NULL, $templatePath = _PS_MAIL_DIR_, $die = false, $cc = null, $bcc = null)
	{
		$configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_MAIL_SERVER', 'PS_MAIL_USER', 'PS_MAIL_PASSWD', 'PS_SHOP_NAME', 'PS_MAIL_SMTP_ENCRYPTION', 'PS_MAIL_SMTP_PORT', 'PS_MAIL_METHOD', 'PS_MAIL_TYPE'));
		if (!isset($configuration['PS_MAIL_SMTP_ENCRYPTION'])) $configuration['PS_MAIL_SMTP_ENCRYPTION'] = 'off';
		if (!isset($configuration['PS_MAIL_SMTP_PORT'])) $configuration['PS_MAIL_SMTP_PORT'] = 'default';

		if (!isset($from)) $from = $configuration['PS_SHOP_EMAIL'];
		if (!isset($fromName)) $fromName = $configuration['PS_SHOP_NAME'];

		if (!empty($from) AND !Validate::isEmail($from))
		{
 			Tools::dieOrLog(Tools::displayError('Error: parameter "from" is corrupted'), $die);
 			return false;
		}
		if (!empty($fromName) AND !Validate::isMailName($fromName))
		{
	 		Tools::dieOrLog(Tools::displayError('Error: parameter "fromName" is corrupted'), $die);
	 		return false;
		}
		if (!is_array($to) AND !Validate::isEmail($to))
		{
	 		Tools::dieOrLog(Tools::displayError('Error: parameter "to" is corrupted'), $die);
	 		return false;
		}

		if (!is_array($templateVars))
		{
	 		Tools::dieOrLog(Tools::displayError('Error: parameter "templateVars" is not an array'), $die);
	 		return false;
		}
		
		// Do not crash for this error, that may be a complicated customer name
		if(is_string($toName))
		{
			if (!empty($toName) AND !Validate::isMailName($toName))
	 			$toName = NULL;
		}
			
		if (!Validate::isTplName($template))
		{
	 		Tools::dieOrLog(Tools::displayError('Error: invalid email template'), $die);
	 		return false;
		}
			
		if (!Validate::isMailSubject($subject))
		{
	 		Tools::dieOrLog(Tools::displayError('Error: invalid email subject'), $die);
	 		return false;
		}

		/* Construct multiple recipients list if needed */
		$to_list = new Swift_RecipientList();
		if (is_array($to) and isset($to))
		{
			foreach ($to AS $key => $addr)
			{
				$to_name = NULL;
				$addr = trim($addr);
				if (!Validate::isEmail($addr))
				{
					Tools::dieOrLog(Tools::displayError('Error: invalid email address'), $die);
					return false;
				}
				if(is_array($toName))
				{
					if ($toName AND is_array($toName) AND Validate::isGenericName($toName[$key]))
						$to_name = $toName[$key];
				}
				$to_list->addTo($addr, $to_name);
			}
			$lof_pugin_to = $to[0];
			//$to = $to_list;
		} else {
			/* Simple recipient, one address */
			$lof_pugin_to = $to;
			$to_list->addTo($to, $to_name);
			//$to = $to_list;
		}
		/* LOF EDIT */
		if($cc){
			$to_name = NULL;
			if(is_array($cc)){
				foreach($cc as $key=>$addr){
					$addr = trim($addr);
					if (!Validate::isEmail($addr)){
						Tools::dieOrLog(Tools::displayError('Error: invalid email address'), $die);
						return false;
					}
					$to_list->addCc($addr, $to_name);
				}
			}else{
				$to_list->addCc($cc, $to_name);
			}
		}
		if($bcc){
			$to_name = NULL;
			if(is_array($bcc)){
				foreach($bcc as $key=>$addr){
					$addr = trim($addr);
					if (!Validate::isEmail($addr)){
						Tools::dieOrLog(Tools::displayError('Error: invalid email address'), $die);
						return false;
					}
					$to_list->addBcc($addr, $to_name);
				}
			}else{
				$to_list->addBcc($bcc, $to_name);
			}
		}
		$to_plugin = $lof_pugin_to;
		$to = $to_list;
		/* END LOF */
		try {
			/* Connect with the appropriate configuration */
			if ($configuration['PS_MAIL_METHOD'] == 2)
			{
				if (empty($configuration['PS_MAIL_SERVER']) OR empty($configuration['PS_MAIL_SMTP_PORT']))
				{
					Tools::dieOrLog(Tools::displayError('Error: invalid SMTP server or SMTP port'), $die);
					return false;
				}
				$connection = new Swift_Connection_SMTP($configuration['PS_MAIL_SERVER'], $configuration['PS_MAIL_SMTP_PORT'], ($configuration['PS_MAIL_SMTP_ENCRYPTION'] == "ssl") ? Swift_Connection_SMTP::ENC_SSL : (($configuration['PS_MAIL_SMTP_ENCRYPTION'] == "tls") ? Swift_Connection_SMTP::ENC_TLS : Swift_Connection_SMTP::ENC_OFF));
				$connection->setTimeout(4);
				if (!$connection)
					return false;
				if (!empty($configuration['PS_MAIL_USER']))
					$connection->setUsername($configuration['PS_MAIL_USER']);
				if (!empty($configuration['PS_MAIL_PASSWD']))
					$connection->setPassword($configuration['PS_MAIL_PASSWD']);
			}
			else
				$connection = new Swift_Connection_NativeMail();

			if (!$connection)
				return false;
			$swift = new Swift($connection, Configuration::get('PS_MAIL_DOMAIN'));
			/* Get templates content */
			
			$iso = Language::getIsoById((int)($id_lang));
			/*
			if (!$iso)
			{
				Tools::dieOrLog(Tools::displayError('Error - No ISO code for email'), $die);
				return false;
			}
			$template = $iso.'/'.$template;
*/
			$moduleName = false;
			$overrideMail = false;

			// get templatePath
			if (preg_match('#'.__PS_BASE_URI__.'modules/#', $templatePath) AND preg_match('#modules/([a-z0-9_-]+)/#ui' , $templatePath , $res))
				$moduleName = $res[1];

			if ($moduleName !== false AND (file_exists(_PS_THEME_DIR_.'modules/'.$moduleName.'/mails/'.$template.'.txt') OR
				file_exists(_PS_THEME_DIR_.'modules/'.$moduleName.'/mails/'.$template.'.html')))
				$templatePath = _PS_THEME_DIR_.'modules/'.$moduleName.'/mails/';
			elseif (file_exists(_PS_THEME_DIR_.'mails/'.$template.'.txt') OR file_exists(_PS_THEME_DIR_.'mails/'.$template.'.html'))
			{
				$templatePath = _PS_THEME_DIR_.'mails/';
				$overrideMail  = true;
			}
			elseif (!file_exists($templatePath.$template.'.txt') OR !file_exists($templatePath.$template.'.html'))
			{
				Tools::dieOrLog(Tools::displayError('Error - The following email template is missing:').' '.$templatePath.$template.'.txt', $die);
				return false;
			}
			$templateHtml = file_get_contents($templatePath.$template.'.html');
			$templateTxt = strip_tags(html_entity_decode(file_get_contents($templatePath.$template.'.txt'), NULL, 'utf-8'));

			if ($overrideMail AND file_exists($templatePath.$iso.'/lang.php'))
					include_once($templatePath.$iso.'/lang.php');
			elseif ($moduleName AND file_exists($templatePath.$iso.'/lang.php'))
				include_once(_PS_THEME_DIR_.'mails/'.$iso.'/lang.php');
			else
				include_once(_PS_MAIL_DIR_.$iso.'/lang.php');

			/* Create mail and attach differents parts */
			$message = new Swift_Message('['.Configuration::get('PS_SHOP_NAME').'] '. $subject);
			$templateVars['{shop_logo}'] = (file_exists(_PS_IMG_DIR_.'logo_mail.jpg')) ? $message->attach(new Swift_Message_Image(new Swift_File(_PS_IMG_DIR_.'logo_mail.jpg'))) : ((file_exists(_PS_IMG_DIR_.'logo.jpg')) ? $message->attach(new Swift_Message_Image(new Swift_File(_PS_IMG_DIR_.'logo.jpg'))) : '');
			$templateVars['{shop_name}'] = Tools::safeOutput(Configuration::get('PS_SHOP_NAME'));
			$templateVars['{shop_url}'] = Tools::getShopDomain(true, true).__PS_BASE_URI__;
			$swift->attachPlugin(new Swift_Plugin_Decorator(array($to_plugin => $templateVars)), 'decorator');
			if ($configuration['PS_MAIL_TYPE'] == 3 OR $configuration['PS_MAIL_TYPE'] == 2)
				$message->attach(new Swift_Message_Part($templateTxt, 'text/plain', '8bit', 'utf-8'));
			if ($configuration['PS_MAIL_TYPE'] == 3 OR $configuration['PS_MAIL_TYPE'] == 1)
				$message->attach(new Swift_Message_Part($templateHtml, 'text/html', '8bit', 'utf-8'));
			if($fileAttachments){
				foreach($fileAttachments as $fileAttachment){
					if ($fileAttachment AND isset($fileAttachment['content']) AND isset($fileAttachment['name']) AND isset($fileAttachment['mime']))
						$message->attach(new Swift_Message_Attachment($fileAttachment['content'], $fileAttachment['name'], $fileAttachment['mime']));
				}
			}
			/* Send mail */
			$send = $swift->send($message, $to, new Swift_Address($from, $fromName));
			$swift->disconnect();
			return $send;
		}

		catch (Swift_ConnectionException $e) { return false; }
	}

}
