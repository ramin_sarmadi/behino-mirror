<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdStatus extends ObjectModel
{
 	/** @var string Name */
	public 		$name;
	
 
	
	/** @var boolean Allow customer to view and download invoice when order is at this state */
	public 		$active = true;
	
	/** @var string Display state in the specified color */
	public		$color;
	
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'hd_status',
		'primary' => 'id_hd_status',
		'multilang' => true,
		'fields' => array(
			'color' => 		array('type' => self::TYPE_STRING, 'validate' => 'isColor'),
			'active' => 			array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			
			// Lang fields
			'name' => 		array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isGenericName', 'size' => 32),
		),
	);
	
	public function add($autodate = true, $nullValues = false){ 
		return parent::add($autodate, true); 
	}
	
	public function update($nullValues = false){
		return parent::update($nullValues);
	}
	
	public function delete(){
		if(!$this->isRemovable($this->id))
			return false;
	 	return parent::delete();
	}
	
	public static function isRemovable($id_hd_status){
		global $lof;
	 	if(!in_array($id_hd_status,$lof['un_remove_status']))
			return true;
		return false;
	}
	
	/**
	 * Return all prioritys
	 *
	 * @return array Status
	 * @deprecated
	 */
	public static function getStatus( $id_lang ) { 
		return Db::getInstance()->ExecuteS('
		SELECT d.`id_hd_status`, name 
		FROM `'._DB_PREFIX_.'hd_status` d
		LEFT JOIN `'._DB_PREFIX_.'hd_status_lang` dl ON d.id_hd_status = dl.id_hd_status
		WHERE `active` = 1 and id_lang='.(int)$id_lang.'
		ORDER BY `id_hd_status` ASC' );
	}
	
}


