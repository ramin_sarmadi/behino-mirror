<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdCustomFields extends ObjectModel {
 	/** @var string Name */
 	public 		$id;
	public 		$id_hd_custom_field;
	public  	$id_hd_department;
	public 		$name;
	public 		$type;
	public 		$values;
	public 		$required;
	public		$active;
	public		$position;
	public		$rows;
	public		$cols;
	public		$size;
	/* lang */
	public 		$title;
	public 		$description;
	public 		$labels;
	public 		$textarea;
	public 		$textbox;
	
	
	public static $definition = array(
		'table' => 'hd_custom_fields',
		'primary' => 'id_hd_custom_field',
		'multilang' => true,
		'fields' => array(
			'id_hd_department' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'position' => 			array('type' => self::TYPE_INT),
			'active' => 			array('type' => self::TYPE_BOOL),
			'name'		=>			array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
			'type'		=>			array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
			'required' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'rows' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'cols' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'size' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'values'		=>			array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
			// Lang fields
			'title' => 	array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true),
			'description' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
			'labels' =>			array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
			'textarea' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
			'textbox' => 			array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
		),
	);
	
	
	public function add($autodate = true, $nullValues = false){ 
		$this->position = HdCustomFields::getLastPosition((int)$this->id_hd_department);
		return parent::add($autodate); 
	}
	
	public function update($nullValues = false){
		if (parent::update($nullValues))
			return $this->cleanPositions($this->id_hd_department);
		return false;
	}
	
	public function delete(){
	 	if (parent::delete())
			return $this->cleanPositions($this->id_hd_department);
		return false;
	}
	/**
	 * Delete several categories from database
	 *
	 * return boolean Deletion result
	 */
	public function deleteSelection($customfields){
		$return = 1;
		foreach ($customfields AS $id_hd_custom_field){
			$customfield = new HdCustomFields((int)($id_hd_custom_field));
			$return &= $customfield->delete();
		}
		return $return;
	}
	
	public static function getLastPosition($id_hd_department){
		return (Db::getInstance()->getValue('SELECT MAX(position)+1 FROM `'._DB_PREFIX_.'hd_custom_fields` WHERE `id_hd_department` = '.(int)($id_hd_department)));
	}
	
	public function updatePosition($way, $position){
		if (!$res = Db::getInstance()->ExecuteS('
			SELECT cp.`id_hd_custom_field`, cp.`position`, cp.`id_hd_department` 
			FROM `'._DB_PREFIX_.'hd_custom_fields` cp
			WHERE cp.`id_hd_department` = '.(int)$this->id_hd_department.' 
			ORDER BY cp.`position` ASC'
		))
			return false;
		
		foreach ($res AS $custom_field)
			if ((int)($custom_field['id_hd_custom_field']) == (int)($this->id))
				$movedField = $custom_field;
		
		if (!isset($movedField) || !isset($position))
			return false;
		
		return (Db::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'hd_custom_fields`
			SET `position`= `position` '.($way ? '- 1' : '+ 1').'
			WHERE `position` 
			'.($way 
				? '> '.(int)($movedField['position']).' AND `position` <= '.(int)($position)
				: '< '.(int)($movedField['position']).' AND `position` >= '.(int)($position)).'
			AND `id_hd_department`='.(int)($movedField['id_hd_department']))
		AND Db::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'hd_custom_fields`
			SET `position` = '.(int)($position).'
			WHERE `id_hd_custom_field` = '.(int)($movedField['id_hd_custom_field']).'
			AND `id_hd_department`='.(int)($movedField['id_hd_department'])));
	}
	
	public static function cleanPositions($id_hd_department) {
		$result = Db::getInstance()->ExecuteS('
		SELECT `id_hd_custom_field`
		FROM `'._DB_PREFIX_.'hd_custom_fields`
		WHERE `id_hd_department` = '.(int)($id_hd_department).'
		ORDER BY `position`');
		$sizeof = sizeof($result);
		for ($i = 0; $i < $sizeof; ++$i){
			$sql = '
			UPDATE `'._DB_PREFIX_.'hd_custom_fields`
			SET `position` = '.(int)($i).'
			WHERE `id_hd_department` = '.(int)($id_hd_department).'
			AND `id_hd_custom_field` = '.(int)($result[$i]['id_hd_custom_field']);
			Db::getInstance()->Execute($sql);
		}
		return true;
	}
	
	public static function listFields($id_lang = NULL, $active = true, $id_hd_department = false){
		if (empty($id_lang))
			$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT cf.*,cfl.`title`,cfl.`labels`,cfl.`textarea`,cfl.`textbox`,cfl.`description`
		FROM  '._DB_PREFIX_.'hd_custom_fields cf
		JOIN '._DB_PREFIX_.'hd_custom_fields_lang cfl ON (cf.id_hd_custom_field = cfl.id_hd_custom_field)
		WHERE cfl.id_lang = '.(int)($id_lang).($active ? ' AND cf.`active` = 1 ' : '')
		.($id_hd_department ? ' AND cf.`id_hd_department` = '.(int)($id_hd_department) : '').' 
		ORDER BY cf.`position`');
	}
	
	public static function addCustomFieldValue($id_hd_custom_field, $id_hd_ticket, $value){
		$sql = 'INSERT INTO '._DB_PREFIX_.'hd_custom_fields_values (`id_hd_custom_field`,`id_hd_ticket`,`value`) 
			VALUES ('.(int)($id_hd_custom_field).','.(int)($id_hd_ticket).',\''.$value.'\')';
		return Db::getInstance()->Execute($sql);
	}
	
	public static function deteleCustomFieldValue($id_hd_custom_field = false, $id_hd_ticket = false){
		if(!$id_hd_custom_field && !$id_hd_ticket){
			return false;
		}
		$sql = 'DELETE FROM '._DB_PREFIX_.'hd_custom_fields_values WHERE 1 '.($id_hd_custom_field ? ' AND id_hd_custom_field = '.(int)($id_hd_custom_field) : '')
		.($id_hd_ticket ? ' AND id_hd_ticket = '.(int)($id_hd_ticket) : '');
		return Db::getInstance()->Execute($sql);
	}
	
	public static function getCustomFields($id_hd_custom_field = false, $id_hd_ticket = false){
		$sql = 'SELECT value
				FROM '._DB_PREFIX_.'hd_custom_fields_values 
				WHERE 1 '.($id_hd_custom_field ? ' AND `id_hd_custom_field` = '.$id_hd_custom_field : '').($id_hd_ticket ? ' AND `id_hd_ticket` = '.$id_hd_ticket : '');
				
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);
		$return = array();
		if($result)
			foreach($result as $row){
				$return[] = $row['value'];
			}
		return $return;
	}
}
?>