<?php

class HdDepartment extends ObjectModel
{
 	/** @var string Name */
	public 		$id;
	
	public 		$id_hd_department;
	public		$prefix;
	/** @var string Display state in the specified color */
	public		$assignment_type;
	public		$generation_rule;
	public		$next_number=0;
	public		$email_address;
	public		$email_address_fullname;
	public		$email_use_global=1;
	public		$customer_send_email=1;
	public		$customer_send_copy_email=1;
	public		$customer_attach_email=1;
	public		$staff_send_email=1;
	public		$staff_attach_email=1;
	public		$upload=1;
	public		$upload_extensions="zip,rar,gif,png,jpg";
	public		$upload_size= 10;
	public		$upload_files = 10;
	public		$notify_new_tickets_to;
	public		$notify_assign = 1;
	public		$id_hd_priority;
	public		$cc;
	public		$bcc;
	public		$predefined_subjects;
	public		$active=1;
	/* lang */
	public 		$name;
	
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'hd_department',
		'primary' => 'id_hd_department',
		'multilang' => true,
		'fields' => array(
			'prefix' => 	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 32, 'required' => true),
			'assignment_type' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'generation_rule' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'next_number' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'email_address' => 	array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'size' => 255),
			'email_address_fullname' => 	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255),
			'email_use_global' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'customer_send_email' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'customer_send_copy_email' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'customer_attach_email' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'staff_send_email' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'staff_attach_email' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'upload' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'upload_extensions' => 	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'upload_size' => 	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
			'upload_files' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'notify_new_tickets_to' => 	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'notify_assign' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'id_hd_priority' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'cc' => 	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'bcc' => 	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'predefined_subjects' => 	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'active' => 	array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'size' => 1),
			'position' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			
			// Lang fields
			'name' => 		array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 32)
		),
	);
	
	
	public function add($autodate = true, $nullValues = false){ 
		return parent::add($autodate, false); 
	}
	
	public function update($nullValues = false){
		return parent::update($nullValues);
	}
	
	public function delete(){
		$id_hd_department = $this->id;
	 	if (parent::delete()){
			$sql = 'DELETE FROM `'._DB_PREFIX_.'hd_custom_fields` WHERE `id_hd_department` = '.(int)($id_hd_department);
			return (Db::getInstance()->Execute($sql));
		}
		return false;
	} 
	
	public static function getDepartments( $id_lang ) { 
		return Db::getInstance()->ExecuteS('
		SELECT d.`id_hd_department`, name 
		FROM `'._DB_PREFIX_.'hd_department` d
		LEFT JOIN `'._DB_PREFIX_.'hd_department_lang` dl ON d.id_hd_department=dl.id_hd_department
		WHERE `active` = 1 and id_lang='.(int)$id_lang.'
		ORDER BY `name`' );
	}
	
	
	public static function getItem( $departmentId ){
		$data = Db::getInstance()->getRow('
		SELECT *
		FROM `'._DB_PREFIX_.'hd_department` d
		WHERE d.`id_hd_department`='.(int)$departmentId );
		
		return $data; 
	}
	
	public static function getEmployeeByDepartment($id_hd_department){
		if(!$id_hd_department)
			return array();
		return Db::getInstance()->ExecuteS('
		SELECT e.`id_employee`, CONCAT(e.`firstname`,\' \',e.`lastname`) AS name, ds.`id_hd_staff` 
		FROM `'._DB_PREFIX_.'employee` e
		LEFT JOIN `'._DB_PREFIX_.'hd_staff_hd_department` ds ON(ds.`id_employee` = e.`id_employee`)
		WHERE ds.`id_hd_department` = '.(int)($id_hd_department).' 
		GROUP BY e.`id_employee`' );
	}
	
}


