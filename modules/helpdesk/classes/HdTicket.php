<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdTicket extends ObjectModel
{
 	/** @var string Name */
 	public $id;
	//public 		$id_employee = true;
	
	/** @var string Display state in the specified color */
	public		$id_hd_ticket;
	public		$id_hd_department;
	public		$id_hd_staff;
 
	public	 	$id_customer;
	public  	$code;
	public 		$id_hd_status = 1;
	public 		$id_hd_priority;
	public 		$date_add;
	public 		$last_reply;
	public 		$last_reply_customer;
	public 		$replies;
	public 		$autoclose_sent;
	public 		$flagged;
	public 		$agent;
	public 		$referer;
	public 		$ip;

	public		$logged;
	public		$feedback;
	public		$has_files;
	public		$time_spent;
	public		$id_order;
	public		$id_product;
	/* lang */
	public 		$subject;
	
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'hd_ticket',
		'primary' => 'id_hd_ticket',
		'multilang' => true,
		'fields' => array(
			'id_hd_department' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'id_hd_staff' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'id_customer' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'code' => 		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
			'id_hd_status' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'id_hd_priority' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'date_add' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'last_reply' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'last_reply_customer' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'replies' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'autoclose_sent' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'flagged' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'agent' => 		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'referer' => 		array('type' => self::TYPE_STRING, 'validate' => 'isUrl'),
			'ip' => 		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'logged' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'feedback' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'has_files' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'time_spent' => 		array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
			'id_order' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'id_product' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			
			// Lang fields
			'subject' => 				array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 255)
			
		),
	);
	
	public function add($autodate = true, $nullValues = false) {
		$params = HdParams::getInstance();
		
		$this->last_reply = date('Y-m-d H:i:s');
		$this->ip = $_SERVER['REMOTE_ADDR'];
		$this->agent = $_SERVER['HTTP_USER_AGENT'];
		$this->referer = $_SERVER['HTTP_REFERER'];
 		$this->id_hd_status = $params->get("default_status",1);	
		
		return parent::add($autodate, $nullValues);
	}
	
	public function update($nullValues = false){
		return parent::update($nullValues);
	}
	
	public function delete() {
		$id_hd_ticket = $this->id;
		$return = 1;
	 	if( parent::delete()){
			$messages = HdTicketMessage::getTicketMessages($id_hd_ticket, false);
			if($messages)
				foreach($messages as $m){
					if(Validate::isLoadedObject($objMess = new HdTicketMessage($m['id_hd_ticket_message'])))
						$return &= $objMess->delete();
				}
		}else{
			$return = 0;
		}
		return $return;
	}
	
	public function deleteSelection($ids){
		$return = 1;
		foreach ($ids AS $id){
			$obj = new HdTicket((int)($id));
			$return &= $obj->delete();
		}
		return $return;
	}
	
	public function updateFlagg($value,$id_hd_ticket){
		$sql = 'UPDATE `'._DB_PREFIX_.'hd_ticket` SET flagged='.(int)($value).' WHERE id_hd_ticket='.$id_hd_ticket;
		return Db::getInstance()->Execute($sql);
	}
	
	public static function getTickets($id_lang = false, $id_hd_ticket = false, $id_customer = false, $id_department = false, $id_hd_staff = false, $id_hd_status = false, $id_hd_priority = false, $orderBy = false, $orderWay = 'DESC', $nb = false, $page = 1, $where = false){
		if (!$id_lang)
			$id_lang = Configuration::get('PS_LANG_DEFAULT');
		$sql = 'SELECT t.*, tl.`subject`, tl.`subject`, dl.`name` de_part_name, stl.`name` status_name, pl.`name` priority_name,
				CONCAT(c.lastname, \' \', c.firstname) as customer,CONCAT(e.lastname, \' \', e.firstname) as staff, st.`color` as color_status, p.`color` as color_priority
				FROM `'._DB_PREFIX_.'hd_ticket` t 
				LEFT JOIN `'._DB_PREFIX_.'hd_ticket_lang` tl ON(t.`id_hd_ticket` = tl.`id_hd_ticket` AND tl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'customer` c ON(c.`id_customer` = t.`id_customer`)
				LEFT JOIN `'._DB_PREFIX_.'hd_department` d ON(t.`id_hd_department` = d.`id_hd_department`)
				LEFT JOIN `'._DB_PREFIX_.'hd_department_lang` dl ON(dl.`id_hd_department` = d.`id_hd_department` AND tl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'hd_staff` s ON(t.`id_hd_staff` = s.`id_hd_staff`)
				LEFT JOIN `'._DB_PREFIX_.'employee` e ON(s.`id_employee` = e.`id_employee`)
				LEFT JOIN `'._DB_PREFIX_.'hd_status` st ON(st.`id_hd_status` = t.`id_hd_status`)
				LEFT JOIN `'._DB_PREFIX_.'hd_status_lang` stl ON(stl.`id_hd_status` = st.`id_hd_status` AND tl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'hd_priority` p ON(p.`id_hd_priority` = t.`id_hd_priority`)
				LEFT JOIN `'._DB_PREFIX_.'hd_priority_lang` pl ON(pl.`id_hd_priority` = p.`id_hd_priority` AND tl.`id_lang` = '.(int)($id_lang).')
				WHERE 1 '.($id_hd_ticket ? ' AND t.`id_hd_ticket` = '.(int)($id_hd_ticket) : '')
				.($id_customer ? ' AND t.`id_customer` = '.(int)($id_customer) : '')
				.($id_department ? ' AND t.`id_hd_department` = '.(int)($id_hd_department) : '')
				.($id_hd_staff ? ' AND t.`id_hd_staff` = '.(int)($id_hd_staff) : '')
				.($id_hd_status ? ' AND t.`id_hd_status` = '.(int)($id_hd_status) : '')
				.($id_hd_priority ? ' AND t.`id_hd_priority` = '.(int)($id_hd_priority) : '')
				.($where ? $where : '')
				.' GROUP BY t.`id_hd_ticket` '
				.($orderBy ? ' ORDER BY '.$orderBy.' '.$orderWay : '')
				.($nb ? ' LIMIT '.(((int)($page) - 1) * (int)($nb)).','.(int)($nb) : '');
		//echo "<pre>".print_r($sql,1); die;
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
	
	public static function getTicketsByNotStatus($id_lang, $id_hd_status){
		if(!$id_hd_status)
			return array();
		if (!$id_lang)
			$id_lang = Configuration::get('PS_LANG_DEFAULT');
		return Db::getInstance()->ExecuteS('SELECT t.* 
				FROM `'._DB_PREFIX_.'hd_ticket` t 
				LEFT JOIN `'._DB_PREFIX_.'hd_ticket_lang` tl ON(t.`id_hd_ticket` = tl.`id_hd_ticket` AND tl.`id_lang` = '.(int)($id_lang).') 
				WHERE 1 AND t.`id_hd_status` <> '.(int)($id_hd_status)
			);
	}
}

