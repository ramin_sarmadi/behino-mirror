<?php
/*
* 2007-2011 PrestaShop 
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2011 PrestaShop SA
*  @version  Release: $Revision: 7540 $
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HdQuickResponse extends ObjectModel
{
 	/** @var string Name */
 	public $id;
	public		$id_hd_quickresponse;
	public		$title;
	public		$content;
	public		$active = 1;
	
	protected 	$table = 'hd_quickresponse';
	protected 	$identifier = 'id_hd_quickresponse';
	
	protected	$fieldsRequired = array('title');
	protected $fieldsSize = array('content' => 3999999999999999999,'title' => 255);
	protected	$fieldsValidates = array( 'title'=>'isCatalogName', 'id_hd_quickresponse'=>'isUnsignedId', 'content'=>'isString' ,'active'=>'isUnsignedId');
	
	
	public function getFields() {
		parent::validateFields();
		
		if (isset($this->id))
			$fields['id_hd_quickresponse'] = (int)($this->id);
		
		$fields['title'] = pSQL($this->title);
		$fields['content'] = pSQL($this->content,true);
		$fields['active'] = (int)($this->active);
		
		return $fields;
	}
	
	public function add($autodate = true, $nullValues = false) {
		return parent::add($autodate, $nullValues);
	}
	
	public function update($nullValues = false){
		return parent::update($nullValues);
	}
	
	public function delete() {
	 	return parent::delete();
	}
	
	public function deleteSelection($ids){
		$return = 1;
		foreach ($ids AS $id){
			$obj = new HdQuickResponse((int)($id));
			$return &= $obj->delete();
		}
		return $return;
	}
	
	public static function getQuickresponses( $active = true, $orderBy = 'id_hd_quickresponse', $orderWay = 'DESC', $nb = false, $page = 1){
		
		$sql = 'SELECT q.*
				FROM `'._DB_PREFIX_.'hd_quickresponse` q 
				WHERE 1 '.($active ? ' AND active = 1 ' : '')
				.($orderBy ? ' ORDER BY '.$orderBy.' '.$orderWay : '')
				.($nb ? ' LIMIT '.(((int)($page) - 1) * (int)($nb)).','.(int)($nb) : '');
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
	
	
}

