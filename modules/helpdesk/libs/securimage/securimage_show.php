<?php
require_once(dirname(__FILE__).'/../../../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../../../init.php');

require_once(dirname(__FILE__).'/../../defines.helpdesk.php');
require_once( _PS_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/classes/HdParams.php' );
require_once dirname(__FILE__) . '/securimage.php';

$params = new HdParams('');
$img = new securimage();

// You can customize the image by making changes below, some examples are included - remove the "//" to uncomment

$img->ttf_file        = './AHGBold.ttf';
if($params->get('captcha_type') == 'SI_CAPTCHA_MATHEMATIC')
	$img->captcha_type    = Securimage::SI_CAPTCHA_MATHEMATIC; // show a simple math problem instead of text

if($params->get('captcha_case_sensitive'))
	$img->case_sensitive  = true;                              // true to use case sensitve codes - not recommended
else
	$img->case_sensitive  = false;

$img->image_height    = $params->get('image_height', 50);                                // width in pixels of the image
$img->image_width     = $img->image_height * M_E;          // a good formula for image size
$img->code_length    	= $params->get('captcha_characters', 5);
$img->perturbation    = $params->get('perturbation', 0.75);                               // 1.0 = high distortion, higher numbers = more distortion
$img->image_bg_color  = new Securimage_Color($params->get('image_bg_color','#fff8f4'));   // image background color
$img->text_color      = new Securimage_Color($params->get('text_color','#000a14'));   // captcha text color
$img->num_lines       = $params->get('num_lines',8);                                 // how many lines to draw over the image
$img->line_color      = new Securimage_Color($params->get('line_color','#6254ff'));   // color of lines over the image
if($params->get('captcha_img_type','default') == 'SI_IMAGE_JPEG')
	$img->image_type      = SI_IMAGE_JPEG;                     // render as a jpeg image
elseif($params->get('captcha_img_type','default') == 'SI_IMAGE_GIF')
	$img->image_type      = SI_IMAGE_GIF; 

$img->signature_color = new Securimage_Color(rand(0, 64),
                                             rand(64, 128),
                                             rand(128, 255));  // random signature color

$img->show();
