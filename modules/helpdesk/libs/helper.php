<?php
/**
 * $ModDesc
 * 
 * @version		$Id: helper.php $Revision
 * @package		modules
 * @subpackage	$Subpackage
 * @copyright	Copyright (C) May 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>. All rights reserved.
 * @website 	htt://landofcoder.com
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
    define('_CAN_LOAD_FILES_',1);
}
include_once( dirname(__FILE__)."/../classes/HdParams.php" );
include_once( dirname(__FILE__)."/../classes/HdTicketFile.php" );
include_once( dirname(__FILE__)."/../classes/HDMail.php" );
include_once( dirname(__FILE__)."/../classes/HdTicket.php" );
include_once( dirname(__FILE__)."/../classes/HdStaff.php" );
include_once( dirname(__FILE__)."/../classes/HdPriority.php" );
include_once( dirname(__FILE__)."/../classes/Hdstatus.php" );
include_once( dirname(__FILE__)."/../classes/HdTicketMessage.php" );
include_once( dirname(__FILE__)."/../classes/HdCustomFields.php" );
include_once( dirname(__FILE__)."/../classes/HdDepartment.php" );
include_once( dirname(__FILE__)."/../classes/HDMail.php" );

if( !class_exists('HelpDeskHelper', false) ){  
	class HelpDeskHelper {
		/**
		* Module name
		*/
		var $modulename = 'helpdesk';
		var $errors = array();
		
		public static function renderField($obj){
			$str = '';
			switch($obj->type){
				case 'freetext':
					$str .= '<div class="fieldfreetext">'.$obj->textarea.'</div>';
				break;
				case 'textbox':
					$str .= '<div class="fieldtextbox"><input type="text" name="'.$obj->name.'" value="'.($obj->lofValues ? $obj->lofValues : $obj->textbox).'" '.($obj->size ? 'size="'.$obj->size.'"' : '').'/>'.($obj->required ? '<sup>*</sup>' : '').'</div>';
				break;
				case 'textarea':
					$params = new HdParams('');
					if($params->get('allow_rich_editor')){
						$str .= '<div class="fieldtextarea"><textarea class="rte" name="'.$obj->name.'" cols="'.$obj->cols.'" rows="'.$obj->rows.'">'.htmlentities(($obj->lofValues ? $obj->lofValues : $obj->textarea), ENT_COMPAT, 'UTF-8').'</textarea></div>';
					}else{
						$str .= '<div class="fieldtextarea"><textarea name="'.$obj->name.'" cols="'.$obj->cols.'" rows="'.$obj->rows.'">'.htmlentities(stripslashes(($obj->lofValues ? $obj->lofValues : $obj->textarea)), ENT_COMPAT, 'UTF-8').'</textarea></div>';
					}
				break;
				case 'select':
				case 'multipleselect':
					$str = '<div class="fieldselect">';
					if($obj->values && $obj->labels){
						$arrValue = array();
						if($obj->lofValues){
							$arrValue = explode('::',$obj->lofValues);
							if($arrValue)
								foreach($arrValue as &$row)
									$row = trim($row);
						}
						$data = self::renStrToArr($obj->values, $obj->labels);
						$str .= '<select name="'.$obj->name.($obj->type == 'multipleselect' ? '[]' : '').'" '.($obj->type == 'multipleselect' ? 'multiple="multiple"' : '').' '.($obj->size ? 'size="'.$obj->size.'"' : '').'>';
						for( $i = 0; $i < $data['nb'] ; $i++ ){
							$str .= '<option value="'.trim($data['values'][$i]).'"'.(in_array(trim($data['values'][$i]), $arrValue) ? ' selected="selected"' : '').'>'.trim($data['labels'][$i]).'</option>';
						}
						$str .= '</select>'.($obj->required ? '<sup>*</sup>' : '');
					}
					$str .= '</div>';
				break;
				case 'checkbox':
					$str .= '<div class="lofcheckbox">';
					if($obj->values && $obj->labels){
						$arrValue = array();
						if($obj->lofValues){
							$arrValue = explode('::',$obj->lofValues);
							if($arrValue)
								foreach($arrValue as &$row)
									$row = trim($row);
						}
						$data = self::renStrToArr($obj->values, $obj->labels);
						for( $i = 0; $i < $data['nb'] ; $i++ ){
							$str .= '<input type="checkbox" name="'.$obj->name.'[]" value="'.trim($data['values'][$i]).'" id="checkbox-'.$obj->id.'-'.$i.'" '.(in_array(trim($data['values'][$i]),$arrValue) ? ' checked="checked"' : '').'/> <label for="checkbox-'.$obj->id.'-'.$i.'" class="lof">'.trim($data['labels'][$i]).'</label>';
						}
						$str .= ($obj->required ? '<sup>*</sup>' : '');
					}
					$str .= '</div>';
				break;
				case 'radio':
					$str .= '<div class="lofradio">';
					if($obj->values && $obj->labels){
						$arrValue = array();
						if($obj->lofValues){
							$arrValue = explode('::',$obj->lofValues);
							if($arrValue)
								foreach($arrValue as &$row)
									$row = trim($row);
						}
						$data = self::renStrToArr($obj->values, $obj->labels);
						for( $i = 0; $i < $data['nb'] ; $i++ ){
							$str .= '<input type="radio" name="'.$obj->name.'[]" value="'.trim($data['values'][$i]).'" id="radio-'.$obj->id.'-'.$i.'" '.(in_array(trim($data['values'][$i]),$arrValue) ? 'checked="checked"' : '').'> <label for="radio-'.$obj->id.'-'.$i.'" class="lof">'.trim($data['labels'][$i]).'</label>';
						}
						$str .= ($obj->required ? '<sup>*</sup>' : '');
					}
					$str .= '</div>';
				break;
				case 'calendar':
				case 'calendartime':
					$str .= '<div class="lofcalendar">';
					$str .= '<input type="text" name="'.$obj->name.'" id="calendar-'.$obj->id.'" class="calendar" value="'.(isset($obj->lofValues) ? $obj->lofValues : $obj->values).'" '.($obj->size ? 'size="'.$obj->size.'"' : '').'>';
					$str .= ($obj->required ? '<sup>*</sup>' : '');
					$str .= '</div>';
					if($obj->type == 'calendar'){
						$str .= self::includeDatepicker('calendar-'.$obj->id);
					}else{
						$str .= self::includeDatepicker('calendar-'.$obj->id, true);
					}
				break;
			}
			return $str;
		}
		/**
		* string value && string labels, render to array
		*
		*/
		public static function renStrToArr($values,$labels){
			$values = str_replace("<br>","\n",$values);
			$arrValues = explode("\n",$values);
			$labels = str_replace("<br>","\n",$labels);
			$arrLabels = explode("\n",$labels);
			if(count($arrValues) > count($arrLabels))
				$nb = count($arrLabels);
			else
				$nb = count($arrValues);
			return  array('values'=>$arrValues,'labels'=>$arrLabels,'nb'=>$nb);
		}
		public static function bindDatepicker($id, $time) {
			$str = '';
			if ($time)
			$str .= '
				var dateObj = new Date();
				var hours = dateObj.getHours();
				var mins = dateObj.getMinutes();
				var secs = dateObj.getSeconds();
				if (hours < 10) { hours = "0" + hours; }
				if (mins < 10) { mins = "0" + mins; }
				if (secs < 10) { secs = "0" + secs; }
				var time = " "+hours+":"+mins+":"+secs;';

			$str .= '
			$(function() {
				$("#'.$id.'").datepicker({
					prevText:"",
					nextText:"",
					dateFormat:"yy-mm-dd"'.($time ? '+time' : '').'});
			});';
			return $str;
		}

		// id can be a identifier or an array of identifiers
		public static function includeDatepicker($id, $time = false) {
			$id_lang = (int)Context::getContext()->language->id;
			$str = '';
			$str .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/jquery-ui-1.8.10.custom.min.js"></script>';
			$iso = Db::getInstance()->getValue('SELECT iso_code FROM '._DB_PREFIX_.'lang WHERE `id_lang` = '.(int)($id_lang));
			if ($iso != 'en')
				$str .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/jquery/datepicker/ui/i18n/ui.datepicker-'.$iso.'.js"></script>';
			$str .= '<script type="text/javascript">';
				if (is_array($id))
					foreach ($id as $id2)
						$str .= self::bindDatepicker($id2, $time);
				else
					$str .= self::bindDatepicker($id, $time);
			$str .= '</script>';
			return $str;
		}
		/**
		* upload file
		*/
		public function checkFileUpload($files, $objDepartment){
			$upload_extensions = explode(",", $objDepartment->upload_extensions);
			foreach($upload_extensions as &$row){
				$row = trim($row);
			}
			$correct_files = array();
			
			if (is_array($files))
				foreach ($files['tmp_name'] as $i => $file_tmp) {
					if ($files['error'][$i] == 4) continue;
					
					$file_name = $files['name'][$i];
					$type 	= $files['type'][$i];
					if ($files['error'][$i]) {
						$this->errors[] = ('Could not upload the following file:').' '.$file_name;
						return false;
					}
					if (!$this->isAllowedExtension($this->getExt($file_name), $upload_extensions)) {
						$upload_extensions = implode(', ', $upload_extensions);
						$this->errors[] = ('Could not upload the following file:').' '.$file_name.' '.('because the file\'s extension is not in the list of accepted extensions:').$upload_extensions;
						return false;
					}
					if ($objDepartment->upload_size > 0 && $files['size'][$i] > $objDepartment->upload_size*1048576) {
						$this->errors[] = ('Could not upload the following file:').' '.$file_name.' '.('because it exceeded the maximum allowed size of').' '.$department->upload_size.('megabytes.');
						return;
					}
					
					$correct_files[] = array('src' => 'upload', 'tmp_name' => $file_tmp, 'name' => $file_name, 'type' => $type);
				}
			return $correct_files;
		}
		/**
		* add attachment
		*/
		public function addAttachments($id_hd_ticket, $id_hd_ticket_message, $objDepartment, $files){
			$attachments = array();
			foreach ($files as $file) {
				if ($objDepartment->upload_files > 0 && count($attachments) >= $objDepartment->upload_files)
					break;
				if ($file['src'] == 'upload') {
					$new_file = new HdTicketFile();
					$new_file->id_hd_ticket = $id_hd_ticket;
					$new_file->id_hd_ticket_message = $id_hd_ticket_message;
					$new_file->filename = $file['name'];
					$new_file->mime = $file['type'];
					
					$new_file->add();
					$hash = md5($new_file->id.' '.$id_hd_ticket_message);
					if(!is_dir(LOF_UPLOAD_FOLDER))
						mkdir(LOF_UPLOAD_FOLDER);
					$result = $this->upload($file['tmp_name'], LOF_UPLOAD_FOLDER.$hash);
				}
				$attachment = array();
				$attachment['path'] = LOF_UPLOAD_FOLDER.$hash;
				$attachment['filename'] = $new_file->filename;
				$attachments[] = $attachment;
			}
			return $attachments;
		}
		/**
		* get extension file
		*/
		function getExt($file) {
			$chunks = explode('.', $file);
			$chunksCount = count($chunks) - 1;
			if($chunksCount > 0) {
				return $chunks[$chunksCount];
			}
			return false;
		}
		
		function isAllowedExtension($ext, $ext_array) {
			if (!is_array($ext_array)) return true;
			if (count($ext_array) == 0) return true;
			if (count($ext_array) == 1 && trim($ext_array[0]) == '') return true;
			if (in_array('*',$ext_array)) return true;
			
			// convert everything to lowercase
			$ext = strtolower($ext);
			array_walk($ext_array, array('HelpDeskHelper', 'arraytolower'));
			
			return in_array($ext,$ext_array);
		}
		
		function arraytolower(&$value, $key) {
			$value = strtolower($value);
		}
		
		function upload($src, $dest) {
			$ret		= false;
			$baseDir = dirname($dest);
			if (is_writeable($baseDir) && move_uploaded_file($src, $dest)) { 
				$ret = true;
			}
			return $ret;
		}
		/**
		*
		*/
		public static function renderDisplayFields($fields, $id_hd_ticket){
			foreach($fields as &$field){
				$lofvalue = HdCustomFields::getCustomFields($field['id_hd_custom_field'], $id_hd_ticket);
				$value = '';
				if($lofvalue){
					$values = explode('::',$lofvalue[0]);
					foreach($values as &$row)
						$row = trim($row);
					$value = implode(', ',$values);
				}
				$field['lofvalue'] = $value;
			}
			return $fields;
		}
		/**
		*
		*/
		public static function getArgsEmail($subject, $message, $objTicket, $objDepart, $objMess){
			$id_lang = (int)Context::getContext()->language->id;
			$params = new HdParams('');
			$arrA = array('{code}','{customer_name}', '{customer_email}', '{subject}', '{title_ticket}', '{message}', '{link_ticket}', '{custom_fields}', 
				'{staff_name}', '{staff_email}', '{link_site}', '{replies}', '{department}', '{close_interval}');
			
			$objCus = new Customer($objTicket->id_customer);
			$link_ticket = _PS_BASE_URL_.__PS_BASE_URI__.'index.php?process=tickets&fc=module&module=helpdesk&controller=tickets&id_ticket='.$objTicket->id;
			$fields = HdCustomFields::listFields($id_lang, true, $objTicket->id_hd_department);
			$fields = self::renderDisplayFields($fields, $objTicket->id);
			$str = '';
			if($fields){
				foreach($fields as $f){
					$str .= $f['title'].': '.$f['lofvalue'].' <br/> ';
				}
			}
			$objStaff = new HdStaff($objTicket->id_hd_staff);
			$objEmp = new Employee($objStaff->id_employee);
			$link_site = Tools::htmlentitiesutf8('http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
			
			$arrB = array($objTicket->code, $objCus->firstname.' '.$objCus->lastname, $objCus->email, $objTicket->subject, $objTicket->subject, html_entity_decode($objMess->message, ENT_QUOTES, 'UTF-8'), $link_ticket, $str, 
				$objEmp->firstname.' '.$objEmp->lastname, $objEmp->email, $link_site, $objTicket->replies, $objDepart->name,$params->get('autoclose_interval') );
			$return = array();
			$return['subject'] = str_replace( $arrA, $arrB, $subject);
			$return['message'] = str_replace( $arrA, $arrB, $message);
			return $return;
		}
		/** 
		* SEND EMAIL
		* var tmpEmail: {code},{customer_name},{subject}, {message},{link_ticket},{customer_email}, {custom_fields}, 
				{staff_name}, {link_site}, {replies}, {staff_email}, {department}
		* $type = message | other | attach 
		*/
		public static function sendMail($to =array(), $toName = array(), $subject = '', $message = '', $id_hd_ticket_message = false, $type = 'message', $id_hd_ticket = false, $id_hd_department = false){
			$id_lang = (int)Context::getContext()->language->id;
			$params = new HdParams('');
			if($id_hd_ticket_message)
				if(!Validate::isLoadedObject($objMess = new HdTicketMessage($id_hd_ticket_message)))
					return false;
			$id_hd_ticket = ($id_hd_ticket ? $id_hd_ticket : ($id_hd_ticket_message ? $objMess->id_hd_ticket : false));
			$objTicket = new stdclass;
			if($id_hd_ticket)
				if(!Validate::isLoadedObject($objTicket = new HdTicket($id_hd_ticket, $id_lang)))
					return false;
			$id_hd_department = ($id_hd_department ? $id_hd_department : ($id_hd_ticket ? $objTicket->id_hd_department : false));
			if($id_hd_department)
				if(!Validate::isLoadedObject($objDepart = new HdDepartment($id_hd_department, $id_lang)))
					return false;
			
			$args = self::getArgsEmail($subject, $message, ($objTicket ? $objTicket : new stdClass()), ($objDepart ? $objDepart : new stdClass()), ($objMess ? $objMess : new stdClass()));
			$subject = $args['subject'];
			$templateVars = array('{content}' => $args['message']);
			
			/* Use rmail global */
			if($objDepart->id && !$objDepart->email_use_global){
				$from = $objDepart->email_address;
				$fromName = $objDepart->email_address_fullname;
			}else{
				$from = $params->get('email_address');
				$fromName = $params->get('email_address_fullname');
			}
			$attachments = array();
			$cc = array();
			$bcc = array();
			if($type == 'attach' || $type == 'message'){
				/* Attachments*/
				if(($objDepart->customer_attach_email && $objMess->id_customer) || ($objDepart->staff_attach_email && $objMess->id_employee)){
					$files = HdTicketMessage::getFiles($objMess->id);
					if($files){
						foreach($files as $f){
							$attach['content'] = @file_get_contents(LOF_UPLOAD_FOLDER.md5($f['id_hd_ticket_files'].' '.$objMess->id));
							$attach['name'] = $f['filename'];
							$attach['mime'] = $f['mime'];
						}
						if(isset($attach))
							$attachments[] = $attach;
					}
				}
			}
			if($type == 'message'){
				/* CC email */
				if($objDepart->cc){
					$strCC = str_replace("\r\n", "\n", $objDepart->cc);
					$cc = explode("\n", $strCC);
				}
				/* BCC email */
				if($objDepart->bcc){
					$strBCC = str_replace("\r\n", "\n", $objDepart->bcc);
					$bcc = explode("\n", $strBCC);
				}
			}
			/*SEND MAIL*/
			//echo "<pre>".print_r($to,1); die;
			HDMail::Send($id_lang, 'helpdesk',$subject, $templateVars, $to, $toName, $from, $fromName, $attachments, NULL, _PS_MODULE_DIR_.LOF_MODULE_NAME_HELPDESK.'/tmpl/emails/', true, $cc, $bcc);
		}
		/**
		* Generate Code
		* 
		*/
		public static function generateCode($objDepart, $max=5, $type = 'random'){
			if($type == 'random'){
				$number = '';
				for($i=0;$i<$max;$i++) {
					$w1 = rand(0,1);
					$w2 = 1-$w1;
					$number .= chr($w1*rand(65,90)+$w2*rand(48,57));
				}
				$number = strtoupper($number);
			}elseif($type == 'sequential'){
				$params = new HdParams('');
				$number = $params->get('next_number',1);
				if($params->isParamsExist('next_number')){
					$result = Db::getInstance()->Execute( 'UPDATE '._DB_PREFIX_.'hd_configuration SET `value` = `value` + 1 WHERE name=\'next_number\' LIMIT 1' );
				}else{
					$result = Db::getInstance()->Execute( "INSERT INTO "._DB_PREFIX_.'hd_configuration(`name`, `value`) VALUES ("next_number",'.($number + 1).')' );
				}
				$params->set('next_number', Tools::getValue('next_number',($number+1)));
				$number = str_pad($number, $max, 0, STR_PAD_LEFT);
			}
			return ($objDepart->prefix.'-'.$number);
		}
		/**
		* function assign ticket for staff
		*
		*/
		public static function assignStaff($objDepart){
			if(!Validate::isLoadedObject($objDepart))
				return 0;
			if(!$objDepart->assignment_type)
				return 0;
			$staffs = HdStaff::getStaffs($objDepart->id);
			if(!$staffs)
				return 0;
			return $staffs[rand(0,(count($staffs) - 1))]['id_hd_staff'];
		}
		/**
		* function get folder admin
		*/
		public static function getFolderAdmin(){
			$folders = array('cache','classes','config','controllers','css','docs','download','img','js','localization','log','mails',
			'modules','override','themes','tools','translations','upload','webservice','.','..');
			$handle = opendir(_PS_ROOT_DIR_);
			if (! $handle) {
				return false;
			}
			while (false !== ($folder = readdir($handle))) {
				if (is_dir(_PS_ROOT_DIR_ .'/'. $folder)){
					if(!in_array($folder, $folders)){
						$folderadmin = opendir(_PS_ROOT_DIR_ .'/'. $folder);
						if (!$folderadmin) 
							return _PS_ROOT_DIR_ .'/'. $folder;
						
						while (false !== ($file = readdir($folderadmin))) {
							if (is_file(_PS_ROOT_DIR_ .'/'.  $folder.'/'.$file) && ($file == 'header.inc.php')){
								return _PS_ROOT_DIR_ .'/'.  $folder;
							}
						}
					}
				}
			}
			return $false;
		}
		/**
		* function Create key
		*/
		public static function createKey($module_name = 'helpdesk', $notExist = false){
			$module_name = strtoupper($module_name);
			if($notExist){
				if(!Configuration::get('_LOF_'.$module_name.'_SESSION_KEY_')){
					Configuration::updateValue('_LOF_'.$module_name.'_SESSION_KEY_',md5(rand(10,10000000).rand(10,10000000).rand(10,10000000)));
				}
			}else{
				Configuration::updateValue('_LOF_'.$module_name.'_SESSION_KEY_',md5(rand(10,10000000).rand(10,10000000).rand(10,10000000)));
			}
		}
		/**
		* function get key
		*/
		public static function getKey($module_name = 'helpdesk'){
			$module_name = strtoupper($module_name);
			return Configuration::get('_LOF_'.$module_name.'_SESSION_KEY_');
		}
		/**
		* function delete key
		*/
		public static function deleteKey($module_name = 'helpdesk'){
			$module_name = strtoupper($module_name);
			return Configuration::deleteByName('_LOF_'.$module_name.'_SESSION_KEY_');
		}
		/**
		* public function check Permission
		*/
		public static function checkPermission($id_employee, $objTicket){
			if(!$id_employee)
				return false;
			if(!Validate::isLoadedObject($employee = new Employee($id_employee)))
				return false;
			$id_hd_staff = HdStaff::getStaffByEmployee($id_employee);
			return (in_array($objTicket->id_hd_staff, $id_hd_staff) || $employee->id_profile == LOF_ID_PROFILE_ADMIN ? true : false);
		}
	}/*End Class*/
}/*End Check*/
?>