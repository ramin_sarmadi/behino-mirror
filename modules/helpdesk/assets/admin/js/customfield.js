$(document).ready(function() {
	var type = $('#lofchosetype').val();
	showType(type);
	$('#lofchosetype').change(function(){
		var type = $(this).find('option:selected').val();
		showType(type);
	});
	$('#idname').blur(function(){
		var value = $(this).val();
		$(this).val(prep4SQL(value));
	});
	function showType(type){
		$('.container-value .valuegeneral').css('display','none');
		$('.container-value .'+type).css('display','block');
	}
	function prep4SQL(str){
		if(str!='') {
			str = str.replace('lof_','');
			str = 'lof_' + str.replace(/[^a-zA-Z]+/g,'');
		}
		return str;
	}
});
