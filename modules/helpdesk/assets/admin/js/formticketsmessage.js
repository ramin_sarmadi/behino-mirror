function changeDepartment(){
	var id_department = $('#id_hd_department').val();
	
	var upload = mess_data_init[2];
	var upload_files = mess_data_init[3];
	var upload_extensions = mess_data_init[4];
	
	attackment(upload,upload_files,upload_extensions);
}

function attackment(upload,upload_files,upload_extensions){
	if (document.getElementById("lof_files") == null)
		return false;
	document.getElementById('lof_files').innerHTML = '';
	document.getElementById('input_lof_files').value = '';
	if(upload){
		if(upload_files > nb_file || upload_files == 0){
			$('.lofattackment').css('display','block');
			$('b.lof_extension').text(upload_extensions);
			nb_file ++;
		}
	}else{
		$('.lofattackment').css('display','none');
	}
}

function lof_add_attachments() {
	var id_department = $('#id_hd_department').val();
	
	var upload = mess_data_init[2];
	var upload_files = mess_data_init[3];
	var upload_extensions = mess_data_init[4];
	
	if(upload_files > nb_file || upload_files == 0){
		var new_upload = document.createElement('input');
		new_upload.setAttribute('name', 'lof_files[]');
		new_upload.setAttribute('type', 'file');
		document.getElementById('lof_files').appendChild(new_upload);
		
		var new_br = document.createElement('br');
		document.getElementById('lof_files').appendChild(new_br);
		nb_file ++;
	}else{
		alert(message_error);
	}
}

function deleteFile(idfield, id_hd_ticket_files){
	$.ajax({
		type: "POST",
		url: ajaxUrl,
		data: "id_hd_ticket_files="+id_hd_ticket_files+"&lofajax=1&task=deleteFile",
		dataType: 'json',
		success: function(json_data){
			if(json_data.result == '1'){
				$('#'+idfield).remove();
				nb_file --;
				if(nb_file < mess_data_init[3] || mess_data_init[3] == 0){
					$('.add_attachment').css('display','block');
				}
			}
		}
	});
}

jQuery(document).ready(function(){
	if(nb_file < mess_data_init[3] || mess_data_init[3] == 0){
		$('.add_attachment').css('display','block');
	}else{
		$('.add_attachment').css('display','none');
	}
	var id_department = mess_data_init[0];
	changeDepartment(id_department);
	$('#id_hd_department').change(function(){
		changeDepartment();
	});
	$('#use_tmpl').change(function(){
		var id_hd_quickresponse = $(this).val();
		var id_hd_ticket = $('#id_hd_ticket').val();
		$.ajax({
			type: "POST",
			url: ajaxUrl,
			data: "id_hd_quickresponse="+id_hd_quickresponse+"&id_hd_ticket="+id_hd_ticket+"&lofajax=1&task=useTemplate",
			success: function(data){
				tinyMCE.get('message').setContent(data);
			}
		});
	});
});