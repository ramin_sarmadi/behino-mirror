function select_innerHTML(objeto,innerHTML){
	objeto.innerHTML = ""
	var selTemp = document.createElement("micoxselect")
	var opt;
	selTemp.id="micoxselect1"
	document.body.appendChild(selTemp)
	selTemp = document.getElementById("micoxselect1")
	selTemp.style.display="none"
	if(innerHTML.toLowerCase().indexOf("<option")<0){//se n�o � option eu converto
		innerHTML = "<option>" + innerHTML + "</option>"
	}
	innerHTML = innerHTML.toLowerCase().replace(/<option/g,"<span").replace(/<\/option/g,"</span")
	selTemp.innerHTML = innerHTML
	
	for(var i=0;i<selTemp.childNodes.length;i++){
  var spantemp = selTemp.childNodes[i];
  
		if(spantemp.tagName){     
			opt = document.createElement("OPTION")
	
   if(document.all){ //IE
	objeto.add(opt)
   }else{
	objeto.appendChild(opt)
   }
   //getting attributes
   for(var j=0; j<spantemp.attributes.length ; j++){
	var attrName = spantemp.attributes[j].nodeName;
	var attrVal = spantemp.attributes[j].nodeValue;
	if(attrVal){
	 try{
	  opt.setAttribute(attrName,attrVal);
	  opt.setAttributeNode(spantemp.attributes[j].cloneNode(true));
	 }catch(e){}
	}
   }
   //getting styles
   if(spantemp.style){
	for(var y in spantemp.style){
	 try{opt.style[y] = spantemp.style[y];}catch(e){}
	}
   }
   //value and text
   opt.value = spantemp.getAttribute("value")
   opt.text = spantemp.innerHTML
   //IE
   opt.selected = spantemp.getAttribute('selected');
   opt.className = spantemp.className;
  }
 }
 document.body.removeChild(selTemp)
 selTemp = null
}

function filterCustomer(){
	var customer_name = document.getElementById("cus_name").value;
	$.ajax({
		type: "POST",
		url: ajaxUrl,
		data: "customer_name="+customer_name+"&lofajax=1&task=filterCusName",
		success: function(data){
			select_innerHTML(document.getElementById("id_customer"),data);
			filterOrder(document.getElementById("id_customer").value);
		}
	});
}

function filterProduct(id_lang){
	var pro_name = document.getElementById("pro_name").value;
	$.ajax({
		type: "POST",
		url: ajaxUrl,
		data: "pro_name="+pro_name+"&id_lang="+id_lang+"&lofajax=1&task=filterProName",
		success: function(data){
			select_innerHTML(document.getElementById("id_product"),data);
		}
	});
}

function filterOrder(id_customer){
	$.ajax({
		type: "POST",
		url: ajaxUrl,
		data: "id_customer="+id_customer+"&lofajax=1&task=filterOrder",
		success: function(data){
			select_innerHTML(document.getElementById("id_order"),data);
		}
	});
}

function changeDepartment(){
	var id_department = $('#id_hd_department').val();
	if(lofinit_data[id_department] === undefined){
		$('.custom-field').css('display','none');
		$('.lofattackment').css('display','none');
		return false;
	}
	var id_hd_priority = lofinit_data[id_department][1];
	var upload = lofinit_data[id_department][2];
	var upload_files = lofinit_data[id_department][3];
	var upload_extensions = lofinit_data[id_department][4];
	
	changePriority(id_hd_priority);
	showCustomfields(id_department);
	attackment(upload,upload_files,upload_extensions);
}

function changePriority(id_hd_priority){
	$('#id_hd_priority').val(id_hd_priority).attr('selected',true);
}

function showCustomfields(id_department){
	$('.custom-field').css('display','none');
	$('.custom-'+id_department).css('display','block');
}

function attackment(upload,upload_files,upload_extensions){
	if (document.getElementById("lof_files") == null)
		return false;
	document.getElementById('lof_files').innerHTML = '';
	document.getElementById('input_lof_files').value = '';
	if(upload){
		if(upload_files > nb_file || upload_files == 0){
			$('.lofattackment').css('display','block');
			$('b.lof_extension').text(upload_extensions);
			nb_file ++;
		}
	}else{
		$('.lofattackment').css('display','none');
	}
}

function lof_add_attachments() {
	var id_department = $('#id_hd_department').val();
	if(lofinit_data[id_department] === undefined)
		return false;
	var upload = lofinit_data[id_department][2];
	var upload_files = lofinit_data[id_department][3];
	var upload_extensions = lofinit_data[id_department][4];
	if(upload_files > nb_file || upload_files == 0){
		var new_upload = document.createElement('input');
		new_upload.setAttribute('name', 'lof_files[]');
		new_upload.setAttribute('type', 'file');
		document.getElementById('lof_files').appendChild(new_upload);
		
		var new_br = document.createElement('br');
		document.getElementById('lof_files').appendChild(new_br);
		nb_file ++;
	}else{
		alert(message_error);
	}
}

jQuery(document).ready(function(){
	var id_department = $('#id_hd_department').val();
	changeDepartment(id_department);
	$('#id_hd_department').change(function(){
		changeDepartment();
	});
	$('#id_customer').change(function(){
		filterOrder($(this).val());
	});
});
