$(document).ready(function() {
	$(".lof-tab").click(function() {
	   $(".ui-state-default").each(function(){
		 $(this).removeClass("ui-state-active");
	   });               
	   $(this).parent().addClass("ui-state-active");
	   
	   $(".ui-tabs-panel").each(function(){
		 $(this).attr("style","display:none;");
	   });
	   iddiv = $(this).attr("rel");
	   iddiv = iddiv.substr(1);
	   $("#" + iddiv).attr("style",'display:"";');
	   return false;
	});
	
	if(nb_file < mess_data_init[3] || mess_data_init[3] == 0){
		$('.add_attachment').css('display','block');
	}else{
		$('.add_attachment').css('display','none');
	}
	var id_department = mess_data_init[0];
	changeDepartment(id_department);
	$('#id_hd_department').change(function(){
		changeDepartment();
	});
});
function changeDepartment(){
	var id_department = $('#id_hd_department').val();
	
	var upload = mess_data_init[2];
	var upload_files = mess_data_init[3];
	var upload_extensions = mess_data_init[4];
	
	attackment(upload,upload_files,upload_extensions);
}

function attackment(upload,upload_files,upload_extensions){
	if (document.getElementById("lof_files") == null)
		return false;
	document.getElementById('lof_files').innerHTML = '';
	document.getElementById('input_lof_files').value = '';
	if(upload){
		if(upload_files > nb_file || upload_files == 0){
			$('.lofattackment').css('display','table-row');
			$('b.lof_extension').text(upload_extensions);
			nb_file ++;
		}
	}else{
		$('.lofattackment').css('display','none');
	}
}

function lof_add_attachments() {
	var id_department = $('#id_hd_department').val();
	
	var upload = mess_data_init[2];
	var upload_files = mess_data_init[3];
	var upload_extensions = mess_data_init[4];
	
	if(upload_files > nb_file || upload_files == 0){
		var new_upload = document.createElement('input');
		new_upload.setAttribute('name', 'lof_files[]');
		new_upload.setAttribute('type', 'file');
		document.getElementById('lof_files').appendChild(new_upload);
		
		var new_br = document.createElement('br');
		document.getElementById('lof_files').appendChild(new_br);
		nb_file ++;
	}else{
		alert(message_error);
	}
}

function formmessage(){
	$('.lofform').slideDown(500);
	$('a.lofbutton_status').css('display','none');
}

function closeform(){
	$('.lofform').slideUp(500);
	$('a.lofbutton_status').css('display','inline');
}