<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
session_start();

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');

require_once(dirname(__FILE__).'/defines.helpdesk.php');
require_once(dirname(__FILE__).'/helpdesk.php');

require_once( dirname(__FILE__).'/libs/helper.php' );
require_once( dirname(__FILE__).'/classes/HdEmailTemplate.php' );
require_once( dirname(__FILE__).'/classes/HDMail.php' );
require_once( dirname(__FILE__).'/classes/HdParams.php' );
require_once( dirname(__FILE__).'/classes/HdStaff.php' );
require_once( dirname(__FILE__).'/classes/HdTicket.php' );
require_once( dirname(__FILE__).'/classes/HdTicketNotice.php' );
/** security */

if(!defined('_HELPDESK'))
	define('_HELPDESK',1);
	
$params = new HdParams(LOF_MODULE_NAME_HELPDESK);
$helpdesk = new helpdesk();
$errors = array();
global $cookie;

$id_hd_ticket = Tools::getValue('id_hd_ticket');
$ticket = HdTicket::getTickets($cookie->id_lang, $id_hd_ticket);

if($ticket)
	$ticket = $ticket[0];
if(!Validate::isLoadedObject($obj = new HdTicket($id_hd_ticket))){
	echo 'error'; die;
}else{
	$fields = HdCustomFields::listFields($cookie->id_lang, true, $obj->id_hd_department);
	if($fields){
		foreach($fields as &$field){
			$lofvalue = HdCustomFields::getCustomFields($field['id_hd_custom_field'], $id_hd_ticket);
			$value = '';
			if($lofvalue){
				$values = explode('::',$lofvalue[0]);
				foreach($values as &$row)
					$row = trim($row);
				$value = implode(', ',$values);
			}
			$field['lofvalue'] = $value;
		}
	}
	$messages = HdTicketMessage::getTicketMessages( $id_hd_ticket, true, $cookie->id_lang);
	if($messages){
		foreach($messages as &$m){
			if($m['id_employee'])
				$m['objEmp'] = new Employee($m['id_employee']);
			elseif($m['id_customer'])
				$m['objCus'] = new Employee($m['id_customer']);
			$m['files'] = HdTicketMessage::getFiles($m['id_hd_ticket_message']);
		}
	}
	$Topmessage = $messages[(count($messages) - 1)];
	array_pop($messages);
	
	require_once( dirname(__FILE__).'/tmpl/admin/print.php' );
?>
<script type="text/javascript">
	javascript:window.print();
</script>
<?php
}
?>

	