<?php

/**
 * @author Danoosh Miralayee
 * @copyright 2013 presta-shop.ir
 */

if (!defined('_PS_VERSION_'))
	exit;

class PSPCustomForm extends Module
{
    private $_html ='';
    private $_postErrors = array();
    
    public function __construct()
    {
        $this->name = 'pspcustomform' ;
        $this->tab = '' ;
        $this->version = '1.0beta' ;

        
        $this->author = 'Presta-Shop.ir' ;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');

        parent::__construct() ;
        
        $this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('PSP Form') ;
        $this->description = $this->l('Custom form.') ;
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        
    }
	
	public function install()
    {
        if (!parent::install() || !$this->registerHook('header') || !$this->installDB() )
			return false;
        return true; 
    }
	
	public function installDB()
	{
		$query = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."seminar_temp_members` (
			`confirm_code` varchar(65) NOT NULL default '',
			`email` varchar(65) NOT NULL default ''
			) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";
		Db::getInstance()->Execute($query);
		
		$query = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."seminar_members` (
			`id_member` int(10) unsigned NOT NULL auto_increment,
			`email` varchar(65) NOT NULL default '',
			`firstname` varchar(65) NOT NULL default '',
			`lastname` varchar(65) NOT NULL default '',
			`id_gender` tinyint(1) NOT NULL,
			`phone_mobile` varchar(32) default NULL,
			`education` tinyint NOT NULL default 0,
			`location` varchar(65) NOT NULL default '',
			`website` varchar(128),
			`familarity` tinyint(1) unsigned NOT NULL default '0',
			`tours` varchar(65) NOT NULL default '',
			`bestday` tinyint(1) unsigned NOT NULL default '0',
			`date_add` datetime NOT NULL,
			`price` int(10) NOT NULL default '0',
			`paid` tinyint(1) unsigned NOT NULL default '0',
			`ref_id` varchar(65) NOT NULL default '',
			`canceled` tinyint(1) unsigned NOT NULL default '0',
			PRIMARY KEY (`id_member`)
			) ENGINE="._MYSQL_ENGINE_." DEFAULT CHARSET=utf8";
		Db::getInstance()->Execute($query);
		return true;
	}
	
	public function uninstall()
	{
		Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seminar_members`');
		Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seminar_temp_members`');
		parent::uninstall();
		return true;
	}
	
}
