<?php
class PSPCustomFormDefaultModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        //$this->auth = true;
        parent::__construct();
        $this->context = Context::getContext();
    }
    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        //$this->display_column_left = false;
        parent::initContent();
        $this->context->controller->addCSS(_THEME_CSS_DIR_ . 'authentication.css');
        $this->context->controller->addCSS($this->module->getPathUri() . 'views/css/customform.css');
        if (Tools::getValue('step') == 'payment')
            return $this->paymentVerification();
        if (Tools::getValue('step') == 'validate')
            $this->prepayValidation();
        $this->assignForm();
    }
    public function postProcess()
    {
        if (Tools::isSubmit('submitValidate') && Tools::getValue('step') == 'register') {
            $this->context->smarty->assign('confirmed', 1);
            if ($this->validateRegister()) {
                $this->context->cookie->__set('pspform_registred_email', Tools::getValue('email'));
                Db::getInstance()->delete('seminar_temp_members', '`confirm_code` = "' . $this->context->cookie->__get('confirm_code') . '"');
                $this->context->cookie->__unset('confirm_code');
                return Tools::redirect($this->context->link->getModuleLink('pspcustomform', 'default', array('step' => 'validate')));
            }
        } elseif (Tools::isSubmit('submitConfirm') || Tools::getValue('step') == 'register') {
            $this->context->smarty->assign('validated', 1);
            if (Tools::getValue('action') == 'edit')
                $this->editMember();
            elseif (Tools::getValue('confirm_code') && !$temp = $this->confirmCode())
                return $this->errors[] = 'کد فعال سازی اشتباه است';
            elseif ($this->context->cookie->__isset('confirm_code'))
                $temp = Db::getInstance()->getRow('SELECT * from `' . _DB_PREFIX_ . 'seminar_temp_members` WHERE `confirm_code` = "' . $this->context->cookie->__get('confirm_code') . '"');
            elseif (!isset($temp) || empty($temp))
                return $this->errors[] = 'کد فعال سازی یافت نشد';
            $this->context->smarty->assign('confirmed', 1);
            $this->context->smarty->assign('yourmail', $temp['email']);
        } elseif (Tools::isSubmit('submitEmail') && $this->validateEmail(true))
            $this->context->smarty->assign('validated', 1);
    }
    private function validateEmail($confirm = false)
    {
        $email = Tools::getValue('email');
        $member = Db::getInstance()->getRow('SELECT * from `' . _DB_PREFIX_ . 'seminar_members` WHERE `email` = "' . $email . '"');
        if (!empty($member)) {
            $this->context->cookie->__set('pspform_registred_email', Tools::getValue('email'));
            Db::getInstance()->delete('seminar_temp_members', '`confirm_code` = "' . $this->context->cookie->__get('confirm_code') . '"');
            $this->context->cookie->__unset('confirm_code');
            return Tools::redirect($this->context->link->getModuleLink('pspcustomform', 'default', array('step' => 'validate')));
        }
        // if (!(filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email)))
        if (!Validate::isEmail($email)) {
            $this->errors[] = 'ايميل نامعتبر است';
            return false;
        }
        if ($confirm && !$this->sendConfirmation()) {
            $this->errors[] = 'مشکلی در ارسال ایمیل به وجود آمد';
            return false;
        }
        return true;
    }
    private function sendConfirmation()
    {
        $customer = $context->customer;
        $confirm_code = md5(uniqid(rand()));
        $email = Tools::getValue('email');
        $data = array(
            'confirm_code' => $confirm_code,
            'email' => $email,
        );
        $where = '`email` = "' . $email . '"';
        $temp = Db::getInstance()->getRow('SELECT * from `' . _DB_PREFIX_ . 'seminar_temp_members` WHERE `email` = "' . $email . '"');
        if (!empty($temp))
            $save = Db::getInstance()->update('seminar_temp_members', $data, $where, 0, true);
        else

            $save = Db::getInstance()->insert('seminar_temp_members', $data, true);
        if (!$save)
            return false;
        //send Email
        $template = 'confirmation';
        $iso = Language::getIsoById($this->context->language->id);
        $configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_SHOP_NAME'));
        $template_vars = array(
            '{email}' => $email,
            '{confirm_code}' => $confirm_code
        );
        if (!Mail::Send(
            $this->context->language->id,
            $template,
            Mail::l('کارگاه آموزشی پرستاشاپ - تأیید ایمیل'),
            $template_vars,
            $email,
            ($customer->id ? $customer->firstname . ' ' . $customer->lastname : ''),
            $configuration['PS_SHOP_EMAIL'],
            $configuration['PS_SHOP_NAME'],
            null,
            null,
            $this->module->getLocalPath() . 'mails/'
        )
        )
            return true;
        return true;
    }
    private function confirmCode()
    {
        $temp = Db::getInstance()->getRow('SELECT * from `' . _DB_PREFIX_ . 'seminar_temp_members` WHERE `confirm_code` = "' . Tools::getValue('confirm_code') . '"');
        if (empty($temp))
            return false;
        $this->context->cookie->__set('confirm_code', Tools::getValue('confirm_code'));
        return $temp;
    }
    private function validateRegister()
    {
        $firstname = Tools::getValue('firstname');
        $lastname = Tools::getValue('lastname');
        $gender = Tools::getValue('gender');
        $email = Tools::getValue('email');
        $mobile = Tools::getValue('phone_mobile');
        $location = Tools::getValue('address');
        $website = Tools::getValue('website');
        $edu = Tools::getValue('education');
        $familarity = Tools::getValue('prestashop');
        $tours = Tools::getValue('tourdaytime');
        $bestday = Tools::getValue('bestday');
        $nbTours = count($tours);
        if (empty($firstname) || !Validate::isName($firstname))
            $this->errors[] = 'نام صحیح نیست';
        if (empty($lastname) || !Validate::isName($lastname))
            $this->errors[] = 'نام خانوادگی صحیح نیست';
        if (empty($email) || !Validate::isEmail($email))
            $this->errors[] = 'ایمیل صحیح نیست';
        if (empty($mobile) || !preg_match('/^(((\+|00)98)|0)?9[123]\d{8}$/', $mobile))
            $this->errors[] = 'شماره تلفن صحیح نیست';
        if (!empty($location) && !Validate::isAddress($location))
            $this->errors[] = 'محل زندگی صحیح نیست';
        if (!empty($website) && !Validate::isUrl($website))
            $this->errors[] = 'آدرس سایت صحیح نیست';
        if (empty($tours) || $nbTours < 1)
            $this->errors[] = 'شما باید حداقل یک دوره آموزشی را انتخاب کنید';
        if (empty($bestday))
            $this->errors[] = 'بخش امکان حضور را انتخاب کنید';
        if (empty($gender))
            $this->errors[] = 'جنسیت خود را انتخاب کنید';
        if (count($this->errors))
            return false;
        $tourString = implode(",", $tours);
        if ($nbTours > 1) {
            $price = 50000 * (int)$nbTours;
        } else

            $price = 60000;
        $data = array(
            'email' => $email,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'id_gender' => $gender,
            'phone_mobile' => $mobile,
            'location' => $location,
            'website' => $website,
            'education' => $edu,
            'familarity' => $familarity,
            'tours' => $tourString,
            'bestday' => $bestday,
            'date_add' => date("Y-m-d H:i:s"),
            'price' => $price
        );
        $dataArray[] = $data;
        $member = Db::getInstance()->getRow('SELECT * from `' . _DB_PREFIX_ . 'seminar_members` WHERE `email` = "' . $email . '"');
        if (!empty($member)) {
            $where = '`email` = "' . $email . '"';
            $save = Db::getInstance()->update('seminar_members', $data, $where, 0, true);
        } else

            $save = Db::getInstance()->insert('seminar_members', $dataArray, true);
        if (!$save) {
            $this->errors[] = 'خطایی هنگام ذخیره داده ها روی داد';
            return false;
        }
        return true;
    }
    private function editMember()
    {
        $email = $this->context->cookie->__get('pspform_registred_email');
        $member = Db::getInstance()->getRow('SELECT * from `' . _DB_PREFIX_ . 'seminar_members` WHERE `email` = "' . $email . '"');
        if (empty($member))
            return $this->errors[] = 'اطلاعات شما یافت نشد. مجدد تلاش نمایید';
        $member['tours'] = explode(",", $member['tours']);
        $this->context->smarty->assign('member', $member);
    }
    private function prepayValidation()
    {
        $email = $this->context->cookie->__get('pspform_registred_email');
        $member = Db::getInstance()->getRow('SELECT * from `' . _DB_PREFIX_ . 'seminar_members` WHERE `email` = "' . $email . '"');
        if (empty($member))
            return $this->errors[] = 'اطلاعات شما یافت نشد. مجدد تلاش نمایید';
        $member['tours'] = explode(",", $member['tours']);
        $this->context->smarty->assign($member);
    }
    public function paymentVerification()
    {
        $verified = 0;
        $email = $this->context->cookie->__get('pspform_registred_email');
        $member = Db::getInstance()->getRow('SELECT * from `' . _DB_PREFIX_ . 'seminar_members` WHERE `email` = "' . $email . '"');
        if (empty($member)) {
            $this->errors[] = 'اطلاعات شما یافت نشد. مجدد تلاش نمایید یا با مدیر سایت تماس بگیرید.';
            return $this->setTemplate('verify.tpl');
        }
        $merchantID = '4ebe9bf3-b7fc-4843-836f-6cceae8e8897';
        $amount = $member['price'];
        $au = Tools::getValue('au');
        $params = array(
            'merchantID' => $merchantID,
            'au' => $au,
            'amount' => $amount
        );
        $client = new SoapClient('http://www.zarinpal.com/WebserviceGateway/wsdl');
        try {
            $res = $client->PaymentVerification($merchantID, $au, $amount);
        } catch (SoapFault $fault) {
            $this->errors[] = $fault;
        }
        if (isset($res) && $res > 0) {
            $verified = 1;
            $data = array(
                'date_add' => date("Y-m-d H:i:s"),
                'paid' => '1',
                'ref_id' => Tools::getValue('refID')
            );
            $where = '`email` = "' . $email . '"';
            $save = Db::getInstance()->update('seminar_members', $data, $where, 0, true);
            if (!$save)
                $this->errors[] = 'خطا هنگام ذخیره اطلاعات در پایگاه داده. جهت رفع مشکل با مدیر سایت تماس بگیرید';
            $this->context->smarty->assign(array(
                'verified' => $verified,
                'ref' => Tools::getValue('refID')
            ));
            //send Email
            $template = 'verify';
            $iso = Language::getIsoById($this->context->language->id);
            $configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_SHOP_NAME'));
            $template_vars = array(
                '{email}' => $email,
                '{refId}' => Tools::getValue('refID')
            );
            Mail::Send(
                $this->context->language->id,
                $template,
                Mail::l('کارگاه آموزشی پرستاشاپ - سپاسگذاریم'),
                $template_vars,
                $configuration['PS_SHOP_EMAIL'],
                $configuration['PS_SHOP_NAME'],
                $email,
                ($member['firstname'] ? $member['firstname'] . ' ' . $member['lastname'] : ''),
                null,
                null,
                $this->module->getLocalPath() . 'mails/'
            );
            $this->context->cookie->__unset('pspform_registred_email');
        }
        return $this->setTemplate('verify.tpl');
    }
    public function assignForm()
    {
        /* $temp = Db::getInstance()->executeS('SELECT * from `'._DB_PREFIX_.'seminar_temp_members` WHERE `email` = "danooshm@yahoo.com"');

        print_r($temp); */
        $language = $this->context->language;
        $customer = $this->context->customer;
        $step = Tools::getValue('step');
        $this->context->smarty->assign(array(
            'lang_id' => $language->id,
            'step' => $step,
            'customer' => $customer
        ));
        return $this->setTemplate('form.tpl');
    }
}