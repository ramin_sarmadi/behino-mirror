{*

* 2013 Presta-Shop.ir

*

*

*  @author Presta-Shop.ir - Danoosh Miralayi

*  @copyright  2013 Presta-Shop.ir

*}

{capture name=path}{l s='ثبت نام دوره آموزشي پرستاشاپ' mod='pspcustomform' mod='pspcustomform'}{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}



<h2>{l s='فرم ثبت نام'}</h2>



<div class="block-center" id="block-history"></div>



{include file="$tpl_dir./errors.tpl"}

<h4>مرحله پنجم - پایان</h4>

{if isset($verified)}

	<p></p>

	<p>با سپاس! پرداخت شما تأیید شد. از طریق ایمیل و تلفن همراه با شما در ارتباط خواهیم بود.</p>

	<p>کد پیگیری: {$ref}</p>

{else}

	<p>خطایی هنگام تأیید اعتبار پرداخت شما به وجود آمده. مشکل را از طریق مدیر سایت پیگیری کنید</p>



{/if}

