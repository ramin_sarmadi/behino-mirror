{*

* 2013 Presta-Shop.ir

*

*

*  @author Presta-Shop.ir - Danoosh Miralayi

*  @copyright  2013 Presta-Shop.ir

*}

{capture name=path}{l s='ثبت نام دوره آموزشی پرستاشاپ' mod='pspcustomform' mod='pspcustomform'}{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}



<h2>{l s='فرم ثبت نام'}</h2>



<div class="block-center" id="block-history"></div>



{include file="$tpl_dir./errors.tpl"}



{if $step == 'validate'}

	<h4>مرحله چهارم - تأیید اطلاعات و پرداخت</h4>

	<br/>

	<p class="conf confirmation">نکته مهم: چنانچه پرداخت را انجام ندهید اطلاعات شما محفوظ خواهد ماند اما  طبیعی است با توجه به محدودیت در تعداد شرکت کنندگان، اولویت با افرادی است که زودتر از سایرین هزینه را پرداخته اند.</p>

	<p>در صورتی که اطلاعات زیر مورد تأیید شماست روی دکمه پرداخت کلیک کنید</p>

	<p>نام: {$firstname}</p>

	<p>نام خانوادگی: {$lastname}</p>

	<p>جنسیت: {if $id_gender == 1}آقا{else}خانوم{/if} </p>

	<p>ایمیل: {$email}</p>

	<p>شماره موبایل: {$phone_mobile}</p>

	<p>جلسات ثبت نام کرده:

	{foreach from=$tours item="tour"}

	{if $tour.0 == 'A'}مبتدی{else}پیشرفته{/if} - 

	{if $tour.1 == 'A'}هفته اول{else}هفته دوم{/if} - 

	{if $tour.2 == 'A'}روز اول(پنجشنبه){else}روز دوم(جمعه){/if} // 

	{/foreach}

	</p>

	<p>در صورت لغو یکی از تاریخ ها امکان شرکت در {if $bestday == 1}فقط 21 و 22 شهریور{else if $bestday ==2}فقط 28 و 29 شهریور{else} هر دو هفته اعلام شده{/if} را دارم.</p>

	<p>جمع هزینه ثبت نام: {$price}</p>

	{if $paid == 1}

		<p>شما هزینه ثبت نام را پرداخت کرده اید</p>

	{else}

		<form action="https://www.zarinpal.com/webservice/Simplepay" method="post">

			<input type="hidden" value="zp.30025" name="data[Transaction][account_id]">

			<input type="hidden" value="{$price}"  name="data[Transaction][amount]">

			<input type="hidden" value="{$id_member} -  {foreach from=$tours item='tour'}{$tour} - {/foreach}{$email}" name="data[Transaction][desc]">

			<input type="hidden" value="{$link->getModuleLink('pspcustomform', 'default', ['step' => 'payment'])}" name="data[Transaction][redirect_url]">

			<input name="submit" type="submit" class="button" style="font-family: Tahoma;font-size:9pt"  value="پـرداخـت ">



		</form>
	<a href="{$link->getModuleLink('pspcustomform', 'default', ['step' => 'register', 'action' => 'edit'])}" class="button">اصلاح مشخصات</a>

	{/if}{*end of paid if*}

	<br />




{else if isset($confirmed) && $step == 'register'}

	{if isset($smarty.post.confirm_code)}<p class="confirmation">ایمیل شما تأیید شد</p>{/if}

	<p class="required"><sup>*</sup> {l s='فیلدهای ضروری'}</p>



	<form action="{$link->getModuleLink('pspcustomform', 'default', ['step' => 'register'])}" method="post" class="std" id="account-creation_form">

	<h3>{l s='مرحله سوم - ثبت اطلاعات' mod='pspcustomform'}</h3>

		<fieldset>

			<legend>اطلاعات شخصی</legend>

			<p class="required text">

				<label for="email">{l s='ایمیل شما:' mod='pspcustomform'}</label>

				<input type="text" id="email" name="showemail" value="{if isset($yourmail)}{$yourmail}{else if isset($smarty.post.email)}{$smarty.post.email}{else if isset($member)}{$member.email}{/if}" disabled="disable"/>

				<input type="hidden"  name="email" value="{if isset($yourmail)}{$yourmail}{else}{if isset($smarty.post.email)}{$smarty.post.email}{else if isset($member)}{$member.email}{/if}{/if}" />



			</p>

			

			<p class="required text">

				<label for="firstname">{l s='نام' mod='pspcustomform'}</label>

				<input type="text" name="firstname" id="firstname" value="{if isset($smarty.post.firstname)}{$smarty.post.firstname}{else if isset($member)}{$member.firstname}{else}{if isset($customer->firstname)}{$customer->firstname}{/if}{/if}" />

			</p>

			<p class="required text">

				<label for="lastname">{l s='نام خانوادگی' mod='pspcustomform'}</label>

				<input type="text" id="lastname" name="lastname" value="{if isset($smarty.post.lastname)}{$smarty.post.lastname}{else if isset($member)}{$member.lastname}{else}{if isset($customer->lastname)}{$customer->lastname}{/if}{/if}" />

			</p>

			<p class="select">

				<label for="gender">{l s='جنسیت' mod='pspcustomform'} </label>

				<select id="gender" name="gender">

					<option value='0'>- انتخاب -</option>

					<option value='1' {if isset($smarty.post.gender) && $smarty.post.gender == 1}selected{else if isset($member) && $member.id_gender ==1}selected{/if}>آقا</option>

					<option value='2' {if isset($smarty.post.gender) && $smarty.post.gender == 2}selected{else if isset($member) && $member.id_gender ==2}selected{/if}>خانم</option>

				<select>

			</p>

			<p class="required text">

				<label for="phone_mobile">{l s='شماره موبایل (مهم)' mod='pspcustomform'} <sup>*</sup></label>

				<input type="text" id="phone_mobile" name="phone_mobile" value="{if isset($smarty.post.phone_mobile)}{$smarty.post.phone_mobile}{else if isset($member)}{$member.phone_mobile}{/if}" />

			</p>

			<p class=" text">

				<label for="address">{l s='محل سکونت (شهر / منطقه)' mod='pspcustomform'}</label>

				<input type="text" id="address1" name="address1" value="{if isset($smarty.post.address1)}{$smarty.post.address1}{else if isset($member)}{$member.location}{/if}" />

			</p>

			<p class="text">

				<label for="website">{l s='آدرس سایت شما' mod='pspcustomform'} </label>

				<input type="text" id="website" name="website" value="{if isset($smarty.post.website)}{$smarty.post.website}{else}{if isset($member)}{$member.website}{/if}{/if}" />

			</p>

			<p class="select">

				<label for="education">{l s='میزان تحصیلات' mod='pspcustomform'} </label>

				<select id="education" name="education">

					<option value='0'>- انتخاب -</option>

					<option value='1' {if isset($smarty.post.education) && $smarty.post.education == 1}selected{else if isset($member) && $member.education ==1}selected{/if}>قبل از دیپلم</option>

					<option value='2' {if isset($smarty.post.education) && $smarty.post.education == 2}selected{else if isset($member) && $member.education ==2}selected{/if}>دیپلم</option>

					<option value='3' {if isset($smarty.post.education) && $smarty.post.education == 3}selected{else if isset($member) && $member.education ==3}selected{/if}>دانشجو</option>

					<option value='4' {if isset($smarty.post.education) && $smarty.post.education == 4}selected{else if isset($member) && $member.education ==4}selected{/if}>فوق دیپلم</option>

					<option value='5' {if isset($smarty.post.education) && $smarty.post.education == 5}selected{else if isset($member) && $member.education ==5}selected{/if}>لیسانس</option>

					<option value='6' {if isset($smarty.post.education) && $smarty.post.education == 6}selected{else if isset($member) && $member.education ==6}selected{/if}>بالاتر از لیسانس</option>

				<select>

			</p>

			<p class="select">

				<label for="prestashop">{l s='میزان آشنایی با پرستاشاپ' mod='pspcustomform'} </label>

				<select id="prestashop" name="prestashop">

					<option value='0' {if isset($smarty.post.prestashop) && $smarty.post.prestashop == 1}selected{else if isset($member) && $member.familarity ==1}selected{/if}>- انتخاب -</option>

					<option value='1' {if isset($smarty.post.prestashop) && $smarty.post.prestashop == 1}selected{else if isset($member) && $member.familarity ==1}selected{/if}>یک سال یا کمتر</option>

					<option value='2' {if isset($smarty.post.prestashop) && $smarty.post.prestashop == 2}selected{else if isset($member) && $member.familarity ==2}selected{/if}>یک تا دو سال</option>

					<option value='3' {if isset($smarty.post.prestashop) && $smarty.post.prestashop == 3}selected{else if isset($member) && $member.familarity ==3}selected{/if}>دو تا سه سال</option>

					<option value='4' {if isset($smarty.post.prestashop) && $smarty.post.prestashop == 4}selected{else if isset($member) && $member.familarity ==4}selected{/if}>سه تا چهار سال</option>

					<option value='5' {if isset($smarty.post.prestashop) && $smarty.post.prestashop == 5}selected{else if isset($member) && $member.familarity ==5}selected{/if}>چهار سال یا بیشتر</option>

				<select>

			</p>



		</fieldset>

			

		<fieldset>

			<legend>انتخاب کلاس ها</legend>

			<p class="info">نکات مهم</p>

			<p class="info">دو روز 28, 29 شهریور برای ثبت نام در نظر گرفته شده اما ممکن است فقط در یکی از این دو روز کلاس برگزار شود. بنابراین هرچند می توانید در هر دو روز ثبت نام کنید اما با در نظر گرفتن این احتمال بهترین تاریخ را انتخاب کنید.</p>

			<p class="info">در صورتی که ثبت نام در یکی از این دو روز به حد نصاب نرسد آن تاریخ لغو و ثبت نام کنندگان طبق اولویت زمان ثبت نام، امکان حضور در تاریخ ذکر شده دیگر را دارند. همچنین اگر هر دو روز به حدنصاب برسد امکان برگزاری کلاس در هر دو روز خواهد بود</p>

			<p class="info">در صورت لغو یا تغییر تاریخ انتخابی شما با شماره موبایل شما تماس خواهیم گرفت. یک بار دیگر صحت آن را بررسی کنید.<br /></p>

			<p class="info">مطالب آموزشی در دوره های هم نام در روزهای متفاوت یکسان هستند.</p>

			<p class="checkbox">
{*
				<label for="tourdaytime">{l s='در کدام یک از دوره ها قصد شرکت دارید؟' mod='pspcustomform'} </label><br />

				<input type="checkbox" name="tourdaytime[]" value='AAA'> مبتدی - پنجشنبه 21 شهریور 92 - نوبت صبح<br />

				<input type="checkbox" name="tourdaytime[]" value='AAB'> مبتدی - جمعه 22 شهریور 92 - نوبت عصر<br />

				<input type="checkbox" name="tourdaytime[]" value='BAA'> پیشرفته - پنجشنبه 21 شهریور 92 - نوبت عصر<br />

				<input type="checkbox" name="tourdaytime[]" value='BAB'> پیشرفته - جمعه 22 شهریور 92 - نوبت صبح<br />

				<input style="visibility:hidden; width:1px" /> --------------------------------------------------<br />
*}
				<input type="checkbox" name="tourdaytime[]" value='ABA'> مبتدی - پنجشنبه 28 شهریور 92 - نوبت صبح<br />

				<input type="checkbox" name="tourdaytime[]" value='ABB'> مبتدی - جمعه 29 شهریور 92 - نوبت عصر<br />

				<input type="checkbox" name="tourdaytime[]" value='BBA'> پیشرفته - پنجشنبه 28 شهریور 92 - نوبت عصر<br />

				<input type="checkbox" name="tourdaytime[]" value='BBB'> پیشرفته - جمعه 29 شهریور 92 - نوبت صبح<br />

			</p>

			<p class="select">

				<label for="bestday">{l s='امکان شرکت در کدام تاریخ را دارید؟' mod='pspcustomform'} </label>

				<select id="bestday" name="bestday">

					<option value='0'>- انتخاب -</option>

					<option value='1'>28 شهریور</option>

					<option value='2'>29 شهریور</option>

					<option value='3'>هردو تاریخ</option>

				<select>

			</p>

		</fieldset>

		<input type="submit" name="submitValidate" id="submitValidate" value="{l s='مرحله بعد' mod='pspcustomform'}" class="button" />

	</form>

{else if isset($validated) && ($step == 'confirm' || $step == 'register')}

	<form action="{$link->getModuleLink('pspcustomform', 'default', ['step' => 'register'])}" method="post" class="std" id="account-creation_form">

		<fieldset>

			<h3>{l s='مرحله دوم - تأیید ایمیل' mod='pspcustomform'}</h3>

			

			<p class="required text">

				<label for="confirm_code">{l s='کد فعال سازی' mod='pspcustomform'} <sup>*</sup></label>

				<input type="text" id="confirm_code" name="confirm_code" value="{if isset($smarty.post.confirm_code)}{$smarty.post.confirm_code}{/if}" />

			</p>

		</fieldset>

		<input type="submit" name="submitConfirm" id="submitConfirm" value="{l s='مرحله بعد' mod='pspcustomform'}" class="button" />

	</form>

{else}

	<form action="{$link->getModuleLink('pspcustomform', 'default', ['step' => 'confirm'])}" method="post" class="std" id="account-creation_form">

		<fieldset>

			<h3>{l s='مرحله اول - ثبت ایمیل' mod='pspcustomform'}</h3>

			

			<p class="required text">

				<label for="email">{l s='ایمیل خود را وارد کنید' mod='pspcustomform'} <sup>*</sup></label>

				<input type="text" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}" />

			</p>

		</fieldset>

		<input type="submit" name="submitEmail" id="submitEmail" value="{l s='مرحله بعد' mod='pspcustomform'}" class="button" />

	</form>

{/if}

