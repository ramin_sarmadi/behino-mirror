<?php

class HotCompare extends Module
{

    public function __construct()
    {
        $this->name = 'hotcompare';
        $this->tab = 'advertising_marketing';
        $this->version = 1.0;
        $this->author = 'Ahmad Khorshidi';
        $this->need_instance = 0;

        parent::__construct();

        HotCompare::initProcess();

        $this->displayName = $this->l('Manage HotCompare');
        $this->description = $this->l('For Manage HotCompare');
    }

    public function install()
    {
        if (!parent::install()
            //|| !$this->registerHook('header')
            //|| !$this->registerHook('home')
            || $this->_createTables() == false
            )
            return false;

    }

    function uninstall()
    {
        $db = Db::getInstance();
        $query = 'DROP TABLE `'._DB_PREFIX_.'hotcompare`';
        $query1 = 'DROP TABLE `'._DB_PREFIX_.'hotcompare_product`';

        $result = $db->Execute($query);
        $result1 = $db->Execute($query1);

        if ((!parent::uninstall()) /*OR (!$result) OR (!$result1)*/ )
            return false;
        return true;
    }

    public function _createTables()
    {
        $db = Db::getInstance();

        $query = '
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'hotcompare` (
          `id_hotcompare` bigint(10) NOT NULL AUTO_INCREMENT,
          `id_lang` int(4) NOT NULL DEFAULT "1",
          `name` varchar(255) NOT NULL,
          `description` text,
          `date_add` datetime NOT NULL,
          `status` int(1) DEFAULT NULL,
          PRIMARY KEY (`id_hotcompare`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			';

        $query1 = '
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'hotcompare_product` (
          `id_hotcompare_product` bigint(12) NOT NULL AUTO_INCREMENT,
          `id_product` bigint(10) NOT NULL,
          `id_hotcompare` bigint(10) NOT NULL,
          PRIMARY KEY (`id_hotcompare_product`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			';


        $result = $db->Execute($query);
        $result1 = $db->Execute($query1);
/*
        if ((!$result)&&(!$result1))
            return false;
*/

        return true;
    }

    public function initProcess()
    {
        if (Tools::isSubmit('update'.$this->name))
        {
            $action = 'updateData';
            $id_hotcompare=Tools::getValue('id_hotcompare');
            $ResultHotCompareArr = $this->getListContent($this->context->language->id,$id_hotcompare,0);
            $nameAction='update';

        }elseif(Tools::isSubmit('add'.$this->name))
        {
            $nameAction='save';
            $action = 'insert';
        }else{
            $action = 'save';

        }



        $this->context->smarty->assign(array(
         'Action_Form' => 'resultHotCompare'
        ,'LinkSiteName' => 'List Hot Compare'
        ,'IconHotCompareImage' => '../modules/'.$this->name.'/logo-hotcompare.png'
        ,'LinkSave' =>  AdminController::$currentIndex.'&configure='.$this->name.'&'.$action.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')
        ,'LinkBack' =>  AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')
        ,'FormDescription' => $this->displayForm($id_hotcompare)
        //,'ProductArr' => $option_product_list
        //,'FeatureArr' => $FeatureArr
        ));





    }


    public function getContent()
    {

        $id_lang=$this->context->language->id;
        $html = '';

        //return $content;
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');


        if (Tools::isSubmit('save'.$this->name)){

        }
        elseif (Tools::isSubmit('insert'.$this->name))
        {
            $id_product = (Tools::getValue('Product_id'));
            if(count($id_product)>5){
                $html .= $this->displayConfirmation($this->l('<font color="#0000FF">5 more products</font>'));
            }
            $name = (Tools::getValue('name'));
            $description = (Tools::getValue('description'));
            $status = (Tools::getValue('status'));
            if($status){
                $status=(Tools::getValue('status'));
            }else{
                $status=0;
            }

            $date = date("Y-m-d H:i:s");


                            $dataArr = array(
                             'id_lang'=>$id_lang
                            ,'name'=>$name
                            ,'description'=>$description
                            ,'date_add'=>$date
                            ,'status'=>$status
                            );

                            $result_add=$this->add('hotcompare',$this->context->language->id,$dataArr,'id_hotcompare','name="'.$name.'"');

            if(count($id_product)>0)
            {
                $id_products='';
                $count=1;
                foreach ($id_product as $row_id_product) {
                    if($count<6){
                        $id_products .= ','.$row_id_product;
                    }

                $count++;
                }
                $id_products=substr($id_products,1);
                $masterTable='hotcompare';
                $NameIdMaster='id_hotcompare';
                $detailTable='hotcompare_product';
                $result_add=$this->addProductsItems($masterTable,$NameIdMaster,$detailTable ,$id_products);
                if($result_add){
                    $result_add='OK';
                }

            }else{
                $result_add='102';
            }

                if($result_add=='OK'){
                    $html .= $this->displayConfirmation($this->l('Add hot Compare Product record for '.$name.' successfully'));
                }elseif($result_add=='102'){
                    $html .= $this->displayConfirmation($this->l('Add hot Compare Product record for '.$name.' successfully but no relation with product.!!'));
                }else{
                    $html .= $this->displayConfirmation($this->l('<font color="#000000">Add Special Product record for '.$name.' unsuccessfully</font>'));
                }







        }
        elseif(Tools::isSubmit('updateData'.$this->name)){

            $id_product = (Tools::getValue('Product_id'));
            if(count($id_product)>5){
                $html .= $this->displayConfirmation($this->l('<font color="#0000FF">5 more products</font>'));
                $helper = $this->initList();
                return $html.$helper->generateList($this->getListContent($this->context->language->id,0,0), $this->fields_list);
            }
            $id_hotcompare = (Tools::getValue('id_hotcompare'));
            $name = (Tools::getValue('name'));
            $description = (Tools::getValue('description'));

            $date = date("Y-m-d H:i:s");

            $status = (Tools::getValue('status'));
            if($status){
                $status=(Tools::getValue('status'));
            }else{
                $status=0;
            }


            if($id_hotcompare){
                $dataArr = array(
                 'id_lang'=>$id_lang
                ,'name'=>$name
                ,'description'=>$description
                ,'date_add'=>$date
                ,'status'=>$status
                );

                $result_add=$this->update('hotcompare',$this->context->language->id,$dataArr,'id_hotcompare='.$id_hotcompare);

                if(count($id_product)>0)
                {

                    $result_detail = $this->getListContentDetail($this->context->language->id,$id_hotcompare);


                        $result=$this->deleted('hotcompare_product','id_hotcompare='.$id_hotcompare);
                        if($result=='OK'){


                            $id_products='';
                            foreach ($id_product as $row_id_product) {

                                $id_products .= ','.$row_id_product;

                            }
                            $id_products=substr($id_products,1);
                            $masterTable='hotcompare';
                            $NameIdMaster='id_hotcompare';
                            $detailTable='hotcompare_product';
                            $result_add=$this->addProductsItems($masterTable,$NameIdMaster,$detailTable ,$id_products,$id_hotcompare);
                            if($result_add){
                                $result_add='OK';
                            }

                        }else{
                            $result='100';
                        }




                }else{
                    $result_add='102';
                }

                if($result_add=='OK'){
                    $html .= $this->displayConfirmation($this->l('Add hot Compare Product record for '.$name.' successfully'));
                }elseif($result_add=='102'){
                    $html .= $this->displayConfirmation($this->l('Add hot Compare Product record for '.$name.' successfully but no relation with product.!!'));
                }else{
                    $html .= $this->displayConfirmation($this->l('<font color="#000000">Add Special Product record for '.$name.' unsuccessfully</font>'));
                }

            }else{
                $html .= $this->displayConfirmation($this->l('<font color="#000000">Miss data for '.$name.' </font>'));
            }


        }

        if (Tools::isSubmit('update'.$this->name))
        {
            return $this->display(__FILE__, 'hotcompare.tpl');
        }
        elseif(Tools::isSubmit('add'.$this->name)){

            return $this->display(__FILE__, 'hotcompare.tpl');

        }
        elseif (Tools::isSubmit('delete'.$this->name))
        {
            $id_hotcompare = (int)(Tools::getValue('id_hotcompare'));

            $result_detail = $this->getListContentDetail($this->context->language->id,$id_hotcompare);


            if (count($result_detail)>0)
            {
                $result=$this->deleted('hotcompare_product','id_hotcompare='.$id_hotcompare);
                if($result='OK'){
                    $result=$this->deleted('hotcompare','id_hotcompare='.$id_hotcompare);
                }else{
                    $result='100';
                }

            }
            else
            {
                $result=$this->deleted('hotcompare','id_hotcompare='.$id_hotcompare);
            }


            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&action=deleted&result='.$result.'&token='.Tools::getAdminTokenLite('AdminModules'));

        }
        elseif (Tools::getValue('action')=='deleted')
        {

            $result = Tools::getValue('result');

            if($result=='OK')
            {
                $html .= '<div class="conf confirm">'.$this->l('Deleted data from database and details.').'</div>';
            }elseif($result=='100'){

                $html .= '<div class="conf confirm">'.$this->l('Miss Data.').'</div>';


            }else{
                $html .= '<div class="conf confirm">'.$this->l('You have error in action.').'</div>';
            }

            $helper = $this->initList();
            return $html.$helper->generateList($this->getListContent($this->context->language->id,0,1), $this->fields_list);

        }
        else
        {
            $helper = $this->initList();
            return $html.$helper->generateList($this->getListContent($this->context->language->id,0,0), $this->fields_list);

        }



    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_hotcompare' => array(
                'title' => $this->l('Id'),
                'width' => 30,
                'type' => 'text',
            ),
            'name' => array(
                'title' => $this->l('Product Name'),
                'width' => 300,
                'type' => 'text',
                'filter_key' => 'a!prd_name'
            ),
            'date_add_h' => array(
                'title' => $this->l('Date'),
                'width' => 70,
                'type' => 'text',
                'orderby' => true,
                'filter_key' => 'a!date_add'

            ),
            'link' => array(
                'title' => $this->l('Link'),
                'width' => 200,
                'type' => 'link',
                'orderby' => true

            )
        );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'id_hotcompare';
        $helper->actions = array('edit','delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] =  array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new')
        );
        $helper->toolbar_btn['back'] =  array(
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('back')
        );

        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    public function getListContent($id_lang,$id_hotcompare = 0)
    {

        if($id_hotcompare>0)
        {
            $sub_query = ' AND hc.id_hotcompare='.$id_hotcompare;
        }

        $SQL = '

            SELECT
            *
            FROM '._DB_PREFIX_.'hotcompare hc
            WHERE id_lang='.$id_lang.'
            '.$sub_query.'

            ORDER BY id_hotcompare
            ';

        $results =  Db::getInstance()->executeS($SQL);

        foreach ($results as &$result){

            if ($result['date_add']!='0000-00-00 00:00:00')
            {

                $result['date_add_h'] = Pdate::convert_date_M_to_H($result['date_add']);

            }

            $result['link'] = _PS_BASE_URL_.''.__PS_BASE_URI__.'index.php?controller=HotCompare&id_hotcompare='.$result['id_hotcompare'];

        }

        return $results;
    }

    public function getListContentDetail($id_lang,$id_hotcompare = 0,$id_hotcompare_product = 0,$where='')
    {
        if($id_hotcompare>0)
        {
            $sub_query = ' AND hc.id_hotcompare='.$id_hotcompare;
        }elseif($id_hotcompare_product>0){
            $sub_query = ' AND hcp.id_hotcompare_product='.$id_hotcompare_product;
        }

         $SQL = '

            SELECT
            p.id_product,p.ean13,
            p.id_category_default,
            pl.link_rewrite,pl.name product_name,
            hc.id_hotcompare,hc.name,
            hcp.id_hotcompare_product
            FROM '._DB_PREFIX_.'hotcompare hc
            INNER JOIN '._DB_PREFIX_.'hotcompare_product hcp ON hc.id_hotcompare=hcp.id_hotcompare
            INNER JOIN '._DB_PREFIX_.'product p ON hcp.id_product=p.id_product
            INNER JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product
            WHERE hc.id_lang='.$id_lang.'
            '.$sub_query.'
            '.$where.'

			';

        $results =  Db::getInstance()->executeS($SQL);

        foreach ($results as &$result){

            if ((int)$result['id_product']!=0)
            {
                $result['link'] = $this->context->link->getProductLink((int)$result['id_product'], $result['link_rewrite'], $result['category'], $result['ean13']);
            }


        }

        return $results;
    }

    protected function getListProduct($id_lang,$id_product=0)
    {
        if($id_product>0)
        {
            $sub_query = 'AND pl.id_product='.$id_product;
        }

        $SQL = '
			SELECT id_product, name
			FROM `'._DB_PREFIX_.'product_lang` pl
			WHERE pl.`id_lang` = '.(int)$id_lang.' '.$sub_query.' ORDER BY id_product DESC';

        return  Db::getInstance()->executeS($SQL);
    }


    public function addProductsItems($masterTable,$NameIdMaster,$detailTable ,$id_products,$id_hotcompare=0)
    {

        if($id_hotcompare>0){
            $id_master=$id_hotcompare;
        }else{

            $result_master = Db::getInstance()->executeS('
                    SELECT '.$NameIdMaster.'
                    FROM ' . _DB_PREFIX_ . ''.$masterTable.'
                    ORDER BY '.$NameIdMaster.' DESC LIMIT 1
                    '
            );
            $id_master=($result_master[0][$NameIdMaster]);
        }



         $SQL='
			INSERT INTO `' . _DB_PREFIX_ . ''.$detailTable.'` (`'.$NameIdMaster.'`, `id_product`)
			SELECT ' . $id_master . ',p.id_product FROM ' . _DB_PREFIX_ . 'product p
			WHERE p.id_product IN ('.$id_products.')
			';
        return(Db::getInstance()->execute($SQL));

    }

    protected function deleted($table,$WhereDeleted)
    {
        if(($table)&&($WhereDeleted))
        {
        $SQL = '
        DELETE FROM `'._DB_PREFIX_.''.$table.'` WHERE '.$WhereDeleted.'
        ';

            Db::getInstance()->executeS($SQL);

            return 'OK'; //success deleted
        }

        return 100; //miss data

    }

    protected function add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam)
    {


        $SQL = '
			SELECT '.$selectCheckParam.'
			FROM '._DB_PREFIX_.''.$table.'
			WHERE '.$whereCheckParam.' ';

        $result = Db::getInstance()->executeS($SQL);

        if(count($result)==0)
        {


            if(($table)&&($dataArr))
            {
                //print_r($dataArr);
                //Db::getInstance()->insert($table, $dataArr);
                if((Db::getInstance()->insert($table, $dataArr)))
                {
                    return 'OK';
                }else{
                    return false;
                }

            }else{
                return '100'; //Miss data
            }

        }else{

            return '200'; //Duplicate record

        }

    }

    protected function update($table,$id_lang,$dataArr,$where)
    {

            if(($table)&&($dataArr)&&($where))
            {
                if((Db::getInstance()->update($table, $dataArr, $where)))
                {
                    return 'OK';
                }else{
                    return false;
                }

            }else{
                return '100'; //Miss data
            }

    }

    protected function displayForm($id_hotcompare = 0)
    {

        // Get default Language
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $id_lang=$this->context->language->id;
        $languages = Language::getLanguages(false);
        $iso = $this->context->language->iso_code;

        $output = '';
        if (Tools::isSubmit('submitCustWikiMessage'))
        {
            $message_trads = array();
            foreach ($_POST as $key => $value)
                if (preg_match('/custpriv_message_/i', $key))
                {
                    $id_lang = preg_split('/custpriv_message_/i', $key);
                    $message_trads[(int)$id_lang[1]] = $value;
                }
            //Configuration::updateValue('CUSTPRIV_MESSAGE', $message_trads, true);
            $output = '<div class="conf confirm">'.$this->l('Configuration updated').'</div>';
        }
        // Init Fields form array
        $content = '';
        if (version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
        {
            $content .= '
			<script type="text/javascript">
				var iso = \''.(file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en').'\' ;
				var pathCSS = \''._THEME_CSS_DIR_.'\' ;
				var ad = \''.dirname($_SERVER['PHP_SELF']).'\' ;
			</script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
			<script language="javascript" type="text/javascript">
				id_language = Number('.$id_lang_default.');
				tinySetup();
			</script>';
        }
        else
        {
            $content .= '
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript">
				tinyMCE.init({
					mode : "textareas",
					theme : "advanced",
					plugins : "safari,pagebreak,style,layer,table,advimage,advlink,inlinepopups,media,searchreplace,contextmenu,paste,directionality,fullscreen",
					theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
					theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
					theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
					theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
					theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : false,
					content_css : "'.__PS_BASE_URI__.'themes/'._THEME_NAME_.'/css/global.css",
					document_base_url : "'.__PS_BASE_URI__.'",
					width: "600",
					height: "auto",
					font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
					template_external_list_url : "lists/template_list.js",
					external_link_list_url : "lists/link_list.js",
					external_image_list_url : "lists/image_list.js",
					media_external_list_url : "lists/media_list.js",
					elements : "nourlconvert",
					entity_encoding: "raw",
					convert_urls : false,
					language : "'.(file_exists(_PS_ROOT_DIR_.'/js/tinymce/jscripts/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en').'"
				});
				id_language = Number('.$id_lang_default.');
			</script>';
        }

        if($id_hotcompare>0)
        {
            $resultValue=$this->getListContent($id_lang,$id_hotcompare);
            //print_r($resultValue);

            $result_details = $this->getListContentDetail($id_lang,$id_hotcompare);
            $id_products='';
            foreach ($result_details as $row_id_product) {

                $id_products .= ','.$row_id_product['id_product'];

            }
            $id_products=substr($id_products,1);
            $id_products_arr=explode(",",$id_products);


            $options_checkbox_index_root = Db::getInstance()->executeS('
                    SELECT cl.id_category , cl.name name_category
                    FROM `' . _DB_PREFIX_ . 'category_lang` cl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=1 AND c.id_category>2
                    ORDER BY cl.id_category'
            );


            $option_product_list = '';
            foreach ($options_checkbox_index_root as $row_index_root) {

                $option_product_list .= '<div style="width:960px; color:#0099AA;padding:5px; font-size:16px; background-color:#BBBBBB">
                        <span><b>'.$row_index_root['name_category'].': </b></span>
                        ';
                $options_checkbox_index = Db::getInstance()->executeS('
                    SELECT cl.id_category , cl.name name_category
                    FROM `' . _DB_PREFIX_ . 'category_lang` cl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.id_parent='.$row_index_root['id_category'].'
                    ORDER BY cl.id_category'
                );

                $options_checkbox = Db::getInstance()->executeS('
                    SELECT pl.id_product ,cl.id_category, cl.name name_category , pl.name name_product
                    FROM `' . _DB_PREFIX_ . 'product_lang` pl
                    INNER JOIN  `' . _DB_PREFIX_ . 'product` p on (pl.id_product = p.id_product AND p.active=1)
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_product` cp on pl.id_product = cp.id_product
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_lang` cl on cp.id_category = cl.id_category
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=0 AND c.id_category>1 AND c.id_parent='.$row_index_root['id_category'].'
                    ORDER BY cl.id_category'
                );

                foreach ($options_checkbox_index as $row_index) {

                    $option_product_list .= '<div style="width:960px; color:#000000; font-size:12px;  background-color:#CCCCCC">
            <!--<input type="checkbox" value="'.$row_index['id_category'].'" name="id_category[]" />-->
            <span><b>'.$row_index['name_category'].'</b></span>

            ' ;
                    $option_product_list .= '<ul style="width:960px; float:right; color:#000000">';
                    $count=1;
                    foreach ($options_checkbox as $row) {

                        if((array_key_exists(array_search($row['id_product'],$id_products_arr),$id_products_arr)))
                        {
                            $checked='checked="checked"';
                            $style='color:#FF0000;';
                        }
                        else{
                            $checked='';
                            $style='';
                        }

                        if($row['id_category']==$row_index['id_category']){
                            $option_product_list .= '<li style="width:300px; float: left; display:inline; '.$style.'">'.$count.'.
                    <input type="checkbox" value="'.$row['id_product'].'" name="Product_id[]" '.$checked.' />'. $row['name_product'] .'
                    </li>';
                            $count++;
                        }

                    }
                    $option_product_list .='</ul>';

                    $option_product_list .= '</div>';


                }

                $option_product_list .= '</div><hr/>';

            }
        }else{


            $options_checkbox_index_root = Db::getInstance()->executeS('
                    SELECT cl.id_category , cl.name name_category
                    FROM `' . _DB_PREFIX_ . 'category_lang` cl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=1 AND c.id_category>2
                    ORDER BY cl.id_category'
            );


            $option_product_list = '';
            foreach ($options_checkbox_index_root as $row_index_root) {

                $option_product_list .= '<div style="width:960px; color:#0099AA;padding:5px; font-size:16px; background-color:#BBBBBB">
                        <span><b>'.$row_index_root['name_category'].': </b></span>
                        ';
                $options_checkbox_index = Db::getInstance()->executeS('
                    SELECT cl.id_category , cl.name name_category
                    FROM `' . _DB_PREFIX_ . 'category_lang` cl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.id_parent='.$row_index_root['id_category'].'
                    ORDER BY cl.id_category'
                );

                $options_checkbox = Db::getInstance()->executeS('
                    SELECT pl.id_product ,cl.id_category, cl.name name_category , pl.name name_product
                    FROM `' . _DB_PREFIX_ . 'product_lang` pl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_product` cp on pl.id_product = cp.id_product
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_lang` cl on cp.id_category = cl.id_category
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=0 AND c.id_category>1 AND c.id_parent='.$row_index_root['id_category'].'
                    ORDER BY cl.id_category'
                );

                foreach ($options_checkbox_index as $row_index) {

                    $option_product_list .= '<div style="width:960px; color:#000000; font-size:12px;  background-color:#CCCCCC">
            <!--<input type="checkbox" value="'.$row_index['id_category'].'" name="id_category[]" />-->
            <span><b>'.$row_index['name_category'].'</b></span>

            ' ;
                    $option_product_list .= '<ul style="width:960px; float:right; color:#000000">';
                    $count=1;
                    foreach ($options_checkbox as $row) {

                        if($row['id_category']==$row_index['id_category']){
                    $option_product_list .= '<li style="width:300px; float: left; display:inline;">'.$count.'.
                    <input type="checkbox" value="'.$row['id_product'].'" name="Product_id[]" />'. $row['name_product'] .'
                    </li>';
                            $count++;
                        }

                    }
                    $option_product_list .='</ul>';

                    $option_product_list .= '</div>';


                }

                $option_product_list .= '</div><hr/>';

            }

        }

        if (Tools::isSubmit('update'.$this->name))
        {
            $nameAction='Update';
            $action = 'updateData';


        }elseif(Tools::isSubmit('add'.$this->name))
        {
            $nameAction='Save';
            $action = 'insert';

        }else{
            $nameAction = 'Save';
            //$action = 'insert';

        }

        $content .= $output;
        $content .= '
		<fieldset><legend><img src="../modules/'.$this->name.'/logo.gif" /> '.$this->displayName.'</legend>
			<form action="'.AdminController::$currentIndex.'&configure='.$this->name.'&'.$action.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'" method="post">
				<input name="id_hotcompare" value="'.$id_hotcompare.'" type="hidden" />
				<label>'.$this->l('Name').':</label>
				<div class="margin-form">';
            $content .= '
					<div id="ccont" style="display: block ;float: left;">
                        <input id="name" value="'.(isset($resultValue[0]['name']) ? $resultValue[0]['name'] : '').'" name="name" type="text" size="50" />
                    '.$this->l('Status').':
					<input id="status" value="1" name="status" type="checkbox" '.(($resultValue[0]['status'])==1 ? 'checked="checked"' : '').'  />
					</div>
				</div>
    			<div class="clear">&nbsp;</div>
				<label>'.$this->l('Description').':</label>
				<div class="margin-form">';
            $content .= '
					<div id="ccont" style="display: block ;float: left;">
						<textarea class="rte" cols="70" rows="30" id="description" name="description">
						'.(isset($resultValue[0]['description']) ? $resultValue[0]['description'] : '').'
						</textarea>
					</div>';
        $content .= $this->displayFlags($languages, $id_lang_default, 'ccont', 'ccont', true).'
					<div class="clear"></div>
					<p>
						'.$this->l('').'<br />
						'.$this->l('').'
					</p>
				</div>
				<div>
				'.$option_product_list.'
				</div>
				<div class="clear">&nbsp;</div>
				<div class="margin-form">
					<input type="submit" class="button" name="submitCustWikiMessage" value="'.$this->l($nameAction).'" />
				</div>
			</form>
		</fieldset>';

        return $content;
    }

    public function hookHeader($params)
    {
       // $this->context->controller->addCSS($this->_path.'default.css', 'all');

    }

    function hookHome($params)
    {

        $result_details = $this->getListContentDetail($this->context->language->id,0,0,'AND status=1 ORDER BY hc.id_hotcompare DESC');
        foreach($result_details as &$result_detail){
            $result_detail['link_hot']='controller=HotCompare&id_hotcompare='.$result_detail['id_hotcompare'];
        }

        $this->context->smarty->assign(array(
         'Title' => $this->l('Hot Comparison Products ')
        ,'IconHotComparison' => '../modules/'.$this->name.'/logo-hotcomparison.png'
        ,'HotComparisonArr' => $result_details
        ));
        return $this->display(__FILE__, 'resultpage.tpl');

    }


}
