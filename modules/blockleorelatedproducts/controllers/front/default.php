<?php


//init model :
require_once(_PS_MODULE_DIR_ . 'blockleorelatedproducts/blockleorelatedproducts.php');


class BlockLeoRelatedProductsDefaultModuleFrontController extends ModuleFrontController
{

    public $id;
    public $ajax;
    public $object;
    public $lang;
    public $idProduct = 0;

    function __construct()
    {


        parent::__construct();

        $module = new BlockLeoRelatedProducts();
        $this->module = $module;
        $this->ajax = Tools::getValue('ajax');
        $this->id = Tools::getValue('id', 0);
        $this->lang = isset(self::$cookie->id_lang) ? self::$cookie->id_lang : 1;
        $this->object = null;
        $this->idProduct = (int)(Tools::getValue('id_product'));


    }


    public function initContent()
    {
        parent::initContent();
        $this->prepareData();
    }

    /**
     * prepare data
     */
    public function prepareData()
    {

        $category = new CategoryCore();

        $this->lang = isset(self::$cookie->id_lang) ? self::$cookie->id_lang : 1;


        $lang_id = self::$cookie->id_lang;


        switch ($this->id) {

            case '1':
                //getSimularProductsWebservice
                //getAjaxSimularProducts
                $categorySimularProducts = $category->getSimularProductsWebservice($this->idProduct, $lang_id, 1, 12, true, $active = true, $order_by = 'count', $check_access = true);
               /* foreach ($categorySimularProducts as &$row) {

                    //echo ((int)$row['id_product'].'_'. $row['link_rewrite'].'_'. $row['id_category_default'].'_'. $row['ean13'].'<div>');
                    //$row['category'] = Category::getLinkRewrite((int)$row['id_category_default'], (int)$id_lang);
                    $row['link'] = $this->context->link->getProductLink((int)$row['id_product'], $row['link_rewrite'], $row['id_category_default'], $row['ean13']);
                    $row['link_lmg'] = $this->context->link->getImageLink($row['link_rewrite'], $row['id_image'], 'home_allinmart');


                }*/


                if ((strlen($categorySimularProducts) > 0)) {
                    $Query='p.id_product IN ('.$categorySimularProducts.')';
                    $action=true;

                }else{
                    $Query='p.id_product IN (0)';
                }

                $productCount = Search::getAllProductsRowCountWithId((int)$lang_id, $Query,$action);

                $order_by = 'ORDER BY p.id_category_default ';

                $products = Search::getAllProductsRowWithId((int)$lang_id, $order_by, $Query ,$action);

                $this->context->smarty->assign('simularproducts', $products);


                $resultshtml = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'blockleorelatedproducts/simularproducts.tpl');
                //print_r($categorySimularProducts);
                /*Context::getContext()->smarty->assign(array(
                    'simularproducts' => $categorySimularProducts
                ));*/

                //$tdirsimular = _PS_MODULE_DIR_ . 'blockleorelatedproducts/simularproducts.tpl';

                if ($this->idProduct) {
                    //$resulthtml = Context::getContext()->smarty->fetch($tdirsimular);
                    //$resulthtml;
                    die(Tools::jsonEncode(array(
                        'productCount'=>$productCount,
                        'resulthtml' => $resultshtml
                    )));
                }

                break;
            case '2':
                $categorySimularProducts = $category->getSimularProductsInPriceWebservice($this->idProduct, $lang_id, 1, 12, true, $active = true, $order_by = 'A_Price', $check_access = true);
                /*foreach ($categorySimularProducts as &$row) {

                    //echo ((int)$row['id_product'].'_'. $row['link_rewrite'].'_'. $row['id_category_default'].'_'. $row['ean13'].'<div>');
                    //$row['category'] = Category::getLinkRewrite((int)$row['id_category_default'], (int)$id_lang);
                    $row['link'] = $this->context->link->getProductLink((int)$row['id_product'], $row['link_rewrite'], $row['id_category_default'], $row['ean13']);
                    $row['link_lmg'] = $this->context->link->getImageLink($row['link_rewrite'], $row['id_image'], 'home_allinmart');


                }*/
                if ((strlen($categorySimularProducts) > 0)) {
                    $Query='p.id_product IN ('.$categorySimularProducts.')';
                    $action=true;

                }else{
                    $Query='p.id_product IN (0)';
                }

                $productCount = Search::getAllProductsRowCountWithId((int)$lang_id, $Query,$action);

                $order_by = 'ORDER BY p.id_category_default ';

                $products = Search::getAllProductsRowWithId((int)$lang_id, $order_by, $Query ,$action);

                $this->context->smarty->assign('pricesimularproducts', $products);

                $resultshtml = $this->context->smarty->fetch(_PS_MODULE_DIR_ . 'blockleorelatedproducts/pricesimularproducts.tpl');
                //print_r($categorySimularProducts);
               /* Context::getContext()->smarty->assign(array(
                    'simularproducts' => $categorySimularProducts
                ));*/

                //$tdirsimular = _PS_MODULE_DIR_ . 'blockleorelatedproducts/simularproducts.tpl';

                if ($this->idProduct) {
                    //$resulthtml = Context::getContext()->smarty->fetch($tdirsimular);
                    //$resulthtml;
                    die(Tools::jsonEncode(array(
                        'productCount'=>$productCount,
                        'resulthtml' => $resultshtml
                    )));
                }

                break;
            default :
                //do some thing
                break;
        }

        $this->context->smarty->assign('test1', 'tessss');

    }

    /**
     * Function to add some media file e.g : css, js ...
     */
    public function setMedia()
    {
        //parent::setMedia();

        //set global medias :

        //$this->context->controller->addJs(DSS_JS_URI.'dss.js', 'all');
        //$this->context->controller->addCSS(DSS_CSS_URI.'dss.css', 'all');


    }

    /**
     * Display front content :
     */
    /*    public function displayContent() {
            parent::displayContent();
            $this->context->smarty->assign('TEST', 'test');

            //$this->assignBlocks();
            switch ($this->view) {


                case 'searchdss':

                    if ($this->format == 'normal')
                    {
                        $this->context->smarty->display(DSS_ROOT.'SearchDss.tpl');
                    } else {
                        $this->context->smarty->display(DSS_ROOT.'404.tpl');
                    }

                    break;

                default :
                    //do some thing
                break;
            }



        }*/


}
