<?php
include('../../config/config.inc.php');
include('../../init.php');
include('blockleorelatedproducts.php');
$object = new blockleorelatedproducts();

$category = new CategoryCore();
$idProduct=(int)(Tools::getValue('id_product'));

$pricetdirsimular = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/blockleorelatedproducts/pricesimularproducts.tpl';
if($idProduct){
$categorySimularProductsInPrice = $category->getSimularProductsInPrice($idProduct, Context::getContext()->language->id, 1, $nb, true, $active = true, $order_by = 'A_Price', $check_access = true );
}
Context::getContext()->smarty->assign(array(
    'pricesimularproducts' => $categorySimularProductsInPrice
));
if (Tools::getValue('gettpl')) {
    //echo __DIR__ . '\resultpage.tpl';
    $resulthtml = Context::getContext()->smarty->fetch($pricetdirsimular);
    //$resulthtml;
    die(Tools::jsonEncode(array(
        'resulthtml' => $resulthtml
    )));
}
