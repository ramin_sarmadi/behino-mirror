<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsorigin}prestashop>statsorigin_f0b1507c6bdcdefb60a0e6f9b89d4ae8'] = 'مبدا بازدیدکنندگان';
$_MODULE['<{statsorigin}prestashop>statsorigin_009c731e158a4235be55aaeea5c05e49'] = 'نمایش وبسایتی که بازدیدکنندگان از آنجا به سایت شما آمده اند.';
$_MODULE['<{statsorigin}prestashop>statsorigin_14542f5997c4a02d4276da364657f501'] = 'لینک مستقیم';
$_MODULE['<{statsorigin}prestashop>statsorigin_3edf8ca26a1ec14dd6e91dd277ae1de6'] = 'مبدا';
$_MODULE['<{statsorigin}prestashop>statsorigin_d7b5bac28f53a30e9d831b6a7fa2f484'] = 'اینجا درصد ده وبسایت ارجاع دهنده محبوب است بر حسب اینکه کدام بازدیدکننده ها وارد فروشگاه شما می شوند.';
$_MODULE['<{statsorigin}prestashop>statsorigin_998e4c5c80f27dec552e99dfed34889a'] = 'استخراج CSV';
$_MODULE['<{statsorigin}prestashop>statsorigin_96b0141273eabab320119c467cdcaf17'] = 'مجموع';
$_MODULE['<{statsorigin}prestashop>statsorigin_0bebf95ee829c33f34fde535ed4ed100'] = 'فقط لینکهای مستقیم';
$_MODULE['<{statsorigin}prestashop>statsorigin_6602bbeb2956c035fb4cb5e844a4861b'] = 'راهنما';
$_MODULE['<{statsorigin}prestashop>statsorigin_cec998cc46cd200fa97490137de2cc7f'] = 'وبسایت مرجع چیست؟';
$_MODULE['<{statsorigin}prestashop>statsorigin_77de261cf4c31a96146bcf1b52fd9856'] = 'در هنگام بازدید از یک صفحه وب ، معرف صفحه وب قبلی است که شما بازدید کرده اید.';
$_MODULE['<{statsorigin}prestashop>statsorigin_bb49f80b601ed472d433b934244b0afd'] = 'یک  ارجاع دهنده (معرف) به شما این امکان را می دهد که بفهمید کدام کلمات را  در موتورهای جستجو سرچ کرده و وارد سایت شما شده اند. این امکان بهینه سازی  سایت را به شما می دهد.';
$_MODULE['<{statsorigin}prestashop>statsorigin_af19c8da1c414055c960a73d86471119'] = 'یک معرف می تواند باشد یک:';
$_MODULE['<{statsorigin}prestashop>statsorigin_c227be237c874ba6b2f8771d7b66b90e'] = 'کسی که لینک فروشگاه شما را در سایتش پیوند می دهد.';
$_MODULE['<{statsorigin}prestashop>statsorigin_ea87a2280d5cdb638a2727147a3dd85c'] = 'یک همکار که با شما بمنظور جذب مشتری جدید تبادل لینک می کند.';
$_MODULE['<{statsorigin}prestashop>statsorigin_450a7e38e636dd49f5dfb356f96d3996'] = '10 وبسایت اول';
$_MODULE['<{statsorigin}prestashop>statsorigin_52ef9633d88a7480b3a938ff9eaa2a25'] = 'سایر';
