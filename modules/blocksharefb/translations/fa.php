<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocksharefb}prestashop>blocksharefb_cbe5bf6cf027e9f9e6cc0e8d725ebfa6'] = 'بلوک اشتراک در فیسبوک';
$_MODULE['<{blocksharefb}prestashop>blocksharefb_f96f72b5ba39796e728cc55ddd1672cb'] = 'از این بلوک می توانید برای نمایش لینک اشتراک صفحه محصولات در فیس بوک استفاده نمایید';
$_MODULE['<{blocksharefb}prestashop>blocksharefb_353005a70db1e1dac3aadedec7596bd7'] = 'اشتراک در فیسبوک';
