<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_f7c34fc4a48bc683445c1e7bbc245508'] = 'بخش محصولات جدید';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_d3ee346c7f6560faa13622b6fef26f96'] = 'نمایش یک بلوک شامل محصولات جدید افزوده شده.';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_1cd777247f2a6ed79534d4ace72d78ce'] = 'لطفا فیلد \"محصولات نمایشی\" را پر کنید.';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_73293a024e644165e9bf48f270af63a0'] = 'تعداد نامعتبر.';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات به روز رسانی شد';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_f4f70727dc34561dfde1a3c529b6205c'] = 'تنظیمات';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_9f6f59f676f90cf034870f3bcce96188'] = 'محصولات نمایشی';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_3ea7689283770958661c27c37275b89c'] = 'تعداد محصولاتی که باید در این بخش نمایش داده شوند را تعیین کنید';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_41385d2dca40c2a2c7062ed7019a20be'] = 'نمایش دائمی این بخش';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'فعال';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_b9f5c797ebbf55adccdd8539a65a0241'] = 'غیر فعال';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_53d61d1ac0507b1bd8cd99db8d64fb19'] = 'نمایش این بلوک حتی در صورت نبودن محصول.';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_9ff0635f5737513b1a6f559ac2bff745'] = 'محصولات جدید';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_43340e6cc4e88197d57f8d6d5ea50a46'] = 'بیشتر';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_60efcc704ef1456678f77eb9ee20847b'] = 'همه محصولات جدید';
$_MODULE['<{blocknewproducts}prestashop>blocknewproducts_18cc24fb12f89c839ab890f8188febe8'] = 'فعلا محصول جدیدی وجود ندارد';
