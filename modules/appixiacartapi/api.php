<?php

ini_set('display_errors', 'On');
 
include(dirname(__FILE__).'/../../config/config.inc.php');

require_once(dirname(__FILE__).'/engine/Engine.php');
require_once(dirname(__FILE__).'/engine/Helpers.php');
require_once(dirname(__FILE__).'/Helpers.php');
require_once(dirname(__FILE__).'/overrides/Validate.php');

//check if request is valid and come from alinmart verified Device
if (!checkIsValid($_SERVER['_K'], $_SERVER['_D'], $_SERVER['_ID']))
{
	echo ("Oop! it seems you are trying to access restricted area!");
	exit(0);
}
// handle the request
$request = CartAPI_Engine::handleRequest();
if ($request === false) die('ERROR');

CartAPI_Handlers_Helpers::preInit($request['metadata']);
require_once(dirname(__FILE__).'/../../init.php');
CartAPI_Handlers_Helpers::setServerNotices();

// mark as an appixia mobile endpoint
// TODO: add some validation of user agent and such, or maybe move this line to the app itself alltogether (maybe should originate from the server at all)
CartAPI_Handlers_Helpers::setAppixiaMobileEngine();

// define all supported operations
$request_router = array
(
	'GetSingleItem' => 'Items',
	'GetItemList' => 'Items',
	'GetCategoryList' => 'Categories',
	'BuyerLogin' => 'Login',
	'BuyerRegister' => 'Login',
	'GetOrderUpdate' => 'Order',
	'GetShippingMethods' => 'Order',
	'GetPaymentMethods' => 'Order',
    'GetHomeItems'=>'Items',
    'GetSimular'=>'Items',
    'Search'=>'Items',
    'AdvanceSearch'=>'Items',
    'MailAlert'=>'Login',
    'Comparison'=>'Login',
    'GetDiscount'=>'Login',
    'GetHistoryOrder'=>'Login',
    'GetHistoryOrderDetails'=>'Login',
    'AddRate'=>'Login',
    'GetRate'=>'Login',
    'GetComment'=>'Login',
    'Address'=>'Login',
    'GetCountry'=>'Login',
    'GetState'=>'Login',
    'GetCriterionRate'=>'Items',
    'GetProductDetail'=>'Items',
	'AddToFavorite'=>'Login',
    'GetUserInfo'=>'Login',
    'GetFav'=>'Login'
);
// find the correct operation handler
$operation = $request['metadata']['X-OPERATION'];
$func_name = 'Handle_'.$operation;
$handler = $request_router[$operation];
$handler_filename = $handler . '.php';
$class_name = 'CartAPI_Handlers_'.$handler;

// load the correct file
if (!file_exists(dirname(__FILE__).'/overrides/'.$handler_filename)) 
{
	// load the base
	require_once(dirname(__FILE__).'/'.$handler_filename);
}
else 
{
	// load the override
	$class_name = 'CartAPI_Handlers_Override_'.$handler;
	require_once(dirname(__FILE__).'/overrides/'.$handler_filename);
}

// init the class
if (!class_exists($class_name, false)) CartAPI_Helpers::dieOnError($request['encoder'], 'UnsupportedOperation', $operation.' not supported');
$handler_instance = new $class_name();

// call the operation handler
$handler_instance->{$func_name}($request['metadata'], $request['data'], $request['encoder']);

// call exit explicitly in case we have PHP auto appended files which can corrupt our XML response
// see http://stackoverflow.com/questions/2268868/webhoster-inserts-a-javascript-which-brokes-my-code-how-to-remove-it
exit();

?>