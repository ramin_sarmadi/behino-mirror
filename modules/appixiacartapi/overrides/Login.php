<?php

// this is an override class related to buyer login and register
// if you need to customize the module to your needs, make all changes here
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Login.php');
include(dirname(__FILE__) . '/../../../modules/favoriteproducts/FavoriteProduct.php');


class UserInfo
{
    public $FName = "";
    public $LName = "";
    public $Email = "";
    public $BirthDay = "";
    public $Phone = 0;


}

class CartAPI_Handlers_Override_Login extends CartAPI_Handlers_Login
{
    // 	override any functions you want to change (from the core Login.php) here

    public function Handle_GetUserInfo($metadata, $request, $encoder)
    {

        global $cookie;
        //echo "cookie is $cookie->id_customer";
        $customer = new Customer((int)($cookie->id_customer));
        //echo ("user id is : $customer->id");
        $userInfo = new UserInfo();
        $userInfo->FName = $customer->firstname;
        $userInfo->LName = $customer->lastname;
        $userInfo->Email = $customer->email;
        $userInfo->BirthDay = $customer->birthday;
        //fix phone!
        die(Tools::jsonEncode($userInfo));
        //echo json_encode($userInfo);

    }

    public function Handle_AddToFavorite($metadata, $request, $encoder)
    {
        global $cookie;
        if (!$cookie) {
            //echo "false";
            return false;
        }
        //echo "$request['pId']";
        $product_id = $request['pId'];
        // check if product exists

        $customer_id = ((int)($cookie->id_customer));
        $SQL='SELECT id_product FROM '._DB_PREFIX_.'product WHERE id_product='.$product_id.' AND active=1';
        $resPid=Db::getInstance()->getValue($SQL);

        $SQL='SELECT id_customer FROM '._DB_PREFIX_.'customer WHERE id_customer='.$customer_id.' AND active=1';
        $resCid=Db::getInstance()->getValue($SQL);


        if ( (!((int)$resCid)) || (!((int)$resPid)) ) {
            //echo "false";
            return false;
            exit();
        }

        $favorite_product = new FavoriteProduct();

        $favorite_product->id_product = $product_id;
        $favorite_product->id_customer = (int)$cookie->id_customer;
        $favorite_product->id_shop = (int)Context::getContext()->shop->id;
        if ($favorite_product->add())
        {
            return true;
        }else{
            return false;
        }

    }

    public function Handle_GetFav($metadata, $request, $encoder)
    {
        global $cookie;
        if (!$cookie) {
            //echo "false";
            return false;
        }

        $customer_id = ((int)($cookie->id_customer));
        $id_lang=$cookie->id_lang;

        $id_shop = (int)Context::getContext()->shop->id;

        $SQL='SELECT id_customer FROM '._DB_PREFIX_.'customer WHERE id_customer='.$customer_id.' AND active=1';
        $resCid=Db::getInstance()->getValue($SQL);


        if ( (!((int)$resCid)) ) {
            //echo "false";
            return false;
            exit();
        }

        $favorite_product = new FavoriteProduct();
        $Fav_C=$favorite_product->getFavoriteProducts($customer_id,$id_lang);

        //print_r($Fav_C);
        foreach ($Fav_C as &$Fav){

            $SQL='SELECT COUNT(id_favorite_product) FROM '._DB_PREFIX_.'favorite_product WHERE id_customer='.$customer_id.'
             AND id_product='.$Fav['id_product'].' AND id_shop='.$id_shop;
            $resCount=Db::getInstance()->getValue($SQL);

            $Fav['CountFav']=(int)$resCount;

        }

        $encoder->render($Fav_C);


    }

    public function Handle_AddRate($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid';
            //return false;
        }

        $customer_id = ((int)($cookie->id_customer));
        $id_guest = ((int)($cookie->id_guest));
        $id_lang=$cookie->id_lang;

        $id_shop = (int)Context::getContext()->shop->id;
        $data = json_decode(file_get_contents('php://input'), true);
        $title = $data['title'];
        $content = $data['content'];
        /*
        $title = $request['title'];
        $content = $request['content'];
        */
        $SQL='SELECT id_customer FROM '._DB_PREFIX_.'customer WHERE id_customer='.$customer_id.' AND active=1';
        $resCid=Db::getInstance()->getValue($SQL);

        if((($title!='')||($content!='')) && (!((int)$resCid)) ){
            $errors[] = 'Customer is not login for add comment!';
        }
        elseif( (!((int)$resCid)))
        {
            $msg[] = 'Customer is not login or valid';
            //return false;
            //exit();
        }

        $id_product = $request['pId'];
        $SQL='SELECT id_product FROM '._DB_PREFIX_.'product WHERE id_product='.$id_product.' AND active=1';
        $resPid=Db::getInstance()->getValue($SQL);

        if ((!(int)$resPid) ) {
            $errors[] = 'Product is not valid';
        }

        $SQL_C = '
			SELECT pccl.`id_product_comment_criterion`
			FROM `'._DB_PREFIX_.'product_comment_criterion` pcc
			INNER JOIN `'._DB_PREFIX_.'product_comment_criterion_lang` pccl ON pccl.`id_product_comment_criterion` = pcc.`id_product_comment_criterion`
			WHERE
			pcc.`id_product_comment_criterion_type` = 1
			AND
			pccl.`id_lang` = '.(int)$id_lang.'

			ORDER BY pccl.`id_product_comment_criterion`
			';
        $resCriid=Db::getInstance()->executeS($SQL_C);


        if ( (count($errors)>0)||(count($resCriid)==0) ) {
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept insert

            $id_product_comment_criterion='';
            $sumGrade=0;
            foreach ($resCriid as $rCrid){

                $id_product_comment_criterion=$rCrid['id_product_comment_criterion'];
                $sumGrade = $sumGrade+$request['CrId_'.$id_product_comment_criterion];

            }

            $CountGrade=count($resCriid);
            $AvgGrade=$sumGrade/$CountGrade;

            if($title!=''){
                $validate=0;
            }else{
                $validate=2;
            }

            if($content==''){
                $content="";
            }

            $FName=($cookie->customer_firstname);
            $LName=($cookie->customer_lastname);
            $date_add = date("Y-m-d H:i:s");

            $table='product_comment';
            $dataArr = array(
                'id_product' => $id_product,
                'id_customer' => $customer_id,
                'id_guest' => $id_guest,
                'title' => $title,
                'content' => $content,
                'customer_name' => $FName.' '.$LName,
                'grade' => $AvgGrade,
                'validate' => $validate,
                'deleted' => 0,
                'date_add' => $date_add
            );

            if((($title!='')||($content!='')) && (((int)$resCid)) ){
                $if_Where='(title="'.$title.'" AND content="'.$content.'" )';
            }else{
                $if_Where=' validate=2 ';
            }

            $selectCheckParam='id_product_comment';
            if($customer_id){
                $whereCheckParam='id_product='.$id_product.' AND id_customer='.$customer_id.' AND ( '.$if_Where.' ) ';
            }else{
                $whereCheckParam='id_product='.$id_product.' AND id_guest='.$id_guest.' AND ( '.$if_Where.' ) ';
            }

            $resAdd=$this->add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam);
            if($resAdd=='OK')
            {
                $msg[] = 'Insert Data successful.';

                $SQL='SELECT id_product_comment FROM '._DB_PREFIX_.'product_comment WHERE id_product='.$id_product.'
                 AND id_customer='.$customer_id.' ORDER BY id_product_comment DESC';
                $id_product_comment=Db::getInstance()->getValue($SQL);

                foreach ($resCriid as $rCrid){

                    $id_product_comment_criterion=$rCrid['id_product_comment_criterion'];
                    $grade = $request['CrId_'.$id_product_comment_criterion];

                    $table_D='product_comment_grade';
                    $dataArrD = array(
                        'id_product_comment' => $id_product_comment,
                        'id_product_comment_criterion' => $id_product_comment_criterion,
                        'grade' => $grade
                    );

                    if((Db::getInstance()->insert($table_D, $dataArrD)))
                    {
                        $msg[] = 'Insert detail Data successful.';
                    }else{
                        $msg[] = 'Insert detail Data unsuccessful.';
                    }

                }


            }else{

                $msg[] = 'Insert Data unsuccessful.';

                if($resAdd=='100')
                {
                    $msg[] = 'Miss Date!';
                }elseif($resAdd='200'){
                    $msg[] = 'Duplicate record!';
                }

            }



        }


        die(Tools::jsonEncode(array(
            //'result' => $id_product_comment_criterion,
            'errors' => $errors,
            'msg' => $msg
        )));


    }

    public function Handle_GetRate($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid';
            //return false;
        }

        $customer_id = ((int)($cookie->id_customer));
        $id_guest = ((int)($cookie->id_guest));
        $id_lang=$cookie->id_lang;

        $id_shop = (int)Context::getContext()->shop->id;

        $id_product = $request['pId'];
        $SQL='SELECT id_product FROM '._DB_PREFIX_.'product WHERE id_product='.$id_product.' AND active=1';
        $resPid=Db::getInstance()->getValue($SQL);

        if ((!(int)$resPid) ) {
            $errors[] = 'Product is not valid';
        }

        $resAvgRate=0;

        $SQL_C = '
			SELECT AVG(pc.`grade`)
			FROM `'._DB_PREFIX_.'product_comment` pc
			WHERE
			pc.`deleted` = 0
			AND
			pc.validate IN (1,2)
			AND
			pc.`id_product` = '.(int)$resPid.'

			';
        $resAvgRate=Db::getInstance()->getValue($SQL_C);

        $resTotalCountRate=0;

        $SQL_TC = '
			SELECT COUNT(pc.`id_product`)
			FROM `'._DB_PREFIX_.'product_comment` pc
			WHERE
			pc.`deleted` = 0
			AND
			pc.validate IN (1,2)
			AND
			pc.`id_product` = '.(int)$resPid.'

			';
        $resTotalCountRate=Db::getInstance()->getValue($SQL_TC);



        if ( (count($errors)>0)) {
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept

            $resAvgRate=number_format((float)$resAvgRate,1);


        }


        die(Tools::jsonEncode(array(
            'resultAvg' => $resAvgRate,
            'resultTotalCountRate' => $resTotalCountRate,
            'errors' => $errors,
            'msg' => $msg
        )));


    }

    public function Handle_GetComment($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid';
            //return false;
        }

        $customer_id = ((int)($cookie->id_customer));
        $id_guest = ((int)($cookie->id_guest));
        $id_lang = $cookie->id_lang;

        $id_shop = (int)Context::getContext()->shop->id;
        $data = json_decode(file_get_contents('php://input'), true);
        $title = $data['title'];
        $content = $data['content'];

        $id_product = $request['pId'];
        $SQL='SELECT id_product FROM '._DB_PREFIX_.'product WHERE id_product='.$id_product.' AND active=1';
        $resPid=Db::getInstance()->getValue($SQL);

        if ((!(int)$resPid) ) {
            $errors[] = 'Product is not valid';
        }

        $SQL_C = '
			SELECT *
			FROM `'._DB_PREFIX_.'product_comment` pc
			WHERE
			pc.`deleted` = 0
			AND
			pc.validate = 1
			AND
			pc.`id_product` = '.(int)$resPid.'

			';
        $resComment=Db::getInstance()->executeS($SQL_C);


        if ( (count($errors)>0) ){
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept


        }

        /*
                die(Tools::jsonEncode(array(
                    'result' => $resComment,
                    'errors' => $errors,
                    'msg' => $msg
                )));
        */

        $encoder->render($resComment);

    }

    public function Handle_Address($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid';
            //return false;
        }

        $action=$request['action'];

        $id_customer = ((int)($cookie->id_customer));
        if ((!(int)$id_customer) ) {
            $errors[] = 'Login Please...';
        }
        $id_lang = $cookie->id_lang;


        if(($action=="add")||($action=="update")){

            $data = json_decode(file_get_contents('php://input'), true);
            $fname = $data['fname'];
            if (($fname=="")) {
                $errors[] = 'please type first name...';
            }

            $lname = $data['lname'];
            if (($lname=="")) {
                $errors[] = 'please type last name...';
            }

            $id_country = (int)$data['id_country'];

            $SQL='SELECT id_country FROM '._DB_PREFIX_.'country_lang WHERE id_country='.$id_country.'';
            $resCid=Db::getInstance()->getValue($SQL);
            if ((!(int)$resCid) ) {
                $errors[] = 'Country is not valid';
            }

            $id_state = (int)$data['id_state'];

            $SQL='SELECT id_state FROM '._DB_PREFIX_.'state WHERE id_state='.$id_state.'';
            $resSid=Db::getInstance()->getValue($SQL);
            if ((!(int)$resSid) ) {
                $errors[] = 'State is not valid';
            }

            $alias = $data['alias'];
            if (($alias=="")) {
                $errors[] = 'please add alias name...';
            }

            $company = $data['company'];
            $address1 = $data['address1'];
            $address2 = $data['address2'];
            if (($address1=="")&&($address2=="")) {
                $errors[] = 'please add address...';
            }

            $postcode = (int)$data['postcode'];
            if ((!$postcode)) {
                $errors[] = 'please add post code...';
            }

            $city = $data['city'];
            if (($city=="")) {
                $errors[] = 'please type city name...';
            }

            $other = $data['other'];
            $phone= (int)$data['phone'];
            $phone_mobile = (int)$data['phone_mobile'];
            if ((!$phone) && (!$phone_mobile) ) {
                $errors[] = 'You need add one phone...';
            }

            $id_address=(int)$data['id_address'];
            if($action=="add"){
                $date_add = date("Y-m-d H:i:s");
                $active = 1;
                $deleted = 0;


            }else{
                if(!$id_address){
                    $errors[] = 'Miss data for update your address...';
                }
                $date_upd = date("Y-m-d H:i:s");
                $active = 1;
                $deleted = 0;
            }


        }
        elseif($action=="del")
        {

            $data = json_decode(file_get_contents('php://input'), true);
            $id_address=(int)$data['id_address'];
            if(!$id_address){
                $errors[] = 'Miss data for delete your address...';
            }
            $date_upd = date("Y-m-d H:i:s");
            $active = 0;
            $deleted = 1;
        }
        elseif($action=="get")
        {

            $id_address=(int)$request['id_address'];
            if(!$id_address){
                $msg[] = 'return all data for your addresses...';
            }


        }else{
            $errors[] = 'Please set action...';
        }


        if ( (count($errors)>0) ){
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept

            if($action=="add")
            {
                $table="address";
                $dataArr=array(
                    'id_country'=>$id_country,
                    'id_state'=>$id_state,
                    'id_customer'=>$id_customer,
                    'id_manufacturer'=>0,
                    'id_supplier'=>0,
                    'id_warehouse'=>0,
                    'alias'=>$alias,
                    'company'=>$company,
                    'lastname'=>$lname,
                    'firstname'=>$fname,
                    'address1'=>$address1,
                    'address2'=>$address2,
                    'postcode'=>$postcode,
                    'city'=>$city,
                    'other'=>$other,
                    'phone'=>$phone,
                    'phone_mobile'=>$phone_mobile,
                    'vat_number'=>0,
                    'dni'=>0,
                    'date_add'=>$date_add,
                    'date_upd'=>$date_upd,
                    'active'=>$active,
                    'deleted'=>$deleted
                );
                $selectCheckParam="id_address";
                $whereCheckParam="firstname='".$fname."' AND lastname='".$lname."' AND alias='".$alias."'";
                $resAdd=$this->add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam);
                if($resAdd=='OK')
                {
                    $msg[] = 'Insert Data successful.';
                }
                elseif($resAdd=="100")
                {

                    $msg[] = 'Miss Data.';

                }
                elseif($resAdd=="200")
                {

                    $msg[] = 'Duplicate Data.';

                }
                else
                {
                    $msg[] = 'Insert Data unsuccessful.';
                }

                $Array=array_merge(array(
                    'result' => $resAdd,
                    'errors' => $errors,
                    'msg' => $msg
                ));
            }
            elseif($action=="update")
            {

                $table="address";
                $dataArr=array(
                    'id_country'=>$id_country,
                    'id_state'=>$id_state,
                    'id_customer'=>$id_customer,
                    'id_manufacturer'=>0,
                    'id_supplier'=>0,
                    'id_warehouse'=>0,
                    'alias'=>$alias,
                    'company'=>$company,
                    'lastname'=>$lname,
                    'firstname'=>$fname,
                    'address1'=>$address1,
                    'address2'=>$address2,
                    'postcode'=>$postcode,
                    'city'=>$city,
                    'other'=>$other,
                    'phone'=>$phone,
                    'phone_mobile'=>$phone_mobile,
                    'vat_number'=>0,
                    'dni'=>0,
                    'date_add'=>$date_add,
                    'date_upd'=>$date_upd,
                    'active'=>$active,
                    'deleted'=>$deleted
                );
                $selectCheckParam="id_address";
                $whereUpdate="id_address=".$id_address." AND active=1 AND deleted=0 AND id_customer=".$id_customer;
                $resAdd=$this->update($table,$id_lang,$dataArr,$selectCheckParam,$whereUpdate);
                if($resAdd=='OK')
                {
                    $msg[] = 'Update Data successful.';
                }
                elseif($resAdd=="100")
                {

                    $msg[] = 'Miss Data.';

                }
                elseif($resAdd=="201")
                {

                    $msg[] = 'Not Find Record.';

                }
                else
                {
                    $msg[] = 'Update Data unsuccessful.';
                }

                $Array=array_merge(array(
                    'result' => $resAdd,
                    'errors' => $errors,
                    'msg' => $msg
                ));
            }
            elseif($action=="del")
            {

                $table="address";
                $dataArr=array(
                    'date_upd'=>$date_upd,
                    'active'=>$active,
                    'deleted'=>$deleted
                );
                $selectCheckParam="id_address";
                $whereUpdate="id_address=".$id_address." AND active=1 AND deleted=0 AND id_customer=".$id_customer;
                $resAdd=$this->update($table,$id_lang,$dataArr,$selectCheckParam,$whereUpdate);
                if($resAdd=='OK')
                {
                    $msg[] = 'Delete Data successful.';
                }
                elseif($resAdd=="100")
                {

                    $msg[] = 'Miss Data.';

                }
                elseif($resAdd=="201")
                {

                    $msg[] = 'Not Find Record.';

                }
                else
                {
                    $msg[] = 'Delete Data unsuccessful.';
                }

                $Array=array_merge(array(
                    'result' => $resAdd,
                    'errors' => $errors,
                    'msg' => $msg
                ));


            }
            elseif($action=="get")
            {

                if($id_address){
                    $Q='AND a.id_address='.$id_address;
                }
            $SQL_A = '
			SELECT *
			FROM `'._DB_PREFIX_.'address` a
			WHERE
			a.`deleted` = 0
			AND
			a.active = 1
			AND
			a.id_customer='.$id_customer.'
			'.$Q.'

			';
                $resAddress=Db::getInstance()->executeS($SQL_A);

                $Array=array_merge(array(
                    'result' => $resAddress,
                    'errors' => $errors,
                    'msg' => $msg
                ));

            }

            $encoder->render($Array);

        }

        $Array=array_merge(array(
            'result' => $action,
            'errors' => $errors,
            'msg' => $msg
        ));


        $encoder->render($Array);

    }

    public function Handle_GetCountry($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid';
            //return false;
        }

        $customer_id = ((int)($cookie->id_customer));
        $id_guest = ((int)($cookie->id_guest));
        $id_lang = $cookie->id_lang;

        $id_country=$request['id_country'];

        if((int)$id_country){
            $Q='AND cl.id_country='.$id_country;
        }


        $SQL_C = '
			SELECT *
			FROM `'._DB_PREFIX_.'country_lang` cl
			WHERE
			cl.`id_lang` = '.$id_lang.'
			'.$Q.'

			';
        $resCountry=Db::getInstance()->executeS($SQL_C);


        if ( (count($errors)>0) ){
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept


        }

        $Array=array_merge(array(
            'result' => $resCountry,
            'errors' => $errors,
            'msg' => $msg
        ));


        $encoder->render($Array);

    }

    public function Handle_GetState($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid';
            //return false;
        }

        $customer_id = ((int)($cookie->id_customer));
        $id_guest = ((int)($cookie->id_guest));
        $id_lang = $cookie->id_lang;

        $id_country=$request['id_country'];

        if(!(int)$id_country){
            $errors[] = 'Country can not Null';
        }

        $SQL='SELECT id_country FROM '._DB_PREFIX_.'country_lang WHERE id_country='.(int)$id_country.'';
        $resCid=Db::getInstance()->getValue($SQL);

        if ((!(int)$resCid) ) {
            $errors[] = 'Country is not valid';
        }



        if ( (count($errors)>0) ){
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept

            $SQL_S = '
			SELECT *
			FROM `'._DB_PREFIX_.'state` s
			WHERE
			s.`active` = 1
			AND
			s.`id_country` = '.(int)$id_country.'

			';
            $resState=Db::getInstance()->executeS($SQL_S);

        }


        $Array=array_merge(array(
            'result' => $resState,
            'errors' => $errors,
            'msg' => $msg
        ));


        $encoder->render($Array);

    }

    public function Handle_GetDiscount($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid.';
            //return false;
        }

        $customer_id = ((int)($cookie->id_customer));
        $id_lang = $cookie->id_lang;

        $SQL='SELECT id_customer FROM '._DB_PREFIX_.'customer WHERE id_customer='.(int)$customer_id.'';
        $resCid=Db::getInstance()->getValue($SQL);

        if ((!(int)$resCid) ) {
            $errors[] = 'Customer is not valid.';
        }

        $SQL='SELECT id_group FROM '._DB_PREFIX_.'customer_group WHERE id_customer='.(int)$customer_id.'';
        $resGid=Db::getInstance()->executeS($SQL);

        if ((!count($resGid)) ) {
            $errors[] = 'This customer have not group!';
        }else{

            $id_groups='';
            foreach($resGid as $Group)
            {

                $id_groups .= ','.$Group['id_group'];

            }

            $id_groups=substr($id_groups,1);

        }


        if ( (count($errors)>0) ){
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept

            $SQL = '
			SELECT crl.`name` AS rule_name, cr.`id_cart_rule`, cr.`id_customer`,  cr.`date_from`
            , cr.`date_to`, cr.`description`, cr.`quantity`
            , cr.`quantity_per_user`, cr.`code`, cr.`minimum_amount`, cr.`reduction_amount`
            , cr.`reduction_percent`, cr.`reduction_currency`, cr.`active` , gl.`name` AS group_name
            FROM `'._DB_PREFIX_.'cart_rule` cr
            INNER JOIN `'._DB_PREFIX_.'cart_rule_lang` crl ON crl.`id_cart_rule`=cr.`id_cart_rule`
            LEFT JOIN `'._DB_PREFIX_.'cart_rule_group` crg ON crg.`id_cart_rule`=cr.`id_cart_rule`
            LEFT JOIN `'._DB_PREFIX_.'group_lang` gl ON crg.`id_group`=gl.`id_group`
            WHERE gl.id_lang='.$id_lang.'
            AND (crg.id_group IN ('.$id_groups.') OR cr.id_customer='.$resCid.')

			';
            $resRule=Db::getInstance()->executeS($SQL);

            foreach($resRule as &$resultR)
            {

                $date_f_arr=explode(" ",$resultR['date_from']);
                $date_t_arr=explode(" ",$resultR['date_to']);

                if ($date_f_arr[0]!='0000-00-00')
                {

                    $resultR['date_from_h'] = Pdate::convert_date_M_to_H($date_f_arr[0]);

                }
                if ($date_t_arr[0]!='0000-00-00')
                {

                    $resultR['date_to_h'] = Pdate::convert_date_M_to_H($date_t_arr[0]);

                }

            }

        }


        $Array=array_merge(array(
            'result' => $resRule,
            'errors' => $errors,
            'msg' => $msg
        ));


        $encoder->render($Array);

    }

    public function Handle_GetHistoryOrder($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid.';
            //return false;
        }

        $customer_id = ((int)($cookie->id_customer));
        $id_lang = $cookie->id_lang;
        $id_shop = (int)Context::getContext()->shop->id;

        $SQL='SELECT id_customer FROM '._DB_PREFIX_.'customer WHERE id_customer='.(int)$customer_id.'';
        $resCid=Db::getInstance()->getValue($SQL);

        if ((!(int)$resCid) ) {
            $errors[] = 'Customer is not valid.';
        }


        if ( (count($errors)>0) ){
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept

            $SQL = '
			SELECT o.`id_order`,o.`reference` , o.`payment`, osl.`name` , o.`gift`, o.`gift_message`, o.`total_discounts`
            , o.`total_discounts_tax_excl`, o.`total_discounts_tax_incl`, o.`total_paid` , o.`total_paid_real`
            , o.`total_paid_tax_excl`, o.`total_paid_tax_incl`, o.`total_products`, o.`total_products_wt`
            ,o.`invoice_date`, o.`delivery_date`, o.`date_add`, o.`date_upd`, o.`valid`

            FROM `'._DB_PREFIX_.'orders` o
            INNER JOIN `'._DB_PREFIX_.'order_state_lang` osl ON o.`current_state`=osl.`id_order_state`

            WHERE o.`id_shop`='.$id_shop.'
            AND o.`id_lang`='.$id_lang.'
            AND o.`id_customer`='.$resCid.'
			';
            $resRule=Db::getInstance()->executeS($SQL);

            foreach($resRule as &$resultR)
            {

                $date_add_arr=explode(" ",$resultR['date_add']);
                $date_upd_arr=explode(" ",$resultR['date_upd']);
                $date_invoice_arr=explode(" ",$resultR['invoice_date']);
                $date_delivery_arr=explode(" ",$resultR['delivery_date']);

                if ($date_add_arr[0]!='0000-00-00')
                {

                    $resultR['date_add_h'] = Pdate::convert_date_M_to_H($date_add_arr[0]);

                }
                if ($date_upd_arr[0]!='0000-00-00')
                {

                    $resultR['date_upd_h'] = Pdate::convert_date_M_to_H($date_upd_arr[0]);

                }
                if ($date_invoice_arr[0]!='0000-00-00')
                {

                    $resultR['date_invoice_h'] = Pdate::convert_date_M_to_H($date_invoice_arr[0]);

                }
                if ($date_delivery_arr[0]!='0000-00-00')
                {

                    $resultR['date_delivery_h'] = Pdate::convert_date_M_to_H($date_delivery_arr[0]);

                }

            }

        }


        $Array=array_merge(array(
            'result' => $resRule,
            'errors' => $errors,
            'msg' => $msg
        ));


        $encoder->render($Array);

    }

    public function Handle_GetHistoryOrderDetails($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid.';
            //return false;
        }

        $id_order = ((int)$request['id_order']);
        $id_lang = $cookie->id_lang;

        $SQL='SELECT id_order FROM '._DB_PREFIX_.'orders WHERE id_order='.(int)$id_order.' AND valid=1';
        $resOid=Db::getInstance()->getValue($SQL);

        if ((!(int)$resOid) ) {
            $errors[] = 'Order is not valid.';
        }


        if ( (count($errors)>0) ){
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept

            $SQL = '
			SELECT od.* , pl.`name`, pl.`link_rewrite`
            FROM `'._DB_PREFIX_.'order_detail` od
            INNER JOIN `'._DB_PREFIX_.'product_lang` pl ON pl.`id_product`=od.`product_id`
            WHERE od.`id_order`='.$resOid.' AND pl.id_lang='.$id_lang.'
			';
            $resOrderDetail=Db::getInstance()->executeS($SQL);


        }


        $Array=array_merge(array(
            'result' => $resOrderDetail,
            'errors' => $errors,
            'msg' => $msg
        ));


        $encoder->render($Array);

    }

    public function Handle_MailAlert($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            $errors[] = 'Cookie is not valid';
        }

        $customer_id = ((int)($cookie->id_customer));
        $action = $request['action'];
        $id_lang=$cookie->id_lang;
        if (($action=='')||(strlen($action)!=3)) {
            $errors[] = 'Action is not valid';
        }

        $SQL='SELECT id_customer FROM '._DB_PREFIX_.'customer WHERE id_customer='.$customer_id.' AND active=1';
        $resCid=Db::getInstance()->getValue($SQL);
        if (!($resCid)) {
            $errors[] = 'User is not valid';
        }

        $id_shop = (int)Context::getContext()->shop->id;

        $id_product = $request['pId'];
        $SQL='SELECT id_product FROM '._DB_PREFIX_.'product WHERE id_product='.$id_product.' AND active=1';
        $resPid=Db::getInstance()->getValue($SQL);

        if ((!(int)$resPid) ) {
            $errors[] = 'Product is not valid';
        }


        if ( (count($errors)>0)) {
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept

            if($action=='get')
            {

                $SQL = '
			SELECT id_product
			FROM `'._DB_PREFIX_.'mailalert_customer_oos` mco
			WHERE
			mco.`id_customer` = '.$customer_id.'
			AND
			mco.`id_product` = '.(int)$resPid.'

			';
                $resGetAlert=Db::getInstance()->getValue($SQL);

            }
            elseif($action=='add')
            {
                $SQL='SELECT email FROM '._DB_PREFIX_.'customer WHERE id_customer='.$customer_id.' AND active=1';
                $resEmail=Db::getInstance()->getValue($SQL);

                $SQL='SELECT id_product_attribute FROM '._DB_PREFIX_.'product_attribute WHERE id_product='.$resPid.' AND default_on=1';
                $resPAtt=Db::getInstance()->getValue($SQL);

                $table="mailalert_customer_oos";
                $dataArr=array(
                    'id_customer'=>$customer_id,
                    'customer_email'=>$resEmail,
                    'id_product'=>$resPid,
                    'id_product_attribute'=>$resPAtt,
                    'id_shop'=>$id_shop
                );
                $selectCheckParam="id_product";
                $whereCheckParam="id_product=".$resPid." AND id_customer=".$resCid;
                $resAddAlert=$this->add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam);
                if($resAddAlert=='OK')
                {
                    $msg[] = 'Insert Data successful.';
                }
                elseif($resAddAlert=="100")
                {

                    $msg[] = 'Miss Data.';

                }
                elseif($resAddAlert=="200")
                {

                    $msg[] = 'Duplicate Data.';

                }
                else
                {
                    $msg[] = 'Insert Data unsuccessful.';
                }



            }elseif($action=='del'){

                $table="mailalert_customer_oos";

                $selectCheckParam="id_product";
                $whereCheckParam="id_product=".$resPid." AND id_customer=".$resCid;
                $resDelAlert=$this->del($table,$id_lang,$selectCheckParam,$whereCheckParam);
                if($resDelAlert=='OK')
                {
                    $msg[] = 'Delete Data successful.';
                }
                elseif($resDelAlert=="100")
                {

                    $msg[] = 'Miss Data.';

                }
                elseif($resDelAlert=="202")
                {

                    $msg[] = 'Not Find Data.';

                }
                else
                {
                    $msg[] = 'Delete Data unsuccessful.';
                }

            }


        }

        $Array=array_merge(array(
            'resultGetAlert' => $resGetAlert,
            'resultAddAlert' => $resAddAlert,
            'resultDelAlert' => $resDelAlert,
            'errors' => $errors,
            'msg' => $msg
        ));
        $encoder->render($Array);



    }

    public function Handle_Comparison($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            $errors[] = 'Cookie is not valid';
        }

        $customer_id = ((int)($cookie->id_customer));
        $action = $request['action'];
        $id_lang=$cookie->id_lang;
        if (($action=='')||(strlen($action)!=3)) {
            $errors[] = 'Action is not valid';
        }

        $SQL='SELECT id_customer FROM '._DB_PREFIX_.'customer WHERE id_customer='.$customer_id.' AND active=1';
        $resCid=Db::getInstance()->getValue($SQL);
        if (!($customer_id)) {
            $msg[] = 'User is not login';
            $id_customer=$customer_id;
        }elseif(!$resCid){
            $errors[] = 'User is not valid';
            $id_customer=$resCid;
        }else{
            $id_customer=$resCid;
        }

        $id_shop = (int)Context::getContext()->shop->id;

        if((($action=='get')||($action=='add')||($action=='del'))&&((int)$request['id_compare']!=0))
        {
            $id_compare = (int)$request['id_compare'];

            $SQL='SELECT id_compare FROM '._DB_PREFIX_.'compare WHERE id_compare='.$id_compare.' AND id_customer='.$id_customer.'';
            $resComid=Db::getInstance()->getValue($SQL);
            if(!$resComid){
                $errors[] = 'Compare is not valid.';
            }else{
                $id_compare=$resComid;
            }
        }


        if ( (count($errors)>0)) {
            $errors[] = 'You Have Error!';
            //return false;
            //exit();
        }else{//Accept

            if($action=='get')
            {
                $id_compare = (int)$request['id_compare'];

            $SQL = '
			SELECT *
			FROM `'._DB_PREFIX_.'compare_product` cp
			WHERE
			cp.`id_compare` = '.(int)$id_compare.'
			';
                $resGetComp=Db::getInstance()->executeS($SQL);

            }
            elseif($action=='add')
            {
                if(!$id_compare){
                    $msg[] = 'You have not id_compare valid, we add this id.';


                    $table="compare";
                    $dataArr=array(
                        'id_customer'=>$id_customer
                    );
                    $selectCheckParam="id_compare";
                    $whereCheckParam="id_customer=".$id_customer;
                    $resAddComp=$this->add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam);
                    if($resAddComp=='OK')
                    {
                        $msg[] = 'Insert Data successful.';
                    }
                    elseif($resAddComp=="100")
                    {

                        $msg[] = 'Miss Data.';

                    }
                    elseif($resAddComp=="200")
                    {

                        $msg[] = 'Duplicate Data.';

                    }
                    else
                    {
                        $msg[] = 'Insert Data unsuccessful.';
                    }


                    $SQL='SELECT id_compare FROM '._DB_PREFIX_.'compare WHERE id_customer='.$id_customer.' ORDER BY id_compare DESC';
                    $resComid=Db::getInstance()->getValue($SQL);

                    $id_compare=$resComid;

                }
                else
                {//you are id_compare

                }

                $id_product = $request['pId'];
                $SQL='SELECT id_product FROM '._DB_PREFIX_.'product WHERE id_product='.$id_product.' AND active=1';
                $resPid=Db::getInstance()->getValue($SQL);

                if ((!(int)$resPid) ) {
                    $errors[] = 'Product is not valid';
                }
                else
                {
                //Add Product to compare
                    $date_add = date("Y-m-d H:i:s");

                    $table="compare_product";
                    $dataArr=array(
                        'id_compare'=>$id_compare,
                        'id_product'=>$resPid,
                        'date_add'=>$date_add,
                        'date_upd'=>"0000-00-00 00:00:00"
                    );
                    $selectCheckParam="id_compare";
                    $whereCheckParam="id_product=".$resPid." AND id_compare=".$id_compare;
                    $resAddCompPrd=$this->add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam);
                    if($resAddCompPrd=='OK')
                    {
                        $msg[] = 'Insert Data successful.';
                    }
                    elseif($resAddCompPrd=="100")
                    {

                        $msg[] = 'Miss Data.';

                    }
                    elseif($resAddCompPrd=="200")
                    {

                        $msg[] = 'Duplicate Data.';

                    }
                    else
                    {
                        $msg[] = 'Insert Data unsuccessful.';
                    }



                }





            }
            elseif($action=='del')
            {

                $table="compare_product";

                $selectCheckParam="id_product";
                $whereCheckParam="id_compare=".$id_compare;
                $resDelCompDetail=$this->del($table,$id_lang,$selectCheckParam,$whereCheckParam);
                if($resDelCompDetail=='OK')
                {

                    $table="compare";

                    $selectCheckParam="id_compare";
                    $whereCheckParam="id_compare=".$id_compare;
                    $resDelComp=$this->del($table,$id_lang,$selectCheckParam,$whereCheckParam);

                    $msg[] = 'Delete Data Detail successful.';

                    if($resDelComp=='OK')
                    {

                        $msg[] = 'Delete Data successful.';

                    }
                    elseif($resDelComp=="100")
                    {

                        $msg[] = 'Miss Data.';

                    }
                    elseif($resDelComp=="202")
                    {

                        $msg[] = 'Not Find Data.';

                    }
                    else
                    {
                        $msg[] = 'Delete Data unsuccessful.';
                    }



                }
                elseif($resDelCompDetail=="100")
                {

                    $msg[] = 'Miss Data Detail.';

                }
                elseif($resDelCompDetail=="202")
                {

                    $msg[] = 'Not Find Data Detail.';

                }
                else
                {
                    $msg[] = 'Delete Data Detail unsuccessful.';
                }

            }


        }

        $Array=array_merge(array(
            'resultGetComp' => $resGetComp,
            'resultAddComp' => $resAddComp,
            'resultCompareid' => $resComid,
            'resultAddCompPrd' => $resAddCompPrd,
            'resultDelComp' => $resDelComp,
            'resultDelCompDetail' => $resDelCompDetail,
            'errors' => $errors,
            'msg' => $msg
        ));
        $encoder->render($Array);



    }


    protected function add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam)
    {

        $SQL = '
			SELECT '.$selectCheckParam.'
			FROM '._DB_PREFIX_.''.$table.'
			WHERE '.$whereCheckParam.' ';

        $result = Db::getInstance()->executeS($SQL);

        if(count($result)==0)
        {

            if(($table)&&($dataArr))
            {
                if((Db::getInstance()->insert($table, $dataArr)))
                {
                    return 'OK';
                }else{
                    return 'Not OK';
                }

            }else{
                return '100'; //Miss data
            }

        }else{

            return '200'; //Duplicate record

        }

    }

    protected function del($table,$id_lang,$selectCheckParam,$whereCheckParam)
    {

        $SQL = '
			SELECT '.$selectCheckParam.'
			FROM '._DB_PREFIX_.''.$table.'
			WHERE '.$whereCheckParam.' ';

        $result = Db::getInstance()->executeS($SQL);

        if(count($result)!=0)
        {

            if(($table)&&($whereCheckParam))
            {
                if((Db::getInstance()->delete($table,$whereCheckParam)))
                {
                    return 'OK';
                }else{
                    return 'Not OK';
                }

            }else{
                return '100'; //Miss data
            }

        }else{

            return '202'; //Not Find record

        }

    }

    protected function update($table,$id_lang,$dataArr,$selectCheckParam,$whereUpdate)
    {

        $SQL = '
			SELECT '.$selectCheckParam.'
			FROM '._DB_PREFIX_.''.$table.'
			WHERE '.$whereUpdate.' ';

        $result = Db::getInstance()->executeS($SQL);

        if(count($result)>0)
        {

            if(($table)&&($dataArr))
            {
                if((Db::getInstance()->update($table, $dataArr, $whereUpdate)))
                {
                    return 'OK';
                }else{
                    return 'Not OK';
                }

            }else{
                return '100'; //Miss data
            }

        }else{

            return '201'; //Not Find record

        }

    }

    public function Handle_RmFav($metadata, $request, $encoder) //remove fav
    {

    }

    public function Handle_BuyerLogin($metadata, $request, $encoder)
    {
        $this->handleBuyerLoginUserPassword($metadata, $request, $encoder);
        /*
        // login with user and password
        if (isset($request['Username']) && isset($request['Password']))
        {
            // dies on error, syncs cookie on success
            $this->handleBuyerLoginUserPassword($metadata, $request, $encoder);
        }
        else // login with an external service
        if (isset($request['AuthServiceName']) && isset($request['AuthServiceToken']))
        {
            // dies on error, syncs cookie on success
            $this->handleBuyerLoginAuthService($metadata, $request, $encoder);
        }
        else CartAPI_Helpers::dieOnError($encoder, 'IncompleteRequest', 'Login arguments missing');

        // if here we assume the customer is authenticated and the cookie is synced
        */
        // create the response
        $response = CartAPI_Helpers::createSuccessResponse($encoder);

        // add the session id
        $encoder->addString($response, 'SessionId', $this->getCookieSessionId());

        // show the response
        $encoder->render($response);
    }

    public function handleBuyerLoginUserPassword($metadata, $request, $encoder)
    {
        // code from AuthController SubmitLogin

        //$email = $request['Username'];
        //$passwd = $request['Password'];
        $data = json_decode(file_get_contents('php://input'), true);
        $email = $data['username'];
        $passwd = $data['password'];

        $customer = new Customer();
        if (!Validate::isEmail($email) OR ($passwd AND !Validate::isPasswd($passwd))) CartAPI_Helpers::dieOnError($encoder, 'LoginNotAuthorized', CartAPI_Handlers_Helpers::removeHtmlTags(Tools::displayError('Authentication failed')));
        $authentication = $customer->getByEmail(trim($email), trim($passwd));
        if (!$authentication OR !$customer->id) {
            /* Handle brute force attacks */
            sleep(1);
            CartAPI_Helpers::dieOnError($encoder, 'LoginNotAuthorized', CartAPI_Handlers_Helpers::removeHtmlTags(Tools::displayError('Authentication failed')));
        }

        // if here than passed authentication
        $this->syncCookie($customer);

        // run the after login events
        $this->afterBuyerLogin($customer);
    }

    public function Handle_BuyerRegister($metadata, $request, $encoder)
    {
        // required arguments
        if (!isset($request['Buyer'])) CartAPI_Helpers::dieOnError($encoder, 'IncompleteRequest', 'Buyer argument missing');

        // register with user and password
        if (isset($request['Buyer']['Username']) && isset($request['Buyer']['Password'])) {
            // dies on error, syncs cookie on success
            $this->handleBuyerRegisterUserPassword($metadata, $request, $encoder);
        } else // login with an external service
            if (isset($request['Buyer']['AuthServiceName']) && isset($request['Buyer']['AuthServiceToken'])) {
                // dies on error, syncs cookie on success
                $this->handleBuyerRegisterAuthService($metadata, $request['Buyer'], $encoder);
            } else CartAPI_Helpers::dieOnError($encoder, 'IncompleteRequest', 'Register arguments missing');

        // if here we assume the customer is authenticated and the cookie is synced

        // create the response
        $response = CartAPI_Helpers::createSuccessResponse($encoder);

        // see if we need to login too, then add the session id
        if (!isset($request['Login']) || ($request['Login'] == 'true')) {
            $encoder->addString($response, 'SessionId', $this->getCookieSessionId());
        }

        // show the response
        $encoder->render($response);
    }

    public function handleBuyerRegisterUserPassword($metadata, $request, $encoder)
    {
        // prepare the fields inside the POST (so we can use Prestashop's validateController)
        unset($_POST['email']);
        if (isset($request['Buyer']['Username'])) $_POST['email'] = $request['Buyer']['Username'];
        unset($_POST['passwd']);
        if (isset($request['Buyer']['Password'])) $_POST['passwd'] = $request['Buyer']['Password'];
        unset($_POST['firstname']);
        if (isset($request['Buyer']['FirstName'])) $_POST['firstname'] = $request['Buyer']['FirstName'];
        unset($_POST['lastname']);
        if (isset($request['Buyer']['LastName'])) $_POST['lastname'] = $request['Buyer']['LastName'];

        // verify fields are valid
        $customer = new Customer();
        if (_PS_VERSION_ < '1.5') $errors = $customer->validateControler();
        else $errors = $customer->validateController();
        if (is_array($errors) && (count($errors) > 0)) CartAPI_Helpers::dieOnError($encoder, 'RegisterNotAuthorized', CartAPI_Handlers_Helpers::removeHtmlTags($errors[0]));

        // make sure the customer doesn't already exist
        if (Customer::customerExists($_POST['email'])) CartAPI_Helpers::dieOnError($encoder, 'RegisterNotAuthorized', CartAPI_Handlers_Helpers::removeHtmlTags(Tools::displayError('An account is already registered with this e-mail, please fill in the password or request a new one.')));

        // add the new user
        $customer->active = 1;
        if (property_exists('Customer', 'is_guest')) $customer->is_guest = 0;
        if (!$customer->add()) CartAPI_Helpers::dieOnError($encoder, 'RegisterNotAuthorized', CartAPI_Handlers_Helpers::removeHtmlTags(Tools::displayError('An error occurred while creating your account.')));

        // see if we need to login too
        if (!isset($request['Login']) || ($request['Login'] == 'true')) {
            $cookie = $this->syncCookie($customer);

            // run the after login events, actually don't since prestashop AuthController doesn't do it
            // $this->afterBuyerLogin($customer);
        }

        // run the after register events
        $this->afterBuyerRegister($customer, $request['Buyer']);
    }

}

?>
