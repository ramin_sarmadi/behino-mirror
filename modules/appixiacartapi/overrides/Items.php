<?php

// this is an override class related to products (item details, item list)
// if you need to customize the module to your needs, make all changes here
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Items.php');

class CartAPI_Handlers_Override_Items extends CartAPI_Handlers_Items
{
	// 	override any functions you want to change (from the core Items.php) here

	// if you want to change the image type for product thumbnails, change it here
	public function getThumbnailImageType()
	{
		//return 'home';
		//FIX THIS
		return null;

	}

	// if you want to change the image type for detailed product images (gallery), change it here
	public function getImagesImageType()
	{
		//return 'thickbox';
		//FIX THIS
		return null;

	}

	// this is an example override to add the HTML description of an item to the item resources list
	public function getResourceDictionariesFromProduct($product, $id_lang)
	{
		$resources = array();
		
		$resource = array(
			'Id' => 'Description',
			'Type' => 'HTML',
			'ContentUrl' => CartAPI_Handlers_Helpers::getCartApiHomeUrl().'overrides/cms/product_description.php?id_product='.(int)($product->id).'&',
		);
		$resources[] = $resource;
		
		// all done
		return $resources;
	}
    public function Handle_GetHomeItems($metadata, $request, $encoder)
	{
		// required arguments
		if (!isset($request['Paging'])) CartAPI_Helpers::dieOnError($encoder, 'IncompleteRequest', 'Paging argument missing');
		$sql_limit = CartAPI_Helpers::getSqlLimitFromPagingRequest($encoder, $request['Paging']);
	
		global $cookie;
		$id_lang = $cookie->id_lang;

		// allow to override filters before the command is executed
		if (isset($request['Filter'])) $this->overrideItemListFilters($request['Filter']);

		// go over the filters
		$sql_filters = array();
		$filters = CartAPI_Helpers::getDictionaryKeyAsArray($request, 'Filter');
		foreach ($filters as $filter)
		{
			$db_field_name_map = array('Title' => 'pl.`name`', 'CategoryId' => 'cp.`id_category`');
			$sql_filters[] = CartAPI_Helpers::getSqlFilterFromFilter($encoder, $filter, $db_field_name_map);
		}
		
		$sql_orderby = 'p.`id_product` desc'; // default sort (newest items first)
		$this->overrideItemListSqlOrderBy($request, $sql_orderby);

		// complete the sql statement
		$sql_filters[] = 'p.`active` = 1';
		$sql_filters[] = 'pl.`id_lang` = '.(int)$id_lang;
		$sql_where = CartAPI_Helpers::getSqlWhereFromSqlFilters($sql_filters);
		$sql = '
			SELECT SQL_CALC_FOUND_ROWS p.`id_product`, pl.`name`, p.`active` 
			FROM `'._DB_PREFIX_.'product` p 
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON cp.`id_product` = p.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON p.`id_product` = pl.`id_product` 
			'.$sql_where.' 
			GROUP BY `id_product` 
			ORDER BY '.$sql_orderby.'
			'.$sql_limit;

		// load the products and the total element count
		$result = Db::getInstance()->ExecuteS($sql);
		$total_elements_row = Db::getInstance()->getRow('SELECT FOUND_ROWS()');
		$total_elements = intval(array_pop($total_elements_row));
		
		// change results before they are returned
		$this->overrideItemListResult($request, $result, $total_elements);

		// create the response
		$response = CartAPI_Helpers::createSuccessResponseWithPaging($encoder, $request['Paging'], $total_elements, CartAPI_Handlers_Helpers::getLocale());

		// add the items to the response if needed
		if (count($result) > 0) $items = &$encoder->addArray($response, 'Item');

		// encode each item
		foreach ($result as $row)
		{
			// to allow support for overrideItemListResult() to return objects instead of arrays, let's check if it's an object and fix as needed
			if (is_object($row) && ($row instanceof ProductCore))
			{
				$arr = array();
				$arr['id_product'] = $row->id;
				$arr['name'] = $row->name;
				$row = $arr; // make the switch from object to array
			}
		
			// encode the item
			$item = &$encoder->addContainerToArray($items);
			$encoder->addNumber($item, 'Id', $row['id_product']);
			$encoder->addString($item, 'Title', $row['name']);
		
			$price = $this->getPriceFromProductId($row['id_product']);
			$encoder->addNumber($item, 'Price', $price);
			$referencePrice = $this->getReferencePriceFromProductId($row['id_product']);
			if ($referencePrice > $price) $encoder->addNumber($item, 'ReferencePrice', $referencePrice);
		
			$this->addThumbnailUrlFromProductId($encoder, $item, $row['id_product']);
			$this->addExtraFieldsFromProductId($metadata, $request, $encoder, $item, $row['id_product']);
		}

		// show the response
		$encoder->render($response);
	}

    public function Handle_GetProductDetail($metadata, $request, $encoder)
    {
        $errors = array();
        $msg=array();

        global $cookie;
        if (!$cookie) {
            //echo "false";
            $errors[] = 'Cookie is not valid';
            //return false;
        }

        $id_lang=$cookie->id_lang;

        $id_shop = (int)Context::getContext()->shop->id;
        $id_product = $request['pId'];
        $SQL='SELECT id_product FROM '._DB_PREFIX_.'product WHERE id_product='.$id_product.' AND active=1';
        $resPid=Db::getInstance()->getValue($SQL);

        if ((!(int)$resPid) ) {
            $errors[] = 'Product is not valid';
        }

        if((count($errors)>0)){

            $errors[] = 'You Have Error!';

        }else{


            $resProduct=Product::getProductRowSimple($id_lang,$id_product,true);

            foreach ($resProduct as &$rProduct)
            {

                $Image=$rProduct['id_image'];
                if($Image!="")
                {
                    $ImageIdArr=explode("-",$Image);
                    $id_image_cover=$ImageIdArr[1];
                    $rProduct['ImageUrl']=$this->getThumbnailUrlFromImageId($id_image_cover,$rProduct['id_product']);

                }


            }

            $TopFeature = Product::getFeatureProductRows($id_lang,$id_product,true);

            $SQL='SELECT * FROM '._DB_PREFIX_.'product_comment WHERE id_product='.$id_product.' AND validate=1 AND deleted=0
            ORDER BY id_product_comment DESC LIMIT 4';
            $resComment=Db::getInstance()->executeS($SQL);

            $CountLike=ProductCore::getCountLike($id_product,1);

            $resAtt=$this->getListContentAtt($id_lang,0,$id_product);



        }

        $Array=array_merge(array(
            'resultProduct' => $resProduct,
            'TopFeature' => $TopFeature,
            'resultComment' => $resComment,
            'resultCountLike' => $CountLike,
            'resAttribute' => $resAtt,
            'errors' => $errors,
            'msg' => $msg
        ));
        $encoder->render($Array);
/*
        die(Tools::jsonEncode(array(
            'resultProduct' => $resProduct,
            'TopFeature' => $TopFeature,
            'resultComment' => $resComment,
            'resultCountLike' => $CountLike,
            'resAttribute' => $resAtt,
            'errors' => $errors,
            'msg' => $msg
        )));*/


    }

    protected function getListContentAtt($id_lang,$id_product_attribute = 0,$id_product = 0, $page = 0, $CountRows = 30,$whereQuery = '' )
    {
        $Q_Limit='';

        $PageResult = $page;

        if (($PageResult>1)) {
            $PageRows = ($PageResult * $CountRows) - $CountRows;
            $Q_Limit = 'LIMIT ' . $PageRows . ' , ' . $CountRows . '';
        } else {
            $Q_Limit = 'LIMIT 0 , ' . $CountRows . '';
        }


        $sub_query='';
        if(($id_product>0)&&($whereQuery==''))
        {
            $sub_query = ' WHERE id_product='.$id_product;
        }elseif(($id_product_attribute>0)&&($whereQuery=='')){
            $sub_query = ' WHERE id_product_attribute='.$id_product_attribute;
        }elseif(($id_product>0)&&($whereQuery!='')){
            $sub_query = ' WHERE id_product='.$id_product.' AND '.$whereQuery;
        }elseif(($id_product_attribute>0)&&($whereQuery!='')){
            $sub_query = ' WHERE id_product_attribute='.$id_product_attribute.' AND '.$whereQuery;
        }elseif(($whereQuery!='')){
            $sub_query = ' WHERE '.$whereQuery;
        }

        $SQL = '
        SELECT * FROM
        (
        SELECT p.id_product,
            (SELECT MAX(image_shop.`id_image`)  FROM '._DB_PREFIX_.'image i
            '.Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
            WHERE i.id_product = p.id_product
            ) id_image,
            pl.name prd_name,
            pa.id_product_attribute,pa.price,p.id_category_default as category, pl.link_rewrite, p.ean13,
            (SELECT quantity FROM '._DB_PREFIX_.'stock_available sa
            WHERE sa.id_product=p.id_product and sa.id_product_attribute=pa.id_product_attribute ) as quantity,
            (
            SELECT GROUP_CONCAT(attName," #",IF(color!="",id_attribute,0),"#")
            FROM
            (
            SELECT
            CONCAT("(",agl.name," / ",al.name," / id_attribute:",pac.id_attribute,")") as attName ,pac.id_attribute,a.id_attribute_group,a.color,pac.id_product_attribute
            FROM '._DB_PREFIX_.'product_attribute_combination pac
            INNER JOIN '._DB_PREFIX_.'attribute a ON pac.id_attribute=a.id_attribute
            INNER JOIN '._DB_PREFIX_.'attribute_group ag ON a.id_attribute_group=ag.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_group_lang agl ON ag.id_attribute_group=agl.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_lang al ON a.id_attribute=al.id_attribute
            ) as tmp WHERE id_product_attribute=pa.id_product_attribute
            ) as attNameANDGroup


            FROM '._DB_PREFIX_.'product_attribute pa
            INNER JOIN '._DB_PREFIX_.'product p ON pa.id_product=p.id_product
            INNER JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product
            ) as tmpQuery
            '.$sub_query.'

            ORDER BY id_product
            '.$Q_Limit;

        $results =  Db::getInstance()->executeS($SQL);

        //$FeatureArr=$this->getListFeatureDss($this->context->language->id);


        $count=0;
        $prd=array();
        foreach ($results as &$result){


            //'<div dir="ltr">'.$result['attNameANDGroup'];
            $attData=explode("#",$result['attNameANDGroup']);
            $count=0;
            $attNameANDGroup='';
            $id_attribute='';
            foreach ($attData as $attRes)
            {

                //echo '<div dir="ltr">'.$attRes;
                if((is_int((int)$attRes))&&((int)$attRes>0)){
                    $id_attribute=$attRes;
                }elseif((is_string($attRes))&&($attRes!='0')){
                    $attNameANDGroup.=$attRes;
                }

                $count++;
            }
            //print_r($attData);
            $result['attNameANDGroup']=$attNameANDGroup;
            $result['id_attribute']=$id_attribute;
            //echo _PS_IMG_DIR_.'co/'.$result['id_attribute'].'.jpg';

            if(file_exists(_PS_IMG_DIR_.'co/'.$result['id_attribute'].'.jpg'))
            {
                $result['ColorAtt']= _PS_IMG_.'/co/'.$result['id_attribute'].'.jpg' ;
            }else{
                $resColor=$this->getValue('attribute','color',' WHERE id_attribute='.$result['id_attribute'].' AND color LIKE "#%"');
                $result['ColorAtt']= $resColor;
            }


            $prd[$count]=$result['id_product'];



        }

        return $results;
    }

    protected function getValue($table,$filed,$where='',$join='')
    {

        $SQL='SELECT '.$filed.' FROM '._DB_PREFIX_.$table.'
        '.$join.'
        '.$where.'
        ';

        return $results =  Db::getInstance()->getValue($SQL);
    }

    public function Handle_GetCriterionRate($metadata, $request, $encoder)
    {
        global $cookie;
        $id_lang = $cookie->id_lang;


        // go over the filters
        $sql_filters = array();


        $sql_orderby = 'pccl.`id_product_comment_criterion` desc'; // default sort (newest items first)
        $this->overrideItemListSqlOrderBy($request, $sql_orderby);

        // complete the sql statement
        $sql_filters[] = 'pcc.`id_product_comment_criterion_type` = 1';
        $sql_filters[] = 'pccl.`id_lang` = '.(int)$id_lang;
        $sql_where = CartAPI_Helpers::getSqlWhereFromSqlFilters($sql_filters);
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS pccl.`id_product_comment_criterion`, pccl.`name`
			FROM `'._DB_PREFIX_.'product_comment_criterion` pcc
			INNER JOIN `'._DB_PREFIX_.'product_comment_criterion_lang` pccl ON pccl.`id_product_comment_criterion` = pcc.`id_product_comment_criterion`
			'.$sql_where.'
			ORDER BY '.$sql_orderby.'
			';

        // load the products and the total element count
        $result = Db::getInstance()->ExecuteS($sql);
        $total_elements_row = Db::getInstance()->getRow('SELECT FOUND_ROWS()');
        $total_elements = intval(array_pop($total_elements_row));

        // change results before they are returned
        $this->overrideItemListResult($request, $result, $total_elements);

        // create the response
        $response = CartAPI_Helpers::createSuccessResponseWithPaging($encoder, $request['Paging'], $total_elements, CartAPI_Handlers_Helpers::getLocale());

        // add the items to the response if needed
        if (count($result) > 0) $items = &$encoder->addArray($response, 'Item');

        // encode each item
        foreach ($result as $row)
        {
            // to allow support for overrideItemListResult() to return objects instead of arrays, let's check if it's an object and fix as needed
            if (is_object($row) && ($row instanceof ProductCore))
            {
                $arr = array();
                $arr['id_product_comment_criterion'] = $row->id;
                $arr['name'] = $row->name;
                $row = $arr; // make the switch from object to array
            }

            // encode the item
            $item = &$encoder->addContainerToArray($items);
            $encoder->addNumber($item, 'Id', $row['id_product_comment_criterion']);
            $encoder->addString($item, 'Title', $row['name']);


        }



        // show the response
        $encoder->render($response);

    }

    public function Handle_GetSimular($metadata, $request, $encoder)
    {

        global $cookie;
        $id_lang = $cookie->id_lang;


        $category = new CategoryCore();

        $idProduct = $request['pId'];

        $nb=2;

        $categorySimularProductsId = $category->getSimularProductsWebservice($idProduct, $id_lang, 1, $nb, true, $active = true, $order_by = 'count' );
        $categorySimularPriceProducts = $category->getSimularProductsInPriceWebservice($idProduct, $id_lang, 1, $nb, true, $active = true, $order_by = 'A_Price' );

        $ArrPidF=explode(",",$categorySimularProductsId);
        $ArrPidP=explode(",",$categorySimularPriceProducts);

        $ArrPid = array_merge($ArrPidF,$ArrPidP);
        $Product_id='';
        foreach ($ArrPid as $rowPid)
        {

            $Product_id .= ",".$rowPid;

        }

        $where_id_product = substr($Product_id,1);


        // go over the filters
        $sql_filters = array();


        $sql_orderby = 'p.`id_product` desc'; // default sort (newest items first)
        $this->overrideItemListSqlOrderBy($request, $sql_orderby);

        // complete the sql statement
        $sql_filters[] = 'p.`active` = 1';
        $sql_filters[] = 'p.`id_product` IN ('.$where_id_product.')';
        $sql_filters[] = 'pl.`id_lang` = '.(int)$id_lang;
        $sql_where = CartAPI_Helpers::getSqlWhereFromSqlFilters($sql_filters);
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS p.`id_product`, pl.`name`, p.`active`
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON cp.`id_product` = p.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON p.`id_product` = pl.`id_product`
			'.$sql_where.'
			GROUP BY `id_product`
			ORDER BY '.$sql_orderby.'
			';

        // load the products and the total element count
        $result = Db::getInstance()->ExecuteS($sql);
        $total_elements_row = Db::getInstance()->getRow('SELECT FOUND_ROWS()');
        $total_elements = intval(array_pop($total_elements_row));

        // change results before they are returned
        $this->overrideItemListResult($request, $result, $total_elements);

        // create the response
        $response = CartAPI_Helpers::createSuccessResponseWithPaging($encoder, $request['Paging'], $total_elements, CartAPI_Handlers_Helpers::getLocale());

        // add the items to the response if needed
        if (count($result) > 0) $items = &$encoder->addArray($response, 'Item');

        // encode each item
        foreach ($result as $row)
        {
            // to allow support for overrideItemListResult() to return objects instead of arrays, let's check if it's an object and fix as needed
            if (is_object($row) && ($row instanceof ProductCore))
            {
                $arr = array();
                $arr['id_product'] = $row->id;
                $arr['name'] = $row->name;
                $row = $arr; // make the switch from object to array
            }

            // encode the item
            $item = &$encoder->addContainerToArray($items);
            $encoder->addNumber($item, 'Id', $row['id_product']);
            $encoder->addString($item, 'Title', $row['name']);

            $price = $this->getPriceFromProductId($row['id_product']);
            $encoder->addNumber($item, 'Price', $price);
            $referencePrice = $this->getReferencePriceFromProductId($row['id_product']);
            if ($referencePrice > $price) $encoder->addNumber($item, 'ReferencePrice', $referencePrice);

            $this->addThumbnailUrlFromProductId($encoder, $item, $row['id_product']);
            $this->addExtraFieldsFromProductId($metadata, $request, $encoder, $item, $row['id_product']);
        }


        // show the response
        $encoder->render($response);
    }

    public function Handle_Search($metadata, $request, $encoder)
    {

        global $cookie;
        $id_lang = $cookie->id_lang;



        (string)$SearchQuery = $request['q'];

        // go over the filters
        $sql_filters = array();


        $sql_orderby = 'p.`id_product` desc'; // default sort (newest items first)
        $this->overrideItemListSqlOrderBy($request, $sql_orderby);

        // complete the sql statement
        $sql_filters[] = 'p.`active` = 1';
        $sql_filters[] = 'pl.`name` LIKE "%'.(string)$SearchQuery.'%"';
        $sql_filters[] = 'pl.`id_lang` = '.(int)$id_lang;
        $sql_where = CartAPI_Helpers::getSqlWhereFromSqlFilters($sql_filters);
        $sql = '
			SELECT SQL_CALC_FOUND_ROWS p.`id_product`, pl.`name`, p.`active`
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON cp.`id_product` = p.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON p.`id_product` = pl.`id_product`
			'.$sql_where.'
			GROUP BY `id_product`
			ORDER BY '.$sql_orderby.'
			';

        // load the products and the total element count
        $result = Db::getInstance()->ExecuteS($sql);
        $total_elements_row = Db::getInstance()->getRow('SELECT FOUND_ROWS()');
        $total_elements = intval(array_pop($total_elements_row));

        // change results before they are returned
        $this->overrideItemListResult($request, $result, $total_elements);

        // create the response
        $response = CartAPI_Helpers::createSuccessResponseWithPaging($encoder, $request['Paging'], $total_elements, CartAPI_Handlers_Helpers::getLocale());

        // add the items to the response if needed
        if (count($result) > 0) $items = &$encoder->addArray($response, 'Item');

        // encode each item
        foreach ($result as $row)
        {
            // to allow support for overrideItemListResult() to return objects instead of arrays, let's check if it's an object and fix as needed
            if (is_object($row) && ($row instanceof ProductCore))
            {
                $arr = array();
                $arr['id_product'] = $row->id;
                $arr['name'] = $row->name;
                $row = $arr; // make the switch from object to array
            }

            // encode the item
            $item = &$encoder->addContainerToArray($items);
            $encoder->addNumber($item, 'Id', $row['id_product']);
            $encoder->addString($item, 'Title', $row['name']);

            $price = $this->getPriceFromProductId($row['id_product']);
            $encoder->addNumber($item, 'Price', $price);
            $referencePrice = $this->getReferencePriceFromProductId($row['id_product']);
            if ($referencePrice > $price) $encoder->addNumber($item, 'ReferencePrice', $referencePrice);

            $this->addThumbnailUrlFromProductId($encoder, $item, $row['id_product']);
            $this->addExtraFieldsFromProductId($metadata, $request, $encoder, $item, $row['id_product']);
        }


        // show the response
        $encoder->render($response);
    }

    public function Handle_AdvanceSearch($metadata, $request, $encoder)
    {

        global $cookie;
        $id_lang = $cookie->id_lang;

        $msg="";
        if((int)$id_lang==0){
            $errors[] = 'Do not set cookie!';
        }

        $action = $request['action'];

        if($action=="getFeature"){
            $Array=array();

            $FeatureName = $request['FeatureName'];
            $FeatureNameArr=explode("|",$FeatureName);

            if(count($FeatureNameArr)==0)
            {
                $errors[] = 'You have not FeatureName!';
            }
            $SQL_FN = "";
            foreach($FeatureNameArr as $FNArr){

                $SQL_FN.=",'".$FNArr."'";

            }
            $SQL_FN = substr($SQL_FN,1);

            $sql_filters[] = '`fgl`.`id_group`!=0';
            $sql_filters[] = '`fl`.`name` IN ('.$SQL_FN.')';
            $sql_where = CartAPI_Helpers::getSqlWhereFromSqlFilters($sql_filters);

            $SQL = '
            SELECT
            `fgl`.`id_group`,
            `fgl`.`groupname`,
            `f`.`id_feature`,
            `fl`.`name`,
            `fv`.`id_feature_value`,
            `fvl`.`value`
            FROM `'._DB_PREFIX_.'feature` f
            INNER JOIN `'._DB_PREFIX_.'feature_lang` fl ON (
            `f`.`id_feature`=`fl`.`id_feature` AND `fl`.`id_lang`='.$id_lang.' )
            INNER JOIN `'._DB_PREFIX_.'featurestogroups` ftog ON
            `f`.`id_feature`=`ftog`.`id_feature`
            INNER JOIN `'._DB_PREFIX_.'features_groups_lang` fgl ON
            `fgl`.`id_group`=`ftog`.`id_group`
            INNER JOIN `'._DB_PREFIX_.'feature_value` fv ON
            `f`.`id_feature`=`fv`.`id_feature`
            INNER JOIN `'._DB_PREFIX_.'feature_value_lang` fvl ON
            `fvl`.`id_feature_value`=`fv`.`id_feature_value`
            '.$sql_where.'
            GROUP BY `fgl`.`id_group`,`fgl`.`groupname` , `f`.`id_feature` , `fl`.`name`,`fvl`.`value`
            ORDER BY `fgl`.`id_group`,`f`.`id_feature`,`f`.`position`
            ';
            if(count($errors)==0)
            {
                $featuresWithGroupArray=Db::getInstance()->executeS($SQL);
            }

            $Array=array_merge(array(
                'resultAllCategories' => Search::getAllCategory(0, (int)$id_lang),
                'resultAllFeaturesWithGroup' => $featuresWithGroupArray,
                'errors' => $errors,
                'msg' => $msg
            ));

            $encoder->render($Array);



        }
        elseif($action=="getResult")
        {
            $data = json_decode(file_get_contents('php://input'), true);

            $Product_Min_Cost = $data['min_cost'];
            $Product_Max_Cost = $data['max_cost'];
            $Product_features_values = $data['feature_value'];
            $Product_id_category = $data['id_category'];
            $PageResult = $data['page'];
            $CountRows = 12;

            $id_features = '';
            $id_features_value = '';
            $id_categorys = '';
            //-------------------------------------------------------
            //START For Category - STEP 2
            foreach ($Product_id_category as $id_category) {
                if($id_category>0){
                    $id_categorys = $id_categorys . ',' . $id_category;
                }
            }
            $id_categorys = substr($id_categorys, 1);

            $Sub_Query='p.active=1 AND cp.id_category IN ('.$id_categorys.') AND p.id_product IN ('.$id_products.')';
            $join='INNER JOIN '._DB_PREFIX_.'category_product cp ON cp.id_product=p.id_product';

            $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

            $id_products='';
            foreach ($ResultIdProductArr as $id_product_arr)
            {
                $id_products = $id_products.','.$id_product_arr['id_product'] ;
            }

            $id_products = substr($id_products,1);
            //END For Category
            //----------------------------------------------------------
            //START For Price - STEP 3
            if(($Product_Max_Cost>=$Product_Min_Cost)
                && (($Product_Min_Cost>0)||($Product_Max_Cost>0))
            ){
                $Sub_Query='p.active=1 AND (pa.price BETWEEN '.$Product_Min_Cost.' AND '.$Product_Max_Cost.')  AND p.id_product IN ('.$id_products.')';
            }else{
                $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.')';
            }
            $join='INNER JOIN '._DB_PREFIX_.'product_attribute pa ON pa.id_product=p.id_product';

            $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

            $id_products='';
            foreach ($ResultIdProductArr as $id_product_arr)
            {
                $id_products = $id_products.','.$id_product_arr['id_product'] ;
            }

            $id_products = substr($id_products,1);


            //END For Price

            //----------------------------------------------------------
            //START For Feature - STEP 4
            //echo $id_products;
            //echo count($Product_features_values);
            if(count($Product_features_values)>0){

                $count_feature=0;
                foreach ($Product_features_values as $feature_values) {

                    if($feature_values>0){
                        $feature_value = explode("_", $feature_values);
                        if ((count($feature_value) > 1)&&($feature_value[0]>0)) {
                            $id_features = $id_features . ',' . $feature_value[0];
                        }
                        /*--------------*/
                        if ((count($feature_value) > 1)&&($feature_value[1]>0)) {
                            $id_features_value = $id_features_value . ',' . $feature_value[1];
                        }
                        /*--------------*/
                        $count_feature++;
                    }

                }

                if($count_feature>0){

                    $id_features = substr($id_features,1);
                    $id_features_value = substr($id_features_value, 1);

                    //Feature - STEP 4,1
                    $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') AND fp.id_feature IN ('.$id_features.')';
                    $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                    $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                    $id_products='';
                    foreach ($ResultIdProductArr as $id_product_arr)
                    {
                        $id_products = $id_products.','.$id_product_arr['id_product'] ;
                    }

                    $id_products = substr($id_products,1);

                    //Feature Value - STEP 4,2
                    $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') AND fp.id_feature_value IN ('.$id_features_value.')';
                    $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                    $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                    $id_products='';
                    foreach ($ResultIdProductArr as $id_product_arr)
                    {
                        $id_products = $id_products.','.$id_product_arr['id_product'] ;
                    }

                    $id_products = substr($id_products,1);
                    //------------------------------------------------
                    if ((strlen($id_features) > 0) && (strlen($id_features_value) > 0)) {
                        $id_features_value_arr = explode(",", $id_features_value);
                        $action = true;

                        //$Q_feature_values_with_value_num='';
                        if ((strlen($id_features_value) > 0)) {
                            foreach ($id_features_value_arr as $feature_valuesArr) {
                                if($feature_valuesArr>0){

                                    $Prd_min_features_value[$feature_valuesArr] = ($data['min_feature_value_' . $feature_valuesArr . ''] ? $data['min_feature_value_' . $feature_valuesArr . ''] : -1 );
                                    $Prd_min_features_value_2[$feature_valuesArr] = ($data['min_feature_value_2_' . $feature_valuesArr . ''] ? $data['min_feature_value_2_' . $feature_valuesArr . ''] : -1);

                                    $Prd_max_features_value[$feature_valuesArr] = ($data['max_feature_value_' . $feature_valuesArr . ''] ? $data['max_feature_value_' . $feature_valuesArr . ''] : -1 );
                                    $Prd_max_features_value_2[$feature_valuesArr] = ($data['max_feature_value_2_' . $feature_valuesArr . ''] ? $data['max_feature_value_2_' . $feature_valuesArr . ''] : -1 );
                                    if( ($Prd_max_features_value_2[$feature_valuesArr] >= $Prd_min_features_value[$feature_valuesArr] )
                                        && ($Prd_max_features_value[$feature_valuesArr] >= $Prd_min_features_value[$feature_valuesArr])
                                        && ($Prd_max_features_value[$feature_valuesArr]>0)&&($Prd_min_features_value[$feature_valuesArr]>0)
                                        && ($Prd_max_features_value_2[$feature_valuesArr]>0)&&($Prd_min_features_value_2[$feature_valuesArr]>0)
                                        && (strlen($id_products)>0) ){

                                        $Q_feature_values_with_value_num = ' AND (fp.id_feature_value=' . $feature_valuesArr . '
                                                             AND
                                                            (fp.value_num_1 BETWEEN ' . $Prd_min_features_value[$feature_valuesArr] . ' AND ' . $Prd_max_features_value[$feature_valuesArr] . ')
                                                             AND
                                                            (fp.value_num_2 BETWEEN ' . $Prd_min_features_value_2[$feature_valuesArr] . ' AND ' . $Prd_max_features_value_2[$feature_valuesArr] . ')

                                                            )';

                                        $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') '.$Q_feature_values_with_value_num;
                                        $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                                        $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                                        $id_products='';
                                        foreach ($ResultIdProductArr as $id_product_arr)
                                        {
                                            $id_products = $id_products.','.$id_product_arr['id_product'] ;
                                        }

                                        $id_products = substr($id_products,1);

                                    }elseif( ($Prd_min_features_value[$feature_valuesArr]>0 ) || ($Prd_max_features_value[$feature_valuesArr]>0 )
                                        && ($Prd_max_features_value[$feature_valuesArr] >= $Prd_min_features_value[$feature_valuesArr])
                                        && (strlen($id_products)>0) ){
                                        $Q_feature_values_with_value_num = ' AND (fp.id_feature_value=' . $feature_valuesArr . '
                                                            AND fp.value_num_1>=' . $Prd_min_features_value[$feature_valuesArr] . '
                                                            AND fp.value_num_1<=' . $Prd_max_features_value[$feature_valuesArr] . ' )';


                                        $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') '.$Q_feature_values_with_value_num;
                                        $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                                        $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                                        $id_products='';
                                        foreach ($ResultIdProductArr as $id_product_arr)
                                        {
                                            $id_products = $id_products.','.$id_product_arr['id_product'] ;
                                        }

                                        $id_products = substr($id_products,1);

                                    }else{

                                        $Q_feature_values_with_value_num = ' AND fp.id_feature_value=' . $feature_valuesArr . ' ';


                                        $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') '.$Q_feature_values_with_value_num;
                                        $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                                        $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                                        $id_products='';
                                        foreach ($ResultIdProductArr as $id_product_arr)
                                        {
                                            $id_products = $id_products.','.$id_product_arr['id_product'] ;
                                        }

                                        $id_products = substr($id_products,1);

                                    }


                                }


                            }//end foreach
                        }
                    }



                }

            }//end if count feature

            //END For Feature
            //------------------------------------------------------------------


            if ((strlen($id_products) > 0)) {
                $Query='p.id_product IN ('.$id_products.')';
            }else{
                $Query='p.id_product IN (0)';
            }

            // go over the filters
            $sql_filters = array();


            $sql_orderby = 'p.`id_product` desc'; // default sort (newest items first)
            $this->overrideItemListSqlOrderBy($request, $sql_orderby);

            // complete the sql statement
            $sql_filters[] = 'p.`active` = 1';
            $sql_filters[] = $Query;
            $sql_filters[] = 'pl.`id_lang` = '.(int)$id_lang;
            $sql_where = CartAPI_Helpers::getSqlWhereFromSqlFilters($sql_filters);
            $sql = '
			SELECT SQL_CALC_FOUND_ROWS p.`id_product`, pl.`name`, p.`active`
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON cp.`id_product` = p.`id_product`
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON p.`id_product` = pl.`id_product`
			'.$sql_where.'
			GROUP BY `id_product`
			ORDER BY '.$sql_orderby.'
			';

            // load the products and the total element count
            $result = Db::getInstance()->ExecuteS($sql);
            $total_elements_row = Db::getInstance()->getRow('SELECT FOUND_ROWS()');
            $total_elements = intval(array_pop($total_elements_row));

            // change results before they are returned
            $this->overrideItemListResult($request, $result, $total_elements);

            // create the response
            $response = CartAPI_Helpers::createSuccessResponseWithPaging($encoder, $request['Paging'], $total_elements, CartAPI_Handlers_Helpers::getLocale());

            // add the items to the response if needed
            if (count($result) > 0) $items = &$encoder->addArray($response, 'Item');

            // encode each item
            foreach ($result as $row)
            {
                // to allow support for overrideItemListResult() to return objects instead of arrays, let's check if it's an object and fix as needed
                if (is_object($row) && ($row instanceof ProductCore))
                {
                    $arr = array();
                    $arr['id_product'] = $row->id;
                    $arr['name'] = $row->name;
                    $row = $arr; // make the switch from object to array
                }

                // encode the item
                $item = &$encoder->addContainerToArray($items);
                $encoder->addNumber($item, 'Id', $row['id_product']);
                $encoder->addString($item, 'Title', $row['name']);

                $price = $this->getPriceFromProductId($row['id_product']);
                $encoder->addNumber($item, 'Price', $price);
                $referencePrice = $this->getReferencePriceFromProductId($row['id_product']);
                if ($referencePrice > $price) $encoder->addNumber($item, 'ReferencePrice', $referencePrice);

                $this->addThumbnailUrlFromProductId($encoder, $item, $row['id_product']);
                $this->addExtraFieldsFromProductId($metadata, $request, $encoder, $item, $row['id_product']);
            }


            // show the response
            $encoder->render($response);

        }

    }


}

?>