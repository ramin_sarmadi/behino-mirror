<?php

$GLOBALS['APPIXIA_DEBUGGER_OVERRIDE_TEMPLATES'] = array(

	'Add your overrides here' => array(
		'Description' => 'Edit appixiacartapi/overrides/debug/templates.php to add your own commands',
		'Url' => '../api.php?X-OPERATION=GetSingleItem&Id=124&',
	),
	
	'CMS Page - Product HTML Description' => array(
		'Description' => 'Show the HTML description of a product',
		'Url' => '../overrides/cms/product_description.php?id_product=9&',
	),
	
	'Get Home Products' => array(
		'Description' => 'Getting first page Products',
		'Url' => '../api.php?X-OPERATION=GetHomeItems&Paging[ElementsPerPage]=25&Paging[PageNumber]=1&Filter[Field]=CategoryId&Filter[Relation]=Equal&Filter[Value]=11&',
	),
		'Get User Info' => array(
		'Description' => 'Get User Info',
		'Url' => '../api.php?X-OPERATION=GetUserInfo',
	),
		'Update User Info' => array(
		'Description' => 'Update User Info',
		'Url' => '../api.php?X-OPERATION=UpdateUserInfo',
	),
    'add to favorite' => array(
        'Description' => 'add to favorite',
        'Url' => '../api.php?X-OPERATION=AddToFavorite&pId=11&',
    ),
    'Get faviorite Customer' => array(
        'Description' => 'Get favorite Customer',
        'Url' => '../api.php?X-OPERATION=GetFav',
    ),
    'Get Similar Products' => array(
        'Description' => 'Get Similar Products in feature',
        'Url' => '../api.php?X-OPERATION=GetSimular&pId=11',
    ),
    'Get Criterion Rate' => array(
        'Description' => 'Get Criterion Product Rate',
        'Url' => '../api.php?X-OPERATION=GetCriterionRate',
    ),
    'Add Product Rate' => array(
        'Description' => 'Add Product Rate',
        'Url' => '../api.php?X-OPERATION=AddRate&pId=11&CrId_1=5&CrId_2=4&CrId_3=3&CrId_4=2&CrId_5=2&content=تست&title=عنوان',
    ),
    'Get Product Rate' => array(
        'Description' => 'Get Product Rate',
        'Url' => '../api.php?X-OPERATION=GetRate&pId=11',
    ),
    'Get Product Comment' => array(
        'Description' => 'Get Product Comment',
        'Url' => '../api.php?X-OPERATION=GetComment&pId=11',
    ),
    'Get Product Detail' => array(
        'Description' => 'Get Product Detail',
        'Url' => '../api.php?X-OPERATION=GetProductDetail&pId=11',
    ),
    'Get Result Search Product' => array(
        'Description' => 'Get Result Search Product',
        'Url' => '../api.php?X-OPERATION=Search&q=sony',
    ),
    'Get Result Advance Search Product' => array(
        'Description' => 'Get Result Advance Search Product , use action getFeature for get result feature name in advance search'.
            ', and use action getResult for get result an advance search'.
        ' for this search send id_category array and id_feature_value array and min_cost and max_cost, and page of result',
        'Url' => '../api.php?X-OPERATION=AdvanceSearch&action=getFeature&FeatureName=پردازنده|حافظه داخلی|RAM|سیستم عامل',
    ),
    'Get Result Country' => array(
        'Description' => 'Get Result Country, if id_country equal null return all country',
        'Url' => '../api.php?X-OPERATION=GetCountry&id_country=112',
    ),
    'Get Result State' => array(
        'Description' => 'Get Result State',
        'Url' => '../api.php?X-OPERATION=GetState&id_country=112',
    ),
    'Result Address' => array(
        'Description' => 'Get/Add/Del/Update Result Address , for GET you need id_address in query string and action (get)'.
            ', for DEL you need id_address in body and action (del)'.
            ', for UPDATE you need id_address in body and all parameters and action (update)'.
            ', for ADD you need all parameters , !!! You need Login.',
        'Url' => '../api.php?X-OPERATION=Address&action=get&id_address=5',
    ),
    'Mail Alert' => array(
        'Description' => 'Get/Add/Del Result Mail Alert , for GET you need id_product and id_customer in query string and action (get)'.
            ', for DEL you need id_product and id_customer in body and action (del)'.
            ', for ADD you need id_product and id_customer and action add , !!! You need Login.',
        'Url' => '../api.php?X-OPERATION=MailAlert&action=get&pId=10',
    ),
    'Get Discount' => array(
        'Description' => 'Get Discount for user !!! You need Login.',
        'Url' => '../api.php?X-OPERATION=GetDiscount',
    ),
    'Get History Order' => array(
        'Description' => 'Get History Order for user !!! You need Login.',
        'Url' => '../api.php?X-OPERATION=GetHistoryOrder',
    ),
    'Get History Order Details' => array(
        'Description' => 'Get History Order Details for user with id_order !!! You need Login.',
        'Url' => '../api.php?X-OPERATION=GetHistoryOrderDetails&id_order=30',
    ),
    'Comparison Product' => array(
        'Description' => 'You can comparison product each other upto 5.use action Get for get result product compare, and action add for adding product for compare.',
        'Url' => '../api.php?X-OPERATION=Comparison&action=get',
    ),
);

?>