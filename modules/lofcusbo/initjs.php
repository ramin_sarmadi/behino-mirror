<script type="text/javascript">
var buttons = { previous:$('#cusbocarousel<?php echo $blockid;?> .lof-previous') ,
						next:$('#cusbocarousel<?php echo $blockid;?> .lof-next') };
 $('#cusbocarousel<?php echo $blockid;?>').lofJSidernews( { interval : <?php echo  (int) $params->get('interval','5000');?>,
										direction:'<?php echo  $params->get('etype','slide');?>',					 
										easing			: '<?php echo  $params->get('effect','easeInOutQuad');?>',
										duration		: <?php echo (int) $params->get('duration',600);?>,
										auto		 	: <?php echo (int) $params->get('autostart','1');?>,
										
										maxItemDisplay  : 3,
										startItem:<?php echo (int) $params->get('startitem','0');?>,
										navPosition     : 'horizontal', // horizontal
										navigatorHeight : null,
										navigatorWidth  : null,
										mainWidth:'<?php echo  (int)$moduleWidth; ?>',
										buttons			: buttons} );	
</script>
 