<?php
/**
 * $ModDesc
 * 
 * @version		$Id: helper.php $Revision
 * @package		modules
 * @subpackage	$Subpackage
 * @copyright	Copyright (C) May 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>. All rights reserved.
 * @website 	htt://landofcoder.com
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
    define('_CAN_LOAD_FILES_',1);
}
if( !class_exists('LofCusboProductDataSource', false) ){  
	class LofCusboProductDataSource extends LofDataSourceBase{
	    /**
		 * @var string $__name;
		 *
		 * @access private
		 */
		var $__name = 'product';
                
        /**
		 * override method: get list image from articles.
		 */
		function getListByParameters( $params, $pparams ){
			$products = self::_getListv14($params, $pparams);
            if( empty($products) ) return array();
			return $products;			
		} 

		/**
        * Get list in prestashop v14
        */
		private function _getListv14($params, $pparams){
			global $cookie;	
			$status  = (string)$params->get("status", '4,2,5');
			$product = null;
			$is 	 =  (int)$params->get("related",1);
			$pid 	 = (int)Tools::getValue('id_product');
			$where   = " AND  oh.id_order_state IN(".$status.")   ";
			if( (int)$params->get('manycats',1) == 0 ){
				$product = new Product( $pid, true, $cookie->id_lang );
			  	$where .= " AND  id_category_default=".(int) $product->id_category_default;
			}
			if(_PS_VERSION_ < "1.5"){	
				$products = self::getProductsV14( $where,0, $params->get("limit_items",12), "p.id_product" ); 
			}else{
				$products = self::getProductsV15( $where,0, $params->get("limit_items",12), "p.id_product" ); 
			}
			$isThumb  = $params->get( 'auto_renderthumb',1);
            $maxDesc  = $params->get( 'des_max_chars',60 );
			$output   = array();
			
			foreach ( $products as &$product ) {    		    	        
    			$product['description']=substr(trim(strip_tags($product['description_short'])),0, $maxDesc);
                $product['price'] = (Tools::displayPrice($product['price']));                                 
                $product = $this->parseImages( $product,$params );                
                $product = $this->generateImages( $product, $params );    
				$output[$product['id_product']] = $product;
	        }   
 		
		 	
			 if( (count($output) < $params->get("limit_items",12)) && $is ){
				 if(!is_object($product) ){
					 $product = new Product( (int)Tools::getValue('id_product'), true, $cookie->id_lang );
				 }
				$rproducts  = array();
				$tags = $product->tags;
				if( isset($tags[$cookie->id_lang]) && is_array($tags[$cookie->id_lang]) ) {
					$tmp = array();
					foreach( $tags[$cookie->id_lang]  as $tag ){
						$tmp[] = pSQL( $tag );
					}
					if(_PS_VERSION_ < "1.5"){	
						$ps = $this->getProductsByTag( $tmp, (int)$params->get("limit_items",12 )-count($output)  );
					}else{
						$ps = $this->getProductsByTagV15( $tmp, (int)$params->get("limit_items",12 )-count($output)  );
					}
					foreach( $ps as $product ){
						if( !array_key_exists($product['id_product'], $output) && $product['id_product'] != $pid ) {
							$product['description']=substr(trim(strip_tags($product['description_short'])),0, $maxDesc);
							$product['price'] = (Tools::displayPrice($product['price']));                                 
							$product = $this->parseImages( $product,$params );                
							$product = $this->generateImages( $product, $params );    
							$output[$product['id_product']] = $product;
						}/* endif */
					} /* endforeach */
				}/* if */
			 }
			
			 unset($products);
			return $output;	
		}				
		
		/**
		 *
		 */
		public function getProductsByTag ($tags, $limit, $associated=true ){
			global $cookie;
			$id_lang =$cookie->id_lang;
			$id_country = (int)Country::getDefaultCountryId();
			
			return Product::getProductsProperties($id_lang, Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT DISTINCT p.id_product, p.*, pa.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`, il.`legend`, m.`name` AS manufacturer_name, tl.`name` AS tax_name, t.`rate`, cl.`name` AS category_default, DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 AS new,
					(p.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1)) AS orderprice       
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product`
				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (p.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
														   AND tr.`id_country` = '.(int)$id_country.'
														   AND tr.`id_state` = 0)
				LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
				LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`		
				WHERE  p.`active` = 1 AND pl.id_lang = '.(int)($id_lang).' AND p.id_product !='. (int)Tools::getValue('id_product').'
		 
				'.('AND p.id_product '.($associated ? 'IN' : 'NOT IN')
							.' (SELECT pt.id_product FROM `'._DB_PREFIX_.'product_tag` pt LEFT JOIN '._DB_PREFIX_.'tag t ON t.id_tag= pt.id_tag 
								WHERE t.name IN ("'.implode('","',$tags).'") AND t.id_lang='.(int)$id_lang.' )').'
			ORDER BY pl.name LIMIT '.$limit));
		}
		
		public function getProductsByTagV15 ($tags, $limit, $associated=true ){
			global $cookie;
			$id_lang =$cookie->id_lang;
	 		
			$context = Context::getContext();
			$id_country = (int)($context->country->id);
			$front = true;
			if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
				$front = false;
				
			$sql = '
			SELECT DISTINCT p.id_product, p.*, pa.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`, il.`legend`, m.`name` AS manufacturer_name, tl.`name` AS tax_name, t.`rate`, cl.`name` AS category_default,
				DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),
					INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
						DAY)) > 0 AS new,
					(product_shop.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1)) AS orderprice       
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (product_shop.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group`
														   AND tr.`id_country` = '.(int)$id_country.'
														   AND tr.`id_state` = 0 AND tr.`zipcode_from` = 0)
				LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
				LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`		
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.' AND product_shop.`active` = 1 AND pl.id_lang = '.(int)($id_lang).' AND p.id_product !='. (int)Tools::getValue('id_product').'
					AND ((product_attribute_shop.id_product_attribute IS NOT NULL OR pa.id_product_attribute IS NULL) 
						OR (product_attribute_shop.id_product_attribute IS NULL AND pa.default_on=1)) '
						.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
				. ('AND p.id_product '.($associated ? 'IN' : 'NOT IN')
							.' (SELECT pt.id_product FROM `'._DB_PREFIX_.'product_tag` pt LEFT JOIN '._DB_PREFIX_.'tag t ON t.id_tag= pt.id_tag 
								WHERE t.name IN (\''.implode('\',\'',$tags).'\') AND t.id_lang='.(int)$id_lang.' )') .'
			ORDER BY pl.name LIMIT '.$limit;
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS( $sql );
			return Product::getProductsProperties($id_lang, $result);
		}
	
	
        /**
        * Get data source: 
        */
    	function getProductsV14($where='', $limiStart=0, $limit=12, $order=''){		
    		global $cookie, $link;
        	$id_lang = intval($cookie->id_lang);
        	$product_id = (int)Tools::getValue('id_product');
			$id_country = (int)Country::getDefaultCountryId();
 	
			// echo 'SELECT distinct(id_order) FROM '._DB_PREFIX_.'order_detail WHERE product_id='.$product_id .'';die;
    		$sql = '
    		SELECT DISTINCT p.id_product, p.*, pa.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`, il.`legend`,   tl.`name` AS tax_name, t.`rate`, cl.`name` AS category_default,
				DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 AS new,
    			(p.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1)) AS orderprice       
    		FROM `'._DB_PREFIX_.'order_detail` od 
			INNER JOIN `'._DB_PREFIX_.'orders` o ON od.id_order=o.id_order 
			LEFT JOIN `'._DB_PREFIX_.'order_history` oh ON od.`id_order` = oh.`id_order`	
    		LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = od.`product_id`
			LEFT JOIN  `'._DB_PREFIX_.'category_product` cp ON  p.`id_product`=cp.`id_product`
    		LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
    		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (p.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)($id_lang).')
    		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
    		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
    		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
    		LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
    		                                           AND tr.`id_country` = '.(int)$id_country.'
    	                                           	   AND tr.`id_state` = 0)
    	    LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
    		LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)($id_lang).')
    		WHERE  p.`active` = 1 '.$where 
				. ' AND p.id_product !='.(int)$product_id
				. ' AND o.id_customer IN( SELECT distinct(o.id_customer) FROM '._DB_PREFIX_.'order_detail  od INNER JOIN '._DB_PREFIX_.'orders o ON o.id_order=od.id_order WHERE product_id='.$product_id .' )'
				. ' AND oh.`id_order_history` = ( SELECT MAX(`id_order_history`) FROM `'
															 	._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = od.`id_order` GROUP BY moh.`id_order` ) ' ;		
				
    		$sql .= ' ORDER BY '.$order
    		.' LIMIT '.$limiStart.','.$limit;       
    
    		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);		
    		return Product::getProductsProperties($id_lang, $result);
    	}
		
    	function getProductsV15($where='', $limiStart=0, $limit=12, $order=''){		
    		global $cookie, $link;
        	$id_lang = intval($cookie->id_lang);
        	$product_id = (int)Tools::getValue('id_product');
			
			$context = Context::getContext();
			$id_country = (int)($context->country->id);
			$front = true;
			if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
				$front = false;
 	
			// echo 'SELECT distinct(id_order) FROM '._DB_PREFIX_.'order_detail WHERE product_id='.$product_id .'';die;
    		$sql = '
    		SELECT DISTINCT p.id_product, p.*, product_shop.*, pa.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`, il.`legend`,   tl.`name` AS tax_name, t.`rate`, cl.`name` AS category_default,
				DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),
					INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
						DAY)) > 0 AS new,
    			(product_shop.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1)) AS orderprice      
    		FROM `'._DB_PREFIX_.'order_detail` od 
			INNER JOIN `'._DB_PREFIX_.'orders` o ON od.id_order=o.id_order 
			LEFT JOIN `'._DB_PREFIX_.'order_history` oh ON od.`id_order` = oh.`id_order`	
    		LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = od.`product_id`
			'.Shop::addSqlAssociation('product', 'p').'
			LEFT JOIN  `'._DB_PREFIX_.'category_product` cp ON  p.`id_product`=cp.`id_product`
    		LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
			'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
			'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'
    		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (product_shop.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
    		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).Shop::addSqlRestrictionOnLang('pl').')
    		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
    		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
    		LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group` AND tr.`id_country` = '.(int)$id_country.'
    	                                           	   AND tr.`id_state` = 0 AND tr.`zipcode_from` = 0)
    	    LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
    		LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)($id_lang).')
    		WHERE  product_shop.`id_shop` = '.(int)$context->shop->id.' AND product_shop.`active` = 1 '.$where 
				. ' AND p.id_product !='.(int)$product_id
				. ' AND o.id_customer IN( SELECT distinct(o.id_customer) FROM '._DB_PREFIX_.'order_detail  od INNER JOIN '._DB_PREFIX_.'orders o ON o.id_order=od.id_order WHERE product_id='.$product_id .' )'
				. ' AND oh.`id_order_history` = ( SELECT MAX(`id_order_history`) FROM `'
															 	._DB_PREFIX_.'order_history` moh WHERE moh.`id_order` = od.`id_order` GROUP BY moh.`id_order` ) 
				AND ((product_attribute_shop.id_product_attribute IS NOT NULL OR pa.id_product_attribute IS NULL) 
					OR (product_attribute_shop.id_product_attribute IS NULL AND pa.default_on=1))'
					.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '') ;		
				
    		$sql .= ' ORDER BY '.$order
    		.' LIMIT '.$limiStart.','.$limit;       
    
    		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);		
    		return Product::getProductsProperties($id_lang, $result);
    	}
        
        /**
		 * get main image and thumb
		 *
		 * @param poiter $row .
		 * @return void
		 */
		public  function parseImages( $product, $params ){
			global $link;
            
            $isRenderedMainImage = 	$params->get("cre_main_size",0);
			if(_PS_VERSION_ <= "1.5.0.17")
				$mainImageSize       =  $params->get("main_img_size",'home');
			else
				$mainImageSize       =  $params->get("main_img_size",'home_allinmart');
            
            if( $isRenderedMainImage ) { 
				if((int)Configuration::get('PS_REWRITING_SETTINGS') == 1){
					$product["mainImge"] = $this->getImageLink($product["link_rewrite"], $product["id_image"] );
				}else{
					$product["mainImge"] = $link->getImageLink($product["link_rewrite"], $product["id_image"] );
				}
	        } else{
	        	$product["mainImge"] = $link->getImageLink($product["link_rewrite"], $product["id_image"], $mainImageSize ); 
	        }
            $product["thumbImge"] = $product["mainImge"];

            return $product; 
		}
	}
}
?>