{if (!empty($pages))}
<div class="lof-cusbo block cusbo_block">
	
    	<h4 class="box-header">{$moduleText}</h4>
     <div class="lof-inner block_content">   
    <div id="cusbocarousel{$blockid}" class="box-content" style="width:{$moduleWidth}; height:{$moduleHeight};">
		<div class="preload"><div></div></div>    
        <ul    class="lof-main-wapper">
        {foreach from=$pages  key=key item=products}
         	<li class="page-wrapper"  style="width:{$moduleWidth}; height:{$moduleHeight};">
            {foreach from=$products key=i item=item}  
                <div class="item-wrapper"  style="width:{$itemWidth}%"><div class="lof-inner">
                    <a href="{$item.link}"   class="product_img_link product_image">
                        <img src="{$item.mainImge}" alt="" />
                    </a>
                     <h5><a href="{$item.link}"   class="product_link">{$item.name}</a></h5>
                     <p>{$item.description}</p> 
                     <p class="box-price">
						<!--
                        {if $item.specific_prices}
                        {assign var='specific_prices' value=$item.specific_prices}
                        {if $specific_prices.reduction_type == 'percentage' && ($specific_prices.from == $specific_prices.to OR ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $specific_prices.to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $specific_prices.from))}
                        <span class="reduction">(-{$specific_prices.reduction*100|floatval}%)</span>
                        {/if}
                        {/if}
						-->
                        <span class="price">{if !$priceDisplay}{$item.price}{else}{displayWtPrice p=$item.price_tax_exc}{/if}</span>


                      </p>
                    
                    
                    <div class="bottom-wrap">	
                     {if ($showCartButton)}
                         {if (($item.quantity > 0 OR $item.allow_oosp))}
                           <a class="exclusive lof-add-cart ajax_add_to_cart_button" rel="ajax_id_product_{$item.id_product}" href="{$site_url}cart.php?add&amp;id_product={$item.id_product}&amp;token={$token}"><span>{l s='Add to cart' mod='lofcusbo'}</span></a>
                        {else}
                            <span class="lof-add-cart exclusive">{l s='Add to cart' mod='lofcusbo'}</span></a>
                        {/if}
                    {/if}
                    	{if ($showDetailButton)}
                        <a class="button" href="{$item.link}">{l s='Detail' mod='lofcusbo'}</a>
                    	{/if}
                    </div>
                   </div> 
                </div> 
                {if ( ($i+1) % $maxItemsPerRow == 0 AND $i < count($products)-1 )}
                	<div class="clr clearfix"></div>
              	 {/if}
              
            {/foreach}
            	</li>
         {/foreach}
         </ul>       
         
         <!-- END MAIN CONTENT --> 
         {if (count($pages)>1)}
    <!-- NAVIGATOR -->
        <div class="lof-navigator-wapper">
              <div class="button-control"><span></span></div>	
              <div class="lof-navigator-outer">
                    <ul class="lof-navigator">
                   	{foreach from=$pages  key=key item=products}
                       <li><span>{$key+1}</span></li>
                    {/foreach}      		
                    </ul>
              </div>
             
          		<div onclick="return false" href="" class="lof-next">Next</div>
                   <div onclick="return false" href="" class="lof-previous">Previous</div>
         </div> 
  <!----------------- --------------------->
  		 {/if}
    </div>
    <div class="clr clearfix"></div>
   </div> 
   <div class="clr clearfix"></div>
</div>
{/if}