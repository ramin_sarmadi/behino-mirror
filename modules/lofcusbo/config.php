<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
?>
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.css";?>" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/farbtastic/farbtastic.css";?>" type="text/css" media="screen" charset="utf-8" />
<script type="text/javascript" src="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/farbtastic/farbtastic.js";?>"></script>
<script type="text/javascript" src="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.js";?>"></script>
<script type="text/javascript">
  $(document).ready(function() {    
   	$('.it-addrow-block .add').each( function( idx , item ){    	  
        $(item).bind('click',function(e){
			var name        = $(item).attr('id').replace('btna-','');            
            var div         = $('<div class="row"></div>');
            var spantext    = $('<span class="spantext"></span>');
            var span        = $('<span class="remove"></span>');
            var input       = $('<input type="text" name="'+name+'[]" value=""/>');
                     			           
			var parent = $(item).parent().parent();
                        
            div.append(spantext);
            div.append(input);
            div.append(span);
                        
            parent.append(div);
            number = parent.find('input').length;                  
            spantext.html(parent.find('input').length);			
            
			span.bind('click',function(){ 
				if( span.parent().find('input').value ) {
					if( confirm('Are you sure to remove this') ) {
						span.parent().remove(); 
					}
				} else {
					span.parent().remove(); 
				}				
			} );				 			
        });
	});
    
	$('.it-addrow-block .remove').bind('click',function(events){	    
	    parent = $(this).parent();        
		if( parent.find('input').value ) {
			if( confirm('Are you sure to remove this') ) {
				parent.remove();
			}
		}else {
			parent.remove();
		}		
	});
        
    $('#demo').hide();
    var f = $.farbtastic('#picker');  
    var selected;
    $('.colorwell')
      .each(function () { f.linkTo(this); $(this).css('opacity', 0.75); })
      .focus(function() {
        //$('#picker').show();
        if (selected) {
          $(selected).css('opacity', 0.75).removeClass('colorwell-selected');
        }
        f.linkTo(this);
        //p.css('opacity', 1);
        $(selected = this).css('opacity', 1).addClass('colorwell-selected');
      });      
  });
</script>
<h3><?php echo $this->l('Customers Who Bought This Also Bought');?></h3>
<?php 
//register Yes - No Lang
$yesNoLang = array("0"=>$this->l('No'),"1"=>$this->l('Yes'));
$postYArr  = array("bottom"=>$this->l('Bottom'),"top"=>$this->l('Top'));
$postXArr  = array("right"=>$this->l('Right'),"left"=>$this->l('Left'));
$fileType  = array("image"=>$this->l('Image'),"flash"=>$this->l('Flash'));
$targetArr = array("_blank"=>$this->l('Blank'),"_parent"=>$this->l('Parent'),"_self"=>$this->l('Self'),"_top"=>$this->l('Top'));
$imagePos  = array("left"=>$this->l('Left'),"center"=>$this->l('Center'),"right"=>$this->l('Right'));
 
?>
<form action="<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>" method="post" id="lofform">
 <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
 
  <fieldset>
    <legend><img src="../img/admin/contact.gif" /><?php echo $this->l('Global Setting'); ?></legend>
    <div class="lof_config_wrrapper clearfix">
      <ul>
      	<?php 
				echo $this->_params->inputTag("module_title",$this->getParamValue("module_title","Customers Who Bought This Also Bought"),$this->l('Module Title'),' size="80" class="text_area"','class="row "','');
			?>
    
        <?php 
            echo $this->_params->selectTag("module_theme",$themes,$this->getParamValue("module_theme",'default'),$this->l('Theme - Layout'),'class="inputbox"', 'class="row" title="'.$this->l('Select a theme').'"');                  
           
        ?>   
        <?php
			 echo $this->_params->radioBooleanTag("show_detail_button", $yesNoLang, $this->getParamValue("show_detail_button",1),$this->l('Show View Button'),'class="select-option"','class="row"','', '' );
			  echo $this->_params->radioBooleanTag("show_cart_button", $yesNoLang, $this->getParamValue("show_cart_button",1),$this->l('Show Button Add to Cart'),'class="select-option"','class="row"','', '' );
			  
			  echo $this->_params->radioBooleanTag("cre_main_size", $yesNoLang, $this->getParamValue("cre_main_size",1),$this->l('Create Size of Main Image'),'class="select-option"','class="row"','',$this->l("You can create a new size or use available size."));
            $tispoptions = array(
                'lof-none'=>$this->l('None'),
                'lof-tooltip'=>$this->l('Tooltip'),
                'lof-tipbox'=>$this->l('TipBox')
            );
			
			 $mainImgSize = array();
            foreach ($formats as $k=> $format):
                $mainImgSize[$format['name']] = $format['name'].'('.$format['width']."x".$format['height'].')';
            endforeach;  
			
             echo $this->_params->selectTag("main_img_size",$mainImgSize,$this->getParamValue("main_img_size","thickbox"),$this->l('Main Image Size'),'class="inputbox select-group"','class="row cre_main_size-0"','',$this->l("You can create a new size via Menu <b>Preferences/Image</b>."));
            echo $this->_params->inputTag("main_height",$this->getParamValue("main_height",100),$this->l('Main Image Height'),'class="text_area"','class="row cre_main_size-1"','');
            echo $this->_params->inputTag("main_width",$this->getParamValue("main_width",150),$this->l('Main Image Width'),'class="text_area"','class="row cre_main_size-1"','');
            
			
			   echo $this->_params->radioBooleanTag("delCaImg", $yesNoLang,$this->getParamValue("delCaImg",0),$this->l('Delete cache folder'),'','class="row"','',$this->l('select it if you want to delete the image folder cache'));             
             
			   echo $this->_params->inputTag("module_height",$this->getParamValue("module_height","320"),$this->l('Module Height'),'class="text_area"','class="row"','');
               echo $this->_params->inputTag("module_width",$this->getParamValue("module_width","556"),$this->l('Module Width'),'class="text_area"','class="row"','');			   
				echo $this->_params->inputTag("item_prow",$this->getParamValue("item_prow","2"),$this->l('Max Item Per Row'),'class="text_area"','class="row"','');	
				echo $this->_params->inputTag("item_ppage",$this->getParamValue("item_ppage","4"),$this->l('Max Item On Page'),'class="text_area"','class="row"','');
				
				
        ?>
      </ul>
    </div>
  </fieldset>
  
  
  <fieldset>
    <legend><img src="../img/admin/contact.gif" /><?php echo $this->l('Data Source Setting'); ?></legend>
    <div class="lof_config_wrrapper clearfix">
      <ul>
       
        	   	                   	        
         	<?php  
				 echo $this->_params->selectTag("orderstatus[]",$pstatus,$this->getParamValue("status",'4,2,5'),$this->l('Order Status'),' size="10" multiple="multiple" class="inputbox"', 'class="row" title="'.$this->l('Select a theme').'"');
			?>
            
         <?php 
		  echo $this->_params->radioBooleanTag("manycats", $yesNoLang,$this->getParamValue("manycats",1),$this->l('In Many Category'),'class="select-option"','class="row"','',$this->l('Display Products In One or Many Categories. If Selected No => only products of this product category will be showed'));
		  echo $this->_params->radioBooleanTag("related", $yesNoLang,$this->getParamValue("related",1),$this->l('Show Related Products(By Tags)'),'class="select-option"','class="row"','',$this->l('If products are not enough to display, this option will be used, pre-defined Related Products will be added to the list. The Relation is specified by Tags'));
		 ?>   
        <li class="row module_group-product">
            
		
			<?php 
                   
            
               $arrOder = array('p.date_add'=>$this->l('Date Add'),'p.date_add DESC'=>$this->l('Date Add DESC'),
                                'name'=>$this->l('Name'),'name DESC'=>$this->l('Name DESC'),
                                'quantity'=>$this->l('Quantity'),'quantity DESC'=>$this->l('Quantity DESC'),
                                'p.price'=>$this->l('Price'),'p.price DESC'=>$this->l('Price DESC'));
               echo $this->_params->inputTag("limit_items",$this->getParamValue("limit_items",12),$this->l('Limit Items'),'class="text_area"','','class="row module_group-product"');                               
               echo $this->_params->selectTag("order_by",$arrOder,$this->getParamValue("order_by","p.date_add"),$this->l('Order By'),'class="inputbox select-group"','','class="row home_sorce-selectcat"');               
           
               echo $this->_params->inputTag("des_max_chars",$this->getParamValue("des_max_chars","60"),$this->l('Description Max Chars'),'class="text_area"','','class="row module_group-product"');                                                            
 
                                
            ?>                    
      </ul>
    </div>
  </fieldset>
    
 <fieldset class="lof-fieldset">
    <legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Effect Setting'); ?></legend>
    <div class="lof_config_wrrapper clearfix">
      <ul>
      <?php $effectArra = 
            	array("easeInQuad"=>"easeInQuad","easeOutQuad"=>"easeOutQuad","easeInOutQuad"=>"easeInOutQuad"
	            	 ,"easeInCubic"=>"easeInCubic","easeOutCubic"=>"easeOutCubic","easeInOutCubic"=>"easeInOutCubic"
	            	 ,"easeInQuart"=>"easeInQuart","easeOutQuart"=>"easeOutQuart","easeInOutQuart"=>"easeInOutQuart"
	            	 ,"easeInQuint"=>"easeInQuint","easeOutQuint"=>"easeOutQuint","easeInOutQuint"=>"easeInOutQuint"
	            	 ,"easeInSine"=>"easeInSine","easeOutSine"=>"easeOutSine","easeInOutSine"=>"easeInOutSine"
	            	 ,"easeInExpo"=>"easeInExpo","easeOutExpo"=>"easeOutExpo","easeInOutExpo"=>"easeInOutExpo"
	            	 ,"easeInCirc"=>"easeInCirc","easeOutCirc"=>"easeOutCirc","easeInOutCirc"=>"easeInOutCirc"	            	 
	            	 ,"easeInElastic"=>"easeInElastic","easeOutElastic"=>"easeOutElastic","easeInOutElastic"=>"easeInOutElastic"
            		 ,"easeInBack"=>"easeInBack","easeOutBack"=>"easeOutBack","easeInOutBack"=>"easeInOutBack"
            		 ,"easeInBounce"=>"easeInBounce","easeOutBounce"=>"easeOutBounce","easeInOutBounce"=>"easeInOutBounce"
            	);            	
            ?>
        <?php
            $options = array(
                'opacity'=>$this->l('Opacity'),
                'slide'=>$this->l('Sliding')
            ); 
			 echo $this->_params->inputTag("startitem",$this->getParamValue("startitem",0),$this->l('Animation Duration'),'class="text_area"','class="row "','',$this->l("numbering start  from: 0 -> n"));
            echo $this->_params->selectTag("etype",$options,$this->getParamValue("etype","slide"),$this->l('Effect Type'),'class="inputbox"','class="row"','','');
			 echo $this->_params->selectTag("effect",$effectArra,$this->getParamValue("effect","easeInOutQuart"),$this->l('Slider Effect'),'class="inputbox"','class="row"','','');
            echo $this->_params->inputTag("interval",$this->getParamValue("interval",5000),$this->l('SlideShow Speed'),'class="text_area"','class="row "','',$this->l("Set Speed in miniseconds."));
            echo $this->_params->inputTag("duration",$this->getParamValue("duration",500),$this->l('Animation Duration'),'class="text_area"','class="row "','',$this->l("Set duration in miniseconds."));
            echo $this->_params->radioBooleanTag("autostart", $yesNoLang,$this->getParamValue("autostart",1),$this->l('Auto Start'),'class="select-option"','class="row"','','');
        ?>
      </ul>
    </div>
  </fieldset>
   
   
<br />
  <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
  	<fieldset><legend><img src="../img/admin/comment.gif" alt="" title="" /><?php echo $this->l('Information');?></legend>    	
    	<ul>
    	     <li>+ <a target="_blank" href="http://landofcoder.com/our-porfolios/prestashop/item/53-prestashop-lof-slider-module.html"><?php echo $this->l('Detail Information');?></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/supports.html"><?php echo $this->l('Forum support');?></a></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/submit-request.html"><?php echo $this->l('Customization/Technical Support Via Email');?>.</a></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/help-desk.html"><?php echo $this->l('Ticket Support');?></a></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/prestashop/guides"><?php echo $this->l('User Guide');?></a></li>
        </ul>
        <br />
        @copyright: <a href="http://landofcoder.com">LandOfCoder.com</a>
    </fieldset>
</form>
