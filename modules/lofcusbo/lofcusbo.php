<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}    
/**
 * lofcusbo Class
 */	
class lofcusbo extends Module
{
	/**
	 * @var LofParams $_params;
	 *
	 * @access private;
	 */
	private $_params = '';	
	
	/**
	 * @var array $_postErrors;
	 *
	 * @access private;
	 */
	private $_postErrors = array();		
	
	/**
	 * @var string $__tmpl is stored path of the layout-theme;
	 *
	 * @access private 
	 */	
	
   /**
    * Constructor 
    */
	function __construct()
	{
		$this->name = 'lofcusbo';
		parent::__construct();			
		$this->tab = 'LandOfCoder';				
		$this->version = '1.3';
		$this->module_key = 'c4180582a733cd1892c6fe8700d43443';
		$this->displayName = $this->l('Customers Who Bought This Also Bought');
		$this->description = $this->l('Customers Who Bought This Also Bought');
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' ) && !class_exists("LofParams", false) ){
			if( !defined("LOF_LOAD_LIB_PARAMS") ){				
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' );
				define("LOF_LOAD_LIB_PARAMS",true);
			}
		}		
		$this->_params = new LofParams( $this->name );		   
	}
  
   /**
    * process installing 
    */
	function install(){		
		if (!parent::install())
			return false;
		if(!$this->registerHook('productfooter'))
			return false;			
		if(!$this->registerHook('header'))
			return false;	
			
		return true;
	}
	
	/*
	 * register hook right comlumn to display slide in right column
	 */
	function hookrightColumn($params)
	{		
		return $this->processHook( $params,"rightColumn");
	}
	
	/*
	 * register hook left comlumn to display slide in left column
	 */
	function hookleftColumn($params)
	{	
		
		return $this->processHook( $params,"leftColumn");
	}
	
	function hooktop($params)
	{		
		return $this->processHook( $params,"top");
	}
	
	function hookfooter($params)
	{		
		return $this->processHook( $params,"footer");
	}
	
	function hookcontenttop($params)
	{ 		
		return $this->processHook( $params,"contenttop");
	}
	
	
	function hookHeader($params)
	{  
		 if(_PS_VERSION_ <="1.4"){							
			$header = '
 			<link type="text/css" rel="stylesheet" href="'.($this->_path).'tmpl/'. $this->getParamValue('module_theme','default').'/assets/pstyle.css'.'" />			
			';			
			return $header;			
		}elseif(_PS_VERSION_ < "1.5"){				
			Tools::addCSS( ($this->_path).'tmpl/'. $this->getParamValue('module_theme','default').'/assets/pstyle.css', 'all');
			Tools::addJS( ($this->_path).'assets/jquery.jcarousel.js', 'all');
		}else{
			$this->context->controller->addCSS( ($this->_path).'tmpl/'. $this->getParamValue('module_theme','default').'/assets/pstyle.css', 'all');
			$this->context->controller->addJS( ($this->_path).'assets/jquery.jcarousel.js', 'all');
		}
		
	}
  	
	function hooklofTop($params){
		return $this->processHook( $params,"lofTop");
	}
		
	function hookProductfooter($params)
	{
		return $this->processHook( $params,"home");
	}
 
	/**
    * Proccess module by hook
    * $pparams: param of module
    * $pos: position call
    */
	function processHook($pparams, $pos="home"){
		global $smarty;                  
		//load param
		if(_PS_VERSION_ < "1.5"){	
			if(  basename($_SERVER['PHP_SELF']) != "product.php" ){	return ; }
		}else{
			if(Tools::getValue('controller') != 'product' || !Tools::getValue('id_product'))	
				return;
		}
		$params = $this->_params;
		$site_url = Tools::htmlentitiesutf8(((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off") ? "https" : "http"). "://" . $_SERVER['HTTP_HOST'].__PS_BASE_URI__) ;
		
		if(_PS_VERSION_ <="1.4"){
			// create thumbnail folder	 						
			$thumbPath = _PS_IMG_DIR_.$this->name;
			
			if( !file_exists($thumbPath) ) {
				mkdir( $thumbPath, 0777 );			
			};
			$thumbUrl = $site_url."img/".$this->name;
		}else{			
			// create thumbnail folder	 			
			$thumbPath = _PS_CACHEFS_DIRECTORY_.$this->name;
			if( !file_exists(_PS_CACHEFS_DIRECTORY_) ) {
				mkdir( _PS_CACHEFS_DIRECTORY_, 0777 );  			
			}; 
			if( !file_exists($thumbPath) ) {
				mkdir( $thumbPath, 0777 );			
			};
			$thumbUrl = $site_url."cache/cachefs/".$this->name;			
		}	
		
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/group_base.php' ) && !class_exists("LofDataSourceBase", false) ){
			if( !defined("LOF_LOAD_LIB_GROUP") ) {
				require_once( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/group_base.php' );
				define("LOF_LOAD_LIB_GROUP",true);
			}
		}
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/phpthumb/ThumbLib.inc.php' ) && !class_exists('PhpThumbFactory', false)){						
			if( !defined("LOF_LOAD_LIB_PHPTHUMB") ) {
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/phpthumb/ThumbLib.inc.php' );	
				define("LOF_LOAD_LIB_PHPTHUMB",true);
			}			
		}
		
        $params->set( 'aurenthumb',0);      
        // get call object to process getting source
		$source =  	"product";    
		
		$path = dirname(__FILE__).'/libs/groups/'.strtolower($source)."/".strtolower($source).'.php';       				
		if( !file_exists($path) ){
			return array();	
		}
        require_once $path;
        $objectName = "LofCusbo".ucfirst($source)."DataSource";
	 	$object = new $objectName();		
        $object->setThumbPathInfo($thumbPath,$thumbUrl)
               ->setImagesRendered( array( 'mainImage' => array( (int)$params->get( 'main_width', 60 ), (int)$params->get( 'main_height', 60 )) ) );
        $products = $object->getListByParameters( $params, $pparams );
 		
	
		if(!$products){		return '';	}
		$maxItemsPerRow = (int)$params->get( 'item_prow', 2 );
		$maxPages		= (int)$params->get( 'item_ppage', 4 );
		$pages 			= array_chunk( $products, $maxPages  ); 
		$totalPages 	= count($pages);
		$itemWidth = 100/$maxItemsPerRow -0.1;
		$tmp 			= $params->get( 'module_height', '320' );
		$moduleHeight   =  ( $tmp=='auto' ) ? 'auto' : (int)$tmp.'px';
		$tmp            = $params->get( 'module_width', '560' );
		$moduleWidth    =  ( $tmp=='auto') ? 'auto': (int)$tmp.'px';
		$centerWidth = 0;
		if($tmp != "auto") $centerWidth    = ($tmp-536)."px";
		if($centerWidth <= 0)$centerWidth = 0;		
        $moduleBg 		= $params->get('moduleBg','#000000');
        $moduleBg 		= $moduleBg == "none" ? "none": $moduleBg;
        $preload        = $params->get('preload','false') ? " preload":"";        
		$theme 			= $params->get( 'module_theme' , 'default');
		$openTarget 	= $params->get( 'open_target', 'parent' );
		$class 			= $params->get( 'navigator_pos', 0 ) ? '':'lof-snleft';
		$blockid        = $this->id;
		$showButtons 	= $params->get('display_button',1);
		$prfSlide       = $pos;
        				
		// template asignment variables
		$smarty->assign( array(	
							  'moduleText'    => $params->get('module_title',"Customers Who Bought This Also Bought"),
						      'modName'         => $this->name,
                              'prfSlide'        => $prfSlide,
							  'blockid' 		=> $blockid,	
							  'moduleWidth'     => $moduleWidth,
							  'centerWidth'		=> $centerWidth,
							  'moduleHeight'	=> $moduleHeight,
							  'params'		    => $params,							  	
					 		  //'priceWithoutReduction_tax_excl' => Tools::ps_round($special['price_without_reduction'], 2),	
                              'group'		    => $params->get('module_group','product'),
                              'target'		    => $params->get('open_target','_blank'),  
							  'pages'  			=> $pages , 
							  'itemWidth' => $itemWidth,
							  'maxItemsPerRow'=>$maxItemsPerRow,
					 		  'showCartButton' =>$params->get('show_cart_button','1'),
							  'showDetailButton' => $params->get('show_detail_button','1'),
                              'preload'         => $preload,
                              'site_url'         => $site_url,
							  
						));
		//$smarty->assign( array('homeSize' => Image::getSize('thickbox')));
		$open_target = $params->get('open_target','_blank');
		$smarty->assign( 'target','$target="'.$open_target.'"');
		// render for content layout of module
		$content = '';
		ob_start();
	       require( dirname(__FILE__).'/initjs.php' );		
	       $content = ob_get_contents();
	    ob_end_clean();		
	    return $this->display(__FILE__, 'tmpl/'.$theme.'/default.tpl').$content;					
	}
        
   /**
    * Get list of sub folder's name 
    */
	public function getFolderList( $path ) {
		$items = array();
		$handle = opendir($path);
		if (! $handle) {
			return $items;
		}
		while (false !== ($file = readdir($handle))) {
			if (is_dir($path . $file))
				$items[$file] = $file;
		}
		unset($items['.'], $items['..'], $items['.svn']);
		
		return $items;
	}
	
   /**
    * Render processing form && process saving data.
    */	
	public function getContent()
	{
		$html = "";
		if (Tools::isSubmit('submit'))
		{
			$this->_postValidation();

			if (!sizeof($this->_postErrors))
			{													
		        $definedConfigs = array(
					'module_theme'  	  => '',	          
					'module_group'      => '',		          
					//file group                  
					'orderstatus'       => '',
					'file_path'         => '', 
					'item_ppage'         => '4', 
					'module_title' => '',
					'item_prow'         => '2', 
					'manycats' => '',
					'related' => '1',
					'show_detail_button' =>'',
					'show_cart_button' => '',
 					//product group
					'home_sorce'        => '',
					'order_by'          => '',
					'limit_items'       => '12',  
					'des_max_chars'     => '60',                  
					'module_width' 	  =>'556',
					'module_height' 	  =>'320',
					'main_height'       => '100',
					'main_width'        => '150',
					'cre_main_size'     => '',
					'main_img_size'     => '',
					'status'        => ''    ,
					'autostart' => '',
					'duration' => '',
					'interval' => '',
					'effect' => '',
					'etype' => '',
					'startitem' => '' 
		        );
               
		        foreach( $definedConfigs as $config => $key ){
		            if(strlen($this->name.'_'.$config)>=32){
		              echo $this->name.'_'.$config;
		            }else{
		              Configuration::updateValue($this->name.'_'.$config, Tools::getValue($config), true);  
		            } 		      		
		    	}
				
                
				 if(Tools::getValue('orderstatus')){
    		        if(in_array("",Tools::getValue('orderstatus'))){
    		          $catList = "";
    		        }else{
    		          $catList = implode(",",Tools::getValue('orderstatus'));  
    		        } 
                    Configuration::updateValue($this->name.'_status', $catList, true);
                }
                 
                	 
		        $delText = '';	
		        if(Tools::getValue('delCaImg')){					
					if(_PS_VERSION_ <="1.4"){						
						$cacheFol = _PS_IMG_DIR_.$this->name;												
					}else{			
						$cacheFol = _PS_CACHEFS_DIRECTORY_.$this->name;							
					}			                    		
					if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/group_base.php' ) && !class_exists("LofDataSourceBase", false) ){
						if( !defined("LOF_LOAD_LIB_GROUP") ) {						  
							require_once( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/group_base.php' );
							define("LOF_LOAD_LIB_GROUP",true);
						}
					}
					if (LofDataSourceBase::removedir($cacheFol)){
						$delText =  $this->l('. Cache folder has been deleted');
					}else{
						$delText =  $this->l('. Cache folder can\'t deleted');
					}  
				}
		        $html .= '<div class="conf confirm">'.$this->l('Settings updated').$delText.'</div>';
				
			}
			else
			{
				foreach ($this->_postErrors AS $err)
				{
					$html .= '<div class="alert error">'.$err.'</div>';
				}
			}
			// reset current values.
				
		 $this->_params = new LofParams( $this->name );
		  

		  
		}
	
		return $html.$this->_getFormConfig();
	}
	
	/**
	 * Render Configuration From for user making settings.
	 *
	 * @return context
	 */
	private function _getFormConfig(){		
		global $cookie;
		
		$html = '';
		 
	    $formats = ImageType::getImagesTypes( 'products' );
	    $themes=$this->getFolderList( dirname(__FILE__)."/tmpl/" );
        $groups=$this->getFolderList( dirname(__FILE__)."/libs/groups/" );
	
		$statuses = OrderState::getOrderStates($cookie->id_lang);
		$pstatus = array();
		foreach( $statuses as $status ){
			$pstatus[$status["id_order_state"]] = $status["name"]; 
		}
			
	    ob_start();
	    include_once dirname(__FILE__).'/config.php'; 
	    $html .= ob_get_contents();
	    ob_end_clean(); 
		return $html;
	}
    
	/**
     * Process vadiation before saving data 
     */
	private function _postValidation()
	{
		if (!Validate::isCleanHtml(Tools::getValue('module_height')))
			$this->_postErrors[] = $this->l('The module height you entered was not allowed, sorry');
		if (!Validate::isCleanHtml(Tools::getValue('module_width')))
			$this->_postErrors[] = $this->l('The module width you entered was not allowed, sorry');
		if (!Validate::isCleanHtml(Tools::getValue('des_max_chars')) || !is_numeric(Tools::getValue('des_max_chars')))
			$this->_postErrors[] = $this->l('The description max chars you entered was not allowed, sorry');	
		if (!Validate::isCleanHtml(Tools::getValue('main_height')) || !is_numeric(Tools::getValue('main_height')))
			$this->_postErrors[] = $this->l('The Main Image Height you entered was not allowed, sorry');
		if (!Validate::isCleanHtml(Tools::getValue('main_width')) || !is_numeric(Tools::getValue('main_width')))
			$this->_postErrors[] = $this->l('The Main Image Width you entered was not allowed, sorry');        
        if (!Validate::isCleanHtml(Tools::getValue('limit_items')) || !is_numeric(Tools::getValue('limit_items')))
			$this->_postErrors[] = $this->l('The limit items you entered was not allowed, sorry');
         							
	}
	
   /**
    * Get value of parameter following to its name.
    * 
	* @return string is value of parameter.
	*/
	public function getParamValue($name, $default=''){
		return $this->_params->get( $name, $default );	
	}	  	  		
} 