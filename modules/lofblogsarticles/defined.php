<?php
/**
 * @name defined.php
 * @author landOfCoder
 * @todo defined some path and link
 */

// *************** SOME PATHS ******************************
define('LOFBLOGSARTICLES_ROOT', _PS_MODULE_DIR_ . 'lofblogsarticles/');
define('LOFBLOGSARTICLES_THEME', LOFBLOGSARTICLES_ROOT.'themes/');

// ***************** SOME LINKS ***************************************

define('LOFBLOGSARTICLES_BASE_URI', __PS_BASE_URI__ . 'modules/lofblogsarticles/');
define('LOFBLOGSARTICLES_THEME_URI', LOFBLOGSARTICLES_BASE_URI.'themes/');

?>
