<?php

/*
 * render items :
 */

class LofblogsArticlesViewDefault extends LofblogsArticlesView {

    function homeItems($items, $params) {
        $result = array();
        $itemPerCategory = $params->get('itemPerCat', 0);
        foreach ($items as $item) {            
            if ($itemPerCategory <= 0 || (!isset($result[$item['id_lofblogs_category']]) || count($result[$item['id_lofblogs_category']]['items']) < $itemPerCategory)) {
                
                $text = trim(strip_tags($item['short_desc'])) ? trim(strip_tags($item['short_desc'])) : trim(strip_tags($item['content']));
                $item['introtext'] = lofContentHelper::limitString($text, $params->get('homeIntro', 200));
                $result[$item['id_lofblogs_category']]['title'] = $item['categorytitle'];
                $result[$item['id_lofblogs_category']]['link'] = lofContentHelper::getCategoryLink($item['id_lofblogs_category'], $item['categoryalias']);
                $result[$item['id_lofblogs_category']]['items'][] = $item;
            }
        }
        return $result;
    }
    
    function otherItems($items, $params) { 
        foreach ($items as $k =>$item) {
            $text = trim(strip_tags($item['short_desc'])) ? trim(strip_tags($item['short_desc'])) : trim(strip_tags($item['content']));
            $items[$k]['introtext'] = lofContentHelper::limitString($text, $params->get('otherIntro', 200));
        }
        return $items;
    }

}