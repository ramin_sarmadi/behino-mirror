<div class="lofblogsarticles_slide" style="{$slideStyle}">
<ul id="lofblogsarticles_{$position}" class="lofblogsarticles_slide_container">
    {foreach from=$items item=item}
        {if $item.image}
        <li class="lofblogsarticles_item">
            <a class="itemTitle" href="{$item.link}" title="{$item.title}" >{$item.title}</a>   
            <a href="{$item.link}" title="{$item.title}" >
                <img class="item_thumb"  src="{$thumbUri}{$item.image}" alt="{$item.title}" />    
            </a>                        
            <div class="item_descrition">
                {$item.introtext}
            </div>
            <div class="clearfix"></div>
        </li>      
        {/if}
    {/foreach}    
</ul>  
<div class="clearfix"></div>
{if $params.showControl}
<a class="prev" id="prevBtn" href="#"><span>prev</span></a>
<a class="next" id="nextBtn" href="#"><span>next</span></a>
{/if}

{if $params.showPaginator}
<div class="pagination" id="paginator"></div>
{/if}
</div>
<div class="clearfix"></div>