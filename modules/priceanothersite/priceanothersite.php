<?php


class PriceAnotherSite extends Module
{

    public function __construct()
    {
        $this->name = 'priceanothersite';
        $this->tab = 'pricing_promotion';
        $this->version = 1.0;
        $this->author = 'Ahmad Khorshidi';
        $this->need_instance = 0;

        parent::__construct();

        PriceAnotherSite::initProcess();

        $this->displayName = $this->l('Price Another Site Module');
        $this->description = $this->l('Response product price from another site ');
    }

    public function install()
    {
        if (!parent::install() || !$this->registerHook('header') || $this->_createTables() == false )
            return false;

    }

    function uninstall()
    {
        $db = Db::getInstance();
        $query = 'DROP TABLE `'._DB_PREFIX_.'product_price_site_lang`';
        $query1 = 'DROP TABLE `'._DB_PREFIX_.'product_price_site_link`';
        $query2 = 'DROP TABLE `'._DB_PREFIX_.'product_price_site_link_log`';
        $query3 = 'DROP TABLE `'._DB_PREFIX_.'product_price_site_productname`';
        $result = $db->Execute($query);
        $result1 = $db->Execute($query1);
        $result2 = $db->Execute($query2);
        $result3 = $db->Execute($query3);
        if ((!parent::uninstall()) OR (!$result) OR (!$result1) OR (!$result2) OR (!$result3) )
            return false;
        return true;
    }

    public function _createTables()
    {
        $db = Db::getInstance();

        $query = '
        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_price_site_lang` (
      `id_product_price_site` int(5) NOT NULL AUTO_INCREMENT,
      `id_lang` int(10) unsigned NOT NULL,
      `name` varchar(255) NOT NULL,
       PRIMARY KEY (`id_product_price_site`,`id_lang`)
       ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

			';
        $query1 = '

        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_price_site_link` (
          `id_product_price_site_link` bigint(10) NOT NULL AUTO_INCREMENT,
          `id_product_price_site` int(5) NOT NULL,
          `id_product_price_site_productname` bigint(10) NOT NULL,
          `link` text,
          PRIMARY KEY (`id_product_price_site_link`,`id_product_price_site`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

			';
        $query2 = '

        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_price_site_link_log` (
          `id_product_price_site_link_log` bigint(12) NOT NULL AUTO_INCREMENT,
          `id_product_price_site_link` bigint(10) NOT NULL,
          `price_log` mediumtext,
          PRIMARY KEY (`id_product_price_site_link_log`,`id_product_price_site_link`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

			';
        $query3 = '

        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'product_price_site_productname` (
          `id_product_price_site_productname` bigint(10) NOT NULL AUTO_INCREMENT,
          `id_lang` int(10) unsigned NOT NULL,
          `name` varchar(255) NOT NULL,
          PRIMARY KEY (`id_product_price_site_productname`,`id_lang`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

			';


        $result = $db->Execute($query);
        $result1 = $db->Execute($query1);
        $result2 = $db->Execute($query2);
        $result3 = $db->Execute($query3);
        if ((!$result)||(!$result1)||(!$result2)||(!$result3))
            return false;
        return true;
    }

    public function initProcess()
    {
        $Action_Form='';
        $resultUpdate =array();
        $Action_Data='none';
        $html='';
        $id_lang=(int)$this->context->language->id;
        $this->context->controller->addCSS($this->_path.'default.css', 'all');
        if((Tools::isSubmit('addLinkProduct'.$this->name))
            || ((Tools::isSubmit('update'.$this->name))&&((int)Tools::getValue('id_product_price_site_link')))){
            $Action_Form='addLinkProduct';
            $Action_Data='insertProductLink';
            $id_product_price_site_link = (int)Tools::getValue('id_product_price_site_link');
            if($id_product_price_site_link){
                $Action_Form='updateLinkProduct';
                $Action_Data='updateProductLink';

                $this->context->smarty->assign(array(
                    'id_product_price_site_link' => $id_product_price_site_link
                ));

                $resultUpdate = $this->getListContent($id_lang,$id_product_price_site_link);
            }

        }elseif((Tools::isSubmit('addSite'.$this->name))
            || ((Tools::isSubmit('update'.$this->name))&&((int)Tools::getValue('id_product_price_site')))){
            $Action_Form='addSite';
            $Action_Data='insertSiteName';
            $id_product_price_site_lang = (int)Tools::getValue('id_product_price_site');
            if($id_product_price_site_lang){
                $Action_Form='updateSite';
                $Action_Data='updateSiteName';

                $this->context->smarty->assign(array(
                'id_product_price_site_lang' => $id_product_price_site_lang
                ));

                $resultUpdate = $this->getListSite($id_lang,$id_product_price_site_lang);
            }

            $helper_site = $this->initList('product_price_site_lang');
            $html.=$helper_site->generateList($this->getListSite((int)$this->context->language->id), $this->fields_list_1);

        }elseif((Tools::isSubmit('addProduct'.$this->name))
            || ((Tools::isSubmit('update'.$this->name))&&((int)Tools::getValue('id_product_price_site_productname')))){
            $Action_Form='addProduct';
            $Action_Data='insertProductName';

            if(Tools::getValue('InsertAction')=='NotExist')
            {
                $resInsert=$this->InsertNotExistAtt();

                if($resInsert){
                    $html .= '<div class="conf confirm">'.($this->l('Insert Not Exist Product Attribute record for '.$this->name.' successfully')).'</div>';
                }else{
                    $html .= '<div class="error" >'.($this->l('Insert Not Exist Product Attribute record for '.$this->name.' <b>unsuccessfully</b>')).'</div>';
                }

            }




            $id_product_price_site_productname = (int)Tools::getValue('id_product_price_site_productname');
            if($id_product_price_site_productname){
                $Action_Form='updateProduct';
                $Action_Data='updateProductName';

                $this->context->smarty->assign(array(
                    'id_product_price_site_productname' => $id_product_price_site_productname
                ));

                $resultUpdate = $this->getListProduct($id_lang,$id_product_price_site_productname);
            }

            $helper_product = $this->initList('product_price_site_productname');
            $html.=$helper_product->generateList($this->getListProduct((int)$this->context->language->id), $this->fields_list_2);

        }elseif(Tools::isSubmit('ViewData'.$this->name)){
            $Action_Form='ViewData';

            $helper_product = $this->initList('viewData');
            $html.=$helper_product->generateList(array(), array());



            $this->context->smarty->assign(array(
             'ProductLinkArr' => $this->getListContent($this->context->language->id)
            ));
        }


        $this->context->smarty->assign(array(
         'LinkSiteName' => $this->l('Add Link site')
        ,'IconLinkSiteName' => '../modules/'.$this->name.'/logo-link.gif'
        ,'ProductName' => 'Add Product Name'
        ,'IconProductName' => '../modules/'.$this->name.'/logo-product.gif'
        ,'LinkProductName' => 'Add Product Link'
        ,'IconLinkProductName' => '../modules/'.$this->name.'/logo-linkproduct.gif'
        ,'SiteArr' => $this->getListSite($this->context->language->id)
        ,'ProductArr' => $this->getListProduct($this->context->language->id)
        ,'Action_Form' => $Action_Form
        ,'address' => __PS_BASE_URI__
        ,'html' => $html
        ,'resultUpdate' => $resultUpdate
        ,'id_lang' => $this->context->language->id
        ,'LinkSave' =>  AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&'.$Action_Data
        //,'ProductListSiteArr' => $this->getListProductSite($id_lang)
        ,'ProductAttributeArr' => $this->getListProductAttribute($id_lang)

        ));


    }


    public function getContent()
    {

        $html = '';
        //$content = $this->displayForm();

        //return $content;
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $date_add = date("Y-m-d H:i:s");
        $id_product_price_site_lang = (int)Tools::getValue('id_product_price_site');
        $id_product_price_site_link = (int)Tools::getValue('id_product_price_site_link');
        $id_product_price_site_productname = (int)Tools::getValue('id_product_price_site_productname');
        $id_lang=$this->context->language->id;
        if ((Tools::isSubmit('insertSiteName')) || (Tools::isSubmit('updateSiteName')))
        {
            $NameSite = (string)(Tools::getValue('NameSite'));
            $LinkSite = (string)(Tools::getValue('LinkSite'));
            $TagSite = (string)(Tools::getValue('TagSite'));
            $TagSite1 = (string)(Tools::getValue('TagSite1'));
            $TagSite2 = (string)(Tools::getValue('TagSite2'));

            if ($id_product_price_site_lang)
            {//for edit

                $dataArr = array(
                 'id_lang'=>$id_lang
                ,'name'=>$NameSite
                ,'link'=>$LinkSite
                ,'tags'=>$TagSite
                ,'tags1'=>$TagSite1
                ,'tags2'=>$TagSite2
                );

                $result_add=$this->update('product_price_site_lang',$id_lang,$dataArr,'id_product_price_site='.$id_product_price_site_lang);

                if($result_add=='OK'){
                    $html .= '<div class="conf confirm">'.($this->l('Update Special Product record for '.$NameSite.' successfully')).'</div>';
                }else{
                    $html .= '<div class="error">'.($this->l('<font color="#000000">Update Special Product record for '.$NameSite.' <b>unsuccessfully</b></font>')).'</div>';
                }

            }
            else
            {


                if(($NameSite)&&($LinkSite)&&($TagSite)){

                    $dataArr = array(
                    'id_lang'=>$id_lang
                    ,'name'=>$NameSite
                    ,'link'=>$LinkSite
                    ,'tags'=>$TagSite
                    ,'tags1'=>$TagSite1
                    ,'tags2'=>$TagSite2
                    );

                    $result=$this->add('product_price_site_lang',$id_lang,$dataArr,'id_product_price_site','name="'.$NameSite.'"');

                    if($result=='OK')
                    {
                        $html .= '<div class="conf confirm">'.$this->l('Add new record into database.').'</div>';
                    }elseif($result=='100'){

                        $html .= '<div class="error">'.$this->l('Miss Data.').'</div>';

                    }elseif($result=='200'){

                        $html .= '<div class="error">'.$this->l('Duplicate Data.').'</div>';

                    }else{
                        $html .= '<div class="error">'.$this->l('You have error in add data.').'</div>';
                    }

                }else{
                    $html .= '<div class="error">'.$this->l('Miss Data.').'</div>';
                }



            }


        }
        elseif((Tools::isSubmit('insertProductName')) || (Tools::isSubmit('updateProductName'))){

            $ProductName = (string)(Tools::getValue('Product_Name'));
            $id_product_attribute = (int)Tools::getValue('id_product_attribute');

            if ($id_product_price_site_productname)
            {//for edit

                $dataArr = array(
                 'id_lang'=>$id_lang
                ,'id_product_attribute'=>$id_product_attribute
                ,'name'=>$ProductName

                );


                $result_add=$this->update('product_price_site_productname',$id_lang,$dataArr,'id_product_price_site_productname='.$id_product_price_site_productname);

                if($result_add=='OK'){
                    $html .= '<div class="conf confirm">'.($this->l('Update Special Product record for '.$ProductName.' successfully')).'</div>';
                }else{
                    $html .= '<div class="error">'.($this->l('<font color="#000000">Update Special Product record for '.$ProductName.' <b>unsuccessfully</b></font>')).'</div>';
                }

            }
            else
            {

                if(($ProductName)&&($id_product_attribute)){

                    $dataArr = array(
                     'id_lang'=>$id_lang
                    ,'id_product_attribute'=>$id_product_attribute
                    ,'name'=>$ProductName

                    );

                    $result=$this->add('product_price_site_productname',$id_lang,$dataArr,'id_product_price_site_productname','name="'.$ProductName.'"');

                    if($result=='OK')
                    {
                        $html .= '<div class="conf confirm">'.$this->l('Add new record into database.').'</div>';
                    }elseif($result=='100'){

                        $html .= '<div class="error">'.$this->l('Miss Data.').'</div>';

                    }elseif($result=='200'){

                        $html .= '<div class="error">'.$this->l('Duplicate Data.').'</div>';

                    }else{
                        $html .= '<div class="error">'.$this->l('You have error in add data.').'</div>';
                    }

                }else{
                    $html .= '<div class="error">'.$this->l('Miss Data.').'</div>';
                }



            }

        }
        elseif((Tools::isSubmit('insertProductLink')) || (Tools::isSubmit('updateProductLink'))){
            $Product_Link = (string)(Tools::getValue('Product_Link'));
            $id_product_price_site_productname = (int)(Tools::getValue('id_product_price_site_productname'));
            $id_product_price_site = (int)(Tools::getValue('id_product_price_site'));

            //use in insert
            $id_product_price_site_checkbox=(Tools::getValue('id_product_price_site_checkbox'));

            if ($id_product_price_site_link)
            {//for edit

                $dataArr = array(
                 'id_product_price_site'=>$id_product_price_site
                ,'id_product_price_site_productname'=>$id_product_price_site_productname
                ,'link'=>$Product_Link
                );


                $result_add=$this->update('product_price_site_link',$id_lang,$dataArr,'id_product_price_site_link='.$id_product_price_site_link);

                if($result_add=='OK'){
                    $html .= '<div class="conf confirm">'.($this->l('Update Special Product record for id '.$id_product_price_site_link.' successfully')).'</div>';
                }else{
                    $html .= '<div class="error">'.($this->l('<font color="#000000">Update Special Product record for id '.$id_product_price_site_link.' <b>unsuccessfully</b></font>')).'</div>';
                }

            }
            else
            {//for insert


                if(($id_product_price_site_productname)&&($id_product_price_site_checkbox)){

                    foreach ($id_product_price_site_checkbox as $id_product_price_site){

                        $Product_Link = (string)(Tools::getValue('Product_Link_'.$id_product_price_site));
                        $Site_Name = (string)(Tools::getValue('Site_Name_'.$id_product_price_site));
                        $dataArr = array(
                         'id_product_price_site'=>$id_product_price_site
                        ,'id_product_price_site_productname'=>$id_product_price_site_productname
                        ,'link'=>$Product_Link
                        );

                        $result=$this->add('product_price_site_link',$id_lang,$dataArr,'id_product_price_site_link','link="'.$Product_Link.'" AND id_product_price_site='.$id_product_price_site);

                        if($result=='OK')
                        {
                            $html .= '<div class="conf confirm">'.$this->l('Add new record into database. Site Name:<b style="color: #003399">'.$Site_Name.'').'</b></div>';
                        }elseif($result=='100'){

                            $html .= '<div class="error">'.$this->l('Miss Data. Site Name:<b style="color: #003399">'.$Site_Name.'').'</b></div>';

                        }elseif($result=='200'){

                            $html .= '<div class="error">'.$this->l('Duplicate Data. Site Name:<b style="color: #003399">'.$Site_Name.'').'</b></div>';

                        }else{
                            $html .= '<div class="error">'.$this->l('You have error in add data. Site Name:<b style="color: #003399">'.$Site_Name.'').'</b></div>';
                        }

                    }





                }else{
                    $html .= '<div class="error">'.$this->l('Miss Data.').'</div>';
                }



            }

        }


        if (Tools::isSubmit('update'.$this->name) || Tools::isSubmit('addProduct'.$this->name)
            || Tools::isSubmit('addSite'.$this->name) || Tools::isSubmit('addLinkProduct'.$this->name))
        {


            return $this->display(__FILE__, 'priceanothersite.tpl');


        }
        elseif (Tools::isSubmit('delete'.$this->name))
        {
            if($id_product_price_site_lang){

                $table='product_price_site_lang';
                $where='id_product_price_site='.$id_product_price_site_lang;

            }elseif($id_product_price_site_link){

                $table='product_price_site_link';
                $where='id_product_price_site_link='.$id_product_price_site_link;

            }elseif($id_product_price_site_productname){

                $table='product_price_site_productname';
                $where='id_product_price_site_productname='.$id_product_price_site_productname;

            }

            $result = $this->deleted($table,$where);

            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&action=deleted&result='.$result.'&token='.Tools::getAdminTokenLite('AdminModules'));

        }
        elseif (Tools::getValue('action')=='deleted')
        {

            $result = Tools::getValue('result');

            if($result=='OK')
            {
                $html .= '<div class="conf confirm">'.$this->l('Deleted record from database.').'</div>';
            }elseif($result=='100'){

                $html .= '<div class="error">'.$this->l('Miss Data.').'</div>';

            }else{
                $html .= '<div class="error">'.$this->l('You have error in add data.').'</div>';
            }

            $helper = $this->initList('product_price_site_link');
            //$helper_site = $this->initList('product_price_site_lang');
            //$helper_product = $this->initList('product_price_site_productname');
            $html.=$helper->generateList($this->getListContent((int)$this->context->language->id), $this->fields_list_0);
            //$html.=$helper_site->generateList($this->getListSite((int)$this->context->language->id), $this->fields_list_1);
            //$html.=$helper_product->generateList($this->getListProduct((int)$this->context->language->id), $this->fields_list_2);
            return $html;
        }
        elseif(Tools::isSubmit('ViewData'.$this->name)){

            return $this->display(__FILE__, 'priceanothersite.tpl');

        }
        else
        {
            $helper = $this->initList('product_price_site_link');
            //$helper_site = $this->initList('product_price_site_lang');
            //$helper_product = $this->initList('product_price_site_productname');
            $html.=$helper->generateList($this->getListContent((int)$this->context->language->id), $this->fields_list_0);
            //$html.=$helper_site->generateList($this->getListSite((int)$this->context->language->id), $this->fields_list_1);
            //$html.=$helper_product->generateList($this->getListProduct((int)$this->context->language->id), $this->fields_list_2);

            return $html;
        }



    }

    protected function initList($form)
    {
        if($form=='product_price_site_link'){
            $this->fields_list_0 = array(
                'id_product_price_site_link' => array(
                    'title' => $this->l('Id'),
                    'width' => 30,
                    'type' => 'text',
                ),
                'site_name' => array(
                    'title' => $this->l('Site Name'),
                    'width' => 100,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                ),
                'product_name' => array(
                    'title' => $this->l('Product Name'),
                    'width' => 180,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                ),
                'link' => array(
                    'title' => $this->l('Link'),
                    'width' => 240,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                )
            );
            $helper = new HelperList();
            $helper->shopLinkType = '';
            $helper->simple_header = true;
            $helper->identifier = 'id_product_price_site_link';
            $helper->actions = array('edit', 'delete');
            $helper->show_toolbar = true;
            $helper->imageType = 'jpg';
            $helper->toolbar_btn['addLink'] =  array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addLinkProduct'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Add new Link Product')
            );
            $helper->toolbar_btn['addProduct'] =  array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addProduct'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Add new Product')
            );
            $helper->toolbar_btn['addSite'] =  array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addSite'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Add new Site')
            );
            $helper->toolbar_btn['preview'] =  array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&ViewData'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('view data')
            );
            $helper->toolbar_btn['back'] =  array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('back')
            );


            $helper->title = $this->displayName.' - Product Link';
            $helper->table = $this->name;
            $helper->token = Tools::getAdminTokenLite('AdminModules');
            $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        }
        elseif($form=='product_price_site_lang'){
            $this->fields_list_1 = array(
                'id_product_price_site' => array(
                    'title' => $this->l('Id'),
                    'width' => 30,
                    'type' => 'text',
                ),
                'name' => array(
                    'title' => $this->l('Site Name'),
                    'width' => 100,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                ),
                'link' => array(
                    'title' => $this->l('Link'),
                    'width' => 120,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                ),
                'tags' => array(
                    'title' => $this->l('Tags'),
                    'width' => 140,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                ),
                'tags1' => array(
                    'title' => $this->l('Tags1'),
                    'width' => 140,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                ),
                'tags2' => array(
                    'title' => $this->l('Tags2'),
                    'width' => 140,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                )
            );

            $helper = new HelperList();
            $helper->shopLinkType = '';
            $helper->simple_header = true;
            $helper->identifier = 'id_product_price_site';
            $helper->actions = array('edit', 'delete');
            $helper->show_toolbar = true;
            $helper->imageType = 'jpg';
            /* $helper->toolbar_btn['new'] =  array(
                 'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addSite'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                 'desc' => $this->l('Add new')
             );*/
            $helper->toolbar_btn['back'] =  array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('back')
            );



            $helper->title = $this->displayName.' - Site Name';
            $helper->table = $this->name;
            $helper->token = Tools::getAdminTokenLite('AdminModules');
            $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        }
        elseif($form=='product_price_site_productname'){
            $this->fields_list_2 = array(
                'id_product_price_site_productname' => array(
                    'title' => $this->l('Id'),
                    'width' => 30,
                    'type' => 'text',
                ),
                'name' => array(
                    'title' => $this->l('Product Name'),
                    'width' => 400,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                ),
                'ProductName' => array(
                    'title' => $this->l('Link to Product'),
                    'width' => 300,
                    'type' => 'text',
                    'filter_key' => 'a!lastname'
                )
            );

            $helper = new HelperList();
            $helper->shopLinkType = '';
            $helper->simple_header = true;
            $helper->identifier = 'id_product_price_site_productname';
            $helper->actions = array('edit', 'delete');
            $helper->show_toolbar = true;
            $helper->imageType = 'jpg';
            /* $helper->toolbar_btn['new'] =  array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addProduct'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Add new')
            );*/
            $helper->toolbar_btn['back'] =  array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('back')
            );
            $helper->toolbar_btn['duplicate'] =  array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addProductpriceanothersite&InsertAction=NotExist&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Insert Not Exist Attribute')
            );



            $helper->title = $this->displayName.' - Product Name';
            $helper->table = $this->name;
            $helper->token = Tools::getAdminTokenLite('AdminModules');
            $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        }
        elseif($form=='viewData'){
            $this->fields_list_3 = array(  );
            $helper = new HelperList();
            //$helper->shopLinkType = '';
            $helper->simple_header = true;
            // $helper->identifier = 'id';
            $helper->actions = array(/*'edit', 'delete'*/);
            $helper->show_toolbar = true;
            $helper->imageType = 'jpg';


            $helper->toolbar_btn['back'] =  array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('back')
            );



            $helper->title = $this->displayName.' - Result Site';
            $helper->table = $this->name;
            $helper->token = Tools::getAdminTokenLite('AdminModules');
            $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        }



        return $helper;
    }

    protected function getListContent($id_lang,$id_product_price_site_link =0)
    {
        $sub_query='';
        if($id_product_price_site_link>0)
        {
            $sub_query = 'AND ppsl.id_product_price_site_link='.$id_product_price_site_link;
        }

        $SQL = '
        SELECT ppsl.*, ppsln.name as site_name, ppsln.id_lang as site_lang, ppsp.name as product_name, ppsp.id_lang as product_lang
            FROM `'._DB_PREFIX_.'product_price_site_link` ppsl
            INNER JOIN `'._DB_PREFIX_.'product_price_site_lang` ppsln ON ppsln.`id_product_price_site` = ppsl.`id_product_price_site`
            INNER JOIN `'._DB_PREFIX_.'product_price_site_productname` ppsp ON ppsp.`id_product_price_site_productname` = ppsl.`id_product_price_site_productname`


			WHERE ppsln.`id_lang` = '.(int)$id_lang.' '.$sub_query.' ORDER BY ppsl.id_product_price_site ';

        $results= Db::getInstance()->executeS($SQL);
        /*
        foreach ($results as &$result){

            if ($result['date_add']!='0000-00-00 00:00:00')
            {

                $result['date_add_h'] = Pdate::convert_date_M_to_H($result['date_add']);

            }


        }
        */
        return $results;
    }

    protected function getListProductAttribute($id_lang,$id_product_attribute = 0,$id_product = 0)
    {
        $sub_query='';
        if($id_product>0)
        {
            $sub_query = ' WHERE p.id_product='.$id_product;
        }elseif($id_product_attribute>0){
            $sub_query = ' WHERE pa.id_product_attribute='.$id_product_attribute;
        }

        $SQL = '
        SELECT *, CONCAT(prd_name," --> ",attNameANDGroup) as AllNameAtt
        FROM (
        SELECT p.id_product,
            pl.name prd_name,
            pa.id_product_attribute,pa.price,p.id_category_default as category, pl.link_rewrite, p.ean13,
            (SELECT quantity FROM '._DB_PREFIX_.'stock_available sa
            WHERE sa.id_product=p.id_product and sa.id_product_attribute=pa.id_product_attribute ) as quantity,
            (
            SELECT GROUP_CONCAT(attName)
            FROM
            (
            SELECT
            CONCAT("(",agl.name," / ",al.name,")") as attName ,pac.id_attribute,a.id_attribute_group,pac.id_product_attribute
            FROM '._DB_PREFIX_.'product_attribute_combination pac
            INNER JOIN '._DB_PREFIX_.'attribute a ON pac.id_attribute=a.id_attribute
            INNER JOIN '._DB_PREFIX_.'attribute_group ag ON a.id_attribute_group=ag.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_group_lang agl ON ag.id_attribute_group=agl.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_lang al ON a.id_attribute=al.id_attribute
            ) as tmp WHERE id_product_attribute=pa.id_product_attribute
            ) as attNameANDGroup


            FROM '._DB_PREFIX_.'product_attribute pa
            INNER JOIN '._DB_PREFIX_.'product p ON pa.id_product=p.id_product
            INNER JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product
            '.$sub_query.'
            ) as tmpAll
            ORDER BY id_product
            ';

        $results =  Db::getInstance()->executeS($SQL);

        foreach ($results as &$result){

            if ((int)$result['id_product']!=0)
            {
                $result['link'] = $this->context->link->getProductLink((int)$result['id_product'], $result['link_rewrite'], $result['category'], $result['ean13']);
            }

        }

        return $results;
    }

    protected function getListSite($id_lang,$id_product_price_site =0)
    {
        $sub_query='';
        if($id_product_price_site>0)
        {
            $sub_query = 'AND ppsl.id_product_price_site='.$id_product_price_site;
        }

        $SQL = '
			SELECT *
			FROM `'._DB_PREFIX_.'product_price_site_lang` ppsl
			WHERE ppsl.`id_lang` = '.(int)$id_lang.' '.$sub_query;

        $results=Db::getInstance()->executeS($SQL);

      foreach ($results as &$result){

          if ($result['tags']!='')
          {

              $result['hash_tags'] = base64_encode($result['tags']);

          }
          if ($result['tags1']!='')
          {

              $result['hash_tags1'] = base64_encode($result['tags1']);

          }
          if ($result['tags2']!='')
          {

              $result['hash_tags2'] = base64_encode($result['tags2']);

          }


      }

        return $results;
    }

    protected function getListProduct($id_lang,$id_product_price_site_productname =0)
    {
        $sub_query='';
        if($id_product_price_site_productname>0)
        {
            $sub_query = 'AND ppsp.id_product_price_site_productname='.$id_product_price_site_productname;
        }

        $SQL = '
			SELECT *,
			(

			SELECT  CONCAT(prd_name," --> ",attNameANDGroup," <div style=\'color:#AA0000;\'>quantity:<b>",quantity,"</b> // Price:<b>",FORMAT(price, 0),"</b></div>") as AllNameAtt
            FROM (
            SELECT p.id_product,
            pl.name prd_name,
            pa.id_product_attribute,pa.price,p.id_category_default as category, pl.link_rewrite, p.ean13,
            (SELECT quantity FROM '._DB_PREFIX_.'stock_available sa
            WHERE sa.id_product=p.id_product and sa.id_product_attribute=pa.id_product_attribute ) as quantity,
            (
            SELECT GROUP_CONCAT(attName)
            FROM
            (
            SELECT
            CONCAT("(",agl.name," / ",al.name,")") as attName ,pac.id_attribute,a.id_attribute_group,pac.id_product_attribute
            FROM '._DB_PREFIX_.'product_attribute_combination pac
            INNER JOIN '._DB_PREFIX_.'attribute a ON pac.id_attribute=a.id_attribute
            INNER JOIN '._DB_PREFIX_.'attribute_group ag ON a.id_attribute_group=ag.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_group_lang agl ON ag.id_attribute_group=agl.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_lang al ON a.id_attribute=al.id_attribute
            ) as tmp WHERE id_product_attribute=pa.id_product_attribute
            ) as attNameANDGroup


            FROM '._DB_PREFIX_.'product_attribute pa
            INNER JOIN '._DB_PREFIX_.'product p ON pa.id_product=p.id_product
            INNER JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product

            ) as tmpAll
            WHERE id_product_attribute=ppsp.id_product_attribute
            ORDER BY id_product

			) as ProductName
			FROM `'._DB_PREFIX_.'product_price_site_productname` ppsp
			WHERE ppsp.`id_lang` = '.(int)$id_lang.' '.$sub_query.' ';

        return  Db::getInstance()->executeS($SQL);
    }


    protected function getListProductSite($id_lang,$id_product = 0)
    {
        $sub_query='';
        if($id_product>0)
        {
            $sub_query = 'AND pl.id_product='.$id_product;
        }

        $SQL = '
        SELECT (SELECT name FROM '._DB_PREFIX_.'category_lang WHERE id_category=c.id_parent) RootName,
            cl.name CategoryName,
            pl.name ProductName,
            pl.id_product
            FROM `'._DB_PREFIX_.'category_product` cp
            INNER JOIN '._DB_PREFIX_.'category c ON c.id_category=cp.id_category
            INNER JOIN '._DB_PREFIX_.'category_lang cl ON cl.id_category=c.id_category
            INNER JOIN '._DB_PREFIX_.'product_lang pl ON cp.id_product=pl.id_product
            WHERE c.is_root_category=0
            AND cl.id_lang='.(int)$id_lang.'
            '.$sub_query.'
            Order BY c.id_parent,c.id_category
             ';

        return  Db::getInstance()->executeS($SQL);
    }

    protected function deleted($table,$WhereDeleted)
    {
        if(($table)&&($WhereDeleted))
        {
        $SQL = '
        DELETE FROM `'._DB_PREFIX_.''.$table.'` WHERE '.$WhereDeleted.'
        ';

            Db::getInstance()->executeS($SQL);

            return 'OK'; //success deleted
        }

        return 100; //miss data

    }

    protected function update($table,$id_lang,$dataArr,$where)
    {

        if(($table)&&($dataArr)&&($where))
        {
            if((Db::getInstance()->update($table, $dataArr, $where)))
            {
                return 'OK';
            }else{
                return false;
            }

        }else{
            return '100'; //Miss data
        }

    }
    protected function add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam)
    {


        $SQL = '
			SELECT '.$selectCheckParam.'
			FROM `'._DB_PREFIX_.''.$table.'`
			WHERE '.$whereCheckParam.' ';

        $result = Db::getInstance()->executeS($SQL);

        if(count($result)==0)
        {
            /*
            $data = array(
                'parameter_wiki' => $parameter_wiki,
                'id_lang' => $id_lang,
                'description_wiki' => $description_wiki,
                'date_add' => $date_add,
                'status' => $status
            );
            */


            if(($table)&&($dataArr))
            {
                if((Db::getInstance()->insert($table, $dataArr)))
                {
                    return 'OK';
                }else{
                    return false;
                }

            }else{
                return '100'; //Miss data
            }

        }else{

            return '200'; //Duplicate record

        }

    }

    public function InsertNotExistAtt(){

        $SQL='
        INSERT INTO '._DB_PREFIX_.'product_price_site_productname (id_lang,id_product_attribute,name)
        SELECT 1,id_product_attribute, (
       SELECT CONCAT(prd_name," ",attNameANDGroup) AS PrdNameAtt FROM
        (
        SELECT p.id_product,

            pl.name prd_name,
            pa.id_product_attribute,pa.price,p.id_category_default AS category, pl.link_rewrite, p.ean13,
            (SELECT quantity FROM '._DB_PREFIX_.'stock_available sa
            WHERE sa.id_product=p.id_product AND sa.id_product_attribute=pa.id_product_attribute ) AS quantity,
            (
            SELECT GROUP_CONCAT(attName)
            FROM
            (
            SELECT
            CONCAT("(",agl.name," / ",al.name,")") AS attName ,pac.id_attribute,a.id_attribute_group,a.color,pac.id_product_attribute
            FROM '._DB_PREFIX_.'product_attribute_combination pac
            INNER JOIN '._DB_PREFIX_.'attribute a ON pac.id_attribute=a.id_attribute
            INNER JOIN '._DB_PREFIX_.'attribute_group ag ON a.id_attribute_group=ag.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_group_lang agl ON ag.id_attribute_group=agl.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_lang al ON a.id_attribute=al.id_attribute
            ) AS tmp WHERE id_product_attribute=pa.id_product_attribute
            ) AS attNameANDGroup


            FROM '._DB_PREFIX_.'product_attribute pa
            INNER JOIN '._DB_PREFIX_.'product p ON pa.id_product=p.id_product
            INNER JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product
            ) AS tmpQueryAtt
            WHERE tmpQueryAtt.id_product_attribute='._DB_PREFIX_.'product_attribute.id_product_attribute
            )

    FROM `'._DB_PREFIX_.'product_attribute` WHERE `id_product_attribute` NOT IN (
    SELECT * FROM (
    SELECT id_product_attribute
    FROM '._DB_PREFIX_.'product_price_site_productname
      WHERE EXISTS (

    SELECT id_product_attribute, CONCAT(prd_name," ",attNameANDGroup) AS PrdNameAtt FROM
            (
        SELECT p.id_product,

            pl.name prd_name,
            pa.id_product_attribute,pa.price,p.id_category_default AS category, pl.link_rewrite, p.ean13,
            (SELECT quantity FROM '._DB_PREFIX_.'stock_available sa
            WHERE sa.id_product=p.id_product AND sa.id_product_attribute=pa.id_product_attribute ) AS quantity,
            (
            SELECT GROUP_CONCAT(attName)
            FROM
            (
            SELECT
            CONCAT("(",agl.name," / ",al.name,")") AS attName ,pac.id_attribute,a.id_attribute_group,a.color,pac.id_product_attribute
            FROM '._DB_PREFIX_.'product_attribute_combination pac
            INNER JOIN '._DB_PREFIX_.'attribute a ON pac.id_attribute=a.id_attribute
            INNER JOIN '._DB_PREFIX_.'attribute_group ag ON a.id_attribute_group=ag.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_group_lang agl ON ag.id_attribute_group=agl.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_lang al ON a.id_attribute=al.id_attribute
            ) AS tmp WHERE id_product_attribute=pa.id_product_attribute
            ) AS attNameANDGroup


            FROM '._DB_PREFIX_.'product_attribute pa
            INNER JOIN '._DB_PREFIX_.'product p ON pa.id_product=p.id_product
            INNER JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product
            ) AS tmpQuery

	    WHERE tmpQuery.id_product_attribute='._DB_PREFIX_.'product_price_site_productname.id_product_attribute

                    )
                    ) AS  tmpIdAtt

                    )

        ';

        $results= Db::getInstance()->execute($SQL);

        if($results)
            return true;

        return false;

    }

    public function getLinkContent($id_lang,$id_product_price_site,$id_product_price_site_productname)
    {


        $SQL = '
        SELECT ppsl.link
            FROM `'._DB_PREFIX_.'product_price_site_link` ppsl
            INNER JOIN `'._DB_PREFIX_.'product_price_site_lang` ppsln ON ppsln.`id_product_price_site` = ppsl.`id_product_price_site`
            INNER JOIN `'._DB_PREFIX_.'product_price_site_productname` ppsp ON ppsp.`id_product_price_site_productname` = ppsl.`id_product_price_site_productname`
			WHERE ppsln.`id_lang` = '.(int)$id_lang.'
			AND ppsln.id_product_price_site='.$id_product_price_site.' AND ppsp.id_product_price_site_productname='.$id_product_price_site_productname;

        $results= Db::getInstance()->executeS($SQL);

        return $results[0]['link'];
    }

    public function scraping_site($url,$tag) {
        // create HTML DOM
        $html = file_get_html($url);
        //echo $tag;
        //echo $url;
        // get title
        $ret['Title'] = $html->find('title', 0)->innertext;

        // get rating
        /*digikala*///$ret['Price'] = $html->find('span[class="finalprice green"] ', 0)->innertext;
        /*alldigitall*/
        $ret['Price'] = $html->find($tag, 0)->innertext;
       /* $count=0;
        foreach($html->find($tag) as $element)
        {
            $ret['Price_'.$count] = $element->innertext . '<br>';
            $count++;
        }*/


        // clean up memory
        $html->clear();
        unset($html);

        return Tools::jsonEncode(array('results' => $ret));
    }


    public function hookHeader($params)
    {
        $this->context->controller->addCSS($this->_path.'default.css', 'all');

    }


}
