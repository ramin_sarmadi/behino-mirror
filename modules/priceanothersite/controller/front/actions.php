<?php

include_once(dirname(__FILE__) . '../include/Fn_priceanothersite.php');

class PriceAnotherSiteActionsModuleFrontController extends ModuleFrontController
{
	/**
	 * @var int
	 */
	public $id_product;
	public $id_product_attribute;

	public function init()
	{
		parent::init();

		require_once($this->module->getLocalPath().'priceanothersite.php');
		$this->id_product = (int)Tools::getValue('id_product');
		$this->id_product_attribute = (int)Tools::getValue('id_product_attribute');
	}

	public function postProcess()
	{
		if (Tools::getValue('process') == '1')
        {
            $this->scraping_site(Tools::getValue('urlsite'),Tools::getValue('tag'));
        }

	}

    public function scraping_site($url,$tag) {
        // create HTML DOM
        $html = file_get_html($url);
        //echo $tag;
        //echo $url;
        // get title
        $ret['Title'] = $html->find('title', 0)->innertext;

        // get rating
        /*digikala*///$ret['Price'] = $html->find('span[class="finalprice green"] ', 0)->innertext;
        /*alldigitall*/
        $ret['Price'] = $html->find($tag, 0)->innertext;
        /* $count=0;
         foreach($html->find($tag) as $element)
         {
             $ret['Price_'.$count] = $element->innertext . '<br>';
             $count++;
         }*/


        // clean up memory
        $html->clear();
        unset($html);

        return Tools::jsonEncode(array('results' => $ret));
    }



}