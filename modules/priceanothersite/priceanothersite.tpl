{if ($Action_Form=="addSite")||($Action_Form=="updateSite")}
    <fieldset>
        <legend><img src="{$IconLinkSiteName}"/> {$LinkSiteName} </legend>
        <form action="{$LinkSave}" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{$id_product_price_site_lang}" name="id_product_price_site">

            <div style="padding: 5px">
                <span class="span_bold_12">{l s='Name Site' mod='priceanothersite'}:</span>
                <input name="NameSite" value="{$resultUpdate[0]['name']}" maxlength="150" style="width: 250px">
            </div>
            <div style="padding: 5px">
                <span class="span_bold_12">{l s='Address Site' mod='priceanothersite'}:</span>
                <input name="LinkSite" value="{$resultUpdate[0]['link']}" maxlength="150" style="width: 250px">
            </div>
            <div style="padding: 5px">
                <span class="span_bold_12">{l s='Tag Site1' mod='priceanothersite'}:</span>
                <input name="TagSite" value="{$resultUpdate[0]['tags']|escape:html}" maxlength="150"
                       style="width: 450px; direction: ltr">
            </div>
            <div style="padding: 5px">
                <span class="span_bold_12">{l s='Tag Site2' mod='priceanothersite'}:</span>
                <input name="TagSite1" value="{$resultUpdate[0]['tags1']|escape:html}" maxlength="150"
                       style="width: 450px; direction: ltr">
            </div>
            <div style="padding: 5px">
                <span class="span_bold_12">{l s='Tag Site3' mod='priceanothersite'}:</span>
                <input name="TagSite2" value="{$resultUpdate[0]['tags2']|escape:html}" maxlength="150"
                       style="width: 450px; direction: ltr">
            </div>
            <div style="padding-left: 150px; padding-top: 10px">
                <input type="submit" value="{l s='Save' mod='priceanothersite'}" class="button" name="AddLink">
            </div>
        </form>
    </fieldset>
    <hr>
    {$html}
{elseif ($Action_Form=="addProduct")||($Action_Form=="updateProduct")}
    <fieldset>
        <legend><img src="{$IconProductName}"/> {$ProductName} </legend>
        <form action="{$LinkSave}" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{$id_product_price_site_productname}" name="id_product_price_site_productname">

            <div style="padding: 5px">
                <span class="span_bold_12">{l s='Product Name' mod='priceanothersite'}:</span>
                <input name="Product_Name" value="{$resultUpdate[0]['name']|escape:html}" maxlength="255"
                       style="width: 450px">
            </div>
            <div>
                <span class="span_bold_12">{l s='Link to Product Site' mod='priceanothersite'}:</span>
                <select name="id_product_attribute" dir="ltr">
                    <option value="-1">...</option>
                    {foreach from=$ProductAttributeArr item=ProductAttributeSite}
                        <option value="{$ProductAttributeSite['id_product_attribute']}"
                                {if $resultUpdate[0]['id_product_attribute']==$ProductAttributeSite['id_product_attribute']} selected="selected" {/if} >
                            {$ProductAttributeSite['id_product_attribute']}
                            . {$ProductAttributeSite['AllNameAtt']}  </option>
                    {/foreach}
                </select>
            </div>
            <div style="padding-left: 150px; padding-top: 10px">
                <input type="submit" value="{l s='Save' mod='priceanothersite'}" class="button" name="AddProduct">
            </div>
        </form>
    </fieldset>
    <hr>
    {$html}
{elseif ($Action_Form=="addLinkProduct")||($Action_Form=="updateLinkProduct")}
    <fieldset>
        <legend><img src="{$IconLinkProductName}"/> {$LinkProductName} </legend>
        <form action="{$LinkSave}" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{$id_product_price_site_link}" name="id_product_price_site_link">
            {if $resultUpdate[0]['id_product_price_site']!=0}
                <div>
                    <span class="span_bold_12">{l s='Product Link' mod='priceanothersite'}:</span>
                    <input name="Product_Link" value="{$resultUpdate[0]['link']|escape:html}" maxlength="2000"
                           style="width: 550px">
                </div>
            {/if}
            <div>
                <span class="span_bold_12">{l s='Product Name' mod='priceanothersite'}:</span>
                <select name="id_product_price_site_productname" dir="ltr">
                    {foreach from=$ProductArr item=Product}
                        <option value="{$Product['id_product_price_site_productname']}"
                                {if $resultUpdate[0]['id_product_price_site_productname']==$Product['id_product_price_site_productname']} selected="selected" {/if} >
                            {$Product['id_product_price_site_productname']}
                            . {$Product['name']}</option>
                    {/foreach}
                </select>
            </div>
            <div>
                <span class="span_bold_12">{l s='Site' mod='priceanothersite'}</span>
                <ul>
                    {foreach from=$SiteArr item=Site}
                        <li style="padding: 4px">
                            {if $resultUpdate[0]['id_product_price_site']!=0}
                                <input type="radio" value="{$Site['id_product_price_site']}"
                                       name="id_product_price_site"
                                        {if $resultUpdate[0]['id_product_price_site']==$Site['id_product_price_site']} checked {/if}/>
                            {/if}
                            <b style="color: #003399">{$Site['name']}</b>
                            {if $resultUpdate[0]['id_product_price_site']==0}
                                <div>
                                    <span class="span_bold_12">
                                    <input type="checkbox" value="{$Site['id_product_price_site']}"
                                           name="id_product_price_site_checkbox[]"/>
                                        {l s='Product Link' mod='priceanothersite'}:
                                    </span>
                                    <input name="Product_Link_{$Site['id_product_price_site']}" value=""
                                           maxlength="2000" style="width: 550px">
                                    <input type="hidden" name="Site_Name_{$Site['id_product_price_site']}"
                                           value="{$Site['name']}">
                                </div>
                                <hr>
                            {/if}
                        </li>
                    {/foreach}
                </ul>
            </div>
            <div style="padding-left: 150px; padding-top: 10px">
                <input type="submit" value="{l s='Save' mod='priceanothersite'}" class="button" name="AddProductLink">
            </div>
        </form>
    </fieldset>
{elseif $Action_Form=="ViewData"}
{*
{$SiteArr}
{$ProductArr}
<div dir="ltr">{$ProductLinkArr}</div>
*}
    {$html}
    <hr>
    <button id="start">شروع</button>
    <table border="1" style="width: 100%">
        <tr>
            <td style="width: 25px"><b>ID</b></td>
            <td><b>ProductName</b></td>
            {foreach from=$SiteArr item=Site}
                <td style="text-align: center"><b>{$Site['name']}</b></td>
            {/foreach}
        </tr>


        {foreach from=$ProductArr item=Products name=loopProduct}
            <tr>
                <td>{$Products.id_product_price_site_productname}</td>
                <td>
                    <div>{$Products['name']}</div>
                    <div style="border-bottom: 1px solid #999999; padding:2px"></div>
                    <div style="color: #0000AA; font-size: 11px">{if $Products['ProductName']!=NULL}{$Products['ProductName']}{else} {l s='No Product'} {/if}</div>
                </td>

                {foreach from=$SiteArr item=Site name=loopSite}
                    {assign var="countSite" value={$SiteArr|count} }
                    {assign var="number" value={math y=$smarty.foreach.loopProduct.iteration i=$countSite equation="y*i-i" } }
                    {assign var="number2" value={math num=$number j=$smarty.foreach.loopSite.iteration equation="j+num" } }
                    <td style="text-align: center">
                        {assign var="Link_URL" value=PriceAnotherSite::getLinkContent($id_lang,$Site.id_product_price_site,$Products.id_product_price_site_productname)}

                        {*{assign var="values" value=PriceAnotherSite::scraping_site(PriceAnotherSite::getLinkContent($id_lang,$Site.id_product_price_site,$Products.id_product_price_site_productname),$Site.tags)}*}

                        <span data-number2="{$number2}" class="priceHolder{if ($Link_URL)!=""} url{/if}"
                              id="price{$number2}"
                              data-link="{$Link_URL}" data-tag="{$Site.hash_tags}" data-tag1="{$Site.hash_tags1}"
                              data-tag2="{$Site.hash_tags2}">
                                {if ($Link_URL)!=""}
                                    <span style="color: #003399; text-align: center">{l s='Price'}</span>
                                    </br>
                                    <span style="color: #0000AA; font-size: 10px; text-align: left">
                                        <a href="{$Link_URL}" target="_blank" title="Link to reference site">
                                            {l s='Link'}
                                        </a>
                                    </span>

                                                                        {else}

                                    <span style="color: #FF0000; font-size: 10px; text-align: left">
                                    {l s='Not Link'}
                                </span>
                                {/if}
                            </span>
                        </br>
                        <span class="Tag1"></span>
                        </br>
                        <span class="Tag2"></span>
                    </td>
                {/foreach}
            </tr>
        {/foreach}
    </table>
{*  {$StrProducts}*}
    <script type="text/javascript">
        $('#start').click(function (e) {
            $("#price1").trigger('click');
        });
        $('.priceHolder').each(function () {
            $(this).click(function (e) {
                var link = $(this).data('link');
                var tagHtml = $(this).data('tag');
                var tagHtml1 = $(this).data('tag1');
                var tagHtml2 = $(this).data('tag2');
                var number2 = $(this).data('number2');
                if ($(this).hasClass('url')) {
                    $(this).text('در حال پردازش').css('color', 'red');
                    ajaxCall(link, tagHtml, tagHtml1, tagHtml2, e.target.id, number2);
                }
                else {
                    var nextnumber = number2 + 1;
                    $('#price' + nextnumber).trigger('click');
                }
            });
        });
        function ajaxCall(urlParam, tagParam, tagParam1, tagParam2, idClick, number2) {

            {if $address!='/'}
            var address = {$address};
            {else}
            var address = '/';
            {/if}
            $.ajax({
                url: address + 'modules/priceanothersite/ajax.php?controller=AdminModules',
                data: {
                    urlsite: urlParam,
                    tag: tagParam,
                    tag1: tagParam1,
                    tag2: tagParam2
                },
                type: 'post',
                dataType: "json",
                success: function (data) {
                    var priceString = data.resultshtml['Tag'];
                    var tag1String = data.resultshtml['Tag1'];
                    var tag2String = data.resultshtml['Tag2'];
                    if (priceString != null) {
//                      var htmlPrice = $(priceString).html().toString().replace('<span style="font-size:16px">تومان</span>' , "تومان");
                        $('#' + idClick).text(priceString).css('color', 'blue');
                        $('#' + idClick).closest('td').children('.Tag1').text(tag1String).css('color', 'purple');
                        $('#' + idClick).closest('td').children('.Tag2').text(tag2String).css('color', 'green');
                    }
                    else {
                        $('#' + idClick).text('قیمت وجود ندارد');
                    }
                    var nextnumber = number2 + 1;
                    $('#price' + nextnumber).trigger('click');
                },
                error: function () {
                    console.log('error');
                    $('#' + idClick).text('ERROR');
                    var nextnumber = number2 + 1;
                    $('#price' + nextnumber).trigger('click');
                }
            });
        }
    </script>
{/if}
