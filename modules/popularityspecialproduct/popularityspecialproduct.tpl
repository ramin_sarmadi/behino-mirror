<fieldset>
    <legend><img src="{$IconSpecialProductImage}"/> {$SpecialProductImage} </legend>
    <form action="{$LinkSave}" method="post" enctype="multipart/form-data">
        <input name="id_product_special" value="{$ResultSpecialProductArr[0]['id_product_special']}" type="hidden">

        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Special Product Name' mod='popularityspecialproduct'}:</span>
            <input name="special_product_name" value="{$ResultSpecialProductArr[0]['name']}" maxlength="2000"
                   style="width: 280px">
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Product Name' mod='popularityspecialproduct'}:</span>
            <select name="id_product" dir="ltr">
                {foreach from=$ProductArr item=Product}
                    <option style="{$Product['BgColor']}" value="{$Product['id_product']}"
                            {if $Product['id_product']==$ResultSpecialProductArr[0]['id_product']} selected="selected" {/if}
                            >{$Product['id_product']}. {$Product['name']}  //--> Count like: {$Product['COUNT_PRD']} </option>
                {/foreach}
            </select>
            {if $ResultSpecialProductArr[0]['id_product']>0}<span style="font-size: 10px; font-style: italic">{l s='If the option is not selected, it is possible to disable the product' mod='popularityspecialproduct'}</span>  {/if}
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Special Product Type' mod='popularityspecialproduct'}:</span>
            <select name="id_product_special_type" dir="ltr">
                <option value="-1">...</option>
                {foreach from=$SpecialType item=SpecialTypes}
                    <option value="{$SpecialTypes['id_product_special_type']}"
                            {if $SpecialTypes['id_product_special_type']==$ResultSpecialProductArr[0]['id_product_special_type']} selected="selected" {/if} >
                        {$SpecialTypes['id_product_special_type']}. {$SpecialTypes['name']}
                    </option>
                {/foreach}
            </select>
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Duration (Day) ' mod='popularityspecialproduct'}:</span>
            <input name="duration_days" value="{$ResultSpecialProductArr[0]['duration_days']}" maxlength="3"
                   style="width: 50px">
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Description' mod='popularityspecialproduct'}:</span>
            <textarea cols="50" rows="3" name="description">{$ResultSpecialProductArr[0]['description']}</textarea>
        </div>
        <div style="padding: 5px">
            <span class="span_bold_12">{l s='Image File' mod='popularityspecialproduct'}:</span>
            <input type="file" name="ImageFile">
        </div>
        <div style="padding: 5px">date_start
            <span class="span_bold_12">{l s='Available from' mod='popularityspecialproduct'}:</span>
            <input class="datepicker" type="text" name="date_start"
                   value="{$ResultSpecialProductArr[0]['date_start_h']}" style="text-align: center" id="sp_from"/>
        </div>
        <div style="padding-left: 150px; padding-top: 10px">
            <input type="submit" value="{l s='Save' mod='popularityspecialproduct'}" class="button" name="AddProductLink">
        </div>
    </form>
</fieldset>
<script type="text/javascript">
    $(document).ready(function () {
        $('#leave_bprice').click(function () {
            if (this.checked)
                $('#sp_price').attr('disabled', 'disabled');
            else
                $('#sp_price').removeAttr('disabled');
        });
        $('.datepicker').datepicker({
            prevText: '',
            nextText: '',
            dateFormat: 'yy-mm-dd',
            // Define a custom regional settings in order to use PrestaShop translation tools
            currentText: '{l s='Now'}',
            closeText: '{l s='Done'}'
        });
    });
</script>