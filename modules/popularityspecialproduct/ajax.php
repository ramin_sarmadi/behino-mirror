<?php
include('../../config/config.inc.php');
include('../../init.php');
include('popularityspecialproduct.php');
$object = new PopularitySpecialProduct();
$res = $object->getResultContent(Context::getContext()->language->id, 0, 2);
Context::getContext()->smarty->assign(array(
    'SpecialProductArr' => $object->getResultContent(Context::getContext()->language->id, 0, 2)
));
if (Tools::getValue('gettpl')) {
    //echo __DIR__ . '\resultpage.tpl';
    $resulthtml = Context::getContext()->smarty->fetch(__DIR__ . '\resultpage.tpl');
    //$resulthtml;
    die(Tools::jsonEncode(array(
        'resulthtml' => $resulthtml
    )));
}
