<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statscheckup}prestashop>statscheckup_e7a908d79d3758c911692ba791da9c70'] = 'ارزیابی فهرست محصولات';
$_MODULE['<{statscheckup}prestashop>statscheckup_24f870376156be0e8e22cfb94d29df6d'] = 'ارزیابی سریع کیفیت محصولات شما.';
$_MODULE['<{statscheckup}prestashop>statscheckup_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'تنظیمات به روز شد';
$_MODULE['<{statscheckup}prestashop>statscheckup_7ff3e75ce6aca348bc513ed3d5882946'] = 'بد';
$_MODULE['<{statscheckup}prestashop>statscheckup_b1897515d548a960afe49ecf66a29021'] = 'متوسط';
$_MODULE['<{statscheckup}prestashop>statscheckup_0c6ad70beb3a7e76c3fc7adab7c46acc'] = 'خوب';
$_MODULE['<{statscheckup}prestashop>statscheckup_619a1efbedc7855aa27c0a50579c104e'] = 'هیچ محصولی یافت نشد';
$_MODULE['<{statscheckup}prestashop>statscheckup_8bc84316c4078bf66723fd019616d920'] = 'توضیحات';
$_MODULE['<{statscheckup}prestashop>statscheckup_aed2078f8eaaf31e013a3c24618cb130'] = 'کارکترها بدون کد اچ تی ام ال';
$_MODULE['<{statscheckup}prestashop>statscheckup_fff0d600f8a0b5e19e88bfb821dd1157'] = 'تصاویر';
$_MODULE['<{statscheckup}prestashop>statscheckup_11ff9f68afb6b8b5b8eda218d7c83a65'] = 'فروش ها';
$_MODULE['<{statscheckup}prestashop>statscheckup_c3917e1f7138a24c6dc954fe81b86679'] = 'سفارش  ماه';
$_MODULE['<{statscheckup}prestashop>statscheckup_7bd5825a187064017975513b95d7f7de'] = 'مقادیر موجود برای فروش';
$_MODULE['<{statscheckup}prestashop>statscheckup_9dea4016dbcc290b773ab2fae678aaa8'] = 'اقلام';
$_MODULE['<{statscheckup}prestashop>statscheckup_dc270bcf468689b19074a5eef7581b52'] = 'کافی نیست';
$_MODULE['<{statscheckup}prestashop>statscheckup_f4abc77d498c89880f67331609406774'] = 'صحیح است';
$_MODULE['<{statscheckup}prestashop>statscheckup_ad647e4ae904c7ed54ee93040d8298c7'] = 'کمتر از';
$_MODULE['<{statscheckup}prestashop>statscheckup_05824b7be006782aaefd1fdb97c71e06'] = 'بیشتر از';
$_MODULE['<{statscheckup}prestashop>statscheckup_38fb7d24e0d60a048f540ecb18e13376'] = 'ذخیره';
$_MODULE['<{statscheckup}prestashop>statscheckup_01fda57aa6c7e9f07f5aa36b108e95cb'] = 'ترتیب با';
$_MODULE['<{statscheckup}prestashop>statscheckup_b718adec73e04ce3ec720dd11a06a308'] = 'شناسه';
$_MODULE['<{statscheckup}prestashop>statscheckup_49ee3087348e8d44e1feda1917443987'] = 'نام';
$_MODULE['<{statscheckup}prestashop>statscheckup_7d74f3b92b19da5e606d737d339a9679'] = 'قلم';
$_MODULE['<{statscheckup}prestashop>statscheckup_4d3d769b812b6faa6b76e1a8abaece2d'] = 'فعال';
$_MODULE['<{statscheckup}prestashop>statscheckup_3f74e68055b1123e5d7deb32cd8b0b1b'] = 'توضیح';
$_MODULE['<{statscheckup}prestashop>statscheckup_4cc6684df7b4a92b1dec6fce3264fac8'] = 'جهانی';
