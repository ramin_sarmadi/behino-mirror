<?php
/*
  **************************************
  **        PrestaShop V1.5.4.1        *
  **        Navigation product         *
  **      hzttp://www.brainos.com      *
  **             V 1.0                 *
  **    Author-team: Land of coder     *
  **************************************
  */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}
include_once(_PS_MODULE_DIR_.'lofnavproduct/libs/Params.php');
include_once(_PS_MODULE_DIR_.'lofnavproduct/libs/helper.php');
/**
 * lofsliding Class
 */

@session_start();
class lofnavproduct extends Module{
	/**
	* @variable
	*/
    private $_html = '';
    private $_configs = '';
	private $_params = '';
	public $_postErrors = array();
    public $_postSuccess = '';

   /**
    * Constructor
    */
	function __construct(){
		$this->name = 'lofnavproduct';
        $this->tab = 'LandOfCoder';
        $this->version = '1.1';

        parent::__construct();

        $this->displayName = $this->l('Lof Navigation Product Module');
		$this->description = $this->l('Lof Navigation product');
		$this->module_key = "";
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' ) && !class_exists("LofNavParams", false) ){
		  require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' );
		}
        $this->_prepareForm();
		$this->params =  new LofNavParams( $this, $this->name, $this->_configs  );

	}
    public function _prepareForm(){
        $this->_configs = array(
            'link_size' => '15px',
            'tooltip_width' =>'350',
            'tooltip_height' => 'auto',
            'show_image' => '1',
            'image_size' => 'home',
            'show_desc' =>'1',
            'desc_chars' => '100',
            'link_color' => '#FFFFFF',
            'enable_module' => '1',
            'use_pro_name'=>'',
            'enable_circle'=>'1',
            'show_tooltip' =>'1'
        );
        $langs = Language::getLanguages(false);
        foreach( $langs  as $lang){
            $this->_configs['link_prev_'.$lang['id_lang']] = '< Pre';
            $this->_configs['link_next_'.$lang['id_lang']] = 'Next >';
        }
    }

    public function getParams(){
        return $this->params;
    }
   /**
    * process installing
    * for example hook "displayDiscounts": {hook h='displayDiscounts'}
    */
	function install(){
        $this->getParams()->batchUpdate( $this->_configs );
		if(!parent::install() ||
            !$this->registerHook('header') ||
            !$this->registerHook('productfooter'))
            return false;
		return true;
	}
    /**
    * process uninstalling
    */
    function uninstall(){
		$this->getParams()->delete();
		if(!parent::uninstall() ||
           !$this->unregisterHook('header') || 
           !$this->unregisterHook('productfooter'))
            return false;
		return true;
	}	
	/*
	 * register hookHeader
	 */
	function hookHeader($params){
		$views = $this->getParams()->get('views','default');
		$this->context->controller->addCSS($this->_path.'views/'.$views.'/assets/styles.css');
		$this->context->controller->addJS($this->_path.'assets/tooltip1.js');
	}
	/*
	 * register hookFooterProduct
	 */	
    public function hookdisplayFooterProduct($params){
       return $this->hookProductTabContent($params);
    }
	/*
	 * register hookProductTabContent
	 */	
    public function hookProductTabContent($params)
    {
        if(!$this->getParams()->get('enable_module'))
            return;
        $this->context->controller->addCSS( ($this->_path).'assets/style.css', 'all');

        $id_product = (int)Tools::getValue('id_product');
        $tooltip_width = $this->getParams()->get("tooltip_width","350");
        $tooltip_height = $this->getParams()->get("tooltip_height","100");
        if(empty($tooltip_width) || !is_numeric($tooltip_width)){
           $tooltip_width = "350";     
        }else{
            $tooltip_width = (int)$tooltip_width."px"; 
        }
        if(empty($tooltip_height) || !is_numeric($tooltip_height)){
           $tooltip_height = "auto";     
        }else{
           $tooltip_height = (int)$tooltip_height."px";  
        }
        $show_desc = $this->getParams()->get("show_desc", 1);
        $desc_chars = $this->getParams()->get("desc_chars", 100);
        $show_image = $this->getParams()->get("show_image",1);
        $image_size = $this->getParams()->get("image_size","home");
        $image_size = !empty($image_size)?$image_size:"home";
        
        $font_size = $this->getParams()->get('link_size');
        $font_color = $this->getParams()->get('link_color');
        $use_pro_name = $this->getParams()->get("use_pro_name");
        $show_tooltip = $this->getParams()->get("show_tooltip");
        
        $link_prev = $this->getParams()->get('link_prev_'.(int)$this->context->language->id);
        $link_next = $this->getParams()->get('link_next_'.(int)$this->context->language->id);
        $proName_prev = $this->getParams()->get('ProName_prev');

        $ProName_next = $this->getParams()->get('ProName_next');
		$products  = $this->getPrevNextProduct($id_product);

        $prev_product = isset($products["prev"]) ? $products["prev"] : null;
        if($prev_product){
           $imgCover = Product::getCover($prev_product->id);
           if($show_desc){
                $prev_product->description = $this->substring($prev_product->description, $desc_chars);
           }
           $prev_product->id_image = $prev_product->id.'-'.$imgCover['id_image'];
           $prev_product->price = Product::getPriceStatic($prev_product->id);
            if($use_pro_name){
                $link_prev = "< ".$prev_product->name;
            }
        }

        $next_product = isset($products["next"])?$products["next"]:null;
        if($next_product){
            $img_next = Product::getCover($next_product->id);
            if($show_desc){
                $next_product->description = $this->substring($next_product->description, $desc_chars);
           }
            $next_product->id_image = $next_product->id.'-'.$img_next['id_image'];
            $next_product->price = Product::getPriceStatic($next_product->id);
            if($use_pro_name){
                $link_next = $next_product->name." >";
            }
        }
        $this->smarty->assign(array(
            'link_prev' => $link_prev,
            'link_next' => $link_next,
            'proName_prev' => $proName_prev,
            'ProName_next' => $ProName_next,
            'link_size' => $font_size,
            'link_color' => $font_color,
            'tooltip_height' => $tooltip_height,
            'tooltip_width' => $tooltip_width,
            'id_product' => $id_product,
            'show_tooltip' => $show_tooltip,
			'next_product' => $next_product,
			'prev_product' => $prev_product,
            'show_image'    => $show_image,
            'show_desc'     => $show_desc,
            'image_size'    => $image_size."_default",
            'mediumSize' => Image::getSize(ImageType::getFormatedName( $image_size ))
        ));

        return $this->processHook($params, "default");
	}
	/*
	 * function getPrevNextProduct
	 * return array
	 */	
    public function getPrevNextProduct($id_product = 0){
        $enable_circle = $this->getParams()->get("enable_circle");
        $id_category = Db::getInstance()->getValue('SELECT id_category_default FROM '._DB_PREFIX_.'product  WHERE id_product='.$id_product);
        $categories = $this->getParams()->get('category');
        if($categories && !in_array($id_category, explode(',',$categories)))
            return array();
        $sql = ' SELECT * FROM '._DB_PREFIX_.'category_product cp';
        $sql .= ' WHERE cp.id_category = '.(int)($id_category).' AND cp.id_product='.$id_product;
        $result = Db::getInstance()->getRow($sql);
        if($result){
            $lastPosition = $this->getLastPosition($id_category);
            $nextPosition = $result['position'] + 1;
            $next_product = $prev_product =  null;

            if($nextPosition > $lastPosition){
                if($enable_circle){
                    $next_id_product = $this->getProductByPositionAndCategory($id_category, 0);
                    $next_product = new Product($next_id_product, $this->context->language->id,true);
                }
            }
            else{
                $next_id_product = $this->getProductByPositionAndCategory($id_category, $nextPosition);
                $next_product = new Product($next_id_product, $this->context->language->id,true);
            }
            $pevPosition = $result['position'] - 1;
            if($pevPosition < 0){
                if($enable_circle){
                    $prev_id_product = $this->getProductByPositionAndCategory($id_category, $lastPosition);
                    $prev_product = new Product($prev_id_product,$this->context->language->id, true);
                }
            }
            else{
                $prev_id_product = $this->getProductByPositionAndCategory($id_category, $pevPosition);
                $prev_product = new Product($prev_id_product,$this->context->language->id, true);
            }
            return array('next' => $next_product, 'prev' => $prev_product);
        }
        return array();
    }

    public function getProductByPositionAndCategory($id_category, $position){
        $sql = ' SELECT * FROM '._DB_PREFIX_.'category_product cp';
        $sql .= ' WHERE cp.id_category = '.(int)($id_category).' AND cp.position = '.$position;
        $result = Db::getInstance()->getRow($sql);
        if($result){
            return $result['id_product'];
        }
        return false;
    }

    public function getLastPosition($id_category){
        $sql = ' SELECT MAX(cp.position) as maxposition FROM '._DB_PREFIX_.'category_product cp';
        $sql .= ' WHERE cp.id_category = '.(int)($id_category);
        $result = Db::getInstance()->getRow($sql);
        if($result){
            return $result['maxposition'];
        }
        return 0;
    }
	/**
    * Proccess module by hook
    * $params: param of module
    * $pos: position call
    */
	function processHook( $params, $pos, $folder = "views", $extrahtml = "", $baseDir=""){
        if(empty($baseDir))
            $baseDir = __FILE__;
        $theme = "default";
        return ($this->display($baseDir, $folder.'/'.$theme.'/'.$pos.'.tpl')).$extrahtml;
	}

    public function getContent(){
        $this->_html .= '<h2>'.$this->displayName.'.</h2>';
        if(Tools::isSubmit('save')){
             $this->_postProcess();
        }
        $this->_displayForm();
        return $this->_html;
    }

    public function _displayForm(){
        $params = $this->params;
        require_once ( dirname(__FILE__).'/form.php' );
    }

    private function _postProcess(){
        if (Tools::isSubmit('save')){
            $langs = Language::getLanguages(false);
            foreach( $langs  as $lang){
                Configuration::updateValue(strtoupper($this->name.'_category'), (Tools::getValue('categoryBox') ? implode(',',Tools::getValue('categoryBox')) : ''));
                Configuration::updateValue(strtoupper($this->name.'_clink_prev_'.$lang['id_lang']), Tools::getValue( strtoupper($this->name.'_clink_prev_'.$lang['id_lang']) ) , true);
                Configuration::updateValue(strtoupper($this->name.'_clink_next_'.$lang['id_lang']), Tools::getValue( strtoupper($this->name.'_clink_next_'.$lang['id_lang']) ) , true);
            }
            $res = $this->getParams()->batchUpdate( $this->_configs );
            $this->getParams()->refreshConfig();
            if (!$res)
                $this->_html .= $this->displayError($this->l('Configuration could not be updated'));
            else
                $this->_html .= $this->displayConfirmation($this->l('Configuration updated successfully'));
        }
    }
   /**
    * Get value of parameter following to its name.
	* @return string is value of parameter.
	*/
	public function getParamValue($name, $default=''){
		return $this->_params->get( $name, $default );
	}
    public function substring($itemtext, $length = 100){
        $itemtext = strip_tags($itemtext);
        if(strlen($itemtext) <= $length){
            return $itemtext;
        }
        $itemtext = substr($itemtext,0,$length);
        $posSpace = strrpos($itemtext,' ');
        $replacer="...";
        return substr($itemtext,0,$posSpace).$replacer;
    }
}



