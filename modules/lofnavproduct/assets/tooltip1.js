var tooltip1={
	tooltipoffsets: [20, -30], 
	fadeinspeed: 200, 
	rightclickstick: true,
	stickybordercolors: ["black", "darkred"],
	stickynotice1: ["Press \"s\"", "or right click", "to sticky box"], 
	stickynotice2: "Click outside this box to hide it", 
	isdocked: false,

	positiontooltip:function($, $tooltip, e){
		var x=e.pageX+this.tooltipoffsets[0], y=e.pageY+this.tooltipoffsets[1]
		var tipw=$tooltip.outerWidth(), tiph=$tooltip.outerHeight(), 
		x=(x+tipw>$(document).scrollLeft()+$(window).width())? x-tipw-(tooltip1.tooltipoffsets[0]*2) : x
		y=(y+tiph>$(document).scrollTop()+$(window).height())? $(document).scrollTop()+$(window).height()-tiph-10 : y
		$tooltip.css({left:x, top:y})
	},	
	showbox:function($, $tooltip, e){
		$tooltip.fadeIn(this.fadeinspeed)
		this.positiontooltip($, $tooltip, e)
	},
	hidebox:function($, $tooltip){
		if (!this.isdocked){
			$tooltip.stop(false, true).hide()
			$tooltip.css({borderColor:'black'}).find('.stickystatus:eq(0)').css({background:this.stickybordercolors[0]}).html(this.stickynotice1)
		}
	},
	docktooltip:function($, $tooltip, e){
		this.isdocked=true
		$tooltip.css({borderColor:'darkred'}).find('.stickystatus:eq(0)').css({background:this.stickybordercolors[1]}).html(this.stickynotice2)
	},
	init:function(targetselector, tipid){
		jQuery(document).ready(function($){
			var $targets=$(targetselector)
			var $tooltip=$('#'+tipid).appendTo(document.body)
			if ($targets.length==0)
				return
			var $alltips=$tooltip.find('div.atip')
			if (!tooltip1.rightclickstick)
				tooltip1.stickynotice1[1]=''
			tooltip1.stickynotice1=tooltip1.stickynotice1.join(' ')
			tooltip1.hidebox($, $tooltip)
			$targets.bind('mouseenter', function(e){
				$alltips.hide().filter('#'+$(this).attr('data-tooltip')).show()
				tooltip1.showbox($, $tooltip, e)
			})
			$targets.bind('mouseleave', function(e){
				tooltip1.hidebox($, $tooltip)
			})
			$targets.bind('mousemove', function(e){
				if (!tooltip1.isdocked){
					tooltip1.positiontooltip($, $tooltip, e)
				}
			})
			$tooltip.bind("mouseenter", function(){
				tooltip1.hidebox($, $tooltip)
			})
			$tooltip.bind("click", function(e){
				e.stopPropagation()
			})
			$(this).bind("click", function(e){
				if (e.button==0){
					tooltip1.isdocked=false
					tooltip1.hidebox($, $tooltip)
				}
			})
			$(this).bind("contextmenu", function(e){
				if (tooltip1.rightclickstick && $(e.target).parents().andSelf().filter(targetselector).length==1){ 
					tooltip1.docktooltip($, $tooltip, e)
					return false
				}
			})
			$(this).bind('keypress', function(e){
				var keyunicode=e.charCode || e.keyCode
				if (keyunicode==115){
					tooltip1.docktooltip($, $tooltip, e)
				}
			})
		}) 
	}
}
tooltip1.init("*[data-tooltip]", "tooltips")