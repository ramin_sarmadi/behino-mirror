<?php
/**
 * Lof Modal Cart Module
 *
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) June 2013 LeoTheme.Com <@emai:leotheme@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */

/**
 * @since 1.5.0
 * @version 1.0 (2013-06-20)
 */

    if (!defined('_PS_VERSION_'))
        exit;
$this->_html .= '
        <script type="text/javascript">
            jQuery(document).ready(function(){
                
                $(".group").click(function(){
                    var id = $(this).attr("id");
                    id = id.replace("_on","");
                    id = id.replace("_off","");
                    $(".group-"+id).hide();
                    var val = $(this).val();
                   if(val == 1){
                      $(".group-"+id+"-"+val).show();  
                   }
                });
                $(".group").each(function(){
                    var id = $(this).attr("id");
                    id = id.replace("_on","");
                    id = id.replace("_off","");
                    if($(this).is(":checked")){
                       var val = $(this).val();
                       if(val == "0"){
                          $(".group-"+id).css("display","none");
                       }
                    }
                
                });
            });
        </script>';
    $this->_html .= '<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">';
    $this->_html .= '<fieldset><legend>
                          <img src="'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" alt="" /> '.$this->l('Modal Cart configurations').
        '</legend>';
    $langs = Language::getLanguages(false);
    $divlang = 'link_size¤link_prev¤link_next';
    $link_prevs = array();
    $link_nexts = array();
    foreach( $langs  as $lang){
        $link_prevs[$lang['id_lang']] = $this->getParams()->get( 'link_prev_'.$lang['id_lang'], '');
        $link_nexts[$lang['id_lang']] = $this->getParams()->get( 'link_next_'.$lang['id_lang'], '');
    }

    $this->_html .= $params->inputTag( $this->l('Link size:'), 'link_size', $this->getParams()->get('link_size'), '',' size="40" ' );
    $this->_html .= $params->inputTagLang($this->l('Link prev:'), 'link_prev', $link_prevs, $divlang, '','' );
    $this->_html .= $params->inputTagLang($this->l('Link next:'), 'link_next', $link_nexts, $divlang, '','' );

    $this->_html .=  $params->statusTag( $this->l('Use Product Name:'), 'use_pro_name', $this->getParams()->get('use_pro_name',0) ,'use_pro_name');
    $this->_html .=  $params->statusTag( $this->l('Show Tooltip:'), 'show_tooltip', $this->getParams()->get('show_tooltip',0) ,'show_tooltip','class="group"');
    $this->_html .= '<div class="group-show_tooltip group-show_tooltip-1">';
    $this->_html .=  $params->inputTag( $this->l('Tooltip width:'), 'tooltip_width', $this->getParams()->get('tooltip_width','auto') ,'');
    $this->_html .=  $params->inputTag( $this->l('Tooltip height:'), 'tooltip_height', $this->getParams()->get('tooltip_height','auto') ,'');
    $this->_html .=  '</div>';
    $imageSizes = array(
                "small" =>$this->l('Small'),
                "medium" =>$this->l('Medium'),
                "large" =>$this->l('Large'),
                "home" =>$this->l('Home')
            );
    $this->_html .=  $params->statusTag( $this->l('Show Image:'), 'show_image', $this->getParams()->get('show_image',1) ,'show_image','class="group"');
    $this->_html .= '<div class="group-show_image group-show_image-1">';
     $this->_html .=  $params->selectTag($imageSizes, $this->l('Image size:'), 'image_size', $this->getParams()->get('image_size','') ,'');
    $this->_html .= '</div>';
    $this->_html .=  $params->statusTag( $this->l('Show Product Description:'), 'show_desc', $this->getParams()->get('show_desc',0) ,'show_desc','class="group"');
    $this->_html .= '<div class="group-show_desc group-show_desc-1">';
    $this->_html .=  $params->inputTag( $this->l('Description max chars:'), 'desc_chars', $this->getParams()->get('desc_chars',25) ,'');
    $this->_html .= '</div>';
    $this->_html .=  $params->statusTag( $this->l('Enable Circle Navigation:'), 'enable_circle', $this->getParams()->get('enable_circle',0) ,'enable_circle');
    $this->_html .= $params->inputTagColor( $this->l('Color:'), 'link_color', $this->getParams()->get('link_color'), '',' size="30" ' );

//-----------------//

    $index = explode(',', $params->get('category'));
    $indexedCategories = ($index ? implode(',', $index) : '');
    $categories = LofNavProductHelper::getCategoriesFull(Context::getContext()->language->id, $indexedCategories);
    $helper = new Helper();
    $category_tree = $helper->renderCategoryTree(null, $categories, 'categoryBox', false, true, array(), false, true);
    $this->_html .= '<label>'.$this->l('Category :').'</label>';
    $this->_html .= '<div class="margin-form">';
    $this->_html .= $category_tree;
    $this->_html .= '</div>';
    //Add show product info when mouse hover on link: product name, image, price
    //allow/disable product name on link
    $this->_html .= $params->statusTag( $this->l('Enable module:'), 'enable_module', $this->getParams()->get('enable_module',1) ,'enable_module');

    $this->_html .= '<br><br>';
    /* Save */
    $this->_html .= '
          <div class="margin-form">
            <input type="submit" class="button" name="save" value="'.$this->l('Save').'" />
          </div>';
    $this->_html .= '</fieldset></form>';
?>
