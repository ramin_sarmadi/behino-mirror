<?php
/**
 * $ModDesc
 *
 * @version   $Id: file.php $Revision
 * @package   modules
 * @subpackage  $Subpackage.
 * @copyright Copyright (C) November 2013 LandOfCoder.com <@emai:LandOfCoder@gmail.com>.All rights reserved.
 * @license   GNU General Public License version 2
 */


class LofNavProductHelper {

	public function getCategory($params){
		$source = $params->get("getproducts","featured");
        if(isset($_GET['sources'])){
            $getSource= ($_GET['sources']);
        }
        $where = "";
		if($source == "categories"){
			$selectCat = $params->get("categories","");
			if($selectCat != ""){
                $where = " AND cat.`id_category` IN (".$selectCat.")";
            }
			$sql = 'SELECT cl.`name`, cl.`id_category` FROM `'._DB_PREFIX_.'category` cat
					LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cat.`id_category` = cl.`id_category`)'.$where;
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
			return $result;
		}if($getSource == "categories"){
            $sql = 'SELECT cl.`name`, cl.`id_category` FROM `'._DB_PREFIX_.'category` cat
					LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cat.`id_category` = cl.`id_category`)';
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
            return $result;
        }
	}

    public static function getCategoriesFull( $id_lang = null, $categories = null)
    {
        if (!$id_lang)
            $id_lang = Context::getContext()->language->id;
        $ret = array();
        //if(!$categories || count($categories) <=0)
         //   return $ret;
        $row = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT cp.`id_category`, cl.`name`, cl.`link_rewrite`
			FROM `'._DB_PREFIX_.'category` cp
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (cp.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').')
			'.Shop::addSqlAssociation('category', 'cp').'
			WHERE cl.`id_lang` = '.(int)$id_lang.($categories ? ' AND  cp.`id_category` IN ('.$categories.')' : '')
        );

        foreach ($row as $val)
            $ret[$val['id_category']] = $val;

        return $ret;
    }

}
