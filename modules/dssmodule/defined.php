<?php
/**
 * @name defined.php
 */
define('DSS_DEVNAME', 'dssmodule');
define('DSS_VERSION', '1.1');

// *************** SOME PATHS ******************************
define('DSS_ROOT', _PS_MODULE_DIR_ . 'dssmodule/');

define('DSS_CONTROLLERS_ADMIN_FRONT', DSS_ROOT.'controllers/front/');

// ***************** SOME LINKS ***************************************
define('DSS_BASE_URI', __PS_BASE_URI__ . 'modules/dssmodule/');
define('DSS_JS_URI', DSS_BASE_URI . 'js/');
define('DSS_CSS_URI', DSS_BASE_URI . 'css/');
define('DSS_IMG_URI', DSS_BASE_URI.'img/');

?>
