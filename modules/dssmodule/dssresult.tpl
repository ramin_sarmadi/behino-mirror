{if ($Action_Form=="resultRecordAttribute")}
<div class="toolbar-placeholder">
    <div class="toolbarBox ">
        <ul class="cc_button">

            <li>
                <a id="desc-priceanothersite-back" class="toolbar_btn" title="back" href="{$LinkBack}">
                    <span class="process-icon-back "></span>
                    <div>{l s='Back' mod='dssmodule'}</div>
                </a>
            </li>

        </ul>
        <div class="pageTitle">
            <h3>
            <span id="current_obj" style="font-weight: normal;">
            <span class="breadcrumb item-0 ">{$LinkSiteName}</span>
            </span>
            </h3>
        </div>
    </div>

    {$htmlMessage}
{/if}