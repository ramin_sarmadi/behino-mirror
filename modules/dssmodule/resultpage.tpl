<h1 class="new-product-title1 border-title-main">جدیدترین محصولات <span></span></h1>
<div class="button-group" id="filters">
    {*<div class="line"></div>*}
    <div class="">
        <a data-filter="*" href="" class="iso">
            <img src="{$img_dir}isotope/all1.jpg"></a>
        <a data-filter=".موبایل" href="" class="iso">
            <img src="{$img_dir}isotope/mobile1.jpg"></a>
        <a data-filter=".تبلت" href="" class="iso">
            <img src="{$img_dir}isotope/tablet1.jpg"></a>
    </div>
</div>
<div class="container">
    <div class="row-fluid">
        <div id="container" style="position: relative; height: 285px;">
            {foreach from=$SpecialProductArr item=SpecialProduct}
                <div data-category="{$SpecialProduct['parent_category_default']}"
                     class="isotope-hover element-item {$SpecialProduct['parent_category_default']} view effect">
                    <div class="view effect">
                        <img src="{$SpecialProduct['address_file']}">

                        <div class="mask"></div>
                        <div class="content">
                            <a class="info" target="_blank" href="{$SpecialProduct['link']}">Read More</a>
                        </div>
                    </div>
                    <a href="{$SpecialProduct['link']}" target="_blank">
                        <div class="main-text">
                            <span class="new-model">{$SpecialProduct['name']}</span>
                            <span class="new-price">{convertPrice price=$SpecialProduct['price']}</span>
                        </div>
                    </a>
                </div>
            {/foreach}
        </div>
    </div>
</div>

