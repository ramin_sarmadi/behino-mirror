<?php

include('../../config/config.inc.php');
include('../../init.php');
include_once('include/Fn_priceanothersite.php');

require('dssmodule.php');

if(Tools::getValue('action')=='requestsite'){
    $url = Tools::getValue('urlsite');
    $tag= base64_decode(Tools::getValue('tag'));
    $tag1= base64_decode(Tools::getValue('tag1'));
    $tag2= base64_decode(Tools::getValue('tag2'));

    //public function scraping_site($url,$tag) {
    // create HTML DOM
    $html = file_get_html($url);


    // get title
    $ret['Title'] = $html->find('title', 0)->plaintext;

    // get rating
    /*digikala*///$ret['Price'] = $html->find('span[class="finalprice green"] ', 0)->innertext;
    /*alldigitall*/
    if((substr($tag,0,3)=='//a')){
        $sTag=1;

        // create empty document
        $document = new DOMDocument();

        // load html
        $document->loadHTML($html);
       /* $elem_list = $document->getElementsByTagName('a');
        foreach($elem_list as $elem)
            echo $elem->getAttribute('href="/directory/view.aspx?id=164"');*/

        // create xpath selector
        $selector = new DOMXPath($document);

        // selects the parent node of <a> nodes
        // which's content is 'Hello world'
        //$results = $selector->query('//td/a[text()="Hello world"]/..');

        $results = $selector->query($tag);
        //print_r($results);
        // output the results
        foreach($results as $node) {
            $nodeResult .= ' <div> '.$node->nodeValue.'</div>';
        }
        $ret['Tag'] = $nodeResult;
    }

    if((substr($tag1,0,3)=='//a')){
        $sTag1=1;

        // create empty document
        $document = new DOMDocument();

        // load html
        $document->loadHTML($html);

        // create xpath selector
        $selector = new DOMXPath($document);

        // selects the parent node of <a> nodes
        // which's content is 'Hello world'
        //$results = $selector->query('//td/a[text()="Hello world"]/..');
        $results = $selector->query($tag1);
        foreach($results as $node) {
            $nodeResult .= ' <div> '.$node->nodeValue.'</div>';
        }
        $ret['Tag1'] = $nodeResult;
    }

    if((substr($tag2,0,3)=='//a')){
        $sTag2=1;
        // create empty document
        $document = new DOMDocument();

        // load html
        $document->loadHTML($html);

        // create xpath selector
        $selector = new DOMXPath($document);

        // selects the parent node of <a> nodes
        // which's content is 'Hello world'
        //$results = $selector->query('//td/a[text()="Hello world"]/..');
        $results = $selector->query($tag);

        foreach($results as $node) {
            $nodeResult .= ' <div> '.$node->nodeValue.'</div>';
        }
        $ret['Tag2'] = $nodeResult;

    }


    if(($sTag)&&($sTag1)&&($sTag2)){
        //out parameter

    }elseif(($sTag)&&($sTag1)){

        $ret['Tag2'] = $html->find($tag2, 0)->plaintext;

    }elseif(($sTag)){

        $ret['Tag1'] = $html->find($tag1, 0)->plaintext;
        $ret['Tag2'] = $html->find($tag2, 0)->plaintext;

    }else{

        $ret['Tag'] = $html->find($tag, 0)->plaintext;
        $ret['Tag1'] = $html->find($tag1, 0)->plaintext;
        $ret['Tag2'] = $html->find($tag2, 0)->plaintext;

    }






    /* $count=0;
     foreach($html->find($tag) as $element)
     {
         $ret['Price_'.$count] = $element->innertext . '<br>';
         $count++;
     }*/




    // clean up memory
    $html->clear();
    unset($html);

    die(Tools::jsonEncode(array(
        'resultshtml' => $ret,
        'errors' => $errors
    )));
}
elseif(Tools::getValue('action')=='save')
{

    if (Tools::getValue('product_attribute')!=0){
        $html='';
        $id_product_attribute = Tools::getValue('product_attribute');
        $id_product = Tools::getValue('product_id');
        $feature_value_array = Tools::getValue('feature_value_arr');

        $price=(int)Tools::getValue('price_'.$id_product_attribute);
        $quantity=(int)Tools::getValue('quantity_'.$id_product_attribute);

        //print_r($feature_value_array);
        $date = date("Y-m-d");
        $id_lang=Context::getContext()->language->id;

        $FeatureArr=DssModule::getListFeatureDss($id_lang,$id_feature = 0 );

        //DSS Feature Value Insert ---------------------------------
        $table='dss_product_attribute';
        $filed='id_dss_product_attribute';
        $where_syn='WHERE';
        $where='id_product_attribute='.$id_product_attribute.' AND id_product='.$id_product;
        $dss_prd_att_db=DssModule::getValue($table,$filed,$where_syn.' '.$where);

        if($dss_prd_att_db)
        {//For Update

            $dataArr = array(
             'price'=>$price
            ,'quantity'=>$quantity
            ,'available_date'=>$date
            );

            $where='id_dss_product_attribute='.$dss_prd_att_db;

            $result_update=DssModule::update($table,$id_lang,$dataArr,$where);

            if($result_update=='OK'){
                $html .= ('Update attribute Product record for id '.$id_product_attribute.' successfully.');

                $table='dss_product_feature';
                $WhereDeleted='id_dss_product_attribute='.$dss_prd_att_db;
                $result_del=DssModule::deleted($table,$WhereDeleted);

                    if($result_del=='OK'){

                        $html .= ('Delete detail DSS attribute Product record for id '.$id_product_attribute.' successfully.');

                        foreach ($FeatureArr as $Feature)
                        {

                            //print_r($feature_value);
                            foreach ($feature_value_array as $feature_value)
                            {
                                //echo 'class_id_feature_value_'.$Feature['id_feature'].'_'.$id_product_attribute;
                                $F_V_A=$feature_value['class_id_feature_value_'.$Feature['id_feature'].'_'.$id_product_attribute];

                                if($F_V_A)
                                {

                                    $table='dss_product_feature';
                                    $where='id_dss_product_attribute='.$dss_prd_att_db.' AND id_feature='.$Feature['id_feature'].' AND id_feature_value='.$F_V_A.' ';
                                    $filed='id_dss_product_feature';

                                    (int)$dss_prd_att_insert_feature_db=DssModule::getValue($table,$filed,$where_syn.' '.$where);

                                    if(!$dss_prd_att_insert_feature_db)
                                    {
                                        $dataArr = array(
                                         'id_dss_product_attribute'=>$dss_prd_att_db
                                        ,'id_feature'=>$Feature['id_feature']
                                        ,'id_feature_value'=>$F_V_A
                                        );

                                        $result_add_sub=DssModule::add('dss_product_feature',$id_lang,$dataArr,'id_dss_product_feature',
                                            'id_dss_product_attribute="'.$dss_prd_att_db.'"
                                        AND id_feature='.$Feature['id_feature'].' AND id_feature_value='.$F_V_A.'','');

                                        if($result_add_sub=='OK'){
                                            $rs_feat_html .= 'id Dss '.$dss_prd_att_db.' - id feature '.$Feature['id_feature'].' / ';
                                        }else{
                                            $rs_feat_html_err .= 'Error id Dss '.$dss_prd_att_db.' - id feature '.$Feature['id_feature'].' /. ';
                                        }

                                    }

                                }


                            }

                        }


                    }else{
                        $html .= ('Delete detail DSS attribute Product record for id '.$id_product_attribute.' unsuccessfully.');
                    }



                }else{
                $html .= ('Update attribute Product record for id '.$id_product_attribute.' unsuccessfully.');
            }



        }
        else
        {// For Insert

            $dataArr = array(
             'id_product_attribute'=>$id_product_attribute
            ,'id_product'=>$id_product
            ,'price'=>$price
            ,'quantity'=>$quantity
            ,'available_date'=>$date
            );

            $result_add=DssModule::add('dss_product_attribute',$id_lang,$dataArr,'id_dss_product_attribute','id_product_attribute="'.$id_product_attribute.'" AND id_product='.$id_product.'','');

            if($result_add=='OK'){
                $html .= 'Insert new record for id product <b>'.$id_product.'</b> successfully. ';

                $table='dss_product_attribute';
                $where='id_product_attribute='.$id_product_attribute.' AND id_product='.$id_product.'';
                $filed='id_dss_product_attribute';

                $dss_prd_att_insert_db=DssModule::getValue($table,$filed,$where_syn.' '.$where);

                if($dss_prd_att_insert_db){//insert feature value
                    $rs_feat_html='';
                    $rs_feat_html_err='';
                    foreach ($FeatureArr as $Feature)
                    {

                        //print_r($feature_value);
                        foreach ($feature_value_array as $feature_value)
                        {
                            //echo 'class_id_feature_value_'.$Feature['id_feature'].'_'.$id_product_attribute;
                            $F_V_A=$feature_value['class_id_feature_value_'.$Feature['id_feature'].'_'.$id_product_attribute];

                            if($F_V_A)
                            {

                                $table='dss_product_feature';
                                $where='id_dss_product_attribute='.$dss_prd_att_insert_db.' AND id_feature='.$Feature['id_feature'].' AND id_feature_value='.$F_V_A.' ';
                                $filed='id_dss_product_feature';

                                (int)$dss_prd_att_insert_feature_db=DssModule::getValue($table,$filed,$where_syn.' '.$where);

                                if(!$dss_prd_att_insert_feature_db)
                                {
                                    $dataArr = array(
                                     'id_dss_product_attribute'=>$dss_prd_att_insert_db
                                    ,'id_feature'=>$Feature['id_feature']
                                    ,'id_feature_value'=>$F_V_A
                                    );

                                    $result_add_sub=DssModule::add('dss_product_feature',$id_lang,$dataArr,'id_dss_product_feature',
                                        'id_dss_product_attribute="'.$dss_prd_att_insert_db.'"
                                        AND id_feature='.$Feature['id_feature'].' AND id_feature_value='.$F_V_A.'','');

                                    if($result_add_sub=='OK'){
                                        $rs_feat_html .= 'id Dss '.$dss_prd_att_insert_db.' - id feature '.$Feature['id_feature'].' / ';
                                    }else{
                                        $rs_feat_html_err .= 'Error id Dss '.$dss_prd_att_insert_db.' - id feature '.$Feature['id_feature'].' /. ';
                                    }

                                }

                            }


                        }

                    }

                    if(strlen($rs_feat_html)>1){
                        $html .= 'Insert new record details for '.$rs_feat_html.' successfully. ';
                    }

                    if(strlen($rs_feat_html_err)>1)
                    {
                        $html .= 'Error Insert new record details for '.$rs_feat_html_err.' unsuccessfully. ';
                    }

                }else{//error for insert feature value
                    $html .= 'You have error for insert detail id Dss <b>'.$dss_prd_att_insert_db.'</b> for id product <b>'.$id_product.'</b> --> <b>unsuccessfully</b>. ';
                }


            }else{//error for insert
                $html .= 'Error on Insert new record for id product <b>'.$id_product.'</b> --> <b>unsuccessfully</b>. ';
            }

        }





        //End DSS --------------------------------------------------


        //foreach ($id_product_attributes as $id_product_attribute)
        //{
            //$id_product_attribute;

            $table='product_attribute';
            $filed='price';
            $where_syn='WHERE';
            $where='id_product_attribute='.$id_product_attribute;
            $price_db=DssModule::getValue($table,$filed,$where_syn.' '.$where);
            if($price!=$price_db){

                $dataArr = array(
                    'price'=>(int)$price
                );

                $result_update=DssModule::update($table,$id_lang,$dataArr,$where);

                if($result_update=='OK'){
                    $html .= 'Update Price attribute Product record for id <b>'.$id_product_attribute.'</b> successfully';
                }else{
                    $html .= '<font color="#000000">Update Price attribute Product record for id <b>'.$id_product_attribute.'</b> --> <b>unsuccessfully</b></font>';
                }

            }

            $table='stock_available';
            $filed='quantity';
            $where_syn='WHERE';
            $where='id_product_attribute='.$id_product_attribute;
            $quantity_db=DssModule::getValue($table,$filed,$where_syn.' '.$where);

            $filed='id_product';
            $id_product_db=DssModule::getValue($table,$filed,$where_syn.' '.$where);

            if($quantity_db!=null){//update in db

                if($quantity!=$quantity_db){

                    $dataArr = array(
                        'quantity'=>(int)$quantity
                    );
                    $result_update=DssModule::update($table,$id_lang,$dataArr,$where);

                    if($result_update=='OK'){
                        $where='id_product='.$id_product_db.' AND id_product_attribute=0';
                        $filed='quantity';
                        $total_quantity_db=DssModule::getValue($table,$filed,$where_syn.' '.$where);

                        $dataArr = array(
                            'quantity'=>(int)$quantity+(int)$total_quantity_db
                        );
                        $result_update=DssModule::update($table,$id_lang,$dataArr,$where);

                        if($result_update=='OK'){
                            $html .= 'Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> for id product <b>'.$id_product_db.'</b> successfully';
                        }else{
                            $html .= '<font color="#000000">Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> for id product <b>'.$id_product_db.'</b> --> <b>unsuccessfully</b></font>';
                        }

                        $html .= 'Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> successfully';
                    }else{
                        $html .= '<font color="#000000">Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> --> <b>unsuccessfully</b></font>';
                    }

                }

            }else{//insert to db

                $table='product_attribute';
                $filed='id_product';
                $where_syn='WHERE';
                $where='id_product_attribute='.$id_product_attribute;
                $id_product_db=$this->getValue($table,$filed,$where_syn.' '.$where);

                $dataArr = array(
                    'id_product'=>$id_product_db
                ,'id_product_attribute'=>$id_product_attribute
                ,'id_shop'=>1
                ,'id_shop_group'=>0
                ,'quantity'=>$quantity
                ,'depends_on_stock'=>0
                ,'out_of_stock'=>2
                );

                $result_add=DssModule::add('stock_available',$id_lang,$dataArr,'id_stock_available','id_product_attribute="'.$id_product_attribute.'"','');

                if($result_add=='OK'){
                    $table='stock_available';
                    $where='id_product='.$id_product_db.' AND id_product_attribute=0';
                    $filed='quantity';
                    $total_quantity_db=DssModule::getValue($table,$filed,$where_syn.' '.$where);

                    $dataArr = array(
                        'quantity'=>(int)$quantity+(int)$total_quantity_db
                    );
                    $result_update=DssModule::update($table,$id_lang,$dataArr,$where);

                    if($result_update=='OK'){
                        $html .= 'Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> for id product <b>'.$id_product_db.'</b> successfully';
                    }else{
                        $html .= '<font color="#000000">Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> for id product <b>'.$id_product_db.'</b> --> <b>unsuccessfully</b></font>';
                    }

                    $html .= 'Insert quantity attribute Product record for id <b>'.$id_product_attribute.'</b> successfully';
                }else{
                    $html .= '<font color="#000000">Insert quantity attribute Product record for id <b>'.$id_product_attribute.'</b> --> <b>unsuccessfully</b></font>';
                }

            }

       // }//end foreach

        die(Tools::jsonEncode(array(
            'htmlMessage' => $html
        ,'LinkSiteName' => 'List Message on Attribute'
        //,'LinkBack' =>  AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')

        )));
        /*
        $this->context->smarty->assign(array(
         'htmlMessage' => $html
        ,'LinkSiteName' => 'List Message on Attribute'
        ,'LinkBack' =>  AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')


        ));
        return $this->display(__FILE__, 'dssresult.tpl');
        */

    }


}

       // return Tools::jsonEncode(array('results' => $ret));
  //  }



