<style>
</style>
{if ($Action_Form=="resultRecordAttribute")}
    {$htmlMessage}
    <form enctype="multipart/form-data" action="{$LinkSave}" method="post">
        <div class="toolbar-placeholder">
            <div class="toolbarBox ">
                <ul class="cc_button">
                    <li>
                        <a id="desc-priceanothersite-back" class="toolbar_btn" title="back" href="{$LinkBack}">
                            <span class="process-icon-back "></span>

                            <div>{l s='Back' mod='dssmodule'}</div>
                        </a>
                    </li>
                    <li class="help-context-AdminModules" style="">
                </ul>
                <div class="pageTitle">
                    <h3>
            <span id="current_obj" style="font-weight: normal;">
            <span class="breadcrumb item-0 ">{$LinkSiteName}</span>
            </span>
                    </h3>
                </div>
            </div>
            {if $countpage != 1}
                <div class="pagination">
                    <ul>

                        {for $page=1 to $countpage }
                            <li class="p-num"><a href="#" class="{if $page == $currentpage}activepage{/if}">{$page}</a>
                            </li>
                        {/for}

                    </ul>
                </div>
            {/if}
            <div class="filter-results">
                <span>{l s='product name' mod='dssmodule'}:</span><input value="{$name}" type="text" name="name"/>
                <span>{l s='min price'  mod='dssmodule'}:</span><input value="{$price}" type="text" name="price"/>
                <span>{l s='min quantity' mod='dssmodule'}:</span><input value="{$quantity}" type="text"
                                                                         name="quantity"/>
                <input type="submit" id="submit" value="ارسال">
            </div>
            <fieldset id="fortoppos">
                <legend><img src="{$IconDssModuleImage}"/> {$LinkSiteName} </legend>
                <table class="table" cellpadding="0" cellspacing="0" style="width:100%;">
                    <colgroup>
                        <col width="50">
                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th style="text-align: center">{l s='checked' mod='dssmodule'}</th>
                        <th style="text-align: center">{l s='Id' mod='dssmodule'}</th>
                        <th style="text-align: center">{l s='Product Name' mod='dssmodule'}</th>
                        <th style="text-align: center">{l s='Price' mod='dssmodule'}</th>
                        <th style="text-align: center">{l s='Quantity' mod='dssmodule'}</th>
                        <th style="text-align: center">{l s='Designation' mod='dssmodule'}</th>
                        {foreach from=$FeatureArr item=Feature}
                            <th style="text-align: center">{$Feature['feature_name']}</th>
                        {/foreach}
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$ProductArr item=Product}
                        <tr style="background-color: {$Product['Color']}; border-bottom: 1px solid #000000">
                            <td style="border-bottom: 1px solid #000000">
                                <input
                                        class="save-input"
                                        type="hidden"
                                        value="{$Product['id_product_attribute']}"
                                        name="product_attribute[]"
                                        />
                                <input class="id_product" type="hidden"
                                        value="{$Product['id_product']}"
                                        name="product_id[]"
                                        />
                                <button disabled="disabled" class="savedata">{l s='Save' mod='dssmodule'}</button>
                                {if $Product['result_site']|count >= 1}
                                    <button data-num="{$Product['id_product_attribute']}" class="show-get-data">
                                        {l s='Get Price' mod='dssmodule'}
                                    </button>
                                {/if}
                                {if $Product['link_lmg']!='NoPic'}
                                    <img src="{$Product['link_lmg']}">
                                {/if}
                            </td>
                            <td class="available_quantity" id="qty_{$Product['id_product_attribute']}"
                                style="border-bottom: 1px solid #000000">
                                <span>{$Product['id_product']}</span>
                            </td>
                            <td style="border-bottom: 1px solid #000000"><a href="{$Product['link']}"
                                                                            target="_blank">{$Product['prd_name']}</a>
                            </td>
                            <td style="text-align: center; border-bottom: 1px solid #000000">
                                <input class="price" type="text" value="{$Product['price']|number_format:0:".":""}"
                                       name="price_{$Product['id_product_attribute']}" style="text-align: center"/>
                            </td>
                            <td style="text-align: center; border-bottom: 1px solid #000000">
                                <input class="quantity" type="text" value="{$Product['quantity']}"
                                       name="quantity_{$Product['id_product_attribute']}"
                                       style="text-align: center; width: 20px"/>
                            </td>
                            <td style="border-bottom: 1px solid #000000">{$Product['attNameANDGroup']}{$Product['ColorAtt']}</td>
                            {foreach from=$FeatureArr item=Feature}
                                {$ListFeatureValueDss = $ListFeatureValueDss_{$Feature['id_feature']}}
                                <td style="text-align: center; border-bottom: 1px solid #000000">
                                    {assign var="PRD" value=$Product['id_product_attribute']}
                                    {assign var="FRD" value=$Feature['id_feature']}
                                    {assign var="NameAtt" value="DSS_Feature_{$Feature['id_feature']}_{$Product['id_product_attribute']}"}
                                    {$F_V_DSS = $Product[$NameAtt]}
{*
                                    {$F_V_DSS}
*}
{*
                                    <input class="class_id_feature_value_{$Feature['id_feature']}_{$Product['id_product_attribute']}" name="ids_feature_value_{$Feature['id_feature']}_{$Product['id_product_attribute']}" value="{$ListFeatureValueDss[0]['id_feature_value']}">
*}
                                    <select name="id_feature_value_{$Feature['id_feature']}_{$Product['id_product_attribute']}"
                                            dir="ltr" style="width: 80px">
                                        <option value="">{l s='select item'}</
                                        </option>
                                        {foreach from=$ListFeatureValueDss item=FeatureValue}
                                            <option value="{$FeatureValue['id_feature_value']}"
                                                    {if $FeatureValue['id_feature_value']==$F_V_DSS} selected="selected" {/if}
                                                    >{$FeatureValue['id_feature_value']}
                                                . {$FeatureValue['value']}</option>
                                        {/foreach}
                                    </select>
                                </td>
                            {/foreach}
                        </tr>
                        {if $Product['result_site']|count >= 1}
                            {assign var="count_colspan" value=($FeatureArr|count)+7}
                            <tr class="other-sites" id="{$Product['id_product_attribute']}">
                                <td colspan="12">
                                    {foreach from=$Product['result_site'] item=result_site}
                                        <div class="site-box">
                                            <span>
                                                <a href="{$result_site['product_link_site']}" target="_blank">
                                                {$result_site['site_name']}
                                                </a>
                                            </span>
                                            <button class="btn getlink"
                                                    data-link="{$result_site['product_link_site']}"
                                                    data-tag1="{$result_site['tag1']}"
                                                    data-tag2="{$result_site['tag2']}"
                                                    data-tag3="{$result_site['tag3']}"
                                                    >get price
                                            </button>
                                            <img class="loading" style="display: none"
                                                 src="{$address}modules/dssmodule/loading.gif"/>

                                            <div class="priceshow">
                                            </div>
                                        </div>
                                    {/foreach}
                                </td>
                            </tr>
                        {/if}

                    {/foreach}
                    </tbody>
                </table>
            </fieldset>
            <input id="number-input" type="hidden" name="page" value="">
    </form>
    <hr>
{/if}
<script type="text/javascript">
    {if $address!='/'}
    var address = {$address};
    {else}
    var address = '/';
    {/if}
</script>