<?php
require_once(_PS_MODULE_DIR_ . "dssmodule/defined.php");

class DssModule extends Module
{


    public function __construct()
    {
        $this->name = DSS_DEVNAME;
        $this->tab = 'pricing_promotion';
        $this->version = DSS_VERSION;
        $this->author = 'Ahmad Khorshidi';
        $this->need_instance = 0;
        $this->secure_key = Tools::encrypt($this->name);

        parent::__construct();

        DssModule::initProcess();

        $this->displayName = $this->l('Manage DSS');
        $this->description = $this->l('Manage DssModule');
    }

    public function install()
    {
        if (!parent::install()
            //|| !$this->registerHook('header')
            //|| !$this->registerHook('home')
            //|| $this->_createTables() == false
            )
            return false;

        return true;

    }

    function uninstall()
    {
        /*$db = Db::getInstance();
        $query = 'DROP TABLE `'._DB_PREFIX_.'product_special`';
        $query1 = 'DROP TABLE `'._DB_PREFIX_.'product_special_type`';*/

        //$result = $db->Execute($query);
        //$result1 = $db->Execute($query1);

        if ((!parent::uninstall()) /*OR (!$result) OR (!$result1)*/ )
            return false;
        return true;
    }

    public function _createTables()
    {
        $db = Db::getInstance();

        $query = '

        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'dss_product_attribute` (
          `id_dss_product_attribute` bigint(12) NOT NULL AUTO_INCREMENT,
          `id_product_attribute` int(10) NOT NULL,
          `id_product` int(10) unsigned NOT NULL,
          `price` decimal(20,6) NOT NULL DEFAULT "0.000000",
          `quantity` int(10) NOT NULL DEFAULT "0",
          `available_date` date NOT NULL,
          PRIMARY KEY (`id_dss_product_attribute`),
          KEY `product_attribute_product` (`id_product`),
          KEY `product_default` (`id_product`),
          KEY `id_product_id_product_attribute` (`id_product`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

			';

        $query1 = '

        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'dss_product_feature` (
          `id_dss_product_feature` bigint(12) NOT NULL AUTO_INCREMENT,
          `id_dss_product_attribute` bigint(12) NOT NULL,
          `id_feature` int(10) unsigned NOT NULL,
          PRIMARY KEY (`id_dss_product_feature`),
          KEY `product_attribute_product` (`id_feature`),
          KEY `product_default` (`id_feature`),
          KEY `id_product_id_product_attribute` (`id_feature`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


			';
        $query2 = '

        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'dss_product_feature_value` (
          `id_dss_product_feature_value` bigint(12) NOT NULL AUTO_INCREMENT,
          `id_dss_product_feature` bigint(12) NOT NULL,
          `id_feature_value` int(10) unsigned NOT NULL,
          PRIMARY KEY (`id_dss_product_feature_value`),
          KEY `product_attribute_product` (`id_feature_value`),
          KEY `product_default` (`id_feature_value`),
          KEY `id_product_id_product_attribute` (`id_feature_value`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

			';


        //$result = $db->Execute($query);
        //$result1 = $db->Execute($query1);
/*
        if ((!$result)&&(!$result1))
            return false;
*/
        return true;
    }

    public function initProcess()
    {

        if(Tools::getValue('fc')!='')
        {


        }
        else
        {

            $this->context->controller->addJs(DSS_JS_URI.'dss.js', 'all');
            $this->context->controller->addCSS(DSS_CSS_URI.'dss.css', 'all');

            if (Tools::isSubmit('update'.$this->name))
            {
                $action = 'updateData';
                $id_product_special=Tools::getValue('id_product_special');
                $nameAction='Update';

            }elseif(Tools::isSubmit('add'.$this->name))
            {
                $nameAction='Add';
                $action = 'insert';
            }else{
                $action = 'save';

            }

            $whereQuery='';
            $name='';
            $quantity='';
            $price='';
            $name=(string)Tools::getValue('name');
            $quantity=(int)Tools::getValue('quantity');
            $price=(int)Tools::getValue('price');

            if(($name)&&($quantity)&&($price)){

                $whereQuery .= ' prd_name LIKE "%'.$name.'%" AND quantity>='.$quantity.' AND price>='.$price;

            }elseif(($name)&&($quantity)){

                $whereQuery .= ' prd_name LIKE "%'.$name.'%" AND quantity>='.$quantity;

            }elseif(($quantity)&&($price)){

                $whereQuery .= ' quantity>='.$quantity.' AND price>='.$price;

            }elseif(($name)&&($price)){

                $whereQuery .= ' prd_name LIKE "%'.$name.'%" AND price>='.$price;

            }elseif(($quantity)){

                $whereQuery .= ' quantity>='.$quantity;

            }elseif(($price)){

                $whereQuery .= ' price>='.$price;

            }elseif(($name)){

                $whereQuery .= ' prd_name LIKE "%'.$name.'%"';

            }

            $page=(int)Tools::getValue('page');
            if($page)
            {
                $page=$page;
            }else{
                $page=1;
            }
            $CountRows=30;
            $countrows=$this->getCountListContent($this->context->language->id,0,0,$whereQuery);
            if($countrows>$CountRows){
                $countpage=$countrows/$CountRows;
                $countpage=(int)$countpage+1;
            }else{
                $countpage=1;
            }


            $FeatureArr=$this->getListFeatureDss($this->context->language->id);
            foreach ($FeatureArr as $Feature){

                if ((int)$Feature['id_feature']!=0)
                {
                    $result[$Feature['id_feature']]=$this->getListFeatureValueDss($this->context->language->id,$Feature['id_feature']);
                    $this->context->smarty->assign(array(
                        'ListFeatureValueDss_'.$Feature['id_feature'] => $result[$Feature['id_feature']]
                    ));
                    //$result['link'] = $this->context->link->getProductLink((int)$result['id_product'], $result['link_rewrite'], $result['category'], $result['ean13']);
                }

            }

            $this->context->smarty->assign(array(
                'Action_Form' => 'resultRecordAttribute'
            ,'LinkSiteName' => 'List Attribute'
            ,'address' => __PS_BASE_URI__
            ,'currentpage' => $page
            ,'countpage' => $countpage
            ,'name' => $name
            ,'quantity' => $quantity
            ,'price' => $price
            ,'IconDssModuleImage' => '../modules/'.$this->name.'/logo-dss.png'
            ,'LinkSave' =>  AdminController::$currentIndex.'&configure='.$this->name.'&'.$action.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')
            ,'LinkBack' =>  AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules')
            ,'ProductArr' => $this->getListContent($this->context->language->id,0,0,$page,$CountRows,$whereQuery)
            ,'FeatureArr' => $FeatureArr
            ));

        }



    }


    public function getContent()
    {
        $html = '';
        //$content = $this->displayForm();

        //return $content;
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');

        if (Tools::getValue('save'.$this->name)==1){
            $id_product_attributes = Tools::getValue('product_attribute');

            foreach ($id_product_attributes as $id_product_attribute)
            {

                $price=Tools::getValue('price_'.$id_product_attribute);
                $quantity=Tools::getValue('quantity_'.$id_product_attribute);

                $table='product_attribute';
                $filed='price';
                $where_syn='WHERE';
                $where='id_product_attribute='.$id_product_attribute;
                $price_db=$this->getValue($table,$filed,$where_syn.' '.$where);
                if($price!=$price_db){

                    $dataArr = array(
                        'price'=>(int)$price
                    );

                    $result_update=$this->update($table,$this->context->language->id,$dataArr,$where);

                    if($result_update=='OK'){
                        $html .= $this->displayConfirmation($this->l('Update Price attribute Product record for id <b>'.$id_product_attribute.'</b> successfully'));
                    }else{
                        $html .= $this->displayConfirmation($this->l('<font color="#000000">Update Price attribute Product record for id <b>'.$id_product_attribute.'</b> --> <b>unsuccessfully</b></font>'));
                    }

                }

                $table='stock_available';
                $filed='quantity';
                $where_syn='WHERE';
                $where='id_product_attribute='.$id_product_attribute;
                $quantity_db=$this->getValue($table,$filed,$where_syn.' '.$where);

                $filed='id_product';
                $id_product_db=$this->getValue($table,$filed,$where_syn.' '.$where);

                if($quantity_db!=null){//update in db


                    if($quantity!=$quantity_db){

                        $dataArr = array(
                            'quantity'=>(int)$quantity
                        );
                        $result_update=$this->update($table,$this->context->language->id,$dataArr,$where);

                        if($result_update=='OK'){
                            $where='id_product='.$id_product_db.' AND id_product_attribute=0';
                            $filed='quantity';
                            $total_quantity_db=$this->getValue($table,$filed,$where_syn.' '.$where);

                            $dataArr = array(
                                'quantity'=>(int)$quantity+(int)$total_quantity_db
                            );
                            $result_update=$this->update($table,$this->context->language->id,$dataArr,$where);

                            if($result_update=='OK'){
                                $html .= $this->displayConfirmation($this->l('Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> for id product <b>'.$id_product_db.'</b> successfully'));
                            }else{
                                $html .= $this->displayConfirmation($this->l('<font color="#000000">Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> for id product <b>'.$id_product_db.'</b> --> <b>unsuccessfully</b></font>'));
                            }

                            $html .= $this->displayConfirmation($this->l('Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> successfully'));
                        }else{
                            $html .= $this->displayConfirmation($this->l('<font color="#000000">Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> --> <b>unsuccessfully</b></font>'));
                        }

                    }

                }else{//insert to db

                    $table='product_attribute';
                    $filed='id_product';
                    $where_syn='WHERE';
                    $where='id_product_attribute='.$id_product_attribute;
                    $id_product_db=$this->getValue($table,$filed,$where_syn.' '.$where);

                    $dataArr = array(
                        'id_product'=>$id_product_db
                    ,'id_product_attribute'=>$id_product_attribute
                    ,'id_shop'=>1
                    ,'id_shop_group'=>0
                    ,'quantity'=>$quantity
                    ,'depends_on_stock'=>0
                    ,'out_of_stock'=>2
                    );

                    $result_add=$this->add('stock_available',$this->context->language->id,$dataArr,'id_stock_available','id_product_attribute="'.$id_product_attribute.'"','');

                    if($result_add=='OK'){
                        $table='stock_available';
                        $where='id_product='.$id_product_db.' AND id_product_attribute=0';
                        $filed='quantity';
                        $total_quantity_db=$this->getValue($table,$filed,$where_syn.' '.$where);

                        $dataArr = array(
                            'quantity'=>(int)$quantity+(int)$total_quantity_db
                        );
                        $result_update=$this->update($table,$this->context->language->id,$dataArr,$where);

                        if($result_update=='OK'){
                            $html .= $this->displayConfirmation($this->l('Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> for id product <b>'.$id_product_db.'</b> successfully'));
                        }else{
                            $html .= $this->displayConfirmation($this->l('<font color="#000000">Update quantity attribute Product record for id <b>'.$id_product_attribute.'</b> for id product <b>'.$id_product_db.'</b> --> <b>unsuccessfully</b></font>'));
                        }

                        $html .= $this->displayConfirmation($this->l('Insert quantity attribute Product record for id <b>'.$id_product_attribute.'</b> successfully'));
                    }else{
                        $html .= $this->displayConfirmation($this->l('<font color="#000000">Insert quantity attribute Product record for id <b>'.$id_product_attribute.'</b> --> <b>unsuccessfully</b></font>'));
                    }

                }

            }//end foreach

            die(Tools::jsonEncode(array(
                'htmlMessage' => $html
            ,'LinkSiteName' => 'List Message on Attribute'
            ,'LinkBack' =>  AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')

            )));
            /*
            $this->context->smarty->assign(array(
             'htmlMessage' => $html
            ,'LinkSiteName' => 'List Message on Attribute'
            ,'LinkBack' =>  AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules')


            ));
            return $this->display(__FILE__, 'dssresult.tpl');
            */

        }
        elseif (Tools::isSubmit('insert'.$this->name))
        {


        }
        elseif(Tools::isSubmit('updateData'.$this->name))
        {


        }

        if (Tools::isSubmit('update'.$this->name))
        {
            $this->context->controller->addJqueryUI('ui.datepicker');
            return $this->display(__FILE__, 'dssmodule.tpl');

        }
        elseif(Tools::isSubmit('add'.$this->name)){
            $this->context->controller->addJqueryUI('ui.datepicker');
            return $this->display(__FILE__, 'dssmodule.tpl');
        }
        elseif (Tools::isSubmit('delete'.$this->name))
        {
            $id_product_special = (int)(Tools::getValue('id_product_special'));
            $result_type = $this->getListContent($this->context->language->id,$id_product_special);
            $result_address=$result_type[0]['upload_address'];
            $result_file=$result_type[0]['file'];
            $result_name=$result_type[0]['name'];
            $file=_PS_UPLOAD_DIR_.$result_address.$result_file;
            //break;

            if (!unlink($file))
            {
                $result='101';
            }
            else
            {
                $result=$this->deleted('product_special','id_product_special='.$id_product_special);
                //$result='OK';
            }


            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&action=deleted&result='.$result.'&token='.Tools::getAdminTokenLite('AdminModules'));

        }
        elseif (Tools::getValue('action')=='deleted')
        {

            $result = Tools::getValue('result');

            if($result=='OK')
            {
                $html .= '<div class="conf confirm">'.$this->l('Deleted data from database and file from site.').'</div>';
            }elseif($result=='100'){

                $html .= '<div class="conf confirm">'.$this->l('Miss Data.').'</div>';

            }elseif($result=='101'){

                $html .= '<div class="conf confirm">'.$this->l('Miss File.').'</div>';

            }else{
                $html .= '<div class="conf confirm">'.$this->l('You have error in add data.').'</div>';
            }

            $helper = $this->initList();
            return $html.$helper->generateList($this->getListContent($this->context->language->id,0,1), $this->fields_list);

        }
        else
        {
            //$helper = $this->initList();
            //return $html.$helper->generateList($this->getListContent($this->context->language->id,0,0), $this->fields_list);
            $this->context->smarty->assign(array(
                'htmlMessage' => $html

            ));
            return $this->display(__FILE__, 'dssmodule.tpl');

        }



    }

    public  function getValue($table,$filed,$where='',$join='')
    {

        $SQL='SELECT '.$filed.' FROM '._DB_PREFIX_.$table.'
        '.$join.'
        '.$where.'
        ';

        return $results =  Db::getInstance()->getValue($SQL);
    }

    protected  function getValueFromAnotherSite($id_lang = 1,$id_product_attribute = 0)
    {
        $where=' WHERE ppsln.id_lang='.$id_lang;
        if($id_product_attribute>0){
            $where.= ' AND ppsp.id_product_attribute='.$id_product_attribute;
        }

        $SQL='
        SELECT ppsp.name product_name_site,
        ppsp.id_product_attribute,
        ppsl.link product_link_site,
        ppsln.name site_name,
        ppsln.tags ,
        ppsln.tags1 ,
        ppsln.tags2
        FROM `'._DB_PREFIX_.'product_price_site_productname` ppsp
        INNER JOIN '._DB_PREFIX_.'product_price_site_link ppsl ON ppsl.id_product_price_site_productname = ppsp.id_product_price_site_productname
        INNER JOIN '._DB_PREFIX_.'product_price_site_lang ppsln ON ppsln.id_product_price_site = ppsl.id_product_price_site

        '.$where.'

        ';

        $results = Db::getInstance()->executeS($SQL);
        if(count($results)>0){
            foreach ($results as &$result){
                $result['tag1'] = base64_encode($result['tags']);
                $result['tag2'] = base64_encode($result['tags1']);
                $result['tag3'] = base64_encode($result['tags2']);
            }

                return $results;
        }
        else{
            return null;
        }
    }


    protected function getListContent($id_lang,$id_product_attribute = 0,$id_product = 0, $page = 0, $CountRows = 30,$whereQuery = '' )
    {
        $Q_Limit='';

        $PageResult = $page;

        if (($PageResult>1)) {
            $PageRows = ($PageResult * $CountRows) - $CountRows;
            $Q_Limit = 'LIMIT ' . $PageRows . ' , ' . $CountRows . '';
        } else {
            $Q_Limit = 'LIMIT 0 , ' . $CountRows . '';
        }


        $sub_query='';
        if(($id_product>0)&&($whereQuery==''))
        {
            $sub_query = ' WHERE p.id_product='.$id_product;
        }elseif(($id_product_attribute>0)&&($whereQuery=='')){
            $sub_query = ' WHERE pa.id_product_attribute='.$id_product_attribute;
        }elseif(($id_product>0)&&($whereQuery!='')){
            $sub_query = ' WHERE p.id_product='.$id_product.' AND '.$whereQuery;
        }elseif(($id_product_attribute>0)&&($whereQuery!='')){
            $sub_query = ' WHERE pa.id_product_attribute='.$id_product_attribute.' AND '.$whereQuery;
        }elseif(($whereQuery!='')){
            $sub_query = ' WHERE '.$whereQuery;
        }

        $SQL = '
        SELECT * FROM
        (
        SELECT p.id_product,
            (SELECT MAX(image_shop.`id_image`)  FROM '._DB_PREFIX_.'image i
            '.Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
            WHERE i.id_product = p.id_product
            ) id_image,
            pl.name prd_name,
            pa.id_product_attribute,pa.price,p.id_category_default as category, pl.link_rewrite, p.ean13,
            (SELECT quantity FROM '._DB_PREFIX_.'stock_available sa
            WHERE sa.id_product=p.id_product and sa.id_product_attribute=pa.id_product_attribute ) as quantity,
            (
            SELECT GROUP_CONCAT(attName," #",IF(color!="",id_attribute,0),"#")
            FROM
            (
            SELECT
            CONCAT("(",agl.name," / ",al.name,")") as attName ,pac.id_attribute,a.id_attribute_group,a.color,pac.id_product_attribute
            FROM '._DB_PREFIX_.'product_attribute_combination pac
            INNER JOIN '._DB_PREFIX_.'attribute a ON pac.id_attribute=a.id_attribute
            INNER JOIN '._DB_PREFIX_.'attribute_group ag ON a.id_attribute_group=ag.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_group_lang agl ON ag.id_attribute_group=agl.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_lang al ON a.id_attribute=al.id_attribute
            ) as tmp WHERE id_product_attribute=pa.id_product_attribute
            ) as attNameANDGroup


            FROM '._DB_PREFIX_.'product_attribute pa
            INNER JOIN '._DB_PREFIX_.'product p ON pa.id_product=p.id_product
            INNER JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product
            ) as tmpQuery
            '.$sub_query.'

            ORDER BY id_product
            '.$Q_Limit;

        $results =  Db::getInstance()->executeS($SQL);

        $FeatureArr=$this->getListFeatureDss($this->context->language->id);


        $count=0;
        $prd=array();
        foreach ($results as &$result){


            $table='dss_product_attribute';
            $where='id_product_attribute='.$result['id_product_attribute'].' AND id_product='.$result['id_product'].'';
            $filed='id_dss_product_attribute';
            $where_syn='WHERE';

            $dss_prd_att_db=$this->getValue($table,$filed,$where_syn.' '.$where);

            if((int)$dss_prd_att_db){

                foreach ($FeatureArr as $Feature)
                {

                    if ((int)$Feature['id_feature']!=0)
                    {


                        $table='dss_product_feature';
                        $where='id_dss_product_attribute='.$dss_prd_att_db.' AND id_feature='.$Feature['id_feature'].'';
                        $filed='id_feature_value';
                        $where_syn='WHERE';

                        $dss_prd_att_F_V_db=$this->getValue($table,$filed,$where_syn.' '.$where);

                        $result['DSS_Feature_'.$Feature['id_feature'].'_'.$result['id_product_attribute']]=$dss_prd_att_F_V_db;


                    }

                }

            }


            //'<div dir="ltr">'.$result['attNameANDGroup'];
            $attData=explode("#",$result['attNameANDGroup']);
            $count=0;
            $attNameANDGroup='';
            $id_attribute='';
                foreach ($attData as $attRes)
                {

                //echo '<div dir="ltr">'.$attRes;
                if((is_int((int)$attRes))&&((int)$attRes>0)){
                    $id_attribute=$attRes;
                }elseif((is_string($attRes))&&($attRes!='0')){
                    $attNameANDGroup.=$attRes;
                }

            $count++;
                }
            //print_r($attData);
            $result['attNameANDGroup']=$attNameANDGroup;
            $result['id_attribute']=$id_attribute;
            //echo _PS_IMG_DIR_.'co/'.$result['id_attribute'].'.jpg';

            if(file_exists(_PS_IMG_DIR_.'co/'.$result['id_attribute'].'.jpg'))
            {
                $result['ColorAtt']= '<img src="'._PS_IMG_.'/co/'.$result['id_attribute'].'.jpg'.'" />' ;
            }else{
                $resColor=$this->getValue('attribute','color',' WHERE id_attribute='.$result['id_attribute'].' AND color LIKE "#%"');
                $result['ColorAtt']= '<div style="float: left; width: 18px; height: 12px; border: 1px solid #996633; background-color: '.$resColor.'; margin-right: 4px;"></div>';
            }


            $prd[$count]=$result['id_product'];

            if (fmod($count,2) == 0){
                //if (($prd[$count] != $prd[$count-1])){
                //$count=0;
                $result['Color'] = "#EFEFEF";

            }else{

                $result['Color'] = "#CDCDCD";

            }

            if ((int)$result['id_product']!=0)
            {
                $result['link'] = $this->context->link->getProductLink((int)$result['id_product'], $result['link_rewrite'], $result['category'], $result['ean13']);
            }

            if(isset($result['id_image'])){
                $result['link_lmg'] = $this->context->link->getImageLink($result['link_rewrite'], $result['id_image'], 'small_allinmart');
            }
            else
            {
                $result['link_lmg'] = 'NoPic';
            }

            $result_site = $this->getValueFromAnotherSite($id_lang = 1,$result['id_product_attribute']);
            //print_r($result_site);
            if(count($result_site)>0){

                $result['result_site'] = $result_site;
                /* $result['product_link_site'] = $result_site[0]['product_link_site'];
                 $result['site_name'] = $result_site[0]['site_name'];
                 $result['tag1'] = base64_encode($result_site[0]['tag1']);
                 $result['tag2'] = base64_encode($result_site[0]['tag2']);
                 $result['tag3'] = base64_encode($result_site[0]['tag3']);*/

            }


            $count++;
        }

        return $results;
    }

    protected function getCountListContent($id_lang,$id_product_attribute = 0,$id_product = 0, $whereQuery = '')
    {

        $sub_query='';
        if(($id_product>0)&&($whereQuery==''))
        {
            $sub_query = ' WHERE id_product='.$id_product;
        }elseif(($id_product_attribute>0)&&($whereQuery=='')){
            $sub_query = ' WHERE id_product_attribute='.$id_product_attribute;
        }elseif(($id_product>0)&&($whereQuery!='')){
            $sub_query = ' WHERE id_product='.$id_product.' AND '.$whereQuery;
        }elseif(($id_product_attribute>0)&&($whereQuery!='')){
            $sub_query = ' WHERE id_product_attribute='.$id_product_attribute.' AND '.$whereQuery;
        }elseif(($whereQuery!='')){
            $sub_query = ' WHERE '.$whereQuery;
        }

        $SQL = '
        SELECT COUNT(*) FROM
        (
        SELECT p.id_product,
            (SELECT MAX(image_shop.`id_image`)  FROM '._DB_PREFIX_.'image i
            '.Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
            WHERE i.id_product = p.id_product
            ) id_image,
            pl.name prd_name,
            pa.id_product_attribute,pa.price,p.id_category_default as category, pl.link_rewrite, p.ean13,
            (SELECT quantity FROM '._DB_PREFIX_.'stock_available sa
            WHERE sa.id_product=p.id_product and sa.id_product_attribute=pa.id_product_attribute ) as quantity,
            (
            SELECT GROUP_CONCAT(attName)
            FROM
            (
            SELECT
            CONCAT("(",agl.name," / ",al.name,")") as attName ,pac.id_attribute,a.id_attribute_group,pac.id_product_attribute
            FROM '._DB_PREFIX_.'product_attribute_combination pac
            INNER JOIN '._DB_PREFIX_.'attribute a ON pac.id_attribute=a.id_attribute
            INNER JOIN '._DB_PREFIX_.'attribute_group ag ON a.id_attribute_group=ag.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_group_lang agl ON ag.id_attribute_group=agl.id_attribute_group
            INNER JOIN '._DB_PREFIX_.'attribute_lang al ON a.id_attribute=al.id_attribute
            ) as tmp WHERE id_product_attribute=pa.id_product_attribute
            ) as attNameANDGroup


            FROM '._DB_PREFIX_.'product_attribute pa
            INNER JOIN '._DB_PREFIX_.'product p ON pa.id_product=p.id_product
            INNER JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product=pl.id_product
            ) as tmpCount
            '.$sub_query.'
            ';

        $results =  Db::getInstance()->getValue($SQL);

        return $results;
    }

    public function getListFeatureDss($id_lang,$id_feature =0)
    {
        $sub_query='';
        if($id_feature>0)
        {
            $sub_query = 'AND f.id_feature='.$id_feature;
        }

        $SQL = '
			SELECT f.*,fl.name feature_name
			FROM `'._DB_PREFIX_.'feature` f
			INNER JOIN '._DB_PREFIX_.'feature_lang fl ON f.id_feature=fl.id_feature
			WHERE fl.`id_lang` = '.(int)$id_lang.' AND f.isDss=1 '.$sub_query .'
			ORDER BY f.position';

        return  Db::getInstance()->executeS($SQL);
    }

    protected function getListFeatureValueDss($id_lang,$id_feature =0,$id_feature_value =0)
    {
        if($id_feature_value>0)
        {
            $sub_query = 'AND fv.id_feature_value='.$id_feature_value;
        }elseif($id_feature>0){
            $sub_query = 'AND fv.id_feature='.$id_feature;
        }

        $SQL = '
			SELECT fv.*,fvl.value
			FROM `'._DB_PREFIX_.'feature_value` fv
			INNER JOIN '._DB_PREFIX_.'feature_value_lang fvl ON fv.id_feature_value=fvl.id_feature_value
			WHERE fvl.`id_lang` = '.(int)$id_lang.' '.$sub_query;

        return  Db::getInstance()->executeS($SQL);
    }

    protected function getListProduct($id_lang,$id_product=0)
    {
        if($id_product>0)
        {
            $sub_query = 'AND pl.id_product='.$id_product;
        }

        $SQL = '
			SELECT id_product, name
			FROM `'._DB_PREFIX_.'product_lang` pl
			WHERE pl.`id_lang` = '.(int)$id_lang.' '.$sub_query.' ORDER BY id_product DESC';

        return  Db::getInstance()->executeS($SQL);
    }

    public function deleted($table,$WhereDeleted)
    {
        if(($table)&&($WhereDeleted))
        {
        $SQL = '
        DELETE FROM `'._DB_PREFIX_.''.$table.'` WHERE '.$WhereDeleted.'
        ';

            Db::getInstance()->executeS($SQL);

            return 'OK'; //success deleted
        }

        return 100; //miss data

    }

    public function add($table,$id_lang,$dataArr,$selectCheckParam,$whereCheckParam,$ifselectCheckParam='')
    {

        /*
                $SQL = '
                INSERT INTO `'._DB_PREFIX_.'wiki` (parameter_wiki,id_lang,description_wiki,date_add,status)
                VALUES("'.$parameter_wiki.'",'.(int)$id_lang.',"'.$description_wiki.'","'.$date_add.'",'.(int)$status.')
                ';*/
        $SQL = '
			SELECT '.$selectCheckParam.'
			FROM '._DB_PREFIX_.''.$table.'
			WHERE '.$whereCheckParam.' ';

        $result = Db::getInstance()->executeS($SQL);

        if(count($result)==0)
        {
            /*
            $data = array(
                'parameter_wiki' => $parameter_wiki,
                'id_lang' => $id_lang,
                'description_wiki' => $description_wiki,
                'date_add' => $date_add,
                'status' => $status
            );
            */


            if(($table)&&($dataArr))
            {
                //print_r($dataArr);
                //Db::getInstance()->insert($table, $dataArr);
                if((Db::getInstance()->insert($table, $dataArr)))
                {
                    return 'OK';
                }else{
                    return false;
                }

            }else{
                return '100'; //Miss data
            }

        }else{

            return '200'; //Duplicate record

        }

    }

    public function update($table,$id_lang,$dataArr,$where)
    {

            if(($table)&&($dataArr)&&($where))
            {
                if((Db::getInstance()->update($table, $dataArr, $where)))
                {
                    return 'OK';
                }else{
                    return false;
                }

            }else{
                return '100'; //Miss data
            }

    }



}
