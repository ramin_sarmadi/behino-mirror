<?php


require_once _PS_MODULE_DIR_ . 'dssmodule/defined.php';
//init model :
require_once DSS_ROOT . 'dssmodule.php';


class DssModuleDefaultModuleFrontController extends ModuleFrontController {

    public $view;
    public $id;
    public $object;
    public $template;
    public $allow = null;
    public $helper = null;
    public $category = null;

    function __construct() {


        parent::__construct();

        $module = new DssModule();
        $this->module = $module;
        $this->view = Tools::getValue('view');
        $this->id = Tools::getValue('id', 0);
        $this->format = Tools::getValue('format', 'normal');
        $this->lang = isset(self::$cookie->id_lang) ? self::$cookie->id_lang : 1;
        $this->object = null;
                

    }


    public function initContent() {
        parent::initContent();
        $this->prepareData();
    }

    /**
     * prepare data
     */
    public function prepareData() {


        $this->lang = isset(self::$cookie->id_lang) ? self::$cookie->id_lang : 1;


        $lang_id = self::$cookie->id_lang;

		
        switch ($this->view) {

            
            case 'searchdss':


                break;                
           
            default :
                //do some thing
                break;
        }

        $this->context->smarty->assign('test1', 'tessss');

    }

    /**
     * Function to add some media file e.g : css, js ...
     */
    public function setMedia() {
        parent::setMedia();

        //set global medias :


        $this->context->controller->addJs(DSS_JS_URI.'dss.js', 'all');
        $this->context->controller->addCSS(DSS_CSS_URI.'dss.css', 'all');



    }

    /**
     * Display front content :
     */
    public function displayContent() {
        parent::displayContent();
        $this->context->smarty->assign('TEST', 'test');

        //$this->assignBlocks();
        switch ($this->view) {


            case 'searchdss':

                if ($this->format == 'normal')
                {
                    $this->context->smarty->display(DSS_ROOT.'SearchDss.tpl');
                } else {
                    $this->context->smarty->display(DSS_ROOT.'404.tpl');
                }

                break;

            default :
                //do some thing
            break;
        }



    }




}
