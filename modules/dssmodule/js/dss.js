$(document).ready(function () {
    $('.p-num').each(function () {
        $(this).click(function (event) {
            event.preventDefault();
            var numInput = $(this).find('a').text();
            $('#number-input').val(numInput);
            $('#submit').trigger('click');
        });
    });
    $('#next').click(function(event){
        event.preventDefault();
       var pagenumber = parseInt($('.activepage').text());
        pagenumber = pagenumber + 1;
        $('#number-input').val(pagenumber);
        $('#submit').trigger('click');
    });
    $('#prev').click(function(event){
        event.preventDefault();
        var pagenumber = $('.activepage').text();
        pagenumber = pagenumber - 1;
        $('#number-input').val(pagenumber);
        $('#submit').trigger('click');
    });

    $('input[type="text"]').each(function () {
        var th = $(this);
        th.keyup(function () {
            th.closest('tr').find('.savedata').attr("disabled", false);
        });
    });
//    $(window).scroll(function () {
//        var forTopPos = $('#fortoppos').offset().top;
//        var currentPos = $(document).scrollTop();
//        if (currentPos > forTopPos) {
//            $('.toolbarBox ').addClass('fixpos');
//        }
//        else {
//            $('.toolbarBox ').removeClass('fixpos');
//        }
//    });
    $('.show-get-data').each(function(){
       $(this).click(function(event){
        event.preventDefault();
           var num1 = $(this).data('num');
           var tr = $('#'+ num1);
           tr.slideToggle('slow');
       });
    });
    $('.savedata').each(function () {
        var ths = $(this);
        ths.click(function (event) {
            event.preventDefault();
            var product_attribute = ths.closest('tr').find('.save-input').val();
            var priceVal = ths.closest('tr').find('.price').val();
            var QVal = ths.closest('tr').find('.quantity').val();
            var idProduct = ths.closest('td').find('.id_product').val();

            //----------------------------------------TEST

            var feature_value_104_663 = ths.closest('tr').find('.class_id_feature_value_104_663').val();
            var feature_value_105_663 = ths.closest('tr').find('.class_id_feature_value_105_663').val();
            var feature_value_106_663 = ths.closest('tr').find('.class_id_feature_value_106_663').val();
            var feature_value_107_663 = ths.closest('tr').find('.class_id_feature_value_107_663').val();
            var feature_value_108_663 = ths.closest('tr').find('.class_id_feature_value_108_663').val();
            var feature_value_116_663 = ths.closest('tr').find('.class_id_feature_value_116_663').val();

            var id_feature_value_arr = [
                 {'class_id_feature_value_104_663':feature_value_104_663}
                ,{'class_id_feature_value_105_663':feature_value_105_663}
                ,{'class_id_feature_value_106_663':feature_value_106_663}
                ,{'class_id_feature_value_107_663':feature_value_107_663}
                ,{'class_id_feature_value_108_663':feature_value_108_663}
                ,{'class_id_feature_value_116_663':feature_value_116_663}
            ];





            //-------------------------------------------

            $.ajax({
                url: address + 'modules/dssmodule/ajax.php?controller=AdminModules&action=save&price_' + product_attribute + '=' + priceVal + '&quantity_' + product_attribute + '=' + QVal,
                data: {
                    product_attribute: product_attribute,
                    product_id: idProduct,
                    feature_value_arr: id_feature_value_arr,
                    ramin: 1
                    /*  priceName: priceVal,
                     QName: QVal*/
                },
                type: 'post',
                dataType: "json",
                success: function (data) {
                    alert(data.htmlMessage);
                    ths.attr("disabled", "disabled");
                    ths.attr("checked", false);
                }

            });
        });
    });
    $('.getlink').each(function () {
        $(this).click(function (event) {
            event.preventDefault();
            var clicked = $(this);
            var link = clicked.data('link');
            var tag1 = clicked.data('tag1');
            var tag2 = clicked.data('tag2');
            var tag3 = clicked.data('tag3');
            var priceshow = clicked.closest(".site-box").find(".priceshow");
            if(priceshow.html().length > 0)
            {
                priceshow.empty();
            }
            clicked.closest(".site-box").find(".loading").show().css('display','block');
            ajaxCall(link, tag1, tag2, tag3, clicked);
        });
    });
    function ajaxCall(urlParam, tagParam, tagParam1, tagParam2, clicked) {
        $.ajax({
            url: address + 'modules/dssmodule/ajax.php?controller=AdminModules&action=requestsite',
            data: {
                urlsite: urlParam,
                tag: tagParam,
                tag1: tagParam1,
                tag2: tagParam2
            },
            type: 'post',
            dataType: "json",
            success: function (data) {
                var priceString = data.resultshtml['Tag'];
                var tag1String = data.resultshtml['Tag1'];
                var tag2String = data.resultshtml['Tag2'];
                clicked.closest(".site-box").find(".loading").hide();
                var priceshow = clicked.closest(".site-box").find(".priceshow");
                if (priceString != null) {

                    priceshow.append(
                        '<div>'
                            + '<a href="'
                            + urlParam
                            + '" >'
                            + priceString
                            + '</a>'
                            + '</br>'
                            + '<a href="'
                            + urlParam
                            + '" >'
                            + tag1String
                            + '</a>'
                            + '</br>'
                            + '<a href="'
                            + urlParam
                            + '" >'
                            + tag2String
                            + '</a>'
                            + '</div>'
                    );
                }
                else{
                    priceshow.append('<a href="'+urlParam+'" target="_blank">'+'قیمت وجود ندارد'+'</a>');
                }
            },
            error: function () {
                console.log('error');
            }
        });
    }
});
