<!-- Data Sheet -->
{if ($datasheet != 0)}
    <div style="clear:both"></div>
    <div id="groupedfeatures" style="float:right; width:100%; margin-top:20px; margin-bottom:30px;">
        {if $loc=='footer'}<h2>{l s='Data Sheet' mod='sodfeaturesgrouped'}</h2>{/if}

        <table width="100%" style="border-collapse:collapse; border-spacing:0;">
            {foreach from=$datasheet item=sheet}
                <tr style="background:{$groupcellcolor}; border:1px solid #999999">
                    <td align="right" valign="middle" colspan="2" style="border:1px solid #999999; padding:5px;">
<span style="color:{$groupfontcolor}; font-family:tahoma, Helvetica, sans-serif; font-size:{$groupfontsize}; font-weight:bold">
{$sheet.groupname} 
</span>
                    </td>
                </tr>
                {foreach from=$sheet.features item=feat}

                    {foreach from=$sodprodfeat item=feature}

                        {if ($feature.id_feature == $feat) and ($feature.id_group == $sheet.id_group) and  ($feature.featurevalue != '')}
                            <tr border="1px" style="border:1px solid #999999">
                                <td align="right" valign="middle" width="50%"
                                    style="border:1px solid #999999; padding:5px; background:{$featurecellcolor}; ">
<span style="color:{$featurefontcolor}; font-family:tahoma, Helvetica, sans-serif; font-size:{$featurefontsize}; font-weight:bold">
{$feature.feature_name}
</span>
                                </td>
                                <td align="right" valign="middle" width="50%"
                                    style="border:1px solid #999999; padding:5px; background:{$featurevaluecellcolor};">
<span style="color:{$featurevaluefontcolor}; font-family:tahoma, Helvetica, sans-serif; font-size:{$featurevaluefontsize};">
{$feature.featurevalue}
</span>
                                </td>
                            </tr>
                        {/if}

                    {/foreach}

                {/foreach}

            {/foreach}
        </table>
    </div>
    <div style="clear:both"></div>
{/if}
<!-- /Data Sheet Table -->


