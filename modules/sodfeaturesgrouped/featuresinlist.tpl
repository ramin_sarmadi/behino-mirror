{if ($datasheet != 0)}
    <div style="clear:both"></div>
    <div id="groupedfeatures" style="float:right; width:100%; margin-top:20px; margin-bottom:30px;">
        {if $loc=='footer'}<h2>{l s='Data Sheet' mod='sodfeaturesgrouped'}</h2>{/if}


        {assign var=i value=0}
        {foreach from=$datasheet item=sheet}
            {assign var=i value=$i+1}
            <table width="40%"
                   style="border-collapse:collapse; border-spacing:0; float:{if $i%2==1}right{else}right;{/if}">
                <tr style="background:none; border:0px solid #999999;">
                    <td align="right" valign="middle" colspan="2"
                        style="border:0px solid #999999; border-bottom:2px solid {$groupfontcolor}; padding:5px;">
<span style="color:{$groupfontcolor}; font-family:Arial, Helvetica, sans-serif; font-size:{$groupfontsize}; font-weight:bold">
{$sheet.groupname} 
</span>
                    </td>
                </tr>

                {foreach from=$sheet.features item=feat}

                    {foreach from=$sodprodfeat item=feature}

                        {if ($feature.id_feature == $feat) and ($feature.featurevalue != '')}
                            <tr border="0px" style="border:0px solid #999999">
                                <td align="right" valign="middle" width="50%"
                                    style="border:0px solid #999999; padding:5px; background:{$featurecellcolor}; ">
<span style="color:{$featurefontcolor}; font-family:Arial, Helvetica, sans-serif; font-size:{$featurefontsize}; font-weight:bold">
{$feature.feature_name}
</span>
                                </td>
                                <td align="right" valign="middle" width="50%"
                                    style="border:0px solid #999999; padding:5px; background:{$featurevaluecellcolor};">
<span style="color:{$featurevaluefontcolor}; font-family:Arial, Helvetica, sans-serif; font-size:{$featurevaluefontsize};">
{$feature.featurevalue}
</span>
                                </td>
                            </tr>
                        {/if}

                    {/foreach}

                {/foreach}
            </table>
            {if $i%2==0}
                <div style="clear:both; height:30px;"></div>
            {/if}

        {/foreach}
    </div>
{/if}