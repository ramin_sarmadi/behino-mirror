<?php

//include_once(PS_ADMIN_DIR.'/../classes/AdminTab.php');

class AdminFeaturesGroups extends AdminTab
{
	/** @var string The field we are sorting on */
	protected $_sortBy = 'date';
	private $_tabClass = 'AdminFeaturesGroups';
	private $_module = 'sodfeaturesgrouped';
    private $_modulePath = '';
	private $_html = '';
	private $_id_lang;
	private $_defaultLanguage;
	private $_iso;

	public function __construct()
	{
		$this->className = 'Features in Groups';
		$this->displayName = $this->l('Features in Groups');
		$this->_list = true;
		$this->multishop_context_group=true;
		$this->multishop_context= 1;
		$this->base_url = Configuration::get('PS_SSL_ENABLED') ? preg_replace('/^http:/','https:', _PS_BASE_URL_) : _PS_BASE_URL_;
		$this->_modulePath =  _PS_MODULE_DIR_ . $this->_module . '/backoffice/';
		parent::__construct();
	}

	protected function loadObject($opt = false)
	{
		if ($id = Tools::getValue($this->identifier))
			return new $this->className($id);
		return new $this->className();
	}

	 public function getContent()
    {
        
		/////
    }
	
	
	public function displayList(){
           
		global $cookie, $currentIndex;
	
   if (substr(_PS_VERSION_,0,3) == '1.5') $id_shop = (int)Context::getContext()->shop->id;
		 else $id_shop=0;
		 
		 
		 $token=Tools::getValue('token');
		 
		 
		
	
	if (substr(_PS_VERSION_,0,3) == '1.5') $version15=true;
	else  $version15=false;
		 
	$numcat=Configuration::get('FEATURES_GROUPED_NUMCAT');  
	
	$lang=(int)($cookie->id_lang);
	

    $output='';


	
//////////////////////////////
		
		
	if 	(Tools::isSubmit('submitAddGroup')) {
	
	  $errors=0;
	//determine the last group id
	    $lastgroup=0;
		$lastid=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."features_groups ORDER BY id_group DESC LIMIT 1");
		
		foreach ($lastid as $idgroup) $lastgroup=$idgroup['id_group'];
		
		$newid= $lastgroup +1;
	
	$languages=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."lang");
		foreach ($languages as $language) {

		$field='groupname_'.$language['id_lang'];
		if ( ($_POST[$field] != '') and (strlen(str_replace(' ','',$_POST[$field])) > 0) ) {
		$errors=2;
		$name=$_POST[$field];
		
		if ($id_shop > 0)
		$insertgroup=Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."features_groups (id_group,id_shop) VALUES ('$newid','$id_shop')"); 
		else 
		$insertgroup=Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."features_groups (id_group) VALUES ('$newid')"); 

		
		break;
		if (!$insertgroup) $errors=1;
		}
					
		}
		
		
		//write group lang
		foreach ($languages as $language) {
		$field='groupname_'.$language['id_lang'];
		$lang=$language['id_lang'];
		if ( ($_POST[$field] != '') and (strlen(str_replace(' ','',$_POST[$field])) > 0) ) {


		$name=$_POST[$field];
		$insertgrouplang=Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."features_groups_lang (id_group,id_lang,groupname) VALUES ('$newid','$lang','$name')");
		if (!$insertgrouplang) $errors=1; 
		}
	
	}
	
	
	if ($errors == 2) $output.="<div class='conf confirm'>".$this->l('The group was added')."</div><br /><br />";
		else 	$output.="<div class='alert error'>".$this->l('ERROR: The group was not added correctly')."</div><br /><br />";
		
	}
////////////////////


/////////////updategroup/////////

if 	(Tools::isSubmit('submitEditGroup')) {
	
	//determine the last group id
	   
	   $groupid=$_POST['groupid']; 
		
			
	$languages=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."lang");
		foreach ($languages as $language) {
        $curlang=$language['id_lang'];
		$field='groupname_'.$language['id_lang'];
		if ( ($_POST[$field] != '') and (strlen(str_replace(' ','',$_POST[$field])) > 0) ) {
		$name=$_POST[$field];
		
		$searchlang=Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."features_groups_lang WHERE id_group='$groupid' AND id_lang='$curlang'");
		
		if ($searchlang) $updategroup=Db::getInstance()->Execute("UPDATE "._DB_PREFIX_."features_groups_lang SET groupname='$name' WHERE id_group='$groupid' AND id_lang='$curlang'");
		
		else $insertgrouplang=Db::getInstance()->Execute("INSERT INTO "._DB_PREFIX_."features_groups_lang (id_group,id_lang,groupname) VALUES ('$groupid','$curlang','$name')"); 
		
		
		}
				
		}
		
		
	 $output.="<div class='conf confirm'>".$this->l('Settings updated')."</div><br /><br />";
	
		
	}


////////////////////////////////


	$output.="<br />";
	
	
	/////design parameters
	
	if (isset($_POST['submitdesign'])) {
	 
		Configuration::updateValue('FEATURES_GROUPED_GCC', $_POST['coloroutput1']);
		Configuration::updateValue('FEATURES_GROUPED_GFC', $_POST['coloroutput2']);
		Configuration::updateValue('FEATURES_GROUPED_GFS', $_POST['groupfontsize']);
		Configuration::updateValue('FEATURES_GROUPED_FCC', $_POST['coloroutput3']);
		Configuration::updateValue('FEATURES_GROUPED_FFC', $_POST['coloroutput4']);
		Configuration::updateValue('FEATURES_GROUPED_FFS', $_POST['featurefontsize']);
		Configuration::updateValue('FEATURES_GROUPED_FVCC', $_POST['coloroutput5']);
		Configuration::updateValue('FEATURES_GROUPED_FVFC', $_POST['coloroutput6']);
		Configuration::updateValue('FEATURES_GROUPED_FVFS', $_POST['featurevaluefontsize']);
	
	
	
	$output.="<div class='conf confirm'>".$this->l('Settings updated')."</div><br /><br />";		
	
	}
	////////////////////////	


$lang=(int)($cookie->id_lang);
///javascripts

$output.="<style type='text/css'>
a {
text-decoration:none;
}

a:hover {
text-decoration:underline;
}

a:active {
text-decoration:none;
}

.upperbuttons {
text-decoration:none;
color:#0066CC;
cursor:pointer;
}

.upperbuttons:hover {
text-decoration:underline;
}


</style>";

$output.="<script type='text/javascript' src='../modules/sodfeaturesgrouped/jquery.hs'></script>";
$output.="<script type='text/javascript'>

function showhidegroups(id) {
var id='features'+id;
var status=document.getElementById(id).style.display;

if (status=='none') document.getElementById(id).style.display='block';
else document.getElementById(id).style.display='none';

}

function showhidecategs(id) {
var id='groups'+id;
var status=document.getElementById(id).style.display;

if (status=='none') document.getElementById(id).style.display='block';
else document.getElementById(id).style.display='none';

}

</script>";

$output.="<script type='text/javascript'>

function hideaddgroup() {
document.getElementById('addgroupform').style.display='none';
}

function showaddgroup() {
document.getElementById('addgroupform').style.display='block';
document.getElementById('addfeaturegroupform').style.display='none';
document.getElementById('addgroupcategform').style.display='none';
}

function hideeditgroup() {
document.getElementById('editgroupform').style.display='none';
}

function hidefeatgroup() {
document.getElementById('addfeaturegroupform').style.display='none';
}

function showfeatgroup() {
document.getElementById('addfeaturegroupform').style.display='block';

document.getElementById('addgroupform').style.display='none';
document.getElementById('addgroupcategform').style.display='none';
}

function showgroupcateg() {
document.getElementById('addgroupcategform').style.display='block';

document.getElementById('addgroupform').style.display='none';
document.getElementById('addfeaturegroupform').style.display='none';

}

function hidegroupcateg() {
document.getElementById('addgroupcategform').style.display='none';
}


function submitfeaturegroup(idshop) {
var group=document.getElementById('groupfeat').value; 
var feature=document.getElementById('feature').value; 

xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	 
	 document.getElementById('responsefeatgroup').innerHTML='..........';
     document.getElementById('responsefeatgroup').innerHTML='".$this->l('Changes saved')."';
	 document.getElementById('responsefeatgroup').style.display='block'; 
	 document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?submitFeatureGroup=1&group='+group+'&feature='+feature+'&id_shop='+idshop+'&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();

}


function submitgroupcategory(idshop) {

var group=document.getElementById('groupcateg').value; 
var categ=document.getElementById('category').value; 

xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	 
	 document.getElementById('responsegroupcateg').innerHTML='.......';
     document.getElementById('responsegroupcateg').innerHTML='".$this->l('Changes saved')."';
	 document.getElementById('responsegroupcateg').style.display='block'; 
	 
	 document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?submitGroupCategory=1&group='+group+'&category='+categ+'&id_shop='+idshop+'&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();


}


function featureup(feature,group,idshop) {
xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?featureup='+feature+'_'+group+'&id_shop='+idshop+'&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();
}


function featuredown(feature,group,idshop) {
xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?featuredown='+feature+'_'+group+'&id_shop='+idshop+'&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();
}


function groupup(group,categ,idshop) {
xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?groupup='+group+'_'+categ+'&id_shop='+idshop+'&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();
}


function groupdown(group,categ,idshop) {
xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?groupdown='+group+'_'+categ+'&id_shop='+idshop+'&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();
}


function deletegroup(group) {
xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?sters=allgroup&stersid='+group+'&id_shop=".$id_shop."&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();
}

function deletegroupincateg(group,categ) {
xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?sters=group&stersid='+group+'&deleteidsup='+categ+'&id_shop=".$id_shop."&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();
}


function deletefeature(feature,group) {
xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?sters=feature&stersid='+feature+'&deleteidsup='+group+'&id_shop=".$id_shop."&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();
}


function deletecateg(categ) {
xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById('listgroups').innerHTML=xmlhttp.responseText;
	 document.getElementById('listgroups').style.display='block'; 
	
	 
	}
  }

xmlhttp.open('GET','../modules/sodfeaturesgrouped/ajax3.php?sters=categ&stersid='+categ+'&id_shop=".$id_shop."&moduleurl=".$currentIndex."&token=".$token."&lang=".$lang."',true);
xmlhttp.send();
}


function expandgroups() {
$('.featuresingroup').show();
}

function expandcategs() {
$('.groupsincateg').show();
}

function collapsecategs() {
$('.groupsincateg').hide();
}

function collapsegroups() {
$('.featuresingroup').hide();
}


</script>";	
	


$output.="<fieldset><legend>".$this->l('Features in groups')."</legend>"; $output.="<div style='clear:both'></div><br />";		



////
$output.="<div style='clear:both'></div>";	


	
$output.='
		<br />
		<a onclick="showgroupcateg()" style="cursor:pointer"><img src="../img/admin/add.gif" border="0" /> <b>'.$this->l('Add group to category').'</b></a><br />
		<a onclick="showfeatgroup()" style="cursor:pointer"><img src="../img/admin/add.gif" border="0" /> <b>'.$this->l('Add feature to group').'</b></a><br />
		<a onclick="showaddgroup()" style="cursor:pointer"><img src="../img/admin/add.gif" border="0" /> <b>'.$this->l('Add a new group').'</b></a><br /><br />';
		$output.="<br /><br />";
		
    ////addgroup form
	

     
		$output.='
			<form id="addgroupform" style="display:none;" action="'.$currentIndex.'&token='.Tools::getValue('token').'" method="post">
			<fieldset class="width2">
				<legend><img src="../img/t/AdminFeatures.gif" />'.$this->l('Add a group').'</legend>
				<a style="float:right; margin-top:-20px; margin-right:-10px;" href="#" onclick="hideaddgroup()"><img src="../img/admin/disabled.gif"></a>
				<div style="clear:both; height:20px;"></div>
				<div id="responseaddgroup"></div>
				<div style="clear:both; height:10px;"></div>
				<label>'.$this->l('Group:').' </label>
				<div class="margin-form">';
		$languages=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."lang");	
		$activelangs=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."lang WHERE active='1'");
		$existinglanguages=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."lang WHERE active='1'");
		$sqlmaxlangid=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."lang ORDER BY id_lang DESC LIMIT 1");
		$maxlangid=(int)$sqlmaxlangid[0]['id_lang'];
		$firstlang=	$existinglanguages['id_lang'];
		for ($i=1;$i<=$maxlangid;$i++)
			$output.='
					<div id="groupvalue_'.$i.'" style="display: '.(($i == $firstlang) ? 'block' : 'none').'; float: left;">
						<input size="33" type="text" name="groupname_'.$i.'" value="" /><sup> *</sup>
						
						<img src="../img/l/'.$i.'.jpg"/>
						
						</div>';
						
			$output.='<div class="clear" style="height:1px;"></div>';
						
			
					
		foreach ($languages as $language)	{
				?>		
		<script type="text/javascript">
			function show(id,maxlang) {
			var device='groupvalue_'+id;
			for (i=1;i<=maxlang;i=i+1) {
			newid='groupvalue_' + i;
			document.getElementById(newid).style.display="none";
			}
			document.getElementById(device).style.display="block";
			
			}
			  </script>		
		
		 <?php
		
		$output.='<img id="lang_'.$language['id_lang'].'" src="../img/l/'.$language['id_lang'].'.jpg" onclick="show('.$language['id_lang'].','.$maxlangid.')" style="cursor:pointer">';
			  
		}
					
					
		$output.='
					<div class="clear"></div>
				<br /><br /><br />
				<center>
					<input type="submit" value="'.$this->l('   Save   ').'" name="submitAddGroup" class="button" />
				</center>
				<div class="clear"></div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>';
		



 ////edit group form
	
	if (isset($_GET['editgroup'])) {
     
	   $idgroup=$_GET['editgroup'];
	
	$output.='
			<form id="editgroupform" action="'.$currentIndex.'&token='.Tools::getValue('token').'" method="post">
			<fieldset class="width2">
				<legend><img src="../img/t/AdminFeatures.gif" />'.$this->l('Edit group').'</legend>
				<a style="float:right; margin-top:-20px; margin-right:-10px;" href="#" onclick="hideeditgroup()"><img src="../img/admin/disabled.gif"></a>
				<div style="clear:both; height:20px;"></div>
				<div id="responseaddgroup"></div>
				<div style="clear:both; height:10px;"></div>
				<label>'.$this->l('Group:').' </label>
				<div class="margin-form">';
		$languages=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."lang");	
		$activelangs=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."lang WHERE active='1'");
		$existinglanguages=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."lang WHERE active='1'");
		
		$sqlmaxlangid=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."lang ORDER BY id_lang DESC LIMIT 1");
		$maxlangid=(int)$sqlmaxlangid[0]['id_lang'];
		$firstlang=	$existinglanguages['id_lang'];
		for ($i=1;$i<=$maxlangid;$i++) {
		$idlang=$i;
		$grouplang=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."features_groups_lang WHERE id_group='$idgroup' AND id_lang='$idlang'");
 
			$output.='
					<div id="editgroupvalue_'.$i.'" style="display: '.(($i == $firstlang) ? 'block' : 'none').'; float: left;">
						<input size="33" type="text" name="groupname_'.$i.'" value="'.$grouplang['groupname'].'" /><sup> *</sup>
						
						<img src="../img/l/'.$i.'.jpg"/>
					</div>
					';
					}
					
			$output.='<div class="clear" style="height:1px"></div>';		
					
					
		
		?>		
		<script type="text/javascript">
			function show(id,maxlang) {
			var device='editgroupvalue_'+id;
			for (i=1;i<=maxlang;i=i+1) { 
			newid='editgroupvalue_' + i;
			document.getElementById(newid).style.display="none"; 
			}
			
			document.getElementById(device).style.display="block";
			
			
			}
			  </script>		
		
		 <?php
		foreach ($languages as $language)	{
		$output.='<img id="lang_'.$language['id_lang'].'" src="../img/l/'.$language['id_lang'].'.jpg" onclick="show('.$language['id_lang'].','.$maxlangid.')" style="cursor:pointer">';
			  
		}
					
					
	$output.='
					<div class="clear"></div>
				<br /><br /><br />
				<input type="hidden" name="groupid" id="groupid" value="'.$idgroup.'"/>
				<center>
					<input type="submit" value="'.$this->l('   Save   ').'" name="submitEditGroup" class="button" />
				</center>
				<div class="clear"></div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>';
	}
//////....	




///////add feature to group//////





$output.='
		
		<form id="addfeaturegroupform" style="display:none" action="'.$currentIndex.'&token='.Tools::getValue('token').'" method="post">
		
			<fieldset class="width2">
				<legend><img src="../img/t/AdminFeatures.gif" />'.$this->l('Add feature to group').'</legend>
				<a style="float:right; margin-top:-20px; margin-right:-10px;" href="#" onclick="hidefeatgroup()"><img src="../img/admin/disabled.gif"></a>
				<div style="clear:both; height:20px;"></div>
				<div id="responsefeatgroup" style="display:none; text-align:center; color:#009933;></div>
				<div style="clear:both; height:10px;"></div>';
				
		 
		    if ($id_shop > 0)
			$groups=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."features_groups WHERE id_shop='$id_shop'");	
			else
			$groups=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."features_groups");		
      
	  
	    if (!$groups) $output.="<br /><center><span style='color:red; font-size:12px; font-family:Arial'><b>".$this->l('You have to create first at least one group')."</b></span></center><br />";	
			
				$output.='<center><table cellpadding="20px;"><tr><td align="left"><span style="font-weight:bold">'.$this->l('Group:').' </span><br />';
		
		$output.='<select id="groupfeat" name="group" size="15" style="min-width:150px">';
			
		
		foreach ($groups as $group) {
		$groupid=$group['id_group'];
		$sqlname=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."features_groups_lang WHERE id_group='$groupid' AND id_lang='$lang'");
		if ($sqlname['groupname'] == '') $sqlname=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."features_groups_lang WHERE id_group='$groupid'");
		
		$groupname=$sqlname['groupname'];
		
	
		$output.='<option value="'.$groupid.'">'.$groupname.'</option>';
		}
		
			$output.='</select></td>';
		$output.='';
		$output.='<td align="left"><span style="font-weight:bold">'.$this->l('Feature:').' </span><br />';
		$output.='<select id="feature" name="feature" size="15" style="min-width:150px">';
		$features=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."feature");
		foreach ($features as $item) {
		$featureid=$item['id_feature'];
		$fshop=true;
		if ($id_shop > 0) { $featureshop=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."feature_shop WHERE id_feature='$featureid' AND id_shop='$id_shop'");
		if ($id_shop == $featureshop['id_shop']) $fshop=true;
		else $fshop=false;
		}
		
		$sqlname=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."feature_lang WHERE id_feature='$featureid' AND id_lang='$lang'");
		$featurename=$sqlname['name'];
		
		if ($fshop) $output.='<option value="'.$featureid.'">'.$featurename.'</option>';
		}
		
			$output.='</select></td></tr></table><div class="clear"></div>';
		
		
		
			$output.='<center>';
			
			if ($groups) $output.='<input type="button" value="'.$this->l('   Add   ').'" name="submitFeatureGroup" class="button" onclick="submitfeaturegroup('.$id_shop.')" />';
			else $output.='<input type="submit" value="'.$this->l('   Add   ').'" name="submitFeatureGroup" class="button" disabled="disabled" />';
			
			$output.='</center>	
			
				<div class="clear"></div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>';




/////////////////////////////////	


/////// add group to category


$output.='
		
		<form id="addgroupcategform" style="display:none" action="'.$currentIndex.'&token='.Tools::getValue('token').'" method="post">
		
			<fieldset class="width2">
				<legend><img src="../img/t/AdminFeatures.gif" />'.$this->l('Add group to category').'</legend>
				<a style="float:right; margin-top:-20px; margin-right:-10px;" href="#" onclick="hidegroupcateg()"><img src="../img/admin/disabled.gif"></a>
				<div style="clear:both; height:20px;"></div>
				<div id="responsegroupcateg" style="display:none; text-align:center; color:#009933; "></div>
				<div style="clear:both; height:10px;"></div>';
				
				 if ($id_shop > 0)
			$groups=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."features_groups WHERE id_shop='$id_shop'");	
			else
			$groups=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."features_groups");
				
					
        if (!$groups) $output.="<br /><center><span style='color:red; font-size:12px; font-family:Arial'><b>".$this->l('You have to create first at least one group')."</b></span></center><br />";	
				
		$output.='<center><table cellpadding="20px;"><tr><td align="left"><span style="font-weight:bold">'.$this->l('Group:').' </span><br />';
		
		$output.='<select id="groupcateg" name="group" size="15" style="min-width:150px">';
		
		foreach ($groups as $group) {
		$groupid=$group['id_group'];
		$sqlname=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."features_groups_lang WHERE id_group='$groupid' AND id_lang='1'");
		if ($sqlname['groupname'] == '') $sqlname=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."features_groups_lang WHERE id_group='$groupid'");
		
		$groupname=$sqlname['groupname'];
		
	
		$output.='<option value="'.$groupid.'">'.$groupname.'</option>';
		}
		
			$output.='</select></td>';
		
		
		$output.='<td align="left"><span style="font-weight:bold">'.$this->l('Category:').' </span><br />';
		$output.='<select id="category" name="category" size="15" style="min-width:150px">';
		
		$categories=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."category WHERE id_category > 1");
		
		
		foreach ($categories as $item) {
		$categoryid=$item['id_category'];
		$sqlname=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."category_lang WHERE id_category='$categoryid' AND id_lang='$lang'");
		$categoryname=$sqlname['name'];
		
		$cshop=true;
		if ($id_shop > 0) { $sqlshop=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."category_shop WHERE id_category='$categoryid' AND id_shop='$id_shop'");
		if ($id_shop == $sqlshop['id_shop']) $cshop=true;
		else $cshop=false;
		}
		
		if ($cshop) $output.='<option value="'.$categoryid.'">'.$categoryname.'</option>';
		}
		
			$output.='</select></td></tr></table></center><div class="clear"></div>';
		$output.='<br />';	
		
		
				
		
		$output.='
					<div class="clear"></div>
				
			<br />	
			<center>';
		
		  if ($groups) $output.='<input type="button" value="'.$this->l('   Add   ').'" name="submitGroupCategory" class="button" onclick="submitgroupcategory('.$id_shop.')" />';
			else  $output.='<input type="submit" value="'.$this->l('   Add   ').'" name="submitGroupCategory" class="button" disabled="disabled" />';
			
			$output.='</center>	
			
				<div class="clear"></div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
			</fieldset>
		</form>';

	


	
	
////////design////////	
	
	
	    
	  $output.="<div id='listgroups'>"; 
	 include ('displaygroupstab.php');
	$output.="</div>";
	
	$output.="<br />";
			
	

	
	/////////////////////////////end function////////////////////////	
   $output.="</fieldset>";
   ?>
   
   <script type="text/javascript">


function subscribe() {
var email=document.getElementById('email1').value; 
var website=document.getElementById('website').value; 
var module=document.getElementById('module').value;
website=website.replace(/ /g, '');

xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById("responsesub").innerHTML='';
	 document.getElementById("responsesub").innerHTML=xmlhttp.responseText;
	 document.getElementById("responsesub").style.display='block'; 
	
	 
	}
  }

xmlhttp.open("GET","../modules/sodfeaturesgrouped/ajax.php?method=subscribe&email="+email+"&website="+website+"&module="+module,true);
xmlhttp.send();


}


function sendmessage() {
   var email=document.getElementById('email2').value; 
var website=document.getElementById('website').value; 
var module=document.getElementById('module').value;
var message=document.getElementById('message').value;
website=website.replace(/ /g, '');
message=message.replace(/ /g, '%');


xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	
     document.getElementById('responsecontact').innerHTML='';
	 document.getElementById('responsecontact').innerHTML=xmlhttp.responseText;
	 document.getElementById('responsecontact').style.display='block'; 
	
	 
	}
  }

xmlhttp.open("GET","../modules/sodfeaturesgrouped/ajax.php?method=contact&email="+email+"&website="+website+"&module="+module+"&message="+message,true);
xmlhttp.send();


}
   
   </script>
   
 
   <?php
     	$output.="<div style='clear:both; height:40px;'></div>";
	
	$output.="<fieldset><legend>".$this->l('Developer Communication')."</legend>";
   
    $output.='<br /><br /><table width="100%">	
	<tr> 
	<td valign="top" style="padding-left:20px"><p style="width:280px; font-weight:bold; background:#009933; border:solid 2px #003300; padding:10px; color:#fff; font-size:14px;">'.$this->l('Subscribe your email if you want to receive this module updates and bug reports.').'</p><br /><br /><div id="responsesub" style="display:none; text-align:center; width:300px"></div><br /><br /><input type="hidden" id="website" name="website"  value="http://'.substr($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],0,strpos($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],'index.php')-1).'" /><input type="hidden" id="module" name="module" value="group_product_features" /><input type="text" name="email" id="email1" value="'.$this->l('Email').'" style="width:300px; height:30px; font-size:14px"><br /><br /><input type="button" onclick="subscribe()" value="Subscribe" class="button" style="cursor:pointer"/></td>
	<td width="50px">&nbsp;</td>
	<td valign="top" align="right" style="padding-right:20px"><p style="width:280px; font-weight:bold; background:#009933; text-align:left; border:solid 2px #003300; padding:10px; color:#fff; font-size:14px;">'.$this->l('If you have any problems with this module or with your shop feel free to contact us.').'</p><br /><br /><div id="responsecontact" style="display:none; text-align:center; width:300px"></div>
	<div style="width:300px">
	<input type="hidden" id="website" name="website" value="http://'.substr($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],0,strpos($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],'index.php')-1).'" /><input type="hidden" id="module" name="module" value="group_product_features" /><input type="text" name="email" id="email2" value="'.$this->l('Email').'" style="width:300px; height:30px; font-size:14px"><br /><br /><textarea name="message" id="message" style="width:300px; height:150px; font-size:14px">'.$this->l('Type your message').'</textarea><br /><br /><center><input type="button" onclick="sendmessage()" value="Send" class="button" style="cursor:pointer"/></center>
	</div>
	</td>
	</tr></table>';
	
	$output.="</fieldset>";
   
   echo $output;
		
		
    }
	
	
    
}
