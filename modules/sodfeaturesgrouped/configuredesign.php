<style type="text/css">
a {
text-decoration:none;
}

a:hover {
text-decoration:underline;
}

a:active {
text-decoration:none;
}
</style>


<script type="text/javascript">

function showhide(id) {

var id='colors'+id;
var status=document.getElementById(id).style.display;

if (status=='none') document.getElementById(id).style.display='block';
else document.getElementById(id).style.display='none';

}


function getcolor(key,field) {

var colorfield='colorinput'+key;
var panel='colors'+field;
var final='coloroutput'+field;
document.getElementById(final).value=document.getElementById(colorfield).value;
document.getElementById(panel).style.display='none';

}

function pushsubmit() {
document.getElementById('submitdesignvalue').value='submit';
}

</script>



<?php

  $colorgroupfield='colorgroupfield';
  $colorgrouptextfield='colorgrouptextfield';
  $colorfeaturefield='colorfeaturefield';
  $colorfeaturetextfield='colorfeaturetextfield';
  $colorfeaturevaluefield='colorfeaturevaluefield';
  $colorfeaturevaluetextfield='colorfeaturevaluetextfield';
  
   
   $output.= "<div class='clear'></div>";

   
   ////groups///////////

$output.= "<h2>".$this->l('Edit appearance for the data sheet table (product page)')."</h2>";
$output.= "<br />"; 
$output.= "<form action='".$currentIndex."&configure=sodfeaturesgrouped&token=".Tools::getValue('token')."&tab_module=StartOnlineDesign&module_name=featuresgrouped' method='post'>";

$description=Configuration::get('FEATURES_GROUPED_DESCRIPTION');



$colors=array("#000000","#000033","#000066","#000099","#0000CC","#0000FF","#330000","#330033","#330066","#330099","#3300CC", "#3300FF","#660000","#660033","#660066","#660099","#6600CC","#6600FF","#990000","#990033","#990066","#990099", "#9900CC","#9900FF","#CC0000","#CC0033","#CC0066","#CC0099","#CC00CC","#CC00FF","#FF0000","#FF0033","#FF0066", "#FF0099","#FF00CC","#FF00FF","#003300","#003333","#003366","#003399","#0033CC","#0033FF","#333300","#333333", "#333366","#333399","#3333CC","#3333FF","#663300","#663333","#663366","#663399","#6633CC","#6633FF","#993300", "#993333","#993366","#993399","#9933CC","#9933FF","#CC3300","#CC3333","#CC3366","#CC3399","#CC33CC","#CC33FF", "#FF3300","#FF3333","#FF3366","#FF3399","#FF33CC","#FF33FF","#006600","#006633","#006666","#006699","#0066CC", "#0066FF","#336600","#336633","#336666","#336699","#3366CC","#3366FF","#666600","#666633","#666666","#666699", "#6666CC","#6666FF","#996600","#996633","#996666","#996699","#9966CC","#9966FF","#CC6600","#CC6633","#CC6666", "#CC6699","#CC66CC","#CC66FF","#FF6600","#FF6633","#FF6666","#FF6699","#FF66CC","#FF66FF","#009900","#009933", "#009966","#009999","#0099CC","#0099FF","#339900","#339933","#339966","#339999","#3399CC","#3399FF","#669900", "#669933","#669966","#669999","#6699CC","#6699FF","#999900","#999933","#999966","#999999","#9999CC","#9999FF", "#CC9900","#CC9933","#CC9966","#CC9999","#CC99CC","#CC99FF","#FF9900","#FF9933","#FF9966","#FF9999","#FF99CC", "#FF99FF","#00CC00","#00CC33","#00CC66","#00CC99","#00CCCC","#00CCFF","#33CC00","#33CC33","#33CC66","#33CC99", "#33CCCC","#33CCFF","#66CC00","#66CC33","#66CC66","#66CC99","#66CCCC","#66CCFF","#99CC00","#99CC33","#99CC66", "#99CC99","#99CCCC","#99CCFF","#CCCC00","#CCCC33","#CCCC66","#CCCC99","#CCCCCC","#CCCCFF","#FFCC00","#FFCC33", "#FFCC66","#FFCC99","#FFCCCC","#FFCCFF","#00FF00","#00FF33","#00FF66","#00FF99","#00FFCC","#00FFFF","#33FF00", "#33FF33","#33FF66","#33FF99","#33FFCC","#33FFFF","#66FF00","#66FF33","#66FF66","#66FF99","#66FFCC","#66FFFF", "#99FF00","#99FF33","#99FF66","#99FF99","#99FFCC","#99FFFF","#CCFF00","#CCFF33","#CCFF66","#CCFF99","#CCFFCC", "#CCFFFF","#FFFF00","#FFFF33","#FFFF66","#FFFF99","#FFFFCC","#FFFFFF");



foreach ($colors as $key=>$color) {
$output.= '<input type="hidden" id="colorinput'.$key.'" name="colorinput'.$key.'" value="'.$color.'" />';
}

//Group Cell Color
$groupcellcolor=Configuration::get('FEATURES_GROUPED_GCC');


$output.='<input type="hidden" name="submitdesignvalue" id="submitdesignvalue" value="" /><span style="color:#333333; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$this->l('Group Cell Color').'</span>&nbsp;&nbsp;<input type="text" name="coloroutput1" id="coloroutput1" size="20" value="'.$groupcellcolor.'"> <a  onclick="showhide(1)" style="color:#0066CC; cursor:pointer; font-weight:bold">'.$this->l('Select').'</a> <br />';

$output.= '<div id="colors1" style="display:none; width:220px">';
foreach ($colors as $key=>$color) {

$output.= '<a  style="width:10px; height:10px; cursor:pointer; float:left; border:1px #CCCCCC solid; background:'.$color.'; color:'.$color.';" onClick="getcolor('.$key.',1)"></a>';
}
$output.= '</div>';
$output.= "<div class='clear' style='height:20px;'></div>";



//Group Font Color
$groupfontcolor=Configuration::get('FEATURES_GROUPED_GFC');

$output.='<span style="color:#333333; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$this->l('Group Text Color').'</span>&nbsp;&nbsp;<input type="text" name="coloroutput2" id="coloroutput2" size="20" value="'.$groupfontcolor.'" > <a  onclick="showhide(2)" style="color:#0066CC; cursor:pointer; font-weight:bold">'.$this->l('Select').'</a> <br />';


$output.= '<div id="colors2" style="display:none; width:220px">';
foreach ($colors as $key=>$color) {

$output.= '<a  style="width:10px; height:10px; cursor:pointer; float:left; border:1px #CCCCCC solid; background:'.$color.'; color:'.$color.';" onClick="getcolor('.$key.',2)"></a>';
}


$output.= '</div>';

$output.= "<div class='clear' style='height:20px;'></div>";

//Group Font Size
$groupfontsize=Configuration::get('FEATURES_GROUPED_GFS');
$output.= '<span style="color:#333333; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$this->l('Group Font Size').'</span>&nbsp;&nbsp;<select name="groupfontsize"> 
<option value="'.$groupfontsize.'">'.$groupfontsize.'</option>
<option value="10px">10px</option>
<option value="12px">12px</option>
<option value="14px">14px</option>
<option value="16px">16px</option>
<option value="18px">18px</option>
<option value="20px">20px</option>
<option value="22px">22px</option>
<option value="24px">24px</option>
</select>';

$output.= "<div class='clear' style='height:20px;'></div>";



//Feature Cell Color
$featurecellcolor=Configuration::get('FEATURES_GROUPED_FCC');

$output.='<span style="color:#333333; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$this->l('Feature Cell Color').'</span>&nbsp;&nbsp;<input type="text" name="coloroutput3" id="coloroutput3" size="20" value="'.$featurecellcolor.'"> <a  onclick="showhide(3)" style="color:#0066CC; cursor:pointer; font-weight:bold">'.$this->l('Select').'</a> <br />';

$output.= '<div id="colors3" style="display:none; width:220px">';
foreach ($colors as $key=>$color) {

$output.= '<a  style="width:10px; height:10px; cursor:pointer; float:left; border:1px #CCCCCC solid; background:'.$color.'; color:'.$color.';" onClick="getcolor('.$key.',3)"></a>';
}
$output.= '</div>';

$output.= "<div class='clear' style='height:20px;'></div>";

//Feature Font
$featurefontcolor=Configuration::get('FEATURES_GROUPED_FFC');

$output.='<span style="color:#333333; font-size:12px; cursor:pointer; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$this->l('Feature Text Color').'</span>&nbsp;&nbsp;<input type="text" name="coloroutput4" id="coloroutput4" size="20" value="'.$featurefontcolor.'"> <a  onclick="showhide(4)" style="color:#0066CC; cursor:pointer; font-weight:bold;">'.$this->l('Select').'</a> <br />';

$output.= '<div id="colors4" style="display:none; width:220px">';
foreach ($colors as $key=>$color) {

$output.= '<a  style="width:10px; height:10px; cursor:pointer; float:left; border:1px #CCCCCC solid; background:'.$color.'; color:'.$color.';" onClick="getcolor('.$key.',4)"></a>';
}

$output.= '</div>';

$output.= "<div class='clear' style='height:20px;'></div>";


//Feature Font Size
$featurefontsize=Configuration::get('FEATURES_GROUPED_FFS');
$output.= '<span style="color:#333333; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$this->l('Feature Font Size').'</span>&nbsp;&nbsp;<select name="featurefontsize"> 
<option value="'.$featurefontsize.'">'.$featurefontsize.'</option>
<option value="10px">10px</option>
<option value="12px">12px</option>
<option value="14px">14px</option>
<option value="16px">16px</option>
<option value="18px">18px</option>
<option value="20px">20px</option>
<option value="22px">22px</option>
<option value="24px">24px</option>
</select>';

$output.= '<div class="clear" style="height:20px;"></div>';


//Feature Value Cell Color
$featurevaluecellcolor=Configuration::get('FEATURES_GROUPED_FVCC');
$output.='<span style="color:#333333; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$this->l('Feature Value Cell Color').'</span>&nbsp;&nbsp;<input type="text" name="coloroutput5" id="coloroutput5" size="20" value="'.$featurevaluecellcolor.'"> <a  onclick="showhide(5)" style="color:#0066CC; cursor:pointer; font-weight:bold">'.$this->l('Select').'</a> <br />';

$output.= '<div id="colors5" style="display:none; width:220px">';
foreach ($colors as $key=>$color) {
 $output.= '<a  style="width:10px; height:10px; cursor:pointer; float:left; border:1px #CCCCCC solid; background:'.$color.'; color:'.$color.';" onClick="getcolor('.$key.',5)"></a>';
}

$output.= '</div>';

$output.= '<div class="clear" style="height:20px;"></div>';



//Feature Value Font Color
$featurevaluefontcolor=Configuration::get('FEATURES_GROUPED_FVFC');

$output.='<span style="color:#333333; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$this->l('Feature Value Text Color').'</span>&nbsp;&nbsp;<input type="text" name="coloroutput6" id="coloroutput6" size="20" value="'.$featurevaluefontcolor.'"> <a  onclick="showhide(6)" style="color:#0066CC; cursor:pointer; font-weight:bold">'.$this->l('Select').'</a> <br />';




$output.= '<div class="clear" style="height:20px;"></div>';
$output.= '<div id="colors6" style="display:none; width:220px">';
foreach ($colors as $key=>$color) {

$output.= '<a  style="width:10px; height:10px; cursor:pointer; float:left; border:1px #CCCCCC solid; background:'.$color.'; color:'.$color.';" onClick="getcolor('.$key.',6)"></a>';
}

$output.= '</div>';

$output.= '<div class="clear" style="height:20px;"></div>';

//Feature Font Size
$featurevaluefontsize=Configuration::get('FEATURES_GROUPED_FVFS');
$output.= '<span style="color:#333333; font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$this->l('Feature Value Font Size').'</span>&nbsp;&nbsp;<select name="featurevaluefontsize"> 
<option value="'.$featurevaluefontsize.'">'.$featurevaluefontsize.'</option>
<option value="10px">10px</option>
<option value="12px">12px</option>
<option value="14px">14px</option>
<option value="16px">16px</option>
<option value="18px">18px</option>
<option value="20px">20px</option>
<option value="22px">22px</option>
<option value="24px">24px</option>
</select>';

$output.= '<div class="clear" style="height:20px;"></div>';

$output.= '<center><input type="submit" name="submitdesign" id="submitdesign" class="button" value="'.$this->l('Save').'" onclick="pushsubmit()"/></center>';
$output.= '</form>'; 


 $output.= '<div class="clear"></div>';   
 
?>
