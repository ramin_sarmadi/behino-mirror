<?php
/**
 * @since 1.5.0
 */
class LofreviewsViewModuleFrontController extends ModuleFrontController
{
	public $display_column_left = false;

	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();

		// Declare smarty function to render pagination link
		smartyRegisterFunction($this->context->smarty, 'function', 'summarypaginationlink', array('LofreviewsViewModuleFrontController', 'getSummaryPaginationLink'));
	}

	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess()
	{
		if (Tools::getValue('process') == 'transformpoints')
			$this->processTransformPoints();
	}

	/**
	 * Transform loyalty point to a voucher
	 */
	public function processTransformPoints()
	{
		
	}

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
		$this->context->controller->addJqueryPlugin(array('fancybox'));

		if (Tools::getValue('process') == 'list')
			$this->assignListExecution();
		elseif (Tools::getValue('process') == 'detail')
			$this->assignDetailExecution();
	}

	/**
	 * Assign summary template
	 */
	public function assignListExecution() {
		global $cookie;
		$link = new Link();
		$site_url = Tools::htmlentitiesutf8(Tools::getShopProtocol().$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		$module_name = 'lofreviews';
		$page = 'modules/'.$module_name.'/view.php';
		
		$id_customer = $this->context->customer->id;
		$customer = new Customer($id_customer);
		$customer_name = $customer->firstname.' '.$customer->lastname;
		/** getList Review **/
		$lofreviews = LofReview::getReviewsByCustomer(array('id_customer'=>$id_customer));
		//echo "<pre>"; print_r($lofreviews); die;   
		$listreviews = $this->module->generateListReviews($lofreviews['lofreviews'],$lofreviews['count']);

		$showOrdering = $this->module->getParamValue("showordering");
		$defaultOrdering = $this->module->getParamValue("defaultordering");
		
		$this->context->smarty->assign(array(
			'id_customer' => $id_customer,
			'customer_name' => $customer_name,
			'showOrdering' => $showOrdering,
			'defaultOrdering' => $defaultOrdering,
			'listreviews' => $listreviews    
		));
		
		$this->setTemplate('myreview.tpl');
	}
	
	public function assignDetailExecution(){
		global $cookie;
		$link = new Link();
		$site_url = Tools::htmlentitiesutf8(Tools::getShopProtocol().$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		$module_name = 'lofreviews';
		$page = 'modules/'.$module_name.'/view.php';
		$theme = $this->module->getParamValue('module_theme',"default");
		
		$id_review = Tools::getValue("review_id");
		$reviewDetails = LofReview::getReviewDetails($cookie->id_lang, $id_review);
		
		$showHelpfullness = $this->module->getParamValue("showhelpfulness",1);
		$guestratehelpful = $this->module->getParamValue("guestratehelpful",0);
		$guestreport    = $this->module->getParamValue("guestreport");
        $isLogin = $this->context->customer->id;
		$review_link = "";
		if(!empty($reviewDetails)){
		  $review_link = $link->getModuleLink('lofreviews', 'view', array('process' => 'detail')).'&review_id='.$id_review;
		  $review_link = urlencode($review_link);
		  $reviewDetails["totalLike"] = (int)$reviewDetails["like_count"]+(int)$reviewDetails["unlike_count"];
		  $reviewDetails["pros"] = LofReview::getProsConsByIdReview($cookie->id_lang, $reviewDetails["id_lofreview_review"], 1);
		  $reviewDetails["cons"] = LofReview::getProsConsByIdReview($cookie->id_lang, $reviewDetails["id_lofreview_review"], 0);
		}

		$sumReview = LofReview::sumReview($reviewDetails['id_product']);
		$this->context->smarty->assign(array(
			'site_url' 		=> $site_url,
			'id_lang' 		=> $cookie->id_lang,
			"review_link" => $review_link,
			"guestreport" => $guestreport,
			"isLogin" => $isLogin,
			"guestratehelpful" => $guestratehelpful,
			"showHelpfullness" => $showHelpfullness,
			'reviewDetails' => $reviewDetails,
			'sumReview' => $sumReview
		));
		
		$this->setTemplate('reviewdetail.tpl');
	}
	
	public function getTemplatePath(){
		$theme = $this->module->getParamValue('module_theme',"default");
		return _PS_MODULE_DIR_.$this->module->name.'/tmpl/'.$theme.'/';
	}
}
