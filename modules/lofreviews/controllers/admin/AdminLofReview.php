<?php


include_once( dirname(__FILE__)."/../../classes/LofReview.php");
class AdminLofReviewController extends AdminController
{
    protected $_stateArray = array();
    public function __construct(){

		global $cookie;
        $this->table = 'lofreview_review';
	 	$this->className = 'LofReview';
        $this->lang = false;
	 	$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->bulk_actions = array(
			'Ac' => array('text' => $this->l('Approved selected')),
			'PenAc' => array('text' => $this->l('Pendding selectied')),
			'UnAc' => array('text' => $this->l('Un-Approved selection')),
			'delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?'))
			
			);
		
		$this->_join = '
				LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.`id_product` = a.`id_product`)
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.`id_product` = a.`id_product`)';
		$this->_where = ' AND pl.id_lang = '.$cookie->id_lang;
		$this->_select = 'pl.name AS product_name, 
				case a.`active`
				when 0 then \''.$this->l('Pending').'\'
				when 1 then \''.$this->l('Approved').'\'
				when 2 then \''.$this->l('Not Approved').'\'
				end as active';
        $this->_group = ' GROUP BY a.`id_lofreview_review` ';
        $this->_stateArray=array(0=>$this->l('Pending'),
                          1=>$this->l('Approved'),
                          2=>$this->l('Not Approved'));
        $this->fields_list = array(
		'id_lofreview_review' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 15),
		'product_name' => array('title' => $this->l('Product Name'),'filter_key' => 'product_name','tmpTableFilter' => true, 'width' => 50),
		'title' => array('title' => $this->l('Title'), 'align' => 'center', 'width' => 100),
        'comment' => array('title' => $this->l('Comment'), 'align' => 'center', 'width' => 200),
		'customer_name' => array('title' => $this->l('Nick Name'),'filter_key' => 'customer_name', 'align' => 'center', 'width' => 55,'orderby' => false),
		'like_count' => array('title' => $this->l('Like'), 'align' => 'center', 'width' => 25,'orderby' => false),
		'unlike_count' => array('title' => $this->l('UnLike'), 'align' => 'center', 'width' => 25,'orderby' => false),
        'active' => array('title' => $this->l('Active'), 'width' => 40,  'filter_key' => 'a!active', 'align' => 'center', 'type' => 'select','list' => $this->_stateArray,'filter_type' => 'int', 'orderby' => false , 'filter_key'=>'active'),
		'date_add' => array('title' => $this->l('Date add'), 'width' => 30, 'type' => 'date', 'align' => 'right'));
    
        parent::__construct();
    }  

     public function renderList(){
        global $currentIndex, $cookie;
        $this->initToolbar();
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_review_shop` AS sa ON (a.`id_lofreview_review` = sa.`id_lofreview_review` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
		else
		    $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_review_shop` AS sa ON (a.`id_lofreview_review` = sa.`id_lofreview_review` AND sa.id_shop = a.id_shop_default) ';


		// we add restriction for shop
		if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$this->_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        return parent::renderList();

    }

	/*
	public function getList($id_lang, $orderBy = NULL, $orderWay = NULL, $start = 0, $limit = NULL)
	{
        parent::getList($id_lang, $orderBy, $orderWay, $start, $limit);
        if(!empty($this->_list)){
            foreach($this->_list as $key=>$item){
               if(isset($item["active"])){
                  $item["active"] = $this->_stateArray[$item["active"]];
                  $this->_list[$key] = $item;
               }
            }
        }
    }
	*/
	public function renderForm($isMainTab = true)
	{
		global $currentIndex, $cookie;
		//parent::renderForm();
		
		if (!($obj = $this->loadObject(true)))
			return;
		$checked1 = ($this->getFieldValue($obj, 'active')=='0')?'selected="selected"':'';
        $checked2 = ($this->getFieldValue($obj, 'active')=='1')?'selected="selected"':'';
        $checked3 = ($this->getFieldValue($obj, 'active')=='2')?'selected="selected"':'';
        /*Get product name by id*/
        $sql ='
            SELECT p.`id_product`, pl.`name`
    		FROM `'._DB_PREFIX_.'product` p
    		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product`)
    		WHERE pl.`id_lang` = '.(int)($cookie->id_lang).'
            AND p.`id_product` = '.(int)$this->getFieldValue($obj,'id_product').'
        ';
        $product = Db::getInstance()->ExecuteS($sql);
        $product_name = isset($product[0])?$product[0]['name']:'';
        $link =  new Link();
        $product_link = $link->getProductLink((int)$this->getFieldValue($obj,'id_product'))."#idTabLofReview";
		$html = '';
		$this->initToolbarTitle();
        $this->initToolbar();
        $this->context->smarty->assign('title', $this->toolbar_title);
        $html = $this->context->smarty->fetch('toolbar.tpl');
		
		$html .= '
		<form id="lofreview_review_form" class="defaultForm AdminLofReview" action="'.$currentIndex.'&submitAdd'.$this->table.'=1&token='.$this->token.'" method="post">
		'.($obj->id ? '<input type="hidden" name="id_'.$this->table.'" value="'.$obj->id.'" />' : '').'
			<fieldset><legend><img src="../img/admin/quick.gif" />'.$this->l('Lof Review').'</legend>
            	<label>'.$this->l('Product:').' </label>
                <div class="margin-form"><h3><a href="'.$product_link.'" target="_BLANK">'.$product_name.'</a></h3></div>
                <label>'.$this->l('Date add:').' </label>
                <div class="margin-form"><font style="font-size:1em">'.$this->getFieldValue($obj,'date_add').'</font></div>
                <label>'.$this->l('Customer:').' </label>
                <div class="margin-form"><font style="font-size:1em">'.$this->getFieldValue($obj,'customer_name').'</font></div>
                <label>'.$this->l('User\'s Ip:').' </label>
                <div class="margin-form"><font style="font-size:1em">'.$this->getFieldValue($obj,'user_ip').'</font></div>
                <label>'.$this->l('Review title:').' </label>
				<div class="margin-form">
					<input type="text" size="30" name="title" value="'.($this->getFieldValue($obj, 'title')).'" /> <sup>*</sup>
					<p class="clear">'.$this->l('review title').'</p>
				</div>
                <label>'.$this->l('Review content:').' </label>
                <div class="margin-form">
					<textarea cols="45" rows="10" name="comment">'.$this->getFieldValue($obj, 'comment').'</textarea> <sup>*</sup>
				</div>
                <label>'.$this->l('Status:').' </label>
                <div class="margin-form">
                    <select name="active">
                      <option value="0" '.$checked1.'> '.$this->l("Pendding").'</option>
                      <option value="1" '.$checked2.'> '.$this->l("Approved").'</option>
                      <option value="2" '.$checked3.'> '.$this->l("Un-Approved").'</option>
                    </select>
				</div>
                ';
		$html .= '<div class="clear">&nbsp;</div>
				<div class="small"><sup>*</sup> '.$this->l('Required field').'</div>
				<input id="lofreview_review_form_submit_btn" class="button" type="submit" name="submitAddlofreview_review" value="Save" style="display: none;">
			</fieldset>
			</form>';	
		return $html;
	}
	
	public function initToolbar() {
        $token = Tools::getAdminTokenLite('AdminLofReview');
		
        switch ($this->display) {
            case 'add':
            case 'edit':
                $this->toolbar_btn['save'] = array(
                    'href' => '#',
                    'desc' => $this->l('Save')
                );
                $back = Tools::safeOutput(Tools::getValue('back', ''));
                if (empty($back))
                    $back = self::$currentIndex.'&token='.$this->token;
                $this->toolbar_btn['back'] = array(
                    'href' => $back,
                    'desc' => $this->l('Back to list')
                );
				 break;
        }

        $this->context->smarty->assign('toolbar_scroll', 1);
        $this->context->smarty->assign('show_toolbar', 1);
        $this->context->smarty->assign('toolbar_btn', $this->toolbar_btn);
        $this->context->smarty->assign('title', $this->toolbar_title);
    }

	protected function copyFromPost(&$object, $table)
	{		
		parent::copyFromPost($object, $table);
	}
	
	public function postProcess()
	{
		global $currentIndex, $cookie;
        $ids_review = Tools::getValue("lofreview_reviewBox");
		if (Tools::getValue('submitBulkAc'.$this->table)){
            $reviewids = Tools::getValue($this->table."Box");
			LofReview::activeSelectionReview($reviewids);
            $postSuccess = 'Review posted successfully.';
            if(!empty($ids_review)){
                $reviews = LofReview::listCustomerByIdReview(implode(",", $ids_review));
                foreach($reviews as $item){
                    $this->sendMail($item['id_customer'], $item['id_product'], $postSuccess);
                }          
            }
            Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.Tools::getValue('token'));

        }elseif(Tools::getValue('submitBulkUnAc'.$this->table)){
            $reviewids = Tools::getValue($this->table."Box");
			LofReview::activeSelectionReview($reviewids,2);
            $postSuccess = 'Sorry! Your review has been looked.';
            if(!empty($ids_review)){
                $reviews = LofReview::listCustomerByIdReview(implode(",", $ids_review));
                foreach($reviews as $item){
                    $this->sendMail($item['id_customer'], $item['id_product'], $postSuccess);
                }          
            }
            Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.Tools::getValue('token'));
        }
        elseif(Tools::getValue('submitBulkPenAc'.$this->table)){
            $reviewids = Tools::getValue($this->table."Box");
			LofReview::activeSelectionReview($reviewids,0);
            $postSuccess = 'Review posted successfully. Please! watting admin valid.';
            if(!empty($ids_review)){
                $reviews = LofReview::listCustomerByIdReview(implode(",", $ids_review));
                foreach($reviews as $item){
                    $this->sendMail($item['id_customer'], $item['id_product'], $postSuccess);
                }          
            }
            Tools::redirectAdmin(self::$currentIndex.'&conf=4&token='.Tools::getValue('token'));
		}else{
			return	parent::postProcess();
		}
	}

    public function sendMail($customer_id, $id_product, $postSuccess){
        global $cookie;
        //Send e-mail
		$link = new Link;
        $configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_MAIL_SERVER', 'PS_MAIL_USER', 'PS_MAIL_PASSWD', 'PS_SHOP_NAME'));
        $product = new Product($id_product, $cookie->id_lang);
        $product_link = ($link->getProductLink($id_product));
        if(!empty($customer_id)){
          $customer = new Customer($customer_id);
          $to = $customer->email;
          $templateVars = array(
              '{customer_name}' => $customer->firstname .' '.$customer->lastname,
              '{product_name}' => $product->name[$cookie->id_lang],
              '{product_link}' => $product_link,
              '{postSuccess}' => $postSuccess
          );
          if (!Mail::Send((int)$cookie->id_lang, 'review', Mail::l('Review posted send to you '),
                          $templateVars, $to, NULL, ($configuration['PS_SHOP_EMAIL'] ? $configuration['PS_SHOP_EMAIL'] : NULL),
                          ($configuration['PS_SHOP_NAME'] ? $configuration['PS_SHOP_NAME'] : NULL), NULL, NULL,
                          _PS_MODULE_DIR_.'lofreviews/mails/', false)){
              $error = $this->l('An error occurred during the process.');
          }
        }
    }
}
