<?php

include_once( dirname(__FILE__)."/../../classes/LofCriterion.php");
include_once( dirname(__FILE__)."/../../classes/LofProductCriterion.php");

class AdminLofCriterionController extends AdminController
{
	public $_defaultFormLanguage;
    public function __construct(){
		
		global $cookie;
        $this->delete = true;
        $this->identifiersDnd = array('id_lofreview_criterion' => 'id_lofreview_criterion_to_move');
        $this->position_identifier = 'id_lofreview_criterion_to_move';

        $this->table = 'lofreview_criterion';
        $this->className = 'LofCriterion';
        $this->moduleName = 'lofreviews';
        $this->lang = true;
        $this->context = Context::getContext();
        Shop::addTableAssociation('lofreview_criterion', array('type' => 'shop'));
		$this->addRowAction('view');
		$this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
        $this->_select = 'CASE a.type WHEN 0 THEN \''.pSQL($this->l('Cons')).'\' ELSE \''.pSQL($this->l('Pros')).'\' END AS \'type\'';
        $this->_stateArray=array(0=>$this->l('No'),
                          1=>$this->l('Yes'));
        $pros = array(0=>$this->l('Cons'),
                          1=>$this->l('Pros'));
        parent::__construct();
        $context = Context::getContext();
        $id_shop = $context->shop->id;

        $this->_where = 'AND sa.`id_shop` = '.(int)$id_shop;

        $this->fields_list = array(
			'id_lofreview_criterion' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'title' => array('title' => $this->l('Title'), 'width' => 200),
			'active' => array('title' => $this->l('Active'), 'align' => 'center','active' => 'status', 'width' => 25,'orderby' => false,'type' => 'bool', 'filter_key' => 'a!active'),
            'position' => array('title' => $this->l('Position'), 'width' => 40, 'filter_key' => 'a!position', 'align' => 'center', 'position' => 'position'),
			'type' => array('title' => $this->l('Type'), 'align' => 'center', 'width' => 60,'orderby' => false, 'type' => 'select','list' => $pros,'orderby' => false, 'filter_key' => 'type', 'search'=>false),
			'is_custom' => array('title' => $this->l('Is Custom?'), 'align' => 'center', 'width' => 45,'type' => 'select','list' => $this->_stateArray,'orderby' => false, 'filter_key' => 'is_custom', 'active' => 'is_custom', 'search'=>false),
			'date_add' => array('title' => $this->l('Date add'), 'width' => 30, 'type' => 'date', 'align' => 'right')
		);

        $this->getlanguages();
		$this->_defaultFormLanguage = (int)Configuration::get('PS_LANG_DEFAULT');
    }

    public function getList($id_lang, $order_by = 'position', $order_way = 'ASC', $start = 0, $limit = null, $id_lang_shop = false) {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit);
    }

	public function renderForm($isMainTab = true)
	{
        /*Add form code at here*/
        global $currentIndex, $cookie;

        if (!$this->loadObject(true))
			return;

		if (Validate::isLoadedObject($this->object))
			$this->display = 'edit';
		else
			$this->display = 'add';
		$this->initToolbar();
        $list_type = array(
                        array("value"=>1,
                            "name"=>$this->l('Pros')),
                        array("value"=>0,
                            "name"=>$this->l('Cons'))
                    );
		$this->fields_form = array(
			'tinymce' => false,
			'legend' => array(
				'title' => $this->l('Criterion'),
				'image' => '../img/admin/quick.gif'
			),
			'input' => array(
				// custom template

				array(
					'type' => 'text',
					'label' => $this->l('Title:'),
					'name' => 'title',
					'lang' => true,
					'required' => true,
					'class' => '',
					'hint' => $this->l('Invalid characters:').' <>;=#{}',
                    'desc' => $this->l('criterion value, e.g., Technical Support'),
					'size' => 50
				),
                array(
					'type' => 'select',
					'label' => $this->l('Type'),
					'name' => 'type',
					'options' => array(
						'query' => $list_type,
                        'id' => 'value',
                        'name' => 'name'
					),
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Active:'),
					'name' => 'active',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						    )
				    	),
				),
                array(
					'type' => 'radio',
					'label' => $this->l('Is Custom:'),
					'name' => 'is_custom',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'is_custom_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'is_custom_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						    )
				    	),
				)
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);

		if (Shop::isFeatureActive())
		{
			$this->fields_form['input'][] = array(
				'type' => 'shop',
				'label' => $this->l('Shop association:'),
				'name' => 'checkBoxShopAsso',
			);
		}

		$this->tpl_form_vars = array(
			'active' => $this->object->active,
			'PS_ALLOW_ACCENTED_CHARS_URL', (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL')
		);
        return parent::renderForm();
	}

    public function initToolbarTitle()
    {
        parent::initToolbarTitle();
        if ($obj = $this->loadObject(true)){
            if ((bool)$obj->id && $this->display != 'list' && isset($this->toolbar_title[2]))
                $this->toolbar_title[2] = $this->toolbar_title[2].' ('.$obj->title[$this->context->shop->id].')';
        }
    }

    public function initToolbar() {
        $token = Tools::getAdminTokenLite('AdminLofCriterion');

        switch ($this->display) {
            case 'add':
            case 'edit':
                // Default save button - action dynamically handled in javascript
                $this->toolbar_btn['save'] = array(
                    'href' => '#',
                    'desc' => $this->l('Save')
                );
                $back = Tools::safeOutput(Tools::getValue('back', ''));
                if (empty($back))
                    $back = self::$currentIndex.'&token='.$this->token;
                $this->toolbar_btn['back'] = array(
                    'href' => $back,
                    'desc' => $this->l('Back to list')
                );
                break;
            case 'view':
                $this->toolbar_btn['edit'] = array(
                    'href' => self::$currentIndex.'&amp;update'.$this->table.'&amp;id_lofreview_criterion='.(int)Tools::getValue('id_lofreview_criterion').'&amp;token='.$token,
                    'desc' => $this->l('Edit')
                );
                $back = Tools::safeOutput(Tools::getValue('back', ''));
                if (empty($back))
                    $back = self::$currentIndex.'&token='.$this->token;
                $this->toolbar_btn['back'] = array(
                    'href' => $back,
                    'desc' => $this->l('Back to list')
                );
                break;
            default: // list
                $link = 'index.php?controller=AdminLofCriterion&amp;add' . $this->table . '&amp;token=' . $this->token;
                $this->toolbar_btn['save'] = array(
                    'href' => $link,
                    'desc' => $this->l('Add new')
                );
        }
        $this->context->smarty->assign('toolbar_scroll', 1);
        $this->context->smarty->assign('show_toolbar', 1);
        $this->context->smarty->assign('toolbar_btn', $this->toolbar_btn);
        $this->context->smarty->assign('title', $this->toolbar_title);
    }

	protected function copyFromPost(&$object, $table) {
		parent::copyFromPost($object, $table);
	}
	
	public function postProcess()
	{
		global $currentIndex, $cookie;
        $id_country = Tools::getValue('id_lofreview_criterion');
        $getvalname = 'submitAddAttribute'.$this->table;
        if(isset($_GET[ $getvalname ]) && $_GET[ $getvalname ] == 1){
            $attributes = isset($_POST['attribute_id'])?$_POST['attribute_id']:array();
            $categories = isset($_POST['categrory_ids'])?$_POST['categrory_ids']:array();
        
            if(empty($categories)){
                echo '
    			<div class="error">
                    <img src="../img/admin/error2.png">
                        1 errors
                    <br>
                    <ol>
                        <li>
                            '.$this->l('Please select category!').'
                        </li>
                    </ol>
                </div>';
            }else{
                LofProductCriterion::addAttributesToCategories($attributes, $categories);
                echo '
    			<div class="conf">
    				<img src="../img/admin/ok2.png" alt="" />
                    '.$this->l('Add successful').'
    			</div>';
            }

        }
        if(Tools::isSubmit('submitSelectionAttribute')){
            $ids_product = Tools::getValue('lof_pro');
            $id_attribute = Tools::getValue('id_lofreview_criterion');
            $ids_pro = Tools::getValue('ids_pro');
            $ids_uncheck = array_diff($ids_pro, $ids_product);
            /** 
            echo "check: <pre>"; print_r($ids_product); echo "<br/>";
            echo "ALL: <pre>"; print_r($ids_pro); echo "<br/>";
            echo "UNCHECK: <pre>"; print_r($ids_uncheck); die;
            **/
            $result = LofProductCriterion::addRecordProductAttribute($ids_product, $ids_uncheck, $id_attribute);
            if(!$result){
                $this->errors[] = $this->l('Add products error');
            }else{
                Tools::redirectAdmin(self::$currentIndex.'&id_lofreview_criterion='.Tools::getValue('id_lofreview_criterion').'&viewlofreview_criterion&conf=4&token='.Tools::getValue('token'));
            }
	    }elseif(Tools::isSubmit('submitAddCategory')){
            $ids_category = Tools::getValue('categoryBox');
            $id_lofreview_criterion = Tools::getValue('id_lofreview_criterion');


            Configuration::updateValue('lofreviews_crit_cates_'.(int)$id_lofreview_criterion, (implode(',',$ids_category)));
            Tools::redirectAdmin(self::$currentIndex.'&id_lofreview_criterion='.Tools::getValue('id_lofreview_criterion').'&viewlofreview_criterion&conf=4&token='.Tools::getValue('token'));

        }
        
        if (Tools::isSubmit('lofsubmitFilter'.$this->table)){
            $page = Tools::getValue('lofsubmitFilter'.$this->table)? Tools::getValue('lofsubmitFilter'.$this->table) : '1';
            $selectedPagination = Tools::getValue('pagination') ? Tools::getValue('pagination') : NULL;
            $id_product = Tools::getValue('id_product') ? Tools::getValue('id_product') : NULL;
            $product_name = Tools::getValue('product_name') ? Tools::getValue('product_name') : NULL;
        }
        if (Tools::isSubmit('lofsubmitReset'.$this->table)){
            unset($_POST);
        }

        if (Tools::isSubmit('position') && Tools::getValue('id_lofreview_criterion_to_move')){
            if ($this->tabAccess['edit'] !== '1')
                $this->errors[] = Tools::displayError('You do not have permission to edit here.');
            elseif (!Validate::isLoadedObject($object = new LofCriterion( (int)Tools::getValue('id_lofreview_criterion_to_move') )))
                $this->errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
            if (!$object->updatePosition((int)(Tools::getValue('way')), (int)(Tools::getValue('position'))))
                $this->errors[] = Tools::displayError('Failed to update the position.');
            else{

                Tools::redirectAdmin(self::$currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5&token='.Tools::getValue('token'));
            }
        }elseif(Tools::getValue('action') == 'updatePositions'){
            $id_lofreview_criterion_to_move = (int)(Tools::getValue('id'));
            $way = (int)(Tools::getValue('way'));
            $positions = Tools::getValue('lofreview_criterion');
            if (is_array($positions))
                foreach ($positions as $key => $value) {
                    $pos = explode('_', $value);
                    if ((isset($pos[1])) && ($pos[2] == $id_lofreview_criterion_to_move)) {
                        $position = $key + 1;
                        break;
                    }
                }
            $category = new LofCriterion($id_lofreview_criterion_to_move);
            if (Validate::isLoadedObject($category))
            {
                if (isset($position) && $category->updatePosition($way, $position)){
                    die(true);
                }
                else
                    die('{"hasError" : true, errors : "Can not update categories position"}');
            }
            else
                die('{"hasError" : true, "errors" : "This category can not be loaded"}');
        }elseif(Tools::isSubmit('is_customlofreview_criterion')){
            if ($this->tabAccess['edit'] !== '1')
                $this->errors[] = Tools::displayError('You do not have permission to edit here.');
            elseif (!Validate::isLoadedObject($object = new LofCriterion( (int)Tools::getValue('id_lofreview_criterion') )))
                $this->errors[] = Tools::displayError('An error occurred while updating status for object.').' <b>'.$this->table.'</b> '.Tools::displayError('(cannot load object)');
            $object->is_custom = ($object->is_custom ? 0 : 1);
            if (!$object->update())
                $this->errors[] = Tools::displayError('Failed to update the is_custom.');
            else{
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5&token='.Tools::getValue('token'));
            }
        }

	    return	parent::postProcess(true);
	}

    public function renderList(){
        global $currentIndex, $cookie;
        $this->initToolbar();
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_criterion_shop` AS sa ON (a.`id_lofreview_criterion` = sa.`id_lofreview_criterion` AND sa.id_shop = '.(int)$this->context->shop->id.') ';
		else
		    $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_criterion_shop` AS sa ON (a.`id_lofreview_criterion` = sa.`id_lofreview_criterion` AND sa.id_shop = a.id_shop_default) ';


		// we add restriction for shop
		if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$this->_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        return parent::renderList();

    }
    public function renderView()
    {	
        $token = $this->token;
        global $currentIndex, $cookie;
        if (!($lofAttribute = $this->loadObject()))
	       return;
		$this->initToolbarTitle();
        $this->initToolbar();
        $this->context->smarty->assign('title', $this->toolbar_title);
        $html = $this->context->smarty->fetch('toolbar.tpl');

        $attributeProducts = $lofAttribute->getProductsList($lofAttribute->id, $cookie->id_lang);
        
        $ids_product = array();
        $i = 0;
        foreach($attributeProducts as $item){
            $ids_product[$i] = $item['id_product'].'';
            $i++;
        }
        
        
        $html .= '<h3>'.$this->l('Total products assign Criterion:').' '.sizeof($attributeProducts).'</h3>';
        $html .= '<hr />';
        $html .= '<form action="'.$currentIndex.'&id_lofreview_criterion='.$lofAttribute->id.'&viewlofreview_criterion&token='.$this->token.'" method="post">';
        
        $_pagination = array(5,20,50,100,300,500,1000);
        $sizeproducts = $lofAttribute->getListProducts($ids_product, $lofAttribute->id, $cookie->id_lang,0,NULL,NULL,NULL,NULL,NULL);
        
        //echo "<pre>"; print_r($sizeproducts); die;
        
        $_listTotal = sizeof($sizeproducts);
        
        $totalPages = ceil($_listTotal / Tools::getValue('pagination', $_pagination[1]));
		if (!$totalPages) $totalPages = 1;
        
        /* Determine current page number */
        $page = (int)(Tools::getValue('lofsubmitFilter'.$this->table, 1));
		if (!$page) $page = 1;
        /* Choose number of results per page */
		$selectedPagination = Tools::getValue('pagination') ? Tools::getValue('pagination') : NULL;
		
		
		//echo "<pre>"; print_r($ids_product); die;
        $id_product = Tools::getValue('id_product') ? Tools::getValue('id_product') : NULL;
        $product_name = Tools::getValue('product_name') ? Tools::getValue('product_name') : NULL;
        $search = array('id_product' => $id_product, 'product_name' => $product_name);
        //var_dump($id_product);
        //var_dump($product_name); die;
            
		$orderBy = isset($_GET['lofreview_criterionOrderby1']) ? $_GET['lofreview_criterionOrderby1'] : NULL;
        $orderWay = isset($_GET['lofreview_criterionOrderway1']) ? $_GET['lofreview_criterionOrderway1'] : NULL;
        if (empty($orderBy))
			$orderBy = $cookie->__get($this->table.'Orderby1') ? $cookie->__get($this->table.'Orderby1') : NULL;
		if (empty($orderWay))
			$orderWay = $cookie->__get($this->table.'Orderway1') ? $cookie->__get($this->table.'Orderway1') : NULL;
        
        $products = $lofAttribute->getListProducts($ids_product, $lofAttribute->id, $cookie->id_lang, 1,($page-1)*$selectedPagination, $selectedPagination, $orderBy, $orderWay, $search);
        //echo "<pre>"; print_r($products); die;
		
        
        $html .= '<input type="hidden" id="lofsubmitFilter'.$this->table.'" name="lofsubmitFilter'.$this->table.'" value="0">';
        
        if ($page > 1){
    		$html .= '<input type="image" src="../img/admin/list-prev2.gif" onclick="getE(\'lofsubmitFilter'.$this->table.'\').value=1"/>&nbsp;
                <input type="image" src="../img/admin/list-prev.gif" onclick="getE(\'lofsubmitFilter'.$this->table.'\').value='.($page - 1).'"/>&nbsp;';
		}
        $html .= $this->l('Page').' <b>'.$page.'</b> / '.$totalPages .'&nbsp;';
        
        if ($page < $totalPages){            
            $html .= ' <input type="image" src="../img/admin/list-next.gif" onclick="getE(\'lofsubmitFilter'.$this->table.'\').value='.($page + 1).'"/>&nbsp;
                <input type="image" src="../img/admin/list-next2.gif" onclick="getE(\'lofsubmitFilter'.$this->table.'\').value='.$totalPages.'"/>&nbsp;';
        }
        
        $html .= $this->l('Display').'
             <select name="pagination">';
             foreach ($_pagination AS $value)
			     $html .= '<option value="'.(int)($value).'"'.($selectedPagination == $value ? ' selected="selected"' : (($selectedPagination == NULL && $value == $this->_pagination[1]) ? ' selected="selected2"' : '')).'>'.(int)($value).'</option>';
		$html .= '</select> / '.(int)($_listTotal).' '.$this->l('result(s)').'
             <span>
			     <input type="submit" name="lofsubmitReset'.$this->table.'" value="'.$this->l('Reset').'" class="button" />
		         <input type="submit" name="lofsubmitFilter'.$this->table.'" id="lofsubmitFilterButton_'.$this->table.'"  value="'.$this->l('Filter').'" class="button" />
		     </span>
        ';
        $html .= '<br/><br/>'; 
        $html .= '<table border="0" cellpadding="0" cellspacing="0" class="table" style="width: 600px;">';
        $html .= '
            	<tr class="nodrag nodrop">
                    <th><input type="checkbox" onclick="checkDelBoxes(this.form, \'lof_pro[]\', this.checked)" ></th>
                    <th>
                        '.$this->l('ID').'
                        <br/>
                        <a href="'.$currentIndex.'&'.$this->identifier.'='.$lofAttribute->id.'&viewlofreview_criterion&'.$this->table.'Orderby1='.urlencode('id_product').'&'.$this->table.'Orderway1=desc&token='.$token.'"><img border="0" src="../img/admin/down'.((isset($orderBy) AND ('id_product' == $orderBy) AND ($orderWay == 'desc')) ? '_d' : '').'.gif" /></a>
						<a href="'.$currentIndex.'&'.$this->identifier.'='.$lofAttribute->id.'&viewlofreview_criterion&'.$this->table.'Orderby1='.urlencode('id_product').'&'.$this->table.'Orderway1=asc&token='.$token.'"><img border="0" src="../img/admin/up'.((isset($orderBy) AND ('id_product' == $orderBy) AND ($orderWay == 'asc')) ? '_d' : '').'.gif" /></a>
                    </th>
                    <th>
                        '.$this->l('Product Name').'
                        <br/>
                        <a href="'.$currentIndex.'&'.$this->identifier.'='.$lofAttribute->id.'&viewlofreview_criterion&'.$this->table.'Orderby1='.urlencode('name').'&'.$this->table.'Orderway1=desc&token='.$token.'"><img border="0" src="../img/admin/down'.((isset($orderBy) AND ('name' == $orderBy) AND ($orderWay == 'desc')) ? '_d' : '').'.gif" /></a>
						<a href="'.$currentIndex.'&'.$this->identifier.'='.$lofAttribute->id.'&viewlofreview_criterion&'.$this->table.'Orderby1='.urlencode('name').'&'.$this->table.'Orderway1=asc&token='.$token.'"><img border="0" src="../img/admin/up'.((isset($orderBy) AND ('name' == $orderBy) AND ($orderWay == 'asc')) ? '_d' : '').'.gif" /></a>
                    </th>
            	</tr>
                <tr class="nodrag nodrop" style="height: 35px;">
                    <td>--</td>
                    <td> 
                        <input type="text" style="width: 20px;" value="" name="id_product">
                    </td>
                    <td> 
                        <input type="text" style="width: 160px;" value="" name="product_name">
                    </td> 
                </tr>
            ';
        $irow = 0;
        foreach ($products AS $product){
            $html .= '
            	<tr class="'.($irow++ % 2 ? 'alt_row' : '').'">
                    <td>
						<input type="checkbox" name="lof_pro[]" value="'.$product['id_product'].'" '.(($product['lofactive']==1)?'checked="checked"':'') .' >
						<input type="hidden" name="ids_pro[]" value="'.$product['id_product'].'" />
					</td>
                    <td>'.$product['id_product'].'</td>
                    <td>'.$product['name'].'</td>
            	</tr>
            ';    
        }
        $html .= '</table>';
        $html .= '<br/>';
        $html .= '<input type="hidden" name="id_attribute" value="'.$lofAttribute->id.'" />';
        $html .= '<input type="submit" name="submitSelectionAttribute" onclick="return confirm(\''.$this->l('Add selected items?').'\');" value="'.$this->l('Add selection to Attribute').'" class="button" />';
        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        //echo ' <input type="submit" name="submitDelSelectionAttribute" onclick="return confirm(\''.$this->l('Delete selected items?').'\');" value="'.$this->l('Remove').'" class="button" />';

        $html .= '<div class="separation"></div>';
        $html .= '<h3>'.$this->l('Assign Categories To Attribute:').'</h3>';
        $html .= '<p>'.$this->l('If you assign categories to this criterion, when you add or edit a product it will auto assign to this criterion').'</p>';

        $categories = Configuration::get('lofreviews_crit_cates_'.(int)$lofAttribute->id);
        if($categories)
            $categories = explode(',',$categories);
        $default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $selected_cat = Category::getCategoryInformations( $categories, $default_form_language);
        $root = Category::getRootCategory();
        $tab_root = array('id_category' => $root->id, 'name' => $root->name);
        $helper = new Helper();
        $category_tree = $helper->renderCategoryTree($tab_root, ($selected_cat ? $selected_cat : array()), 'categoryBox', false, true, array(), false, true);
        $html .= $category_tree;
        $html .= '<br/><input type="submit" name="submitAddCategory" value="'.$this->l('Add category to Criterion').'" class="button" /><br/><br/><br/>';

        $html .= '</form>';
		return $html;
    }
}


