<?php

include_once( dirname(__FILE__)."/../../classes/LofReport.php");


class AdminLofReportController extends AdminController
{
    public function __construct(){

		global $cookie;
        $this->table = 'lofreview_report';
	 	$this->className = 'LofReport';

		$this->addRowAction('delete');
        $this->_join = 'LEFT JOIN `'._DB_PREFIX_.'lofreview_review` r ON (r.id_lofreview_review = a.id_lofreview_review)';
        $this->_join .= 'LEFT JOIN `'._DB_PREFIX_.'customer` c ON (a.report_by = c.id_customer)';
		$this->_select = 'r.title AS review_title, CASE a.report_by WHEN 0 THEN \'Guest\' ELSE CONCAT(c.`firstname`,\' \',c.`lastname`,\' (\', c.`email`,\')\') END AS \'report_name\'';
        
        $this->fields_list = array(
		'id_lofreview_report' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 10),
        'review_title' => array('title' => $this->l('Title  Review'), 'align' => 'center', 'width' => 200),
		'report_name' => array('title' => $this->l('Report by'), 'align' => 'center', 'width' => 200, 'filter_key' => 'report_name','havingFilter' => true),
        'date_add' => array('title' => $this->l('Date add'), 'width' => 100, 'type' => 'date', 'align' => 'right'),
        'user_ip' => array('title' => $this->l('User IP'), 'width' => 100));
    
        parent::__construct();
    }  
	
	public function renderForm($isMainTab = true)
	{
		global $currentIndex, $cookie;
		parent::renderForm();
		
		if (!($obj = $this->loadObject(true)))
			return;	
	}
	
	public function initToolbar() {
        $token = Tools::getAdminTokenLite('AdminLofAttribute');
		
        switch ($this->display) {
            case 'add':
            case 'edit':
            case 'view':
				break;
			/*
            default: // list                
                $link = 'index.php?controller=AdminLofAttribute&amp;add' . $this->table . '&amp;token=' . $this->token;
                $this->toolbar_btn['save'] = array(
                    'href' => $link,
                    'desc' => $this->l('Add new')
                );
			*/
        }

        $this->context->smarty->assign('toolbar_scroll', 1);
        $this->context->smarty->assign('show_toolbar', 1);
        $this->context->smarty->assign('toolbar_btn', $this->toolbar_btn);
        $this->context->smarty->assign('title', $this->toolbar_title);
    }

    public function initToolbarTitle() {
        parent::initToolbarTitle();
        if ($this->display == 'edit' || $this->display == 'add') {
            $article = $this->loadObject(true);
            if ($article)
                if ((bool) $article->id && $this->display != 'list' && isset($this->toolbar_title[3]))
                    $this->toolbar_title[3] = $this->toolbar_title[3] . ' (' . $article->title[$this->context->language->id] . ')';
        }
    }
    
    /**
	 * Display list
	 *
	 * @global string $currentIndex Current URL in order to keep current Tab
	 */
	public function displayList()
	{
		global $currentIndex;
        $this->displayTop();
		/* Append when we get a syntax error in SQL query */
		if ($this->_list === false)
		{
			$this->displayWarning($this->l('Bad SQL query'));
			return false;
		}
		/* Display list header (filtering, pagination and column names) */
		$this->displayListHeader();
		if (!sizeof($this->_list))
			echo '<tr><td class="center" colspan="'.(sizeof($this->fieldsDisplay) + 2).'">'.$this->l('No items found').'</td></tr>';
		/* Show the content of the table */
		$this->displayListContent();
		/* Close list table and submit button */
		$this->displayListFooter();
	}
    
	protected function copyFromPost(&$object, $table)
	{		
		parent::copyFromPost($object, $table);
	}
	
	public function postProcess()
	{
		global $currentIndex, $cookie;
		return	parent::postProcess();
		
	}
}


