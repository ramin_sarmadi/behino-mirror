<?php
include(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');


include_once(dirname(__FILE__).'/classes/LofReview.php');

// Get data
$id_product = intval(Tools::getValue('id_product'));
$product = new Product($id_product,false,$cookie->id_lang);
//echo "<pre>"; print_r($product); die;
$lofreviews = LofReview::getReviewsByProductId($id_product,'', NULL, NULL, NULL);
$i=0;
foreach($lofreviews as $lofreview){
    $lofreviews[$i]['review_attribute'] = LofReview::getAttributesByReviewId($cookie->id_lang ,$lofreview['id_lofreview_review']);
	$i++;
}
//echo "<pre>"; print_r($lofreviews); die;




$currency = new Currency((int)($cookie->id_currency));
$affiliate = (Tools::getValue('ac') ? '?ac='.(int)(Tools::getValue('ac')) : '');
//echo "<pre>"; print_r($products); die;



// Send feed
header("Content-Type:text/xml; charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
?>
<rss version="2.0">
	<channel>
		<title><![CDATA[<?php echo $product->name; ?>]]></title>
        <pubDate><?php echo $product->date_add; ?></pubDate>
        <description><?php echo $product->description_short; ?></description>              
        <language><?php echo Language::getIsoById((int)($cookie->id_lang)); ?></language>
        <?php 
            $image = Image::getImages((int)($cookie->id_lang), $product->id);
    		echo "<item>\n";
    		echo "<title><![CDATA[".$product->name." - ".html_entity_decode(Tools::displayPrice(Product::getPriceStatic($product->id), $currency), ENT_COMPAT, 'UTF-8')." ]]></title>\n";
    		echo "<description>";
    		$cdata = true;
    		if (is_array($image) AND sizeof($image)){
    			$imageObj = new Image($image[0]['id_image']);
    			echo "<![CDATA[<img src='"._PS_BASE_URL_._THEME_PROD_DIR_.$imageObj->getExistingImgPath()."-medium.jpg' title='".str_replace('&', '', $product->name)."' alt='thumb' />";
    			$cdata = false;
    		}
    		if ($cdata)
    			echo "<![CDATA[";
    		echo $product->description_short."]]></description>\n";
    		echo "<link><![CDATA[".htmlspecialchars($link->getproductLink($product->id, $product->link_rewrite, Category::getLinkRewrite((int)($product->id_category_default), $cookie->id_lang))).$affiliate."]]></link>\n";
            echo "</item>\n";
            
            
            foreach($lofreviews as $item){
                echo "<item>\n";
                echo "<title><![CDATA[".$item['review_title'].' - ('.$item['date_add']. ") ]]></title>\n";
                echo "<description><![CDATA[".$item['comment']."<br/> ";
                foreach($item['review_attribute'] as $itema){
                    echo $itema['attribute_title'].': '.$itema['rating_sum'].'<br/>';
                }
				echo "]]></description>\n";
                echo "</item>\n";
            }
        ?>
	</channel>
</rss>