DROP TABLE IF EXISTS `ps_lofreview_attribute`;

DROP TABLE IF EXISTS `ps_lofreview_attribute_lang`;

DROP TABLE IF EXISTS `ps_lofreview_attribute_shop`;

DROP TABLE IF EXISTS `ps_lofreview_criterion`;

DROP TABLE IF EXISTS `ps_lofreview_criterion_lang`;

DROP TABLE IF EXISTS `ps_lofreview_criterion_shop`;

DROP TABLE IF EXISTS `ps_lofreview_product_attribute`;

DROP TABLE IF EXISTS `ps_lofreview_product_criterion`;

DROP TABLE IF EXISTS `ps_lofreview_report`;

DROP TABLE IF EXISTS `ps_lofreview_report_shop`;

DROP TABLE IF EXISTS `ps_lofreview_review`;

DROP TABLE IF EXISTS `ps_lofreview_review_attribute`;

DROP TABLE IF EXISTS `ps_lofreview_review_criterion`;

DROP TABLE IF EXISTS `ps_lofreview_review_shop`;