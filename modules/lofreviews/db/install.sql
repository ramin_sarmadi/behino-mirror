CREATE TABLE IF NOT EXISTS `ps_lofreview_attribute` (
  `id_lofreview_attribute` int(11) NOT NULL AUTO_INCREMENT,
  `review_type` varchar(200) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `id_shop_default` int(11) unsigned DEFAULT '1',
  PRIMARY KEY (`id_lofreview_attribute`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `ps_lofreview_attribute_lang` (
  `id_lofreview_attribute` int(11) NOT NULL DEFAULT '0',
  `id_lang` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_lofreview_attribute`,`id_lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ps_lofreview_attribute_shop` (
  `id_lofreview_attribute` int(11) unsigned NOT NULL DEFAULT '0',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lofreview_attribute`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ps_lofreview_criterion` (
  `id_lofreview_criterion` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT '1',
  `date_add` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `position` int(11) DEFAULT '0',
  `is_custom` tinyint(4) DEFAULT '0',
  `id_shop_default` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`id_lofreview_criterion`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `ps_lofreview_criterion_lang` (
  `id_lofreview_criterion` int(11) NOT NULL DEFAULT '0',
  `id_lang` int(11) NOT NULL DEFAULT '0',
  `title` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_lofreview_criterion`,`id_lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ps_lofreview_criterion_shop` (
  `id_lofreview_criterion` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_lofreview_criterion`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ps_lofreview_product_attribute` (
  `id_product` int(11) NOT NULL DEFAULT '0',
  `id_lofreview_attribute` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) DEFAULT '0',
  `rating_count` int(11) DEFAULT '0',
  `id_shop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_product`,`id_lofreview_attribute`,`id_shop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ps_lofreview_product_criterion` (
  `id_product` int(11) NOT NULL DEFAULT '0',
  `id_lofreview_criterion` int(11) NOT NULL DEFAULT '0',
  `id_shop` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_product`,`id_lofreview_criterion`,`id_shop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ps_lofreview_report` (
  `id_lofreview_report` int(11) NOT NULL AUTO_INCREMENT,
  `id_lofreview_review` int(11) DEFAULT NULL,
  `report_by` int(11) DEFAULT '0' COMMENT '0: guest',
  `date_add` datetime DEFAULT NULL,
  `user_ip` varchar(100) DEFAULT NULL,
  `id_shop_default` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`id_lofreview_report`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `ps_lofreview_report_shop` (
  `id_lofreview_report` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_lofreview_report`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ps_lofreview_review` (
  `id_lofreview_review` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `comment` text,
  `id_customer` int(11) DEFAULT '0' COMMENT '0:guest',
  `customer_name` varchar(200) DEFAULT '',
  `lock` tinyint(4) DEFAULT '0',
  `active` tinyint(4) DEFAULT '0' COMMENT '0: pending, 1: approved, 2: un-approved',
  `date_add` datetime DEFAULT NULL,
  `like_count` int(11) DEFAULT '0',
  `unlike_count` int(11) DEFAULT '0',
  `user_ip` varchar(100) DEFAULT NULL,
  `id_shop_default` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`id_lofreview_review`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `ps_lofreview_review_attribute` (
  `id_lofreview_review` int(11) NOT NULL DEFAULT '0',
  `id_lofreview_attribute` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) DEFAULT '0',
  `rating_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id_lofreview_review`,`id_lofreview_attribute`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ps_lofreview_review_criterion` (
  `id_lofreview_review` int(11) NOT NULL DEFAULT '0',
  `id_lofreview_criterion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lofreview_review`,`id_lofreview_criterion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ps_lofreview_review_shop` (
  `id_lofreview_review` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_lofreview_review`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;