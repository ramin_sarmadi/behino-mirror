<?php
/**
 * $ModDesc
 * 
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}    
/**
 * lofsliding Class
 */	
 @session_start();
class lofreviews extends Module
{
	private $_params = '';	
	private $_postErrors = array();	
    const INSTALL_SQL_FILE = 'install.sql';
    const UNINSTALL_SQL_FILE = 'uninstall.sql';
    	
   /**
    * Constructor 
    */
	function __construct()
	{
		$this->name = 'lofreviews';
		parent::__construct();			
		$this->tab = 'LandOfCoder';
		$this->author = 'LandOfCoder';
		$this->version = '1.1';
		$this->displayName = $this->l('Lof Rating Reviews Module');
		$this->description = $this->l('Lof Rating Reviews Module');
		$this->module_key = "a5909e073e43bf863f2f3ec743d75b51";
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' ) && !class_exists("LofParams", false) ){
		  require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' );
		}		
		$this->_params = new LofParams( $this->name );	

        if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofAttribute.php' ) ){
			if( !defined("LOF_LOAD_CLASSES_ATT") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofAttribute.php' );
				define("LOF_LOAD_CLASSES_ATT",true);
			}
		}
        if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofCriterion.php' ) ){
			if( !defined("LOF_LOAD_CLASSES_CRI") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofCriterion.php' );
				define("LOF_LOAD_CLASSES_CRI",true);
			}
		}
        if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofProductAttribute.php' ) ){
			if( !defined("LOF_LOAD_CLASSES_PROATT") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofProductAttribute.php' );
				define("LOF_LOAD_CLASSES_PROATT",true);
			}
		}
        if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofProductCriterion.php' ) ){
			if( !defined("LOF_LOAD_CLASSES_PROCRI") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofProductCriterion.php' );
				define("LOF_LOAD_CLASSES_PROCRI",true);
			}
		}
        if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofReport.php' ) ){
			if( !defined("LOF_LOAD_CLASSES_REP") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofReport.php' );
				define("LOF_LOAD_CLASSES_REP",true);
			}
		}
        if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofReview.php' ) ){
			if( !defined("LOF_LOAD_CLASSES_REVI") ){
				require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofReview.php' );
				define("LOF_LOAD_CLASSES_REVI",true);
			}
		}
        $this->postSubmit();	   
	}
   /**
    * process installing 
    */
	function install(){
		if (!parent::install()) return false;
        if(!$this->installTable()) return false;

        if(!$this->installTabAttributes()) return false;
        if(!$this->installTabCriterions()) return false;
        if(!$this->installTabReviews()) return false;
        if(!$this->installTabReports()) return false;

        //$this->installFile();

		if(!$this->registerHook('header') 
            OR !$this->registerHook('productTab') 
            OR !$this->registerHook('productTabContent')
            OR !$this->registerHook('extraRight')
            OR !$this->registerHook('rightColumn')
            OR !$this->registerHook('customerAccount') 
            OR !$this->registerHook('myAccountBlock')
            OR !$this->registerHook('actionProductSave')
			)
            return false;

        $this->initAttributes();
        $this->initCriterions();
        $this->initAtributeProduct();

		return true;
	}
    /**
     * process uninstalling
     */
    function uninstall()
    {
        if (!parent::uninstall())return false;
        if(!$this->uninstallTable()) return false;

        if(!$this->uninstallModuleTab('AdminLofCriterion')) return false;
        if(!$this->uninstallModuleTab('AdminLofAttribute')) return false;
        if(!$this->uninstallModuleTab('AdminLofReview')) return false;
        if(!$this->uninstallModuleTab('AdminLofReport')) return false;

        return true;
    }
    /**
     * Uninstall Table
     */
    public function uninstallTable(){
        if (!file_exists(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE))
            return (false);
        else if (!$sql = file_get_contents(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE))
            return (false);
        $sql = str_replace('ps_', _DB_PREFIX_, $sql);
        $sql = preg_split("/;\s*[\r\n]+/",$sql);
        foreach ($sql as $query)
            if ($query && !Db::getInstance()->Execute(trim($query)))
                return (false);
        return true;
    }
    /**
     * Install Table
     */
    public function installTable(){
        if (!file_exists(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE))
            return (false);
        else if (!$sql = file_get_contents(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE))
            return (false);
        $sql = str_replace('ps_', _DB_PREFIX_, $sql);
        $sql = preg_split("/;\s*[\r\n]+/",$sql);
        foreach ($sql as $query){
            if(!empty($query)){
                if (!Db::getInstance()->Execute(trim($query)))
                    return (false);
            }
        }
        return true;
    }
    /**
     *** Init Attributes
     **/
    private function initAttributes()
    {
        $attributes = LofAttribute::getAttributes();
        if(isset($attributes) AND !empty($attributes))return;

        $titles = array('Quality','Performance','Usability');
        foreach($titles as $title){
            $attribute = new LofAttribute();
            $languages = Language::getLanguages(true);
            foreach($languages AS $lang)
            {
                $attribute->{'title'}[$lang['id_lang']] = $title;
            }
            $attribute->active = 1;
            $attribute->id_shop_default = $this->context->shop->id;
            $attribute->add();
            $attribute->addAllShop();
        }
    }

    /**
     ** Init Atributes to Product
     **/

    private function initAtributeProduct(){
        $id_lang = $this->context->language->id;
        $shops = Shop::getShops();
        if($shops)
            foreach($shops as $shop){
                $attributes = LofAttribute::getAttributes(false, (int)($id_lang), $shop['id_shop']);
                $str_attributes = array();
                if(!empty($attributes)){
                    foreach($attributes as $k=>$v){
                        $str_attributes[$k] = $v['id_lofreview_attribute'];
                    }
                }
                $categories = self::getCategories( (int)($id_lang), $shop['id_shop'] );
                $str_category = array();
                if(!empty($categories)){
                    foreach($categories as $key=>$item){
                        if(!empty($item)){
                            foreach($item as $k=>$val){
                                if(isset($val["infos"])){
                                    $str_category[$k] = $val["infos"]["id_category"];
                                }
                            }
                        }
                    }
                }
                LofProductAttribute::addAttributesToCategories($str_attributes, $str_category, $shop['id_shop']);
            }
    }

    public static function getCategories($id_lang = false, $id_shop = false, $active = true, $order = true, $sql_filter = '', $sql_sort = '', $sql_limit = '') {
        if(!$id_shop)
            $id_shop = (int)Context::getContext()->shop->id;
        if (!Validate::isBool($active))
            die(Tools::displayError());
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT *
			FROM `'._DB_PREFIX_.'category` c
			LEFT JOIN `'._DB_PREFIX_.'category_shop` cs ON (c.`id_category` = cs.`id_category` AND cs.`id_shop` = '.$id_shop.')
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_category` = cl.`id_category`'.Shop::addSqlRestrictionOnLang('cl').'
			WHERE 1 '.$sql_filter.' '.($id_lang ? 'AND `id_lang` = '.(int)$id_lang : '').'
			'.($active ? 'AND `active` = 1' : '').'
			'.(!$id_lang ? 'GROUP BY c.id_category' : '').'
			'.($sql_sort != '' ? $sql_sort : 'ORDER BY c.`level_depth` ASC, cs.`position` ASC').'
			'.($sql_limit != '' ? $sql_limit : '')
        );

        if (!$order)
            return $result;

        $categories = array();
        foreach ($result as $row)
            $categories[$row['id_parent']][$row['id_category']]['infos'] = $row;

        return $categories;
    }

    /**
     *** Init Criterions
     **/
    private function initCriterions()
    {
        //require_once(dirname(__FILE__).'/classes/LofCriterion.php');
        $criterions = LofCriterion::getCriterions();
        //echo "<pre>"; print_r($criterions); die;
        if(isset($criterions) AND !empty($criterions))return;

        $titles = array('Performance','Usability');
        foreach($titles as $title){
            $criterion = new LofCriterion();
            $languages = Language::getLanguages(true);
            foreach($languages AS $lang) {
                $criterion->{'title'}[$lang['id_lang']] = $title;
            }
            $criterion->active = 1;
            $criterion->id_shop_default = $this->context->shop->id;
            $criterion->add();
            $criterion->addAllShop();
        }

        $titles2 = array('Quality','Value');
        foreach($titles2 as $title){
            $criterion2 = new LofCriterion();
            $languages2 = Language::getLanguages(true);
            foreach($languages2 AS $lang) {
                $criterion2->{'title'}[$lang['id_lang']] = $title;
            }
            $criterion2->active = 1;
            $criterion2->type = 1;
            $criterion->id_shop_default = $this->context->shop->id;
            $criterion2->add();
            $criterion2->addAllShop();
        }
    }

    function hookCustomerAccount($params){
		global $cookie,$smarty;
        $theme = Configuration::get($this->name.'_module_theme');
		$smarty->assign(array(
            'id_customer'=>(int)($cookie->id_customer),
            'theme' => $theme,
            'module_name' => $this->name
        ));
		return $this->display(__FILE__, 'tmpl/'.$this->_params->get("theme","default").'/my-account.tpl');
	}
    
    function hookMyAccountBlock($params){
		global $cookie,$smarty;
        $theme = Configuration::get($this->name.'_module_theme');
		$smarty->assign(array(
            'id_customer'=>(int)($cookie->id_customer),
            'theme' => $theme,
            'module_name' => $this->name
        ));
		return $this->display(__FILE__, 'tmpl/'.$this->_params->get("theme","default").'/my-account.tpl');
		
	}
    /**
    * Proccess module by hook
    * $pparams: param of module
    * $pos: position call
    */
	function processHook( $params, $pos, $extrahtml = ""){
		$theme = Configuration::get($this->name.'_module_theme');
        if(!$theme) $theme = "default";
		return ($this->display(__FILE__, 'tmpl/'.$theme.'/'.$pos.'.tpl')).$extrahtml;	
	}
    
    public function hookProductTab($params){
	    if (!$this->active) return;
		global $smarty,$cookie;

        $theme = Configuration::get($this->name.'_module_theme');
        if(!$theme) $theme = "default";
        $id_product = Tools::getValue("id_product");
        $countReview = LofReview::sumReview($id_product);

  		$smarty->assign(array(
			'sum_reviews' => $countReview
        ));
		return ($this->display(__FILE__, 'tmpl/'.$theme.'/hooktab.tpl'));
	}
    
    public function hookextraRight($params){
        global $smarty,$cookie;
        $id_product = Tools::getValue("id_product");

        $avgReviewByAttribute = LofReview::getAveragesByProduct($cookie->id_lang, $id_product);

        $countReview = LofReview::sumReview($id_product);

        if($countReview == 0 || empty($countReview)){
			foreach($avgReviewByAttribute as $k=>$value){
				$avgReviewByAttribute[$k]['avg'] = '0';
			}
        }
  		$smarty->assign(array(
            'avgReviewByAttribute' => $avgReviewByAttribute,
            'sumReview' =>  $countReview
        ));
		return $this->processHook($params,"hookextraright");
    }

    function hookrightColumn($params)
	{		
	    global $smarty,$cookie;
        $site_url = Tools::htmlentitiesutf8(Tools::getShopProtocol().$_SERVER['HTTP_HOST'].__PS_BASE_URI__);

        $id_product = Tools::getValue("id_product");
		$limititems = $this->getParamValue("limititems",5);
		$recent_title = $this->getParamValue("recent_title","Recent Reviews");
        $recentReview = LofProductAttribute::recentReview($cookie->id_lang, $limititems);
        $smarty->assign(array(
            'site_url' => $site_url,
            'recentReview' => $recentReview,
			'recent_title' => $recent_title
        ));
		return $this->processHook( $params,"hookrightcolumn");
	}
	
	function hookleftColumn($params)
	{		
		global $smarty,$cookie; 
        $site_url = Tools::htmlentitiesutf8(Tools::getShopProtocol().$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
        $id_product = Tools::getValue("id_product");
		$limititems = $this->getParamValue("limititems",5);
		$recent_title = $this->getParamValue("recent_title","Recent Reviews");
        $recentReview = LofProductAttribute::recentReview($cookie->id_lang, $limititems);
        $smarty->assign(array(
            'site_url' => $site_url,
            'recentReview' => $recentReview,
			'recent_title' => $recent_title
        ));
		return $this->processHook( $params,"hookrightcolumn");
	}
	
    public function getProductReview($id_product, $module_name = '') {
        global $smarty, $cookie;
        
        $productReview = LofProductAttribute::assigProductReview($cookie->id_lang, $id_product);
        $theme = Configuration::get($this->name . '_module_theme');
        if (!$theme)
            $theme = "default";
        
		$l = $this->getLangs();
		ob_start();
        require( _PS_MODULE_DIR_ . 'lofreviews/tmpl/' . $theme . '/product_review.php');
        $contentHtml = ob_get_contents();
        ob_end_clean();
        return $contentHtml;
    }
    /**
    * Hook Position
    */
    function hookHeader($params)
	{
		$this->context->controller->addCSS( ($this->_path).'assets/style.css', 'all');
		$this->context->controller->addCSS( ($this->_path).'assets/rateit.css', 'all');
		$this->context->controller->addCSS( ($this->_path).'tmpl/'. $this->getParamValue('module_theme','default').'/assets/style.css', 'all');
		$this->context->controller->addCSS( ($this->_path).'tmpl/'. $this->getParamValue('module_theme','default').'/assets/jquery.rating.css', 'all');
		if( !defined("_LOF_REVIEWS_") ){
			$this->context->controller->addJS( ($this->_path).'assets/jquery.uni-form.js', 'all');
			$this->context->controller->addJS( ($this->_path).'assets/jquery-ui.custom.min.js', 'all');
			$this->context->controller->addJS( ($this->_path).'assets/script.js', 'all');
			$this->context->controller->addJS( ($this->_path).'assets/jquery.rateit.js', 'all');
			define('_LOF_REVIEWS_', 1);         
		}
		
	}

	function hookfooter($params)
	{		
		return $this->processHook( $params,"footer");
	}
	function hookcontenttop($params)
	{ 		
    	return $this->processHook( $params,"contenttop");
	}
	
	function hooklofTop($params){
		return $this->processHook( $params,"lofTop");
	}
	function hookHome($params)
	{
		return $this->processHook( $params,"home");
	}
    function hookproductfooter($params){
		return $this->processHook( $params,"productfooter");
	}
    
	
    public function getProductNameById($id_lang, $id_product){
        $sql ='
            SELECT p.`id_product`, pl.`name`
    		FROM `'._DB_PREFIX_.'product` p
    		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product`)
    		WHERE pl.`id_lang` = '.(int)($id_lang).'
            AND p.`id_product` = '.$id_product.'
        ';
        return Db::getInstance()->ExecuteS($sql);
    }
    
    public static function saveCustomProsConsOfUser($pros_cons, $type, $active, $id_review){
        if(!empty($pros_cons)){
            $titles = explode(',', $pros_cons);
            foreach($titles as $title){
                $criterion = new LofCriterion();
    	    	$languages = Language::getLanguages(true);
                foreach($languages AS $lang)
                {
                    $criterion->{'title'}[$lang['id_lang']] = $title;
                }
                $criterion->type = $type;
                $criterion->active = $active;
				$criterion->is_custom = 1;
                $criterion->save();
                $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
                if ($is_multishop){
                    $shop_id = (int)Context::getContext()->shop->id;
                    $criterion->storeToShop($shop_id);
                }
                LofCriterion::addReviewCriterion($id_review, $criterion->id);
    	    }
            return true;
        }
        return false;
    }
    
    public static function saveProsConsOfUser($pros_cons,$id_review){
         if(!empty($pros_cons)){
            foreach($pros_cons as $key => $value){
                LofCriterion::addReviewCriterion($id_review,$key);
            }
            return true;
         }
         return false;
    }
    
    public static function checkStateOrderOfCustomer($id_order_state){
        global $cookie;
		
        $id_customer = $cookie->id_customer;
        $id_product = Tools::getValue("id_product");
		
		if(empty($id_order_state)){
			$idSql = '';
		}
		else{
			$idSql = ' AND osl.id_order_state IN ('.$id_order_state.')';
		}
        $sql = '
            SELECT oh.id_order ,osl.* , o.id_customer
            	FROM '._DB_PREFIX_.'order_history oh
            		LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON oh.id_order_state = osl.id_order_state
            		LEFT JOIN '._DB_PREFIX_.'orders o ON oh.id_order = o.id_order
            WHERE osl.id_lang = '.(int)($cookie->id_lang).$idSql.'
                  AND o.id_customer = '.(int)($id_customer).'  
            ORDER BY oh.date_add DESC
            LIMIT 1
        ';
		$order_state = array();
        if($id_customer == 0) 
			return 0;
		else{
			$order_state = Db::getInstance()->ExecuteS($sql);
			if(empty($order_state)) return 0;
		}
        //check id_product is exists in Order_Details ?  
        $orderDetails = Db::getInstance()->ExecuteS('
            SELECT *
            	FROM '._DB_PREFIX_.'order_detail
           	WHERE product_id = '.(int)($id_product).' 
                AND id_order = '.$order_state[0]['id_order'].'
        ');
        if(empty($orderDetails)) return 0;
        return true;
    }

    private function postSubmit(){
        global $cookie;
        $link = new Link();
        $adminapprovalitems = $this->getParamValue("adminapprovalitems",1);
        $adminapprovalreview = $this->getParamValue("adminapprovalreview",1);
        $antispam = $this->getParamValue("antispam",1);
        
        $id_customer = $cookie->id_customer;
        $id_product = (int)Tools::getValue("id_product");

        //event LofSubmitMessage
        $cookie = $this->context->cookie;
        
        if(Tools::isSubmit('lofreviews_submitMessage')){
			$customer = new Customer($this->context->customer->id);
			$lofcaptcha = Tools::getValue("lofreview_captcha");
            if($antispam == '1'){
                if(trim($lofcaptcha) == '' || empty($lofcaptcha) || ($_SESSION['lofreview_captcha'] != strtoupper($lofcaptcha))){
                    $this->_postErrors[] = $this->l('Captcha verification does not match.');
					return false;
                }    
            }
            if(empty($this->_postErrors)){
                //add Review
                $objReview = new LofReview();
                $objReview->id_product = (int)Tools::getValue("id_product");
                $objReview->title = Tools::getValue("lof_title");
                $objReview->comment = Tools::getValue("lof_content");
                
                if($this->context->customer->id){
                    $objReview->id_customer = $this->context->customer->id;
                    $objReview->customer_name = $customer->firstname.' '.$customer->lastname;
                }else{
                    $objReview->id_customer = 0;    
                    $objReview->customer_name = Tools::getValue("lof_nickname");
                }
                $objReview->user_ip = $_SERVER['REMOTE_ADDR'];
                $objReview->lock = 0;
                //Admin approval review required
                $objReview->active = (!$adminapprovalreview) ? 1 : 0;
                
                $objReview->like_count = 0;
                $objReview->unlike_count = 0;
                $lof_recommend = Tools::getValue("lof_recommend");
                $objReview->save();


                //add Rate + SumRate
                $id_review = $objReview->id;
                $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
                if ($is_multishop){
                  $shop_id = (int)$this->context->shop->id;
                  $objReview->storeToShop($shop_id);
                }
                $lofselect_stars = Tools::getValue("lofselect_stars");
                if(!empty($lofselect_stars)){
                    foreach($lofselect_stars as $key=>$value){
                        LofReview::addRating($id_review,$key,$value);
                        LofProductAttribute::sumRatingToProduct($id_product, $key, $value);
                    }
                }
                //create custom pros/cons
                $lofcustom_pros = Tools::getValue('lofcustom_pros');
                $lofcustom_cons = Tools::getValue('lofcustom_cons');
                
                
                //$adminapprovalitems
                $active = (!$adminapprovalitems) ? 1 : 0;
                $this->saveCustomProsConsOfUser($lofcustom_pros,1, $active,$id_review);
                $this->saveCustomProsConsOfUser($lofcustom_cons,0, $active,$id_review);
    
                //add pros/cons
                $lof_pros = Tools::getValue('lof_pros');
                $lof_cons = Tools::getValue('lof_cons');

                $this->saveProsConsOfUser($lof_pros,$id_review);
                $this->saveProsConsOfUser($lof_cons,$id_review);
                //postSuccess
				if($adminapprovalreview){
					$postSuccess = $this->l('Review posted successfully. Awaiting moderator validation.');
				}else{
					$postSuccess = $this->l('Review posted successfully.');
			   }
               
               //send e-mail to admin
				$configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_MAIL_SERVER', 'PS_MAIL_USER', 'PS_MAIL_PASSWD', 'PS_SHOP_NAME'));
				//$isGetEmail = $this->getParamValue("isgetemail");
                if($this->context->customer->id){
					//if($isGetEmail){
						$to = $this->getParamValue("sendtoadmin", Configuration::get("PS_SHOP_EMAIL","landofcoder@gmail.com"));
						$customer_name = ($this->context->customer->id) ? ($customer->firstname.' '.$customer->lastname) : 'Guest';
						$product = new Product( (int)Tools::getValue("id_product"), true, (int)($this->context->language->id) );
						
						$review_title = Tools::getValue("lof_title");
						$review_comment = Tools::getValue("lof_content");
						$review_date_add = $objReview->date_add;
						$product_link =  ($link->getProductLink($id_product));
						
						$templateVars = array(
									'{review_title}' => $review_title,
									'{customer_name}' => $customer_name,
									'{product_name}'  => $product->name,
									'{product_link}' => $product_link,
									'{review_content}' => $review_comment,
									'{review_date_add}' => $review_date_add
						);
						try{
							if(!Mail::Send((int)$this->context->language->id, 'sendtoadmin', Mail::l('Review posted send to you '),
										$templateVars, $to, NULL, ($configuration['PS_SHOP_EMAIL'] ? $configuration['PS_SHOP_EMAIL'] : NULL),
										($configuration['PS_SHOP_NAME'] ? $configuration['PS_SHOP_NAME'] : NULL), NULL, NULL,
										dirname(__FILE__).'/mails/', false)){
								$this->_postErrors[] = $this->l('An error occurred during the process.');
							}
						}
						catch (Exception $e) {
							$this->_postErrors[] = $this->l('An error occurred during the process.');
						}
						
					//}
				}

                $id_product = Tools::getValue("id_product");
    			Tools::redirect('index.php?id_product='.$id_product.'&controller=product&id_lang='.(int)($this->context->language->id));
            }
            
        }
        
    }
    
    public function hookactionProductSave($params){
        $id_product = $params['id_product'];
        $categories = Product::getProductCategories($id_product);
        $attrs = LofAttribute::getAttributes(false);
        if($attrs){
            foreach($attrs as $attr){
                $cates = Configuration::get('lofreviews_attr_cates_'.$attr['id_lofreview_attribute']);
                if($cates){
                    $cates = explode(',',$cates);
                    foreach($categories as $id_category){
                        if(in_array($id_category, $cates)){
                            LofProductAttribute::addAttributesToProduct($id_product,array($attr['id_lofreview_attribute']));
                        }
                    }
                }
            }
        }
        $criterions = LofCriterion::getCriterions(false);
        if($criterions){
            foreach($criterions as $cri){
                $cates = Configuration::get('lofreviews_crit_cates_'.$cri['id_lofreview_criterion']);
                if($cates){
                    $cates = explode(',',$cates);
                    foreach($categories as $id_category){
                        if(in_array($id_category, $cates)){
                            LofProductCriterion::addAttributesToProduct($id_product, array($cri['id_lofreview_criterion']));
                        }
                    }
                }
            }
        }
    }
    /**
     * Hook Product Tab Content
     **/
    public function hookProductTabContent($params)
    {
		if(!$this->active)return;
		global $smarty, $cookie;
		$link = new Link();
        $params = $this->_params;
        $theme = Configuration::get($this->name.'_module_theme');
        if(!$theme) $theme = "default";
        $site_url = Tools::htmlentitiesutf8(Tools::getShopProtocol().$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
		//global setting
	    $socialSharing = $this->getParamValue("socialbox",1);
        $showOrdering = $this->getParamValue("showordering", 1);
		$defaultOrdering = $this->getParamValue("defaultordering","Date"); 
		$showHelpfullness = $this->getParamValue("showhelpfulness", 1);
		$reportAbuse = $this->getParamValue("showreport", 1);
		$showAdvanceReport = $this->getParamValue("show_advancereport",1);
		$showFilterForm = $this->getParamValue("show_filterform",1);
		$showProsCons = $this->getParamValue("prosconsbox", 1);
		$showAllMyReviews = $this->getParamValue("showmyreview",1);
		$reviewSummaryBlock = $this->getParamValue("showsummary",1);
		$rssOption = $this->getParamValue("rssoption", 1);
		$mincharreview = $this->getParamValue("mincharreview",50);
		$messageMinChars = $this->l("Please input your comment greater than ".$mincharreview." characters.");
		//permisstion setting
        $onlycustomer = $this->getParamValue("onlycustomer",0);
        $orderState = $this->getParamValue("orderstate",5);
        $checkStateOrder = self::checkStateOrderOfCustomer($orderState);
        //echo"<pre>"; print_r($orderState); die;
        $guestratehelpful = $this->getParamValue("guestratehelpful",0);
        $guestreport = $this->getParamValue("guestreport",0);
        $isLogin = $this->context->customer->id;

        $adminapprovalitems = $this->getParamValue("adminapprovalitems",0); 
        $adminapprovalreview = $this->getParamValue("adminapprovalreview",0); 
        
        $customproscons = $this->getParamValue("customproscons",0);
        $antispam = $this->getParamValue("antispam",1);
        $postSuccess = '';
        
        $id_customer = $this->context->customer->id;
        $id_product = (int)Tools::getValue("id_product");
        $customer = new Customer($cookie->id_customer);
        /** test **/
        $reportAttributes = LofReview::getReportAttributes($cookie->id_lang, $id_product);
        //echo "<pre>"; print_r($reportAttributes); die;
        /**  get product name **/
        $productname = $this->getProductNameById($cookie->id_lang, $id_product);
        $attributes = LofProductAttribute::getAttributeToProduct($cookie->id_lang,$id_product);
		
		$l = $this->getLangs();
		ob_start();
			require( dirname(__FILE__).'/initjs.php' );
			$initjs = ob_get_contents();
		ob_end_clean();

        //load pros/cons
        $pros = LofCriterion::getProsCons($cookie->id_lang, $id_product, 1, 0);
        $cons = LofCriterion::getProsCons($cookie->id_lang, $id_product, 0, 0);

        $total_reviews = LofReview::sumReview($id_product);

        /** getList Review **/
        $start = isset($_REQUEST['start'])?$_REQUEST['start']:0;
        $lofreviews = NULL;
        $lofreviews = LofReview::getReviewsList(array('id_product'=> $id_product, 'id_customer'=>NULL));
         
		$listreviews = $this->generateListReviews($lofreviews['lofreviews'],$lofreviews['count']);
        $feedUrl = __PS_BASE_URI__.'modules/'.$this->name.'/rss.php?id_product='.(int)(Tools::getValue('id_product'));
        $include_form = _PS_MODULE_DIR_.$this->name.'/tmpl/'.$theme.'/formreview.tpl';
        
        //assign hook tab content
  		$smarty->assign(array(
            'site_url' => $site_url,
            'include_form' => $include_form,
            'feedUrl' => $feedUrl,
            'id_customer' => ($id_customer > 0) ? $id_customer : 0,
            'customer_name' => $customer->firstname.' '.$customer->lastname,
            'customer_email' => $customer->email,
            'module_name' => $this->name,
			'site_url' => $site_url,
            'rand' => rand(10,100),
            'postSuccess' => $postSuccess,
            'post_errors' =>$this->_postErrors,
            'id_product' =>$id_product,
            'lofreviews' => $lofreviews['lofreviews'],
			'listreviews'	=> $listreviews,
            'count' => $lofreviews['count'], 

			'productname' => $productname[0]['name'],
            'attributes' => $attributes,
			'minCharReview' => $mincharreview,
			'messageMinChars' => $messageMinChars,
            'pros' => $pros,
            'cons' => $cons,
			
			"socialSharing" => $socialSharing,
			"showHelpfullness" => $showHelpfullness,
            "showOrdering" => $showOrdering, 
            "defaultOrdering" => $defaultOrdering,
            "checkStateOrder" => $checkStateOrder,
            
			"showFilterForm"	=> $showFilterForm,
			"showAdvanceReport" => $showAdvanceReport,
			"reportAbuse" => $reportAbuse,
			"showProsCons" => $showProsCons,
			"showAllMyReviews" => $showAllMyReviews, //chua fix
			"reviewSummaryBlock" => $reviewSummaryBlock, //chua fix
			"rssOption" => $rssOption, //chua fix
			"onlycustomer" => $onlycustomer,
			"guestratehelpful" => $guestratehelpful,
			"guestreport" => $guestreport,
            "isLogin" => $isLogin,
			"antispam" => $antispam,
			"customproscons" => $customproscons,
            
            'total_reviews' => $total_reviews,
            'reportAttributes' => $reportAttributes
        ));
		return $this->processHook( $params,"hooktabcontent", $initjs);
    }

	public function generateListReviews($lofreviews,$count = 0){
	    global $cookie;
		$link = new Link();
	    $socialSharing = $this->getParamValue("socialbox",1);
		$showOrdering = $this->getParamValue("showordering",1);
		$defaultOrdering = $this->getParamValue("defaultordering","Date"); //array
		$showHelpfullness = $this->getParamValue("showhelpfulness",1);
		$guestreport = $this->getParamValue("guestreport", 0);
        $isLogin = $this->context->customer->id;

		$guestratehelpful = $this->getParamValue("guestratehelpful", 0);
        $showAllMyReviews = $this->getParamValue("showmyreview",1);
        $rssOption = $this->getParamValue("rssoption",1);
        $limititems = $this->getParamValue("limitreview",5);
        $id_customer = $cookie->id_customer;
        $id_product = (int)Tools::getValue("id_product");
        
		$site_url = Tools::htmlentitiesutf8(Tools::getShopProtocol().$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
        $feedUrl = __PS_BASE_URI__.'modules/'.$this->name.'/rss.php?id_product='.(int)(Tools::getValue('id_product'));
        
		$html = "";
		$theme = Configuration::get($this->name.'_module_theme');
		if(!$theme) $theme = "default";   

        $l = $this->getLangs();
        ob_start();
        require_once( _PS_MODULE_DIR_. 'lofreviews/tmpl/'.$theme.'/listreviews.php');
        $html = ob_get_contents();
        ob_end_clean();

		return $html;
	}

   /**
    * Get list of sub folder's name 
    */
	public function getFolderList( $path ) {
		$items = array();
		$handle = opendir($path);
		if (! $handle) {
			return $items;
		}
		while (false !== ($file = readdir($handle))) {
			if (is_dir($path . $file))
				$items[$file] = $file;
		}
		unset($items['.'], $items['..'], $items['.svn']);
		return $items;
	}

	public function getListOrderStatus(){
		global $cookie;
		$sql = 'SELECT id_order_state, name FROM '._DB_PREFIX_.'order_state_lang WHERE id_lang='.(int)($cookie->id_lang);
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
   /**
    * Render processing form && process saving data.
    */	
	public function getContent()
	{

 		$html = "";
		if (Tools::isSubmit('submit')){
			if (!sizeof($this->_postErrors)){	
                $definedConfigs = array(
                    'module_theme' => '',
                    'socialbox' => '',
                    'showordering' => '',
                    'defaultordering' => '',
                    'showhelpfulness' => '',
                    'showreport' => '',
					'show_advancereport'=>'',
					'prosconsbox' => '',
					'show_filterform' => '',
					'showmyreview' => '',
					'showsummary' => '',
					'rssoption' => '',
					'imagerate' => '',
					'limitreview' => '',
					'onlycustomer' => '',
					'mincharreview'=>'',
					'limititems'	=>'',
					'show_reviewlink' =>'',
					//'orderstate' => '',
					'guestratehelpful' => '',
					'guestreport' => '',
					'customproscons' => '',
					'adminapprovalitems' => '',
					'adminapprovalreview' => '',
					'antispam' => '',
                    'sendtoadmin' => '',
                    'isgetemail' => '',
					'recent_title' => ''
                );
				
                foreach( $definedConfigs as $config => $key ){
					$config_value = Tools::getValue($config);
					if(is_array($config_value)){
						$config_value = serialize($config_value);
					}
		      		Configuration::updateValue($this->name.'_'.$config, $config_value, true);
		    	}                
                
                if(Tools::getValue('orderstate')){
    		        if(in_array("",Tools::getValue('orderstate'))){
    		          $orderstate = "";
    		        }else{
    		          $orderstate = implode(",",Tools::getValue('orderstate'));  
    		        }
                    Configuration::updateValue($this->name.'_orderstate', $orderstate, true);
                }
                
                
             
		        $html .= '<div class="conf confirm">'.$this->l('Settings updated').'</div>';
			}
			else{
				foreach ($this->_postErrors AS $err){
					$html .= '<div class="alert error">'.$err.'</div>';
				}
			}
			// reset current values.
			$this->_params = new LofParams( $this->name );	
		}
		return $html.$this->_getFormConfig();
	}
	/**
	 * Render Configuration From for user making settings.
	 *
	 * @return context
	 */
	private function _getFormConfig(){		
		$html = '';
	    $themes = $this->getFolderList( dirname(__FILE__)."/tmpl/" );
		$l = $this->getLangs();
	    ob_start();
	    include_once dirname(__FILE__).'/config/lofreviews.php'; 
	    $html .= ob_get_contents();
	    ob_end_clean(); 
		return $html;
	}
   /**
    * Get value of parameter following to its name.
    * 
	* @return string is value of parameter.
	*/
	public function getParamValue($name, $default=''){
		return $this->_params->get( $name, $default );	
	}

    /**
    *********************************** FUNCTION FOR TAB
    ***/
    private function installModuleTab($tabClass, $tabName, $idTabParent)
	{
		@copy(_PS_MODULE_DIR_.$this->name.'/logo.gif', _PS_IMG_DIR_.'t/'.$tabClass.'.gif');
		$tab = new Tab();
		$tab->name = $tabName;
		$tab->class_name = $tabClass;
		$tab->module = $this->name;
		$tab->id_parent = $idTabParent;
		if(!$tab->save())
			return false;
        
		return true;
	}
	private function uninstallModuleTab($tabClass)
	{
		$idTab = Tab::getIdFromClassName($tabClass);
		if($idTab != 0){
			$tab = new Tab($idTab);
			$tab->delete();
			return true;
		}
		return false;
	}
    /** Install tab **/
    private function installTabCriterions(){
        $languages = Language::getLanguages(false);
		foreach ($languages as $language) {
			$arrName[$language['id_lang']] = 'Lof Review Criterions';
		}
        $idTab = Tab::getIdFromClassName('AdminParentModules');
        $this->installModuleTab('AdminLofCriterion', $arrName, $idTab);
		return true;
    }
    
    private function installTabAttributes(){
        $languages = Language::getLanguages(false);
		foreach ($languages as $language) {
			$arrName[$language['id_lang']] = 'Lof Review Attributes';
		}
        $idTab = Tab::getIdFromClassName('AdminParentModules');
        $this->installModuleTab('AdminLofAttribute', $arrName, $idTab);
		return true;
    }
    
    private function installTabReviews(){
        $languages = Language::getLanguages(false);
		foreach ($languages as $language) {
			$arrName[$language['id_lang']] = 'Lof Reviews';
		}
        $idTab = Tab::getIdFromClassName('AdminParentModules');
        $this->installModuleTab('AdminLofReview', $arrName, $idTab);
		return true;
    }
    
    private function installTabReports(){
        $languages = Language::getLanguages(false);
		foreach ($languages as $language) {
			$arrName[$language['id_lang']] = 'Lof Review Reports';
		}
        $idTab = Tab::getIdFromClassName('AdminParentModules');
        $this->installModuleTab('AdminLofReport', $arrName, $idTab);
		return true;
    }
    /**
    * Get list of sub folder's name 
    */
	public function getFolderAdmin() {
		$folders = array('cache','classes','config','controllers','css','docs','download','img','js','localization','log','mails',
		'modules','override','themes','tools','translations','upload','webservice','.','..');
		$handle = opendir(_PS_ROOT_DIR_);
		if (! $handle) {
			return false;
		}
		while (false !== ($folder = readdir($handle))) {
			if (is_dir(_PS_ROOT_DIR_ .'/'. $folder)){
				if(!in_array($folder, $folders)){
					$folderadmin = opendir(_PS_ROOT_DIR_ .'/'. $folder);
					if (!$folderadmin) 
						return _PS_ROOT_DIR_ .'/'. $folder;
					while (false !== ($file = readdir($folderadmin))) { 
						if (is_file(_PS_ROOT_DIR_ .'/'.  $folder.'/'.$file) && ($file == 'header.inc.php')){
							return _PS_ROOT_DIR_ .'/'.  $folder;
						}
					}
				}
			}
		}
		return false;
	}
    public function viewReview($obj, $review_id){
        global $cookie,$smarty ;
		$theme = $obj->getParamValue('module_theme',"default");
        $id_review = Tools::getValue("review_id");
        $reviewDetails = LofReview::getReviewDetails($cookie->id_lang, $id_review);
        
        
        $showHelpfullness = $obj->getParamValue("showhelpfulness",1);
        $guestratehelpful = $obj->getParamValue("guestratehelpful",0);
        $guestreport    = $obj->getParamValue("guestreport");
        $site_url = Tools::htmlentitiesutf8(Tools::getShopProtocol().$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
        $review_link = "";
        if(!empty($reviewDetails)){
          $review_link = $site_url."lofreviews.php?task=view&review_id=".$id_review;
          
          $review_link = urlencode($review_link);
          $reviewDetails["totalLike"] = (int)$reviewDetails["like_count"]+(int)$reviewDetails["unlike_count"];
          $reviewDetails["pros"] = LofReview::getProsConsByIdReview($cookie->id_lang, $reviewDetails["id_lofreview_review"], 1);
          $reviewDetails["cons"] = LofReview::getProsConsByIdReview($cookie->id_lang, $reviewDetails["id_lofreview_review"], 0);
        }
        $sumReview = LofReview::sumReview($reviewDetails['id_product']);
        
        $smarty->assign(array(
            "review_link" => $review_link,
            "guestreport" => $guestreport,
            "guestratehelpful" => $guestratehelpful,
            "showHelpfullness" => $showHelpfullness,
			'reviewDetails' => $reviewDetails,
            'sumReview' => $sumReview
        ));
    }
	
	public function getLangs(){
		return array(
			//listreview
			'Found' => $this->l('Found '),
			'reviews' => $this->l(' review(s)'),
			'Reviews' => $this->l('Review(s)'),
			'review by' => $this->l(' review by '),
			'All my review' => $this->l('All my review'),
			'Pros' => $this->l('Pros: '),
			'Cons' => $this->l('Cons: '),
			'Posted on' => $this->l('Posted on'),
			'Share this review' => $this->l('Share this review'),
			'Do you find this review helpful' => $this->l('(Do you find this review helpful?)'),
			'Yes' => $this->l('Yes'),
			'No' => $this->l('No'),
			'of' => $this->l(' of '),
			'people found this review helpful' => $this->l('people found this review helpful)'),
			'Inappropriate review' => $this->l('Inappropriate review '),
			'Report it' => $this->l('Report it'),
			//intjs
			'Please select your rating' => $this->l('Please select your rating!'),
			'Please enter captcha' => $this->l('Please enter captcha!'),
			'Please input your name' => $this->l('Please input your name!'),
			'Please input your title' => $this->l('Please input your title!'),
			'Please input your content' => $this->l('Please input your content!')
		);
	}
}



