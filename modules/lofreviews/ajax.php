<?php
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

include_once(dirname(__FILE__).'/lofreviews.php');

ob_start(); 
$status = 'success';
$message = '';
error_reporting(E_ALL);

$action = $_REQUEST['action'];
$obj_reviews = new lofreviews();
$response = new stdClass();

switch ($action){
	case 'pagenav':
        $start = isset($_REQUEST['page'])?$_REQUEST['page']:0;
		$is_search = (int)$_REQUEST['is_search'];
        $data_pros_cons = $_REQUEST['data_pros_cons'];
        
        $is_orderby = (int)$_REQUEST['is_orderby'];
        $orderby_value = (string)$_REQUEST['orderby_value'];
		$id_product = (int)$_REQUEST['id_product'];
        
        $id_customer = intval($_REQUEST['id_customer']);
        
		$_html = '';
 
        $lofreviews = NULL;
        if(empty($id_customer)){
            $lofreviews = LofReview::getReviewsList(array('id_product'=> $id_product, 'is_orderby' =>$is_orderby,'orderby_value' => $orderby_value, 'is_search' => $is_search, 'data_pros_cons' => $data_pros_cons ));
        }else{
            $lofreviews = LofReview::getReviewsList(array('id_product'=> $id_product, 'is_orderby' =>$is_orderby,'orderby_value' => $orderby_value, 'is_search' => $is_search, 'data_pros_cons' => $data_pros_cons, 'id_customer' => $id_customer ));
        }
		
		$_html = $obj_reviews->generateListReviews($lofreviews['lofreviews'],$lofreviews['count']);
        
        
	break;
    case 'resort':
        $id_customer = intval($_REQUEST['id_customer']);
        $orderby_value = (string)$_REQUEST['orderby_value'];
        
        $lofreviews = NULL;
        if(empty($id_customer)){
            $lofreviews = LofReview::getReviewsByCustomer(array('id_customer'=> 0, 'orderby_value' => $orderby_value));
        }else{
            $lofreviews = LofReview::getReviewsByCustomer(array('id_customer'=>$id_customer, 'orderby_value' => $orderby_value));
        }
        $_html = $obj_reviews->generateListReviews($lofreviews['lofreviews'],$lofreviews['count']);
        
        

        break;
    
    case 'helpfull':
        $id_review = (int)$_REQUEST['id_review'];
        $yes_no = isset($_REQUEST['yes_no'])?$_REQUEST['yes_no']:0;
        if($yes_no == 1){
            LofReview::updateLikeCount($id_review);
        }
        else{
            LofReview::updateUnLikeCount($id_review);
        }
		$objReview = new LofReview($id_review);
		$response->totalLike = (int)$objReview->like_count;
		$response->totalUnLike = (int)$objReview->unlike_count;
		$response->total = (int)$objReview->like_count + (int)$objReview->unlike_count;
		
        $_html = '';    
    break;
    
    case 'reportit':
        global $cookie;
        $id_review = (int)$_REQUEST['id_review'];
        $obj = new LofReport();
        $obj->id_lofreview_review = $id_review; 
        $obj->report_by = isset($cookie->id_customer) ? $cookie->id_customer : '0';  
        $obj->user_ip = $_SERVER['REMOTE_ADDR']; 
        $obj->save();
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if ($is_multishop){
            $shop_id = (int)Context::getContext()->shop->id;
            $obj->storeToShop($shop_id);
        }
        $_html = '';    
    break;
    
    
	default:
		$status = 'error';
		$message = 'Unknown parameters!';
	break;
    
}


$content = ob_get_clean();
$response->status = $status;
$response->message = $message;
switch($action){ 
	default:
		$response->params = array('content' => $_html);
	break;
}
echo json_encode($response); 
?>