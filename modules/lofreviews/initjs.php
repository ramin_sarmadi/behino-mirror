

<script type="text/javascript">
    jQuery(document).ready(function() {
        //captcha
        $('#lofreview_refresh').click(function(){
    		$('#lofreview_captcha').attr('src','<?php echo _MODULE_DIR_.$this->name; ?>/captcha.php?rand=' + Math.random());
        });
        
        //slideToggle for box
        $('#lof-reviewsStatistics-box').click(function() {
            $('#lof-reviewsStatistics-contentbox').slideToggle('slow');
                return;
        });
        $('#lof-reviewsearch-box').click(function() {
            $('#lof-reviewsearch-contentbox').slideToggle('slow');
                return;
        });
        $('#lof-reviewsubmit-box').click(function() {
            $('#lof-reviewsubmit-contentbox').slideToggle('slow');
                return;
        });
        
        //ratting
		/*
        <?php foreach($attributes as $value){ ?>   
        var $caption, $cap = jQuery("<span/>");
		jQuery('.lofrating-fields-<?php echo $value['title']; ?>').children().hide();
		$caption = $cap.clone();
		jQuery(".lofrating-fields-<?php echo $value['title']; ?>")
			.stars({
				inputType: "select",
				cancelValue: 999,
				cancelShow: false,
				captionEl: $caption
			})
        .append($caption);
        <?php } ?>
       */
        //validate form
        var form = $("#lof-send-review");
        
        var name = $("#lof_nickname");
        var nameInfo = $("#lof_nickname_info");
        
        var title = $("#lof_title");
        var titleInfo = $("#lof_title_info");
        
        var content = $("#lof_content");
        var contentInfo = $("#lof_content_info");
        
        <?php if($antispam) { ?>
        var captcha = $("#lofreview_captchainput");
        var captchaInfo = $("#lofreview_captchainput_info");
        
        captcha.blur(validateCap);
        captcha.keyup(validateCap);
         <?php }?>
        
        name.blur(validateName);
    	name.keyup(validateName);
        
        title.blur(validateTitle);
    	title.keyup(validateTitle);
        
        content.blur(validateContent);
    	content.keyup(validateContent);
        
        //check star
        function validateStar(){
            $star_info = $('#lof_star_info');
            var ret = true;
            
            $.each($("input:hidden"),function(index, obj){
                if($(obj).val() == 999) ret = false;
            });     
            
            if(!ret){
                $star_info.text("<?php echo $l['Please select your rating'];?>");
    			$star_info.addClass("lof-error");
                return false;
            }
            else{
                $star_info.text("");
    			$star_info.removeClass("lof-error");
                return true;
            }
        }
        function validateCap(){
            if(captcha.val().length < 6){
    			captcha.addClass("lof-error");
    			captchaInfo.text("<?php echo $l['Please enter captcha'];?>");
    			captchaInfo.addClass("lof-error");
    			return false;
    		}
    		else{
    			captcha.removeClass("lof-error");
                captchaInfo.text("");
    			captchaInfo.removeClass("lof-error");
    			return true;
    		}
        }
        function validateName(){
    		if(name.val().length < 4){
    			name.addClass("lof-error");
    			nameInfo.text("<?php echo $l['Please input your name'];?>");
    			nameInfo.addClass("lof-error");
    			return false;
    		}
    		else{
    			name.removeClass("lof-error");
    			nameInfo.text("");
    			nameInfo.removeClass("lof-error");
    			return true;
    		}
    	}
        function validateTitle(){
    		if(title.val().length < 4){
    			title.addClass("lof-error");
    			titleInfo.text("<?php echo $l['Please input your title'];?>");
    			titleInfo.addClass("lof-error");
    			return false;
    		}
    		else{
    			title.removeClass("lof-error");
    			titleInfo.text("");
    			titleInfo.removeClass("lof-error");
    			return true;
    		}
    	}
        function validateContent(){
    		if(content.val().length < 4){
    			content.addClass("lof-error");
    			contentInfo.text("<?php echo $l['Please input your content'];?>");
    			contentInfo.addClass("lof-error");
    			return false;
    		}
    		else{
    			content.removeClass("lof-error");
    			contentInfo.text("");
    			contentInfo.removeClass("lof-error");
    			return true;
    		}
    	}
        
        
        form.submit(function(){
    	   if(validateStar() <?php if($antispam) { ?> & validateCap() <?php } ?> & validateContent() & validateTitle() & validateName())
    			return true
    		else
    			return false;
    	});
     });
</script>