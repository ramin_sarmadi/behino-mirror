<?php
/*
  * @module 
  * @author 
  * @copyright 
  * @version 1.0
*/
if (!defined('_CAN_LOAD_FILES_') AND _PS_VERSION_ > '1.3')
	exit;


class LofReport extends ObjectModel{
    
    public  $id;
    
    public  $id_lofreview_report;
    public  $id_lofreview_review;
    public  $report_by;
    public  $date_add;
    public  $user_ip; 
    /** @var int id_shop_default **/
    public      $id_shop_default;
	
    public static $definition = array(
		'table' => 'lofreview_report',
		'primary' => 'id_lofreview_report',
		'fields' => array(
			'id_lofreview_review' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'report_by' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'date_add' => 				array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'user_ip' => 				array('type' => self::TYPE_STRING, 'validate' => 'isString')
		),
	);

    public function add($autoDate = true, $nullValues = false) {
        if(parent::add($autoDate, true))
            return $this->addShop();
        return false;
    }

    public function delete(){
        $id = $this->id;
        if(parent::delete()){
            return Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'lofreview_report_shop WHERE id_lofreview_report = '.$id);
        }
        return false;
    }

    public function addShop(){
        $sql = 'REPLACE INTO '._DB_PREFIX_.'lofreview_report_shop (id_lofreview_report,id_shop) VALUES ('.(int)$this->id.','.(int)(Context::getContext()->shop->id).')';
        return Db::getInstance()->execute($sql);
    }

    public function storeToShop($shop_id = 1, $report_id = null){
      if(isset($this->id) && !empty($this->id)){
        $report_id = $this->id;
      }
      if(!empty($report_id)){
          $query = "select COUNT(*) FROM "._DB_PREFIX_."lofreview_report_shop WHERE id_lofreview_report=".$report_id." AND id_shop=".$shop_id;
          $result = Db::getInstance()->getValue($query);
          if(empty($result) || $result <=0){
            $sql = "INSERT INTO "._DB_PREFIX_."lofreview_report_shop VALUES(".$report_id.",".$shop_id.")";
            Db::getInstance()->Execute($sql);
            return true;
          }
      }
      return false;
    }
    
};