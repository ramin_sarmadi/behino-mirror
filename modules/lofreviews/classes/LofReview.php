<?php
/*
  * @module 
  * @author 
  * @copyright 
  * @version 1.0
*/
if (!defined('_CAN_LOAD_FILES_') AND _PS_VERSION_ > '1.3')
	exit;


class LofReview extends ObjectModel{
    
    public  $id;
    public  $id_product;
    public  $title;
    public  $comment;
    public  $id_customer;
    public  $customer_name;
    public  $lock;
    public  $active;
    public  $date_add;
    public  $like_count;
    public  $unlike_count;
    public  $user_ip;
    /** @var int id_shop_default **/
    public      $id_shop_default;

	
    public static $definition = array(
		'table' => 'lofreview_review',
		'primary' => 'id_lofreview_review',
		'fields' => array(
			'id_product' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'title' => 				array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size'=>200),
			'comment' => 				array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size'=>200),
			'id_customer' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'customer_name' => 				array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
			'lock' => 				array('type' => self::TYPE_INT, 'validate' => 'isBool'),
			'active' => 				array('type' => self::TYPE_INT, 'validate' => 'isBool'),
			'date_add' => 				array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'like_count' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'unlike_count' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'user_ip' => 				array('type' => self::TYPE_STRING, 'validate' => 'isString'),
		),
	);

    public function add($autoDate = true, $nullValues = false) {
        if(parent::add($autoDate, true))
            return $this->addShop();
        return false;
    }

    public function delete(){
        $id_lofreview_review = $this->id;
        $result = true;
        if(parent::delete()){
            $result &= Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'lofreview_review_shop` WHERE id_lofreview_review = '.(int)($id_lofreview_review));
            $result &= Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'lofreview_review_attribute` WHERE id_lofreview_review = '.(int)($id_lofreview_review));
            $result &= Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'lofreview_review_criterion` WHERE id_lofreview_review = '.(int)($id_lofreview_review));
            $res = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'lofreview_report` WHERE id_lofreview_review = '.(int)($id_lofreview_review));
            if($res){
                foreach($res as $row){
                    $obj = new LofReport($row['id_lofreview_report']);
                    $result &= $obj->delete();
                }
            }
            return $result;
        }
        return false;
    }

    public function addShop(){
        $sql = 'REPLACE INTO '._DB_PREFIX_.'lofreview_review_shop (id_lofreview_review,id_shop) VALUES ('.(int)$this->id.','.(int)(Context::getContext()->shop->id).')';
        return Db::getInstance()->execute($sql);
    }

    public static function getReviewById($id_review){
        $id_shop = (int)Context::getContext()->shop->id;
        global $cookie;
        $sql = '
            SELECT * 
            	FROM  '._DB_PREFIX_.'lofreview_review r
            	LEFT JOIN '._DB_PREFIX_.'lofreview_review_shop rs ON (rs.`id_lofreview_review` = r.`id_lofreview_review` AND rs.`id_shop` = '.$id_shop.')
            WHERE r.id_lofreview_review = '.(int)($id_review).'
        ';
        $lofreviews = Db::getInstance()->ExecuteS($sql);
        
        $i=0;
        foreach($lofreviews as $lofreview){
            $lofreviews[$i]['review_attribute'] = self::getAttributesByReviewId($cookie->id_lang ,$lofreview['id_lofreview_review']);
			$i++;
        }
        return $lofreviews[0];
    }
    
    
    public static function addRating($id_review,$id_attribute,$rating_sum){
        if ($rating_sum < 0) $rating_sum = 0;
        //if($rating_count < 0) $rating_count = 0;
        $sql = '
            INSERT INTO `'._DB_PREFIX_.'lofreview_review_attribute`
                (`id_lofreview_review`,`id_lofreview_attribute`, `rating_sum`)
            VALUES('.(int)($id_review).', '.(int)($id_attribute).','.(int)($rating_sum).')';
        return Db::getInstance()->Execute($sql);
    } 
    
    
    public static function getAttributesByReviewId($id_lang, $id_review){
        $id_shop = (int)Context::getContext()->shop->id;
        $_join = "";
        $_where = "";
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_attribute_shop` AS sa ON (a.`id_lofreview_attribute` = sa.`id_lofreview_attribute` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
		else
		    $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_attribute_shop` AS sa ON (a.`id_lofreview_attribute` = sa.`id_lofreview_attribute` AND sa.id_shop = a.id_shop_default) ';


		// we add restriction for shop
		if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        $sql = 'SELECT ra.id_lofreview_review, a.*, al.title as \'attribute_title\', ra.rating_sum
            	FROM '._DB_PREFIX_.'lofreview_attribute a
                '.$_join.'
            	LEFT JOIN '._DB_PREFIX_.'lofreview_attribute_lang al ON a.id_lofreview_attribute = al.id_lofreview_attribute
            	LEFT JOIN '._DB_PREFIX_.'lofreview_review_attribute ra ON ra.id_lofreview_attribute = a.id_lofreview_attribute
            		WHERE al.id_lang = '.(int)($id_lang).' AND a.active = 1 AND  ra.id_lofreview_review = '.(int)($id_review).$_where.'
        ';   
        
        //echo "<pre>"; print_r($sql); 
        return Db::getInstance()->ExecuteS($sql);
    }
    public static function getReviewsByProductId($id_product,$orderby_data = NULL, $search_data = NULL, $id_customer = NULL){
        if($id_product == '0' || empty($id_product)){
            $where_id_product = ' ';
        }
        else{
            $where_id_product = ' AND r.id_product = '.(int)($id_product);    
        }
        $orderby = isset($orderby_data) ? $orderby_data : ' ORDER BY r.date_add DESC';

        if($search_data == '' || empty($search_data)){
            $search = '';
        }   
        else{
            $search = isset($search_data) ? (' AND rc.id_lofreview_criterion IN ('.$search_data.')') : '';    
        }

        $_join = "";
        $_where = "";
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$_join .= ' JOIN `'._DB_PREFIX_.'lofreview_review_shop` AS sa ON (r.`id_lofreview_review` = sa.`id_lofreview_review` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
		else
		    $_join .= ' JOIN `'._DB_PREFIX_.'lofreview_review_shop` AS sa ON (r.`id_lofreview_review` = sa.`id_lofreview_review` AND sa.id_shop = r.id_shop_default) ';

		// we add restriction for shop
		if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        $sql = 'SELECT DISTINCT(r.id_lofreview_review), r.active, r.id_product ,r.id_customer, r.customer_name, r.title as \'review_title\', r.`comment` , r.date_add, r.like_count, r.unlike_count, r.`lock`
            	   FROM '._DB_PREFIX_.'lofreview_review r
                   LEFT JOIN '._DB_PREFIX_.'lofreview_review_criterion rc  ON r.id_lofreview_review = rc.id_lofreview_review
                   '.$_join.'
                WHERE r.active = 1 '.$where_id_product.$_where
                .$search.$orderby ;
        return Db::getInstance()->ExecuteS($sql);
    }
    
	public static function getProsConsByIdReview($id_lang, $id_review, $type){
	    $_join = "";
        $_where = "";
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_criterion_shop` AS sa ON (c.`id_lofreview_criterion` = sa.`id_lofreview_criterion` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
		else
		    $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_criterion_shop` AS sa ON (c.`id_lofreview_criterion` = sa.`id_lofreview_criterion` AND sa.id_shop = c.id_shop_default) ';


		// we add restriction for shop
		if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
		$sql = '
			SELECT rc.id_lofreview_review, c.*, cl.title
				FROM '._DB_PREFIX_.'lofreview_criterion c
                    '.$_join.'
					LEFT JOIN '._DB_PREFIX_.'lofreview_criterion_lang cl ON c.id_lofreview_criterion = cl.id_lofreview_criterion
					LEFT JOIN '._DB_PREFIX_.'lofreview_review_criterion rc ON c.id_lofreview_criterion = rc.id_lofreview_criterion
			WHERE c.active = 1
				AND cl.id_lang = '.(int)($id_lang).'
				AND id_lofreview_review = '.(int)($id_review).'
                AND c.type = '.(int)($type).$_where.'
		';
		$result = Db::getInstance()->ExecuteS($sql);
		return $result;
	}
    
    public static function getReviewsByCustomer($data = NULL){
        global $cookie;
        $id_shop = (int)Context::getContext()->shop->id;
        $orderby = isset($data['orderby_value'])? (' ORDER BY r.'.$data['orderby_value'].' DESC') : ' ORDER BY r.date_add DESC';
        $id_customer = $data['id_customer'];
        
        $sql = 'SELECT DISTINCT(r.id_lofreview_review), r.active, r.id_product ,r.id_customer, r.customer_name, r.title as \'review_title\', r.`comment` , r.date_add, r.like_count, r.unlike_count, r.`lock`
            	   FROM '._DB_PREFIX_.'lofreview_review r
                   LEFT JOIN '._DB_PREFIX_.'lofreview_review_shop rs  ON (r.id_lofreview_review = rs.id_lofreview_review AND rs.`id_shop` = '.$id_shop.')
                   LEFT JOIN '._DB_PREFIX_.'lofreview_review_criterion rc  ON r.id_lofreview_review = rc.id_lofreview_review
                WHERE r.active = 1 AND id_customer = '.$id_customer.$orderby;
        
        $lofreviews = Db::getInstance()->ExecuteS($sql);
		
        if(!empty($lofreviews)){
    		foreach($lofreviews as $key => $value){
                $lofreviews[$key]["pros"] = self::getProsConsByIdReview($cookie->id_lang, $lofreviews[$key]["id_lofreview_review"], 1);
                $lofreviews[$key]["cons"] = self::getProsConsByIdReview($cookie->id_lang, $lofreviews[$key]["id_lofreview_review"], 0);
            }  
		}
        $i=0;
        foreach($lofreviews as $lofreview){
            $lofreviews[$i]['review_attribute'] = self::getAttributesByReviewId($cookie->id_lang ,$lofreview['id_lofreview_review']);
			$i++;
        }
        return array('lofreviews' => $lofreviews, 'count' => '');
    }
	
    public static function getReviewsList($data = null){
        global $cookie;
        $id_product = $data['id_product'];
 
	    $step = (int)Configuration::get('lofreviews_limitreview');
		$is_search = isset($data['is_search'])? $data['is_search']:0;
        $data_pros_cons = isset($data['data_pros_cons']) ? $data['data_pros_cons'] : NULL;
        
        $is_orderby = isset($data['is_orderby'])? $data['is_orderby']:0;
        $orderby_value = isset($data['orderby_value'])? (' ORDER BY r.'.$data['orderby_value'].' DESC') : NULL;
        $id_customer = isset($data['id_customer'])? $data['id_customer']: NULL; 
        
        $data_count_reviews = sizeof(self::getReviewsByProductId($id_product, $orderby_value, $data_pros_cons, $id_customer));
        $lofreviews = self::getReviewsByProductId($id_product, $orderby_value, $data_pros_cons, $id_customer);

        if(!empty($lofreviews)){
    		foreach($lofreviews as $key => $value){
                $lofreviews[$key]["pros"] = self::getProsConsByIdReview($cookie->id_lang, $lofreviews[$key]["id_lofreview_review"], 1);
                $lofreviews[$key]["cons"] = self::getProsConsByIdReview($cookie->id_lang, $lofreviews[$key]["id_lofreview_review"], 0);
            }  
		}
        $i=0;
        foreach($lofreviews as $lofreview){
            $lofreviews[$i]['review_attribute'] = self::getAttributesByReviewId($cookie->id_lang ,$lofreview['id_lofreview_review']);
			$i++;
        }
		
        return array('lofreviews' => $lofreviews, 'count' => $data_count_reviews);
    }
	
	public static function activeAllReview(){
        $id_shop = (int)Context::getContext()->shop->id;
	   $reviews = Db::getInstance()->ExecuteS('SELECT r.* FROM '._DB_PREFIX_.'lofreview_review r
	    LEFT JOIN '._DB_PREFIX_.'lofreview_review_shop rs ON ( r.`id_lofreview_review` = rs.`id_lofreview_review` AND rs.`id_shop` = '.$id_shop.')');
       if(!empty($reviews)){
            foreach($reviews as $item){
                Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'lofreview_review SET active = 1 WHERE id_lofreview_review = '.$item['id_lofreview_review']);
            }
            return true; 
       }
       return false;
	}
    public static function activeSelectionReview( $reviewids = array(),$active = 1){
       if(!empty($reviewids)){
          Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'lofreview_review SET active = '.$active.' WHERE id_lofreview_review IN ('.implode(',',$reviewids).')');
          return true;
       }
       return false;
    }
   
    public static function updateLikeCount($id_review = NULL){
        if(!empty($id_review)){
            Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'lofreview_review SET like_count = like_count + 1 WHERE id_lofreview_review = '.(int)($id_review));    
        }
        return false;
    }
    
    public static function updateUnLikeCount($id_review = NULL){
        if(!empty($id_review)){
            Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'lofreview_review SET unlike_count = unlike_count + 1 WHERE id_lofreview_review = '.(int)($id_review));    
        }
        return false;
    }
    
    public static function sumReview($id_product = NULL){
        $_join = "";
        $_where = "";
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        //if ($is_multishop){
            if (Shop::getContext() == Shop::CONTEXT_SHOP)
    			$_join .= ' JOIN `'._DB_PREFIX_.'lofreview_review_shop` AS sa ON (a.`id_lofreview_review` = sa.`id_lofreview_review` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
    		else
    		    $_join .= ' JOIN `'._DB_PREFIX_.'lofreview_review_shop` AS sa ON (a.`id_lofreview_review` = sa.`id_lofreview_review` AND sa.id_shop = a.id_shop_default) ';
        //}

		// we add restriction for shop
		if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        $result = Db::getInstance()->getValue('
            SELECT COUNT(a.id_lofreview_review) AS \'sum_review\'
        	FROM '._DB_PREFIX_.'lofreview_review AS a
            '.$_join.'
        	WHERE id_product = '.(int)($id_product).$_where.'
            AND active = 1
        ');
        return $result;
    }

    public static function getAveragesByProduct($id_lang, $id_product = NULL){
        $_join = "";
        $_where = "";
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$_join .= ' JOIN `'._DB_PREFIX_.'lofreview_attribute_shop` AS sa ON (a.`id_lofreview_attribute` = sa.`id_lofreview_attribute` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
		else
		    $_join .= '  JOIN `'._DB_PREFIX_.'lofreview_attribute_shop` AS sa ON (a.`id_lofreview_attribute` = sa.`id_lofreview_attribute` AND sa.id_shop = a.id_shop_default) ';


		// we add restriction for shop
		//if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        $result = Db::getInstance()->ExecuteS('
            SELECT pa.id_product, pa.id_lofreview_attribute, al.title,a.active, a.review_type, a.position,
            	pa.rating_count, pa.rating_sum, ROUND((pa.rating_sum/pa.rating_count),2) as \'avg\'
            	FROM '._DB_PREFIX_.'lofreview_product_attribute pa
            	LEFT JOIN '._DB_PREFIX_.'lofreview_attribute a ON a.id_lofreview_attribute = pa.id_lofreview_attribute
            	LEFT JOIN '._DB_PREFIX_.'lofreview_attribute_lang al ON al.id_lofreview_attribute = a.id_lofreview_attribute
                '.$_join.'
            WHERE id_product = '.(int)($id_product).' 
                AND al.id_lang = '.(int)($id_lang).' 
                AND a.active = 1'.$_where.' AND pa.id_shop = '.(int)Context::getContext()->shop->id.'
            ORDER BY a.`position` ASC
        ');
        return $result;
    }
    
    //get Total Number Review By id_Attribute and rating_sum
    public static function getNumberReview($id_lang, $id_product, $numberStar = '', $id_attribute){
        $id_shop = (int)Context::getContext()->shop->id;
        if(!isset($numberStar) || trim($numberStar) != ''){
            $numberStar = ' AND ra.rating_sum = '.(int)($numberStar).'';
        }
        
        $sql = '
            SELECT ra.*,r.id_product, al.title as \'attribute_name\'
            	FROM '._DB_PREFIX_.'lofreview_review_attribute ra
            	   LEFT JOIN '._DB_PREFIX_.'lofreview_attribute a ON a.id_lofreview_attribute = ra.id_lofreview_attribute
            	   LEFT JOIN '._DB_PREFIX_.'lofreview_attribute_lang al ON al.id_lofreview_attribute = ra.id_lofreview_attribute
            	   LEFT JOIN '._DB_PREFIX_.'lofreview_review r ON ra.id_lofreview_review = r.id_lofreview_review
            	   JOIN '._DB_PREFIX_.'lofreview_review_shop rs ON (rs.id_lofreview_review = ra.id_lofreview_review AND rs.id_shop = '.$id_shop.')
            WHERE al.id_lang = '.(int)($id_lang).'
            	AND r.active = 1 
            	AND a.active = 1
                AND r.id_product = '.(int)($id_product).'
            	'.($numberStar).'
                AND ra.id_lofreview_attribute = '.(int)($id_attribute).'
        ';
        $result = Db::getInstance()->ExecuteS($sql);
        return $result;
    } 
    
    // total rating_sum
    public static function sumRatingByNumberStar($id_lang, $id_product, $numberStar, $id_attribute){
        $total_review = sizeof(self::getNumberReview($id_lang, $id_product, $numberStar, $id_attribute));
        $sum_review = sizeof(self::getNumberReview($id_lang, $id_product, '', $id_attribute)); 
        
        
        if($total_review == 0 || $sum_review == 0){
            $avg = 0;
        }else{
            $avg = round($total_review*100/$sum_review, 0);    
        }
        
        return array('total_review' => $total_review, // total review by star
                     'sum_review' => $sum_review, // total review by Attribute
                     'avg' => $avg
                     );
    }
    
    //get Attributes
    public static function getReportAttributes($id_lang, $id_product){
        $_join = "";
        $_where = "";
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_attribute_shop` AS sa ON (a.`id_lofreview_attribute` = sa.`id_lofreview_attribute` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
		else
		    $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_attribute_shop` AS sa ON (a.`id_lofreview_attribute` = sa.`id_lofreview_attribute` AND sa.id_shop = a.id_shop_default) ';


		// we add restriction for shop
		//if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        $sql = '
            SELECT a.*, al.title as \'attribute_name\'
            	FROM '._DB_PREFIX_.'lofreview_attribute a
                   '.$_join.'
            	   LEFT JOIN '._DB_PREFIX_.'lofreview_attribute_lang al ON al.id_lofreview_attribute = a.id_lofreview_attribute
				   LEFT JOIN '._DB_PREFIX_.'lofreview_product_attribute pa ON( pa.id_lofreview_attribute = a.id_lofreview_attribute AND pa.id_shop = '.(int)Context::getContext()->shop->id.')
            WHERE al.id_lang = '.(int)($id_lang).'
            	AND a.active = 1
				AND pa.id_product = '.$id_product.$_where.'
        ';

        $attributes = Db::getInstance()->ExecuteS($sql);
        foreach($attributes as $key => $value){
            $attributes[$key]["1star"] = self::sumRatingByNumberStar($id_lang, $id_product, 1, $attributes[$key]['id_lofreview_attribute']);
            $attributes[$key]["2star"] = self::sumRatingByNumberStar($id_lang, $id_product, 2, $attributes[$key]['id_lofreview_attribute']);
            $attributes[$key]["3star"] = self::sumRatingByNumberStar($id_lang, $id_product, 3, $attributes[$key]['id_lofreview_attribute']); 
            $attributes[$key]["4star"] = self::sumRatingByNumberStar($id_lang, $id_product, 4, $attributes[$key]['id_lofreview_attribute']); 
            $attributes[$key]["5star"] = self::sumRatingByNumberStar($id_lang, $id_product, 5, $attributes[$key]['id_lofreview_attribute']); 
                                                
        }
        return $attributes;
    }
    
    public static function getReviewDetails($id_lang, $id_review){
        $sql = '
            SELECT r.*, pl.description, pl.description_short, pl.`name`, i.cover, CONCAT( pl.id_product, \'-\', i.id_image) id_image, pl.link_rewrite
            	FROM '._DB_PREFIX_.'lofreview_review r
            	   LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (pl.id_product = r.id_product)
            	   LEFT JOIN '._DB_PREFIX_.'image i ON (i.id_product = r.id_product AND i.cover = 1)
            	WHERE r.active = 1 AND pl.id_lang = '.(int)($id_lang).' 
           		   AND r.id_lofreview_review = '.(int)($id_review).'
        ';    
        $result = Db::getInstance()->ExecuteS($sql);
        $result[0]["avg_rating"] = LofProductAttribute::avgStarRecentReview($id_lang, $result[0]['id_product']);
        $result[0]['review_attribute'] = self::getAttributesByReviewId($id_lang ,$result[0]['id_lofreview_review']);
        
        return $result[0];
    }
    
    public static function listCustomerByIdReview($ids_review){
        if(empty($ids_review) || $ids_review == ''){
            return false;
        }else{
            $sql = '
               SELECT pl.`name`, r.* 
        	   FROM '._DB_PREFIX_.'lofreview_review r 
        	       LEFT JOIN '._DB_PREFIX_.'product_lang pl ON pl.id_product = r.id_product
        	   WHERE id_lang = 1 
                    AND id_lofreview_review IN ('.$ids_review.')
            ';
            $result = Db::getInstance()->ExecuteS($sql);
            return $result;
        }
        return false;
    }
    public function storeToShop($shop_id = 1, $review_id = null){
      if(isset($this->id) && !empty($this->id)){
        $review_id = $this->id;
      }
      if(!empty($review_id)){
          $query = "select COUNT(*) FROM "._DB_PREFIX_."lofreview_review_shop WHERE id_lofreview_review=".$review_id." AND id_shop=".$shop_id;
          $result = Db::getInstance()->getValue($query);
          if(empty($result) || $result <=0){
            $sql = "INSERT INTO "._DB_PREFIX_."lofreview_review_shop VALUES(".$review_id.",".$shop_id.")";
            Db::getInstance()->Execute($sql);
            return true;
          }
      }
      return false;
    }
};