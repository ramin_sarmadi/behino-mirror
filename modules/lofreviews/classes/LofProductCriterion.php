<?php
/*
  * @module 
  * @author 
  * @copyright 
  * @version 1.0
*/
if (!defined('_CAN_LOAD_FILES_') AND _PS_VERSION_ > '1.3')
	exit;


class LofProductCriterion extends ObjectModel{
    public 		$id;

    public static function addAttributesToCategories($attributes = array(), $categories = array()){
        
        if (empty($attributes) OR empty($categories))
	 		die(Tools::displayError());
        $categories = !is_array($categories)?array($categories):$categories;
        $attributes = !is_array($attributes)?array($attributes):$attributes;
        $result = Db::getInstance()->ExecuteS('
		SELECT `id_product`
		FROM `'._DB_PREFIX_.'category_product`
		WHERE `id_category` in ('.implode(',',$categories).')');
        $checkproducts = array();
        foreach ($result AS $row) {
			if(!in_array($row["id_product"],$checkproducts)){
                $checkproducts[ (int)$row["id_product"]] = (int)$row["id_product"];
                self::addAttributesToProduct( (int)$row["id_product"], $attributes);
			}
		}
    }
    public static function addAttributesToProduct( $id_product, $attributes = array()){
        $id_shop = (int)Context::getContext()->shop->id;
       if(!empty($attributes)){
          Db::getInstance()->Execute('
	        DELETE FROM `'._DB_PREFIX_.'lofreview_product_criterion`
	    	WHERE `id_product` ='.$id_product);
          $tmp = array();
          foreach($attributes as $val){
            $tmp[] = '('.$id_product.','.$val.','.$id_shop.')';
          }
          if(!empty($tmp))
            Db::getInstance()->Execute('
	            INSERT INTO `'._DB_PREFIX_.'lofreview_product_criterion`
	        	(`id_product`, `id_lofreview_criterion`, `id_shop`) VALUES '.implode(",",$tmp));
          return true;
       }
       return false;
    }
    
    public static function getAttributeToProduct($id_lang, $id_product){
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = '
            SELECT pa.*, al.id_lang, al.title
        	   FROM '._DB_PREFIX_.'lofreview_attribute a 
            LEFT JOIN '._DB_PREFIX_.'lofreview_product_criterion pa ON (a.id_lofreview_criterion = pa.id_lofreview_criterion AND pa.id_shop = '.$id_shop.')
            LEFT JOIN '._DB_PREFIX_.'lofreview_attribute_lang al ON a.id_lofreview_criterion = al.id_lofreview_criterion
            WHERE al.id_lang = '.(int)$id_lang.' 
                AND id_product = '.(int)$id_product.'
            ORDER BY a.`position` ASC
        ';
        $result = Db::getInstance()->ExecuteS($sql);
        
        return $result;
    }
    
    public static function checkExistsPA($id_product, $id_attribute){
        $id_shop = (int)Context::getContext()->shop->id;
        $result = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'lofreview_product_criterion
                    WHERE id_product = '.intval($id_product).'
                        AND id_lofreview_criterion = '.intval($id_attribute).' AND id_shop = '.$id_shop);
        if(sizeof($result) > 0){
            return false;
        }
        else{
            return true;
        }
        return false;
    } 
    
    public static function addRecordProductAttribute($ids_product = array(), $ids_uncheck = array(), $id_attribute){
        $id_shop = (int)Context::getContext()->shop->id;
        $return = true;
        if(!empty($ids_product)){
            foreach($ids_product as $id){
                if(self::checkExistsPA($id, $id_attribute)){
                    $return &= Db::getInstance()->Execute('
                    INSERT INTO '._DB_PREFIX_.'lofreview_product_criterion(id_product, id_lofreview_criterion, id_shop)
                        VALUES('.intval($id).', '.intval($id_attribute).', '.$id_shop.')');
                }
            }
            //sau khi add xong thi` xoa tat ca nhung~ id_product cua id_attribute ko thuoc mang? ids_product
            $return &= self::removeRecordProductAttribute($ids_uncheck, $id_attribute);
        }
        return $return;
    }
    
    
    public static function removeRecordProductAttribute($ids_uncheck = array(), $id_attribute){
        $id_shop = (int)Context::getContext()->shop->id;
        $result = Db::getInstance()->ExecuteS('
            SELECT * FROM '._DB_PREFIX_.'lofreview_product_criterion 
                WHERE id_lofreview_criterion = '.intval($id_attribute).' AND id_shop = '.$id_shop);
        $return = true;
        if($return)
            foreach($result as $item){
                if(in_array($item['id_product'], $ids_uncheck)){
                    $return &= Db::getInstance()->Execute('
                        DELETE FROM '._DB_PREFIX_.'lofreview_product_criterion
                            WHERE id_product = '.intval($item['id_product']).' AND id_lofreview_criterion = '.intval($id_attribute).' AND id_shop = '.$id_shop);
                }
            }
        return $return;
    }

};