<?php
/*
  * @module 
  * @author 
  * @copyright 
  * @version 1.0
*/

if (!defined('_CAN_LOAD_FILES_') AND _PS_VERSION_ > '1.3')
	exit;

class LofCriterion extends ObjectModel
{
	public 		$id;
	
	/** @var string title **/
	public 		$title;
	/** @var boolean type **/
	public 		$type;
	/** @var boolean active **/
	public 		$active = 1;
	/** @var boolean position **/
    public 		$position;
	/** @var string Object creation date */
	public 		$date_add;
	
	public 		$is_custom;
    /** @var int id_shop_default **/
    public      $id_shop_default;
    
	
	public static $definition = array(
		'table' => 'lofreview_criterion',
		'primary' => 'id_lofreview_criterion',
		'multilang' => true,
		'fields' => array(
			'type' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'date_add' => 				array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'active' => 		array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'position' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'is_custom' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'size' => 4),
			
			// Lang fields
			'title' => 				array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 200)
		),
	);

    public function __construct($id = null, $id_lang = null, $id_shop = null)
	{
		parent::__construct($id, $id_lang, $id_shop);
	}

    public function add($autoDate = true, $nullValues = false) {
        $this->position = $this->getLastPosition();
        if(parent::add($autoDate, true))
            return $this->addShop();
        return false;
    }

    public function update($null_values = false) {
        if(parent::update($null_values))
            return $this->addShop();
        return false;
    }

    public function delete(){
        $id_lofreview_criterion = $this->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $result = true;
        if(parent::delete()){
            $result &= Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'lofreview_criterion_shop` WHERE id_lofreview_criterion = '.(int)($id_lofreview_criterion).' AND id_shop = '.$id_shop);
            $result &= Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'lofreview_product_criterion` WHERE id_lofreview_criterion = '.(int)($id_lofreview_criterion).' AND id_shop = '.$id_shop);
            $result &= Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'lofreview_review_criterion` WHERE
                id_lofreview_criterion = '.(int)($id_lofreview_criterion).' AND id_lofreview_review IN (
                    SELECT r.id_lofreview_review
                    FROM `'._DB_PREFIX_.'lofreview_review` r
                    JOIN `'._DB_PREFIX_.'lofreview_review_shop` rs ON(r.`id_lofreview_review` = rs.`id_lofreview_review`)
                    WHERE rs.`id_shop` = '.$id_shop.')');

            return $result;
        }
        return false;
    }

    public function addShop(){
        $id_shop = (int)(Context::getContext()->shop->id);
        if($id_shop){
            $sql = 'REPLACE INTO '._DB_PREFIX_.'lofreview_criterion_shop (id_lofreview_criterion, id_shop) VALUES ('.(int)$this->id.','.(int)(Context::getContext()->shop->id).')';
            return Db::getInstance()->execute($sql);
        }
        return true;
    }

    public function addAllShop(){
        $shops = Shop::getShops();
        $return = true;
        if($shops)
            foreach($shops as $shop){
                $sql = 'REPLACE INTO '._DB_PREFIX_.'lofreview_criterion_shop (id_lofreview_criterion,id_shop) VALUES ('.(int)$this->id.','.(int)($shop['id_shop']).')';
                $return &= Db::getInstance()->execute($sql);
            }
        return $return;
    }

    function getLastPosition() {
        $current = Db::getInstance()->getValue('SELECT MAX(position) FROM ' . _DB_PREFIX_ . 'lofreview_criterion ');
        return (int)($current) + 1;
    }

    public function updatePosition($way, $position) {
        $query = '
			SELECT cp.`id_lofreview_criterion`, cp.`position`
			FROM `' . _DB_PREFIX_ . 'lofreview_criterion` cp
			ORDER BY cp.`position` ASC';

        if (!$res = Db::getInstance()->ExecuteS($query))
            return false;
        foreach ($res AS $attribute)
            if ((int) ($attribute['id_lofreview_criterion']) == (int) ($this->id))
                $movedAttribute = $attribute;

        if (!isset($movedAttribute) || !isset($position))
            return false;
        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases
        $query1 = '
			UPDATE `' . _DB_PREFIX_ . 'lofreview_criterion`
			SET `position`= `position` ' . ($way ? '- 1' : '+ 1') . '
			WHERE `position`
			' . ($way ? '> ' . (int) ($movedAttribute['position']) . ' AND `position` <= ' . (int) ($position) : '< ' . (int) ($movedAttribute['position']) . ' AND `position` >= ' . (int) ($position));
        $query2 = '
			UPDATE `' . _DB_PREFIX_ . 'lofreview_criterion`
			SET `position` = ' . (int) ($position) . '
			WHERE `id_lofreview_criterion`=' . (int) ($movedAttribute['id_lofreview_criterion']);
        $result = (Db::getInstance()->Execute($query1) AND Db::getInstance()->Execute($query2));
        return $result;
    }

    public static function cleanPositions() {
        $return = true;
        $result = Db::getInstance()->executeS('
			SELECT c.`id_lofreview_criterion`
			FROM `'._DB_PREFIX_.'lofreview_criterion` c
			ORDER BY c.`position`
		');
        $count = count($result);
        for ($i = 0; $i < $count; $i++){
            $sql = '
				UPDATE `'._DB_PREFIX_.'lofreview_criterion` c
				SET c.`position` = '.(int)($i+1).'
				WHERE c.`id_lofreview_criterion` = '.(int)$result[$i]['id_lofreview_criterion'];
            $return &= Db::getInstance()->execute($sql);
        }
        return $return;
    }

	public static function getCriterions($id_lang = NULL){
	    if(!$id_lang)$id_lang = Configuration::get('PS_LANG_DEFAULT');
        $_join = "";
        $_where = "";
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_criterion_shop` AS sa ON (c.`id_lofreview_criterion` = sa.`id_lofreview_criterion` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
		else
		    $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_criterion_shop` AS sa ON (c.`id_lofreview_criterion` = sa.`id_lofreview_criterion` AND sa.id_shop = c.id_shop_default) ';

		// we add restriction for shop
		if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
		$sql ='
		SELECT c.*,cl.title
		FROM `'._DB_PREFIX_.'lofreview_criterion` c
		    LEFT JOIN `'._DB_PREFIX_.'lofreview_criterion_lang` cl ON c.id_lofreview_criterion = cl.id_lofreview_criterion
            '.$_join.'
		WHERE 1
		    AND cl.`id_lang` = '.(int)($id_lang).$_where.'
		ORDER BY c.`position` ASC
		';

		return Db::getInstance()->ExecuteS($sql);
	}
    
    public static function getProsCons($id_lang, $id_product = NULL, $type, $is_custom = null){
        $_join = "";
        $_where = "";
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_criterion_shop` AS sa ON (c.`id_lofreview_criterion` = sa.`id_lofreview_criterion` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
		else
		    $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_criterion_shop` AS sa ON (c.`id_lofreview_criterion` = sa.`id_lofreview_criterion` AND sa.id_shop = c.id_shop_default) ';


		// we add restriction for shop
		if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        $sql='
            SELECT DISTINCT c.id_lofreview_criterion, c.*, cl.title, pc.id_product
                FROM '._DB_PREFIX_.'lofreview_criterion c
                    '.$_join.'
                    LEFT JOIN '._DB_PREFIX_.'lofreview_criterion_lang cl ON c.id_lofreview_criterion = cl.id_lofreview_criterion
					LEFT JOIN '._DB_PREFIX_.'lofreview_product_criterion pc ON c.id_lofreview_criterion = pc.id_lofreview_criterion
                WHERE cl.id_lang = '.(int)($id_lang).' AND c.active = 1 AND c.type = '.$type.$_where.'
        ';
		if($is_custom !== NULL){
			$sql .=" AND c.is_custom ='".(int)$is_custom."'";
		}
		if($id_product !==NULL){
			$sql .=" AND pc.id_product =".(int)$id_product;
		}
        $sql .= ' ORDER BY c.`position` ASC';

        return Db::getInstance()->ExecuteS($sql);
    }
    
    
    public static function addReviewCriterion($id_review, $id_criterion){
        return Db::getInstance()->Execute('
            INSERT INTO '._DB_PREFIX_.'lofreview_review_criterion
                (id_lofreview_review, id_lofreview_criterion)
                VALUES('.(int)($id_review).', '.(int)($id_criterion).')
        ');
    }
	
	public function getProductsList($id_attribute, $id_lang = NULL ){
        $id_shop = (int)Context::getContext()->shop->id;
        return Db::getInstance()->ExecuteS('
            SELECT pl.`name`, pa.* 
            	FROM '._DB_PREFIX_.'lofreview_product_criterion pa
            	   LEFT JOIN '._DB_PREFIX_.'lofreview_criterion_lang al ON al.id_lofreview_criterion = pa.id_lofreview_criterion
            	   LEFT JOIN '._DB_PREFIX_.'product_lang pl ON pl.id_product = pa.id_product
                WHERE pl.id_lang = '.(int)($id_lang).' AND al.id_lang = '.(int)($id_lang).' AND al.id_lofreview_criterion = '.(int)($id_attribute).' and pa.`id_shop` = '.$id_shop.'
        ');
    }
	public function getListProducts($ids_product = array(), $id_attribute, $id_lang = NULL, $isPaging, $start = NULL, $end = NULL, $orderBy = NULL, $orderWay = NULL,$search = NULL){
        $id_shop = Context::getContext()->shop->id;
        if($isPaging){
            if(!empty($start) || !empty($end)){
                $limit=' LIMIT '.$start.','.$end;    
            }
            else{
                $limit='';    
            }
        }
        else{
            $limit='';
        }
        if(empty($orderBy) || empty($orderWay)){
            $strOrderBy = '';
        }else{
            $strOrderBy = ' ORDER BY '.trim($orderBy).' '.trim($orderWay);
        }
        if(empty($search)){
            $strSearch = '';
        }else{
            $strSearch = empty($search['id_product']) ? '' : ' AND pl.id_product='.$search['id_product'];
            $strSearch .= empty($search['product_name']) ? '' : ' AND pl.name=\''.$search['product_name'].'\'';
        }
        $sql = '
            SELECT DISTINCT pl.id_product, pl.name
            	FROM '._DB_PREFIX_.'product_lang pl
            	JOIN '._DB_PREFIX_.'product_shop ps ON(pl.`id_product` = ps.`id_product` AND ps.`id_shop` = '.(int)($id_shop).')
                WHERE pl.id_lang = '.(int)($id_lang).$strSearch.$strOrderBy.$limit;

        $result = Db::getInstance()->ExecuteS($sql);
        $i = 0;
        foreach($result as $item){
            $result[$i]['lofactive'] = (in_array($item['id_product'], $ids_product)) ?  '1' : '0';
            //$result[$i]['rating'] = self::getRatingCountOfProduct($item['id_product'], $id_attribute, $id_lang);
            $i++;
        }
		//echo "<pre>"; print_r($result); echo "</pre><br/>";
        return $result;
    }
    public function storeToShop($shop_id = 1, $criterion_id = null){
      if(isset($this->id) && !empty($this->id)){
        $criterion_id = $this->id;
      }
      if(!empty($criterion_id)){
          $query = "select COUNT(*) FROM "._DB_PREFIX_."lofreview_criterion_shop WHERE id_lofreview_criterion=".$criterion_id." AND id_shop=".$shop_id;
          $result = Db::getInstance()->getValue($query);
          if(empty($result) || $result <=0){
            $sql = "INSERT INTO "._DB_PREFIX_."lofreview_criterion_shop VALUES(".$criterion_id.",".$shop_id.")";
            Db::getInstance()->Execute($sql);
            return true;
          }
      }
      return false;
    }
};


