<?php
/*
  * @module 
  * @author 
  * @copyright 
  * @version 1.0
*/
if (!defined('_CAN_LOAD_FILES_') AND _PS_VERSION_ > '1.3')
	exit;


class LofProductAttribute extends ObjectModel{
    public 		$id;

    public static function addAttributesToCategories($attributes = array(), $categories = array(), $id_shop = null){
        if(!$id_shop)
            $id_shop = (int)Context::getContext()->shop->id;
        if (empty($attributes) OR empty($categories))
	 		return false;
        $categories = !is_array($categories)?array($categories):$categories;
        $attributes = !is_array($attributes)?array($attributes):$attributes;
        $result = Db::getInstance()->ExecuteS('
		SELECT `id_product`
		FROM `'._DB_PREFIX_.'category_product`
		WHERE `id_category` in ('.implode(',',$categories).')');
        $checkproducts = array();
        $i = 0;
        foreach ($result AS $row)
		{
			if(!in_array($row["id_product"],$checkproducts)){
                $checkproducts[ (int)$row["id_product"]] = (int)$row["id_product"];
                self::addAttributesToProduct( (int)$row["id_product"], $attributes, $id_shop);
			}
		}
    }

    public static function addAttributesToProduct( $id_product, $attributes = array(), $id_shop = null){
        if(!$id_shop)
            $id_shop = (int)Context::getContext()->shop->id;
       if(!empty($attributes)){
         /**
          Db::getInstance()->Execute('
	        DELETE FROM `'._DB_PREFIX_.'lofreview_product_attribute`
	    	WHERE `id_product` ='.$id_product);*/
          $exists_attrs = self::checkProductExists($id_product, $attributes );
           if(!$exists_attrs)
               $exists_attrs = array();
          $tmp = array();
          foreach($attributes as $val){
            if(!in_array($val, $exists_attrs))
                $tmp[] = '('.$id_product.','.$val.','.$id_shop.')';
          }

          if(!empty($tmp))
            Db::getInstance()->Execute('
	            INSERT INTO `'._DB_PREFIX_.'lofreview_product_attribute`
	        	(`id_product`, `id_lofreview_attribute`, `id_shop`) VALUES '.implode(",", $tmp));
          return true;
       }
       return false;
    }
    public static function checkProductExists($id_product, $attributes = array()){
        $id_shop = (int)Context::getContext()->shop->id;
      if(!empty($id_product) && $attributes){
        $result = Db::getInstance()->ExecuteS('
		SELECT `id_lofreview_attribute`
		FROM `'._DB_PREFIX_.'lofreview_product_attribute`
		WHERE `id_product` ='.$id_product. ' AND id_shop ='.$id_shop);
        $delete_attrs = array();
        $exists_attrs = array();
        if(!empty($result)){

          foreach($result as $row){
             if(!in_array($row["id_lofreview_attribute"], $attributes)){
                $delete_attrs[] = $row["id_lofreview_attribute"];
             }else{
                $exists_attrs[] = $row["id_lofreview_attribute"];
             }
          }
          if(!empty($delete_attrs)){
             Db::getInstance()->Execute('
	         DELETE FROM `'._DB_PREFIX_.'lofreview_product_attribute`
	    	 WHERE `id_product` ='.$id_product.' AND `id_lofreview_attribute` IN ('.implode(",",$delete_attrs).') AND `id_shop` = '.$id_shop);
          }
        }
        if(!empty($exists_attrs))
            return $exists_attrs;
      }
      return false;
    }
    public static function getAttributeToProduct($id_lang, $id_product, $id_shop = null){
        if(!$id_shop)
            $id_shop = (int)Context::getContext()->shop->id;
        $_join = "";
        $_where = "";
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_attribute_shop` AS sa ON (a.`id_lofreview_attribute` = sa.`id_lofreview_attribute` AND sa.id_shop = '.(int)Context::getContext()->shop->id.') ';
		else
		    $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofreview_attribute_shop` AS sa ON (a.`id_lofreview_attribute` = sa.`id_lofreview_attribute` AND sa.id_shop = a.id_shop_default) ';


		// we add restriction for shop
		if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
			$_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        $sql = '
            SELECT pa.*, al.id_lang, al.title
        	   FROM '._DB_PREFIX_.'lofreview_attribute a
            '.$_join.'
            LEFT JOIN '._DB_PREFIX_.'lofreview_product_attribute pa ON (a.id_lofreview_attribute = pa.id_lofreview_attribute AND pa.id_shop = '.$id_shop.')
            LEFT JOIN '._DB_PREFIX_.'lofreview_attribute_lang al ON a.id_lofreview_attribute = al.id_lofreview_attribute
            WHERE al.id_lang = '.(int)$id_lang.' 
                AND id_product = '.(int)$id_product.$_where.'
            ORDER BY a.`position` ASC
        ';
        $result = Db::getInstance()->ExecuteS($sql);
        
        return $result;
    }
    
    public static function sumRatingToProduct($id_product,$id_attribute,$rate){
        $id_shop = (int)Context::getContext()->shop->id;
        $result = Db::getInstance()->ExecuteS('
            SELECT * 
                FROM '._DB_PREFIX_.'lofreview_product_attribute 
                WHERE id_product = '.$id_product.' AND id_lofreview_attribute = '.$id_attribute. ' AND id_shop = '.$id_shop);
        if(sizeof($result) > 0){
            $sql = '
                UPDATE '._DB_PREFIX_.'lofreview_product_attribute
                    SET rating_count = rating_count + 1, 
                        rating_sum = rating_sum + '.intval($rate).'
                    WHERE id_product = '.$id_product.' AND id_lofreview_attribute = '.$id_attribute.' AND id_shop = '.$id_shop;
            Db::getInstance()->Execute($sql);
            return true;
        }
        return false;
    }
    
    
    public static function getReviewByDate($id_product){
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = '
            SELECT r.*
            	FROM '._DB_PREFIX_.'lofreview_review r
            	JOIN '._DB_PREFIX_.'lofreview_review_shop rs ON(rs.id_shop = '.$id_shop.' AND rs.`id_lofreview_review` = r.`id_lofreview_review`)
            	WHERE r.active = 1 
            		AND id_product = '.intval($id_product).'
            		ORDER BY r.date_add DESC
            	LIMIT 1
        ';
        $result = Db::getInstance()->ExecuteS($sql);
        return $result[0];
    }
    
    public static function avgStarRecentReview($id_lang, $id_product){
        $avgByAttributes = LofReview::getAveragesByProduct($id_lang, $id_product);

        $avgByRecent = 0;
        $count = 0;
        if(!empty($avgByAttributes)){
            $count = sizeof($avgByAttributes);
            foreach($avgByAttributes as $k => $v){
                $avgByRecent = $avgByRecent + $avgByAttributes[$k]['avg'];
            }
        }
		
		if($count == 0)
			$result = 0;
		else
			$result = $avgByRecent*20/$count;
		
        return $result;
    }
    
    public static function recentReview($id_lang, $limit=5, $start = 0){
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = '
            SELECT DISTINCT(pl.id_product), pl.`name`, pl.link_rewrite, CONCAT(pl.id_product, \'-\', i.id_image) as id_image, i.position, i.cover
            	FROM '._DB_PREFIX_.'lofreview_review r
            	JOIN '._DB_PREFIX_.'lofreview_review_shop rs ON(rs.id_shop = '.$id_shop.' AND rs.`id_lofreview_review` = r.`id_lofreview_review`)
            	LEFT JOIN '._DB_PREFIX_.'image i ON (i.id_product = r.id_product AND i.cover = 1)
            	LEFT JOIN '._DB_PREFIX_.'product_lang pl ON r.id_product = pl.id_product
            WHERE pl.id_lang = '.intval($id_lang).'
                AND r.active = 1
			ORDER BY date_add DESC 
			LIMIT '.$start.','.$limit.'
        ';
        $ids_product = Db::getInstance()->ExecuteS($sql);
        foreach($ids_product as $k => $values){
            $ids_product[$k]['review'] = self::getReviewByDate($ids_product[$k]['id_product']);
            $ids_product[$k]['avg_rating'] = self::avgStarRecentReview($id_lang, $ids_product[$k]['id_product']);
			
			$total_review = LofReview::sumReview($ids_product[$k]['id_product']);
            $ids_product[$k]['total_review'] = $total_review;
        }
        return $ids_product;
    }
    public static function getProductReview($id_lang, $id_product = 0){
         $data['review'] = self::getReviewByDate($id_product);
         $data['avg_rating'] = self::avgStarRecentReview($id_lang, $id_product);
        return $data;
	}
    public static function checkExistsPA($id_product, $id_attribute){
        $id_shop = (int)Context::getContext()->shop->id;
        $result = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'lofreview_product_attribute
                    WHERE id_product = '.intval($id_product).'
                        AND id_lofreview_attribute = '.intval($id_attribute).' AND id_shop = '.$id_shop);
        if(sizeof($result) > 0){
            return false;
        }
        else{
            return true;
        }
        return false;
    } 
    
    public static function addRecordProductAttribute($ids_product = array(), $ids_uncheck = array(), $id_attribute){
        $id_shop = (int)Context::getContext()->shop->id;
        $return = true;
        if(!empty($ids_product)){
            foreach($ids_product as $id){
                if(self::checkExistsPA($id, $id_attribute)){
                    $return &= Db::getInstance()->Execute('
                    INSERT INTO '._DB_PREFIX_.'lofreview_product_attribute(id_product, id_lofreview_attribute, id_shop)
                        VALUES('.intval($id).', '.intval($id_attribute).','.$id_shop.')');
                }
            }
            $return &= self::removeRecordProductAttribute($ids_uncheck, $id_attribute);
        }
        return $return;
    }
    
    
    public static function removeRecordProductAttribute($ids_uncheck = array(), $id_attribute){
        $id_shop = (int)Context::getContext()->shop->id;
        $result = Db::getInstance()->ExecuteS('
            SELECT * FROM '._DB_PREFIX_.'lofreview_product_attribute 
                WHERE id_lofreview_attribute = '.intval($id_attribute).'');
        $return = true;
        if($result)
            foreach($result as $item){
                if(in_array($item['id_product'], $ids_uncheck)){
                    $return &= Db::getInstance()->Execute('
                        DELETE FROM '._DB_PREFIX_.'lofreview_product_attribute
                            WHERE id_product = '.intval($item['id_product']).' AND id_lofreview_attribute = '.intval($id_attribute).' AND id_shop = '.$id_shop);
                }
            }
        return $return;
    }
    
    
    public static function assigProductReview($id_lang, $id_product){
        
        $total_review = LofReview::sumReview($id_product);
        $avg_rating = LofProductAttribute::avgStarRecentReview($id_lang, $id_product);
        //echo '-'.$total_review[0]['sum_review'].','.$avg_rating;
		if($total_review == 0 || empty($total_review)){
            $avg_rating = 0;
        }
        $result = array(
            "total_review" => $total_review,
            "avg_rating" => $avg_rating 
        );
        return $result;
    }
};