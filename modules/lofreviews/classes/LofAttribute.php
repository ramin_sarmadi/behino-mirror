<?php
/*
  * @module 
  * @author 
  * @copyright 
  * @version 1.0
*/

if (!defined('_CAN_LOAD_FILES_') AND _PS_VERSION_ > '1.3')
	exit;

class LofAttribute extends ObjectModel
{
    /** @var string id_lofreview_attribute **/
	public 		$id_lofreview_attribute;
    /** @var string title **/
	public 		$title;
    /** @var string description **/
	public 		$description;
    
    /** @var string review_type **/
	public 		$review_type;
    /** @var string status **/
	public 		$active = 1;
    /** @var string ordering **/
    public 		$position;
    /** @var int id_shop_default **/
    public      $id_shop_default;

    /**
	 * @see ObjectModel::$definition
	 */
    public static $definition = array(
		'table' => 'lofreview_attribute',
		'primary' => 'id_lofreview_attribute',
		'multilang' => true,
		'fields' => array(
            // Classic fields
    		'id_shop_default' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            // Shop fields
			'review_type' => 		array('type' => self::TYPE_STRING, 'validate' => 'isCatalogName', 'size' => 200),
			'active' => 		    array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'position' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),

			// Lang fields
			'title' => 				array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 200),
			'description' => 		array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size'=>250)
		),
	);

    public function __construct($id_attribute = null, $id_lang = null, $id_shop = null) {
		parent::__construct($id_attribute, $id_lang, $id_shop);
	}

    public function add($autoDate = true, $nullValues = false) {
        $this->position = $this->getLastPosition();
        if(parent::add($autoDate, true))
            return true;
            //return $this->addShop();
        return false;
    }

    public function update($null_values = false) {
        if(parent::update($null_values))
            return true;
            //return $this->addShop();
        return false;
    }

    public function delete(){
        $id_lofreview_attribute = $this->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $result = true;
        if(parent::delete()){
            $result &= Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'lofreview_attribute_shop` WHERE id_lofreview_attribute = '.(int)($id_lofreview_attribute).' AND id_shop ='.($id_shop));
            $result &= Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'lofreview_product_attribute` WHERE id_lofreview_attribute = '.(int)($id_lofreview_attribute).' AND id_shop ='.($id_shop));
            $result &= Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'lofreview_review_attribute` WHERE
                id_lofreview_attribute = '.(int)($id_lofreview_attribute).' AND id_lofreview_review IN (
                    SELECT r.id_lofreview_review
                    FROM `'._DB_PREFIX_.'lofreview_review` r
                    JOIN `'._DB_PREFIX_.'lofreview_review_shop` rs ON(r.`id_lofreview_review` = rs.`id_lofreview_review`)
                    WHERE rs.`id_shop` = '.$id_shop.')');
            return $result;
        }
        return false;
    }

    public function addShop(){
        $sql = 'REPLACE INTO '._DB_PREFIX_.'lofreview_attribute_shop (id_lofreview_attribute,id_shop) VALUES ('.(int)$this->id.','.(int)(Context::getContext()->shop->id).')';
        return Db::getInstance()->execute($sql);
    }

    public function addAllShop(){
        $shops = Shop::getShops();
        $return = true;
        if($shops)
            foreach($shops as $shop){
                $sql = 'REPLACE INTO '._DB_PREFIX_.'lofreview_attribute_shop (id_lofreview_attribute,id_shop) VALUES ('.(int)$this->id.','.(int)($shop['id_shop']).')';
                $return &= Db::getInstance()->execute($sql);
            }
        return $return;
    }

    function getLastPosition() {
        $current = Db::getInstance()->getValue('SELECT MAX(position) FROM ' . _DB_PREFIX_ . 'lofreview_attribute ');
        return (int)($current) + 1;
    }

    public function updatePosition($way, $position) {
        $query = '
			SELECT cp.`id_lofreview_attribute`, cp.`position`
			FROM `' . _DB_PREFIX_ . 'lofreview_attribute` cp
			ORDER BY cp.`position` ASC';

        if (!$res = Db::getInstance()->ExecuteS($query))
            return false;
        foreach ($res AS $attribute)
            if ((int) ($attribute['id_lofreview_attribute']) == (int) ($this->id))
                $movedAttribute = $attribute;

        if (!isset($movedAttribute) || !isset($position))
            return false;
        // < and > statements rather than BETWEEN operator
        // since BETWEEN is treated differently according to databases
        $query1 = '
			UPDATE `' . _DB_PREFIX_ . 'lofreview_attribute`
			SET `position`= `position` ' . ($way ? '- 1' : '+ 1') . '
			WHERE `position`
			' . ($way ? '> ' . (int) ($movedAttribute['position']) . ' AND `position` <= ' . (int) ($position) : '< ' . (int) ($movedAttribute['position']) . ' AND `position` >= ' . (int) ($position));
        $query2 = '
			UPDATE `' . _DB_PREFIX_ . 'lofreview_attribute`
			SET `position` = ' . (int) ($position) . '
			WHERE `id_lofreview_attribute`=' . (int) ($movedAttribute['id_lofreview_attribute']);
        $result = (Db::getInstance()->Execute($query1) AND Db::getInstance()->Execute($query2));
        return $result;
    }

    public static function cleanPositions()
    {
        $return = true;
        $result = Db::getInstance()->executeS('
			SELECT c.`id_lofreview_attribute`
			FROM `'._DB_PREFIX_.'lofreview_attribute` c
			ORDER BY c.`position`
		');
        $count = count($result);
        for ($i = 0; $i < $count; $i++){
            $sql = '
				UPDATE `'._DB_PREFIX_.'lofreview_attribute` c
				SET c.`position` = '.(int)($i+1).'
				WHERE c.`id_lofreview_attribute` = '.(int)$result[$i]['id_lofreview_attribute'];
            $return &= Db::getInstance()->execute($sql);
        }
        return $return;
    }

    public function getTitle($id_lang = NULL)
    {
    	if (!$id_lang){
    		global $cookie;
    		if (isset($this->title[$cookie->id_lang]))
    			$id_lang = $cookie->id_lang;
    		else
    			$id_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
    	}
    	return isset($this->title[$id_lang]) ? $this->title[$id_lang] : '';
    }
    
	public static function getAttributes($active = true, $id_lang = NULL, $id_shop = null){
    	if(!$id_lang)
            $id_lang = (int)Context::getContext()->language->id;
        if(!$id_shop)
            $id_shop = (int)Context::getContext()->shop->id;
    	$sql ='
    	SELECT a.*,al.*
    	FROM `'._DB_PREFIX_.'lofreview_attribute` a
    	LEFT JOIN `'._DB_PREFIX_.'lofreview_attribute_lang` al ON a.id_lofreview_attribute = al.id_lofreview_attribute
    	LEFT JOIN `'._DB_PREFIX_.'lofreview_attribute_shop` sa ON a.id_lofreview_attribute = sa.id_lofreview_attribute
    	WHERE al.`id_lang` = '.(int)($id_lang).' AND sa.`id_shop` = '.(int)($id_shop).($active ? ' AND a.active = '.(int)($active) : '');

    	return Db::getInstance()->ExecuteS($sql);
	}

    
    public function getProductsLite($id_attribute, $id_lang = NULL ){
        $id_shop = Context::getContext()->shop->id;
        return Db::getInstance()->ExecuteS('
            SELECT pl.`name`, pa.* 
            	FROM '._DB_PREFIX_.'lofreview_product_attribute pa
            	   LEFT JOIN '._DB_PREFIX_.'lofreview_attribute_lang al ON al.id_lofreview_attribute = pa.id_lofreview_attribute
            	   LEFT JOIN '._DB_PREFIX_.'product_lang pl ON pl.id_product = pa.id_product
            	   LEFT JOIN '._DB_PREFIX_.'product_shop ps ON (ps.id_product = pa.id_product AND ps.`id_shop` = '.(int)($id_shop).')
                WHERE pl.id_lang = '.(int)($id_lang).' AND al.id_lang = '.(int)($id_lang).' AND al.id_lofreview_attribute = '.(int)($id_attribute).' AND pa.`id_shop` = '.$id_shop.'
        ');
    }
    
    public function getRatingCountOfProduct($id_product, $id_attribute, $id_lang = NULL){
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = '
            SELECT pa.rating_sum, pa.rating_count
            	FROM '._DB_PREFIX_.'lofreview_product_attribute pa
            	   LEFT JOIN '._DB_PREFIX_.'lofreview_attribute_lang al ON al.id_lofreview_attribute = pa.id_lofreview_attribute
            	   LEFT OUTER JOIN '._DB_PREFIX_.'product_lang pl ON pl.id_product = pa.id_product
                WHERE pl.id_lang = '.(int)($id_lang).' AND al.id_lang = '.(int)($id_lang).' 
            	   AND pl.id_product = '.(int)($id_product).'	
            	   AND al.id_lofreview_attribute = '.(int)($id_attribute).' AND pa.`id_shop` = '.$id_shop.'
                   LIMIT 1
        ';
        $result = Db::getInstance()->ExecuteS($sql);
        if(empty($result)) return array('rating_sum' => '--', 'rating_count' => '--');
        return $result[0];
    }
    
    public function getListProducts($ids_product = array(), $id_attribute, $id_lang = NULL, $isPaging, $start = NULL, $end = NULL, $orderBy = NULL, $orderWay = NULL,$search = NULL){
        $id_shop = (int)Context::getContext()->shop->id;
        if($isPaging){
            if(!empty($start) || !empty($end)){
                $limit=' LIMIT '.$start.','.$end;    
            }else{
                $limit='';    
            }
        }else{
            $limit='';
        }
		if(empty($orderBy) || empty($orderWay)){
            $strOrderBy = '';
        }else{
            $strOrderBy = ' ORDER BY '.trim($orderBy).' '.trim($orderWay);
        }
        if(empty($search)){
            $strSearch = '';
        }else{
            $strSearch = empty($search['id_product']) ? '' : ' AND pl.id_product='.$search['id_product'];
            $strSearch .= empty($search['product_name']) ? '' : ' AND pl.name=\''.$search['product_name'].'\'';
        }
        $sql = '
            SELECT DISTINCT pl.id_product, pl.name
            	FROM '._DB_PREFIX_.'product_lang pl
            	JOIN '._DB_PREFIX_.'product_shop ps ON (pl.id_product = ps.id_product AND ps.id_shop = '.$id_shop.')
                WHERE pl.id_lang = '.(int)($id_lang).$strSearch.$strOrderBy.$limit;
        //echo "<pre>"; print_r($sql); echo "</pre><br/>";
        
        $result = Db::getInstance()->ExecuteS($sql);
        $i = 0;
        foreach($result as $item){
            $result[$i]['lofactive'] = (in_array($item['id_product'], $ids_product)) ?  '1' : '0';
            $result[$i]['rating'] = self::getRatingCountOfProduct($item['id_product'], $id_attribute, $id_lang);
            $i++;
        }
        return $result;
    }
    
    
    
};


