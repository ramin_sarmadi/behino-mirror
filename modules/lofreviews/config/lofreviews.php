<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
?>
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.css";?>" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/jquery-ui.css";?>" type="text/css" media="screen" charset="utf-8" />


<script type="text/javascript" src="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.js";?>"></script>
<script type="text/javascript">
$(document).ready(function() {   	            
    //When page loads...
    $(".tab_content").hide(); //Hide all content
    $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content
    
    //On Click Event
    $("ul.tabs li").click(function() {
    
    	$("ul.tabs li").removeClass("active"); //Remove any "active" class
    	$(this).addClass("active"); //Add "active" class to selected tab
    	$(".tab_content").hide(); //Hide all tab content
    
    	var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content        
    	$(activeTab).fadeIn(); //Fade in the active ID content
    	return false;
    });     
    
  });
</script>
<?php  
    $yesNoLang = array("0"=>$this->l('No'),"1"=>$this->l('Yes'));  
    
    $themes = $this->getFolderList(_PS_MODULE_DIR_.$this->name."/tmpl/" );    
?>
<h3><?php echo $this->l('Lof Rating Reviews Configuration');?></h3>
<form action="<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>" enctype="multipart/form-data" method="post" id="lofform">
  <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
            
            <fieldset>
                <legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Global Setting'); ?></legend>
                <div class="lof_config_wrrapper clearfix">
                  <ul>
                    <li>
                    <?php
						$order_status = $this->getListOrderStatus();
						$orderstatus = array();
						if(!empty($order_status)){
							foreach($order_status as $key=>$item){
								$orderstatus[$item["id_order_state"]] = $item["name"];
							}
						}
                        echo $this->_params->selectTag("module_theme",$themes,$this->getParamValue("module_theme","default"),$this->l('Theme - Layout'),'class="inputbox select-group"','class="row"','' );
                        echo $this->_params->radioBooleanTag("socialbox", $yesNoLang,$this->getParamValue("socialbox",1),$this->l('Show social sharing box'),'class="select-option"','class="row"','','');
						
						  $ordering = array(
								'date'=>$this->l('Date'),
								'name'=>$this->l('Name'),
								'title' => $this->l('Title '),
                                'helpfullness' => $this->l('Helpfullness ')
							);
						echo $this->_params->radioBooleanTag("showordering", $yesNoLang,$this->getParamValue("showordering",1),$this->l('Show Ordering'),'class="select-option"','class="row"','','');
						echo $this->_params->selectTag("defaultordering",$ordering,$this->getParamValue("defaultordering","date"),$this->l('Default Ordering'),'class="inputbox select-group" size="5"','class="row"','' );
						
						echo $this->_params->radioBooleanTag("showhelpfulness", $yesNoLang,$this->getParamValue("showhelpfulness",1),$this->l('Show Helpfulness'),'class="select-option"','class="row"','','');
                        echo $this->_params->radioBooleanTag("showreport", $yesNoLang,$this->getParamValue("showreport",1),$this->l('Show report abuse box'),'class="select-option"','class="row"','','');
                        echo $this->_params->radioBooleanTag("prosconsbox", $yesNoLang,$this->getParamValue("prosconsbox",1),$this->l('Show pros, cons box'),'class="select-option"','class="row"','','');
						echo $this->_params->radioBooleanTag("showmyreview", $yesNoLang,$this->getParamValue("showmyreview",1),$this->l('Show all my reviews '),'class="select-option"','class="row"','','');
						echo $this->_params->radioBooleanTag("show_advancereport", $yesNoLang,$this->getParamValue("show_advancereport",1),$this->l('Show Advance Stats Block'),'class="select-option"','class="row"','','');
						echo $this->_params->radioBooleanTag("show_filterform", $yesNoLang,$this->getParamValue("show_filterform",1),$this->l('Show Filter Reviews Block'),'class="select-option"','class="row"','','');
						echo $this->_params->radioBooleanTag("showsummary", $yesNoLang,$this->getParamValue("showsummary",1),$this->l('Review summary block '),'class="select-option"','class="row"','','');
                        echo $this->_params->radioBooleanTag("rssoption", $yesNoLang,$this->getParamValue("rssoption",1),$this->l('Rss option'),'class="select-option"','class="row"','','');
                        //echo $this->_params->inputTag("imagerate",$this->getParamValue("imagerate",30),$this->l('Image rate'),'class="select-option"','class="row"','',$this->l('30 seconds'));
						
						echo $this->_params->inputTag("limitreview",$this->getParamValue("limitreview", 5),$this->l('Limit reviews on a page'),'class="text_area"','class="row"','');
						echo $this->_params->inputTag("mincharreview",$this->getParamValue("mincharreview", 50),$this->l('Min characters of review content'),'class="text_area"','class="row"','',$this->l('Minimum characters of review content'));

                        echo $this->_params->inputTag("sendtoadmin",$this->getParamValue("sendtoadmin", Configuration::get("PS_SHOP_EMAIL","landofcoder@gmail.com")),$this->l('Send e-mail to admin'),'class="text_area"','class="row"','');
                        echo $this->_params->radioBooleanTag("isgetemail", $yesNoLang,$this->getParamValue("isgetemail",1),$this->l('Get e-mail sent by user'),'class="select-option"','class="row"','','');
                    ?> 
                    </li>    	                   	        
                  </ul>
                </div>
           </fieldset>  
<br/>
  <fieldset>
                <legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Permission Setting'); ?></legend>
                <div class="lof_config_wrrapper clearfix">
                  <ul>
                    <li>
                    <?php
                        
                        //radio
                        echo $this->_params->radioBooleanTag("onlycustomer", $yesNoLang,$this->getParamValue("onlycustomer",0),$this->l('Only those  who bought the product can review it '),'class="select-option"','class="row"','','');
						$orderstate = $this->getParamValue("orderstate", 5);
                        $orderstate = explode(',', $orderstate);
                        //show / hide
                        echo $this->_params->selectTag("orderstate[]",$orderstatus,$orderstate,$this->l('Order state to post review'),'class="inputbox select-group" multiple="multiple"  size="10"','class="row onlycustomer-1"','' );


						echo $this->_params->radioBooleanTag("guestratehelpful", $yesNoLang,$this->getParamValue("guestratehelpful",0),$this->l('Guest can rate helpfulness. '),'class="select-option"','class="row"','','');

						echo $this->_params->radioBooleanTag("guestreport", $yesNoLang,$this->getParamValue("guestreport",0),$this->l('Guest can report abuse '),'class="select-option"','class="row"','','');
						
                        echo $this->_params->radioBooleanTag("customproscons", $yesNoLang,$this->getParamValue("customproscons",1),$this->l('Enable user defined Pros and Cons'),'class="select-option"','class="row"','','');       
						
                        echo $this->_params->radioBooleanTag("adminapprovalitems", $yesNoLang,$this->getParamValue("adminapprovalitems",1),$this->l('Admin approval custom items required '),'class="select-option"','class="row"','','');
						
						echo $this->_params->radioBooleanTag("adminapprovalreview", $yesNoLang,$this->getParamValue("adminapprovalreview",1),$this->l('Admin approval review required '),'class="select-option"','class="row"','','');
						echo $this->_params->radioBooleanTag("antispam", $yesNoLang,$this->getParamValue("antispam",1),$this->l('Anti spam Protection '),'class="select-option"','class="row"','','');
                    ?> 
                    </li>    	                   	        
                  </ul>
                </div>
           </fieldset>  
<br /> 
<fieldset>
                <legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Recent Reviews Setting'); ?></legend>
                <div class="lof_config_wrrapper clearfix">
                  <ul>
                    <li>
					<?php

						//echo $this->_params->inputTag("recent_title",$this->getParamValue("recent_title", "Recent Reviews"),$this->l('Title'),'class="text_area"','class="row"','');
						echo $this->_params->inputTag("limititems",$this->getParamValue("limititems", 5),$this->l('Limit items reviewed(most recently)'),'class="text_area"','class="row"','');
						
                        
                        
					?>
					</li>
					</ul>
				</div>
</fieldset>
<br/>
<input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />  

<br />  
  	<fieldset><legend class="lof-legend"><img src="../img/admin/comment.gif" alt="" title="" /><?php echo $this->l('Information');?></legend>    	
    	<ul>
    	     
             <li>+ <a target="_blank" href="http://landofcoder.com/supports/forum.html?id=100"><?php echo $this->l('Forum support');?></a></li>
             <li>+ <a target="_blank" href="http://www.landofcoder.com/submit-request.html"><?php echo $this->l('Customization/Technical Support Via Email');?>.</a></li>
             
        </ul>
        <br />
        @Copyright: <a href="http://wwww.landofcoder.com">LandOfCoder.com</a>
    </fieldset>
</form>