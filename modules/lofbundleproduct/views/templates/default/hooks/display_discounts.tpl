{if $bundles}
<div class="discount_wrapper">
	<span class="price">{$total}</span>
	<span class="label">{l s="Total Without Discount(s)" mod="lofbundleproduct"}</span>
	<span class="price">{$total_with_discount}</span>
	<span class="label">{l s="Total With Discount(s)"}</span>
	<br/>
	<div class="line_break">&nbsp;</div>
	<span class="label">{l s="Discount(s)" mod="lofbundleproduct"}</span>
	<ul>
		{if $bundles}
			{foreach from=$bundles item='row' name='bundles'}
				<li>{$row.title} : <span class="price_bold">{$row.discount_amount}</span></li>
			{/foreach}
		{/if}
	</ul>
</div>
{/if}