<script type="text/javascript">
	function addBundleToCart(productids){
		$(".lofbundleproduct_"+productids).each(function(index){
			 var self = $(this);
			  setTimeout(function () {
					self.click();
			  }, index*1000);
		});
	}
</script>
<div class="lofbundle_wrapper">
<h3 class="title_block">{l s="Bundle Promotion" mod="lofbundleproduct"}</h3>
<div class="bundle_list">
	<ul>
		{foreach from=$bundles item='row' name='bundles'}
		
		{if $smarty.foreach.bundles.last}
		<li class="last_item">
		{else}
		<li>
		{/if}
			<div class="bundle_item">
				{foreach from=$row.products item='product' name='bundle_products'}
				<div class="bundle_product">
					<a class="product_image" href="{$product.product_link}" title="{$product.product_name|escape:html:'UTF-8'}"><img src="{$link->getImageLink($product.link_rewrite, $product.product_image, 'medium_allinmart')}" height="{$mediumSize.height}" width="{$mediumSize.width}" alt="{$product.product_name|escape:html:'UTF-8'}" /></a>
					<div class="bundle_price">{convertPrice price=$product.product_price}</div>
						<div style="display:none;">
						{if $product.show_price AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}<p class="price_container"></p>{else}<div style="height:21px;"></div>{/if}
						
						{if ((isset($add_prod_display) AND ($add_prod_display == 1))) AND $product.available_for_order AND !isset($restricted_country_mode) AND $product.minimal_quantity == 1 AND $product.customizable != 2 AND !$PS_CATALOG_MODE}
							{if ($product.quantity > 0 OR $product.allow_oosp)}
							<a class="exclusive ajax_add_to_cart_button lofbundleproduct_{$row.id_lofbundleproduct}" rel="ajax_id_product_{$product.product_id}" href="{$link->getPageLink('cart')}?qty=1&amp;id_product={$product.product_id}&amp;token={$static_token}&amp;add" title="{l s='Add to cart' mod='lofbundleproduct'}">{l s='Add to cart' mod='lofbundleproduct'}</a>
							{else}
							<span class="exclusive">{l s='Add to cart' mod='lofbundleproduct'}</span>
							{/if}
						{else}
							<div style="height:23px;"></div>
						{/if}
					</div>
				</div>
				{if !$smarty.foreach.bundle_products.last}
					<div class="plus">+</div>
				{/if}
				{/foreach}
				<div class="lof-addtocart-countdown">
				<div class="deal-clock lof-clock-{$row.id_lofbundleproduct}-detail">
											
											{if $row.js == 'unlimited'}
												<div class="lof-labelexpired">
													{l s='Unlimited' mod='lofdeal'}
												</div>
											{/if}
				</div>
				<div class="add_to_cart">
					<!--<form id="bundle_buy_block" {if $row.isExpired}class="hidden"{/if} action="{$link->getModuleLink('lofbundleproduct','cart',['process' => 'addBundle'])}" method="post"> -->

					<!-- hidden datas -->
					<p class="hidden">
						<input type="hidden" name="token" value="{$static_token}" />
						{foreach from=$row.products item='product' name='bundle_products'}
						<input type="hidden" name="id_product[]" value="{$product.product_id|intval}" class="product_bundle_id" />
						{/foreach}
						<input type="hidden" name="add" value="1" />
						<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
					</p>
					{if !$row.isExpired}
					<p id="add_to_cart_{$row.id_lofbundleproduct}" class="add_to_cart2">
						<span></span>
						<input type="submit" name="Submit"  onClick="return addBundleToCart({$row.id_lofbundleproduct});" value="{l s='Add to cart'}" class="exclusive lofsubmittocart" />
					</p>
					{/if}
				<!-- </form> -->
					<div class="price">
						<div class="bundle_price">{convertPrice price=$row.total_price}</div>
						<div>{if $row.discount_type == 1}<span class="price_bold bundle_price">{convertPrice price=$row.discount_amount}</span>
							{else}<span class="price_bold bundle_price">{$row.discount_amount}%</span>{/if} {l s="(Discount)" mod="lofbundleproduct"}</div>
						<div>{l s="Total: " mod="lofbundleproduct"}<span class="bundle_price">{convertPrice price=$row.total_price_with_discount}</span></div>
					</div>
				</div>
				{if $row.js != 'unlimited'}	
					<script type="text/javascript">
						{literal}
						jQuery(document).ready(function($){{/literal}
							$(".lof-clock-{$row.id_lofbundleproduct}-detail").lofCountDown({literal}{{/literal}
								TargetDate:"{$row.js.month}/{$row.js.day}/{$row.js.year} {$row.js.hour}:{$row.js.minute}:{$row.js.seconds}",
								DisplayFormat:"<div>%%D%% {l s='Days' mod='lofbundleproduct'}</div><div>%%H%% {l s='Hours' mod='lofbundleproduct'}</div><div>%%M%% {l s='Minutes' mod='lofbundleproduct'}</div><div>%%S%% {l s='Seconds' mod='lofbundleproduct'}</div>",
								FinishMessage: "{$row.finish}"
							{literal}
							});
						});
						{/literal}
					</script>
				{/if}
				</div>
				<br class="clear clr"/>
			</div>
		</li>
		{/foreach}
	</ul>
	</div>
</div>