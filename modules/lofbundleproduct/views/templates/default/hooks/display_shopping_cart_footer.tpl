<a href="javascript:;" id="lofbundle_modalbox" title="{$message_title}">
<div class="modalboxContent">
	<div class="lofbundle_wrapper">
		<div class="messages">{$message_before}
			<p>{l s="Order Total: " mod="lofbundleproduct"}<span class="price_bold">{$order_total}</span></p>
			<p>{l s="Order Total After Apply Discount(s): " mod="lofbundleproduct"}<span class="price_bold">{$order_total_discount}</span></p>
			<p><b>{l s="You get discounts when purchase bundle product(s): " mod="lofbundleproduct"}</b></p>
				<ul>
					{if $bundles}
						{foreach from=$bundles item='row' name='bundles'}
							<li>
								{if $show_discount_info && !empty($row.products)}
									<div class="bundle_item">
									{foreach from=$row.products item='product' name='products'}
										<div class="bundle_product">
											<img src="{$link->getImageLink($product.link_rewrite, $product.product_image, 'medium_allinmart')}" height="{$mediumSize.height}" width="{$mediumSize.width}" title="{$product.product_name|escape:html:'UTF-8'}" alt="{$product.product_name|escape:html:'UTF-8'}" />
										</div>
										{if !$smarty.foreach.products.last}
											<div class="plus">+</div>
										{/if}
									{/foreach}
											<div class="equal">=</div>
											<div class="float_left price_bold">{$row.discount_amount}{l s=" discount(s)" mod="lofbundleproduct"}</div>
									</div>
								{else}
									{$row.title} : <span class="price_bold">{$row.discount_amount}</span>
								{/if}
							</li>
						{/foreach}
					{/if}
				</ul>
			{$message_after}</div>
	</div>
</div>
</a>
<script type="text/javascript">
	/* <![CDATA[ */
	$(document).ready(function(){
		jQuery("#lofbundle_modalbox").modalBox({
			localizedStrings:{
					messageCloseWindow: "{$message_title}"
				}
			});
		jQuery("#lofbundle_modalbox").click();
	});
/* ]]> */
</script>