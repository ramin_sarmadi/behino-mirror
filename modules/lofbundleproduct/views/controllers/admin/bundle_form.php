<?php
$id = Tools::getValue('id_product') ? Tools::getValue('id_product') : 0;
//echo "<pre>".print_r($id,1);die;
if ($id) {
    $lofproduct = new Product($id, false, $this->context->language->id);
    $id_category_default = $lofproduct->id_category_default ? $lofproduct->id_category_default : 0;
    $category = new Category($lofproduct->id_category_default, $this->context->language->id);
    ?>
    <script>
        $(document).ready(function () {
            var lofselected_products = new Array();
            var html = "";
            var lofcount_rows = $("#bundle_products").find("tr").length;
            lofcount_rows = lofcount_rows - 2;
            lofcount_rows += 1;
            var my_class = "";
            if (lofcount_rows % 2 == 0) {
                my_class = "alt_row";
            }
            html = "<tr class=\"" + my_class + "\">";
            html += "            <td>";
            html += "    <input type=\"checkbox\" checked=\"checked\" value=\"<?php echo $id ;?>\" name=\"lof_pro[]\">";
            html += "    <input type=\"hidden\" value=\"<?php echo $id ;?>\" name=\"ids_pro[]\">";
            html += "  </td>";
            html += "            <td><?php echo $id ;?></td>";
            html += "            <td><?php echo $lofproduct->name ;?></td>";
            html += "            <td><?php echo $category->name ;?></td>";
            html += "            <td>" + lofcount_rows + "</td>";
            html += "      </tr>";
            $("#bundle_products").append(html);
        })
    </script>
<?php
}
?>
<form id="lofbundleproduct_form" name="lofbundleproduct"
      action="<?php echo $currentIndex . '&id_lofbundleproduct=' . $bundle->id_lofbundleproduct . '&viewlofbundleproduct&token=' . $this->token; ?>"
      method="post">
<div class="lofbundle_wrapper">
<?php
$discounttypes = array("1" => $this->l("Fixed"),
    "2" => $this->l("Percent"));
if (!isset($bundle->active)) {
    $bundle->active = 1;
}
$bundle->id = isset($bundle->id) ? $bundle->id : 0;
$bundle->ordering = isset($bundle->ordering) ? $bundle->ordering : 1;
$bundle->title = isset($bundle->title) ? $bundle->title : array();
$bundle->discount_amount = isset($bundle->discount_amount) ? $bundle->discount_amount : "0";
$bundle->discount_type = isset($bundle->discount_type) ? $bundle->discount_type : "1";
$bundle->from = isset($bundle->from) ? $bundle->from : "";
$bundle->to = isset($bundle->to) ? $bundle->to : "";
$product_edit_link = LofClassBundleproduct::getProductEditLink($id_product);
$divLangName = 'title';
?>

<input name="id_product" value="<?php echo $id; ?>" type="hidden"/>
<fieldset id="bundleproduct">
<legend><?php echo $this->l("Bundle Information"); ?></legend>
<div class="clr clear"></div>
<label><?php echo $this->l("Title"); ?></label>
<?php echo $this->inputTagLang("title", "title", $bundle->title); ?>
<div class="clr clear"></div>
<label><?php echo $this->l("Discount Amount"); ?></label>

<div class="margin-form">
    <input type="text" name="discount_amount" id="discount_amount" value="<?php echo $bundle->discount_amount; ?>"/>
</div>
<div class="clr clear"></div>
<label><?php echo $this->l("Discount Type"); ?></label>

<div class="margin-form">
    <select name="discount_type" id="discount_type">
        <?php
        foreach ($discounttypes as $key => $val) {
            if ($key == $bundle->discount_type) {
                echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
            } else {
                echo '<option value="' . $key . '">' . $val . '</option>';
            }
        }
        $checked1 = ($bundle->active == "1") ? ' checked="checked" ' : '';
        $checked2 = ($bundle->active == "0") ? ' checked="checked" ' : '';
        $this->context->controller->addJqueryUI('ui.datepicker');
        $this->context->controller->addJqueryUI('ui.slider');
        $this->addJs(_PS_JS_DIR_ . 'jquery/plugins/timepicker/jquery-ui-timepicker-addon.js');
        $this->context->controller->addCss(__PS_BASE_URI__ . "modules/" . $this->moduleName . "/assets/admin/form.css");
        $this->context->controller->addJqueryPlugin('autocomplete');
        ?>
    </select>
</div>
<div class="clr clear"></div>
<label><?php echo $this->l("Active"); ?></label>

<div class="margin-form">
    <input type="radio" name="active" id="active_on" value="1" <?php echo $checked1; ?>/> <label for="active_on"
                                                                                                 class="t">
        <img title="Enabled" alt="Enabled" src="../img/admin/enabled.gif">
    </label>
    <input type="radio" name="active" id="active_off" value="0" <?php echo $checked2; ?>/> <label for="active_off"
                                                                                                  class="t">
        <img title="Disabled" alt="Disabled" src="../img/admin/disabled.gif">
    </label>
</div>
<div class="clr clear"></div>
<label><?php echo $this->l("From"); ?></label>

<div class="margin-form">
    <input type="text" data-hex="true" class="datepicker" name="from" value="<?php echo $bundle->from; ?>"/>
</div>
<div class="clr clear"></div>
<label><?php echo $this->l("To"); ?></label>

<div class="margin-form">
    <input type="text" data-hex="true" class="datepicker" name="to" value="<?php echo $bundle->to; ?>"/>
</div>
<div class="clr clear"></div>
<script type="text/javascript">
    $(document).ready(function () {
        if ($(".datepicker").length > 0)
            $(".datepicker").datetimepicker({
                prevText: "",
                nextText: "",
                dateFormat: "yy-mm-dd"
            });
        $("#desc-lofbundleproduct-save").click(function () {
            $("#action").val("save");
            $("#lofbundleproduct_form").submit();
        });
        $("#desc-lofbundleproduct-save-and-new").click(function () {
            $("#action").val("save_and_new");
            $("#lofbundleproduct_form").submit();
        });
        $("#desc-lofbundleproduct-save-and-stay").click(function () {
            $("#action").val("save_and_stay");
            $("#lofbundleproduct_form").submit();
        });
        $("#desc-lofbundleproduct-duplicate").click(function () {
            $("#action").val("duplicate");
            $("#lofbundleproduct_form").submit();
        });
    });
</script>
<label><?php echo $this->l("Search for a Product"); ?></label>

<div class="margin-form">
    <input class="search_query" type="search" size="40" style="padding:2px 4px" id="search_product" name="search_query"
           placeholder="<?php echo $this->l('Search'); ?>" value=""/>

    <p><?php echo $this->l('Search a product by tapping the first letters of his/her name.'); ?></p>
    <script type="text/javascript">
        // <![CDATA[
        $("document").ready(function () {
            var selected_products = new Array();

            function addNewRow(data) {
                if (typeof(selected_products[data.id_product]) == "undefined") {
                    var html = "";
                    var count_rows = $("#bundle_products").find("tr").length;
                    count_rows = count_rows - 2;
                    count_rows += 1;
                    var my_class = "";
                    if (count_rows % 2 == 0) {
                        my_class = "alt_row";
                    }
                    html = "<tr class=\"" + my_class + "\">";
                    html += "            <td>";
                    html += "    <input type=\"checkbox\" checked=\"checked\" value=\"" + data.id_product + "\" name=\"lof_pro[]\">";
                    html += "    <input type=\"hidden\" value=\"" + data.id_product + "\" name=\"ids_pro[]\">";
                    html += "  </td>";
                    html += "            <td>" + data.id_product + "</td>";
                    html += "            <td>" + data.pname + "</td>";
                    html += "            <td>" + data.cname + "</td>";
                    html += "            <td>" + count_rows + "</td>";
                    html += "      </tr>";
                    $("#bundle_products").append(html);
                    selected_products[data.id_product] = data.id_product;
                }
            }

            var AjaxProgrees = '';
            var searchBox = $('#search_product');
            searchBox.keyup(function () {
                if (AjaxProgrees) {
                    AjaxProgrees.abort();
                }
                var lengthOfSearch = searchBox.val().length;
                if (lengthOfSearch > 1) {
                    $(".products-append, .video-append, .article-append").empty();
                    $('.search-results').fadeIn();
                    AjaxProgrees = $.ajax({
                        url: 'http://localhost/ibartar2/index.php?controller=search',
                        data: {
                            ajaxSearch: '1',
                            id_lang: '1',
                            limit: 20,
                            q: searchBox.val()
                        },
                        type: 'POST',
                        dataType: "json",
                        success: function (data) {
                            $(".products-append, .video-append, .article-append").empty();
                            if (data.resultErrProduct != 'Not Find Product') {
                                for (var k in data.resultProduct) {
                                    var pName = data.resultProduct[k]['pname'];
                                    pName = pName.substring(0, 40);
                                    var priceContent = '';
                                    $('.products-append').append(
                                        '<li class="Psearch-li">'
                                            + '<a href="'
                                            + data.resultProduct[k]['product_link']
                                            + ' ">'
                                            + '<span class="qsearch-price-name"> '
                                            + '<span class="qsearch-name">'
                                            + pName
                                            + '</span>'
                                            + priceContent
                                            + '</a>'
                                            + '</li>'
                                    );
                                }
                            }
                            else {
                               $('.products-append ').html('محصولی وجود ندارد.');
                            }
                        }
                    });
                }
            });
        });
    </script>
</div>
<div style="display: none" class="search-results">
    <div class="search-products">
        <h3 class="search-h3">محصولات</h3>
        <ul class="products-append">
        </ul>
    </div>
    <div class="search-video">
        <h3 class="search-h3">ویدئو ها</h3>
        <ul class="video-append">
        </ul>
        <h3 class="search-h3">اخبار</h3>
        <ul class="article-append">
        </ul>
    </div>
</div>
<div class="clr clear"></div>

<?php
$_pagination = array(5, 20, 50, 100, 300, 500, 1000);
$idproduct = NULL;
$product_name = Tools::getValue('product_name') ? Tools::getValue('product_name') : NULL;

if (empty($product_name)) {
    $product_name = isset($_POST["product_name"]) ? $_POST["product_name"] : NULL;
}

$search = array('id_product' => $idproduct,
    'product_name' => $product_name
);

$_listTotal = LofClassBundleproduct::getListProducts($id_product, $bundle->id, $cookie->id_lang, 1, 0, 0, "", "", $search);

$totalPages = ceil($_listTotal / Tools::getValue('pagination', $_pagination[1]));
if (!$totalPages) $totalPages = 1;

/* Determine current page number */
$page = (int)(Tools::getValue('lofsubmitFilter' . $this->table, 1));
if (!$page) $page = 1;
/* Choose number of results per page */
$selectedPagination = Tools::getValue('pagination') ? Tools::getValue('pagination') : NULL;


$orderBy = isset($_GET[$this->table . 'Orderby1']) ? $_GET[$this->table . 'Orderby1'] : Null;
$orderWay = isset($_GET[$this->table . 'Orderway1']) ? $_GET[$this->table . 'Orderway1'] : Null;
if (empty($orderBy))
    $orderBy = $cookie->__get($this->table . 'Orderby1') ? $cookie->__get($this->table . 'Orderby1') : NULL;
if (empty($orderWay))
    $orderWay = $cookie->__get($this->table . 'Orderway1') ? $cookie->__get($this->table . 'Orderway1') : NULL;

$products = LofClassBundleproduct::getListProducts($id_product, $bundle->id, $cookie->id_lang, 0, ($page - 1) * $selectedPagination, $selectedPagination, $orderBy, $orderWay, $search);
?>
<input type="hidden" id="lofsubmitFilter<?php echo $this->table; ?>" name="lofsubmitFilter<?php echo $this->table; ?>"
       value="0">
<?php
if ($page > 1) {
    echo '<input type="image" src="../img/admin/list-prev2.gif" onclick="getE(\'lofsubmitFilter' . $this->table . '\').value=1"/>&nbsp;
                <input type="image" src="../img/admin/list-prev.gif" onclick="getE(\'lofsubmitFilter' . $this->table . '\').value=' . ($page - 1) . '"/>&nbsp;';
}
?>
<?php echo $this->l('Page') . ' <b>' . $page . '</b> / ' . $totalPages . '&nbsp;'; ?>
<?php
if ($page < $totalPages) {
    echo ' <input type="image" src="../img/admin/list-next.gif" onclick="getE(\'lofsubmitFilter' . $this->table . '\').value=' . ($page + 1) . '"/>&nbsp;
                <input type="image" src="../img/admin/list-next2.gif" onclick="getE(\'lofsubmitFilter' . $this->table . '\').value=' . $totalPages . '"/>&nbsp;';
}
?>
<?php echo $this->l('Display'); ?>
<select name="pagination">
    <?php foreach ($_pagination AS $value): ?>
        <option
            value="<?php echo (int)($value); ?>"<?php echo($selectedPagination == $value ? ' selected="selected"' : (($selectedPagination == NULL && $value == $this->_pagination[1]) ? ' selected="selected2"' : '')); ?> ><?php echo (int)($value); ?></option>
    <?php endforeach; ?>
</select> / <?php echo (int)($_listTotal) . ' ' . $this->l('result(s)'); ?>
<span>
			     <input type="submit" name="lofsubmitReset<?php echo $this->table; ?>"
                        value="<?php echo $this->l('Reset'); ?>" class="button"/>
		         <input type="submit" name="lofsubmitFilter<?php echo $this->table; ?>"
                        id="lofsubmitFilterButton_<?php echo $this->table; ?>" value="<?php echo $this->l('Filter'); ?>"
                        class="button"/>
		     </span>
<br/><br/>
<table id="bundle_products" border="0" cellpadding="0" cellspacing="0" class="table" style="width: 700px;">
    <tr class="nodrag nodrop">
        <th><input type="checkbox" onclick="checkDelBoxes(this.form, 'lof_pro[]', this.checked)"></th>
        <th>
            <?php echo $this->l('ID'); ?>
            <br/>
            <a href="<?php echo $currentIndex . '&' . $this->identifier . '=' . $bundle->id . '&updatelofbundleproduct&' . $this->table . 'Orderby=' . urlencode('id_product') . '&' . $this->table . 'Orderway1=desc&token=' . $token; ?>"><img
                    border="0"
                    src="../img/admin/down<?php echo((isset($orderBy) AND ('id_product' == $orderBy) AND ($orderWay == 'desc')) ? '_d' : ''); ?>.gif"/></a>
            <a href="<?php echo $currentIndex . '&' . $this->identifier . '=' . $bundle->id . '&updatelofbundleproduct&' . $this->table . 'Orderby1=' . urlencode('id_product') . '&' . $this->table . 'Orderway1=asc&token=' . $token; ?>"><img
                    border="0"
                    src="../img/admin/up<?php echo((isset($orderBy) AND ('id_product' == $orderBy) AND ($orderWay == 'asc')) ? '_d' : ''); ?>.gif"/></a>
        </th>
        <th>
            <?php echo $this->l('Product Name'); ?>
            <br/>
            <a href="<?php echo $currentIndex . '&' . $this->identifier . '=' . $bundle->id . '&updatelofbundleproduct&' . $this->table . 'Orderby1=' . urlencode('name') . '&' . $this->table . 'Orderway1=desc&token=' . $token; ?>"><img
                    border="0"
                    src="../img/admin/down<?php echo((isset($orderBy) AND ('name' == $orderBy) AND ($orderWay == 'desc')) ? '_d' : ''); ?>.gif"/></a>
            <a href="<?php echo $currentIndex . '&' . $this->identifier . '=' . $bundle->id . '&updatelofbundleproduct&' . $this->table . 'Orderby1=' . urlencode('name') . '&' . $this->table . 'Orderway1=asc&token=' . $token; ?>"><img
                    border="0"
                    src="../img/admin/up<?php echo((isset($orderBy) AND ('name' == $orderBy) AND ($orderWay == 'asc')) ? '_d' : ''); ?>.gif"/></a>
        </th>
        <th>
            <?php echo $this->l('Category Name'); ?>
            <br/>
            <a href="<?php echo $currentIndex . '&' . $this->identifier . '=' . $bundle->id . '&updatelofbundleproduct&' . $this->table . 'Orderby1=' . urlencode('id_category') . '&' . $this->table . 'Orderway1=desc&token=' . $token; ?>"><img
                    border="0"
                    src="../img/admin/down<?php echo((isset($orderBy) AND ('name' == $orderBy) AND ($orderWay == 'desc')) ? '_d' : ''); ?>.gif"/></a>
            <a href="<?php echo $currentIndex . '&' . $this->identifier . '=' . $bundle->id . '&updatelofbundleproduct&' . $this->table . 'Orderby1=' . urlencode('id_category') . '&' . $this->table . 'Orderway1=asc&token=' . $token; ?>"><img
                    border="0"
                    src="../img/admin/up<?php echo((isset($orderBy) AND ('name' == $orderBy) AND ($orderWay == 'asc')) ? '_d' : ''); ?>.gif"/></a>
        </th>
        <th>
            <?php echo $this->l('No'); ?>
        </th>
    </tr>
    <tr class="nodrag nodrop" style="height: 35px;">
        <td>--</td>
        <td>
        </td>
        <td>
            <input type="text" style="width: 160px;" value="" name="product_name">
        </td>
        <td>
            <input type="text" style="width: 160px;" value="" name="category_name">
        </td>
        <td style="width:50px"></td>
    </tr>

    <?php
    $irow = 0;
    $i = 1;
    if (!empty($products)) {
        foreach ($products AS $product) {
            ?>
            <tr class="<?php echo($irow++ % 2 ? 'alt_row' : ''); ?>">
                <td>
                    <input type="checkbox" name="lof_pro[]"
                           value="<?php echo $product['id_product']; ?>" <?php echo(($product['lofactive'] == 1) ? 'checked="checked"' : ''); ?> >
                    <input type="hidden" name="ids_pro[]" value="<?php echo $product['id_product']; ?>"/>
                </td>
                <td><?php echo $product['id_product']; ?></td>
                <td><?php echo $product['name']; ?></td>
                <td><?php echo $product['category_name']; ?></td>
                <td><?php echo $i; ?></td>
            </tr>
            <?php
            $i++;
        }
    }

    ?>
</table>
<br/>
<input type="hidden" name="action" id="action" value=""/>
<input type="hidden" name="id_lofbundleproduct" value="<?php echo $bundle->id; ?>"/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</fieldset>
</div>
</form>
