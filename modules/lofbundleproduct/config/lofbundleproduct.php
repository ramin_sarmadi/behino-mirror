<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
?>
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.css";?>" type="text/css" media="screen" charset="utf-8" />
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/jquery-ui.css";?>" type="text/css" media="screen" charset="utf-8" />


<script type="text/javascript" src="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.js";?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    //When page loads...
    $(".tab_content").hide(); //Hide all content
    $("ul.tabs li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content

    //On Click Event
    $("ul.tabs li").click(function() {

    	$("ul.tabs li").removeClass("active"); //Remove any "active" class
    	$(this).addClass("active"); //Add "active" class to selected tab
    	$(".tab_content").hide(); //Hide all tab content

    	var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
    	$(activeTab).fadeIn(); //Fade in the active ID content
    	return false;
    });

  });
</script>
<?php
    $yesNoLang = array("0"=>$this->l('No'),"1"=>$this->l('Yes'));
?>
<h3><?php echo $this->l('Lof Bundle Promotion Configuration');?></h3>
<form action="<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>" enctype="multipart/form-data" method="post" id="lofform">
<input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
	<fieldset>
		<legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Global Setting'); ?></legend>
		<div class="lof_config_wrrapper clearfix">
		  <ul>
			<li>
			<?php
				echo $this->_params->selectTag("module_theme",$themes,$this->getParamValue("module_theme","default"),$this->l('Theme - Layout'),'class="inputbox select-group"','class="row"','' );
				echo $this->_params->radioBooleanTag("enable_module", $yesNoLang,$this->getParamValue("enable_module",1),$this->l('Enable Module'),'class="select-option"','class="row"','',$this->l(''));
			?>
			</li>
		  </ul>
		</div>
   </fieldset>
<br />
<fieldset>
		<legend class="lof-legend"><img src="../img/admin/contact.gif" /><?php echo $this->l('Display On Shopping Cart Setting'); ?></legend>
		<div class="lof_config_wrrapper clearfix">
		  <ul>
			<li>
			<?php
          $notify_types = array("popup"=>$this->l('Popup'),
                                "block"=>$this->l('Block'));
          echo $this->_params->selectTag("notify_stype",$notify_types,$this->getParamValue("notify_stype","block"),$this->l('Discount Notify Style'),'class="inputbox select-group"','class="row"','',$this->l("When user get discount, a notification will show on popup or block after shopping cart block.") );
          echo $this->_params->radioBooleanTag("show_info", $yesNoLang,$this->getParamValue("show_info",1),$this->l('Show Discount Info'),'class="select-option"','class="row"','',$this->l(''));
          echo $this->_params->inputTag("msg_title",$this->getParamValue("msg_title_".$this->_defaultFormLanguage),$this->l('Message Title'),'class="text_area"',' length="60" class="row"','',$this->l("<br/>For example: You get %s discount!"), true);
                echo $this->_params->textTag("msg_before", $this->getParamValue("msg_before_".$this->_defaultFormLanguage),$this->l('Message Before'),'class="text_area"','class="row"','','', true);
                echo $this->_params->textTag("msg_after",$this->getParamValue("msg_after_".$this->_defaultFormLanguage),$this->l('Message After'),'class="text_area"','class="row"','','', true);
			?>
			</li>
		</ul>
</fieldset>
<br/>
   <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
</form>
