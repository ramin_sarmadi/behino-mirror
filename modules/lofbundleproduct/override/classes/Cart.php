<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Cart extends CartCore
{

	public function __construct($id = null, $id_lang = null)
	{
		parent::__construct($id, $id_lang);


	}
	/**
	* This function returns the total cart amount
	*
	* Possible values for $type:
	* Cart::ONLY_PRODUCTS
	* Cart::ONLY_DISCOUNTS
	* Cart::BOTH
	* Cart::BOTH_WITHOUT_SHIPPING
	* Cart::ONLY_SHIPPING
	* Cart::ONLY_WRAPPING
	* Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING
	* Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING
	*
	* @param boolean $withTaxes With or without taxes
	* @param integer $type Total type
	* @param boolean $use_cache Allow using cache of the method CartRule::getContextualValue
	* @return float Order total
	*/
	public function getOrderTotal($with_taxes = true, $type = Cart::BOTH, $products = null, $id_carrier = null, $use_cache = true)
	{
		$total = parent::getOrderTotal($with_taxes, $type, $products, $id_carrier, $use_cache);
	    if($type == Cart::BOTH){
	        $params = array("order_total"=>$total,
	                        "with_taxes"=>$with_taxes,
	                        "type"=>$type,
	                        "products"=>$products,
	                        "id_carrier"=>$id_carrier,
	                        "use_cache"=>$use_cache);
        $total2 = Hook::exec('actionAfterGetTotal',$params,Module::getModuleIdByName('lofbundleproduct'));
        $total = !empty($total2)?$total2:$total;
    }
		return $total;
	}
	
}
