/*
Navicat MySQL Data Transfer

Source Server         : thuanlq
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : db_prestashop1541

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2013-06-19 16:11:35
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `ps_lofbundleproduct`
-- ----------------------------
DROP TABLE IF EXISTS `ps_lofbundleproduct`;
CREATE TABLE `ps_lofbundleproduct` (
  `id_lofbundleproduct` int(11) NOT NULL AUTO_INCREMENT,
  `discount_amount` float(9,3) DEFAULT NULL,
  `discount_type` tinyint(4) DEFAULT '1',
  `products` varchar(200) DEFAULT NULL,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `id_shop_default` int(11) DEFAULT '1',
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id_lofbundleproduct`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ps_lofbundleproduct
-- ----------------------------
-- ----------------------------
-- Table structure for `ps_lofbundleproduct_shop`
-- ----------------------------
DROP TABLE IF EXISTS `ps_lofbundleproduct_shop`;
CREATE TABLE `ps_lofbundleproduct_shop` (
  `id_lofbundleproduct` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL DEFAULT '1',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lofbundleproduct`,`id_shop`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ps_lofbundleproduct_shop
-- ----------------------------

-- ----------------------------

DROP TABLE IF EXISTS `ps_lofbundleproduct_lang`;
CREATE TABLE `ps_lofbundleproduct_lang` (
  `id_lofbundleproduct` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL DEFAULT '1',
  `title` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Table structure for `ps_lofbundleproduct_product`
-- ----------------------------
DROP TABLE IF EXISTS `ps_lofbundleproduct_product`;
CREATE TABLE `ps_lofbundleproduct_product` (
	`id_lofbundleproduct_product` int(11) NOT NULL AUTO_INCREMENT,
	`id_lofbundleproduct` int(11) NOT NULL,
	`id_product` int(11) NOT NULL,
  PRIMARY KEY (`id_lofbundleproduct_product`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;