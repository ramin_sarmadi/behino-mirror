<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads,
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com>
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */


if (!defined('_CAN_LOAD_FILES_'))
	exit;
if(!class_exists('LofClassBundleproduct')) {
	/**
	 * Class LofClassBundleproduct
	 * Discount type: 1=Fixed value | 2=Percent
	 */
	class LofClassBundleproduct extends ObjectModel{
		    public $id_lofbundleproduct;
		    public $discount_amount;
        public $discount_type;
        public $title;
        public $products;
		    public $from;
		    public $to;
        public $position;
        public $active;

		public static $definition = array(
			'table' => 'lofbundleproduct',
			'primary' => 'id_lofbundleproduct',
			'multilang' => true,
			'fields' => array(
				'discount_amount' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
				'discount_type' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
				'title' => 				array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString', 'size' => 200),
				'products' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 200),
				'active' => 				array('type' => self::TYPE_INT, 'validate' => 'isBool'),
				'from' => 			array('type' => self::TYPE_DATE, 'validate' => 'isString'),
				'to' => 			array('type' => self::TYPE_DATE, 'validate' => 'isString')
			)
		);


		public function __construct($id = NULL, $id_lang = NULL, $id_shop = NULL){
			parent::__construct($id, $id_lang, $id_shop);
		}

		public function add($autodate = true, $null_values = false)
		{
			$context = Context::getContext();
			$id_shop = $context->shop->id;
			$res = parent::add($autodate, $null_values);
				$position = $this->getLastPosition($id_shop);
				if (!$position)
					$position = 1;
				$this->addPosition($position, $id_shop);
			return $res;
		}

		public function delete()
		{
			$res = true;
			$res &= Db::getInstance()->execute('
				DELETE FROM `'._DB_PREFIX_.'lofbundleproduct_shop`
				WHERE `id_lofbundleproduct` = '.(int)$this->id
				);
			$res &= $this->deleteItemProduct($this->id);
			$res &= parent::delete();
			$res &= Db::getInstance()->execute('
				DELETE FROM `'._DB_PREFIX_.'lofbundleproduct_lang`
				WHERE `id_lofbundleproduct` = '.(int)$this->id
			  );
		return $res;
		}
    public static function getBundlesByIds($id_bundles = array(),$id_lang=NULL){
        global $cookie;
        if(!empty($id_bundles)){
           $sql = "SELECT * FROM "._DB_PREFIX_."lofbundleproduct  hs
					LEFT JOIN "._DB_PREFIX_."lofbundleproduct_lang pl ON hs.id_lofbundleproduct = pl.id_lofbundleproduct
					WHERE hs.id_lofbundleproduct IN(".implode(",",$id_bundles).") AND pl.id_lang = ".($id_lang)."
					";
           return Db::getInstance()->ExecuteS($sql);
        }
        return false;
    }
		/**
		* select products and bundle products with discount
		*/
    public static function getBundleProducts($id_product = 0){
			global $cookie, $smarty;
			$module = new lofbundleproduct();
			$context = Context::getContext();
			$now = date('Y-m-d H:i:s');
			$finish = $module->l('Expired');
            if($id_product == 0) return false;
            $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
            $_join = "";
	        if (Shop::getContext() == Shop::CONTEXT_SHOP)
				$_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` AS bps ON (bp.`id_lofbundleproduct` = bps.`id_lofbundleproduct` AND bps.id_shop = '.(int)Context::getContext()->shop->id.') ';
			else
			    $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` AS bps ON (bp.`id_lofbundleproduct` = bps.`id_lofbundleproduct` AND bps.id_shop = bp.id_shop_default) ';
        $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_lang` AS bpl ON bp.`id_lofbundleproduct` = bpl.`id_lofbundleproduct` ';
		$_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_product` AS hss ON bp.`id_lofbundleproduct` = hss.`id_lofbundleproduct` ';
			// we add restriction for shop
			$_where = "";
			$id_lang = (int)Context::getContext()->language->id;
			if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
				$_where = ' AND bps.`id_shop` = '.(int)Context::getContext()->shop->id;
				$_where .= ' AND	(((bp.`from` = \'0000-00-00 00:00:00\' OR \''.$now.'\' >= bp.`from`) AND (bp.`to` = \'0000-00-00 00:00:00\' OR \''.$now.'\' <= bp.`to`))) ';
				$order = 'ORDER BY bps.`position` ASC';
            $sql = 'SELECT DISTINCT bp.`id_lofbundleproduct`,bp.*,bpl.title FROM '._DB_PREFIX_.'lofbundleproduct bp

            		'.$_join.'
            		WHERE bp.active = 1 AND bpl.id_lang='.$id_lang.' AND hss.id_product ='.$id_product.$_where.$order;
            $result = Db::getInstance()->ExecuteS($sql);
			
            if(!empty($result)){
            	$product = new Product($id_product, true,$id_lang);

            	foreach($result as $k=>&$item){

            		$total_price = floatval($product->price);
            		if(!empty($item["products"])){
            			$products = explode(",",$item["products"]);
						$item["products"] = array();
            			if($products){
            				$arr_ids = array();
							$total_price = 0;
            				foreach($products as $val){
            					$val = (int)$val;
            					if($val){
            						$arr_ids[] = $val;
            						$tmpp = new Product($val, true,$id_lang);
            						$total_price += floatval($tmpp->price);
            						$item_p = array();
            						$item_p["product_id"] 			= $val;
            						$item_p["product_price"] 		= $tmpp->price;
            						$item_p["product_name"] 		= $tmpp->name;
            						$item_p["link_rewrite"] 		= $tmpp->link_rewrite;
            						$item_p["product_image"] 		= $tmpp->getCover($val);
            						$item_p["product_image"] 		= isset($item_p["product_image"]["id_image"])?$item_p["product_image"]["id_image"]:0;
            						$item_p["product_link"] 		= $tmpp->getLink();
									$item_p["show_price"] 			= $tmpp->show_price;
									$item_p["available_for_order"] 	= $tmpp->available_for_order;
									$item_p["minimal_quantity"] 	= $tmpp->minimal_quantity;
									$item_p["customizable"] 		= $tmpp->customizable;
									$item_p["quantity"] 			= $tmpp->quantity;
									$item_p["customizable"] 		= $tmpp->customizable;
            						$item["products"][] 			= $item_p;
            					}

            				}
            				$item["id_products"] = implode(",", $arr_ids);
            			}
            			$item["isExpired"] = self::checkExpired($item["from"], $item["to"]);
            			$item["total_price"] = $total_price;
            			$item["total_price_with_discount"] = self::getPriceWithDiscount($total_price, $item["discount_amount"], $item["discount_type"]);
            			if($item["discount_type"] == 2)
            				$item["discount_amount"] = (int)$item["discount_amount"];
						$time = false;
						//$row2['specific_prices'] = self::getSpecificPriceById($row['id_specific_price']);
						if(isset($item['from']) && $item['from'] > $now){
							$time = strtotime($item['from']);
							$item['finish'] 	= $finish;
							$item['check_status'] = 0;
							$item['lofdate'] = Tools::displayDate($item['from'], $context->cookie->id_lang);
						}elseif(isset($item['to']) && $item['to'] > $now){
							$time = strtotime($item['to']);
							$item['finish'] 	= $finish;
							$item['check_status'] 	= 1;
							$item['lofdate'] = Tools::displayDate($item['to'], $context->cookie->id_lang);
						}elseif($item['to'] == '0000-00-00 00:00:00'){
							$item['js'] 	= 'unlimited';
							$item['finish'] 	= $module->l('Unlimited');
							$item['check_status'] 	= 1;
							$item['lofdate'] = $module->l("Unlimited");
						}else{
							$time = strtotime($item['to']);
							$item['finish'] 	= $finish;
							$item['check_status'] 	= 2;
							$item['lofdate'] = Tools::displayDate($item['from'], $context->cookie->id_lang);
						}
						if($time){
							$item['js'] 	= array(
								'month' => date('m',$time),
								'day' => date('d',$time),
								'year' => date('Y',$time),
								'hour' => date('H',$time),
								'minute' => date('i',$time),
								'seconds' => date('s',$time)
							);
						}				
            		}else{
            			unset($result[$k]);
            		}
            	}
            	return $result;
            }
            return false;

        }
		
		public static function getListBundleProducts(){
			global $cookie, $smarty;
			$module = new lofbundleproduct();
			$context = Context::getContext();
			$now = date('Y-m-d H:i:s');
			$finish = $module->l('Expired');
            $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
            $_join = "";
	        if (Shop::getContext() == Shop::CONTEXT_SHOP)
				$_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` AS bps ON (bp.`id_lofbundleproduct` = bps.`id_lofbundleproduct` AND bps.id_shop = '.(int)Context::getContext()->shop->id.') ';
			else
			    $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` AS bps ON (bp.`id_lofbundleproduct` = bps.`id_lofbundleproduct` AND bps.id_shop = bp.id_shop_default) ';
        $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_lang` AS bpl ON bp.`id_lofbundleproduct` = bpl.`id_lofbundleproduct` ';
			// we add restriction for shop
			$_where = "";
			$id_lang = (int)Context::getContext()->language->id;
			$order = 'ORDER BY bps.`position` ASC';
			if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
				$_where = ' AND bps.`id_shop` = '.(int)Context::getContext()->shop->id;
				$_where .= ' AND	(((bp.`from` = \'0000-00-00 00:00:00\' OR \''.$now.'\' >= bp.`from`) AND (bp.`to` = \'0000-00-00 00:00:00\' OR \''.$now.'\' <= bp.`to`))) ';
            $sql = 'SELECT DISTINCT bp.`id_lofbundleproduct`,bp.*,bpl.title FROM '._DB_PREFIX_.'lofbundleproduct bp

            		'.$_join.'
            		WHERE bp.active = 1  AND bpl.id_lang='.$id_lang.$_where.$order
					;
            $result = Db::getInstance()->ExecuteS($sql);
            if(!empty($result)){
            	foreach($result as $k=>&$item){
            		if(!empty($item["products"])){
            			$products = explode(",",$item["products"]);
						$item["products"] = array();
            			if($products){
            				$arr_ids = array();
							$total_price = 0;
            				foreach($products as $val){
            					$val = (int)$val;
            					if($val){
            						$arr_ids[] = $val;
            						$tmpp = new Product($val, true,$id_lang);
            						$total_price += floatval($tmpp->price);
            						$item_p = array();
            						$item_p["product_id"] 			= $val;
            						$item_p["product_price"] 		= $tmpp->price;
            						$item_p["product_name"] 		= $tmpp->name;
            						$item_p["link_rewrite"] 		= $tmpp->link_rewrite;
            						$item_p["product_image"] 		= $tmpp->getCover($val);
            						$item_p["product_image"] 		= isset($item_p["product_image"]["id_image"])?$item_p["product_image"]["id_image"]:0;
            						$item_p["product_link"] 		= $tmpp->getLink();
									$item_p["show_price"] 			= $tmpp->show_price;
									$item_p["available_for_order"] 	= $tmpp->available_for_order;
									$item_p["minimal_quantity"] 	= $tmpp->minimal_quantity;
									$item_p["customizable"] 		= $tmpp->customizable;
									$item_p["quantity"] 			= $tmpp->quantity;
									$item_p["customizable"] 		= $tmpp->customizable;
            						$item["products"][] 			= $item_p;
            					}

            				}
            				$item["id_products"] = implode(",", $arr_ids);
            			}
            			$item["isExpired"] = self::checkExpired($item["from"], $item["to"]);
            			$item["total_price"] = $total_price;
            			$item["total_price_with_discount"] = self::getPriceWithDiscount($total_price, $item["discount_amount"], $item["discount_type"]);
            			if($item["discount_type"] == 2)
            				$item["discount_amount"] = (int)$item["discount_amount"];
						$time = false;
						//$row2['specific_prices'] = self::getSpecificPriceById($row['id_specific_price']);
						if(isset($item['from']) && $item['from'] > $now){
							$time = strtotime($item['from']);
							$item['finish'] 	= $finish;
							$item['check_status'] = 0;
							$item['lofdate'] = Tools::displayDate($item['from'], $context->cookie->id_lang);
						}elseif(isset($item['to']) && $item['to'] > $now){
							$time = strtotime($item['to']);
							$item['finish'] 	= $finish;
							$item['check_status'] 	= 1;
							$item['lofdate'] = Tools::displayDate($item['to'], $context->cookie->id_lang);
						}elseif($item['to'] == '0000-00-00 00:00:00'){
							$item['js'] 	= 'unlimited';
							$item['finish'] 	= $module->l('Unlimited');
							$item['check_status'] 	= 1;
							$item['lofdate'] = $module->l("Unlimited");
						}else{
							$time = strtotime($item['to']);
							$item['finish'] 	= $finish;
							$item['check_status'] 	= 2;
							$item['lofdate'] = Tools::displayDate($item['from'], $context->cookie->id_lang);
						}
						if($time){
							$item['js'] 	= array(
								'month' => date('m',$time),
								'day' => date('d',$time),
								'year' => date('Y',$time),
								'hour' => date('H',$time),
								'minute' => date('i',$time),
								'seconds' => date('s',$time)
							);
						}						
            		}else{
            			unset($result[$k]);
            		}
            	}
		
            	return $result;
            }
            return false;

        }	
		
      public static function addBundleToOrder($products = array(),$order_id = 0){
          if(count($products) > 1){
              $product_ids = array();
              foreach($products as $product){
                  $product_ids[] = isset($product["product_id"])?$product["product_id"]:$product["id_product"];
              }
              asort($product_ids);
              $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
              $_join = "";
              if (Shop::getContext() == Shop::CONTEXT_SHOP)
                  $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` AS bps ON (bp.`id_lofbundleproduct` = bps.`id_lofbundleproduct` AND bps.id_shop = '.(int)Context::getContext()->shop->id.') ';
              else
                  $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` AS bps ON (bp.`id_lofbundleproduct` = bps.`id_lofbundleproduct` AND bps.id_shop = bp.id_shop_default) ';
              // we add restriction for shop
              $_where = "";
              $id_lang = (int)Context::getContext()->language->id;
              if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
                  $_where = ' AND bps.`id_shop` = '.(int)Context::getContext()->shop->id;

              $sql = '
                            SELECT bp.*
                             FROM '._DB_PREFIX_.'lofbundleproduct bp
                             '.$_join.'
                             WHERE bp.active = 1 AND bp.id_product IN ('.implode(",",$product_ids).') AND (bp.products IS NOT NULL OR bp.products <> "") '.$_where.'
                             ORDER BY bp.id_lofbundleproduct DESC
                        ';

              $bundles = Db::getInstance()->ExecuteS($sql);
              $discounts = array();
              $key_array = array();
              if(!empty($bundles)){
                  foreach($bundles as $row){
                      if(!self::checkExpired($row["from"], $row["to"])){
                          $tmp_products = explode(",",$row["products"]);
                          if(!empty($tmp_products)){
                              $check = true;

                              foreach($tmp_products as $val){
                                  if(!in_array($val, $product_ids)){
                                      $check = false;
                                      break;
                                  }
                              }
                              if($check){
                                  $tmp_products[] = $row["id_product"];
                                  asort($tmp_products);
                                  $tmp_key = implode(",",$tmp_products);
                                  $discounts[$tmp_key] = $row;
                                  $key_array[$row["id_product"]][] = $tmp_key;
                              }
                          }
                      }
                  }
                  if(!empty($discounts)){
                      // d($cookie);
                      $tmpdiscounts = array();
                      $tmpbundles = array();
                      foreach($product_ids as $val1){
                          $check = array();
                          $check[$val1] = $val1;
                          $tmpkey = "";
                          foreach($product_ids as $val2){
                              if($val2 != $val1){
                                  $check[$val2] = $val2;
                                  asort($check);
                                  $tmp = implode(",", $check);
                                  if(isset($discounts[$tmp])){
                                      $tmpkey = $tmp;
                                  }
                              }
                          }
                          if(isset($discounts[$tmpkey]) && !isset($tmpdiscounts[$tmpkey])){
                              $tmpdiscounts[$tmpkey] = array();
                              $tmpdiscounts[$tmpkey]["id"] = $discounts[$tmpkey]["id_lofbundleproduct"];
                              $tmpdiscounts[$tmpkey]["amount"] = $discounts[$tmpkey]["discount_amount"];
                              $tmpdiscounts[$tmpkey]["type"] = $discounts[$tmpkey]["discount_type"];
                              $tmpbundles[] = $tmpdiscounts[$tmpkey];
                          }
                      }

                      $sql = 'INSERT INTO '._DB_PREFIX_.'lofbundleproduct_order VALUES ';
                      $sql2 = array();
                      foreach($tmpbundles as $item){
                          $check = Db::getInstance()->ExecuteS("SELECT id_lofbundleproduct FROM "._DB_PREFIX_."lofbundleproduct_order WHERE id_lofbundleproduct=".$item["id"]." AND id_order=".$order_id);
                          if(count($check) == 0 )
                              $sql2 []= '('.$item["id"].','.$order_id.') ';
                      }
                      if(!empty($sql2))
                       Db::getInstance()->Execute($sql.implode(",",$sql2));
                  }
              }
          }
          return true;
      }
      public static function applyDiscount($data = array(), $total = 0){
          global $cookie;
		  $now = date('Y-m-d H:i:s');
           if(!empty($data)){
               $cart = isset($data["cart"])?$data["cart"]:null;
               if($cart){
                   $products = $cart->getProducts();
                   if(count($products) > 1){
                       $product_ids = array();
                       foreach($products as $product){
                         $product_ids[] = $product["id_product"];
                       }
					   
                       asort($product_ids);
                       $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
                       $_join = "";
					    $_join .= 'LEFT JOIN   `'._DB_PREFIX_.'lofbundleproduct_product` AS hss ON (bp.`id_lofbundleproduct` = hss.`id_lofbundleproduct`)';
                       if (Shop::getContext() == Shop::CONTEXT_SHOP)
                           $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` AS bps ON (bp.`id_lofbundleproduct` = bps.`id_lofbundleproduct` AND bps.id_shop = '.(int)Context::getContext()->shop->id.') ';
                       else
                           $_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` AS bps ON (bp.`id_lofbundleproduct` = bps.`id_lofbundleproduct` AND bps.id_shop = bp.id_shop_default) ';
                       // we add restriction for shop
                       $_where = "";
                       $id_lang = (int)Context::getContext()->language->id;
                       if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
                           $_where = ' AND bps.`id_shop` = '.(int)Context::getContext()->shop->id;
						   $_where .= ' AND	(((bp.`from` = \'0000-00-00 00:00:00\' OR \''.$now.'\' >= bp.`from`) AND (bp.`to` = \'0000-00-00 00:00:00\' OR \''.$now.'\' <= bp.`to`))) ';

                       $sql = '
                            SELECT DISTINCT bp.* 
                             FROM '._DB_PREFIX_.'lofbundleproduct bp
                             '.$_join.'
                             WHERE bp.active = 1 AND hss.id_product IN ('.implode(",",$product_ids).') AND (bp.products IS NOT NULL OR bp.products <> "") '.$_where.'
                             ORDER BY bp.id_lofbundleproduct DESC
                        ';
                       $bundles = Db::getInstance()->ExecuteS($sql);
					   
                       $discounts = array();
                       $key_array = array();
                       if(!empty($bundles)){
                           foreach($bundles as $key1=>$row1){
                              if(!self::checkExpired($row1["from"], $row1["to"])){
                                 $tmp_product1 = explode(",",$row1["products"]);
                                  if(!empty($tmp_product1)){
                                    foreach($tmp_product1 as $val){
                                        if(!in_array($val, $product_ids)){
											unset($bundles[$key1]);
                                            break;
											}
										}
									}
								}
                           }
						   
						   if(!empty($bundles)){						   
							   foreach($bundles as $key1=>$row1){
									 $tmp_product1 = explode(",",$row1["products"]);
									 foreach($bundles as $row2){
											if($row2['id_lofbundleproduct'] != $row1['id_lofbundleproduct'] ){
											$tmp_product2 = explode(",",$row2["products"]);
											$tmp_product2 = explode(",",$row2["products"]);
											$tmp_product = array_intersect($tmp_product1, $tmp_product2);
												if( count($tmp_product) >= count($tmp_product1)){
													unset($bundles[$key1]);
													break;	
													}
											}
										}
							   }
							}		
						
						   if(!empty($bundles)){
								$tmpdiscounts = array();
								$tmpbundles = array();
								$total_wt_shipping = isset($data["total_wt_shipping"])?$data["total_wt_shipping"]:0;
								$total_on_shipping = isset($data["total_on_shipping"])?$data["total_on_shipping"]:0;
								$cookie->__set("orderTotal",$total);
								$cookie->__set("total_wt_shipping",$total_wt_shipping);
								$cookie->__set("total_on_shipping",$total_on_shipping);
								 foreach($bundles as $key => $val1){
								 $tmpdiscounts = array();
									$tmpdiscounts["id"] = $val1["id_lofbundleproduct"];
                                    $tmpdiscounts["amount"] = $val1["discount_amount"];
                                    $tmpdiscounts["type"] = $val1["discount_type"];
                                    $tmpbundles[] = $tmpdiscounts;
                                    $total = self::getPriceWithDiscount($total, $val1["discount_amount"],$val1["discount_type"]);
								}
								$cookie->__set("bundleDiscount",json_encode($tmpbundles));	
							}
							else{
								$cookie->__unset("bundleDiscount");
							}
							
						}
                   }
               }
           }
          return $total;
      }
        public static function getPriceWithDiscount($price = 0,$discount=0,$discount_type=1){

        	if($discount_type == 1){
        		if($price >= $discount)
        			$price = $price - $discount;
        	}elseif($discount_type == 2){
        		$discount = $price*$discount/100;
        		$price = $price - $discount;
        	}
        	return $price;
        }
        public static function checkExpired($from = NULL, $to = NULL){
        	  $isExpired = false;
            $now = date("Y-m-d H:i:s",time());
            $now = strtotime($now);
            $fromstr = strtotime($from);
            $tostr = strtotime($to);

          if((empty($from) || $from=="0000-00-00 00:00:00" || $now >= $fromstr ) && ( empty($to) || $to == "0000-00-00 00:00:00") )
          {
              return $isExpired;
          }
        	if($now < $fromstr || $now > $tostr)
        		$isExpired = true;
        	return $isExpired;
        }

      public static function getListProducts($id_product = 0, $id_lofbundleproduct = 0,$id_lang = NULL, $isPaging = 0, $start = NULL, $end = NULL, $orderBy = NULL, $orderWay = NULL,$search = NULL){
          $ids_product = array();
          if(!empty($id_lofbundleproduct)){
              $sql = "SELECT products from "._DB_PREFIX_."lofbundleproduct WHERE id_lofbundleproduct=".$id_lofbundleproduct;
              $ids_product = Db::getInstance()->getValue($sql);
              $ids_product = explode(",", $ids_product);

          }
          if(!$isPaging){
              if(!empty($start) || !empty($end)){
                  $limit=' LIMIT '.$start.','.$end;
              }
              else{
                  $limit='';
              }

          }
          else{

              $limit='';
          }
          if(empty($ids_product))
              return false;
          if(empty($orderBy) || empty($orderWay)){
              $strOrderBy = '';
          }else{
              $strOrderBy = ' ORDER BY '.trim($orderBy).' '.trim($orderWay);
          }

          if(empty($search)){
              $strSearch = ' AND pl.id_product IN ('.implode(",",$ids_product).') ';
          }else{
              //$strSearch = empty($search['id_product']) ? '' : ' AND pl.id_product='.$search['id_product'];
              $strSearch = ' AND pl.id_product IN ('.implode(",",$ids_product).') ';
              $strSearch .= empty($search['product_name']) ? '' : ' AND pl.name LIKE \'%'.$search['product_name'].'%\'';
              $strSearch .= empty($search['category_name']) ? '' : ' AND cl.name LIKE \'%'.$search['category_name'].'%\'';
          }
          if($isPaging){
              $sql = '
                SELECT pl.id_product
                	  FROM '._DB_PREFIX_.'product_lang pl
                    WHERE pl.id_lang = '.intval($id_lang).$strSearch." GROUP BY pl.id_product ";

              $result = Db::getInstance()->ExecuteS($sql);
              $result = count($result);

          }else{

              $sql = '
                SELECT pl.id_product, pl.name,cl.name AS category_name,cl.id_category
                    FROM '._DB_PREFIX_.'product p
                	  LEFT JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product
                	  LEFT JOIN '._DB_PREFIX_.'category_lang cl ON p.id_category_default = cl.id_category
                    WHERE pl.id_lang = '.intval($id_lang).$strSearch." GROUP BY pl.id_product ".$strOrderBy.$limit;

              $result = Db::getInstance()->ExecuteS($sql);
              foreach($result as $i=>$item){
                  if($item["id_product"] == $id_product)
                  {
                      unset($result[$i]);
                      continue;
                  }
                  $result[$i]['lofactive'] = (in_array($item['id_product'], $ids_product)) ?  '1' : '0';
              }
          }
          return $result;
      }
      public function getTitle($id_lang = NULL)
      {
          if (!$id_lang){
              global $cookie;
              if (isset($this->title[$cookie->id_lang]))
                  $id_lang = $cookie->id_lang;
              else
                  $id_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
          }
          return isset($this->title[$id_lang]) ? $this->title[$id_lang] : '';
      }
      public static  function getProductEditLink($id_product = 0){
          $html ="";
          if(!empty($id_product)){
              $sql = '
                SELECT pl.name
                	  FROM '._DB_PREFIX_.'product_lang pl
                    WHERE id_product='.$id_product;

              $product_name = Db::getInstance()->getValue($sql);
              $html = ' ( <a href="index.php?controller=AdminProducts&id_product='.$id_product.'&updateproduct&token='.Tools::getAdminTokenLite("AdminProducts").'" target="_BLANK" title="Edit product">'.$product_name.'</a> )';
          }
          return $html;
      }
      public function storeBundle(){
          $data = $_POST;
          if(!empty($data)){
			
			$id_lofbundleproduct = Tools::getValue('id_lofbundleproduct') ? Tools::getValue('id_lofbundleproduct') : 0 ;
              $this->active = isset($data["active"])?$data["active"]:0;
              $languages = $this->getLanguages();
              $this->title = array();
              if(isset($languages["languages"]) && !empty($languages["languages"])){
                  foreach($languages["languages"] as $lang){
                      $this->title[$lang['id_lang']] = isset($data["title_".$lang['id_lang']])?$data["title_".$lang['id_lang']]:"";
                  }
              }
              $this->discount_amount = isset($data["discount_amount"])?$data["discount_amount"]:0;
              $this->discount_type = isset($data["discount_type"])?$data["discount_type"]:1;
              $this->position = isset($data["position"])?$data["position"]:1;
              $this->from = isset($data["from"])?$data["from"]:"";
              $this->to = isset($data["to"])?$data["to"]:"";
              $this->products = isset($data["lof_pro"])?implode(",", $data["lof_pro"]):"";
			  
              if(!$this->save()){
                  return false;
              }
			  else{
					if($id_lofbundleproduct  && $id_lofbundleproduct != 0){
						$this->deleteItemProduct($id_lofbundleproduct);
					}
					$this->addItemProduct($data["lof_pro"],$this->id);
			  }
          }
          return true;
      }
      /**
       * Get languages and default language
       */
      public function getLanguages(){
          global $cookie;
          $allowEmployeeFormLang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
          if ($allowEmployeeFormLang && !$cookie->employee_form_lang)
              $cookie->employee_form_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
          $useLangFromCookie = false;
          $_languages = Language::getLanguages(false);
          if ($allowEmployeeFormLang)
              foreach ($_languages AS $lang)
                  if ($cookie->employee_form_lang == $lang['id_lang'])
                      $useLangFromCookie = true;
          if (!$useLangFromCookie)
              $_defaultFormLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
          else
              $_defaultFormLanguage = (int)($cookie->employee_form_lang);

          return array('languages' => $_languages, 'defaultFormLanguage' => $_defaultFormLanguage);
      }
	  
	  public function addItemProduct($items,$id_lofbundleproduct)
	  {
		$res = true;
		if(!empty($items)){
			foreach($items as $item){
				$res &= Db::getInstance()->execute('
					INSERT INTO `'._DB_PREFIX_.'lofbundleproduct_product` (`id_lofbundleproduct`, `id_product`)
					VALUES('.(int)$id_lofbundleproduct.', '.(int)$item.')'
				);
			}
		}
		return $res;
	  }
	  
	  public function deleteItemProduct($id_lofbundleproduct)
	  {
			$res = true;
			$res &= Db::getInstance()->execute('
				DELETE FROM  `'._DB_PREFIX_.'lofbundleproduct_product`
				WHERE `id_lofbundleproduct` = '.(int)$id_lofbundleproduct
			);
			return $res;
		}	
		
		
	public  function getLastPosition($id_shop)
		{
			return (int)(Db::getInstance()->getValue('
			SELECT MAX(`position`)
			FROM `'._DB_PREFIX_.'lofbundleproduct_shop`
			WHERE `id_shop` = '.(int)$id_shop) + 1);
		}	
		
	public function addPosition($position, $id_shop = null)
	{
		$return = true;
		if (is_null($id_shop))
		{
			if (Shop::getContext() != Shop::CONTEXT_SHOP)
				foreach (Shop::getContextListShopID() as $id_shop)
					$return &= Db::getInstance()->execute('
						INSERT INTO `'._DB_PREFIX_.'lofbundleproduct_shop` (`id_lofbundleproduct`, `id_shop`, `position`) VALUES
						('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
						ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
			else
			{
				$id = Context::getContext()->shop->id;
				$id_shop = $id ? $id : Configuration::get('PS_SHOP_DEFAULT');
				$return &= Db::getInstance()->execute('
					INSERT INTO `'._DB_PREFIX_.'lofbundleproduct_shop` (`id_lofbundleproduct`, `id_shop`, `position`) VALUES
					('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
					ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
			}
		}
		else
			$return &= Db::getInstance()->execute('
			INSERT INTO `'._DB_PREFIX_.'lofbundleproduct_shop` (`id_lofbundleproduct`, `id_shop`, `position`) VALUES
			('.(int)$this->id.', '.(int)$id_shop.', '.(int)$position.')
			ON DUPLICATE KEY UPDATE `position` = '.(int)$position);
		return $return;
	}	
	
	
	public function updatePosition($way, $position)
		{
				$context = Context::getContext();
				$id_shop = $context->shop->id;
				if (!$res = Db::getInstance()->ExecuteS('
					SELECT cp.`id_lofbundleproduct`, cp.`position`
					FROM `'._DB_PREFIX_.'lofbundleproduct_shop` cp
					WHERE cp.`id_shop` = '.(int)$id_shop.'
					ORDER BY cp.`position` ASC'
				))
					return false;
				foreach ($res AS $bundle)
					if ((int)($bundle['id_lofbundleproduct']) == (int)($this->id))
						$movedBundle = $bundle;
		
				if (!isset($movedBundle) || !isset($position))
					return false;
					
				// < and > statements rather than BETWEEN operator
				// since BETWEEN is treated differently according to databases
				$result = (Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'lofbundleproduct_shop`
					SET `position`= `position` '.($way ? '- 1' : '+ 1').'
					WHERE `position`
					'.($way
						? '> '.(int)($movedBundle['position']).' AND `position` <= '.(int)($position)
						: '< '.(int)($movedBundle['position']).' AND `position` >= '.(int)($position)).'
					')
				AND Db::getInstance()->Execute('
					UPDATE `'._DB_PREFIX_.'lofbundleproduct_shop`
					SET `position` = '.(int)($position).'
					WHERE `id_shop` = '.(int)$id_shop.'
					AND `id_lofbundleproduct`='.(int)($movedBundle['id_lofbundleproduct'])));
					
				return $result;
		}
		
	  
	}
}
?>
