<?php
/*
 * LofDownload Module, Build for Prestashop which use to manage downloads,
 * and allow customers downloads files, resource.
 * 2012 LandOfCoder.Com <@site http://landofcoder.com>
 *
 * @author LandOfCoder <landofcoder@gmail.com>
 * @copyright  2012 LandOfCoder
 * @version  Release: $Revision: 8005 $
 * @license   GPL2 and Later
 */
include_once( dirname(__FILE__)."/../../classes/LofClassBundleproduct.php");
class AdminLofManageBundleController extends AdminController
{
	/** @var object adminCMSCategories() instance */
	protected $adminBundleproducts;

	/** @var object Category() instance for navigation*/
	protected static $_lofbundleproduct = null;
  public $_defaultFormLanguage;

	public function __construct()
	{
		$this->table = 'lofbundleproduct';
		$this->className = 'LofClassBundleproduct';
		$this->identifiersDnd = array('id_lofbundleproduct' => 'id_lofbundleproduct_to_move');
		$this->position_identifier = 'id_lofbundleproduct_to_move';
		$this->identifier = 'id_lofbundleproduct';
    $this->moduleName = 'lofbundleproduct';
    $this->context = Context::getContext();
    $this->lang = true;

    Shop::addTableAssociation('lofbundleproduct', array('type' => 'shop'));
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
     $this->addRowAction('edit');
     $this->addRowAction('delete');
    $this->_typeArray = array(
        1=>$this->l('Fixed Value'),
        2=>$this->l('Percent'));
    $this->_select = '
				case a.`discount_type`
				when 1 then \''.$this->l('Fixed').'\'
				when 2 then \''.$this->l('%').'\'
				end as discount_type';
	$this->_select .= ',sa.`position` as position';			
    $this->fields_list = array(
          'id_lofbundleproduct' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 20),
          'title' => array('title' => $this->l('Title'), 'width' => 160),
          'discount_amount' => array('title' => $this->l('Discount'), 'width' => 50),
          'discount_type' => array('title' => $this->l('Discount Type'), 'width' => 40,'align' => 'center', 'type' => 'select','list' => $this->_typeArray,'filter_type' => 'int', 'orderby' => false , 'filter_key'=>'discount_type'),
		  'position' => array(
				'title' => $this->l('Position'),
				'width' => 40,
				'filter_key' => 'sa!position',
				'align' => 'center',
				'position' => 'position'
				),
          'active' => array('title' => $this->l('Active'), 'align' => 'center','active' => 'status', 'width' => 25,'orderby' => false,'type' => 'bool', 'filter_key' => 'a!active'),
         // 'position' => array('title' => $this->l('Ordering'), 'align' => 'center', 'width' => 18,'orderby' => true)
		  );

		parent::__construct();
    $this->getlanguages();
    $this->_defaultFormLanguage = (int)Configuration::get('PS_LANG_DEFAULT');

	}
	
	
	public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
	{
		$alias = 'sa';
		parent::getList($id_lang, $alias.'.position', $order_way, $start, $limit);
		
	}

	public function viewAccess($disable = false)
	{
		$result = parent::viewAccess($disable);
		return $result;
	}

    public function initToolbar() {
        $controller = Tools::getValue("controller");
        $id_product = (int)Tools::getValue("id_product");
        switch ($this->display) {
            case 'add':
            case 'edit':
            case 'view':
            $link = '#';
            $this->toolbar_btn['save'] = array(
                'href' => $link,
                'desc' => $this->l('Save')
            );
            $this->toolbar_btn['save-and-stay'] = array(
                'href' => $link,
                'desc' => $this->l('Save And Stay')
            );
            $this->toolbar_btn['save-and-new'] = array(
                'href' => $link,
                'desc' => $this->l('Save and New')
            );

            if($this->display == "edit" || $this->display == "view"){
                $this->toolbar_btn['duplicate'] = array(
                    'href' => $link,
                    'desc' => $this->l('Duplicate')
                );
                /**
                $id_lofbundleproduct = Tools::getValue("id_lofbundleproduct",0);
                $this->toolbar_btn['delete'] = array(
                    'href' => 'index.php?controller=AdminLofManageBundle&amp;id_lofbundleproduct='.$id_lofbundleproduct.'&amp;deletelofbundleproduct&amp;token=' . $this->token,
                    'confirm' => $this->l("Do you want delete this item?"),
                    'desc' => $this->l('Delete')
                );*/
            }
            $this->toolbar_btn['back'] = array(
                'href' => 'index.php?controller=AdminLofManageBundle&amp;token=' . $this->token,
                'desc' => $this->l('Back to list')
            );
                break;
            default:
                $link = 'index.php?controller=AdminLofManageBundle&amp;add' . $this->table . '&amp;id_product='.$id_product.'&amp;token=' . Tools::getAdminTokenLite("AdminLofManageBundle");
                $this->toolbar_btn['save'] = array(
                    'href' => $link,
                    'desc' => $this->l('Add new')
                );

        }

        if($controller == "AdminProducts" && !empty($id_product)){
            $this->context->smarty->assign('toolbar_scroll', 0);
            $this->context->smarty->assign('show_toolbar', 0);
            $this->context->smarty->assign('toolbar_btn', $this->toolbar_btn);
            $this->context->smarty->assign('title', "");

        }else{
            $this->context->smarty->assign('toolbar_scroll', 1);
            $this->context->smarty->assign('show_toolbar', 1);
            $this->context->smarty->assign('toolbar_btn', $this->toolbar_btn);
            $this->context->smarty->assign('title', $this->toolbar_title);
        }

    }

    public function initToolbarTitle() {
        parent::initToolbarTitle();
        if ($this->display == 'edit' || $this->display == 'add') {
            $article = $this->loadObject(true);
            if ($article)
                if (isset($article->id)&&(bool) $article->id && $this->display != 'list' && isset($this->toolbar_title[3]))
                    $this->toolbar_title[3] = $this->toolbar_title[3] . ' (' . $article->title[$this->context->language->id] . ')';
        }
    }
    public function renderList(){
        global $currentIndex, $cookie;
        $this->initToolbar();
        $is_multishop = Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE');
        if (Shop::getContext() == Shop::CONTEXT_SHOP)
            $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` sa ON (a.`id_lofbundleproduct` = sa.`id_lofbundleproduct` AND sa.id_shop = '.(int)$this->context->shop->id.') ';
        else
            $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_shop` sa ON (a.`id_lofbundleproduct` = sa.`id_lofbundleproduct` AND sa.id_shop = a.id_shop_default) ';
		 
        // we add restriction for shop
        if (Shop::getContext() == Shop::CONTEXT_SHOP && $is_multishop)
            $this->_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;
        $controller = Tools::getValue("controller");
        $id_product = (int)Tools::getValue("id_product");
        if($controller == "AdminProducts" && !empty($id_product)){
			$this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'lofbundleproduct_product` hs ON (a.`id_lofbundleproduct` = hs.`id_lofbundleproduct`) ';
            $id_product = (int)Tools::getValue("id_product");
            $this->_where .= ' AND hs.`id_product`='.$id_product;
            self::$currentIndex = "index.php?controller=AdminLofManageBundle";
            return parent::renderList();
        }else if($controller == "AdminProducts"){
            return "";
        }else{
            return parent::renderList();
        }
    }
    public function renderForm($isMainTab = true)
    {
        global $currentIndex, $cookie;
        if (Validate::isLoadedObject($this->object))
            $this->display = 'edit';
        else
            $this->display = 'add';
        if($this->display == "add"){
            $this->object = new stdClass();
            $this->object->id_lofbundleproduct = "0";
        }
        return $this->renderView();
    }
    public function renderView()
    {
        global $currentIndex;
        $link = new Link();
        $token = $this->token;
        $bundle = $this->object;
        $cookie = $this->context->cookie;
        $html = '';
        $this->initToolbarTitle();
        $this->initToolbar();
        $this->context->smarty->assign('title', $this->toolbar_title);
        $html = $this->context->smarty->fetch('toolbar.tpl');
        $id_product = (int)Tools::getValue("id_product");
        if(empty($id_product)){
            $id_product = isset($bundle->id_product)?$bundle->id_product:0;
        }
        $ids_product = array();
        $i = 0;
        $bundle->id_lofbundleproduct = isset($bundle->id_lofbundleproduct)?$bundle->id_lofbundleproduct:0;
        ob_start();
        include_once _PS_MODULE_DIR_.$this->moduleName.'/views/controllers/admin/bundle_form.php';
        $html .= ob_get_contents();
        ob_end_clean();
        return $html;
    }
    protected function copyFromPost(&$object, $table)
    {
        parent::copyFromPost($object, $table);
    }
	public function postProcess()
	{
	

	$this->viewAccess();
	
	if( Tools::getValue('action') == 'updatePositions'  && Tools::getValue('lofbundleproduct') ){
			$id_lofbundleproduct_to_move = (int)(Tools::getValue('id'));
			$way = (int)(Tools::getValue('way'));
			$positions = Tools::getValue('lofbundleproduct');
			if (is_array($positions))
				foreach ($positions as $key => $value)
				{
					$pos = explode('_', $value);
					if ((isset($pos[1])) && ($pos[2] == $id_lofbundleproduct_to_move))
					{
						$position = $key + 1;
						break;
					}
				}
				
			$lofbundleproduct = new LofClassBundleproduct($id_lofbundleproduct_to_move);
			if (Validate::isLoadedObject($lofbundleproduct))
			{
				if (isset($position) && $lofbundleproduct->updatePosition($way, $position))
				{
					die(true);
				}
				else
					die('{"hasError" : true, errors : "Can not bundle bundle position"}');
			}
			else
				die('{"hasError" : true, "errors" : "This bundle can not be loaded"}');
		}	
	
    $action = isset($_POST["action"])?$_POST["action"]:"";

      if(!empty($action)){
          $products = isset($_POST['ids_pro'])?$_POST['ids_pro']:array();
          $id_lofbundleproduct = isset($_POST["id_lofbundleproduct"])?$_POST["id_lofbundleproduct"]:0;
          if(empty($products)){
			$this->errors[] = Tools::displayError('Please select product(s)!');
          }
		elseif(!empty($_POST['discount_amount']) && !Validate::isFloat($_POST['discount_amount'])){
			$this->errors[] = Tools::displayError('Invalid bundle discount_amount!');
          }
		  else{
              $lofbundle = new LofClassBundleproduct($id_lofbundleproduct);
              if($action == "duplicate"){
                  if($newLofbundle = $lofbundle->duplicateObject()){
                      Tools::redirectAdmin('index.php?controller=AdminLofManageBundle&id_lofbundleproduct='.$newLofbundle->id.'&updatelofbundleproduct&token='.Tools::getAdminTokenLite("AdminLofManageBundle"));
                  }
              }
			  else{
                  if($lofbundle->storeBundle()){
					$id_product = isset($_POST['id_product']) ? $_POST['id_product']: 0;
				
						if($action == "save"){
							if( $id_product != 0 )
								Tools::redirectAdmin('index.php?controller=AdminProducts&id_product='.$id_product.'&updateproduct&token='.Tools::getAdminTokenLite("AdminProducts"));
							else
								  Tools::redirectAdmin('index.php?controller=AdminLofManageBundle&token='.Tools::getAdminTokenLite("AdminLofManageBundle"));
						  
						  }elseif($action =="save_and_stay"){
							if( $id_product != 0 )
								Tools::redirectAdmin('index.php?controller=AdminLofManageBundle&id_lofbundleproduct='.$lofbundle->id.'&updatelofbundleproduct&id_product='.$id_product.'&token='.Tools::getAdminTokenLite("AdminLofManageBundle"));
							else
							  Tools::redirectAdmin('index.php?controller=AdminLofManageBundle&id_lofbundleproduct='.$lofbundle->id.'&updatelofbundleproduct&id_product='.$id_product.'&token='.Tools::getAdminTokenLite("AdminLofManageBundle"));
						  }elseif($action == "save_and_new"){
							if( $id_product != 0 )
								Tools::redirectAdmin('index.php?controller=AdminLofManageBundle&addlofbundleproduct&id_product='.$id_product.'&token='.Tools::getAdminTokenLite("AdminLofManageBundle"));
							else	
							    Tools::redirectAdmin('index.php?controller=AdminLofManageBundle&addlofbundleproduct&token='.Tools::getAdminTokenLite("AdminLofManageBundle"));
                      }
					}					 
                 }
              
              echo '
    			<div class="conf">
    				<img src="../img/admin/ok2.png" alt="" />
                    '.$this->l('Stored successful').'
    			</div>';
          }
      }

      parent::postProcess();
	}
	
	
	public function processPosition()
	{
		if ($this->tabAccess['edit'] !== '1')
			$this->errors[] = Tools::displayError('You do not have permission to edit this.');
		else if (!Validate::isLoadedObject($object = new LofClassBundleproduct((int)Tools::getValue($this->identifier, Tools::getValue('id_category_to_move', 1)))))
			$this->errors[] = Tools::displayError('An error occurred while updating the status for an object.').' <b>'.
				$this->table.'</b> '.Tools::displayError('(cannot load object)');
		if (!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position')))
			$this->errors[] = Tools::displayError('Failed to update the position.');
		else
		{
			 Tools::redirectAdmin('index.php?controller=AdminLofManageBundle&token='.Tools::getAdminTokenLite("AdminLofManageBundle"));
		}
	}

	public function setMedia()
	{
		parent::setMedia();
		$this->addJqueryUi('ui.widget');
		$this->addJqueryPlugin('tagify');
	}
  protected  function inputTagLang($divLangName, $name, $values, $note=""){
        $langs = $this->getLanguages2();
        $_languages = $langs['languages'];
        $_defaultFormLanguage = $langs['defaultFormLanguage'];
        $strValue = '';

        foreach ($_languages as $language){
            $fieldvalue = isset($values[$language['id_lang']])?$values[$language['id_lang']]:'';
            $strValue .= '<div id="'.$name.'_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $_defaultFormLanguage ? 'block' : 'none').';float: left;width: 230px;">
					<input size="40" type="text" id="'.$name.'_'.$language['id_lang'].'" name="'.$name.'_'.$language['id_lang'].'" value="'.$fieldvalue.'" />
				</div>';
        }
        $html = '
		<div class="margin-form">
			'.$strValue.$this->displayFlags($_languages, $_defaultFormLanguage, $divLangName, $name).'<div class="clear"></div>'.$note.'
		</div>';

        return $html;
    }
    /**
     * Get languages and default language
     */
    public function getLanguages2(){
        global $cookie;
        $allowEmployeeFormLang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        if ($allowEmployeeFormLang && !$cookie->employee_form_lang)
            $cookie->employee_form_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
        $useLangFromCookie = false;
        $_languages = Language::getLanguages(false);
        if ($allowEmployeeFormLang)
            foreach ($_languages AS $lang)
                if ($cookie->employee_form_lang == $lang['id_lang'])
                    $useLangFromCookie = true;
        if (!$useLangFromCookie)
            $_defaultFormLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
        else
            $_defaultFormLanguage = (int)($cookie->employee_form_lang);

        return array('languages' => $_languages, 'defaultFormLanguage' => $_defaultFormLanguage);
    }
    /**
     * display flag for language
     */
    /**
     * display flag for language
     */
    public function displayFlags( $languages, $defaultLanguage, $ids, $id, $return = false ) {
        if (sizeof($languages) == 1)
            return false;
        $output = '
		<div class="displayed_flag">
			<img src="../img/l/'.$defaultLanguage.'.jpg" class="pointer" id="language_current_'.$id.'" onclick="toggleLanguageFlags(this);" alt="" />
		</div>
		<div id="languages_'.$id.'" class="language_flags">
			'.$this->l('Choose language:').'<br />';
        foreach ($languages as $language)
            $output .= '<img src="../img/l/'.(int)($language['id_lang']).'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="changeLanguage(\''.$id.'\', \''.$ids.'\', '.$language['id_lang'].', \''.$language['iso_code'].'\');" /> ';
        $output .= '</div>';
        return $output;
    }
}
