<?php
/**
 * $ModDesc
 *
 * @version		$Id: file.php $Revision
 * @package		modules
 * @subpackage	$Subpackage.
 * @copyright	Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license		GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')){
	define('_CAN_LOAD_FILES_',1);
}
/**
 * lofsliding Class
 */

@session_start();
class lofbundleproduct extends Module{
	/**
	* @variable
	*/
	private $_params = '';
	public $_postErrors = array();
    public $_postSuccess = '';
    const INSTALL_SQL_FILE = 'install.sql';
    const UNINSTALL_SQL_FILE = 'uninstall.sql';
    public $_defaultFormLanguage;
    public $_languages;

   /**
    * Constructor
    */
	function __construct(){
		$this->name = 'lofbundleproduct';
		parent::__construct();
		$this->tab = 'LandOfCoder';
		$this->version = '1.1';
		$this->displayName = $this->l('Lof Bundle Product Promotion Module');
		$this->description = $this->l('Lof Bundle Product Promotion Module: possible to sell product combinations. Product A + B for a discounted price.');
		$this->module_key = "";
		if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' ) && !class_exists("LofParams", false) ){
		  require( _PS_ROOT_DIR_.'/modules/'.$this->name.'/libs/params.php' );
		}

      if( file_exists( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofClassBundleproduct.php' ) ){
				require_once( _PS_ROOT_DIR_.'/modules/'.$this->name.'/classes/LofClassBundleproduct.php' );
			}

     $this->Languages();
		$this->_params = new LofParams( $this->name, $this->_languages, $this->_defaultFormLanguage );
	}
    public function Languages(){
        global $cookie;
        $allowEmployeeFormLang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        if ($allowEmployeeFormLang && !$cookie->employee_form_lang)
            $cookie->employee_form_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
        $useLangFromCookie = false;
        $this->_languages = Language::getLanguages(false);
        if ($allowEmployeeFormLang)
            foreach ($this->_languages AS $lang)
                if ($cookie->employee_form_lang == $lang['id_lang'])
                    $useLangFromCookie = true;
        if (!$useLangFromCookie)
            $this->_defaultFormLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
        else
            $this->_defaultFormLanguage = (int)($cookie->employee_form_lang);
    }
   /**
    * process installing
    * for example hook "displayDiscounts": {hook h='displayDiscounts'}
    */
	function install(){
		if(!parent::install()
            OR !$this->registerHook('header')
            OR !$this->registerHook('displayAdminProductsExtra')
            OR !$this->registerHook('displayShoppingCart')
            OR !$this->registerHook('displayDiscounts')
            OR !$this->registerHook('displayAdminOrder')
            OR !$this->registerHook('displayHome')
            OR !$this->registerHook('displayOrderDetail')
            OR !$this->registerHook('actionAfterGetTotal')
            OR !$this->registerHook('orderConfirmation')
            OR !$this->registerHook('displayFooterProduct')
            )
            return false;

        if(!$this->installTable()) return false;
		if(!$this->installAdminTabs()) return false;

		return true;
	}
    /**
    * process uninstalling
    */
    function uninstall(){
		if(!parent::uninstall())return false;
		if(!$this->uninstallModuleTab('AdminLofManageBundle')) return false;
        if(!$this->uninstallTable()) return false;
		return true;
	}
	/**
	* Install Module Tab
	*/
	private function installAdminTabs() {
		$languages = Language::getLanguages(false);
		foreach ($languages as $language) {
			$arrName[$language['id_lang']] = 'Bundle Products';
		}
		$idTab = Tab::getIdFromClassName('AdminCatalog');
		$this->installModuleTab('AdminLofManageBundle', $arrName, $idTab);
		return true;
	}
	private function installModuleTab($tabClass, $tabName, $idTabParent){
		@copy(_PS_MODULE_DIR_.$this->name.'/logo.png', _PS_IMG_DIR_.'t/'.$tabClass.'.png');
		$tab = new Tab();
		$tab->name = $tabName;
		$tab->class_name = $tabClass;
		$tab->module = $this->name;
		$tab->id_parent = $idTabParent;
		if(!$tab->save())
			return false;
		return true;
	}
	private function uninstallModuleTab($tabClass){
		$idTab = Tab::getIdFromClassName($tabClass);
		if($idTab != 0){
			$tab = new Tab($idTab);
			$tab->delete();
			return true;
		}
		return false;
	}
    /**
    * Uninstall Table
    */
    public function uninstallTable(){
        if (!file_exists(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE))
			return (false);
		else if (!$sql = file_get_contents(dirname(__FILE__).'/db/'.self::UNINSTALL_SQL_FILE))
			return (false);
		$sql = str_replace('ps_', _DB_PREFIX_, $sql);
		$sql = preg_split("/;\s*[\r\n]+/",$sql);
        foreach ($sql as $query)
			if (!Db::getInstance()->Execute(trim($query)))
				return (false);
        return true;
    }
    /**
    * Install Table
    */
    public function installTable(){
        if (!file_exists(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE))
			return (false);
		else if (!$sql = file_get_contents(dirname(__FILE__).'/db/'.self::INSTALL_SQL_FILE))
			return (false);
		$sql = str_replace('ps_', _DB_PREFIX_, $sql);
		$sql = preg_split("/;\s*[\r\n]+/",$sql);
		foreach ($sql as $query){
			if(!empty($query)){
				if (!Db::getInstance()->Execute(trim($query)))
					return (false);
			}
		}
        return true;
    }

    /**
    * Hook Position
    */
    function hookHeader($params)
	{
		$this->context->controller->addCSS( ($this->_path).'assets/style.css', 'all');
		$this->context->controller->addCSS( ($this->_path).'tmpl/'. $this->getParamValue('module_theme','default').'/assets/style.css', 'all');
		$this->context->controller->addJS(_MODULE_DIR_.$this->name.'/assets/countdown.js');

	}
    function getBundlesAndOrderTotals($params){
        global $cookie;

        // Set currency
        if ((int)$params['cart']->id_currency && (int)$params['cart']->id_currency != $this->context->currency->id)
            $currency = new Currency((int)$params['cart']->id_currency);
        else
            $currency = $this->context->currency;
        $order_total = isset($params["orderTotal"])?$params["orderTotal"]:$cookie->__get("orderTotal");
		$total_wt_shipping = isset($params["total_wt_shipping"])?$params["total_wt_shipping"]:$cookie->__get("total_wt_shipping");
		$total_on_shipping = isset($params["total_on_shipping"])?$params["total_on_shipping"]:$cookie->__get("total_on_shipping");
        if(isset($params["bundleDiscount"]) && !empty($params["bundleDiscount"])){
            $id_bundles = $params["bundleDiscount"];
        }else{
            $bundle_discounts = json_decode($cookie->__get("bundleDiscount"));
            $id_bundles = array();
            if(!empty($bundle_discounts)){
                foreach($bundle_discounts as $discount){
                    $id_bundles[] = $discount->id;
                }
            }
        }
        $bundles = LofClassBundleproduct::getBundlesByIds($id_bundles,(int)$this->context->cookie->id_lang);
        $order_total_discount = $order_total;
        if(!empty($bundles)){
            foreach($bundles as &$row){
              $order_total_discount = LofClassBundleproduct::getPriceWithDiscount($order_total_discount,$row["discount_amount"],$row["discount_type"]);
              if($row["discount_type"] == 2)
                $row["discount_amount"] = (int)$row["discount_amount"]."%";
              else
                $row["discount_amount"] = Tools::displayPrice($row["discount_amount"], $currency);
            }


        }
        $data = array();
        $data["order_total"] = Tools::displayPrice($order_total, $currency);
        $data["order_total_discount"] = Tools::displayPrice($order_total_discount, $currency);
		$data["total_wt_shipping"] = Tools::displayPrice($total_wt_shipping, $currency);
		$data["total_on_shipping"] = Tools::displayPrice($total_on_shipping, $currency);
        $data["bundles"] = $bundles;
        return $data;
    }

    function hookactionAfterGetTotal($params){
        $total = isset($params["order_total"])?$params["order_total"]:0;
        $total = LofClassBundleproduct::applyDiscount($params, $total);
        return $total;
    }
    function hookdisplayAdminOrder($params){
        $this->hookHeader($params);
        $id_order = Tools::getValue("id_order");
        $order = new Order($id_order);
        $params["orderTotal"] = $order->total_products;
        if ((int)$params['cart']->id_currency && (int)$params['cart']->id_currency != $this->context->currency->id)
            $currency = new Currency((int)$params['cart']->id_currency);
        else
            $currency = $this->context->currency;

        $total_discount = $order->total_discounts;
        $params["bundleDiscount"] = array();
        $rows = Db::getInstance()->ExecuteS("SELECT id_lofbundleproduct FROM "._DB_PREFIX_."lofbundleproduct_order WHERE id_order=".$id_order);
        if(!empty($rows)){
            foreach($rows as $row){
                $params["bundleDiscount"][] = $row["id_lofbundleproduct"];
            }
        }

        $this->context->smarty->assign(array(
            "total_discount"=>Tools::displayPrice($total_discount, $currency)
        ));
        return $this->runHook($params,"display_order_detail");
    }
    function hookdisplayOrderDetail($params){
        if(!$this->getParamValue("enable_module", 1))
             return;
        $params["orderTotal"] = isset($params["order"])?$params["order"]->total_products:0;
        if ((int)$params['cart']->id_currency && (int)$params['cart']->id_currency != $this->context->currency->id)
            $currency = new Currency((int)$params['cart']->id_currency);
        else
            $currency = $this->context->currency;
        $id_order = isset($params["order"])?$params["order"]->id:0;

        $total_discount = isset($params["order"])?$params["order"]->total_discounts:0;
        $params["bundleDiscount"] = array();
        $rows = Db::getInstance()->ExecuteS("SELECT id_lofbundleproduct FROM "._DB_PREFIX_."lofbundleproduct_order WHERE id_order=".$id_order);
        if(!empty($rows)){
            foreach($rows as $row){
                $params["bundleDiscount"][] = $row["id_lofbundleproduct"];
            }
        }

        $this->context->smarty->assign(array(
            "total_discount"=>Tools::displayPrice($total_discount, $currency)
        ));
        return $this->runHook($params,"display_order_detail");
    }
    function hookOrderConfirmation($params){
       $order = isset($params["objOrder"])?$params["objOrder"]:null;
       if($order){
           $products = $order->getProducts();
           LofClassBundleproduct::addBundleToOrder($products, $order->id);
       }
        return ;
    }
    function hookdisplayShoppingCart($params){
        global $cookie;
        if(!Tools::getValue("controller") == "order")
            return;
        if(!$this->getParamValue("enable_module", 1))
            return;
        return $this->runHook($params);
    }
    function runHook($params, $hook_name = "display_shopping_cart"){
        global $cookie;
        $data = $this->getBundlesAndOrderTotals($params);
		
        $order_total = isset($data["order_total"])?$data["order_total"]:0;
		$total_wt_shipping = isset($data["total_wt_shipping"])?$data["total_wt_shipping"]:0;
		$total_on_shipping = isset($data["total_on_shipping"])?$data["total_on_shipping"]:0;
        $order_total_discount = isset($data["order_total_discount"])?$data["order_total_discount"]:0;
        $bundles = isset($data["bundles"])?$data["bundles"]:array();
        $show_discount_info_on_cart = $this->_params->get("show_info", 1);
        if(!empty($bundles) && $show_discount_info_on_cart){
            $id_lang = $this->context->language->id;
            foreach($bundles as &$row){
                if(!empty($row["products"])){
                    $products = explode(",",$row["products"]);
                    $row["products"] = array();
                    foreach($products as $val){
                        $val = (int)$val;
                        if($val){
                            $tmpp = new Product($val, true,$id_lang);
                            $item_p = array();
                            $item_p["product_id"] = $val;
                            $item_p["product_price"] = $tmpp->price;
                            $item_p["product_name"] = $tmpp->name;
                            $item_p["link_rewrite"] = $tmpp->link_rewrite;
                            $item_p["product_image"] = $tmpp->getCover($val);
                            $item_p["product_image"] = isset($item_p["product_image"]["id_image"])?$item_p["product_image"]["id_image"]:0;
                            $item_p["product_link"] = $tmpp->getLink();
                            $row["products"][] = $item_p;
                        }
                    }
                }

            }
        }
        $message_before = $this->getParamValue("msg_before_".$this->_defaultFormLanguage,"");
        $message_after = $this->getParamValue("msg_after_".$this->_defaultFormLanguage,"");
        $message_title = $this->getParamValue("msg_title_".$this->_defaultFormLanguage, $this->l("You get discount"));
        if(empty($bundles))
            return;
        $this->context->smarty->assign(array(
            "message_title" => $message_title,
            "message_before" => $message_before,
            "message_after" => $message_after,
            "order_total" => $order_total,
			"total_wt_shipping" => $total_wt_shipping,
			"total_on_shipping" => $total_on_shipping,
            "order_total_discount" => $order_total_discount,
            "bundles" => $bundles,
            "mediumSize" => Image::getSize(ImageType::getFormatedName('medium')),
            "show_discount_info" => $show_discount_info_on_cart
        ));
        $notify_style = $this->getParamValue("notify_stype");
        if($notify_style == "popup")
            return $this->processHook( $params,$hook_name,"hooks");
        else
            return $this->processHook( $params,$hook_name."_notify","hooks");
    }
  	function hookdisplayDiscounts($params){
        global $cookie;
        $data = $this->getBundlesAndOrderTotals($params);
        $order_total = isset($data["order_total"])?$data["order_total"]:0;
        $order_total_discount = isset($data["order_total_discount"])?$data["order_total_discount"]:0;
        $bundles = isset($data["bundles"])?$data["bundles"]:array();

        $this->context->smarty->assign(array(
                 "total_with_discount" => $order_total_discount,
                 "total" => $order_total,
                 "bundles" => $bundles
            ));

        $module_bundle_dir =  _PS_MODULE_DIR_.$this->name."/";
        return $this->processHook( $params,"display_discounts","hooks","",$module_bundle_dir);
    }
    function hookdisplayAdminProductsExtra($params){

        require_once _PS_MODULE_DIR_.$this->name."/controllers/admin/AdminLofManageBundle.php";
        $controller = new AdminLofManageBundleController();
        $html = $controller->renderList();
        return $html;
    }
    function hookdisplayShoppingCartFooter($params){

    }
    function hookrightColumn($params)
    {
        global $smarty,$cookie;
    }
    function hookleftColumn($params)
	{
		global $smarty,$cookie;
	}

    public function hookdisplayFooterProduct($params){
        global $smarty,$cookie;
        $id_product = Tools::getValue("id_product");
        if(empty($id_product))
            return;
        $bundle_promotions = LofClassBundleproduct::getBundleProducts($id_product);
		
        if(empty($bundle_promotions))
            return;
        $this->context->smarty->assign(array(
            'id_product' =>$id_product,
            'disable_buy' => false,
            'bundles' => $bundle_promotions,
            'mediumSize' => Image::getSize(ImageType::getFormatedName('medium'))
        ));
		
        return $this->processHook( $params,"display_product_footer","hooks");
    }
	
	 public function hookdisplayHome($params){
        global $smarty,$cookie;
       $bundle_promotions = LofClassBundleproduct::getListBundleProducts();
		
        if(empty($bundle_promotions))
            return;
        $this->context->smarty->assign(array(
            'disable_buy' => false,
            'bundles' => $bundle_promotions,
			'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
            'mediumSize' => Image::getSize(ImageType::getFormatedName('medium'))
        ));
        return $this->processHook( $params,"display_product_home","hooks");
    }

    function hooktop($params)
    {
        global $smarty,$cookie;
		    return $this->processHook( $params,"hooktop");
    }
    function hookdisplayLeftColumn($params)
    {
        global $smarty,$cookie;
        return $this->processHook( $params,"hookleft");
    }
	function hookdisplayRightColumn($params)
	{
		return $this->processHook( $params,"hookright");
	}



	/**
    * Proccess module by hook
    * $pparams: param of module
    * $pos: position call
    */
	function processHook( $params, $pos, $folder = "", $extrahtml = "", $baseDir=""){
		$theme = Configuration::get($this->name.'_module_theme');

        if(!$theme) $theme = "default";
        if(!empty($folder))
            $folder .="/";
        if(empty($baseDir))
            $baseDir = __FILE__;

        return ($this->display($baseDir, 'views/templates/'.$theme.'/'.$folder.$pos.'.tpl')).$extrahtml;
	}
    public function getLayoutPath( $theme ){
        $layout = 'tmpl/'.$theme.'/default.tpl';
        if( !file_exists(__FILE__."/".$layout) ){
            return $layout;
        }
        return 'tmpl/default.tpl';
    }
   /**
    * Get list of sub folder's name
    */
	public function getFolderList( $path ) {
		$items = array();
		$handle = opendir($path);
		if (! $handle) {
			return $items;
		}
		while (false !== ($file = readdir($handle))) {
			if (is_dir($path . $file))
				$items[$file] = $file;
		}
		unset($items['.'], $items['..'], $items['.svn']);
		return $items;
	}
   /**
    * Render processing form && process saving data.
    */
	public function getContent()
	{
 		$html = "";
		if (Tools::isSubmit('submit')){
			$this->_postValidation();
			if (!sizeof($this->_postErrors)){
                $definedConfigs = array(
                    'module_theme' => '',
					          'show_info' => '',
					          'msg_before' => '',
					          'msg_after' => '',
                    'msg_title' => '',
                    'enable_module' => '',
                    'notify_stype' => ''

                );
                foreach($this->_languages as $language){
                    $definedConfigs['msg_title_'.$language['id_lang']] = '';
                    $definedConfigs['msg_after_'.$language['id_lang']] = '';
                    $definedConfigs['msg_before_'.$language['id_lang']] = '';
                }
                foreach( $definedConfigs as $config => $key ){
					$config_value = Tools::getValue($config);
					if(is_array($config_value)){
						$config_value = serialize($config_value);
					}
		      		Configuration::updateValue($this->name.'_'.$config, $config_value, true);
		    	}


		        $html .= '<div class="conf confirm">'.$this->l('Settings updated').'</div>';
			}
			else{
				foreach ($this->_postErrors AS $err){
					$html .= '<div class="alert error">'.$err.'</div>';
				}
			}
			// reset current values.
			$this->_params = new LofParams( $this->name, $this->_languages, $this->_defaultFormLanguage );
		}
		return $html.$this->_getFormConfig();
	}
	/**
	 * Render Configuration From for user making settings.
	 *
	 * @return context
	 */
	private function _getFormConfig(){
		$html = '';
	    $themes=$this->getFolderList( dirname(__FILE__)."/views/templates/" );
	    ob_start();
        echo '<script type="text/javascript">
                id_language='.$this->_defaultFormLanguage.';
            </script>';
	    include_once dirname(__FILE__).'/config/lofbundleproduct.php';
	    $html .= ob_get_contents();
	    ob_end_clean();
		return $html;
	}
	/**
     * Process vadiation before saving data
     */
	private function _postValidation()
	{
		//if (!Validate::isCleanHtml(Tools::getValue('module_height')))
		//	$this->_postErrors[] = $this->l('The module height you entered was not allowed, sorry');
	}
   /**
    * Get value of parameter following to its name.
    *
	* @return string is value of parameter.
	*/
	public function getParamValue($name, $default=''){
		return $this->_params->get( $name, $default );
	}
}



