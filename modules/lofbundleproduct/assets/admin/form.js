$(document).ready(function() {
	
	$('.select-option').each(function() {		
		name = $(this).attr("name");		
		elemens = $.find('input[name="'+name+'"]');		
		for(i =0; i< elemens.length; i++){			
			if(!$(elemens[i]).attr("checked")){				
				$('.' + name + '-' +$(elemens[i]).val()).hide();
			}					
			
			$(elemens[i]).click(function() {
				subNameb   = $(this).attr("name");
				subElemens = $.find('input[name="'+subNameb+'"]');
				for(j =0; j< subElemens.length; j++){					
					if(!$(subElemens[j]).attr("checked")){						
						$('.' + $(subElemens[j]).attr("name") + '-' +$(subElemens[j]).val()).hide();
					}else{						
						$('.' + $(subElemens[j]).attr("name") + '-' +$(subElemens[j]).val()).show();
					}
				}				
			});
		}			
	});	
		
	
	$('.select-group').each(function() {
		currentValue = $(this).val();
		name = $(this).attr("name");		
		$(this).find("option").each(function(index,Element) {		
		    if($(Element).val() == currentValue){		    	
		    	$('.' + name + '-' + $(Element).val()).show();
		    }else{		    	
		    	$('.' + name + '-' + $(Element).val()).hide();
		    }
		});
	});	
	
	$('.select-group').change(function() {	   
		currentValue = $(this).val();
		name = $(this).attr("name");        		
		$(this).find("option").each(function(index,Element) {		
		    if($(Element).val() == currentValue){		          
		    	$('.' + name + '-' + $(Element).val()).show();
		    }else{
		    	$('.' + name + '-' + $(Element).val()).hide();
		    }
		});		
	});
	var value = $('#menu_type').find("option:selected").val();
	$('.lof_type').css('display','none');
	$('.menu_'+ value ).css('display','inline');
	$('#menu_type').change(function(){
		var value = $(this).find("option:selected").val();
		$('.lof_type').css('display','none');
		$('.menu_'+ value ).css('display','inline');
	});
	$('.checkall').click(function(){
		var checked_status = $(this).checked;
		$('#lofform').find('input:checkbox').each(function(){
			$(this).checked = checked_status;
		});
	});
	$('#submitDelete').click(function(){
		var result = false;
		$('#lofform').find('.checkme').each(function(){
			if($(this).is(':checked')){
				result = true;
			}
		});
		if(result == true ){
			$('#valuetaskdelete').val(1);
		}else{
			alert('please, choice menus to delete');
			return false;
		}
	});
	
	var submenu_type = $('input[name=type_submenu]:checked').val();
	fc_submenu_type(submenu_type);
	$('input[name=type_submenu]').click(function(){
		var submenu_type = $('input[name=type_submenu]:checked').val();
		fc_submenu_type(submenu_type);
	});
	var group = $('input[name=group]:checked').val();
	if( group == 0){
		$('.group_no').css('display','block');
	}else{
		$('.group_no').css('display','none');
	}
	$('input[name=group]').click(function(){
		var group = $('input[name=group]:checked').val();
		if( group == 0){
			$('.group_no').css('display','block');
		}else{
			$('.group_no').css('display','none');
		}
	});
	
	$(".lof-tab").click(function() {
	   iddiv = $(this).attr("rel");
	   iddiv = iddiv.substr(1);
	   loftab(iddiv);
	   return false;
	});
	if(typeof iddiv === 'undefined'){
		return false;
	}else{
		loftab(iddiv);
	}
});

function loftab(iddiv){
	$('#loftask_filter').val(iddiv);
	$(".ui-state-default").each(function(){
	 $(this).removeClass("ui-state-active");
   });               
   $(".ui-state-default").each(function(){
	var rel = $(this).find('a').attr('rel');
	if(rel == '#'+iddiv){
		$(this).addClass("ui-state-active");
	}
   });
   
   $(".ui-tabs-panel").each(function(){
	 $(this).attr("style","display:none;");
   });
   $("#" + iddiv).attr("style",'display:"";');
}

function fc_submenu_type(submenu_type){
	if( submenu_type == 1){
		$('.menu_type_modules').css('display','none');
		$('.menu_type_text').css('display','none');
		$('.menu_type_product').css('display','none');
		$('.menu_type_category').css('display','none');
		$('.menu_type_cms').css('display','none');
		$('.menu_type_import_category').css('display','none');
	}else if( submenu_type == 0){
		$('.menu_type_modules').css('display','inline');
		$('.menu_type_text').css('display','none');
		$('.menu_type_product').css('display','none');
		$('.menu_type_category').css('display','none');
		$('.menu_type_cms').css('display','none');
		$('.menu_type_import_category').css('display','none');
	}else if( submenu_type == 2){
		$('.menu_type_modules').css('display','none');
		$('.menu_type_text').css('display','inline');
		$('.menu_type_product').css('display','none');
		$('.menu_type_category').css('display','none');
		$('.menu_type_cms').css('display','none');
		$('.menu_type_import_category').css('display','none');
	}else if( submenu_type == 3){
		$('.menu_type_modules').css('display','none');
		$('.menu_type_text').css('display','none');
		$('.menu_type_product').css('display','inline');
		$('.menu_type_category').css('display','none');
		$('.menu_type_cms').css('display','none');
		$('.menu_type_import_category').css('display','none');
	}else if( submenu_type == 4){
		$('.menu_type_modules').css('display','none');
		$('.menu_type_text').css('display','none');
		$('.menu_type_product').css('display','none');
		$('.menu_type_category').css('display','inline');
		$('.menu_type_cms').css('display','none');
		$('.menu_type_import_category').css('display','none');
	}else if( submenu_type == 5){
		$('.menu_type_modules').css('display','none');
		$('.menu_type_text').css('display','none');
		$('.menu_type_product').css('display','none');
		$('.menu_type_category').css('display','none');
		$('.menu_type_cms').css('display','inline');
		$('.menu_type_import_category').css('display','none');
	}else if( submenu_type == 6){
		$('.menu_type_modules').css('display','none');
		$('.menu_type_text').css('display','none');
		$('.menu_type_product').css('display','none');
		$('.menu_type_category').css('display','none');
		$('.menu_type_cms').css('display','none');
		$('.menu_type_import_category').css('display','inline');
	}
}
function lofDelete( id ) {
	var divid = document.getElementById( id );
	divid.parentNode.removeChild( divid );
}
function lofSelectAll(obj){	
	$(obj).find("option").each(function(index,Element) {
		$(Element).attr("selected","selected");
	});	
}
function changeLanguage1(field, fieldsString, id_language_new, iso_code)
{
	var fields = fieldsString.split('-');
	for (var i = 0; i < fields.length; ++i)
	{
		getE(fields[i] + '_' + id_language).style.display = 'none';
		getE(fields[i] + '_' + id_language_new).style.display = 'block';
		getE('language_current_' + fields[i]).src = '../img/l/' + id_language_new + '.jpg';
	}
	getE('languages_' + field).style.display = 'none';
	id_language = id_language_new;
}
function toggleChecked(status) {
	$(".checkme").each( function() {
		$(this).attr("checked",status);
	});
}

function addAttributeToProducts(table_name){
	var listCheckbox = $(".form input[name*='"+table_name+"Box']:checked");
	var listAttributes = new Array();
	var htmlAttributes = "";
	if(listCheckbox.length > 0){
		var i = 0;
		listCheckbox.each(function(index,el){
			listAttributes[i]= $(this).val();
			htmlAttributes += '<input type="hidden" name="attribute_id[]" value="'+$(this).val()+'"/>';
			i++;
		});

		if($(".lofreview_back_holder")){
			$(".lofreview_back_holder").find(".attributes").eq(0).html( htmlAttributes );
			openMyModal( $(".lofreview_back_holder").html(), 280, 205 );
		}
	}
	else{
		alert("Please select an or more attribute");
	}
}
var openMyModal = function(content, width, height)
	{
		modalWindow.windowId = "myModal";
		modalWindow.width = width;//480;
		modalWindow.height = height;//405;
		modalWindow.content = content;
		modalWindow.open();
	};
var modalWindow = { 
        parent:"body",  
        windowId:null,  
        content:null,  
        width:null,  
        height:null,  
        close:function()  
        {  
            $(".modal-window").remove();  
            $(".modal-overlay").remove();  
        },  
        open:function()  
        {  
            var modal = "";  
            modal += "<div class=\"modal-overlay\"></div>";  
            modal += "<div id=\"" + this.windowId + "\" class=\"modal-window\" style=\"width:" + this.width + "px; height:" + this.height + "px; margin-top:-" + (this.height / 2) + "px; margin-left:-" + (this.width / 2) + "px;\">";  
            modal += this.content;  
            modal += "</div>";      
      
            $(this.parent).append(modal);  
      
            $(".modal-window").append("<a class=\"close-window\"></a>");  
            $(".close-window").click(function(){modalWindow.close();});  
            $(".modal-overlay").click(function(){modalWindow.close();});  
        }  
};  