$(function () {
    $('input[type=radio].star').rating();
    $('.auto-submit-star').rating();
    $('.open-comment-form').fancybox({
        'hideOnContentClick': false,
        beforeLoad: function () {
            $('#phantom').hide();
        },
        afterClose: function () {
            $('#phantom').show();
        }
    });
    $('button.usefulness_btn').click(function () {
        var id_product_comment = $(this).data('id-product-comment');
        var is_usefull = $(this).data('is-usefull');
        var parent = $(this).parent();
        $.ajax({
            url: productcomments_controller_url + '?rand=' + new Date().getTime(),
            data: {
                id_product_comment: id_product_comment,
                action: 'comment_is_usefull',
                value: is_usefull
            },
            type: 'POST',
            headers: { "cache-control": "no-cache" },
            success: function (result) {
                parent.fadeOut('slow', function () {
                    parent.remove();
                });
            }
        });
    });
    $('span.report_btn').click(function () {
        if (confirm(confirm_report_message)) {
            var idProductComment = $(this).data('id-product-comment');
            var parent = $(this).parent();
            $.ajax({
                url: productcomments_controller_url + '?rand=' + new Date().getTime(),
                data: {
                    id_product_comment: idProductComment,
                    action: 'report_abuse'
                },
                type: 'POST',
                headers: { "cache-control": "no-cache" },
                success: function (result) {
                    parent.fadeOut('slow', function () {
                        parent.remove();
                    });
                }
            });
        }
    });
    $('#submitNewMessage').click(function (event) {
        // Kill default behaviour
        event.preventDefault();
        // Form element
        url_options = parseInt(productcomments_url_rewrite) ? '?' : '&';
        $.ajax({
            url: productcomments_controller_url + url_options + 'action=add_comment&secure_key=' + secure_key + '&rand=' + new Date().getTime(),
            data: $('#new_comment_form form').serialize(),
            type: 'POST',
            headers: { "cache-control": "no-cache" },
            dataType: "json",
            success: function (data) {
                if (data.result) {
                    $.fancybox.close();
                }
                else {
                    $('#new_comment_form_error ul').html('');
                    $.each(data.errors, function (index, value) {
                        $('#new_comment_form_error ul').append('<li>' + value + '</li>');
                    });
                    $('#new_comment_form_error').slideDown('slow');
                }
            }
        });
        return false;
    });
    $('#submitNewGrade').click(function (event) {
        event.preventDefault();
        url_options = parseInt(productcomments_url_rewrite) ? '?' : '&';
        $.ajax({
            url: productcomments_controller_url + url_options + 'action=add_grade&secure_key=' + secure_key + '&rand=' + new Date().getTime(),
            data: $('#RateGrade').serialize(),
            type: 'POST',
            headers: { "cache-control": "no-cache" },
            dataType: "json",
            success: function (data) {
                if (data.result) {
                    $('#user-rate-success').html('امتیاز شما با موفقیت ثبت شد.');
                }
                else {
                    $('#new_comment_form_error ul').html('');
                    $.each(data.errors, function (index, value) {
                        $('#new_comment_form_error ul').append('<li>' + value + '</li>');
                    });
                    $('#new_comment_form_error').slideDown('slow');
                }
            }
        });
        return false;
    });
    $('#submitNewLike').click(function (event) {
        // Kill default behaviour
        event.preventDefault();
        // Form element
        url_options = parseInt(productcomments_url_rewrite) ? '?' : '&';
        var likeStat = $('.like-status');
        if(likeStat.hasClass('like-hand')){
            likeStat.removeClass('like-hand');
            likeStat.addClass('like-loading');
        }
        else if( likeStat.hasClass('like-tik') ){
            likeStat.removeClass('like-tik');
            likeStat.addClass('unlike-loading');
        }


        $.ajax({
            url: productcomments_controller_url + url_options + 'action=add_like&secure_key=' + secure_key + '&rand=' + new Date().getTime(),
            data: $('#frmLike').serialize(),
            type: 'POST',
            headers: { "cache-control": "no-cache" },
            dataType: "json",
            success: function (data) {
                if (data.errors && data.errors != '') {
                    likeStat.removeClass('like-loading');
                    likeStat.addClass('like-hand');
                    alert('برای ثبت لایک وارد سیستم شوید.');
                    console.log(1);
                }
                else {
                    $("#like-count").text(data.CountLike);
                    if (data.CustomerLike == 0) {
                        $('.like-btn').removeClass('liked');
                        $('.like-btn').addClass('like');
                        $('.like-text').text('می پسندم');
                        likeStat.removeClass('like-tik');
                        likeStat.removeClass('unlike-loading');
                        likeStat.addClass('like-hand');
                    }
                    if (data.CustomerLike == 1) {
                        $('.like-btn').removeClass('like');
                        $('.like-btn').addClass('liked');
                        $('.like-text').text('پسندیدم');
                        likeStat.removeClass('like-loading', 'unlike-loading' , 'like-hand');
                        likeStat.addClass('like-tik');
                    }
                    $('.like-count').text(data.CountLike);
                }
            },
            error: function () {
            }
        });
    });
});
