<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_f2ec0fd2abc5714608241a1a0eac321b'] = 'بخش حفظ اسرار و اطلاعات مشتریان';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_cb6b92b60c9302e74a26ac246d0af0fa'] = 'یک بلوک نمایش پیغام درباره ی اطلاعات حریم شخصی مشتری اضافه خواهد کرد.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d71315851e7e67cbacf5101c5c4ab83d'] = 'اطلاعات شخصی تهیه شده برای پایگاه داده،فرآیند سفارش ها یا دسترسی به اطلاعات خاص استفاده می شود';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_b43d08355db3df47796e65c72cfd5714'] = 'شما حق تغییر و حذف تمامی اطلاعات شخصی خود را در حساب کاربری خود دارید';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_20015706a8cbd457cbb6ea3e7d5dc9b3'] = 'تنظیمات به روز شد';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_d8b716e21c731aba4b94ba9f3ac4858c'] = 'پیغام برای حفظ اسرار و اطلاعات مشتریان';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_03e1a999dcdb904300ee1b1e767c83c9'] = 'پیغامی که در فرم ایجاد حساب کاربری نمایش داده خواهد شد.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_b51d73fb490ad1245fa9b87042bbbbb7'] = 'هشدار:اگر پیام حفظ اسرار بسیار طولانی است برای نوشتن در فرم شما می توانید یک لینک از صفحه های خود را اضافه نمائید. این کار بسیار آسان خواهد بود با ساخت صفحه ای در زیر برگه \"مدیریت محتوا\" در برگه تنظیمات';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_c9cc8cce247e49bae79f15173ce97354'] = 'ذخیره';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb32badede7c8613fddb8502d847c18b'] = 'لطفا حریم شخصی مشتری را با تایید چک باکس زیر قبول کنید.';
$_MODULE['<{blockcustomerprivacy}prestashop>blockcustomerprivacy_fb0440f9ca32a8b49eded51b09e70821'] = 'حفظ اسرار و اطلاعات مشتریان';
