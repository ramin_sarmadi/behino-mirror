    <div class="row-fluid box-row">
        <div class="span12">
            <div class="product-box-title">
                <img id="in-one-eye" src="{$img_dir}product-icons/video.png"/>
                <span class="prod-title-text">{l s="ویدئوهای مرتبط"}</span>
            </div>
        </div>
    </div>
    <div id="more_info_sheets">
        <div id="idTab15">
            <div id="product_videos_block_tab">
                {if $results_video}
                    {section name=row loop=$results_video}
                        <div class="video clearfix">
                            <img src="{$ProductVideoImageLocation}{$results_video[row].imagefile}" width="120 px"
                                 height="67 px"/>

                            <div class="video_author_infos">
                                <span style="font-size: 14px; color: #003399">{$smarty.section.row.index+1}.</span>
                                <strong>
                                    <a href="{$results_video[row].ProductVideo_Link}"
                                       target="_blank">{*l s='Video Name'*}{$results_video[row].name|escape:'html':'UTF-8'}</a>
                                </strong><br/>
                            </div>
                            <div class="video_author_infos">
                                <strong>{l s='Video Description'}:
                                    <a href="{$results_video[row].ProductVideo_Link}" target="_blank">
                                    {$results_video[row].description|escape:'html':'UTF-8'}</strong><br/>
                                </a>
                            </div>
                            <div class="video_author_infos">
                                <strong>{l s='Video Type'}
                                    :{$results_video[row].type_name|escape:'html':'UTF-8'}</strong><br/>
                            </div>
                            <em>{l s='Video Date'}
                                :{dateFormat date=$results_video[row].date_add|escape:'html':'UTF-8' full=0}</em>
                        </div>
                        {if (($smarty.section.row.max - ($smarty.section.row.index+1))!=0)}
                            <hr style="border-bottom: 1px dotted #999999"/>
                        {/if}


                    {/section}


                {*/if*}

                {else}
                    <p class="align_center">
                        {l s='ویدئوی مرتبطی وجود ندارد.'}!
                    </p>
                {/if}
            </div>
        </div>
    </div>
