<h3 class="title_category" xmlns="http://www.w3.org/1999/html">{l s='Product Video List Right Column' mod='productvideo'}</h3>

<div id="idRight">
    <div id="product_videos_block_tab">
        {if $results_video}
            {if (($id_customer>0))}

                {section name=row loop=$results_video}

                    <div class="video clearfix">

                        <div class="video_author_infos">
                            <span style="font-size: 14px; color: #003399">{$smarty.section.row.index+1}.</span>
                            <strong>
                                <a href="{$results_video[row].ProductVideo_Link}" target="_blank">{*l s='Video Name'*}{$results_video[row].name|escape:'html':'UTF-8'}</a>
                            </strong><br/>
                        </div>
                        <div class="video_author_infos">
                                <a href="{$results_video[row].ProductVideo_Link}" target="_blank">
                                    <img src="{$ProductVideoImageLocation}{$results_video[row].imagefile}" width="120 px" height="67 px" />
                                </a>
                        </div>


                        <em>{l s='Video Date'}:{dateFormat date=$results_video[row].date_add|escape:'html':'UTF-8' full=0}</em>

                    </div>

                    <hr/>


                {/section}



            {/if}

        {else}
                <p class="align_center">
                        {l s='Noting Video' mod='productvideos'}!
                </p>
        {/if}
    </div>
</div>
