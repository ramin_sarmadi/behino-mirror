<?php


class ProductVideo extends Module
{
    public function __construct()
    {
        $this->name = 'productvideo';
        $this->tab = 'advertising_marketing';
        $this->version = 1.0;
        $this->author = 'Ahmad Khorshidi';
        $this->need_instance = 0;
        parent::__construct();
        ProductVideo::initProcess();
        $this->img_fpath = _PS_UPLOAD_DIR_ . 'video/thumbnails/';
        $this->video_fpath = _PS_UPLOAD_DIR_ . 'video/';
        $this->displayName = $this->l('Product Video');
        $this->description = $this->l('Add Video Clip for Product');
    }
    public function install()
    {
        if (!parent::install() || !$this->registerHook('rightColumn') || !$this->registerHook('ProductFooter')
            || !$this->registerHook('header') || $this->_createTables() == false
        )
            return false;
        if (!file_exists($this->video_fpath))
            if (!@mkdir($this->video_fpath, 0777))
                return false;
        if (!file_exists($this->img_fpath))
            if (!@mkdir($this->img_fpath, 0777))
                return false;
        return true;
    }
    public function uninstall()
    {
        $db = Db::getInstance();
        $query = 'DROP TABLE `' . _DB_PREFIX_ . 'product_video`';
        $query1 = 'DROP TABLE `' . _DB_PREFIX_ . 'product_video_comment`';
        $query2 = 'DROP TABLE `' . _DB_PREFIX_ . 'product_video_comment_like`';
        $query3 = 'DROP TABLE `' . _DB_PREFIX_ . 'product_video_relation`';
        $query4 = 'DROP TABLE `' . _DB_PREFIX_ . 'product_video_type`';
        $result = $db->Execute($query);
        $result1 = $db->Execute($query1);
        $result2 = $db->Execute($query2);
        $result3 = $db->Execute($query3);
        $result4 = $db->Execute($query4);
        if (!parent::uninstall() OR !$result OR !$result1 OR !$result2 OR !$result3 OR !$result4)
            return false;
        return true;
    }
    public function _createTables()
    {
        $db = Db::getInstance();
        $query = '

        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'product_video` (
          `id_product_video` bigint(12) NOT NULL AUTO_INCREMENT,
          `name` varchar(255) DEFAULT NULL,
          `description` text,
          `file` text,
          `imagefile` text,
          `date_add` datetime NOT NULL,
          `type_video` int(4) DEFAULT NULL,
          `video_format` varchar(100) DEFAULT NULL,
          `tag` mediumtext,
          PRIMARY KEY (`id_product_video`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8

        ';
        $query1 = '
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'product_video_comment` (
          `id_product_video_comment` bigint(12) NOT NULL AUTO_INCREMENT,
          `id_product_video_relation` bigint(12) NOT NULL,
          `id_customer` int(10) DEFAULT NULL,
          `name_customer` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
          `video_comment` text CHARACTER SET utf8,
          `status` tinyint(1) NOT NULL DEFAULT "1",
          PRIMARY KEY (`id_product_video_comment`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8

			';
        $query2 = '
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'product_video_comment_like` (
          `id_product_video_comment_rate` bigint(12) NOT NULL AUTO_INCREMENT,
          `id_product_video_comment` bigint(12) NOT NULL,
          `id_customer` int(10) DEFAULT NULL,
          `value_like` tinyint(1) DEFAULT NULL,
          `status` tinyint(1) NOT NULL DEFAULT "1",
          `date_add` datetime NOT NULL,
          PRIMARY KEY (`id_product_video_comment_rate`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8

			';
        $query3 = '
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'product_video_relation` (
          `id_product_video_relation` bigint(12) NOT NULL AUTO_INCREMENT,
          `id_product_video` bigint(12) NOT NULL,
          `id_product` int(10) NOT NULL,
          `status` tinyint(1) NOT NULL DEFAULT "1",
          PRIMARY KEY (`id_product_video_relation`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8

			';
        $query4 = '
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'product_video_type` (
          `id_product_video_type` int(4) NOT NULL AUTO_INCREMENT,
          `type_name` varchar(255) NOT NULL,
          `width` int(5) DEFAULT NULL,
          `height` int(5) DEFAULT NULL,
          `description` text,
          PRIMARY KEY (`id_product_video_type`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

        --
        -- Dumping data for table `tbl_product_video_type`
        --

        INSERT INTO `' . _DB_PREFIX_ . 'product_video_type` (`id_product_video_type`, `type_name`, `width`, `height`, `description`) VALUES
        (1, "240 p", 320, 240, NULL),
        (2, "360 p", 560, 360, NULL),
        (3, "480 p", 720, 480, NULL),
        (4, "720 p", 1280, 720, NULL);


			';
        $result = $db->Execute($query);
        $result1 = $db->Execute($query1);
        $result2 = $db->Execute($query2);
        $result3 = $db->Execute($query3);
        $result4 = $db->Execute($query4);
        if ((!$result) || (!$result1) || (!$result2) || (!$result3) || (!$result4))
            return false;
        return true;
    }
    /*
    public function hookDisplayAdminProductsExtra($params)
    {      
        return $this->display(__FILE__, 'productvideo.tpl');
    }*/
    public function initProcess()
    {
        $action = '';
        $nameAction = '';
        $result_product = '';
        if (Tools::isSubmit('update' . $this->name)) {
            $id_product_video_relation = (int)Tools::getValue('id_product_video_relation');
            $result_product = $this->getListContent($id_product_video_relation);
            $upload_target = '../upload/video/';
            $uploadImage_target = '../upload/video/thumbnails/';
            foreach ($result_product as &$Vproduct) {
                $Vproduct['file_link'] = $upload_target . $Vproduct['file'];
                $Vproduct['image_link'] = $uploadImage_target . $Vproduct['imagefile'];
            }
            $action = 'updateData';
            $nameAction = 'Update';
        } elseif (Tools::isSubmit('add' . $this->name)) {
            $nameAction = 'Save';
            $action = 'save';
        }
        $options = Db::getInstance()->executeS('
                    SELECT pl.id_product, CONCAT(cl.name," >> ",pl.name) name
                    FROM `' . _DB_PREFIX_ . 'product_lang` pl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_product` cp on pl.id_product = cp.id_product
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_lang` cl on cp.id_category = cl.id_category
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=0 AND c.id_category>1
                    ORDER BY cl.id_category'
        );
        $options_checkbox_index_root = Db::getInstance()->executeS('
                    SELECT cl.id_category , cl.name name_category
                    FROM `' . _DB_PREFIX_ . 'category_lang` cl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=1 AND c.id_category>2
                    ORDER BY cl.id_category'
        );
        $options_type = Db::getInstance()->executeS('
                    SELECT pvt.id_product_video_type, pvt.type_name
                    FROM ' . _DB_PREFIX_ . 'product_video_type pvt
                    '
        );
        //print_r($options_type);
        $option_product_list = '';
        foreach ($options_checkbox_index_root as $row_index_root) {
            $option_product_list .= '<div style="width:960px; color:#0099AA;padding:5px; font-size:16px; background-color:#BBBBBB">
                        <span><b>' . $row_index_root['name_category'] . ': </b></span>
                        ';
            $options_checkbox_index = Db::getInstance()->executeS('
                    SELECT cl.id_category , cl.name name_category
                    FROM `' . _DB_PREFIX_ . 'category_lang` cl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.id_parent=' . $row_index_root['id_category'] . '
                    ORDER BY cl.id_category'
            );
            $options_checkbox = Db::getInstance()->executeS('
                    SELECT pl.id_product ,cl.id_category, cl.name name_category , pl.name name_product
                    FROM `' . _DB_PREFIX_ . 'product_lang` pl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_product` cp on pl.id_product = cp.id_product
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_lang` cl on cp.id_category = cl.id_category
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=0 AND c.id_category>1 AND c.id_parent=' . $row_index_root['id_category'] . '
                    ORDER BY cl.id_category'
            );
            foreach ($options_checkbox_index as $row_index) {
                $option_product_list .= '<div style="width:960px; color:#000000; font-size:12px;  background-color:#CCCCCC">
            <!--<input type="checkbox" value="' . $row_index['id_category'] . '" name="id_category[]" />-->
            <span><b>' . $row_index['name_category'] . '</b></span>

            ';
                $option_product_list .= '<ul style="width:960px; float:right; color:#000000">';
                $count = 1;
                foreach ($options_checkbox as $row) {
                    if ($row['id_category'] == $row_index['id_category']) {
                        $option_product_list .= '<li style="width:300px; float: left; display:inline;">' . $count . '.
                    <input type="checkbox" value="' . $row['id_product'] . '" name="Product_id[]" />' . $row['name_product'] . '
                    </li>';
                        $count++;
                    }
                }
                $option_product_list .= '</ul>';
                $option_product_list .= '</div>';
            }
            $option_product_list .= '</div><hr/>';
        }
        $this->context->smarty->assign(array(
            'VideoProductImage' => $nameAction . ' Special Product'
        , 'IconVideoProductImage' => '../modules/' . $this->name . '/logo.png'
        , 'LinkSave' => AdminController::$currentIndex . '&configure=' . $this->name . '&' . $action . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules')
        , 'LinkBack' => AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules')
            //,'sd'=>    AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token=' . Tools::getAdminTokenLite('AdminModules')
        , 'VideoType' => $options_type
        , 'ListProductArr' => $option_product_list
        , 'ProductArr' => $this->getListProduct($this->context->language->id)
        , 'Action' => $action
        , 'nameAction' => $nameAction
        , 'ResultVideoProductArr' => $result_product
        ));
    }
    public function getContent()
    {
        $html = '';
        //$content = $this->displayForm();
        //return $content;
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        //$parameter_wiki = (string)Tools::getValue('parameter_wiki_'.$id_lang_default);
        //$description_wiki = (string)(Tools::getValue('description_wiki_'.$id_lang_default));
        //$date_add = date("Y-m-d H:i:s");
        if (Tools::isSubmit('save' . $this->name) || (Tools::isSubmit('updateData' . $this->name))) {
            $output = null;
            if (Tools::isSubmit('save' . $this->name)) {
                $id_product = (Tools::getValue('Product_id'));
                $name = strval(Tools::getValue('Name'));
                $description = strval(Tools::getValue('Des_video'));
                $date = date("Y-m-d H:i:s");
                $typeVideo = strval(Tools::getValue('Type_video'));
                $tags = strval(Tools::getValue('Tags'));
                $file = $_FILES['file']['name'];
                $video_format = $_FILES['file']['type'];
                $image_format = $_FILES['imagefile']['type'];
                if (($video_format != 'video/mp4') && ($video_format != 'video/x-flv')) {
                    $output .= $this->displayError($this->l('Invalid video format! use (mp4 OR flv) for video product.'));
                }
                if (($image_format != 'image/jpeg') && ($image_format != 'image/pjpeg') && ($image_format != 'image/png') && ($image_format != 'image/x-png')) {
                    $output .= $this->displayError($this->l('Invalid image format! use (jpeg OR png) for thumbnails.'));
                }
                if ((!$name || empty($name) || !Validate::isGenericName($name) || (!$id_product) || (!$typeVideo) || (!$file))) {
                    $output .= $this->displayError($this->l('Invalid Data value! Please try again.'));
                } elseif (($video_format != 'video/mp4') && ($video_format != 'video/x-flv') && ($image_format != 'image/jpeg')
                    && ($image_format != 'image/pjpeg') && ($image_format != 'image/png') && ($image_format != 'image/x-png')
                ) {
                    $output .= $this->displayError($this->l('Invalid Data for add video product.'));
                } else {
                    $upload_target = _PS_UPLOAD_DIR_ . 'video/';
                    $uploadImage_target = _PS_UPLOAD_DIR_ . 'video/thumbnails/';
                    if ($_FILES["file"]["error"] > 0) {
                        //echo "Error: " . $_FILES["file"]["error"] . "<br />";
                    } else {
                        $TF = (explode(".", $_FILES["file"]["name"]));
                        $TFCount = count($TF);
                        $FILENAME = ((date('Ymd') * rand(12345, 99999)) . (date('Ymd') * rand(1234, 9999))) . '.' . $TF[$TFCount - 1];
                        $TARGET_UPLOAD = $upload_target;
                        $target_N = $TARGET_UPLOAD . $FILENAME;
                        $TF_Image = (explode(".", $_FILES["imagefile"]["name"]));
                        $TFCount_Image = count($TF_Image);
                        $FILENAME_Image = ((date('Ymd') * rand(12345, 99999)) . (date('Ymd') * rand(1234, 9999))) . '.' . $TF_Image[$TFCount_Image - 1];
                        $TARGET_UPLOAD_Image = $uploadImage_target;
                        $target_N_Image = $TARGET_UPLOAD_Image . $FILENAME_Image;
                        if ((move_uploaded_file(($_FILES["imagefile"]['tmp_name']), $target_N_Image))) {
                            $output .= $this->displayConfirmation($this->l('Add Thumbnail File ' . $name));
                            if ((move_uploaded_file(($_FILES["file"]['tmp_name']), $target_N))) {
                                $id_products = '';
                                foreach ($id_product as $row_id_product) {
                                    $id_products .= ',' . $row_id_product;
                                }
                                $id_products = substr($id_products, 1);
                                //echo '<div dir="ltr">'.$id_products;
                                $id_product_video_arr = $this->addVideo($name, $description, $FILENAME, $FILENAME_Image, $date, $typeVideo, $video_format, $tags);
                                $id_product_video = ($id_product_video_arr[0]['id_product_video']);
                                $this->addProductsVideo($id_product_video, $id_products, 1);
                                $output .= $this->displayConfirmation($this->l('Add Video File ' . $name));
                            } else {
                                $output .= $this->displayConfirmation($this->l('Do not Add Video Product file for ' . $name . ' '));
                                //return false; //فایل ارسال نشد
                            }
                        } else {
                            $output .= $this->displayConfirmation($this->l('Do not Add Image Video Product file for ' . $name . ' '));
                            //return false; //فایل ارسال نشد
                        }
                    }
                    $output .= $this->displayConfirmation($this->l('Add Video Product record about ' . $name . ' successfully'));
                }
            } elseif (Tools::isSubmit('updateData' . $this->name)) {
                $id_product_video_relation = (int)Tools::getValue('id_product_video_relation');
                $result_product = $this->getListContent($id_product_video_relation);
                $upload_target = _PS_UPLOAD_DIR_ . 'video/';
                $uploadImage_target = _PS_UPLOAD_DIR_ . 'video/thumbnails/';
                foreach ($result_product as &$Vproduct) {
                    $Vproduct['file_link'] = $upload_target . $Vproduct['file'];
                    $Vproduct['image_link'] = $uploadImage_target . $Vproduct['imagefile'];
                }
                $id_product = (Tools::getValue('id_product'));
                $name = strval(Tools::getValue('Name'));
                $description = strval(Tools::getValue('Des_video'));
                $date = date("Y-m-d H:i:s");
                $typeVideo = strval(Tools::getValue('Type_video'));
                $tags = strval(Tools::getValue('Tags'));
                $file = $_FILES['file']['name'];
                $imagefile = $_FILES['imagefile']['name'];
                $video_format = $_FILES['file']['type'];
                $image_format = $_FILES['imagefile']['type'];
                if (($video_format != 'video/mp4') && ($video_format != 'video/x-flv')) {
                    $output .= $this->displayError($this->l('Invalid video format! use (mp4 OR flv) for video product.'));
                }
                if (($image_format != 'image/jpeg') && ($image_format != 'image/pjpeg') && ($image_format != 'image/png') && ($image_format != 'image/x-png')) {
                    $output .= $this->displayError($this->l('Invalid image format! use (jpeg OR png) for thumbnails.'));
                }
                if ((!$name || empty($name) || !Validate::isGenericName($name) || (!$id_product) || (!$typeVideo))) {
                    $output .= $this->displayError($this->l('Invalid Data value! Please try again.'));
                }
                if ($_FILES["file"]["error"] > 0) {
                    //echo "Error: " . $_FILES["file"]["error"] . "<br />";
                } else {
                    if (file_exists($Vproduct['file_link'])) {
                        unlink($Vproduct['file_link']);
                        $TF = (explode(".", $_FILES["file"]["name"]));
                        $TFCount = count($TF);
                        $FILENAME = ((date('Ymd') * rand(12345, 99999)) . (date('Ymd') * rand(1234, 9999))) . '.' . $TF[$TFCount - 1];
                        $TARGET_UPLOAD = $upload_target;
                        $target_N = $TARGET_UPLOAD . $FILENAME;
                        if ((move_uploaded_file(($_FILES["file"]['tmp_name']), $target_N))) {
                            $output .= $this->displayConfirmation($this->l('Add Video File ' . $name));
                        } else {
                            $output .= $this->displayConfirmation($this->l('Do not Add Video Product file for ' . $name . ' '));
                            //return false; //فایل ارسال نشد
                        }
                    } else {
                    }
                }
                if ($_FILES["imagefile"]["error"] > 0) {
                    //echo "Error: " . $_FILES["file"]["error"] . "<br />";
                } else {
                    if (file_exists($Vproduct['image_link'])) {
                        unlink($Vproduct['image_link']);
                        $TF_Image = (explode(".", $_FILES["imagefile"]["name"]));
                        $TFCount_Image = count($TF_Image);
                        $FILENAME_Image = ((date('Ymd') * rand(12345, 99999)) . (date('Ymd') * rand(1234, 9999))) . '.' . $TF_Image[$TFCount_Image - 1];
                        $TARGET_UPLOAD_Image = $uploadImage_target;
                        $target_N_Image = $TARGET_UPLOAD_Image . $FILENAME_Image;
                        if ((move_uploaded_file(($_FILES["imagefile"]['tmp_name']), $target_N_Image))) {
                            $output .= $this->displayConfirmation($this->l('Add Thumbnail File ' . $name));
                        } else {
                            $output .= $this->displayConfirmation($this->l('Do not Add Image Video Product file for ' . $name . ' '));
                            //return false; //فایل ارسال نشد
                        }
                    } else {
                    }
                }
                $id_products = $id_product;
                if ((!$file)) {
                    $FILENAME_File = $Vproduct['file'];
                } else {
                    $FILENAME_File = $FILENAME;
                }
                if ((!$imagefile)) {
                    $FILENAME_Imagefile = $Vproduct['imagefile'];
                } else {
                    $FILENAME_Imagefile = $FILENAME_Image;
                }
                $dataArr = array(
                    'name' => $name
                , 'description' => $description
                , 'file' => $FILENAME_File
                , 'imagefile' => $FILENAME_Imagefile
                , 'date_add' => $date
                , 'type_video' => $typeVideo
                , 'video_format' => $video_format
                , 'tag' => $tags
                );
                $result_add = $this->update('product_video', $this->context->language->id, $dataArr, 'id_product_video=' . $result_product[0]['id_product_video']);
                if ($result_add == 'OK') {
                    $output .= $this->displayConfirmation($this->l('Update Video Product record for <b>' . $name . '</b> successfully'));
                } else {
                    $output .= $this->displayConfirmation($this->l('<font color="#000000">Update Video Product record for <b>' . $name . '</b> <b>unsuccessfully</b></font>'));
                }
                $dataArrR = array(
                    'id_product' => $id_product
                );
                $result_addR = $this->update('product_video_relation', $this->context->language->id, $dataArrR, 'id_product_video_relation=' . $result_product[0]['id_product_video_relation']);
                if ($result_addR == 'OK') {
                    $output .= $this->displayConfirmation($this->l('Update relation Product record for <b>' . $name . '</b> successfully'));
                } else {
                    $output .= $this->displayConfirmation($this->l('<font color="#000000">Update relation Product record for <b>' . $name . '</b> <b>unsuccessfully</b></font>'));
                }
                $output .= $this->displayConfirmation($this->l('Update Video Product record about <b>' . $name . '</b> successfully'));
            } else {
                $output .= $this->displayConfirmation($this->l('No Action!!!'));
            }
            $helper = $this->initList();
            return $output . $helper->generateList($this->getListContent(), $this->fields_list);
        }
        if (Tools::isSubmit('update' . $this->name) || Tools::isSubmit('add' . $this->name)) {
            //$content = $this->displayForm($id_product_video_relation);
            //return $content;
            return $this->display(__FILE__, 'productvideo.tpl');
        } elseif (Tools::isSubmit('delete' . $this->name)) {
            $result = $this->deleted((int)Tools::getValue('id_product_video_relation'));
            //Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&action=deleted&result='.$result.'&token='.Tools::getAdminTokenLite('AdminModules'));
        } elseif (Tools::getValue('action') == 'deleted') {
            $result = Tools::getValue('result');
            if ($result == 'OK') {
                $html .= '<div class="conf confirm">' . $this->l('Deleted record from database.') . '</div>';
            } elseif ($result == '100') {
                $html .= '<div class="conf confirm">' . $this->l('Miss Data.') . '</div>';
            } else {
                $html .= '<div class="conf confirm">' . $this->l('You have error in add data.') . '</div>';
            }
            $helper = $this->initList();
            return $html . $helper->generateList($this->getListContent(), $this->fields_list);
        } else {
            $helper = $this->initList();
            return $html . $helper->generateList($this->getListContent(), $this->fields_list);
        }
    }
    protected function deleted($id_product_video_relation)
    {
        if ($id_product_video_relation > 0) {
            $sub_query = ' WHERE pvr.id_product_video_relation=' . $id_product_video_relation;
            $SQL_Product = '
            SELECT COUNT(pvr.id_product_video_relation) as Count_Rec, pv.id_product_video
            , pv.imagefile , pv.file
            FROM `' . _DB_PREFIX_ . 'product_video_relation` pvr
            INNER JOIN `' . _DB_PREFIX_ . 'product_video` pv ON pvr.`id_product_video` = pv.`id_product_video`
            INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON pvr.`id_product` = pl.`id_product`
            INNER JOIN `' . _DB_PREFIX_ . 'product_video_type` pvt ON pv.`type_video` = pvt.`id_product_video_type`
            ' . $sub_query;
            $Count_Data = Db::getInstance()->executeS($SQL_Product);
            //print_r($Count_Data);
            if ($Count_Data[0]['Count_Rec'] == 1) {
                $upload_target = _PS_UPLOAD_DIR_ . 'video/';
                $uploadImage_target = _PS_UPLOAD_DIR_ . 'video/thumbnails/';
                $SQL = '
             DELETE FROM `' . _DB_PREFIX_ . 'product_video_relation` WHERE  id_product_video_relation IN (' . $id_product_video_relation . ')
            ';
                Db::getInstance()->executeS($SQL);
                $SQL = '
             DELETE FROM `' . _DB_PREFIX_ . 'product_video` WHERE  id_product_video IN (' . $Count_Data[0]['id_product_video'] . ')
            ';
                Db::getInstance()->executeS($SQL);
                if ((unlink($upload_target . $Count_Data[0]['file'])) && (unlink($upload_target . $Count_Data[0]['imagefile']))) {
                    return 'OK'; //success deleted
                } else {
                    return 'No Deleted'; //success deleted
                }
            }
            $SQL = '
             DELETE FROM `' . _DB_PREFIX_ . 'product_video_relation` WHERE  id_product_video_relation IN (' . $id_product_video_relation . ')
            ';
            Db::getInstance()->executeS($SQL);
            return 'OK'; //success deleted
        }
        return 100; //miss data
    }
    protected function initList()
    {
        $this->fields_list = array(
            'id_product_video_relation' => array(
                'title' => $this->l('Id'),
                'width' => 30,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'id_product_video' => array(
                'title' => $this->l('Id Parent'),
                'width' => 40,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'name' => array(
                'title' => $this->l('Video Name'),
                'width' => 140,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'product_name' => array(
                'title' => $this->l('Product Name'),
                'width' => 140,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'video_format' => array(
                'title' => $this->l('Video Format'),
                'width' => 60,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'type_name' => array(
                'title' => $this->l('Video Type'),
                'width' => 60,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            ),
            'tag' => array(
                'title' => $this->l('Tag'),
                'width' => 80,
                'type' => 'text',
                'filter_key' => 'a!lastname'
            )
        );
        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = true;
        $helper->identifier = 'id_product_video_relation';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] = array(
            'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&add' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new')
        );
        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        return $helper;
    }
    protected function getListContent($id_product_video_relation = 0)
    {
        if ($id_product_video_relation > 0) {
            $sub_query = ' WHERE pvr.id_product_video_relation=' . $id_product_video_relation;
        }
        $SQL = '
            SELECT pvr.id_product_video_relation,pvr.id_product
            ,pv.id_product_video,pv.description, pv.file, pv.imagefile
            ,pv.name ,pl.name as product_name,pvt.type_name
            , pvt.id_product_video_type ,pv.video_format,pv.tag
            FROM `' . _DB_PREFIX_ . 'product_video_relation` pvr
            INNER JOIN `' . _DB_PREFIX_ . 'product_video` pv ON pvr.`id_product_video` = pv.`id_product_video`
            INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON pvr.`id_product` = pl.`id_product`
            INNER JOIN `' . _DB_PREFIX_ . 'product_video_type` pvt ON pv.`type_video` = pvt.`id_product_video_type`
            ' . $sub_query;
        return Db::getInstance()->executeS($SQL);
    }
    public function displayForm_OLD($id_product_video_relation = 0)
    {
        if ($id_product_video_relation > 0) {
            $result_product = $this->getListContent($id_product_video_relation);
            //print_r($result_product);
        }
        $options = Db::getInstance()->executeS('
                    SELECT pl.id_product, CONCAT(cl.name," >> ",pl.name) name
                    FROM `' . _DB_PREFIX_ . 'product_lang` pl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_product` cp on pl.id_product = cp.id_product
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_lang` cl on cp.id_category = cl.id_category
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=0 AND c.id_category>1
                    ORDER BY cl.id_category'
        );
        $options_checkbox_index_root = Db::getInstance()->executeS('
                    SELECT cl.id_category , cl.name name_category
                    FROM `' . _DB_PREFIX_ . 'category_lang` cl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=1 AND c.id_category>2
                    ORDER BY cl.id_category'
        );
        $options_type = Db::getInstance()->executeS('
                    SELECT pvt.id_product_video_type, pvt.type_name
                    FROM ' . _DB_PREFIX_ . 'product_video_type pvt
                    '
        );
        //print_r($options_type);
        $option_product_list = '';
        foreach ($options_checkbox_index_root as $row_index_root) {
            $option_product_list .= '<div style="width:500px; color:#0099AA; font-size:16px; background-color:#BBBBBB">
                        <span><b>' . $row_index_root['name_category'] . ': </b></span>
                        ';
            $options_checkbox_index = Db::getInstance()->executeS('
                    SELECT cl.id_category , cl.name name_category
                    FROM `' . _DB_PREFIX_ . 'category_lang` cl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.id_parent=' . $row_index_root['id_category'] . '
                    ORDER BY cl.id_category'
            );
            $options_checkbox = Db::getInstance()->executeS('
                    SELECT pl.id_product ,cl.id_category, cl.name name_category , pl.name name_product
                    FROM `' . _DB_PREFIX_ . 'product_lang` pl
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_product` cp on pl.id_product = cp.id_product
                    INNER JOIN  `' . _DB_PREFIX_ . 'category_lang` cl on cp.id_category = cl.id_category
                    INNER JOIN  `' . _DB_PREFIX_ . 'category` c on cl.id_category = c.id_category
                    AND c.is_root_category=0 AND c.id_category>1 AND c.id_parent=' . $row_index_root['id_category'] . '
                    ORDER BY cl.id_category'
            );
            foreach ($options_checkbox_index as $row_index) {
                $option_product_list .= '<div style="width:200px; color:#000000; font-size:12px;  background-color:#CCCCCC">
            <!--<input type="checkbox" value="' . $row_index['id_category'] . '" name="id_category[]" />-->
            <span><b>' . $row_index['name_category'] . '</b></span>

            ';
                $option_product_list .= '<ul style="width:200px; float:right; color:#000000">';
                foreach ($options_checkbox as $row) {
                    if ($row['id_category'] == $row_index['id_category']) {
                        $option_product_list .= '<li style="width:200px; float: right; display:inline;">
                    <input type="checkbox" value="' . $row['id_product'] . '" name="Product_id[]" />' . $row['name_product'] . '
                    </li>';
                    }
                }
                $option_product_list .= '</ul>';
                $option_product_list .= '</div>';
            }
            $option_product_list .= '</div><hr/>';
        }
        // Get default Language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Information Product Video'), 'desc' => $option,
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name Video'),
                    'name' => 'Name',
                    'size' => 53,
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description Video'),
                    'name' => 'Des_video',
                    'rows' => 3,
                    'cols' => 50,
                    'required' => false
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('File'),
                    'name' => 'file',
                    'desc' => $this->l('MP4 or FLV file'),
                    'size' => 10,
                    'required' => true
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Thumbnail File'),
                    'name' => 'imagefile',
                    'desc' => $this->l('JPEG or PNG file'),
                    'size' => 10,
                    'required' => true
                ),
                /* array(
                     'type' => 'select',
                     'label' => $this->l('Products'),
                     'desc' => $this->l('Choose a product'),
                     'name' => 'Product_id[]',
                     'class' => 'ltr',
                     'required' => true,
                     'options' => array(
                     'query' => $options,
                     'id' => 'id_product',      // The value of the 'id' key must be the same as the key for 'value' attribute of the <option> tag in each $options sub-array.
                     'name' => 'name'          // The value of the 'name' key must be the same as the key for the text content of the <option> tag in each $options sub-array.
                     )
                 ),*/
                array(
                    'type' => 'select',
                    'label' => $this->l('Video Type'),
                    'desc' => $this->l('Choose a video type'),
                    'name' => 'Type_video',
                    'required' => true,
                    'options' => array(
                        'query' => $options_type,
                        'id' => 'id_product_video_type', // The value of the 'id' key must be the same as the key for 'value' attribute of the <option> tag in each $options sub-array.
                        'name' => 'type_name' // The value of the 'name' key must be the same as the key for the text content of the <option> tag in each $options sub-array.
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Video tags'),
                    'name' => 'Tags',
                    'size' => 53,
                    'required' => false
                ),
                array(
                    'type' => 'tags',
                    'label' => $this->l('Products List'),
                    'name' => 'products',
                    'lang' => true,
                    //'hint' => $this->l('Forbidden characters:').' <>;=#{}',
                    'desc' => $option_product_list
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        $helper = new HelperForm();
        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        // Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true; // false -> remove toolbar
        $helper->toolbar_scroll = true; // yes - > Toolbar is always visible on the top of the screen .
        $helper->submit_action = 'submit' . $this->name;
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
        // Load current value
        //$helper->fields_value['MYMODULE_NAME'] = Configuration::get('MYMODULE_NAME');
        return $helper->generateForm($fields_form);
    }
    public function hookHeader($params)
    {
        $this->context->controller->addCSS($this->_path . 'productvideos.css', 'all');
    }
    public function hookRightColumn()
    {
        $id_products = (int)(Tools::getValue('id_product'));
        $result_prd = ProductVideo::getLastProductvideo(3, 1);
        $result = ProductVideo::CreateProductVideoResult($result_prd);
        $this->context->smarty->assign(
            array(
                'id_customer' => (int)$this->context->cookie->id_customer,
                'ProductVideoImageLocation' => 'upload/video/thumbnails/',
                'results_video' => $result
            )
        );
        return $this->display(__FILE__, '/right.tpl');
    }
    public function hookProductFooter($params)
    {
        $id_products = (int)(Tools::getValue('id_product'));
        $result_prd = ProductVideo::getProductvideo($id_products, 1);
        $result = ProductVideo::CreateProductVideoResult($result_prd);
        $this->context->smarty->assign(
            array(
                'id_customer' => (int)$this->context->cookie->id_customer,
                'results_video' => $result
            )
        );
        return $this->display(__FILE__, '/tab.tpl');
    }
    protected function getListProduct($id_lang, $id_product = 0)
    {
        $sub_query = '';
        if ($id_product > 0) {
            $sub_query = 'AND pl.id_product=' . $id_product;
        }
        $SQL = '
			SELECT id_product, name
			FROM `' . _DB_PREFIX_ . 'product_lang` pl
			WHERE pl.`id_lang` = ' . (int)$id_lang . ' ' . ($sub_query) . ' ORDER BY id_product DESC';
        return Db::getInstance()->executeS($SQL);
    }
    public function getProductvideo($id_products, $status = 1)
    {
        return $result = Db::getInstance()->executeS('

        SELECT pvr.id_product , pv.*, pvt.type_name, pvt.description as description_type_video
        FROM `' . _DB_PREFIX_ . 'product_video_relation` pvr
        INNER JOIN `' . _DB_PREFIX_ . 'product_video` pv ON pv.id_product_video = pvr.id_product_video
        INNER JOIN `' . _DB_PREFIX_ . 'product_video_type` pvt ON pv.type_video = pvt.id_product_video_type

        WHERE pvr.id_product IN (' . $id_products . ') AND pvr.status=' . $status . '

        ORDER BY type_video

        '
        );
    }
    public function getLastProductvideo($limit, $status = 1)
    {
        return $result = Db::getInstance()->executeS('

        SELECT (SELECT pvr.id_product FROM `' . _DB_PREFIX_ . 'product_video_relation` pvr
        WHERE pvr.id_product_video=pv.id_product_video ORDER BY id_product Limit 1 ) as id_product
        , pv.*, pvt.type_name, pvt.description as description_type_video
        FROM `' . _DB_PREFIX_ . 'product_video` pv
        INNER JOIN `' . _DB_PREFIX_ . 'product_video_type` pvt ON pv.type_video = pvt.id_product_video_type

        ORDER BY id_product_video DESC

        LIMIT ' . $limit . '

        '
        );
    }
    public function getVideo($id_products = 0, $ids_product_video = 0, $status = 1, $Limit = 0)
    {
        $SQL_WHERE = '';
        if ($id_products > 0) {
            $SQL_WHERE .= 'AND pvr.id_product IN (' . $id_products . ')';
        }
        if ($ids_product_video > 0) {
            $SQL_WHERE .= 'AND pv.id_product_video IN (' . $ids_product_video . ')';
        }
        if ($Limit > 0) {
            $SQL_Limit = 'LIMIT ' . $Limit . '';
        }
        $SQL = '

        SELECT pv.id_product_video,pvr.id_product , pv.*, pvt.type_name,
         pvt.width, pvt.height, pvt.description as description_type_video,
         pl.name as product_name
        FROM `' . _DB_PREFIX_ . 'product_video_relation` pvr
        INNER JOIN `' . _DB_PREFIX_ . 'product_video` pv ON pv.id_product_video = pvr.id_product_video
        INNER JOIN `' . _DB_PREFIX_ . 'product_video_type` pvt ON pv.type_video = pvt.id_product_video_type
        INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON pl.id_product = pvr.id_product

        WHERE
        pvr.status=' . $status . '
        ' . $SQL_WHERE . '

        ORDER BY pv.id_product_video,type_video DESC
        ' . $SQL_Limit . '

        ';
        return $result = Db::getInstance()->executeS($SQL);
    }
    public function getRelatedVideo($not_id_products, $ids_product_video, $status = 1)
    {
        return $result = Db::getInstance()->executeS('

        SELECT pvr.id_product , pv.*, pvt.type_name, pvt.width, pvt.height, pvt.description as description_type_video, pl.name as product_name
        FROM `' . _DB_PREFIX_ . 'product_video_relation` pvr
        INNER JOIN `' . _DB_PREFIX_ . 'product_video` pv ON pv.id_product_video = pvr.id_product_video
        INNER JOIN `' . _DB_PREFIX_ . 'product_video_type` pvt ON pv.type_video = pvt.id_product_video_type
        INNER JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON pl.id_product = pvr.id_product

        WHERE
        pvr.id_product NOT IN (' . $not_id_products . ')
        AND pv.id_product_video IN (' . $ids_product_video . ')
        AND pvr.status=' . $status . '

        ORDER BY type_video

        '
        );
    }
    public function CreateProductVideoResult($result)
    {
        $result_arr = array();
        //$result_arr1 = array();
        $count = 0;
        foreach ($result as $row) {
            $result_arr1 = array();
            $Link_Arr = array('ProductVideo_Link' => 'index.php?controller=video&id_video=' . $row['id_product_video'] . '&id_product=' . $row['id_product']);
            //echo '<div dir="ltr">'.print_r($result_arr).'</div>';
            $result_arr1 = array_merge($row, $Link_Arr);
            $result_arr[$count] = array_merge($result_arr, $result_arr1);
            //echo '<div dir="ltr">'.($row['file']).'</div>';
            //echo '<div dir="ltr">'.($result[0]['id_product_video']).'</div>';
            //echo '<div dir="ltr">'.print_r($result_arr).'</div>';
            $count++;
        }
        return $result_arr;
    }
    public function ProcessAddVideo()
    {
        $id_product = Tools::getValue('id_product');
        $text = Tools::getValue('Des_video');
        $ProductVideo = new ProductVideo();
        if ($text) {
            $ProductVideo->addComent($id_product, $text);
        }
    }
    public function addVideo($name, $description, $file, $imagefile, $date_add, $type_video, $video_format, $tag)
    {
        (Db::getInstance()->execute('
			INSERT INTO `' . _DB_PREFIX_ . 'product_video` (`name`, `description`, `file`, `imagefile`, `date_add`, `type_video`, `video_format`, `tag`)
			VALUES ("' . $name . '" ,"' . $description . '" ,"' . $file . '" ,"' . $imagefile . '" ,"' . $date_add . '" ,"' . $type_video . '","' . $video_format . '","' . $tag . '")'));
        return $id_product_video = Db::getInstance()->executeS('
                    SELECT pv.id_product_video
                    FROM ' . _DB_PREFIX_ . 'product_video pv
                    ORDER BY id_product_video DESC LIMIT 1
                    '
        );
    }
    public function addProductsVideo($id_product_video, $id_products, $status)
    {
        /*  echo '
              INSERT INTO `' . _DB_PREFIX_ . 'product_video_relation` (`id_product_video`, `id_product`, `status`)
              SELECT ' . $id_product_video . ',p.id_product,' . $status . ' FROM ' . _DB_PREFIX_ . 'product p
              WHERE p.id_product IN ('.$id_products.')
              ';*/
        return (Db::getInstance()->execute('
			INSERT INTO `' . _DB_PREFIX_ . 'product_video_relation` (`id_product_video`, `id_product`, `status`)
			SELECT ' . $id_product_video . ',p.id_product,' . $status . ' FROM ' . _DB_PREFIX_ . 'product p
			WHERE p.id_product IN (' . $id_products . ')
			'));
    }
    protected function update($table, $id_lang, $dataArr, $where)
    {
        if (($table) && ($dataArr) && ($where)) {
            if ((Db::getInstance()->update($table, $dataArr, $where))) {
                return 'OK';
            } else {
                return false;
            }
        } else {
            return '100'; //Miss data
        }
    }
}

?>