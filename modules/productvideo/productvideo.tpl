<div class="toolbarBox ">
    <ul class="cc_button">
        <li>
            <a id="desc--back" class="toolbar_btn" title="back" href="{$LinkBack}">
                <span class="process-icon-back "></span>
                <div>{l s='Back' mod='Videoproduct'}</div>
            </a>
        </li>
    </ul>

    <div class="pageTitle">
        <h3>
        <span id="current_obj" style="font-weight: normal;">
        <span class="breadcrumb item-0 ">
                {$VideoProductImage}
        </span>
        </span>
        </h3>
    </div>
</div>
<fieldset id="fieldset_0"><legend><img src="{$IconVideoProductImage}" /> {$VideoProductImage} </legend>
    {if $Action=='updateData'}
        <div style="color: #990000; font-size: 14px; font-weight: bold">

            توجه! به غیر از نام محصول مرتبط مابقی فیلدها با دیگر رکوردهای این ویدئو مشترک می باشد.

        </div>
    {/if}
    <form action="{$LinkSave}" method="post" enctype="multipart/form-data">
        <input name="id_product_video_relation" value="{$ResultVideoProductArr[0]['id_product_video_relation']}" type="hidden">
        <span class="span_bold_12">{l s='Video Name' mod='Videoproduct'}:</span>
        <div style="padding: 5px">
            <input name="Name" value="{$ResultVideoProductArr[0]['name']}" maxlength="2000" style="width: 335px" >
        </div>

        <span class="span_bold_12">{l s='Description' mod='Videoproduct'}:</span>
        <div style="padding: 5px">
            <textarea cols="50" rows="3" name="Des_video">{$ResultVideoProductArr[0]['description']}</textarea>
        </div>

        <span class="span_bold_12">{l s='Video Type' mod='Videoproduct'}:</span>
        <div style="padding: 5px">
            <select name="Type_video" dir="ltr">
                <option value="-1">...</option>
                {foreach from=$VideoType item=VideoTypes}
                    <option value="{$VideoTypes['id_product_video_type']}"
                            {if $VideoTypes['id_product_video_type']==$ResultVideoProductArr[0]['id_product_video_type']} selected="selected" {/if} >
                        {$VideoTypes['id_product_video_type']}. {$VideoTypes['type_name']}
                    </option>
                {/foreach}
            </select>
        </div>

        <span class="span_bold_12">{l s='File' mod='Videoproduct'}:</span>
        <div style="padding: 5px">
            <input type="file" name="file">
            <p class="preference_description"> MP4 or FLV file
            {if $Action=='updateData'}
                --->
                <a href="{$ResultVideoProductArr[0]['file_link']}" style="color: #003399" target="_blank">
                    Download File
                </a>

            {/if}
            </p>
        </div>

        <span class="span_bold_12">{l s='Thumbnail File' mod='Videoproduct'}:</span>
        <div style="padding: 5px">
            <input type="file" name="imagefile">
            <p class="preference_description"> JPEG or PNG file
            {if $Action=='updateData'}
                --->
                <a href="{$ResultVideoProductArr[0]['image_link']}" style="color: #003399" target="_blank">
                    Download File
                </a>

            {/if}
            </p>
        </div>

        <span class="span_bold_12">{l s='Video Name' mod='Videoproduct'}:</span>
        <div style="padding: 5px">
            <input name="Tags" value="{$ResultVideoProductArr[0]['tag']}" maxlength="2000" style="width: 335px" >
        </div>

        {if $Action=='save'}

            <div style="padding: 5px">
                <span class="span_bold_12">{l s='Product Name' mod='Videoproduct'}:</span>
                {$ListProductArr}
            </div>

        {elseif $Action=='updateData'}
            <div style="padding: 5px">
                <span class="span_bold_12">{l s='Product Name' mod='specialproduct'}:</span>
                <select name="id_product" dir="ltr">
                    {foreach from=$ProductArr item=Product}
                        <option value="{$Product['id_product']}"
                                {if $Product['id_product']==$ResultVideoProductArr[0]['id_product']} selected="selected" {/if}
                                >{$Product['id_product']}. {$Product['name']}</option>
                    {/foreach}
                </select>
            </div>
        {/if}

        <div style="padding-left: 150px; padding-top: 10px">
            <input type="submit" value="{l s=$nameAction mod='Videoproduct'}" class="button" name="AddProductLink">
        </div>


    </form>
</fieldset>
