<?php
/**
 * Lof Deal Module
 * @version        $Id: file.php $Revision
 * @package        modules
 * @subpackage    $Subpackage.
 * @copyright    Copyright (C) September 2012 prestafox.com <@emai:prestafox@gmail.com>.All rights reserved.
 * @license        GNU General Public License version 2
 */
/**
 * @since 1.5.0
 */
class ClassLofDeal
{
    protected static $_specificPriceCache = array();
    public static function getList($module, $where = '', $page_number = 0, $nb_products = 10, $orderby = '', $showdeal = 'recently', $count = false)
    {
        $status = 1;
        switch ($showdeal) {
            case 'recently':
                $status = 1;
                break;
            case 'future':
                $status = 0;
                break;
            case 'finished':
                $status = 2;
                break;
            case 'recently,future':
                $status = 3;
                break;
            case 'recently,finished':
                $status = 4;
                break;
            case 'future,finished':
                $status = 5;
                break;
            case 'recently,future,finished':
                $status = 6;
                break;
        }
        $where_date = '';
        $now = date('Y-m-d H:i:s');
        switch ($status) {
            case 1:
                $where_date .= ' AND	((sp.`from` = \'0000-00-00 00:00:00\' OR \'' . $now . '\' >= sp.`from`) AND (sp.`to` = \'0000-00-00 00:00:00\' OR \'' . $now . '\' <= sp.`to`)) ';
                break;
            case 0:
                $where_date .= ' AND	(sp.`from` <> \'0000-00-00 00:00:00\' AND \'' . $now . '\' < sp.`from`) ';
                break;
            case 2:
                $where_date .= ' AND	(sp.`from` <> \'0000-00-00 00:00:00\' AND \'' . $now . '\' > sp.`to`) ';
                break;
            case 3:
                $where_date .= ' AND	(((sp.`from` = \'0000-00-00 00:00:00\' OR \'' . $now . '\' >= sp.`from`) AND (sp.`to` = \'0000-00-00 00:00:00\' OR \'' . $now . '\' <= sp.`to`)) OR (\'' . $now . '\' < sp.`from`)) ';
                break;
            case 4:
                $where_date .= ' AND	(((sp.`from` = \'0000-00-00 00:00:00\' OR \'' . $now . '\' >= sp.`from`) AND (sp.`to` = \'0000-00-00 00:00:00\' OR \'' . $now . '\' <= sp.`to`)) OR (\'' . $now . '\' > sp.`to`)) ';
                break;
            case 5:
                $where_date .= ' AND	(\'' . $now . '\' < sp.`from` OR \'' . $now . '\' > sp.`to`) ';
                break;
            case 6:
                $where_date .= '';
                break;
        }
        $order = '';
        switch ($orderby) {
            case 'latest' :
                $order = "ORDER BY p.id_product DESC ";
                break;
            case 'price_lowest_first' :
                $order = "ORDER BY p.price ASC ";
                break;
            case 'price_highest_first' :
                $order = "ORDER BY p.price DESC ";
                break;
            case 'name_a_z' :
                $order = "ORDER BY pl.name DESC ";
                break;
            case 'name_z_a' :
                $order = "ORDER BY pl.name ASC ";
                break;
            case 'in_stock' :
                $order = "ORDER BY p.quantity DESC ";
                break;
            case 'finish_earliest_date' :
                $order = "ORDER BY sp.`to` ASC ";
                break;
            case 'finish_latest_date' :
                $order = "ORDER BY sp.`to` DESC ";
                break;
            default:
                $order = "ORDER BY p.id_product DESC ";
                break;
        }
        $results = self::getDeal($module, $where, $where_date, $page_number, $nb_products, $order, $count);
        return $results;
    }
    /**
     * Get all attributes groups for a given language
     * @param integer $id_lang Language id
     * @return array Attributes groups
     */
    public static function getDeal($module, $where = '', $where_date = '', $page_number = 0, $nb_products = 10, $order = '', $count = false)
    {
        $context = Context::getContext();
        if ($page_number < 0) $page_number = 0;
        if ($nb_products < 1) $nb_products = 10;
        $id_lang = (int)($context->language->id);
        $front = true;
        if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
            $front = false;
        $ids_product = self::_getProductIdByDate($where_date, $context);
        if (!$ids_product)
            return array();
        $tab_id_product = array();
        foreach ($ids_product as $product)
            if (is_array($product))
                $tab_id_product[] = (int)$product['id_product'];
            else
                $tab_id_product[] = (int)$product;
        $groups = FrontController::getCurrentCustomerGroups();
        $sql_groups = (count($groups) ? 'IN (' . implode(',', $groups) . ')' : '= 1');
        $id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
        $ids = Address::getCountryAndState($id_address);
        $id_country = (int)($ids['id_country'] ? $ids['id_country'] : Configuration::get('PS_COUNTRY_DEFAULT'));
        $sql = 'SELECT DISTINCT p.`id_product`, p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`, product_attribute_shop.id_product_attribute,
					pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`,
					pl.`name`, image_shop.`id_image`, il.`legend`, t.`rate`, m.`name` AS manufacturer_name,
					DATEDIFF(
						p.`date_add`,
						DATE_SUB(
							NOW(),
							INTERVAL ' . (Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20) . ' DAY
						)
					) > 0 AS new, sp.`id_specific_price`
				FROM `' . _DB_PREFIX_ . 'product` p
				' . Shop::addSqlAssociation('product', 'p') . '
				LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute pa ON (pa.id_product = p.id_product)
				' . Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.default_on=1') . '
				' . Product::sqlStock('p', 0, false, $context->shop) . '
				LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
					p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = ' . (int)$id_lang . Shop::addSqlRestrictionOnLang('pl') . '
				)
				LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_product` = p.`id_product`)' .
            Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1') . '
				LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)$id_lang . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group`
					AND tr.`id_country` = ' . (int)$context->country->id . '
					AND tr.`id_state` = 0)
				LEFT JOIN `' . _DB_PREFIX_ . 'tax` t ON (t.`id_tax` = tr.`id_tax`)
				LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
				LEFT JOIN `' . _DB_PREFIX_ . 'category_product` cp ON (cp.`id_product` = p.`id_product`)
				JOIN `' . _DB_PREFIX_ . 'specific_price` sp ON (sp.`id_product` = p.`id_product`
						AND sp.`id_shop` IN(0, ' . (int)($context->shop->id) . ')
						AND sp.`id_currency` IN(0, ' . (int)($context->currency->id) . ')
						AND sp.`id_country` IN(0, ' . (int)($id_country) . ')
						AND sp.`id_group` IN(0, ' . (int)($context->customer->id_default_group) . ')
						AND sp.`id_customer` IN(0, ' . (int)($context->customer->id) . ')' .
            $where_date . '
						AND sp.`reduction` > 0
					)
				
				WHERE product_shop.`active` = 1
				AND product_shop.`show_price` = 1
				AND ((image_shop.id_image IS NOT NULL OR i.id_image IS NULL) OR (image_shop.id_image IS NULL AND i.cover=1))
				' . ($front ? ' AND p.`visibility` IN ("both", "catalog")' : '') . '
				 AND p.`id_product` IN (' . ((is_array($tab_id_product) && count($tab_id_product)) ? implode(', ', $tab_id_product) : 0) . ')
				AND p.`id_product` IN (
					SELECT cp.`id_product`
					FROM `' . _DB_PREFIX_ . 'category_group` cg
					LEFT JOIN `' . _DB_PREFIX_ . 'category_product` cp ON (cp.`id_category` = cg.`id_category`)
					WHERE cg.`id_group` ' . $sql_groups . '
				)' . $where . '
				AND (pa.id_product_attribute IS NULL OR product_attribute_shop.default_on = 1)
				' . $order . (!$count ? ' LIMIT ' . (int)($page_number * $nb_products) . ', ' . (int)$nb_products : '');
        //echo "<pre>".print_r($sql,1); die;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if ($count)
            return count($result);
        //echo "<pre>".print_r($result,1); die;
        $return = self::getProductsProperties($module, $id_lang, $result, $context);
        return $return;
    }
    public static function getProductsProperties($module, $id_lang, $query_result, $context)
    {
        $results_array = array();
        $maxDesc = $module->getParams()->get('des_max_chars', 100);
        $img_size = $module->getParams()->get('img_size', 'tabs_allinmart');
        $description = $module->getParams()->get('description', 'intro_text');
        $link = new Link();
        if (is_array($query_result)) {
            $id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
            $ids = Address::getCountryAndState($id_address);
            $id_country = (int)($ids['id_country'] ? $ids['id_country'] : Configuration::get('PS_COUNTRY_DEFAULT'));
            $now = date('Y-m-d H:i:s');
            $finish = $module->l('Expired');
            foreach ($query_result as $row)
                if (($row2 = Product::getProductProperties($id_lang, $row))) {
                    $time = false;
                    $row2['specific_prices'] = self::getSpecificPriceById($row['id_specific_price']);
                    if (isset($row2['specific_prices']['from']) && $row2['specific_prices']['from'] > $now) {
                        $time = strtotime($row2['specific_prices']['from']);
                        $row2['finish'] = $finish;
                        $row2['check_status'] = 0;
                        $row2['lofdate'] = Tools::displayDate($row2['specific_prices']['from'], $context->cookie->id_lang);
                    } elseif (isset($row2['specific_prices']['to']) && $row2['specific_prices']['to'] > $now) {
                        $time = strtotime($row2['specific_prices']['to']);
                        $row2['finish'] = $finish;
                        $row2['check_status'] = 1;
                        $row2['lofdate'] = Tools::displayDate($row2['specific_prices']['to'], $context->cookie->id_lang);
                    } elseif ($row2['specific_prices']['to'] == '0000-00-00 00:00:00') {
                        $row2['js'] = 'unlimited';
                        $row2['finish'] = $module->l('Unlimited');
                        $row2['check_status'] = 1;
                        $row2['lofdate'] = $module->l("Unlimited");
                    } else {
                        $time = strtotime($row2['specific_prices']['to']);
                        $row2['finish'] = $finish;
                        $row2['check_status'] = 2;
                        $row2['lofdate'] = Tools::displayDate($row2['specific_prices']['from'], $context->cookie->id_lang);
                    }
                    if ($time) {
                        $row2['js'] = array(
                            'month' => date('m', $time),
                            'day' => date('d', $time),
                            'year' => date('Y', $time),
                            'hour' => date('H', $time),
                            'minute' => date('i', $time),
                            'seconds' => date('s', $time)
                        );
                    }
                    $row2['mainImge'] = $link->getImageLink($row2['link_rewrite'], $row2['id_image'], $img_size);
                    $row2['reductionprice'] = $row2['price_without_reduction'] - $row2['price'];
                    if ($description == 'intro_text')
                        $row2['description'] = substr(trim(strip_tags($row2['description_short'])), 0, $maxDesc);
                    else
                        $row2['description'] = substr(trim(strip_tags($row2['description'])), 0, $maxDesc);
                    if ($module->getParams()->get('count_bought', 'quantity') == 'customer')
                        $row2['SumCustomer'] = self::getSumCustomer($row2['id_product'], $context->currency->id, $id_country, $context->customer->id_default_group, $row2['specific_prices']['from'], $row2['specific_prices']['to']);
                    if ($module->getParams()->get('count_bought', 'quantity') == 'quantity')
                        $row2['SumQuantity'] = self::getSumProductsSaled($row2['id_product'], $row2['specific_prices']['from'], $row2['specific_prices']['to']);
                    $results_array[] = $row2;
                }
        }
        //echo "<pre>".print_r($results_array,1); die;
        return $results_array;
    }
    protected static function _getProductIdByDate($where = '', Context $context = null, $with_combination = false)
    {
        if (!$context)
            $context = Context::getContext();
        $id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
        $ids = Address::getCountryAndState($id_address);
        $id_country = (int)($ids['id_country'] ? $ids['id_country'] : Configuration::get('PS_COUNTRY_DEFAULT'));
        //$special = self::getSpecificPrice($where, 2, $context->shop->id, $context->currency->id, $id_country, $context->customer->id_default_group, 1, 0, 0, 0, 1);
        return self::getProductIdByDate(
            $where,
            $context->shop->id,
            $context->currency->id,
            $id_country,
            $context->customer->id_default_group,
            0,
            $with_combination
        );
    }
    public static function getProductIdByDate($where = '', $id_shop, $id_currency, $id_country, $id_group, $id_customer = 0, $with_combination_id = false)
    {
        if (!SpecificPrice::isFeatureActive())
            return array();
        $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT `id_product`, `id_product_attribute`
			FROM `' . _DB_PREFIX_ . 'specific_price` sp
			WHERE	`id_shop` IN(0, ' . (int)$id_shop . ') AND
					`id_currency` IN(0, ' . (int)$id_currency . ') AND
					`id_country` IN(0, ' . (int)$id_country . ') AND
					`id_group` IN(0, ' . (int)$id_group . ') AND
					`id_customer` IN(0, ' . (int)$id_customer . ')
					' . $where . '
					AND
					`reduction` > 0
		');
        $ids_product = array();
        foreach ($results as $row)
            $ids_product[] = $with_combination_id ? array('id_product' => (int)$row['id_product'], 'id_product_attribute' => (int)$row['id_product_attribute']) : (int)$row['id_product'];
        return $ids_product;
    }
    public static function getSpecificPrice($where, $id_product, $id_shop, $id_currency, $id_country, $id_group, $id_product_attribute = null, $id_customer = 0, $id_cart = 0)
    {
        if (!SpecificPrice::isFeatureActive())
            return array();
        $key = ((int)$id_product . '-' . (int)$id_shop . '-' . (int)$id_currency . '-' . (int)$id_country . '-' . (int)$id_group . '-' . (int)$id_product_attribute . '-' . (int)$id_cart);
        if (!array_key_exists($key, self::$_specificPriceCache)) {
            $now = date('Y-m-d H:i:s');
            self::$_specificPriceCache[$key] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
				SELECT *, ' . self::_getScoreQuery($id_product, $id_shop, $id_currency, $id_country, $id_group, $id_customer) . '
				FROM `' . _DB_PREFIX_ . 'specific_price` sp
				WHERE `id_product` IN (0, ' . (int)$id_product . ')
				AND `id_product_attribute` IN (0, ' . (int)$id_product_attribute . ')
				AND `id_shop` IN (0, ' . (int)$id_shop . ')
				AND `id_currency` IN (0, ' . (int)$id_currency . ')
				AND `id_country` IN (0, ' . (int)$id_country . ')
				AND `id_group` IN (0, ' . (int)$id_group . ')
				AND `id_customer` IN (0, ' . (int)$id_customer . ')
				' . $where . '
				AND id_cart IN (0, ' . (int)$id_cart . ')
				
				ORDER BY `id_product_attribute` DESC, `from_quantity` DESC, `id_specific_price_rule` ASC, `score` DESC');
        }
        return self::$_specificPriceCache[$key];
    }
    public static function getSpecificPriceById($id_specific_price)
    {
        if (!SpecificPrice::isFeatureActive())
            return array();
        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT *
			FROM `' . _DB_PREFIX_ . 'specific_price` sp
			WHERE `id_specific_price` =' . (int)($id_specific_price));
        return $res;
    }
    protected static function _getScoreQuery($id_product, $id_shop, $id_currency, $id_country, $id_group, $id_customer)
    {
        $select = '(';
        $now = date('Y-m-d H:i:s');
        $select .= ' IF (\'' . $now . '\' >= `from` AND \'' . $now . '\' <= `to`, ' . pow(2, 0) . ', 0) + ';
        $priority = SpecificPrice::getPriority($id_product);
        foreach (array_reverse($priority) as $k => $field)
            $select .= ' IF (`' . bqSQL($field) . '` = ' . (int)$$field . ', ' . pow(2, $k + 1) . ', 0) + ';
        return rtrim($select, ' +') . ') AS `score`';
    }
    /**
     * get sum customer
     */
    public static function getSumCustomer($id_product, $id_currency, $id_country, $id_group, $from, $to)
    {
        if ($from != "0000-00-00 00:00:00" || $to != "0000-00-00 00:00:00") {
            $where = ' AND o.`date_add` >= \'' . $from . '\' AND o.`date_add` <= \'' . $to . '\'';
        } else {
            $where = '';
        }
        $sql = 'SELECT DISTINCT o.`id_customer` FROM `' . _DB_PREFIX_ . 'orders` o
		LEFT JOIN `' . _DB_PREFIX_ . 'order_detail` od ON (od.`id_order` = o.`id_order`)
		LEFT JOIN `' . _DB_PREFIX_ . 'customer_group` cg ON (cg.`id_customer` = o.`id_customer`)
		LEFT JOIN `' . _DB_PREFIX_ . 'address` ad ON (ad.`id_customer` = o.`id_customer`)
		WHERE od.`product_id` = ' . $id_product . $where;
        $sql .= ($id_currency == 0) ? '' : ' AND o.`id_currency` = ' . $id_currency;
        $sql .= ($id_group == 0) ? '' : ' AND cg.`id_group` = ' . $id_group;
        $sql .= ($id_country == 0) ? '' : ' AND ad.`id_country` = ' . $id_country;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);
        return count($result);
    }
    /**
     * get sum customer
     */
    public static function getSumProductsSaled($id_product, $from, $to)
    {
        if ($from != "0000-00-00 00:00:00" || $to != "0000-00-00 00:00:00") {
            $where = ' AND o.`date_add` >= \'' . $from . '\' AND o.`date_add` <= \'' . $to . '\'';
        } else {
            $where = '';
        }
        $sql = 'SELECT DISTINCT od.`product_quantity` FROM `' . _DB_PREFIX_ . 'orders` o
		LEFT JOIN `' . _DB_PREFIX_ . 'order_detail` od ON (od.`id_order` = o.`id_order`)
		WHERE od.`product_id` = ' . $id_product . $where;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);
        $total = 0;
        if ($result) {
            foreach ($result as $row) {
                $total += $row['product_quantity'];
            }
        }
        return $total;
    }
    /**
     * Get List Categories Tree source
     * @access public
     * @static method
     * return array contain list of categories source
     */
    public static function getCategories()
    {
        global $cookie;
        $id_lang = intval($cookie->id_lang);
        $allCat = Db::getInstance()->ExecuteS('
		SELECT c.*, cl.id_lang, cl.name, cl.description, cl.link_rewrite, cl.meta_title, cl.meta_keywords, cl.meta_description
		FROM `' . _DB_PREFIX_ . 'category` c
		LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (c.`id_category` = cl.`id_category` AND `id_lang` = ' . intval($id_lang) . ')
		LEFT JOIN `' . _DB_PREFIX_ . 'category_group` cg ON (cg.`id_category` = c.`id_category`)
		WHERE `active` = 1
		GROUP BY c.`id_category`
		ORDER BY `name` ASC');
        $children = array();
        if ($allCat) {
            foreach ($allCat as $v) {
                $pt = $v["id_parent"];
                $list = @$children[$pt] ? $children[$pt] : array();
                array_push($list, $v);
                $children[$pt] = $list;
            }
            $list = array();
            self::treeCategory(1, $list, $children);
            return $list;
        }
        return array();
    }
    /**
     * Build category tree list
     */
    public static function treeCategory($id, &$list, $children, $tree = "")
    {
        if (isset($children[$id])) {
            if ($id != 0) {
                $tree = $tree . " - ";
            }
            foreach ($children[$id] as $v) {
                $v["tree"] = $tree;
                $list[] = $v;
                self::treeCategory($v["id_category"], $list, $children, $tree);
            }
        }
    }
}