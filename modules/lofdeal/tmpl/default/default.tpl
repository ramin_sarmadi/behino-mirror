<div class="span4 deal-span">
    <div class="row-fluid deal-row">
        <span class="sug-title-text">{l s='فروش ویژه به مناسبت روز زن'}</span>

        <div class="sug-car-contr">
            <a id="deal-left"></a>
            <a id="deal-right"></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12 carousel-deal">
            {foreach from=$lofdeal_products key=key item=item name=foo}
                <div class="deal-item">
                    <div class="row-fluid">
                        <div class="deal-info">
                            <h4 class="time"><a title="{$item.description}"
                                                href="{$item.link}">{$item.name}</a></h4>
                            {if $lofdeal_show_des}
                                <div class="des">{$item.description}</div>
                            {/if}
                        </div>
                        <div class="deal-img">
                            <img src="{$link->getImageLink($item.link_rewrite, $item.id_image, $lofdeal_img_size)}"
                                 alt="{$item.name}"/>
                        </div>
                    </div>
                    <div class="row-fluid deal-price">
                        {*<div class="price_action">*}
                        {*<div class="show_price">*}
                        <div class="deal-new-price">
                                 <span class="lof-sale">
                                {convertPrice price=$item.price}
                                </span>
                        </div>
                        <div class="deal-old-price">
                                    <span class="lof-price">
                                {convertPrice price=$item.price_without_reduction}
                            </span>
                        </div>
                    </div>
                    <div class="row-fluid" style="margin-top: 40px !important;">
                        <div class="span12" style="text-align: center">
                            <span class="deal-time-text">{l s='زمان باقی مانده:'}</span>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="deal-clock lof-clock-{$item.id_product}-{$smarty.foreach.foo.index}">
                                {if $item.js == 'unlimited'}
                                    <div class="lof-labelexpired">
                                        {l s='Unlimited' mod='lofdeal'}
                                    </div>
                                {/if}
                            </div>
                        </div>
                    </div>
                    {*<li class="{if $smarty.foreach.foo.index % 2}alt {/if}{if ($smarty.foreach.foo.index == 0)}first{/if}">*}
                    <div class="lof-li-wrapper">
                        {if $item.js != 'unlimited'}
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var date = '{$item.js.month}' + '/' + '{$item.js.day}' + '/' + '{$item.js.year}' + ' ' + '{$item.js.hour}' + ':' + '{$item.js.minute}' + ':' + '{$item.js.seconds}';
                                    var NY1 = Math.round((new Date(date)).getTime() / 1000);
                                    $('.lof-clock-{$item.id_product}-{$smarty.foreach.foo.index}').flipcountdown({
                                        {literal}
                                        size: "sm",
                                        tick: function () {
                                            var nol = function (h) {
                                                return h > 9 ? h : '0' + h;
                                            }
                                            var range = NY1 - Math.round((new Date()).getTime() / 1000),
                                                    secday = 86400, sechour = 3600,
                                                    days = parseInt(range / secday),
                                                    hours = parseInt((range % secday) / sechour),
                                                    min = parseInt(((range % secday) % sechour) / 60),
                                                    sec = ((range % secday) % sechour) % 60;
                                            return nol(days) + ' ' + nol(hours) + ' ' + nol(min) + ' ' + nol(sec);
                                        }
                                    });
                                    $('.carousel-deal').carouFredSel({
                                        items: 1,
                                        direction: "right",
                                        width: 381,
                                        scroll: {
                                            items: 1,
                                            duration: 1000,
                                            pauseOnHover: true
                                        },
                                        auto: false,
                                        responsive: true,
                                        prev: $('#deal-left'),
                                        next: $('#deal-right')
                                    });
                                });
                                {/literal}
                            </script>
                        {/if}
                    </div>
                    {* </li>*}
                </div>
            {/foreach}
        </div>


        {*<div class="lof-deal lof-deal-{$id_lang}">*}
        {* <div class="lof-head">*}
        {*<h2>{l s='Today\'s deals' mod='lofdeal'}</h2>*}
        {*</div>*}
        {*<div class="lof-content">*}
        {*<ul class="lofdeal-content">*}

        {*</ul>*}
        {*</div>*}
        {*<div class="lof-paging">*}
        {*<ul>*}
        {*<li><a href="{$link->getModuleLink('lofdeal', 'deal', ['process' => 'view'])}"*}
        {*title="view more">{l s='View more' mod='lofdeal'} ...</a></li>*}
        {*</ul>*}
        {*</div>*}
        {*</div>*}
    </div>
</div>

