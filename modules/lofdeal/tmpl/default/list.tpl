<div class="lof-deal lof-deal-{$id_lang}">
    <div id="menu" class="lof-head-link">
        <ul>
            <li id="head-1" {if $lofstatus==1}class="lofactive"{/if}>
                <a href="{$link->getModuleLink('lofdeal', 'deal', ['process' => 'view'])}&task=recently{if $cookiecate}&cate={$cookiecate}{/if}{if $orderby}&sortby={$orderby}{/if}&n={$limit}"
                   title="{l s='Current Deals'}">{l s='Current Deals' mod='lofdeal'}</a>
            </li>
            <li id="head-2" {if $lofstatus==0}class="lofactive"{/if}>
                <a href="{$link->getModuleLink('lofdeal', 'deal', ['process' => 'view'])}&task=future{if $cookiecate}&cate={$cookiecate}{/if}{if $orderby}&sortby={$orderby}{/if}&n={$limit}"
                   title="{l s='Future Deals'}">{l s='Future Deals' mod='lofdeal'}</a>
            </li>
            <li id="head-3" {if $lofstatus==2}class="lofactive"{/if}>
                <a href="{$link->getModuleLink('lofdeal', 'deal', ['process' => 'view'])}&task=finished{if $cookiecate}&cate={$cookiecate}{/if}{if $orderby}&sortby={$orderby}{/if}&n={$limit}"
                   title="{l s='Finished Deals'}">{l s='Finished Deals' mod='lofdeal'}</a>
            </li>
        </ul>
    </div>
    {if $lofdeal_show_filter == 1}
        <div class="lof-head-filter">
            <form action="{$link->getModuleLink('lofdeal', 'deal', ['process' => 'view'])}" method="get">
                <input type="hidden" name="task"
                       value="{if $lofstatus == 1}recently{elseif $lofstatus == 0}future{else}finished{/if}"/>
                <input type="hidden" name="process" value="filter"/>
                <input type="hidden" name="module" value="lofdeal"/>
                <input type="hidden" name="fc" value="module"/>
                <input type="hidden" name="controller" value="deal"/>

                <div style="clear:both;"></div>
                <div class="lof-filter">
                    <div class="col"><label for="lof-select-cate">{l s='Categories' mod='lofdeal'}:</label><br/>
                        <select name="cate" id="lof-select-cate">
                            <option value="all"
                                    {if $cookiecate == 'all'}selected="selected"{/if}>{l s='All' mod='lofdeal'}</option>
                            {foreach from=$categories item=cate}
                                <option value="{$cate.id_category}"
                                        {if ($cate.id_category == $cookiecate)}selected="selected"{/if} >
                                    -{$cate.tree}{$cate.name}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col"><label for="lof-select-sortby">{l s='Sort by' mod='lofdeal'}:</label><br/>
                        <select name="sortby" id="lof-select-sortby">
                            {foreach from=$arrSortBy key=key item=sortby}
                                <option value="{$key}" {if $key == $orderby}selected="selected"{/if}>{$sortby}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col"><label for="lof-select-limit">{l s='Limit' mod='lofdeal'}:</label><br/>
                        <select name="limit" id="lof-select-limit">
                            {foreach from=$limitArr key=key item=lim}
                                <option value="{$lim}" {if $lim == $limit}selected="selected"{/if}>{$lim}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col"><input type="submit" class="lof-submit" name="submit" value="Filter"/></div>
                </div>
            </form>
        </div>
    {/if}
    <div class="lof-content">
        <ul class="lofdeal-content">
            {foreach from=$lofdeal_products key=key item=item name=foo}
                <li class="{if $smarty.foreach.foo.index % 2}alt {/if}{if ($smarty.foreach.foo.index == 0)}first{/if}">
                    <div class="lof-li-wrapper">
                        <div class="lofdeal_left">
                            <div class="pic">
                                {if $item.check_status == 1}
                                    <div class="isopen"></div>
                                {elseif $item.check_status == 0}
                                    <div class="isfuture"></div>
                                {else}
                                    <div class="isclosed"></div>
                                {/if}
                                <a title="{$item.name}" href="{$item.link}" class="product_image"><img
                                            src="{$link->getImageLink($item.link_rewrite, $item.id_image, $lofdeal_img_size)}"
                                            alt="{$item.name}"/></a>
                            </div>
                            <h4 class="time"><a title="{$item.description}" href="{$item.link}">{$item.name}</a></h4>
                            {if $lofdeal_show_date}
                                <p class="lof-date">{$item.lofdate}</p>
                            {/if}
                            {if $item.check_status != 0 && (isset($item.SumCustomer) || isset($item.SumQuantity))}
                                <div class="info">
                                    {if isset($item.SumCustomer)}
                                        <p class="total">
                                            {l s='Customers Bought' mod='lofdeal'}: <span
                                                    class="count">{$item.SumCustomer}</span><!--persons have bought-->
                                        </p>
                                    {/if}
                                    {if isset($item.SumQuantity)}
                                        <p class="total">
                                            {l s='Saled quantity' mod='lofdeal'}: <span
                                                    class="count">{$item.SumQuantity}</span><!--persons have bought-->
                                        </p>
                                    {/if}
                                </div>
                            {/if}
                        </div>
                        <div class="lofdeal_right">
                            {if $lofdeal_show_des}
                                <div class="des">{$item.description}</div>
                            {/if}
                            {if $lofdeal_show_addcart}
                                {if ($item.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $item.available_for_order AND !isset($restricted_country_mode) AND $item.minimal_quantity == 1 AND $item.customizable != 2 AND !$PS_CATALOG_MODE AND ($item.quantity > 0 OR $item.allow_oosp)}
                                    <a class="button lof-addcart ajax_add_to_cart_button exclusive"
                                       rel="ajax_id_product_{$item.id_product}"
                                       href="{$link->getPageLink('cart')}?qty=1&amp;id_product={$item.id_product}&amp;token={$static_token}&amp;add"
                                       title="{l s='Add to cart' mod='lofdeal'}">{l s='Add to cart' mod='lofdeal'}</a>
                                {else}
                                    <span class="exclusive lof-addcart">{l s='Add to cart' mod='lofdeal'}</span>
                                {/if}
                            {/if}
                            <div class="price_action">
                                <div class="show_price">
                                    <div class="lof-sale">{l s='Sale' mod='lofdeal'}
                                        :<strong> {convertPrice price=$item.price}</strong></div>
                                    <div class="lof-price">{l s='Price' mod='lofdeal'}: <strike
                                                class="old">{convertPrice price=$item.price_without_reduction}</strike>
                                    </div>
                                    <div class="lof-discount">{l s='Reduction' mod='lofdeal'}: </br>
                                        <strong class="discount">
                                            {if $item.specific_prices.reduction_type == 'percentage'}
                                                {$item.specific_prices.reduction*100}%
                                            {else}
                                                {math equation="pricere/pricewt*100" pricewt=$item.price_without_reduction pricere=$item.specific_prices.reduction assign=reduction_per}
                                                {$reduction_per|round:2}%
                                            {/if}
                                        </strong>
                                    </div>
                                    <div class="lof-save">{l s='Save' mod='lofdeal'}:</br>
                                        <strong>
                                            {if $item.specific_prices.reduction_type == 'percentage'}
                                                {math equation="pricewt*pricere" pricewt=$item.price_without_reduction pricere=$item.specific_prices.reduction assign=reduction_price}
                                                {convertPrice price=$reduction_price}
                                            {else}
                                                {convertPrice price=$item.specific_prices.reduction}
                                            {/if}
                                        </strong>
                                    </div>
                                    <div class="deal-clock lof-clock-{$item.id_product}-{$smarty.foreach.foo.index}">
                                        {if $item.js == 'unlimited'}
                                            <div class="lof-labelexpired">
                                                {l s='Unlimited' mod='lofdeal'}
                                            </div>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {if $item.js != 'unlimited'}
                            <script type="text/javascript">
                                {literal}
                                jQuery(document).ready(function ($) {{/literal}
                                    $(".lof-clock-{$item.id_product}-{$smarty.foreach.foo.index}").lofCountDown({literal}{{/literal}
                                        TargetDate: "{$item.js.month}/{$item.js.day}/{$item.js.year} {$item.js.hour}:{$item.js.minute}:{$item.js.seconds}",
                                        DisplayFormat: "<div>%%D%% {l s='Days' mod='lofdeal'}</div><div>%%H%% {l s='Hours' mod='lofdeal'}</div><div>%%M%% {l s='Min' mod='lofdeal'}</div><div>%%S%% {l s='Sec' mod='lofdeal'}</div>",
                                        FinishMessage: "{$item.finish}"
                                        {literal}
                                    });
                                });
                                {/literal}
                            </script>
                        {/if}
                    </div>
                </li>
            {/foreach}
        </ul>
    </div>
    <div id="pagination" class="pagination">
        {if $nbpagination < $total}
            <ul class="pagination">
                {if $page != 1}
                    {assign var='p_previous' value=$page-1}
                    <li id="pagination_previous"><a href="{viewpaginationlink p=$p_previous n=$nbpagination}">
                            &laquo;&nbsp;{l s='Previous' mod='lofdeal'}</a></li>
                {else}
                    <li id="pagination_previous" class="disabled">
                        <span>&laquo;&nbsp;{l s='Previous' mod='lofdeal'}</span></li>
                {/if}
                {if $page > 2}
                    <li><a href="{viewpaginationlink p='1' n=$nbpagination}">1</a></li>
                    {if $page > 3}
                        <li class="truncate">...</li>
                    {/if}
                {/if}
                {section name=pagination start=$page-1 loop=$page+2 step=1}
                    {if $page == $smarty.section.pagination.index}
                        <li class="current"><span>{$page|escape:'htmlall':'UTF-8'}</span></li>
                    {elseif $smarty.section.pagination.index > 0 && $total+$nbpagination > ($smarty.section.pagination.index)*($nbpagination)}
                        <li>
                            <a href="{viewpaginationlink p=$smarty.section.pagination.index n=$nbpagination}">{$smarty.section.pagination.index|escape:'htmlall':'UTF-8'}</a>
                        </li>
                    {/if}
                {/section}
                {if $max_page-$page > 1}
                    {if $max_page-$page > 2}
                        <li class="truncate">...</li>
                    {/if}
                    <li><a href="{viewpaginationlink p=$max_page n=$nbpagination}">{$max_page}</a></li>
                {/if}
                {if $total > $page * $nbpagination}
                    {assign var='p_next' value=$page+1}
                    <li id="pagination_next"><a
                                href="{viewpaginationlink p=$p_next n=$nbpagination}">{l s='Next' mod='lofdeal'}
                            &nbsp;&raquo;</a></li>
                {else}
                    <li id="pagination_next" class="disabled"><span>{l s='Next' mod='lofdeal'}&nbsp;&raquo;</span></li>
                {/if}
            </ul>
        {/if}
    </div>
</div>

