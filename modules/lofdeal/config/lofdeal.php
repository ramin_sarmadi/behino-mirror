<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
?>
<link rel="stylesheet" href="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.css";?>" type="text/css" media="screen" charset="utf-8" />
<script type="text/javascript" src="<?php echo __PS_BASE_URI__."modules/".$this->name."/assets/admin/form.js";?>"></script>
<?php
global $cookie;
$iso = Language::getIsoById((int)($cookie->id_lang));
?>
<h3><?php echo $this->l('Lof Deal');?></h3>
<?php 
//register Yes - No Lang
$yesNoLang = array("1"=>$this->l('Yes'),"0"=>$this->l('No'));
$showDes = array("intro_text"=>$this->l('Short description'),"description"=>$this->l('Description'));
$showHideLang = array("1"=>$this->l('Show'),"0"=>$this->l('Hide'));
?>
<form action="<?php echo $_SERVER['REQUEST_URI'].'&rand='.rand();?>" method="post" id="lofform">
 <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />               
  <fieldset>
    <legend><img src="../img/admin/contact.gif" /><?php echo $this->l('General Setting'); ?></legend>
        <div class="lof_config_wrrapper clearfix"><ul>
            <?php                                
                echo $this->_params->selectTag("module_theme",$themes,$this->getParamValue("module_theme",'default'),$this->l('Theme - Layout'),'class="inputbox"', 'class="row" title="'.$this->l('Select a theme').'"');				
				//echo $this->_params->inputTag("title",$this->getParamValue("title","Today's deals"),$this->l('Module Title'),'','class="row"','','This title is display in module');
				//echo $this->_params->inputTag("finish",$this->getParamValue("finish","Expired"),$this->l('Expiration text'),'','class="row"','','This text is display when time deal finished');
				$mainImgSize = array();
				$formats = ImageType::getImagesTypes( 'products' );
				foreach ($formats as $k=> $format)
					$mainImgSize[$format['name']] = $format['name'].'('.$format['width']."x".$format['height'].')';
				
				echo $this->_params->selectTag("img_size",$mainImgSize,$this->getParamValue("img_size","home_allinmart"),$this->l('Image Size'),'class="inputbox"','class="row"','',$this->l("You can select size image"));
				echo $this->_params->selectTag("description",$showDes,$this->getParamValue("description",'intro_text'),$this->l('Description'),'class="inputbox"', 'class="row" title="'.$this->l('Select Desciption').'"');				
                echo $this->_params->inputTag("des_max_chars",$this->getParamValue("des_max_chars","100"),$this->l('Description Max Chars'),'class="text_area"','class="row"','');
				echo $this->_params->inputTag("limitonmodule",$this->getParamValue("limitonmodule",4),$this->l('Limit Items On Module'),'class="text_area"','class="row"','', 'Limit Item when view module'); 
				echo $this->_params->inputTag("limitonpage",$this->getParamValue("limitonpage","8,12,16,20"),$this->l('Limit items when filter'),'class="text_area"','class="row"','','show list on filter'); 
				$homeSorceArr = array("selectcat"=>$this->l('Select category'),"productids"=>$this->l('Product IDs')); 
                echo $this->_params->selectTag("home_sorce",$homeSorceArr,$this->getParamValue("home_sorce","selectcat"),$this->l('Get Product From'),'class="inputbox select-group"','class="row"','class="row"');               
				echo $this->_params->getCategory("category[]",$this->getParamValue("category",""),$this->l('Select Category'),'size="10" multiple="multiple" style="width: 90%;" class="inputbox"','class="row home_sorce-selectcat"','','',$this->l('All Categories'));
				echo $this->_params->inputTag("productids",$this->getParamValue("productids","1,2,3,4,5"),$this->l('Product IDs'),'class="text_area"','class="row"','class="home_sorce-productids"');
				$arrSortBy = array("latest"=>$this->l('Lastest'),
									"price_lowest_first"=>$this->l('Price: lowest first'),
									"price_highest_first"=>$this->l('Price: highest first'),
									"name_a_z"=>$this->l('Product Name: A to Z'),
									"name_z_a"=>$this->l('Product Name: Z to A'),
									"in_stock"=>$this->l('In-stock first'),
									'finish_earliest_date'	=>	$this->l('Finish date: earliest first'),
									'finish_latest_date'	=>	$this->l('Finish date: latest first'),
									);
				echo $this->_params->selectTag("orderby",$arrSortBy,$this->getParamValue("orderby","latest"),$this->l('Order By'),'class="inputbox"','class="row"','');
        echo $this->_params->radioBooleanTag("show_date",$yesNoLang,$this->getParamValue("show_date",1),$this->l("Display Date"),'','class="row"',"",$this->l('Show expire date or show start date (future deal) in title.'));
				echo $this->_params->radioBooleanTag("show_des",$yesNoLang,$this->getParamValue("show_des",1),$this->l("Display Description"),'','class="row"',"");
				echo $this->_params->radioBooleanTag("show_addcart",$yesNoLang,$this->getParamValue("show_addcart",1),$this->l("Display Add To Cart"),'','class="row"',"");
        echo $this->_params->radioBooleanTag("show_filter",$yesNoLang,$this->getParamValue("show_filter",1),$this->l("Display Filter"),'','class="row"',"");
        $count_bought = array(
            "none"=>$this->l('None'),
            "customer"=>$this->l('Count Customers'),
            "quantity"=>$this->l('Count quantity products')
        );
        echo $this->_params->selectTag("count_bought",$count_bought,$this->getParamValue("count_bought","quantity"),$this->l('Bought quantity'),'class="inputbox"','class="row"','',$this->l('Display bought quanty by amount product or customers bought.'));
				$valDisP  = array (
                                    "recently"=>array("langName"=>$this->l("Current Deal")),"future"=>array("langName"=>$this->l("Future Deal")),
                                    "finished"=>array("langName"=>$this->l("Finished Deal"))                                    
                                    );							
                echo $this->_params->displayList($valDisP,$this->getParamValue("showdeal","recently"),'',$showHideLang,$this->l('Deal Type'),'','class="row"','class="row"');
				/*
				$arrAmountDis = array("all"=>$this->l('All discount in a product'),
									"price_lowest"=>$this->l('Price: lowest'),
									"price_highest"=>$this->l('Price: highest')
									);
				echo $this->_params->selectTag("amount_discount",$arrAmountDis,$this->getParamValue("amount_discount","price_lowest"),$this->l('Amount discount in a product'),'class="inputbox"','class="row"','');
				*/
            ?>                                                 
        </ul></div>    
  </fieldset>
<br />
  <input type="submit" name="submit" value="<?php echo $this->l('Update');?>" class="button" />
  	<fieldset><legend><img src="../img/admin/comment.gif" alt="" title="" /><?php echo $this->l('Information');?></legend>    	
    	<ul>
    	     <li>+ <a target="_blank" href="http://landofcoder.com/others/item/84-prestashop-lof-deal.html"><?php echo $this->l('Detail Information');?></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/supports/forum.html?id=64"><?php echo $this->l('Forum support');?></a></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/submit-request.html"><?php echo $this->l('Customization/Technical Support Via Email');?>.</a></li>
             <li>+ <a target="_blank" href="http://landofcoder.com/prestashop-guide-and-docs/prestashop-14x-lof-deal.html"><?php echo $this->l('UserGuide ');?></a></li>
             <li>+ <a target="_blank" href="http://www.youtube.com/watch?v=IfXl4q-AA4M"><?php echo $this->l('Youtube link');?></a></li>
        </ul>
        <br />
        @copyright: <a href="http://landofcoder.com">LandOfCoder.com</a>
    </fieldset>
</form>