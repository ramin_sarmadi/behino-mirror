<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 14206 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
class LofdealDealModuleFrontController extends ModuleFrontController
{
	public function __construct()
	{
		//$this->auth = true;
		parent::__construct();

		$this->context = Context::getContext();
		include_once($this->module->getLocalPath().'classes/ClassLofDeal.php');
		
		// Declare smarty function to render pagination link
		smartyRegisterFunction($this->context->smarty, 'function', 'viewpaginationlink', array('LofdealDealModuleFrontController', 'getViewPaginationLink'));
	}

	/**
	 * @see FrontController::postProcess()
	 */
	public function postProcess() {
		if (Tools::getValue('process') == 'filter'){
			$cate = Tools::getValue('cate');
			$this->context->cookie->cate = Tools::getValue('cate');
			$this->context->cookie->sortby = Tools::getValue('sortby');
			$this->context->cookie->limit = Tools::getValue('limit');
			$this->assignViewExecution();
		}
	}
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent() {
		//$this->display_column_left =  ($this->module->getParams()->get('show_leftcolumn') ? true : false);
		//$this->display_column_right = ($this->module->getParams()->get('show_rightcolumn') ? true : false);
		
		parent::initContent();
		$theme = $this->module->getParams()->get('theme');
		$this->context->controller->addCSS(_MODULE_DIR_.$this->module->name."/themes/".$theme."/assets/style.css", 'all');
        $this->context->controller->addCSS(_MODULE_DIR_.$this->module->name."/assets/jquery.flipcountdown.js", 'all');
        $this->context->controller->addCSS(_MODULE_DIR_.$this->module->name."/assets/jquery.flipcountdown.css", 'all');
		// $this->context->controller->addJS( _MODULE_DIR_.$this->module->name."/js/countdown.js" );
		
		if (Tools::getValue('process') == 'view')
			$this->assignViewExecution();
	}
	/**
	 * Render pagination link for summary
	 *
	 * @param (array) $params Array with to parameters p (for page number) and n (for nb of items per page)
	 * @return string link
	 */
	public static function getViewPaginationLink($params, &$smarty) {
		if (!isset($params['p']))
			$p = 1;
		else
			$p = $params['p'];
		if (!isset($params['n']))
			$n = 10;
		else
			$n = $params['n'];
		return Context::getContext()->link->getModuleLink(
			'lofdeal',
			'deal',
			array(
				'process' => 'view',
				'p' => $p,
				'n' => $n,
			)
		);
	}
	/**
	 * Assign summary template
	 */
	public function assignViewExecution() {
		$params = $this->module->getParams();
		if(!$this->context->cookie->cate)
			$selectCat = $params->get("category","");
		else
			$selectCat = $this->context->cookie->cate;
		// get order
		if(!isset($this->context->cookie->orderby))
			$orderby = $params->get("orderby","");
		else
			$orderby = $this->context->cookie->orderby;
		
		$task = $params->get("active_tab","recently");
		if(isset($_GET['task']))
			$task = Tools::getValue('task');
		
		if($task == 'recently')
			$status = 1;
		elseif($task == 'future')
			$status = 0;	
		else
			$status = 2;
		
		$limit = $params->get("limitonpage","10,20,50");
		$limitArr  = explode(",",$limit);
		if(isset($_GET['n']))
			$this->context->cookie->limit = (int)$_GET['n'];
		$n = ($this->context->cookie->limit ? (int)$this->context->cookie->limit : (int)$limitArr[0]);
		
		$page = 0;
		if(isset($_GET["p"]) && (int)($p = $_GET["p"])){
			$page = ((int)($p) <= 1 ? 0 : ((int)($p)-1));
		}
		$where = '';
		if($selectCat && $selectCat !="all")
			$where = " AND cp.`id_category` IN (".$selectCat.")";
		$total = ClassLofDeal::getList( $this->module, $where, 0, $n, $orderby, $task, true);
		$total = is_array($total) ? count($total) : $total;
		$products = ClassLofDeal::getList( $this->module, $where, $page, $n, $orderby, $task );
		
		$this->context->smarty->assign(array(
			'total' => $total,
			'page' => ((int)Tools::getValue('p') > 0 ? (int)Tools::getValue('p') : 1),
			'nbpagination' => $n,
			'nArray' => $limitArr,
			'max_page' => floor($total / $n)
		));
		
		$arrSortBy = array(
			'latest'				=>	$this->module->l('Lastest Product'),
			'price_lowest_first'	=>	$this->module->l('Price: lowest first'),
			'price_highest_first'	=>	$this->module->l('Price: highest first'),
			'name_a_z'				=>	$this->module->l('Product Name: A to Z'),
			'name_z_a'				=>	$this->module->l('Product Name: Z to A'),
			'finish_earliest_date'	=>	$this->module->l('Finish date: earliest first'),
			'finish_latest_date'	=>	$this->module->l('Finish date: latest first'),
		);
		$categories = ClassLofDeal::getCategories();
		//$configs = $this->module->getConfig();
		$show_filter = $params->get('show_filter',1);
		$img_size 		= $params->get("img_size","tabs_allinmart");
		$show_addcart 		= $params->get("show_addcart",1);
		$show_des 		= $params->get("show_des",1);
		$show_date 		= $params->get("show_date",1);
		$this->context->smarty->assign(array(
			'lofdeal_products' => $products,
			'lofdeal_show_filter' => $show_filter,
			'lofdeal_img_size' => $img_size,
		    'lofdeal_show_addcart' => $show_addcart,
		    'lofdeal_show_des' => $show_des,
			'lofdeal_show_date' => $show_date,

			'limitArr' => $limitArr,
			'categories' => $categories,
			'arrSortBy' => $arrSortBy,
			'lofstatus' => $status,
			'cookiecate' => $this->context->cookie->cate,
			'orderby' => $this->context->cookie->orderby,
			'limit' => $this->context->cookie->limit,
			'id_lang' => $this->context->cookie->id_lang,
		));
		$this->setTemplate('list.tpl');
	}
	
	public function getTemplatePath(){
		return _PS_MODULE_DIR_.$this->module->name.'/tmpl/'.$this->module->getParams()->get('theme','default').'/';
	}
}
