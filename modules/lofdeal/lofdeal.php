<?php
/**
 * $ModDesc
 * @version        $Id: file.php $Revision
 * @package        modules
 * @subpackage    $Subpackage.
 * @copyright    Copyright (C) December 2010 LandOfCoder.com <@emai:landofcoder@gmail.com>.All rights reserved.
 * @license        GNU General Public License version 2
 */
if (!defined('_CAN_LOAD_FILES_')) {
    define('_CAN_LOAD_FILES_', 1);
}
/**
 * lofdeal Class
 */
include_once(dirname(__FILE__) . '/classes/ClassLofDeal.php');
class Lofdeal extends Module
{
    /**
     * @var LofParams $_params ;
     * @access private;
     */
    private $_params = '';
    /**
     * @var array $_postErrors ;
     * @access private;
     */
    private $_postErrors = array();
    /**
     * @var string $__tmpl is stored path of the layout-theme;
     * @access private
     */
    /**
     * Constructor
     */
    function __construct()
    {
        $this->name = 'lofdeal';
        parent::__construct();
        $this->tab = 'LandOfCoder';
        $this->version = '1.8';
        $this->displayName = $this->l('Lof Deal Module');
        $this->description = $this->l('Lof Deal Version 1.8');
        if (file_exists(_PS_ROOT_DIR_ . '/modules/' . $this->name . '/libs/params.php') && !class_exists("LofParams", false)) {
            if (!defined("LOF_LOAD_LIB_PARAMS")) {
                require(_PS_ROOT_DIR_ . '/modules/' . $this->name . '/libs/params.php');
                define("LOF_LOAD_LIB_PARAMS", true);
            }
        }
        $this->_params = new LofParams($this->name);
    }
    /**
     * process installing
     */
    function install()
    {
        if (!parent::install())
            return false;
        if (!$this->registerHook('productfooter'))
            return false;
        if (!$this->registerHook('header'))
            return false;
        if (!$this->registerHook('home'))
            return false;
        return true;
    }
    public function uninstall()
    {
        return parent::uninstall();
    }
    function hookHeader($params)
    {
        $params = $this->_params;
        $theme = $this->_params->get('module_theme', 'default');
        $this->context->controller->addCSS(_MODULE_DIR_ . $this->name . '/tmpl/' . $this->_params->get("theme", "default") . '/assets/style.css', 'all');
        $this->context->controller->addJS(_MODULE_DIR_ . $this->name . '/assets/countdown.js');
    }
    /*
     * register hook right comlumn to display slide in right column
     */
    function hookrightColumn($params)
    {
        return $this->processHook($params, "rightColumn");
    }
    /*
     * register hook left comlumn to display slide in left column
     */
    function hookleftColumn($params)
    {
        return $this->processHook($params, "leftColumn");
    }
    function hooktop($params)
    {
        return $this->processHook($params, "top");
    }
    function hookfooter($params)
    {
        return $this->processHook($params, "footer");
    }
    function hookcontenttop($params)
    {
        return $this->processHook($params, "contenttop");
    }
    function hooklofTop($params)
    {
        return $this->processHook($params, "lofTop");
    }
    function hookHome($params)
    {
        return $this->processHook($params, "home");
    }
    /*
     * register hook productfooter comlumn to display slide in product footer
     */
    function hookDisplayRightColumnProduct($params)
    {
        return $this->hookproductfooter($params);
    }
    function hookproductfooter($params)
    {
        $params = $this->getParams();
        $where = " AND p.`id_product` = " . (int)Tools::getValue('id_product');
        $showdeal = "recently,future,finished";
        $orderby = $params->get("orderby", "");
        $products = ClassLofDeal::getList($this, $where, 0, 1, $orderby, $showdeal);
        if (!$products)
            return;
        //echo "<pre>".print_r($products,1); die;
        $this->smarty->assign(array(
            'lofdeal_product' => $products[0],
            'id_lang' => $this->context->language->id,
        ));
        $theme = $params->get('theme', 'default');
        return $this->display(__FILE__, 'tmpl/' . $theme . '/productdetail.tpl');
    }
    /**
     * Proccess module by hook
     * $pparams: param of module
     * $pos: position call
     */
    public function getParams()
    {
        return $this->_params;
    }
    private function processHook($pparams, $pos = "left")
    {
        $params = $this->getParams();
        $site_url = Tools::htmlentitiesutf8('http://' . $_SERVER['HTTP_HOST'] . __PS_BASE_URI__);
        // get params
        $tmp = $params->get('module_height', '375');
        $moduleHeight = ($tmp == 'auto') ? 'auto' : (int)$tmp . 'px';
        $tmp = $params->get('module_width', 'auto');
        $moduleWidth = ($tmp == 'auto') ? 'auto' : (int)$tmp . 'px';
        $img_size = $params->get("img_size", "tabs_allinmart");
        $show_addcart = $params->get("show_addcart", 1);
        $show_des = $params->get("show_des", 1);
        $show_date = $params->get("show_date", 1);
        $selectCat = $params->get("category", "");
        $showdeal = trim($params->get("showdeal", "recently"));
        $limit = $params->get("limitonmodule", 4);
        $orderby = $params->get("orderby", "");
        $where = "";
        if ($params->get("home_sorce") == "productids") {
            if ($params->get("productids", "")) {
                $proArray = explode(",", $params->get("productids"));
                if (count($proArray) == 1) {
                    $where = " AND p.id_product = " . $proArray[0];
                } else {
                    $where = " AND p.id_product IN (" . implode(',', $proArray) . ")";
                }
            }
        } else {
            if ($selectCat != "" && $selectCat != "all") {
                $catArray = explode(",", $selectCat);
                if (count($catArray) == 1) {
                    $where = " AND cp.`id_category` = " . $catArray[0];
                } else {
                    $where = " AND cp.`id_category` IN (" . implode(',', $catArray) . ")";
                }
            }
        }
        $products = ClassLofDeal::getList($this, $where, 0, $limit, $orderby, $showdeal);
        if (!$products)
            return false;
        //echo "<pre>".print_r($products,1); die;
        // template asignment variables
        $this->smarty->assign(array(
            'modName' => $this->name,
            'blockid' => $this->id,
            'moduleWidth' => $moduleWidth,
            'moduleHeight' => $moduleHeight,
            'site_url' => $site_url,
            'theme' => $params->get('theme'),
            'lofdeal_products' => $products,
            'lofdeal_img_size' => $img_size,
            'lofdeal_show_addcart' => $show_addcart,
            'lofdeal_show_des' => $show_des,
            'lofdeal_show_date' => $show_date,
            'id_lang' => $this->context->language->id,
        ));
        // render for content layout of module
        $theme = $params->get('theme');
        $theme = $params->get("theme", "default");
        return $this->display(__FILE__, 'tmpl/' . $theme . '/default.tpl');
    }
    function orderAmountDiscountInAProduct($productsl = array(), $type = 'all')
    {
        if (!$productsl || $type == 'all')
            return $productsl;
        $result = array();
        for ($i = 0; $i < count($productsl); $i++) {
            $checkExist = false;
            $arrProductId = array();
            for ($j = $i + 1; $j < count($productsl); $j++) {
                if ($productsl[$i]['id_product'] == $productsl[$j]['id_product']) {
                    $checkExist = true;
                    switch ($type) {
                        case 'price_lowest':
                            if ($productsl[$i]['reductionprice'] <= $productsl[$j]['reductionprice']) {
                                $productTG = $productsl[$i];
                            } else {
                                $productTG = $productsl[$j];
                            }
                            $arrProductId[] = $j;
                            break;
                        case 'price_highest':
                            if ($productsl[$i]['reductionprice'] >= $productsl[$j]['reductionprice']) {
                                $productTG = $productsl[$i];
                            } else {
                                $productTG = $productsl[$j];
                            }
                            $arrProductId[] = $j;
                            break;
                    }
                }
            }
            if ($checkExist) {
                $productsl[$i] = $productTG;
                if ($arrProductId) {
                    foreach ($arrProductId as $id) {
                        unset($productsl[$id]);
                    }
                }
                $LOFproductsl = array();
                foreach ($productsl as $lofPro) {
                    $LOFproductsl[] = $lofPro;
                }
                $productsl = $LOFproductsl;
            }
        }
        return $productsl;
    }
    /**
     * get main image and thumb
     * @param poiter $row .
     * @return void
     */
    public function parseImages($product, $params)
    {
        global $link;
        $isRenderedMainImage = $params->get("cre_main_size", 0);
        $mainImageSize = $params->get("img_size", 'tab');
        if ($isRenderedMainImage) {
            $product["mainImge"] = $link->getImageLink($product["link_rewrite"], $product["id_image"]);
        } else {
            $product["mainImge"] = $link->getImageLink($product["link_rewrite"], $product["id_image"], $mainImageSize);
        }
        $product["thumbImge"] = $product["mainImge"];
        return $product;
    }
    /**
     * Get list of sub folder's name
     */
    public function getFolderList($path)
    {
        $items = array();
        $handle = opendir($path);
        if (!$handle) {
            return $items;
        }
        while (false !== ($file = readdir($handle))) {
            if (is_dir($path . $file))
                $items[$file] = $file;
        }
        unset($items['.'], $items['..'], $items['.svn']);
        return $items;
    }
    /**
     * Render processing form && process saving data.
     */
    public function getContent()
    {
        $html = "";
        if (Tools::isSubmit('submit')) {
            $this->_postValidation();
            if (!sizeof($this->_postErrors)) {
                $definedConfigs = array(
                    'module_theme' => '',
                    'img_size' => '',
                    'description' => '',
                    'des_max_chars' => '',
                    'limitonpage' => '',
                    'limitonmodule' => '',
                    //'title'           => '',
                    //'finish'          => '',
                    'orderby' => '',
                    'show_filter' => '',
                    'dealtype' => '',
                    'home_sorce' => '',
                    'show_addcart' => '',
                    'count_bought' => '',
                    'show_date' => '',
                    //'amount_discount' => '',
                    'productids' => ''
                );
                foreach ($definedConfigs as $config => $key) {
                    if (strlen($this->name . '_' . $config) >= 32) {
                        echo $this->name . '_' . $config;
                    } else {
                        Configuration::updateValue($this->name . '_' . $config, Tools::getValue($config), true);
                    }
                }
                if (Tools::getValue('category')) {
                    if (in_array("", Tools::getValue('category'))) {
                        $catList = "";
                    } else {
                        $catList = implode(",", Tools::getValue('category'));
                    }
                    Configuration::updateValue($this->name . '_category', $catList, true);
                }
                if (Tools::getValue('showdeal')) {
                    if (in_array("", Tools::getValue('showdeal'))) {
                        $catList = "";
                    } else {
                        $catList = implode(",", Tools::getValue('showdeal'));
                    }
                    Configuration::updateValue($this->name . '_showdeal', $catList, true);
                }
                $html .= '<div class="conf confirm">' . $this->l('Settings updated successful') . '</div>';
            } else {
                foreach ($this->_postErrors AS $err) {
                    $html .= '<div class="alert error">' . $err . '</div>';
                }
            }
            // reset current values.
            $this->_params = new LofParams($this->name);
        }
        return $html . $this->_getFormConfig();
    }
    /**
     * Render Configuration From for user making settings.
     * @return context
     */
    private function _getFormConfig()
    {
        $html = '';
        $mes = '';
        $formats = ImageType::getImagesTypes('products');
        $themes = $this->getFolderList(dirname(__FILE__) . "/tmpl/");
        ob_start();
        include_once dirname(__FILE__) . '/config/lofdeal.php';
        $html .= ob_get_contents();
        ob_end_clean();
        return $html;
    }
    /**
     * Process vadiation before saving data
     */
    private function _postValidation()
    {
        if (!Validate::isCleanHtml(Tools::getValue('des_max_chars')) || !is_numeric(Tools::getValue('des_max_chars')))
            $this->_postErrors[] = $this->l('The description max chars you entered was not allowed, sorry');
        if (!Validate::isCleanHtml(Tools::getValue('limitonmodule')) || !is_numeric(Tools::getValue('limitonmodule')))
            $this->_postErrors[] = $this->l('The Limit you entered was not allowed, sorry');
    }
    /**
     * Get value of parameter following to its name.
     * @return string is value of parameter.
     */
    public function getParamValue($name, $default = '')
    {
        return $this->_params->get($name, $default);
    }
} 