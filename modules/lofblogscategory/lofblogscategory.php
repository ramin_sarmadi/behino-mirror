<?php
/*
 * 2011 LandOfCoder
 *
 *  @author LandOfCoder 
 *  @copyright  2011 LandOfCoder
 *  @version  Release: $Revision: 1.0 $
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */
if (!defined('_PS_VERSION_'))
    exit;
require_once(_PS_MODULE_DIR_ . "lofblogscategory/defined.php");
if (!class_exists('LOFXParams')) {
    require LOFCONTENTMENU_ROOT . 'config/params.php';
}
if (!class_exists('lofContentHelper')) {
    require _PS_MODULE_DIR_ . 'lofblogs/libs/lof_content_helper.php';
}
class lofBlogsCategory extends Module {
    /* @var boolean error */
    protected $error = false;
    private $_postErrors = array();
    public function __construct() {
        $this->name = 'lofblogscategory';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'LandOfCoder';
        $this->need_instance = 0;
        parent::__construct();
        $this->params = new LOFXParams($this);
        $this->displayName = $this->l('Lof Blogs Category');
        $this->description = $this->l('Show list of link to category page for Lof Blogs content');
        $this->confirmUninstall = $this->l('do you realy want to uninstall Lof Blogs Category?');
    }
    public function install() {
        if (parent::install() == false
                OR !$this->registerHook('header')
                OR !$this->registerHook('rightColumn')
        )
            return false;
        return true;
    }
    public function uninstall() {
        if (!parent::uninstall())
            return false;
        return true;
    }
    /**
     * Render processing form && process saving data.
     */
    public function getContent() {
        $html = "";
        if (Tools::isSubmit('submit')) {
            $this->_postValidation();
            if (is_array($this->_postErrors) && !count($this->_postErrors)) {
                $this->params->update();
                $html .= '<div class="conf confirm">' . $this->l('Settings updated') . '</div>';
            }
        }
        if ($this->params->hasError())
            die($this->params->getErrorMsg());
        return $html . $this->params->displayForm();
    }
    function hookHeader() {
        $helper = new lofContentHelper();
        $template = $this->params->get('template', 'default');
        $helper->moduleMedia($this->name, $template);
		$this->context->controller->addJS(_THEME_JS_DIR_.'tools/treeManagement.js');
		$this->context->controller->addCSS(($this->_path).'blockcategories.css', 'all');
    }
    function hookleftColumn($params) {
        return $this->processHook($params, 'left');
    }
    function hookrightColumn($params) {
        return $this->processHook($params, 'right');
    }
    /**
     * Process vadiation before saving data 
     */
    private function _postValidation() {
        
    }
    function processHook($params, $hook='') {
        global $smarty;
        $items = null;
        $params = $this->params->getValues();       
        $items = $this->getCategories();
        $smarty->assign(array(
            'items' => $items,
            'params' => $params
        ));
        $this->smarty->assign('branche_tpl_path', _PS_MODULE_DIR_.'lofblogscategory/themes/category-tree-branch.tpl');
		$isDhtml = (Configuration::get('BLOCK_CATEG_DHTML') == 1 ? true : false);
		$this->smarty->assign('isDhtml', $isDhtml);

        return $this->display(__FILE__, $this->getLayoutPath('default'));
    }
    function getCategories() {
        global $cookie;
        $lang = $cookie->id_lang ? $cookie->id_lang : $this->params->defaultLang;
        
        $query = 'SELECT c.id_lofblogs_category, cl.name, cl.link_rewrite, c.id_parent, cl.description';
        $query .= ' FROM ' . _DB_PREFIX_ . 'lofblogs_category c ';
        $query .= ' LEFT JOIN ' . _DB_PREFIX_ . 'lofblogs_category_lang cl ON (c.id_lofblogs_category = cl.id_lofblogs_category)';
        $query .= ' WHERE cl.id_lang = ' . pSQL($lang);
        $query .= ' AND c.level_depth > 0';
        $query .= ' AND c.active = 1';
        $catids = $this->params->get('cats', '');
        if ($catids) {
            //filter bt category id :
            $query .= ' AND c.id_lofblogs_category IN (' . $catids . ')';
        }
        
        //list ordering :
        $query .= ' ORDER BY '.  $this->params->get('ordering', 'c.id_lofblogs_category DESC');
        
        //list limit , zero mean no limit :
        if($this->params->get('count', 0)) {
            $query .= ' LIMIT 0,' . $this->params->get('count', 0);
        }
        
        $items = Db::getInstance()->ExecuteS($query);
        foreach ($items as $k => $item) {
			$i = $item['id_lofblogs_category'];
			$new_items[$i]['link'] = lofContentHelper::getCategoryLink($item['id_lofblogs_category'], $item['link_rewrite']);
			$new_items[$i]['name'] = $item['name'];
			$new_items[$i]['desc'] = $item['description'];
            if(!array_key_exists('children', $new_items[$i]))
				$new_items[$i]['children'] = array();
			$j = $item['id_parent'];
			$new_items[$j]['children'][] = $new_items[$i];
        }
        return $new_items[1];
    }
    public function getLayoutPath($layout='default') {
        $theme = $this->params->get('template', 'default');
        $template = 'themes/' . $theme . '/'.$layout.'.tpl';
        if (!file_exists(__FILE__ . "/" . $template)) {
            return $template;
        }
    }
}