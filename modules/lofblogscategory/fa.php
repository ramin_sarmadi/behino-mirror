<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{lofblogscategory}prestashop>lofblogscategory_64f8902039652bf098a285a9c4dc12e5'] = 'شاخه های بلاگ';
$_MODULE['<{lofblogscategory}prestashop>lofblogscategory_5268c9c231685a2afcd3ff6559d93499'] = 'نمایش لیستی از لینک ها به صفحه ی شاخه های مطالب بلاگ';
$_MODULE['<{lofblogscategory}prestashop>lofblogscategory_0f5e0f5b39466287332211f3217aa0cf'] = 'آیا از حذف ماژول اطمینان دارید؟';
$_MODULE['<{lofblogscategory}prestashop>lofblogscategory_c888438d14855d7d96a2724ee9c306bd'] = 'تنظیمات به روز شد';
$_MODULE['<{lofblogscategory}prestashop>form_06933067aafd48425d67bcb01bba5cb6'] = 'به روز رسانی';
$_MODULE['<{lofblogscategory}prestashop>params_lang_4ede758053afb49f54ea2ef0d5276910'] = 'تنظیمات عمومی';
$_MODULE['<{lofblogscategory}prestashop>params_lang_d721757161f7f70c5b0949fdb6ec2c30'] = 'قالب';
$_MODULE['<{lofblogscategory}prestashop>params_lang_b78a3223503896721cca1303f776159b'] = 'عنوان';
$_MODULE['<{lofblogscategory}prestashop>params_lang_e40a71c9e79b9f11659ee8cef8d8ad92'] = 'نمایش/مخفی سازی عنوان';
$_MODULE['<{lofblogscategory}prestashop>params_lang_38d4de28b224822af6f27b2f47accc2a'] = 'آیتم شمارش شده';
$_MODULE['<{lofblogscategory}prestashop>params_lang_68f7cdec4e4979a9870e95636c846a0d'] = 'اولویت لیست';
$_MODULE['<{lofblogscategory}prestashop>params_lang_03368e3c1eb4d2a9048775874301b19f'] = 'انتخاب شاخه';
