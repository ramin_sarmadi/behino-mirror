<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include_once(dirname(__FILE__).'/parsianpayment.php');

if (!$cookie->isLogged())
    Tools::redirect('authentication.php?back=order.php');

$parsianpayment= new parsianpayment();

$status = $_REQUEST['rs'];
$authority = $_REQUEST['au'];

$result = $parsianpayment->confirmPayment($status,$authority);


if (($result == '0'))
{
    $currency = new Currency(intval(isset($_POST['currency_payement']) ? $_POST['currency_payement'] : $cookie->id_currency));
    $total = floatval(number_format($cart->getOrderTotal(true, 3), 2, '.', ''));

    $link=(__PS_BASE_URI__.'../../order-confirmation.php?id_cart='.$cart->id.'&id_module='.$parsianpayment->id.'&id_order='.$parsianpayment->currentOrder.'&key='.$customer->secure_key);

    echo '<a href="'.$link.'" > تاریخچه سفارشات شما </a>';


    $parsianpayment->validateOrder($cart->id,  _PS_OS_PAYMENT_, $total, $parsianpayment->displayName, NULL, NULL, $currency->id, false, $cart->secure_key);
    $order = new Order($parsianpayment->currentOrder);

    Tools::redirectLink(__PS_BASE_URI__.'order-confirmation.php?id_cart='.$cart->id.'&id_module='.$parsianpayment->id.'&id_order='.$parsianpayment->currentOrder.'&key='.$customer->secure_key);



} else {
    $res=($parsianpayment->showMessages($result));
    $link=('../../order-confirmation.php');
    echo '<a href="'.$link.'" > تاریخچه سفارشات شما </a>';

    echo $res[0];
}

include_once(dirname(__FILE__).'/../../footer.php');

?>