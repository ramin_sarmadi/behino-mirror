<!-- Parsian Payment Module -->
<p class="payment_module">
    <a href="javascript:$('#parsianpayment_form').submit();" title="{l s='Pay by Parsian' mod='parsianpayment'}">
        <img src="modules/parsianpayment/parsian.png" alt="{l s='Pay by Parsian' mod='parsianpayment'}" />
		{l s='Pay by Debit/Credit card through Parsian Bank.' mod='parsianpayment'}
<br>
</a></p>
<form action="modules/parsianpayment/payment.php" method="post" id="parsianpayment_form" class="hidden">
    <input type="hidden" name="orderId" value="{$orderId}" />
</form>
<br><br>
<!-- End of Parsian Payment Module-->
