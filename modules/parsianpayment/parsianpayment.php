<?php

class parsianpayment extends PaymentModule{

    private $_html = '';
    private $_postErrors = array();

    public function __construct()
    {

        $this->name = 'parsianpayment';
        $this->tab = 'payments_gateways';
        $this->version = 1.0;
        $this->author = 'Ariadp.com(Ali Masooumi)';

        $this->currencies = true;
        $this->currencies_mode = 'radio';

        parent::__construct();

        $this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('Parsian Payment');
        $this->description = $this->l('A free module to pay online for Parsain.');
        $this->confirmUninstall = $this->l('Are you sure, you want to delete your details?');

        if (!sizeof(Currency::checkPaymentCurrencies($this->id)))
            $this->warning = $this->l('No currency has been set for this module');

        $config = Configuration::getMultiple(array('PARSIAN_PIN_CODE', ''));
        if (!isset($config['PARSIAN_PIN_CODE']))
            $this->warning = $this->l('Your Parsian Pin Code must be configured in order to use this module');

        $config = Configuration::getMultiple(array('PARSIAN_WEB_SERVICE', ''));
        if (!isset($config['PARSIAN_WEB_SERVICE']))
            $this->warning = $this->l('select web service of Parsian Bank to use this module');

        if ($_SERVER['SERVER_NAME'] == 'localhost')
            $this->warning = $this->l('Your are in localhost, Parsian Payment can\'t validate order');

    }
    public function install(){
        if (!parent::install()
            OR !Configuration::updateValue('PARSIAN_PIN_CODE', '')
            OR !Configuration::updateValue('PARSIAN_WEB_SERVICE', '')
            OR !$this->registerHook('payment')
            OR !$this->registerHook('paymentReturn')){
            return false;
        }else{
            return true;
        }
    }
    public function uninstall(){
        if (!Configuration::deleteByName('PARSIAN_PIN_CODE')
            OR !Configuration::deleteByName('PARSIAN_WEB_SERVICE')
            OR !parent::uninstall())
            return false;
        return true;
    }

    public function displayFormSettings()
    {
        $this->_html .= '
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset>
				<legend><img src="../img/admin/cog.gif" alt="" class="middle" />'.$this->l('Settings').'</legend>
				<label>'.$this->l('Parsian PIN').'</label>
				<div class="margin-form"><input type="text" size="30" name="ParsianPinCode" value="'.Configuration::get('PARSIAN_PIN_CODE').'" /></div>
				<label>'.$this->l('Parsian Bank Web Service').'</label>
				<div class="margin-form"><select name="webservice">
					<option value="new" '.(Tools::getValue('webservice') == 'new' ? 'selected="selected"' : '').'>pecco24.com</option>
					<option value="old" '.(Tools::getValue('webservice') == 'old' ? 'selected="selected"' : '').'>pec24.com</option>
					</select></div>
				<center><input type="submit" name="submitParsian" value="'.$this->l('Update Settings').'" class="button" /></center>			
			</fieldset>
		</form>';
    }

    public function displayConf()
    {
        $this->_html .= '
		<div class="conf confirm">
			<img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />
			'.$this->l('Settings updated').'
		</div>';
    }

    public function displayErrors()
    {
        foreach ($this->_postErrors AS $err)
            $this->_html .= '<div class="alert error">'. $err .'</div>';
    }

    public function getContent()
    {
        $this->_html = '<h2>'.$this->l('Parsian Payment').'</h2>';
        if (isset($_POST['submitParsian']))
        {
            if (empty($_POST['ParsianPinCode']))
                $this->_postErrors[] = $this->l('Parsian PIN is required.');
            if (empty($_POST['webservice']))
                $this->_postErrors[] = $this->l('Your Hash Key is required.');
            if (!sizeof($this->_postErrors))
            {
                Configuration::updateValue('PARSIAN_PIN_CODE', $_POST['ParsianPinCode']);
                Configuration::updateValue('PARSIAN_WEB_SERVICE', $_POST['webservice']);
                $this->displayConf();
            }
            else
                $this->displayErrors();
        }

        $this->displayFormSettings();
        return $this->_html;
    }

    private function displayParsianPayment()
    {
        $this->_html .= '<img src="../modules/parsianpayment/parsian.png" style="float:left; margin-right:15px;"><b>'.$this->l('This module allows you to accept payments by Parsian.').'</b><br /><br />
		'.$this->l('Any cart from Shetab Banks are accepted.').'<br /><br /><br />';

    }

    public function execPayment($cart)
    {
        include('nusoap.php');
        global $cookie, $smarty;

        $purchase_currency = $this->GetCurrency();
        if ($cookie->id_currency == $purchase_currency->id)
            $PurchaseAmount = number_format($cart->getOrderTotal(true, 3), 0, '', '');
        else
            $PurchaseAmount= number_format(Tools::convertPrice($cart->getOrderTotal(true, 3), $purchase_currency), 0, '', '');

        if (Configuration::get('PARSIAN_WEB_SERVICE') == 'new') {
            $soapclient = new nusoap_client('https://www.pecco24.com:27635/pecpaymentgateway/eshopservice.asmx?wsdl', 'wsdl');
        } else {
            $soapclient = new nusoap_client('https://www.pec24.com/pecpaymentgateway/eshopservice.asmx?wsdl', 'wsdl');
        }
        $err = $soapclient->getError();
        if ($err) {
            $this->_postErrors[] = $this->l('Could not connect to bank or service.');
            $this->displayErrors();
        }
        $ParsianPin = Configuration::get('PARSIAN_PIN_CODE');
        $amount = intval($PurchaseAmount)*10;
        $times=substr( time(), 0);
        $times=substr( $times, -2);
        $OrderId = $cart->id.$times;
        $authority = 0;
        $status = 1;
        $ParsURL = 'payment.php';
        $CpiReturnUrl = (Configuration::get('PS_SSL_ENABLED') ?'https://' :'http://').$_SERVER['HTTP_HOST'].__PS_BASE_URI__.'modules/parsianpayment/validation.php';

        $i = 10; //to garantee the connection and authorization, this process should be repeat maximum 10 times
        do{
            $params = array(
                'pin' => $ParsianPin, // this is our PIN NUMBER
                'amount' => $amount,
                'orderId' => $OrderId,
                'callbackUrl' => $CpiReturnUrl,
                'authority' => $authority,
                'status' => $status);

            $date_inv=date("Y-m-d H:i:s");
            $res = $soapclient->call('PinPaymentRequest', $params);

            $authority = $res['authority'];
            $status = $res['status'];


            //
            $date = date("Y-m-d H:i:s");
            $id_customer = $this->context->customer->id;
            $price = $amount;
            $payment_order=$OrderId;
            $id_cart= $cart->id;
            $dataArr = array(
                'id_order'=>0
            ,'authority'=>$authority
            ,'id_customer'=>$id_customer
            ,'payment_order'=>$payment_order
            ,'id_cart'=>$id_cart
            ,'total_paid'=>$price
            ,'delivery_number'=>$status
            ,'invoice_date'=>$date_inv
            ,'delivery_date'=>$date
            ,'result'=>'goto Parsian'
            ,'valid'=>0
            ,'date_add'=>$date
            ,'date_upd'=>'0000-00-00 00:00:00'
            );

            Db::getInstance()->insert('orders_parsian', $dataArr);
            //
            $OrderId = intval($OrderId)-1;
            $i -= 1;
        } while(($status==30) and ($i>0));


        //--------------------------------
        if (($authority) and ($status == 0)) {
            // this is a succcessfull connection
            //saving order information

            echo "
                <p align='center' >
                " .$this->l('Conection to Parsian Bank...')."
                <br><br>
                <img align='middle' src='loader.gif' />
                </p>";
            //redirecting
            if (Configuration::get('PARSIAN_WEB_SERVICE') == 'new') {

                $ParsURL = "https://www.pecco24.com/pecpaymentgateway/default.aspx?au=".$authority;
            } else {
                $ParsURL = "https://www.pec24.com/pecpaymentgateway/default.aspx?au=" . $authority;
            }
            Tools::redirectLink($ParsURL);
            exit() ;
            die() ;
            return;
        }
        else
        {
            $this->_postErrors[] = $this->l('error code : ') .$status ;
            $this->displayErrors();
        }

        return $this->_html;
    }
    public function confirmPayment($status,$authority){
        include('nusoap.php');

        if (($status == '0')) {

            if (Configuration::get('PARSIAN_WEB_SERVICE') == 'new')
            {
                $soapclient = new nusoap_client('https://www.pecco24.com:27635/pecpaymentgateway/eshopservice.asmx?wsdl', 'wsdl');
            } else {
                $soapclient = new nusoap_client('https://www.pec24.com/pecpaymentgateway/eshopservice.asmx?wsdl', 'wsdl');
            }
            $err = $soapclient->getError();
            if ($err) {
                $this->_postErrors[] = $this->l('error') . $err;
                $this->displayErrors();
            }

            $ParsianPin = Configuration::get('PARSIAN_PIN_CODE');

            $params = array(
                'pin' => $ParsianPin ,  // this is our PIN NUMBER
                'authority' => $authority,
                'status' => 1) ; //

            $res = $soapclient->call('PinPaymentEnquiry', $params);
            //
            $SQL = '
			SELECT total_paid,payment_order,id_cart
			FROM `'._DB_PREFIX_.'orders_parsian` op
			WHERE op.`authority` = '.$authority.' ORDER BY id_order_parsian DESC LIMIT 1 ';

            $result_q=Db::getInstance()->executeS($SQL);

            $date = date("Y-m-d H:i:s");
            $id_customer = $this->context->customer->id;
            $price = $result_q[0]['total_paid'];
            $payment_order=$result_q[0]['payment_order'];
            $id_cart= $result_q[0]['id_cart'];
            $msg=$this->showMessages($res['status']);
            $dataArr = array(
                'id_order'=>0
            ,'authority'=>$authority
            ,'id_customer'=>$id_customer
            ,'payment_order'=>$payment_order
            ,'id_cart'=>$id_cart
            ,'total_paid'=>$price
            ,'delivery_number'=>$res['status']
            ,'invoice_date'=>$date
            ,'delivery_date'=>'0000-00-00 00:00:00'
            ,'result'=>'check Parsian -- '.$msg[0]
            ,'valid'=>1
            ,'date_add'=>$date
            ,'date_upd'=>'0000-00-00 00:00:00'
            );

            Db::getInstance()->insert('orders_parsian', $dataArr);
            //
            if ($client->fault) {
                echo '<h2>Fault1</h2><pre>';
                print_r($result);
                echo '</pre>';
                die();
            } else {
                $status = $res['status'];
            }
        }
        return $status;
    }

    public function showMessages($result)
    {
        switch($result)
        {
            case '0':
                $this->_postErrors[]=$this->l('The transaction was approved.');
                break;
            case '1':
                $this->_postErrors[]=$this->l('The transaction was canceled.');
                break;
            case '20':
                $this->_postErrors[]=$this->l('Parsian PIN or IP Address is not correct.');
                break;
            case '21':
                $this->_postErrors[]=$this->l('Do not try checking for this order.');
                break;
            case '22':
                $this->_postErrors[]=$this->l('Parsian PIN or IP Address is not correct.');
                break;
            case '30':
                $this->_postErrors[]=$this->l('Operation has already been performed successfully or suspended.');
                break;
            case '34':
                $this->_postErrors[]=$this->l('Transaction number is not correct or expired.');
                break;
            default:
                $this->_postErrors[]=$this->l('');
        }
        $this->displayErrors();
        echo $resultA[0]= $this->_html;
        $resultA[1]=$result;
        return $resultA;
    }

    public function hookPayment($params){

        if (!$this->active)
            return ;

        return $this->display(__FILE__, 'payment.tpl');
    }

    public function hookPaymentReturn($params)
    {
        if (!$this->active)
            return ;

        return $this->display(__FILE__, 'confirmation.tpl');
    }

}