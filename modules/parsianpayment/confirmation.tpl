<p>{l s='Your order on' mod='parsianpayment'} <span class="bold">{$shop_name}</span> {l s='is complete.' mod='parsianpayment'}
	<br /><br />
	{l s='You have chosen the Parsian Payment method.' mod='parsianpayment'}
	<br /><br /><span class="bold">{l s='Your order will be sent very soon.' mod='parsianpayment'}</span>
	<br /><br />{l s='For any questions or for further information, please contact our' mod='parsianpayment'} <a href="{$base_dir_ssl}contact-form.php">{l s='customer support' mod='parsianpayment'}</a>.
</p>
