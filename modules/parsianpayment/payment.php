<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include_once(dirname(__FILE__).'/parsianpayment.php');

if (!$cookie->isLogged())
    Tools::redirect('authentication.php?back=order.php');
	
$parsianpayment= new parsianpayment();
echo $parsianpayment->execPayment($cart);

include_once(dirname(__FILE__).'/../../footer.php');

?>
