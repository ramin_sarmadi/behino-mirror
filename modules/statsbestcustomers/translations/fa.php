<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_4f29d8c727dcf2022ac241cb96c31083'] = 'خالی کردن ثبت شده ها';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_f5c493141bb4b2508c5938fd9353291a'] = 'نمايش %1$s از %2$s';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_77587239bf4c54ea493c7033e1dbf636'] = 'نام خانوادگی';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_bc910f8bdf70f29374f496f05be0330c'] = 'نام';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'ایمیل';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_d7e637a6e9ff116de2fa89551240a94d'] = 'بازدیدها';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_1ff1b62d08d1195f073c6adac00f1894'] = 'پول خرج شده';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_8b83489bd116cb60e2f348e9c63cd7f6'] = 'بهترین مشتری ها';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_ca5c7eec61c59f2bd4572d1c5f34b10a'] = 'یک لیستی از بهترین مشتری ها';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_998e4c5c80f27dec552e99dfed34889a'] = 'استخراج CSV';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_6602bbeb2956c035fb4cb5e844a4861b'] = 'راهنما';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_cb21e843b037359d0fb5b793fccf964f'] = 'توسعه وفاداری مشتری ها';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_0c854a411bcae590a2a44a9b18487d12'] = 'نگه  داشتن یک مشتری سود آور تر از به دست آوردن یک مشتری جدید است. بنابراین ،  وفادار ساختن آنها ضروری است، به عبارت دیگر آنها را وادار به بازگشت به  فروشگاه خود کنید.';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_197bad7ad08abfd1dc45ab92f96f155d'] = 'راضی بودن یک مشتری وسیله ای برای جذب یک مشتری جدید است، یک مشتری ناراضی مشتری جدیدی را جذب نمیکند.';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_860b64638db92a2d084e4e5182b20f31'] = 'برای رسیدن به این هدف شما می توانید موارد زیر را سازماندهی کنید:';
$_MODULE['<{statsbestcustomers}prestashop>statsbestcustomers_99666939cbe5b9f120bb5a5050abda66'] = 'این عملیات مشتریان را به خرید محصولات و بازدید منظم از فروشگاه شما تشویق می کند.';
