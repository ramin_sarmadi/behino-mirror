<?php
/**
 * @name defined.php
 * @author landOfCoder
 * @todo defined some path and link
 */

// *************** SOME PATHS ******************************
define('LOFBLOGSSEARCH_ROOT', _PS_MODULE_DIR_ . 'lofblogssearch/');
define('LOFBLOGSSEARCH_THEME', LOFBLOGSSEARCH_ROOT.'themes/');

// ***************** SOME LINKS ***************************************

define('LOFBLOGSSEARCH_BASE_URI', __PS_BASE_URI__ . 'modules/lofblogssearch/');
define('LOFBLOGSSEARCH_THEME_URI', LOFBLOGSSEARCH_BASE_URI.'themes/');

?>
