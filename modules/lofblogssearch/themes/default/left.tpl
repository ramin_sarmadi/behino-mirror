<!-- Block tags module -->
<div class="block">
	<h4 class="title_block">{l s='Blogs Search' mod='lofblogssearch'}</h4>
	<form id="blogs_search_form" action="{$lofContentHelper->getSearchLink()}" method="get">
		<input type="hidden" name="view" value="search">
		<input type="hidden" name="fc" value="module">
		<input type="hidden" name="module" value="lofblogs">
		<input type="hidden" name="controller" value="articles">
		<input type="text" name="title" size="30">
		<input type="submit" value="{l s='Search' mod='lofblogssearch'}" name="submitSearch"> 
	</form>
</div>
<!-- /Block tags module -->
