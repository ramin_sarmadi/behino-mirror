<?php

class AdminProductsController extends AdminProductsControllerCore
{
	
	public function initFormFeatures($obj)
	{
		$data = $this->createTemplate($this->tpl_form);
		if (!Feature::isFeatureActive())
			$this->displayWarning($this->l('This feature has been disabled, you can active this feature at this page:').' <a href="index.php?tab=AdminPerformance&token='.Tools::getAdminTokenLite('AdminPerformance').'#featuresDetachables">'.$this->l('Performances').'</a>');
		else
		{
			if ($obj->id)
			{
				if ($this->product_exists_in_shop)
				{
//////collect groups
$id_shop=Shop::CONTEXT_SHOP;
$lang=$this->context->language->id;

$product_fgroups=array();
$pgroups=array();

$sqlgroups=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."features_groups_categ WHERE id_categ='$obj->id_category_default' AND id_shop='$id_shop'");

if ($sqlgroups) {
foreach ($sqlgroups as $item) {
$row=array();
$getname=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."features_groups_lang WHERE id_group='".$item['id_group']."' AND id_lang='$lang'");

$row['id']=$item['id_group'];
$row['name']=$getname['groupname'];
$pgroups[]=$item['id_group'];
$product_fgroups[]=$row;
}
}
else {
$sqlgroupsall=Db::getInstance()->ExecuteS("SELECT * FROM  "._DB_PREFIX_."features_groups WHERE id_shop='$id_shop'");
if ($sqlgroupsall) {
foreach ($sqlgroupsall as $item) {
$row=array();
$getname=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."features_groups_lang WHERE id_group='".$item['id_group']."' AND id_lang='$lang'");

$row['id']=$item['id_group'];
$row['name']=$getname['groupname'];
$pgroups[]=$item['id_group'];
$product_fgroups[]=$row;
}
}

}
				
///////////
				
				
					$features = Feature::getFeatures($this->context->language->id, (Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP));

					foreach ($features as $k => $tab_features)
					{
						$features[$k]['current_item'] = false;
						$features[$k]['val'] = array();

						$custom = true;
						foreach ($obj->getFeatures() as $tab_products)
							if ($tab_products['id_feature'] == $tab_features['id_feature'])
								$features[$k]['current_item'] = $tab_products['id_feature_value'];

						$features[$k]['featureValues'] = FeatureValue::getFeatureValuesWithLang($this->context->language->id, (int)$tab_features['id_feature']);
						if (count($features[$k]['featureValues']))
							foreach ($features[$k]['featureValues'] as $value)
								if ($features[$k]['current_item'] == $value['id_feature_value'])
									$custom = false;

						if ($custom){
                            $features[$k]['val'] = FeatureValue::getFeatureValueLang($features[$k]['current_item']);
                        }

                        $features[$k]['value_prd'] = FeatureValue::getFeatureValuePrd((int)Tools::getValue('id_product'),$features[$k]['current_item']);


                    }
					
/////////selecting only feaetures that are in groups
$featureingroups=array();

   foreach ($product_fgroups as $pfg) {
		
		            foreach ($features as $k => $tab_features)

					{
                    $sqlfeature=Db::getInstance()->getRow("SELECT * FROM  "._DB_PREFIX_."featurestogroups WHERE id_group='".$pfg['id']."' AND id_shop='$id_shop' AND id_feature='".$tab_features['id_feature']."'");
                    if ($sqlfeature) {
                    $tab_features['name'].=' ('.$pfg['name'].')';
                    $grouprow=array('name'=>$pfg['name']);
                    $featureingroups[]=$tab_features;


                    }

					}

	    }

///////////					
                    //print_r($featureingroups);

					$data->assign('available_features', $featureingroups);

					$data->assign('product', $obj);
					$data->assign('link', $this->context->link);
					$data->assign('languages', $this->_languages);
					$data->assign('default_form_language', $this->default_form_language);
				}
				else
					$this->displayWarning($this->l('You must save this product in this shop before adding features.'));
			}
			else
				$this->displayWarning($this->l('You must save this product before adding features.'));
		}
		$this->tpl_form_vars['custom_form'] = $data->fetch();
	}
	
	
}

