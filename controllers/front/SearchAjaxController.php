<?php
class SearchAjaxControllerCore extends FrontController
{
    public $php_self = 'searchAjax';
    public function init()
    {
        parent::init();
        $this->canonicalRedirection();
    }
    public function initContent()
    {
        parent::initContent();
        if (Tools::isSubmit('go')) {
            switch (Tools::getValue('go')) {
                case 'true':
                    $this->ResultProductSearch();
                    break;
            }
        }
        $Product_Name = (Tools::getValue('NAME_PRODUCT'));
        $Product_Min_Cost = (Tools::getValue('min_cost'));
        $Product_Max_Cost = (Tools::getValue('max_cost'));
        $Product_features_values = (Tools::getValue('feature_value'));
        $Product_id_category = (Tools::getValue('id_category'));
        $PageResult = (Tools::getValue('page'));
        $CountRows = 12;
        if ($PageResult) {
            $PageRows = ($PageResult * $CountRows) - $CountRows;
            $Q_Limit = 'LIMIT ' . $PageRows . ' , ' . $CountRows . '';
        } else {
            $Q_Limit = 'LIMIT 0 , ' . $CountRows . '';
        }
        $id_features = '';
        $id_features_value = '';
        $id_categorys = '';
        foreach ($Product_id_category as $id_category) {
            if($id_category>0){
                $id_categorys = $id_categorys . ',' . $id_category;
            }
        }
        $id_categorys = substr($id_categorys, 1);
        if (strlen($id_categorys) > 0) {
            $action = true;
            $Q_id_categorys = 'AND cl.id_category IN (' . $id_categorys . ')';
        } else {
            $Q_id_categorys = '';
        }
        foreach ($Product_features_values as $feature_values) {
            $feature_value = explode("_", $feature_values);
                //echo '<div>'.count($feature_value);
                if ((count($feature_value) > 1)&&($feature_value[0]>0)) {
                    $id_features = $id_features . ',' . $feature_value[0];
                }
                if ((count($feature_value) > 1)&&($feature_value[1]>0)) {
                    $id_features_value = $id_features_value . ',' . $feature_value[1];
                }
            }
        $id_features = substr($id_features, 1);
        $id_features_value = substr($id_features_value, 1);
        if ((strlen($id_features) > 0) && (strlen($id_features_value) > 0)) {
            $id_features_value_arr = explode(",", $id_features_value);
            $action = true;
            $Q_feature_values = 'AND f.id_feature IN (' . $id_features . ') AND fv.id_feature_value IN (' . $id_features_value . ')';
            $Q_feature_values_with_value_num='';
            if ((strlen($id_features_value) > 0)) {
                foreach ($id_features_value_arr as $feature_valuesArr) {

                   // echo '<div dir="ltr">'.$feature_valuesArr.'</div>';
                    $Prd_min_features_value[$feature_valuesArr] = (Tools::getValue('min_feature_value_' . $feature_valuesArr . ''));
                    $Prd_min_features_value_2[$feature_valuesArr] = (Tools::getValue('min_feature_value_2_' . $feature_valuesArr . ''));

                    $Prd_max_features_value[$feature_valuesArr] = (Tools::getValue('max_feature_value_' . $feature_valuesArr . ''));
                    $Prd_max_features_value_2[$feature_valuesArr] = (Tools::getValue('max_feature_value_2_' . $feature_valuesArr . ''));
                    if( ($Prd_max_features_value_2[$feature_valuesArr] > $Prd_min_features_value_2[$feature_valuesArr] ) && ($Prd_max_features_value[$feature_valuesArr] > $Prd_min_features_value[$feature_valuesArr]) ){

                        $Q_feature_values_with_value_num .= ' AND (fp.id_feature_value=' . $feature_valuesArr . '
                                                             AND
                                                            (fp.value_num_1 BETWEEN ' . $Prd_min_features_value[$feature_valuesArr] . ' AND ' . $Prd_min_features_value_2[$feature_valuesArr] . ')
                                                             AND
                                                            (fp.value_num_2 BETWEEN ' . $Prd_max_features_value[$feature_valuesArr] . ' AND ' . $Prd_max_features_value_2[$feature_valuesArr] . ')

                                                            )';

                    }elseif( ($Prd_min_features_value[$feature_valuesArr]!=0 ) && ($Prd_max_features_value[$feature_valuesArr]!=0 ) && ($Prd_max_features_value[$feature_valuesArr] > $Prd_min_features_value[$feature_valuesArr]) ){

                        $Q_feature_values_with_value_num .= ' AND (fp.id_feature_value=' . $feature_valuesArr . '
                                                            AND fp.value_num_1>=' . $Prd_min_features_value[$feature_valuesArr] . '
                                                            AND fp.value_num_2<=' . $Prd_max_features_value[$feature_valuesArr] . ' )';
                    }
                }
            }
        } elseif (strlen($id_features) > 0) {
            $action = true;
            $Q_feature_values = 'AND f.id_feature IN (' . $id_features . ')';
        } else {
            $Q_feature_values = '';
        }
        $Q_Product_Cost = '';
        if (($Product_Max_Cost > $Product_Min_Cost)) {
            $action = true;
            $Q_Product_Cost = 'AND (pa.price>=' . $Product_Min_Cost . ' AND  pa.price<=' . $Product_Max_Cost . ')';
        }
        if ($Product_Name) {
            $action = true;
            $Q_Product_Name = '(pl.name LIKE "%' . $Product_Name . '%" OR pl.description_short LIKE "%' . $Product_Name . '%" )';
        } else {
            $Q_Product_Name = 'pl.name LIKE "%%"';
        }
        $order_by = 'ORDER BY cl.id_category, p.price ' . $Q_Limit;
        //$ResultProductSearch = Search::getAllProductsRow((int)$this->context->cookie->id_lang,$order_by,$Q_Product_Name,$Q_Product_Cost,$Q_feature_values,$Q_id_categorys,$action);
        $AllFeaturesWithGroup = Search::getAllFeaturesWithGroup((int)$this->context->cookie->id_lang, 0, 0, true, true, false);
        $CountResult = Search::getAllProductsRowCount((int)$this->context->cookie->id_lang, $order_by, $Q_Product_Name, $Q_Product_Cost, $Q_feature_values, $Q_id_categorys, $Q_feature_values_with_value_num,$action);


        $this->context->smarty->assign(array(
            'AllFeatureValues' => Search::getAllFeatureValue((int)$this->context->cookie->id_lang, 0),
            'AllGroups' => Search::getAllGroup((int)$this->context->cookie->id_lang),
            'AllFeatures' => Search::getAllFeatures((int)$this->context->cookie->id_lang),
            'AllFeaturesWithGroup' => Search::getAllFeaturesWithGroup((int)$this->context->cookie->id_lang, 0 /* id_featuer */, 0 /* id_group */, false /*groupBy*/, false /*group by feature*/, false /*group by value*/),
            'AllCategories' => Search::getAllCategory(0, (int)$this->context->cookie->id_lang),
            'CountResult' => $CountResult[0]['Counter'],
            'MinPriceDB' => (int)Search::getValue('product_attribute','MIN(price)'),
            'MaxPriceDB' => (int)Search::getValue('product_attribute','MAX(price)'),
            'MinWeightDB' => (int)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=4')),
            'MaxWeightDB' => (int)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=4')),

            'MinPpiDB' => (int)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=21')),
            'MaxPpiDB' => (int)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=21')),

            'MinSpeedDB' => (int)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=18')),
            'MaxSpeedDB' => (int)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=18')),
            'MinCameraDB' => (float)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=27')),
            'MaxCameraDB' => (float)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=27')),
            'MinScreenDB' => (float)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=22')),
            'MaxScreenDB' => (float)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=22')),
            'MinResDB' => (int)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=10')),
            'MaxResDB' => (int)(Search::getValue('feature_product','MAX(value_num_2)','WHERE id_feature=10')),
            'ResultProductSearch' => Search::getAllProductsRow((int)$this->context->cookie->id_lang, $order_by, $Q_Product_Name, $Q_Product_Cost, $Q_feature_values, $Q_id_categorys, $Q_feature_values_with_value_num, $action)
        ));
        $countv = 0;
        foreach ($AllFeaturesWithGroup as $Feature) {
            $this->context->smarty->assign(array(
                'AllFeaturesWithGroup_' . $Feature['id_group'] . '_' . $Feature['id_feature'] => Search::getAllFeaturesWithGroup((int)$this->context->cookie->id_lang, $Feature['id_feature'], $Group['id_group'], true, true, true)
            ));
            $countv++;
        }
        $this->setTemplate(_PS_THEME_DIR_ . 'searchajax.tpl');
    }



    public function ResultProductSearch()
    {
        $errors = array();
        $Product_Name = (Tools::getValue('NAME_PRODUCT'));
        $Product_Min_Cost = (Tools::getValue('min_cost'));
        $Product_Max_Cost = (Tools::getValue('max_cost'));
        $Product_features_values = (Tools::getValue('feature_value'));
        $Product_id_category = (Tools::getValue('id_category'));
        $available = (Tools::getValue('available'));
        $PageResult = (Tools::getValue('page'));
        $CountRows = 12;

        $id_features = '';
        $id_features_value = '';
        $id_categorys = '';
        //-------------------------------------------------------
        //For Name - STEP 1

        $Sub_Query='p.active=1 AND pl.name LIKE "%'.$Product_Name.'%"';
        $join='INNER JOIN '._DB_PREFIX_.'product_lang pl ON pl.id_product=p.id_product';

        $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

        $id_products='';
        foreach ($ResultIdProductArr as $id_product_arr)
        {
            $id_products = $id_products.','.$id_product_arr['id_product'] ;
        }

        $id_products = substr($id_products,1);


        //-------------------------------------------------------
        //START For Category - STEP 2
        foreach ($Product_id_category as $id_category) {
            if($id_category>0){
                $id_categorys = $id_categorys . ',' . $id_category;
            }
        }
        $id_categorys = substr($id_categorys, 1);

        $Sub_Query='p.active=1 AND cp.id_category IN ('.$id_categorys.') AND p.id_product IN ('.$id_products.')';
        $join='INNER JOIN '._DB_PREFIX_.'category_product cp ON cp.id_product=p.id_product';

        $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

        $id_products='';
        foreach ($ResultIdProductArr as $id_product_arr)
        {
            $id_products = $id_products.','.$id_product_arr['id_product'] ;
        }

        $id_products = substr($id_products,1);
        //END For Category
        //----------------------------------------------------------
        //START For Price - STEP 3
        if(($Product_Max_Cost>=$Product_Min_Cost)
            && (($Product_Min_Cost>0)||($Product_Max_Cost>0))
        ){
            $Sub_Query='p.active=1 AND (pa.price BETWEEN '.$Product_Min_Cost.' AND '.$Product_Max_Cost.')  AND p.id_product IN ('.$id_products.')';
        }else{
            $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.')';
        }
        $join='INNER JOIN '._DB_PREFIX_.'product_attribute pa ON pa.id_product=p.id_product';

        $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

        $id_products='';
        foreach ($ResultIdProductArr as $id_product_arr)
        {
            $id_products = $id_products.','.$id_product_arr['id_product'] ;
        }

        $id_products = substr($id_products,1);


        //END For Price

        //----------------------------------------------------------
        //START For Feature - STEP 4
        //echo $id_products;
        //echo count($Product_features_values);
        if(count($Product_features_values)>0){

            $count_feature=0;
            foreach ($Product_features_values as $feature_values) {

                if($feature_values>0){
                    $feature_value = explode("_", $feature_values);
                    if ((count($feature_value) > 1)&&($feature_value[0]>0)) {
                        $id_features = $id_features . ',' . $feature_value[0];
                    }
                    /*--------------*/
                    if ((count($feature_value) > 1)&&($feature_value[1]>0)) {
                        $id_features_value = $id_features_value . ',' . $feature_value[1];
                    }
                    /*--------------*/
                    $count_feature++;
                }

            }

            if($count_feature>0){

                $id_features = substr($id_features,1);
                $id_features_value = substr($id_features_value, 1);

                //Feature - STEP 4,1
                $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') AND fp.id_feature IN ('.$id_features.')';
                $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                $id_products='';
                foreach ($ResultIdProductArr as $id_product_arr)
                {
                    $id_products = $id_products.','.$id_product_arr['id_product'] ;
                }

                $id_products = substr($id_products,1);

                //Feature Value - STEP 4,2
                $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') AND fp.id_feature_value IN ('.$id_features_value.')';
                $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                $id_products='';
                foreach ($ResultIdProductArr as $id_product_arr)
                {
                    $id_products = $id_products.','.$id_product_arr['id_product'] ;
                }

                $id_products = substr($id_products,1);
                //------------------------------------------------
                if ((strlen($id_features) > 0) && (strlen($id_features_value) > 0)) {
                    $id_features_value_arr = explode(",", $id_features_value);
                    $action = true;

                    //$Q_feature_values_with_value_num='';
                    if ((strlen($id_features_value) > 0)) {
                        foreach ($id_features_value_arr as $feature_valuesArr) {
                            if($feature_valuesArr>0){

                                $Prd_min_features_value[$feature_valuesArr] = (Tools::getValue('min_feature_value_' . $feature_valuesArr . '') ? Tools::getValue('min_feature_value_' . $feature_valuesArr . '') : -1 );
                                $Prd_min_features_value_2[$feature_valuesArr] = (Tools::getValue('min_feature_value_2_' . $feature_valuesArr . '') ? Tools::getValue('min_feature_value_2_' . $feature_valuesArr . '') : -1);

                                $Prd_max_features_value[$feature_valuesArr] = (Tools::getValue('max_feature_value_' . $feature_valuesArr . '') ? Tools::getValue('max_feature_value_' . $feature_valuesArr . '') : -1 );
                                $Prd_max_features_value_2[$feature_valuesArr] = (Tools::getValue('max_feature_value_2_' . $feature_valuesArr . '') ? Tools::getValue('max_feature_value_2_' . $feature_valuesArr . '') : -1 );
                                if( ($Prd_max_features_value_2[$feature_valuesArr] >= $Prd_min_features_value[$feature_valuesArr] )
                                    && ($Prd_max_features_value[$feature_valuesArr] >= $Prd_min_features_value[$feature_valuesArr])
                                    && ($Prd_max_features_value[$feature_valuesArr]>0)&&($Prd_min_features_value[$feature_valuesArr]>0)
                                    && ($Prd_max_features_value_2[$feature_valuesArr]>0)&&($Prd_min_features_value_2[$feature_valuesArr]>0)
                                    && (strlen($id_products)>0) ){

                                   $Q_feature_values_with_value_num = ' AND (fp.id_feature_value=' . $feature_valuesArr . '
                                                             AND
                                                            (fp.value_num_1 BETWEEN ' . $Prd_min_features_value[$feature_valuesArr] . ' AND ' . $Prd_max_features_value[$feature_valuesArr] . ')
                                                             AND
                                                            (fp.value_num_2 BETWEEN ' . $Prd_min_features_value_2[$feature_valuesArr] . ' AND ' . $Prd_max_features_value_2[$feature_valuesArr] . ')

                                                            )';

                                    $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') '.$Q_feature_values_with_value_num;
                                    $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                                    $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                                    $id_products='';
                                    foreach ($ResultIdProductArr as $id_product_arr)
                                    {
                                        $id_products = $id_products.','.$id_product_arr['id_product'] ;
                                    }

                                    $id_products = substr($id_products,1);

                                }elseif( ($Prd_min_features_value[$feature_valuesArr]>0 ) || ($Prd_max_features_value[$feature_valuesArr]>0 )
                                    && ($Prd_max_features_value[$feature_valuesArr] >= $Prd_min_features_value[$feature_valuesArr])
                                    && (strlen($id_products)>0) ){
                                    $Q_feature_values_with_value_num = ' AND (fp.id_feature_value=' . $feature_valuesArr . '
                                                            AND fp.value_num_1>=' . $Prd_min_features_value[$feature_valuesArr] . '
                                                            AND fp.value_num_1<=' . $Prd_max_features_value[$feature_valuesArr] . ' )';


                                    $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') '.$Q_feature_values_with_value_num;
                                    $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                                    $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                                    $id_products='';
                                    foreach ($ResultIdProductArr as $id_product_arr)
                                    {
                                        $id_products = $id_products.','.$id_product_arr['id_product'] ;
                                    }

                                    $id_products = substr($id_products,1);

                                }else{

                                    $Q_feature_values_with_value_num = ' AND fp.id_feature_value=' . $feature_valuesArr . ' ';


                                    $Sub_Query='p.active=1 AND p.id_product IN ('.$id_products.') '.$Q_feature_values_with_value_num;
                                    $join='INNER JOIN '._DB_PREFIX_.'feature_product fp ON fp.id_product=p.id_product';

                                    $ResultIdProductArr = Search::getValues('product p','DISTINCT p.id_product',$Sub_Query,true,$join);

                                    $id_products='';
                                    foreach ($ResultIdProductArr as $id_product_arr)
                                    {
                                        $id_products = $id_products.','.$id_product_arr['id_product'] ;
                                    }

                                    $id_products = substr($id_products,1);

                                }


                            }


                        }//end foreach
                    }
                }



            }

        }//end if count feature

        //END For Feature




        if ((strlen($id_products) > 0)) {
            $Query='p.id_product IN ('.$id_products.')';
        }else{
            $Query='p.id_product IN (0)';
        }

       /* if($available){
            $Query.='';
        }else{

        }*/

        $action=true;
        $productCount = Search::getAllProductsRowCountWithId((int)$this->context->cookie->id_lang, $Query,$action);

        if($productCount>0)
        {
            $TotalPages = (int)($productCount/$CountRows)+1;

            if (($PageResult)&&($productCount>$CountRows)&&($PageResult<=$TotalPages)) {
                $PageRows = ($PageResult * $CountRows) - $CountRows;
                $Q_Limit = 'LIMIT ' . $PageRows . ' , ' . $CountRows . '';
            } else {
                $Q_Limit = 'LIMIT 0 , ' . $CountRows . '';
            }

            $order_by = 'ORDER BY p.id_category_default ' . $Q_Limit;

            $products = Search::getAllProductsRowWithId((int)$this->context->cookie->id_lang, $order_by, $Query ,$action);
            $this->context->smarty->assign('products', $products);


            $resultshtml = $this->context->smarty->fetch(_PS_THEME_DIR_ . 'product-list.tpl');
        }
        else
        {
            $errors[] = 'Not Result \n';
        }



        die(Tools::jsonEncode(array(
            'resultshtml' => $resultshtml,
            'countresult' => $productCount,
            'MinPriceDB' => (int)Search::getValue('product_attribute','MIN(price)','WHERE id_product IN ('.$id_products.') '),
            'MaxPriceDB' => (int)Search::getValue('product_attribute','MAX(price)','WHERE id_product IN ('.$id_products.') '),
            'MinWeightDB' => (int)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=4 AND id_product IN ('.$id_products.') ')),
            'MaxWeightDB' => (int)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=4 AND id_product IN ('.$id_products.') ')),
            'MinPpiDB' => (int)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=21 AND id_product IN ('.$id_products.')')),
            'MaxPpiDB' => (int)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=21 AND id_product IN ('.$id_products.')')),
            'MinSpeedDB' => (int)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=18 AND id_product IN ('.$id_products.') ')),
            'MaxSpeedDB' => (int)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=18 AND id_product IN ('.$id_products.') ')),
            'MinCameraDB' => (float)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=27 AND id_product IN ('.$id_products.') ')),
            'MaxCameraDB' => (float)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=27 AND id_product IN ('.$id_products.') ')),
            'MinScreenDB' => (float)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=22 AND id_product IN ('.$id_products.') ')),
            'MaxScreenDB' => (float)(Search::getValue('feature_product','MAX(value_num_1)','WHERE id_feature=22 AND id_product IN ('.$id_products.') ')),
            'MinResDB' => (int)(Search::getValue('feature_product','MIN(value_num_1)','WHERE id_feature=10 AND id_product IN ('.$id_products.') ')),
            'MaxResDB' => (int)(Search::getValue('feature_product','MAX(value_num_2)','WHERE id_feature=10 AND id_product IN ('.$id_products.') ')),
             'errors' => $errors
        )));
    }
}
