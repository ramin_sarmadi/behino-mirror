<?php
/*
* 2013
* iBartar - Khorshidi. Ahmad
* Load Product With Ajax
*/
class ProductAjaxControllerCore extends FrontController
{
    protected $product;

    private $id_product;
    public function setMedia()
    {
        parent::setMedia();
    }
    public function init()
    {
        parent::init();
        if (($id_product = (int)Tools::getValue('id_product'))&&(Tools::getValue('action')=='quickview')){

            $resultsProduct = Product::getProductRow($this->context->language->id,$id_product,true);
            $resultsFeature = Product::getFeatureProductRows($this->context->language->id,$id_product,true);


            if(count($resultsProduct)==0){
                $p_errors = 'Not Find!';
            }

            if(count($resultsFeature)==0){
                $f_errors = 'Not Find!';
            }

            foreach ($resultsProduct as &$Product){
                $Product['link_lmg'] = $this->context->link->getImageLink($Product['link_rewrite'], $Product['id_image'], 'large_allinmart');
            }
                die(Tools::jsonEncode(array(
                'resultsProduct' => $resultsProduct,
                'resultsFeature' => $resultsFeature,
                'errorsProduct' => $p_errors,
                'errorsFeature' => $f_errors
            )));
        }




    }


}
