<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CompareControllerCore extends FrontController
{
    public $php_self = 'products-comparison';

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_THEME_CSS_DIR_ . 'comparator.css');

        if (Configuration::get('PS_COMPARATOR_MAX_ITEM') > 0) {
            //$this->addJS(_PS_JS_DIR_.'jquery/ui/jquery.ui.core.min.js');
            //$this->addJS(_PS_JS_DIR_.'jquery/ui/jquery.ui.widget.min.js');
            /* $this->addJS(_PS_JS_DIR_.'jquery/ui/jquery-ui.min.js');*/
            $this->addJS(_THEME_JS_DIR_ . 'products-comparison.js');
            $this->addJS(_THEME_JS_DIR_ . 'searchproducts.js');

        }
    }

    /**
     * Display ajax content (this function is called instead of classic display, in ajax mode)
     */
    public function displayAjax()
    {


        if (Tools::getValue('action') == 'delallcookie') {

            $id_customer = $this->context->customer->id;

            if (!$id_customer) {
                $id_customer = 'guest_' . $this->context->cookie->id_guest;
            }

            if ($id_customer) {
                $ids_product = CompareProduct::getIdProducts('WHERE p.active=1');

                foreach ($ids_product as $productId) {
                    $prd0[$productId['id_product']] = 'cookie_id_product_' . $id_customer . '_' . $productId['id_product'];
                    $prd[$productId['id_product']] = $this->context->cookie->$prd0[$productId['id_product']];

                    if (($prd[$productId['id_product']])) {
                        $prd0[$productId['id_product']];
                        $this->context->cookie->__unset($prd0[$productId['id_product']]);

                    }

                }

            }
        }

        if (Tools::getValue('ajax') && Tools::getValue('id_product') && Tools::getValue('action')) {

            if (Tools::getValue('action') == 'delcookie') {

                $id_customer = $this->context->customer->id;
                if ($id_customer) {
                    $this->context->cookie->__unset('cookie_id_product_' . $id_customer . '_' . Tools::getValue('id_product'));
                }


            } elseif (Tools::getValue('action') == 'addProduct') {
                $product_name_arr = explode("|", Tools::getValue('id_product'));

                $id_product = Db::getInstance()->getValue('
                SELECT `id_product`
                FROM `' . _DB_PREFIX_ . 'product_lang`
                WHERE `name` = "' . $product_name_arr[0] . '"');

                $id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare : false;
                if (CompareProduct::getNumberProducts($id_compare) < Configuration::get('PS_COMPARATOR_MAX_ITEM'))
                    CompareProduct::addCompareProduct($id_compare, (int)$id_product);
                else
                    die('0');
            } elseif (Tools::getValue('action') == 'add') {
                $id_product = Tools::getValue('id_product');

                $id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare : false;


                if (CompareProduct::getNumberProducts($id_compare) > 0) {
                    $ids = CompareProduct::getCompareProducts($id_compare);
                    $count_ids = count($ids);
                    if ($count_ids > 0) {

                        $id_product_last = $ids[$count_ids - 1];
                        $id_parentCat_last_product = CompareProduct::getIdCategoryParentProducts((int)$id_product_last);
                        $id_parentCat = CompareProduct::getIdCategoryParentProducts((int)$id_product);
                    } else {
                        $id_parentCat = 0;
                        $id_parentCat_last_product = 0;
                    }


                } else {
                    $id_parentCat = 0;
                    $id_parentCat_last_product = 0;
                }


                if (CompareProduct::getNumberProducts($id_compare) < Configuration::get('PS_COMPARATOR_MAX_ITEM')) {

                    if (($id_parentCat_last_product == $id_parentCat)) {

                        CompareProduct::addCompareProduct($id_compare, (int)$id_product);

                    } else {

                        die('102');

                    }

                } else {
                    die('0');
                }
            } elseif (Tools::getValue('action') == 'remove') {
                if (isset($this->context->cookie->id_compare))
                    CompareProduct::removeCompareProduct((int)$this->context->cookie->id_compare, (int)Tools::getValue('id_product'));
                else
                    die('0');
            } else {
                die('0');
            }

            die('1');
        }

        //this form method

        if (Tools::getValue('form') && Tools::getValue('id_product') && Tools::getValue('action')) {

            $html='';
            if (Tools::getValue('action') == 'delcookie') {

                $id_customer = $this->context->customer->id;
                if ($id_customer) {
                    $this->context->cookie->__unset('cookie_id_product_' . $id_customer . '_' . Tools::getValue('id_product'));
                }

                return 1;

            } elseif (Tools::getValue('action') == 'addProduct') {
                $product_name_arr = explode("|", Tools::getValue('id_product'));

                $id_product = Db::getInstance()->getValue('
                SELECT `id_product`
                FROM `' . _DB_PREFIX_ . 'product_lang`
                WHERE `name` = "' . $product_name_arr[0] . '"');

                $id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare : false;
                if (CompareProduct::getNumberProducts($id_compare) < Configuration::get('PS_COMPARATOR_MAX_ITEM')){

                    CompareProduct::addCompareProduct($id_compare, (int)$id_product);
                }else{
                    $html .= '<div class="error">'.($this->l('Error, Maximum 5 products are available for compare')).'</div>';
                }
                    //die('0');
                return 1;

            } elseif (Tools::getValue('action') == 'add') {
                $id_product = Tools::getValue('id_product');

                $id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare : false;


                if (CompareProduct::getNumberProducts($id_compare) > 0) {
                    $ids = CompareProduct::getCompareProducts($id_compare);
                    $count_ids = count($ids);
                    if ($count_ids > 0) {

                        $id_product_last = $ids[$count_ids - 1];
                        $id_parentCat_last_product = CompareProduct::getIdCategoryParentProducts((int)$id_product_last);
                        $id_parentCat = CompareProduct::getIdCategoryParentProducts((int)$id_product);
                    } else {
                        $id_parentCat = 0;
                        $id_parentCat_last_product = 0;
                    }


                } else {
                    $id_parentCat = 0;
                    $id_parentCat_last_product = 0;
                }


                if (CompareProduct::getNumberProducts($id_compare) < Configuration::get('PS_COMPARATOR_MAX_ITEM')) {

                    if (($id_parentCat_last_product == $id_parentCat)) {

                        CompareProduct::addCompareProduct($id_compare, (int)$id_product);

                    }else{
                        $html .= '<div class="error">'.($this->l('Error, are not equal parent product')).'</div>';

                        //die('102');

                    }

                } else {
                    $html .= '<div class="error">'.($this->l('Error, Maximum 5 products are available for compare')).'</div>';

                    //die('0');
                }
                return 1;

            } elseif (Tools::getValue('action') == 'remove') {

                if (isset($this->context->cookie->id_compare)){
                    CompareProduct::removeCompareProduct((int)$this->context->cookie->id_compare, (int)Tools::getValue('id_product'));

                }else{
                    $html .= '<div class="error">'.($this->l('Error, can not delete this record.')).'</div>';

                    //die('0');
                }

                return 1;

            }else{
                $html .= '<div class="error">'.($this->l('Error, you have not action.')).'</div>';

                //die('0');
            }

            $html .= '<div class="conf confirm">'.($this->l('completed action.')).'</div>';


            //die('1');
        }else{

        }

        $this->context->smarty->assign(array(
            'html' => $html
        ));
        //$this->setTemplate(_PS_THEME_DIR_ . 'products-comparison.tpl');

    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();



        //Clean compare product table
        CompareProduct::cleanCompareProducts('week');

        $this->displayAjax();

        $hasProduct = false;

        if (!Configuration::get('PS_COMPARATOR_MAX_ITEM'))
            return Tools::redirect('index.php?controller=404');

        if (($product_list = Tools::getValue('compare_product_list')) && ($postProducts = (isset($product_list) ? rtrim($product_list, '|') : '')))
            $ids = array_unique(explode('|', $postProducts));
        else if (isset($this->context->cookie->id_compare))
            $ids = CompareProduct::getCompareProducts($this->context->cookie->id_compare);
        else
            $ids = null;

        if ($ids) {
            if (count($ids) > 0) {
                if (count($ids) > Configuration::get('PS_COMPARATOR_MAX_ITEM'))
                    $ids = array_slice($ids, 0, Configuration::get('PS_COMPARATOR_MAX_ITEM'));

                $listProducts = array();
                $listFeatures = array();

                foreach ($ids as $k => &$id) {
                    $curProduct = new Product((int)$id, true, $this->context->language->id);
                    if (!Validate::isLoadedObject($curProduct) || !$curProduct->active || !$curProduct->isAssociatedToShop()) {
                        if (isset($this->context->cookie->id_compare))
                            CompareProduct::removeCompareProduct($this->context->cookie->id_compare, $id);
                        unset($ids[$k]);
                        continue;
                    }

                    $resultFeature=$curProduct->getFrontFeatures($this->context->language->id);
                    //print_r($r);
                    foreach ($resultFeature as $feature){
                        $listFeatures[$curProduct->id][$feature['id_feature']] = $feature['valueT'];
            /*            $listFeatures[$curProduct->id][$feature['id_feature']]['value_num_1'] = $feature['value_num_1'];
                        $listFeatures[$curProduct->id][$feature['id_feature']]['value_num_2'] = $feature['value_num_2'];
                        $listFeatures[$curProduct->id][$feature['id_feature']]['id_feature_value'] = $feature['id_feature_value'];*/
                    }

                    //print_r($listFeatures);

                    $cover = Product::getCover((int)$id);

                    $curProduct->id_image = Tools::htmlentitiesUTF8(Product::defineProductImage(array('id_image' => $cover['id_image'], 'id_product' => $id), $this->context->language->id));
                    $curProduct->allow_oosp = Product::isAvailableWhenOutOfStock($curProduct->out_of_stock);
                    $curProduct->TopFeature = Product::getFeatureProductRows($this->context->language->id, $id, true);

                    $listProducts[] = $curProduct;
                }

                if (count($listProducts) > 0) {
                    $width = 80 / count($listProducts);

                    $hasProduct = true;
                    $ordered_features = Feature::getFeaturesForComparison($ids, $this->context->language->id);
                    $this->context->smarty->assign(array(
                        'ordered_features' => $ordered_features,
                        'product_features' => $listFeatures,
                        'products' => $listProducts,
                        'width' => $width,
                        'homeSize' => Image::getSize(ImageType::getFormatedName('home'))
                    ));
                    $this->context->smarty->assign('HOOK_EXTRA_PRODUCT_COMPARISON', Hook::exec('displayProductComparison', array('list_ids_product' => $ids)));
                } else if (isset($this->context->cookie->id_compare)) {
                    $object = new CompareProduct((int)$this->context->cookie->id_compare);
                    if (Validate::isLoadedObject($object))
                        $object->delete();
                }
            }
        }
        $this->context->smarty->assign('hasProduct', $hasProduct);

        //print_r($Cookies = $this->context->cookie);

        $id_customer = $this->context->customer->id;

        if (!$id_customer) {
            $id_customer = 'guest_' . $this->context->cookie->id_guest;
        }

        if ($id_customer) {
            $ids_product = CompareProduct::getIdProducts('WHERE p.active=1');
            $product_compare_cookie = '';
            //print_r($ids_product);
            foreach ($ids_product as $productId) {
                $prd0[$productId['id_product']] = 'cookie_id_product_' . $id_customer . '_' . $productId['id_product'];
                $prd[$productId['id_product']] = $this->context->cookie->$prd0[$productId['id_product']];
                //echo ($prd[$productId['id_product']]);

                if (($prd[$productId['id_product']])) {

                    $product_compare_cookie = $product_compare_cookie . ',' . $prd[$productId['id_product']];

                }

            }

            if ($ids) {
                $ids = $ids;
            } else {
                $ids = array();
            }
            $product_compare_cookie = substr($product_compare_cookie, 1);
            $product_compare_cookie_arr = explode(",", $product_compare_cookie);
            $product_id_diff_arr = array_diff($product_compare_cookie_arr, $ids);
            //print_r($ids);
            $ids_product_diff = '';
            foreach ($product_id_diff_arr as $product_id_diff) {
                $ids_product_diff = $ids_product_diff . ',' . $product_id_diff;
            }
            $ids_product_diff = substr($ids_product_diff, 1);
            $id_lang = (int)$this->context->cookie->id_lang;
            $products_compare_in_cookie = CompareProduct::getProductRow($ids_product_diff, $id_lang);

            $id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare : false;

            foreach ($products_compare_in_cookie as &$p_c_c) {

                if (CompareProduct::getNumberProducts($id_compare) > 0) {
                    $ids = CompareProduct::getCompareProducts($id_compare);
                    $count_ids = count($ids);
                    $id_product_last = $ids[$count_ids - 1];
                    $id_parentCat_last_product = CompareProduct::getIdCategoryParentProducts((int)$id_product_last);
                    $id_parentCat = CompareProduct::getIdCategoryParentProducts((int)$p_c_c['id_product']);


                } else {
                    $id_parentCat = 0;
                    $id_parentCat_last_product = 0;
                }

                if ($id_parentCat == $id_parentCat_last_product) {
                    $p_c_c['isValid'] = 1;
                } else {
                    $p_c_c['isValid'] = 0;
                }
            }


        }

        $this->context->smarty->assign(array(
            'COOKIE' => $this->context->cookie,
            'id_customer' => $id_customer,
            'id_compare' => $this->context->cookie->id_compare,
            'id_products' => $ids,
            'products_compare_in_cookie' => $products_compare_in_cookie

        ));

        $this->setTemplate(_PS_THEME_DIR_ . 'products-comparison.tpl');
    }

}