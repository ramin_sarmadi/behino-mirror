<?php

class VideoControllerCore extends FrontController
{

    public function init()
    {
        parent::init();

/*
        if (!Validate::isLoadedObject($this->product))
        {
            header('HTTP/1.1 404 Not Found');
            header('Status: 404 Not Found');
            $this->errors[] = Tools::displayError('Product not found');
        }
        else
        {
            $this->canonicalRedirection();

        }

*/
    }


	public function initContent()
	{

        parent::initContent();


        $id_products=(Tools::getValue('id_product'));
        $ids_product_video=(Tools::getValue('id_video'));

        $productVideo = new ProductVideo();

        if(($id_products)&&($ids_product_video)){
            $result_prd = $productVideo->getVideo($id_products,$ids_product_video, 1);
        }
        $result_related = $productVideo->getRelatedVideo($id_products,$ids_product_video, 1);
        $result_related_prd = $productVideo->CreateProductVideoResult($result_related);

        $result_AllVideoProduct = $productVideo->getVideo(0,0, 1,20);
        $result_AllVideoProductLink = $productVideo->CreateProductVideoResult($result_AllVideoProduct);


        //print_r($result_AllVideoProduct);


        $this->context->smarty->assign(array(
            'ProductVideoLocation' => 'upload/video/',
            'ProductVideoImageLocation' => 'upload/video/thumbnails/',
            'ResultProductVideo' => $result_prd,
            'ResultAllProductVideo' => $result_AllVideoProductLink,
            'id_customer' => (int)$this->context->cookie->id_customer,
            'ResultrelatedProductVideo' => $result_related_prd

         ));






        $this->setTemplate(_PS_THEME_DIR_.'video.tpl');


	}


}
