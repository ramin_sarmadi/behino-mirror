<?php

include_once(_PS_MODULE_DIR_.'/hotcompare/hotcompare.php');

class HotCompareControllerCore extends FrontController
{
    public $php_self = 'HotCompare';
    public function init()
    {
        parent::init();
        $this->canonicalRedirection();
    }
    public function initContent()
    {
        parent::initContent();
        $id_hotcompare = (Tools::getValue('id_hotcompare'));
        $this->addCSS(_THEME_CSS_DIR_.'comparator.css');

        $CompareProduct = new HotCompare();
        $result_hotcompare = $CompareProduct->getListContent($this->context->language->id,$id_hotcompare);
        $result_details = $CompareProduct->getListContentDetail($this->context->language->id,$id_hotcompare);
        //print_r($result_details);
        $id_products='';
        foreach ($result_details as $row_id_product) {

            $id_products .= ','.$row_id_product['id_product'];

        }
        $id_products=substr($id_products,1);
        $ids=explode(",",$id_products);

        $hasProduct = false;

        if ($ids)
        {
            if (count($ids) > 0)
            {
                if (count($ids) > Configuration::get('PS_COMPARATOR_MAX_ITEM'))
                    $ids = array_slice($ids, 0, Configuration::get('PS_COMPARATOR_MAX_ITEM'));

                $listProducts = array();
                $listFeatures = array();

                foreach ($ids as $k => &$id)
                {
                    $curProduct = new Product((int)$id, true, $this->context->language->id);
                    if (!Validate::isLoadedObject($curProduct) || !$curProduct->active || !$curProduct->isAssociatedToShop())
                    {
                        if (isset($this->context->cookie->id_compare))
                            //CompareProduct::removeCompareProduct($this->context->cookie->id_compare, $id);
                        unset($ids[$k]);
                        continue;
                    }

                    $resultFeature=$curProduct->getFrontFeatures($this->context->language->id);
                    //print_r($r);
                    foreach ($resultFeature as $feature){
                        $listFeatures[$curProduct->id][$feature['id_feature']] = $feature['valueT'];
                        /*            $listFeatures[$curProduct->id][$feature['id_feature']]['value_num_1'] = $feature['value_num_1'];
                                    $listFeatures[$curProduct->id][$feature['id_feature']]['value_num_2'] = $feature['value_num_2'];
                                    $listFeatures[$curProduct->id][$feature['id_feature']]['id_feature_value'] = $feature['id_feature_value'];*/
                    }

                    $cover = Product::getCover((int)$id);

                    $curProduct->id_image = Tools::htmlentitiesUTF8(Product::defineProductImage(array('id_image' => $cover['id_image'], 'id_product' => $id), $this->context->language->id));
                    $curProduct->allow_oosp = Product::isAvailableWhenOutOfStock($curProduct->out_of_stock);
                    $curProduct->TopFeature =  Product::getFeatureProductRows($this->context->language->id,$id,true);

                    $listProducts[] = $curProduct;
                }

                if (count($listProducts) > 0)
                {
                    $width = 80 / count($listProducts);

                    $hasProduct = true;
                    $ordered_features = Feature::getFeaturesForComparison($ids, $this->context->language->id);
                    $this->context->smarty->assign(array(
                        'result_hotcompare' => $result_hotcompare,
                        'ordered_features' => $ordered_features,
                        'product_features' => $listFeatures,
                        'products' => $listProducts,
                        'width' => $width,
                        'homeSize' => Image::getSize(ImageType::getFormatedName('home'))
                    ));
                    $this->context->smarty->assign('HOOK_EXTRA_PRODUCT_COMPARISON', Hook::exec('displayProductComparison', array('list_ids_product' => $ids)));
                }
                else if (isset($this->context->cookie->id_compare))
                {
                    $object = new CompareProduct((int)$this->context->cookie->id_compare);
                    if (Validate::isLoadedObject($object))
                        $object->delete();
                }
            }
        }
        $this->context->smarty->assign('hasProduct', $hasProduct);




        $this->setTemplate(_PS_THEME_DIR_ . 'HotCompare.tpl');
    }



}
