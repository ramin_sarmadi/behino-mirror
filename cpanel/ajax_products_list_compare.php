<?php

define('_PS_ADMIN_DIR_', getcwd());
include(_PS_ADMIN_DIR_.'/../config/config.inc.php');
/* Getting cookie or logout */
require_once(_PS_ADMIN_DIR_.'/init.php');

$query = Tools::getValue('q', false);
if (!$query OR $query == '' OR strlen($query) < 1)
    die();

if($pos = strpos($query, ' (ref:'))
    $query = substr($query, 0, $pos);

$excludeIds = Tools::getValue('excludeIds', false);
if ($excludeIds && $excludeIds != 'NaN')
    $excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
else
    $excludeIds = '';

// Excluding downloadable products from packs because download from pack is not supported
$excludeVirtuals = (bool)Tools::getValue('excludeVirtuals', false);
$exclude_packs = (bool)Tools::getValue('exclude_packs', false);
$id_lang=(int)Context::getContext()->language->id;

/*$sql = 'SELECT p.`id_product`, `reference`, pl.name, MAX(image_shop.`id_image`) id_image, pl.link_rewrite
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)Context::getContext()->language->id.Shop::addSqlRestrictionOnLang('pl').')
        LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
		WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.
    (!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
    ($excludeVirtuals ? 'AND p.id_product NOT IN (SELECT pd.id_product FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))' : '').
    ($exclude_packs ? 'AND (p.cache_is_pack IS NULL OR p.cache_is_pack = 0)' : '');*/

$sql = 'SELECT p.`id_product`, `reference`, pl.name, pl.link_rewrite
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)Context::getContext()->language->id.Shop::addSqlRestrictionOnLang('pl').')
		WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.
    (!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
    ($excludeVirtuals ? 'AND p.id_product NOT IN (SELECT pd.id_product FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))' : '').
    ($exclude_packs ? 'AND (p.cache_is_pack IS NULL OR p.cache_is_pack = 0)' : '');

$items = Db::getInstance()->executeS($sql);

$result ='';
if ($items)
{
    foreach ($items AS $item)
    {
        /*
        $IdImageArr=str_split($item['id_image']);
        $CountIdImageArr=count($IdImageArr);
        for($i=0;$i<$CountIdImageArr;$i++)
        {
            $UrlIdImage = $UrlIdImage.'/'.$IdImageArr[$i];
        }
        $UrlIdImage = substr($UrlIdImage,1);
        $item['src'] = '<img src="'._PS_PROD_IMG_DIR_.$UrlIdImage.'/'.$item['id_product'].'-medium_leocame.jpg" border=0 />';
        */
        //$item['src'] = link::getImageLink($item['link_rewrite'], (int)($item['id_product']).'-'.$item['id_image'], 'small_default');
        $result .= trim($item['name']).(!empty($item['reference']) ? ' (ref: '.$item['reference'].')' : '').'|'.(int)($item['id_product'])."\n";
    }

    echo($result);

}
