<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
abstract class ControllerCore
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var array list of css files
     */
    public $css_files = array();

    /**
     * @var array list of javascript files
     */
    public $js_files = array();

    /**
     * @var bool check if header will be displayed
     */
    protected $display_header;

    /**
     * @var string template name for page content
     */
    protected $template;

    /**
     * @var string check if footer will be displayed
     */
    protected $display_footer;

    /**
     * @var string check if only content will be displayed
     */
    protected $content_only = false;

    /**
     * @var bool If ajax parameter is detected in request, set this flag to true
     */
    public $ajax = false;
    protected $json = false;
    protected $status = '';

    protected $redirect_after = null;

    public $controller_type;

    /**
     * check that the controller is available for the current user/visitor
     */
    abstract public function checkAccess();

    /**
     * check that the current user/visitor has valid view permissions
     */
    abstract public function viewAccess();

    /**
     * Initialize the page
     */
    public function init()
    {
        if (!defined('_PS_BASE_URL_'))
            define('_PS_BASE_URL_', Tools::getShopDomain(true));
        if (!defined('_PS_BASE_URL_SSL_'))
            define('_PS_BASE_URL_SSL_', Tools::getShopDomainSsl(true));
    }

    /**
     * Do the page treatment : post process, ajax process, etc.
     */
    abstract public function postProcess();

    /**
     * Display page view
     */
    abstract public function display();

    /**
     * Redirect after process if no error
     */
    abstract protected function redirect();

    /**
     * Set default media list for controller
     */
    abstract public function setMedia();

    /**
     * Get an instance of a controller
     *
     * @param string $class_name
     * @param bool $auth
     * @param bool $ssl
     */
    public static function getController($class_name, $auth = false, $ssl = false)
    {
        return new $class_name($auth, $ssl);
    }

    public function __construct()
    {
        if (is_null($this->display_header))
            $this->display_header = true;

        if (is_null($this->display_footer))
            $this->display_footer = true;

        $this->context = Context::getContext();
        $this->context->controller = $this;
        // Usage of ajax parameter is deprecated
        $this->ajax = Tools::getValue('ajax') || Tools::isSubmit('ajax');


        //This for Logger
        $SQL = '
			SELECT COUNT(id_site_log)
			FROM ' . _DB_PREFIX_ . 'site_log
	    ';

        $id_customer = $this->context->customer->id;
        $id_employee = $this->context->employee->id;
        $email = $this->context->employee->email;
        $id_shop = $this->context->shop->id;

        $COUNT_LOGGER_SITE = Db::getInstance()->getValue($SQL);

        if (($COUNT_LOGGER_SITE == 0)) {
            $query = '
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'site_log` (
              `id_site_log` bigint(16) NOT NULL AUTO_INCREMENT,
              `id_customer` int(10) DEFAULT "0",
              `id_guest` int(10) DEFAULT "0",
              `id_employee` int(10) DEFAULT "0",
              `email` varchar(255) DEFAULT NULL,
              `id_shop` int(6) DEFAULT "1",
              `controller` varchar(255) DEFAULT NULL,
              `url_ref` text,
              `url_scr` text,
              `user_agent` text,
              `date_add` datetime DEFAULT "0000-00-00 00:00:00",
              `ip_address` varchar(50) DEFAULT NULL,
              KEY `id_site_log` (`id_site_log`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ';

            $result = Db::getInstance()->Execute($query);
        }


        if (!$this->context->customer->id) {
            $id_guest = $this->context->cookie->id_guest;
        } else {
            $id_guest = 0;
        }

        if ($id_customer) {
            $id_customer = $id_customer;
        } else {
            $id_customer = 0;
        }

        if ($id_employee) {
            $id_employee = $id_employee;
        } else {
            $id_employee = 0;
            $email = "No Employee";
        }

        $controller = Tools::getValue('controller');
        $url_ref = $_SERVER['HTTP_REFERER'];
        $url_scr = $_SERVER['REQUEST_URI'];
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $date = date("Y-m-d H:i:s");
        $ip_address = $_SERVER['SERVER_ADDR'];

        $dataArr = array(
            'id_customer' => $id_customer,
            'id_guest' => $id_guest,
            'id_employee' => $id_employee,
            'email' => $email,
            'id_shop' => $id_shop,
            'controller' => $controller,
            'url_ref' => $url_ref,
            'url_scr' => $url_scr,
            'user_agent' => $user_agent,
            'date_add' => $date,
            'ip_address' => $ip_address
        );

        $table = 'site_log';

        Db::getInstance()->insert($table, $dataArr);


        //This for Logger


    }

    /**
     * Start controller process (this method shouldn't be overriden !)
     */
    public function run()
    {
        $this->init();
        if ($this->checkAccess()) {
            // setMedia MUST be called before postProcess
            if (!$this->content_only && ($this->display_header || (isset($this->className) && $this->className)))
                $this->setMedia();

            // postProcess handles ajaxProcess
            $this->postProcess();

            if (!empty($this->redirect_after))
                $this->redirect();

            if (!$this->content_only && ($this->display_header || (isset($this->className) && $this->className)))
                $this->initHeader();

            if ($this->viewAccess())
                $this->initContent();
            else
                $this->errors[] = Tools::displayError('Access denied.');

            if (!$this->content_only && ($this->display_footer || (isset($this->className) && $this->className)))
                $this->initFooter();

            // default behavior for ajax process is to use $_POST[action] or $_GET[action]
            // then using displayAjax[action]
            if ($this->ajax) {
                $action = Tools::getValue('action');
                if (!empty($action) && method_exists($this, 'displayAjax' . Tools::toCamelCase($action, true)))
                    $this->{'displayAjax' . $action}();
                elseif (method_exists($this, 'displayAjax'))
                    $this->displayAjax();
            } else
                $this->display();
        } else {
            $this->initCursedPage();
            $this->smartyOutputContent($this->layout);
        }
    }

    public function displayHeader($display = true)
    {
        $this->display_header = $display;
    }

    public function displayFooter($display = true)
    {
        $this->display_footer = $display;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * Assign smarty variables for the page header
     */
    abstract public function initHeader();

    /**
     * Assign smarty variables for the page main content
     */
    abstract public function initContent();

    /**
     * Assign smarty variables when access is forbidden
     */
    abstract public function initCursedPage();

    /**
     * Assign smarty variables for the page footer
     */
    abstract public function initFooter();

    /**
     * Add a new stylesheet in page header.
     *
     * @param mixed $css_uri Path to css file, or list of css files like this : array(array(uri => media_type), ...)
     * @param string $css_media_type
     * @return true
     */
    public function addCSS($css_uri, $css_media_type = 'all')
    {
        if (is_array($css_uri))
            foreach ($css_uri as $css_file => $media) {
                if (is_string($css_file) && strlen($css_file) > 1) {
                    $css_path = Media::getCSSPath($css_file, $media);
                    if ($css_path && !in_array($css_path, $this->css_files))
                        $this->css_files = array_merge($this->css_files, $css_path);
                } else {
                    $css_path = Media::getCSSPath($media, $css_media_type);
                    if ($css_path && !in_array($css_path, $this->css_files))
                        $this->css_files = array_merge($this->css_files, $css_path);
                }
            }
        else if (is_string($css_uri) && strlen($css_uri) > 1) {
            $css_path = Media::getCSSPath($css_uri, $css_media_type);
            if ($css_path)
                $this->css_files = array_merge($this->css_files, $css_path);
        }
    }

    /**
     * Add a new javascript file in page header.
     *
     * @param mixed $js_uri
     * @return void
     */
    public function addJS($js_uri)
    {
        if (is_array($js_uri))
            foreach ($js_uri as $js_file) {
                $js_path = Media::getJSPath($js_file);
                if ($js_path && !in_array($js_path, $this->js_files))
                    $this->js_files[] = $js_path;
            }
        else {
            $js_path = Media::getJSPath($js_uri);
            if ($js_path)
                $this->js_files[] = $js_path;
        }
    }

    /**
     * Add a new javascript file in page header.
     *
     * @param mixed $js_uri
     * @return void
     */
    public function addJquery($version = null, $folder = null, $minifier = true)
    {
        $this->addJS(Media::getJqueryPath($version, $folder, $minifier));
    }

    /**
     * Add a new javascript file in page header.
     *
     * @param mixed $js_uri
     * @return void
     */
    public function addJqueryUI($component, $theme = 'base', $check_dependencies = true)
    {
        $ui_path = array();
        if (!is_array($component))
            $component = array($component);

        foreach ($component as $ui) {
            $ui_path = Media::getJqueryUIPath($ui, $theme, $check_dependencies);
            $this->addCSS($ui_path['css']);
            $this->addJS($ui_path['js']);
        }
    }

    /**
     * Add a new javascript file in page header.
     *
     * @param mixed $js_uri
     * @return void
     */
    public function addJqueryPlugin($name, $folder = null)
    {
        $plugin_path = array();
        if (is_array($name)) {
            foreach ($name as $plugin) {
                $plugin_path = Media::getJqueryPluginPath($plugin, $folder);
                if (!empty($plugin_path['js']))
                    $this->addJS($plugin_path['js']);
                if (!empty($plugin_path['css']))
                    $this->addCSS($plugin_path['css']);
            }
        } else
            $plugin_path = Media::getJqueryPluginPath($name, $folder);

        if (!empty($plugin_path['css']))
            $this->addCSS($plugin_path['css']);
        if (!empty($plugin_path['js']))
            $this->addJS($plugin_path['js']);
    }

    /**
     * @since 1.5
     * @return bool return true if Controller is called from XmlHttpRequest
     */
    public function isXmlHttpRequest()
    {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    protected function smartyOutputContent($content)
    {
        $this->context->cookie->write();
        $this->context->smarty->display($content);
    }
}
